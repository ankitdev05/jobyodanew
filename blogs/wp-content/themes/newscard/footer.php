<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NewsCard
 */

?>
		<?php global $newscard_settings; ?>
			</div><!-- row -->
		</div><!-- .container -->
	</div><!-- #content .site-content-->
	<footer id="colophon" class="site-footer" role="contentinfo">


		<?php if ( is_active_sidebar('newscard_footer_sidebar') || is_active_sidebar('newscard_footer_column2') || is_active_sidebar('newscard_footer_column3') || is_active_sidebar('newscard_footer_column4') ) { ?>
			<div class="widget-area">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-lg-4">
							<?php
								// Calling the Footer Sidebar Column 1
								if ( is_active_sidebar( 'newscard_footer_sidebar' ) ) :
									dynamic_sidebar( 'newscard_footer_sidebar' );
								endif;
							?>
						</div><!-- footer sidebar column 1 -->
						

						<div class="col-sm-6 col-lg-4">
							<?php
								// Calling the Footer Sidebar Column 2
								if ( is_active_sidebar( 'newscard_footer_column2' ) ) :
									dynamic_sidebar( 'newscard_footer_column2' );
								endif;
							?>
						</div><!-- footer sidebar column 2 -->
						<div class="col-sm-6 col-lg-4">
							<?php
								// Calling the Footer Sidebar Column 3
								if ( is_active_sidebar( 'newscard_footer_column3' ) ) :
									dynamic_sidebar( 'newscard_footer_column3' );
								endif;
							?>
						</div><!-- footer sidebar column 3 -->
						<!--<div class="col-sm-6 col-lg-3">
							<?php
								// Calling the Footer Sidebar Column 4
								// if ( is_active_sidebar( 'newscard_footer_column4' ) ) :
								// 	dynamic_sidebar( 'newscard_footer_column4' );
								// endif;
							?>
						</div>--><!-- footer sidebar column 4 -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .widget-area -->
		<?php } ?>

		<div class="site-info">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<?php
						if ( $newscard_settings['newscard_social_profiles'] != '' ) { ?>
							<div class="col-lg-auto order-lg-2 ml-auto">
								<div class="social-profiles">
									<?php echo esc_html( newscard_social_profiles() ); ?>
								</div>
							</div>
						<?php } ?>
						<div class="copyright">
							<div class="theme-link">
								<?php echo esc_html__('Copyright JobYoDA &copy; 2020. All Rights Reserved','newscard'); ?>
							</div> 
						</div> 
					</div>
					<div class="col-sm-6">
                 		<ul>
                       		<li>
                               	<a href="https://www.facebook.com/jobyodapage/" target="_blank">
                                    <i class="fa fa-facebook-f"></i>
                            	</a>
                         	</li>
                             
                        	<li>
                                <a href="https://instagram.com/jobyoda_ig?igshid=1u7xaxp20etlb" target="_blank">
                                  	<i class="fa fa-instagram"></i>
                            	</a>
                     		</li>  
                  		</ul>
              		</div>
				</div> 
			</div> 
		</div> 
	</footer> 

	<div class="back-to-top"><a title="<?php esc_attr_e('Go to Top','newscard');?>" href="#masthead"></a></div>
</div> 

<?php wp_footer(); ?>

</body>
</html>
