<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NewsCard
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<meta name="google-site-verification" content="059_geDz93Z9O9EKdpV5BF3oQAB_Vu6keNmwG_l2kc8" />
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-R64BFR96SR"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-R64BFR96SR');
      </script>
      <!-- Global site tag (gtag.js) - Google Ads: 851948051 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-851948051"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-851948051');
    </script>

	<?php wp_head(); ?>
</head>

<body <?php body_class('theme-body'); ?>>
<?php global $newscard_settings;
$newscard_settings = newscard_get_option_defaults(); ?>





<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'newscard' ); ?></a>
	<?php if (has_header_video() || has_header_image()) {
		the_custom_header_markup();
	} ?>

	<header id="masthead" class="site-header">
		<?php if ( $newscard_settings['newscard_top_bar_hide'] == 0 ) { ?>
			<div class="info-bar<?php echo ( has_nav_menu('right-section') ) ? ' infobar-links-on' : ''; ?>">
				<div class="container">
					<div class="row gutter-10">
						<div class="col col-sm contact-section">
							<div class="date">
								<ul><li><?php echo esc_html(date_i18n("l, F j, Y")); ?></li></ul>
							</div>
						</div><!-- .contact-section -->

						<?php if ( $newscard_settings['newscard_social_profiles'] != '' && $newscard_settings['newscard_top_bar_social_profiles'] === 0 ) { ?>
							<div class="col-auto social-profiles order-md-3">
								<?php echo esc_html( newscard_social_profiles() ); ?>
							</div><!-- .social-profile -->
						<?php }

						if ( has_nav_menu('right-section') ) { ?>
							<div class="col-md-auto infobar-links order-md-2">
								<button class="infobar-links-menu-toggle"><?php esc_html_e('Responsive Menu', 'newscard' ); ?></button>
								<?php wp_nav_menu( array(
									'theme_location'	=> 'right-section',
									'container'			=> '',
									'depth'				=> 1,
									'items_wrap'      	=> '<ul class="clearfix">%3$s</ul>',
								) ); ?>
							</div><!-- .infobar-links -->
						<?php } ?>
					</div><!-- .row -->
          		</div><!-- .container -->
        	</div><!-- .infobar -->
        <?php } ?>
		
		<nav class="navbar navbar-expand-lg d-block">
			<div class="navbar-head<?php echo ($newscard_settings['newscard_header_background'] !== '') ? ' navbar-bg-set' : '' ; echo ($newscard_settings['newscard_header_bg_overlay'] === 'dark') ? ' header-overlay-dark' : '' ; echo ($newscard_settings['newscard_header_bg_overlay'] === 'light') ? ' header-overlay-light' : '' ;?>" <?php if ($newscard_settings['newscard_header_background'] !== '') { ?> style="background-image:url('<?php echo esc_url($newscard_settings['newscard_header_background']); ?>');"<?php } ?>>
				
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-3">
							<div class="site-branding navbar-brand">
								<?php echo do_shortcode('[csl_display_logo]');?>
								<?php
								//the_custom_logo();

								if ( is_page_template('templates/front-page-template.php') || is_home() ) :
									?>
									<h1 class="site-title"><a href="https://jobyoda.com/" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
								<?php
								else :
									?>
									<h2 class="site-title"><a href="https://jobyoda.com/" rel="home"><?php bloginfo( 'name' ); ?></a></h2>
								<?php
								endif;
									$newscard_description = get_bloginfo( 'description', 'display' );
								if ( $newscard_description || is_customize_preview() ) :
									?>
									<p class="site-description"><?php echo $newscard_description; /* WPCS: xss ok. */ ?></p>
								<?php endif; ?>
							</div><!-- .site-branding .navbar-brand -->
						</div>
						
						
						<style type="text/css">
							.rightpan {
    float: right;
    display: flex;
    flex-direction: row;
}

.onedrimenu {
    display: flex;
    width: 100%;
}

.setbt {
    padding: 0 0 0 33px;
    text-align: center;
}

.setbt p.tgs {
    margin: 0 0 5px 0;
    text-align: center;
    font-size: 18px;
    font-weight: 600;
}
.setbt a {
    background: 
#1f3864;
padding: 7px 18px;
border-radius: 5px;
font-family: 'Quicksand', sans-serif;
margin: auto;
color:
    #fff;
    font-size: 14px;
}
.setbt a {
    background: 
#1f3864;
padding: 7px 18px;
border-radius: 5px;
font-family: 'Quicksand', sans-serif;
margin: auto;
color:
    #fff;
    font-size: 14px;
    font-weight: 500;
    display: block;
}
						</style>

						<?php if ( $newscard_settings['newscard_header_add_image'] == '' ) { ?>
							<div class="col-lg-9 navbar-ad-section menurightopn">

								<div class="rightpan">
				                    <div class="onedrimenu">
									    <div class="setbt">
										    <p class="tgs">Got a job to offer?</p>
				                          	<a href="https://jobyoda.com/recruiter/" target="_blank">Recruiter's Portal</a>
									    </div>
				  					    <div class="setbt">
				  					        <p class="tgs">Got skills to offer ?</p>
				                          	<a href="https://jobyoda.com/" target="_blank">Jobseekers</a>
				  					    </div>
									</div>
				                </div>

							</div>
						<?php } ?>

					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .navbar-head -->



			<div class="navigation-bar">
				<div class="navigation-bar-top">
					<div class="container">
						<button class="navbar-toggler menu-toggle" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="<?php esc_attr_e('Toggle navigation', 'newscard'); ?>"></button>
						<span class="search-toggle"></span>
					</div><!-- .container -->
					<div class="search-bar">
						<div class="container">
							<div class="search-block off">
								<?php get_search_form(); ?>
							</div><!-- .search-box -->
						</div><!-- .container -->
					</div><!-- .search-bar -->
				</div><!-- .navigation-bar-top -->
				<div class="navbar-main">
					<div class="container">
						<div class="collapse navbar-collapse" id="navbarCollapse">
							<div id="site-navigation" class="main-navigation<?php echo ($newscard_settings['newscard_nav_uppercase'] == 1) ? " nav-uppercase" : "";?>" role="navigation">
								<?php
								if ( has_nav_menu('primary') ) {
									wp_nav_menu( array(
										'theme_location'	=> 'primary',
										'container'			=> '',
										'items_wrap'		=> '<ul class="nav-menu navbar-nav d-lg-block">%3$s</ul>',
									) );
								} else {
									wp_page_menu( array(
										'before' 			=> '<ul class="nav-menu navbar-nav d-lg-block">',
										'after'				=> '</ul>',
									) );
								}
								?>
							</div><!-- #site-navigation .main-navigation -->
						</div><!-- .navbar-collapse -->
						<div class="nav-search">
							<span class="search-toggle"></span>
						</div><!-- .nav-search -->
					</div><!-- .container -->
				</div><!-- .navbar-main -->
			</div><!-- .navigation-bar -->
		</nav><!-- .navbar -->

		<?php if ( ( is_front_page() || is_home() ) && $newscard_settings['newscard_top_stories_hide'] === 0 ) {

			$newscard_cat_tp = absint($newscard_settings['newscard_top_stories_categories']);

			$post_type_tp = array(
				'posts_per_page' => 5,
				'post__not_in' => get_option('sticky_posts'),
				'post_type' => array(
					'post'
				),
			);
			if ( $newscard_settings['newscard_top_stories_latest_post'] == 'category' ) {
				$post_type_tp['category__in'] = $newscard_cat_tp;
			}

			$newscard_get_top_stories = new WP_Query($post_type_tp); ?>

			<div class="top-stories-bar">
				<div class="container">
					<div class="row top-stories-box clearfix">
						<div class="col-sm-auto">
							<div class="top-stories-label">
								<div class="top-stories-label-wrap">
									<span class="flash-icon"></span>
									<span class="label-txt">
										<?php echo esc_html($newscard_settings['newscard_top_stories_title']); ?>
									</span>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm top-stories-lists">
							<div class="row align-items-center">
								<div class="col">
									<div class="marquee">
										<?php while ($newscard_get_top_stories->have_posts()) {
											$newscard_get_top_stories->the_post();
											the_title( '<a href="' . esc_url( get_permalink() ) . '">', '</a>' );
										}
										// Reset Post Data
										wp_reset_postdata(); ?>
									</div><!-- .marquee -->
								</div><!-- .col -->
							</div><!-- .row .align-items-center -->
						</div><!-- .col-12 .col-sm .top-stories-lists -->
					</div><!-- .row .top-stories-box -->
				</div><!-- .container -->
			</div><!-- .top-stories-bar -->
		<?php } ?>

		<?php if ( ( ( is_front_page() || ( is_home() && $newscard_settings['newscard_banner_display'] === 'front-blog' ) ) && ( $newscard_settings['newscard_banner_slider_posts_hide'] === 0 || $newscard_settings['newscard_banner_featured_posts_1_hide'] === 0 || $newscard_settings['newscard_banner_featured_posts_2_hide'] === 0 ) ) || ( ( is_front_page() || ( is_home() && $newscard_settings['newscard_header_featured_posts_banner_display'] === 'front-blog' ) ) && $newscard_settings['newscard_header_featured_posts_hide'] === 0 ) ) { ?>
			
		<?php } ?>

		<?php if ( !is_front_page() && !is_home() && !is_page_template('templates/front-page-template.php') && function_exists('newscard_breadcrumbs') && $newscard_settings['newscard_breadcrumbs_hide'] === 0 ) { ?>
			<div id="breadcrumb">
				<div class="container">
					<?php newscard_breadcrumbs(); ?>
				</div>
			</div><!-- .breadcrumb -->
		<?php } ?>
	</header><!-- #masthead -->
	<?php 
		if(is_front_page() ||  is_home()) {
	?>
	<section>
        <div class="RecentArea">
            <div class="container">
            	<?php
            		global $wpdb;
            		$results = $wpdb->get_results("SELECT * FROM jd_posts WHERE post_status='publish' AND post_type='post' ORDER BY ID DESC LIMIT 7");
            		//var_dump($results);die;
            	?>
                <ul>
                    <li>
                        <article>
                            <a href="<?php echo esc_url( get_permalink($results[0]->ID) ); ?>">
                                <figure style='background-image:url("<?php echo site_url();?>/wp-content/uploads/2020/02/68942274_742792066139435_8891453515361157120_n.jpg")'>                                
                                </figure>
                            </a>
                            <aside>
                                <a class="Recent" href="javascript:void(0);">Recent Project</a>
                                <h3><a href="<?php echo esc_url( get_permalink($results[0]->ID) ); ?>"> <?php echo $results[0]->post_title; ?> </a></h3> 
                            </aside>
                        </article>
                        <article>
                        	<a href="<?php echo esc_url( get_permalink($results[1]->ID) ); ?>">
	                            <figure style='background-image:url("<?php echo site_url();?>/wp-content/uploads/2020/02/68942274_742792066139435_8891453515361157120_n.jpg")'>                                
	                            </figure>
	                        </a>
                            <aside>
                                <a class="Recent" href="javascript:void(0);">Recent Project</a> 
                                <h4><a href="<?php echo esc_url( get_permalink($results[1]->ID) ); ?>"><?php echo $results[1]->post_title; ?></a></h4>
                            </aside>
                        </article>
                    </li>
                    <li>
                        <article>
                        	<a href="<?php echo esc_url( get_permalink($results[2]->ID) ); ?>">
	                            <figure style='background-image:url("<?php echo site_url();?>/wp-content/uploads/2020/02/68942274_742792066139435_8891453515361157120_n.jpg")'>                                
	                            </figure>
	                        </a>
                            <aside>
                                <a class="Recent" href="javascript:void(0);">Recent Project</a>
                                <h4><a href="<?php echo esc_url( get_permalink($results[2]->ID) ); ?>"><?php echo $results[2]->post_title; ?></a></h4> 
                            </aside>
                        </article>
                        <article>
                        	<a href="<?php echo esc_url( get_permalink($results[3]->ID) ); ?>">
	                            <figure style='background-image:url("<?php echo site_url();?>/wp-content/uploads/2020/02/68942274_742792066139435_8891453515361157120_n.jpg")'>                                
	                            </figure>
	                        </a>
                            <aside>
                                <a class="Recent" href="javascript:void(0);">Recent Project</a> 
                                <h4><a href="<?php echo esc_url( get_permalink($results[3]->ID) ); ?>"><?php echo $results[3]->post_title; ?></a></h4>
                            </aside>
                        </article>
                        <article>
                        	<a href="<?php echo esc_url( get_permalink($results[4]->ID) ); ?>">
	                            <figure style='background-image:url("<?php echo site_url();?>/wp-content/uploads/2020/02/68942274_742792066139435_8891453515361157120_n.jpg")'>                                
	                            </figure>
	                        </a>
                            <aside>
                                <a class="Recent" href="javascript:void(0);">Recent Project</a> 
                                <h4><a href="<?php echo esc_url( get_permalink($results[4]->ID) ); ?>"><?php echo $results[4]->post_title; ?></a></h4>
                            </aside>
                        </article>
                    </li>
                    <li>
                        <article>
                        	<a href="<?php echo esc_url( get_permalink($results[5]->ID) ); ?>">
	                            <figure style='background-image:url("<?php echo site_url();?>/wp-content/uploads/2020/02/68942274_742792066139435_8891453515361157120_n.jpg")'>                                
	                            </figure>
	                        </a>
                            <aside>
                                <a class="Recent" href="javascript:void(0);">Recent Project</a>
                                <h4><a href="<?php echo esc_url( get_permalink($results[5]->ID) ); ?>"><?php echo $results[5]->post_title; ?></a></h4> 
                            </aside>
                        </article>
                        <article>
                        	<a href="<?php echo esc_url( get_permalink($results[6]->ID) ); ?>">
	                            <figure style='background-image:url("<?php echo site_url();?>/wp-content/uploads/2020/02/68942274_742792066139435_8891453515361157120_n.jpg")'>                                
	                            </figure>
	                        </a>
                            <aside>
                                <a class="Recent" href="javascript:void(0);">Recent Project</a> 
                                <h3><a href="<?php echo esc_url( get_permalink($results[6]->ID) ); ?>"><?php echo $results[6]->post_title; ?></a></h3>
                            </aside>
                        </article>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </section>
    <?php
    	}
    ?>
	<div id="content" class="site-content <?php echo ( ( ( is_front_page() || ( is_home() && $newscard_settings['newscard_banner_display'] === 'front-blog' ) ) && ( $newscard_settings['newscard_banner_slider_posts_hide'] === 0 || $newscard_settings['newscard_banner_featured_posts_1_hide'] === 0 || $newscard_settings['newscard_banner_featured_posts_2_hide'] === 0 ) ) || ( ( is_front_page() || ( is_home() && $newscard_settings['newscard_header_featured_posts_banner_display'] === 'front-blog' ) ) && $newscard_settings['newscard_header_featured_posts_hide'] === 0 ) ) ? "pt-0" : ""; ?>">
		<div class="container">
			<?php if ( is_page_template('templates/front-page-template.php') ) { ?>
				<div class="row gutter-14 justify-content-center site-content-row">
			<?php } else { ?>
				<div class="row justify-content-center site-content-row">
			<?php } ?>
