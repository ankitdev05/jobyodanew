<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'jdblog' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'JobYODA@@)@)(*&^@2k20' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
define( 'FS_METHOD', 'direct' );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '2gE,~(@Z8(`XD#pXr^BzlW0F[,#+HN<.7Q~Fb_@b^5M`RnBJhJ)tb:uv6o7ukw{o' );
define( 'SECURE_AUTH_KEY',  '<|<whaH&oh+sw^6H)` {b(xtv_Z5O*%(X$Nvz@!p2{6gg.ae#,?kSf0?(Kum4mnr' );
define( 'LOGGED_IN_KEY',    'bj#U_~#$>$Tg17[87pwl-rxBg)I42%~rfQzK2]J?Tvw ouj1gD9CI#~Z.YZw8c8-' );
define( 'NONCE_KEY',        '?ErMq/ K/VyJL]Mj,/(mlo7|Pg5XNCnU^*v<!/_CW|AI[fSK);PKw,s/[@;mIJU{' );
define( 'AUTH_SALT',        ' B5(`^GrBAbTl(y2Ghz5>X=NWZe;0J7OM=c/0+3?P/6?tT@^BI?yTFS41w1q@nT[' );
define( 'SECURE_AUTH_SALT', 'EZP!i0nDGnC$G:*S{:X/ ,S1!]{a8^1phAS_{N:.oBt*GGc+trm]wv!4MKga/MYh' );
define( 'LOGGED_IN_SALT',   'z+s_9sRFu=a(%09hhSUYV]4&WUUOec%fw3y )Gt>xq~V*4k _9W6L?9E:l/!vX..' );
define( 'NONCE_SALT',       'rU*mdUNH{o2`P=uvd08YpaL)8FE|>P78e@Wpe_[,M?u*T5O]FVB1rr#B&#fR7KaJ' );


define('FORCE_SSL_ADMIN', true);
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'jd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
//Disable File Edits
define('DISALLOW_FILE_EDIT', true);