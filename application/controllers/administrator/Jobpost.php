<?php
ob_start();
ini_set("allow_url_fopen", 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Jobpost extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Jobpostadmin_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('Recruiteradmin_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
         $this->load->library('pdf');
         if($this->session->userdata('adminSession')) {
            
        } else {
            redirect("administrator/admin/index");
        }
    }

    public function index() {
        $listings = $this->Jobpostadmin_Model->job_fetchAll();
        $listget = array();

        if($listings) {
            foreach($listings as $listing) {
               $Listcount = $this->Jobpostadmin_Model->job_fetchsingle($listing['id']);
               $site_detail = $this->Jobpostadmin_Model->companyname_fetch($listing['company_id']);
               if(count($Listcount) >0){
                  $cc= 1;
               } else{
                  $cc=0;
               }

               $getCat = $this->Jobpostadmin_Model->categoryname_fetch($listing['category']);
               $getSubCat = $this->Jobpostadmin_Model->subcategoryname_fetch($listing['subcategory']);

               $currentDate = date("Y-m-d");
               $diff = strtotime($listing['jobexpire']) - strtotime($currentDate);
               $dyasLeft = abs(round($diff / 86400));
               $dyasLeft = $dyasLeft. " Days ";

               $listget[] = ["jobtitle"=> $listing['jobtitle'], 
                            "category"=>$getCat[0]['category'], 
                            "subcategory"=>$getSubCat[0]['subcategory']??'',
                            "created_at" => $listing['created_at'], 
                            "jobexpire" => $listing['jobexpire'], 
                            "daysleft"=>$dyasLeft, 
                            "experience"=>$listing['experience'], 
                            "address"=> $site_detail[0]['address'], 
                            "id" => $listing['id'], 
                            "cname" => $listing['cname'],
                            'opening' => $listing['opening'], 
                            "site_name" => $site_detail[0]['cname'], 
                            "company_id" =>$listing['company_id'], 
                            "haveList"=> $cc, 
                            'mode' => $listing['mode']
                        ];
            }
        }
        $data["jobLists"] = $listget;

        $subcategoryArray1 = array();
        $subcategory_list = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        foreach ($subcategory_list as $subcategory_lists) {
            $subcategoryArray1[] = [
              "id" => $subcategory_lists['id'],
              "category" => $subcategory_lists['category_id'],                    
              "subcategory" => $subcategory_lists['subcategory'],
            ];
        }

        $powerArray = array();
        $power_lists = $this->Common_Model->power_lists();
        foreach ($power_lists as $power_list) {
            
            $powerArray[] = [
              "id" => $power_list['id'],                  
              "power" => $power_list['power'],
            ];
        }

        $data["intrestedLists"] = $subcategoryArray1;
        $data["powerLists"] = $powerArray;
        $this->load->view('administrator/jobListing', $data);
    }

    public function jobnotifications() {

        $jobIds = explode(',',$this->input->post('checkarr'));
        $title = $this->input->post('title');
        $message = $this->input->post('message');
        $intrestedid = $this->input->post('intrestedid');
        $powerid = $this->input->post('powerid');
        
        if(count($title) > 0 && count($message) > 0) { 

          if(count($intrestedid) > 0 && isset($intrestedid)) { 

              foreach ($jobIds as $jobid) {
                  $getJob = $this->Jobpostadmin_Model->job_fetchnotificationsingle($jobid);
                  $jobsubcat = $getJob[0]['subcategory'];
                  $jobtitle = @$getJob[0]['jobTitle'];

                  if($getJob[0]['subrecruiter_id'] == 0) {
                    $joblocationid = $getJob[0]['recruiter_id'];
                  } else {
                    $joblocationid = $getJob[0]['subrecruiter_id'];
                  }
                  $joblocationid = $getJob[0]['company_id'];

                  $getLocation = $this->Common_Model->company_address_location($joblocationid);
                  //var_dump($getLocation);die;

                  $joblocation = $getLocation[0]['city'];
                  $notifyMsg = $jobtitle . "  - new job has been posted";

                  $getCatData = $this->Common_Model->subcategory_listsbysubcatid($jobsubcat);

                  $this->load->model('Jobseekeradmin_Model');
                  $userIdArr = array();
                  
                  foreach($intrestedid as $intrested) {
                    $getusers1 = $this->Jobseekeradmin_Model->jobseeker_bylocationlists($intrested, $joblocation);
                    if($getusers1) {
                      foreach($getusers1 as $getuser1) {
                        array_push($userIdArr, $getuser1['id']);
                      }
                    }
                  }
                  $userIdArr = array_unique($userIdArr);

                  $getusers = $this->Jobseekeradmin_Model->jobseeker_byIntrestedInId($userIdArr);

                  $record = ["job_id"=>$jobid, "recruiter_id"=>$joblocationid, "title"=>urlencode($title), "notification"=> urlencode($message), "intrestedIn"=>implode(',', $intrestedid)];
                  $notifyid = $this->Jobpost_Model->jobrecord_notifydata($record);

                  if($getusers) {                

                      foreach($getusers as $getuser) {

                          //$addLatLong = $this->getLatLong($getuser['location']);
                        if(strlen($getuser['latitude']) > 0) {

                          $findDistance = $this->distance($getLocation[0]['latitude'], $getLocation[0]['longitude'], $getuser['latitude'], $getuser['longitude'], "K");

                          if($findDistance <= 50) {
                            $saveNotify = ["notify_id"=>$notifyid, "user_id"=>$getuser['id'], "job_id"=>$jobid, "recruiter_id"=>$joblocationid, "title"=>urlencode($title), "notification"=> urlencode($message), "status"=>1];

                            $this->Jobpost_Model->newjob_notifydata($saveNotify);
                          }
                        }
                      }
                  }
              }

              $this->session->set_flashdata('success','Notification send successfully');
              redirect("administrator/Jobpost/index");
          
          } else if(count($powerid) > 0 && isset($powerid)) { 

              foreach ($jobIds as $jobid) {

                  $getJob = $this->Jobpostadmin_Model->job_fetchnotificationsingle($jobid);
                  $jobsubcat = $getJob[0]['subcategory'];
                  $jobtitle = @$getJob[0]['jobTitle'];


                  if($getJob[0]['subrecruiter_id'] == 0) {
                    $joblocationid = $getJob[0]['recruiter_id'];
                  } else {
                    $joblocationid = $getJob[0]['subrecruiter_id'];
                  }
                  $joblocationid = $getJob[0]['company_id'];

                  $getLocation = $this->Common_Model->company_address_location($joblocationid);

                  $joblocation = $getLocation[0]['city'];
                  $notifyMsg = $jobtitle . "  - new job has been posted";

                  $getCatData = $this->Common_Model->subcategory_listsbysubcatid($jobsubcat);

                  $this->load->model('Jobseekeradmin_Model');
                  $userIdArr = array();
                  
                  foreach($powerid as $power) {
                    $getusers1 = $this->Jobseekeradmin_Model->jobseekerpower_bylocationlists($power, $joblocation);
                      
                      if($getusers1) {
                          foreach($getusers1 as $getuser1) {
                            
                            if($getuser1['id'] == 93915) {
                              array_push($userIdArr, $getuser1['id']);
                            }
                          }
                      }
                  }

                  // var_dump($userIdArr);
                  // die;
                  
                  $userIdArr = array_unique($userIdArr);

                  $getusers = $this->Jobseekeradmin_Model->jobseeker_byIntrestedInId($userIdArr);

                  $record = ["job_id"=>$jobid, "recruiter_id"=>$joblocationid, "title"=>urlencode($title), "notification"=> urlencode($message), "superpower"=>implode(',', $powerid)];
                  $notifyid = $this->Jobpost_Model->jobrecord_notifydata($record);

                  if($getusers) {                

                      foreach($getusers as $getuser) {

                        if(strlen($getuser['latitude']) > 0) {

                          $findDistance = $this->distance($getLocation[0]['latitude'], $getLocation[0]['longitude'], $getuser['latitude'], $getuser['longitude'], "K");

                          if($findDistance <= 50) {
                            $saveNotify = ["notify_id"=>$notifyid, "user_id"=>$getuser['id'], "job_id"=>$jobid, "recruiter_id"=>$joblocationid, "title"=>urlencode($title), "notification"=> urlencode($message), "status"=>1];

                            $this->Jobpost_Model->newjob_notifydata($saveNotify);
                          }
                        }
                      }
                  }
              }

              $this->session->set_flashdata('success','Notification send successfully');
              redirect("administrator/Jobpost/index");
          
          } else {
              $this->session->set_flashdata('success','Notification not able to send');
              redirect("administrator/Jobpost/index");
          }
          
        } else {
              $this->session->set_flashdata('success','Notification not able to send');
              redirect("administrator/Jobpost/index");
        }
    }

    public function jobexpirelist() {
        $listings = $this->Jobpostadmin_Model->jobexpire_fetchAll();
        $listget = array();

        if($listings) {
            foreach($listings as $listing) {
               $Listcount = $this->Jobpostadmin_Model->job_fetchsingle($listing['id']);
               $site_detail = $this->Jobpostadmin_Model->companyname_fetch($listing['company_id']);
               if(count($Listcount) >0){
                  $cc= 1;
               } else{
                  $cc=0;
               }
               $listget[] = ["jobtitle"=> $listing['jobtitle'], "created_at" => $listing['created_at'], "jobexpire" => $listing['jobexpire'], "experience"=>$listing['experience'], "address"=> $site_detail[0]['address'], "id" => $listing['id'], "cname" => $listing['cname'],'opening' => $listing['opening'], "site_name" => $site_detail[0]['cname'], "company_id" =>$listing['company_id'], "haveList"=> $cc];
            }
        }
        $data["jobLists"] = $listget;
        $this->load->view('administrator/jobExpireListing', $data);
    }


    public function hiring_reports(){

        $this->load->view('administrator/hiring_reports');
    }

    public function screening_reports(){

        $this->load->view('administrator/screening_reports');
    }

    public function hirecost(){
      $listings = $this->Recruiteradmin_Model->recruitercost_lists(); 
      $listget = array();

        if($listings) {
            foreach($listings as $listing) {
               $Listcount = $this->Recruiteradmin_Model->hiredcandidates_lists($listing['id']);
               //echo "<pre>";
               //echo count($Listcount);
               $invoices = $this->Jobpostadmin_Model->get_invoices_data($listing['id']);
               if(count($Listcount) >0){
                  $cc= 1;
                  $countt = count($Listcount);
               } else{
                  $cc=0;
                  $countt = 0;
               }

               if(count($invoices) >0){
                  $ic= 1;
               } else{
                  $ic=0;
               }
               $listget[] = ["cname"=> $listing['cname'], "cost"=>$listing['cost'], "effective_date"=> $listing['effective_date'], "billing_date"=> $listing['billing_date'],"id"=>$listing['id'], "haveList"=> $cc,'countt'=>$countt , "iclist"=>$ic];
            }
        }
        $data["Lists"] = $listget;
      
        if(!empty($data["Lists"])){
            $this->load->view('administrator/hirecost', $data);
        }
        else{
            $this->session->set_flashdata('invoice_error','Invoice Not available');
            redirect("administrator/Jobpost/hirecost");
        }
    }

    public function notappliedjob_reports(){
        $data["jobLists"] = $this->Jobpostadmin_Model->notappliedjob_fetchAll();

        $this->load->view('administrator/notappliedjob_reports',$data);
    }

    public function user_reports() {
        $data = array();

        //$data["jobLists"] = $this->Jobpostadmin_Model->notappliedjob_fetchAll();
        $data["jobLists"] = [];

        //---------------------------------------------------------------------------------------
        
        $perPage = 10;
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->library('session');

        // If search request submitted

        if($this->input->post('submitSearch')){
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if(!empty($searchKeyword)){
                $this->session->set_userdata('searchKeyword',$searchKeyword);
            }else{
                $this->session->unset_userdata('searchKeyword');
            }

            $inputStatus = $this->input->post('status_dropdown');
            $searchStatus = strip_tags($inputStatus);

            if(!empty($searchStatus)) {
                $this->session->set_userdata('searchStatus',$searchStatus);
            }else{
                $this->session->unset_userdata('searchStatus');
            }
            
        } elseif($this->input->post('submitSearchReset')) {
            $this->session->unset_userdata('searchKeyword');
            $this->session->unset_userdata('searchStatus');
        
        } else {
            $inputKeywords = $this->input->post('searchKeyword');
            $searchKeyword = strip_tags($inputKeywords);
            if(!empty($searchKeyword)){
                $this->session->set_userdata('searchKeyword',$searchKeyword);
            }else{
                if($this->uri->segment(3) == 0) {
                  $this->session->unset_userdata('searchKeyword');
                }
            }

            $inputStatus = $this->input->post('status_dropdown');
            $searchStatus = strip_tags($inputStatus);

            if(!empty($searchStatus)) {
                $this->session->set_userdata('searchStatus',$searchStatus);
            }else{
                if($this->uri->segment(3) == 0) {
                  $this->session->unset_userdata('searchStatus');
                }
            }
        }

        $data['searchKeyword'] = $this->session->userdata('searchKeyword');
        $data['searchStatus'] = $this->session->userdata('searchStatus');

        // Get rows count
        $conditions['searchKeyword'] = $data['searchKeyword'];
        $conditions['searchStatus'] = $data['searchStatus'];
        $conditions['returnType']    = 'count';
        $rowsCount = $this->Jobpostadmin_Model->getRowsUsers($conditions);
        //var_dump($rowsCount);die;
        // Pagination config
        $config['base_url']    = base_url().'administrator/userreport';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $rowsCount;
        $config['per_page']    = $perPage;

        // Initialize pagination library
        $this->pagination->initialize($config);
        
        // Define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;

        // Get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $perPage;

        $data['result'] = $this->Jobpostadmin_Model->getRowsUsers($conditions);
        $data['resultTotal'] = $rowsCount;
        
        if(strlen($data['searchStatus']) > 0) {
          $data['checkactive'] = 1;  

        } else if(strlen($data['searchKeyword']) > 0) {
          $data['checkactive'] = 1;  
        } else {
          $data['checkactive'] = $offset;
        }

        //var_dump($data['checkactive']);die;
        //---------------------------------------------------------------------------------------

        $this->load->view('administrator/user_reports',$data);
    }

    public function export_csv_report() {

        $filename = 'users_'.date('Ymd').'.csv'; 
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; ");

        // get data
        $status = "all";
        $date =  date('Y-m-d');
        $usersData = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);

        // file creation 
        $file = fopen('php://output','w');
        $header = array("SNo","Name","Email","Phone","Registration Date","Experience","Location","State","City","Intrested In","Education","Nationality","Superpower","Platform","App Version","Last Used","Signup Via"); 
        fputcsv($file, $header);

        $x1 =1;
        $dataArray = array();

          foreach($usersData as $jobList) {

            if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ) {

                if($jobList['phone']==0){
                  $jobList['phone']='';
                }else{
                  $jobList['phone']=$jobList['phone'];
                }
                
                if($jobList['exp_year']>1){
                  $exp_year = $jobList['exp_year'].' Years'; 
                }else{
                  $exp_year = $jobList['exp_year'].' Year';
                }
                
                if($jobList['exp_month']>1){
                  $exp_month = $jobList['exp_month'].' Months'; 
                }else{
                  $exp_month = $jobList['exp_month'].' Month';
                }
                if(!empty($jobList['device_type'])){
                  $jobList['device_type']=$jobList['device_type'];
                }else{
                  $jobList['device_type']='web';
                }

                if(!empty($jobList['state'])){
                  $jobList['state']=$jobList['state'];
                }else{
                  $jobList['state']=" ";
                }

                if(!empty($jobList['city'])){
                  $jobList['city']=$jobList['city'];
                }else{
                  $jobList['city']=" ";
                }

                if(!empty($jobList['jobsInterested'])){
                  $jobList['jobsInterested']=$jobList['jobsInterested'];
                }else{
                  $jobList['jobsInterested']=" ";
                }

                if(!empty($jobList['app_version'])){
                  $jobList['app_version']=$jobList['app_version'];
                }else{
                  $jobList['app_version']=" ";
                }

                if(!empty($jobList['last_used'])){
                  $jobList['last_used']=$jobList['last_used'];
                }else{
                  $jobList['last_used']=" ";
                }

                $dataArray[] = ["srno"=>$x1, 
                               "name"=>$jobList['name'], 
                               "email"=>$jobList['email'],
                               "phone"=>$jobList['phone'],
                               "created_at"=>date('Y-m-d', strtotime($jobList['created_at'])),
                               "experience"=>$exp_year.' '.$exp_month,
                               "location"=>$jobList['location'],
                               "state"=>$jobList['state'],
                               "city"=>$jobList['city'],
                               "jobsInterested"=>$jobList['jobsInterested'],
                               "education"=>$jobList['education'],
                               "nationality"=>$jobList['nationality'],
                               "superpower"=>$jobList['superpower'],
                               "device_type"=>$jobList['device_type'],
                               "app_version"=>$jobList['app_version'],
                               "last_used"=>$jobList['last_used'],
                               "type"=>$jobList['type'],
                            ];
              $x1++;
            }
          }
          // var_dump($dataArray);die;
          
        foreach ($dataArray as $key=>$line) { 
          fputcsv($file,$line); 
        }
        fclose($file); 
        exit; 
    }

    public function non_user_reports() {
        //$data["jobLists"] = $this->Jobpostadmin_Model->notappliedjob_fetchAll();

        $this->load->view('administrator/non_user_reports');
    }

    public function invoice(){
        $company_id = base64_decode($this->input->get('id'));
        //echo $company_id;die;
        $data["invoices"] = $this->Jobpostadmin_Model->get_invoices_data($company_id);

        $this->load->view('administrator/invoice',$data);
    }

    public function getuserDatabyDay(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">App Version</span>
             </div>
          </th>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Login</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }

              if(!empty($jobList['device_type'])){
                if($jobList['device_type']=='android'){
                  $jobList['device_type']='Android';
                }
                else{
                  $jobList['device_type']='iOS';
                }
              }else{
                $jobList['device_type']='Web';
              }

              if($jobList['type']=='normal'){
                $jobList['type'] = 'Normal';
              }elseif($jobList['type']=='gmail'){
                $jobList['type'] = 'Gmail';
              }elseif($jobList['type']=='apple'){
                $jobList['type'] = 'Apple';
              }else{
                $jobList['type'] = 'Facebook';
              }

              $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getnonuserDatabyDay(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }

              if(!empty($jobList['device_type'])){
                if($jobList['device_type']=='android'){
                  $jobList['device_type']='Android';
                }
                else{
                  $jobList['device_type']='iOS';
                }
              }else{
                $jobList['device_type']='Web';
              }

              if($jobList['type']=='normal'){
                $jobList['type'] = 'Normal';
              }elseif($jobList['type']=='gmail'){
                $jobList['type'] = 'Gmail';
              }elseif($jobList['type']=='apple'){
                $jobList['type'] = 'Apple';
              }else{
                $jobList['type'] = 'Facebook';
              }

              $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getuserDatabyWeek(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
 <div class="table-header">
    <span class="column-title">App Version</span>
 </div>
</th>

<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Used</span>
             </div>
          </th>

<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>

       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getnonuserDatabyWeek(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getuserDatabyMonth(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">App Version</span>
 </div>
</th>

          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">Last Used</span>
 </div>
</th>

<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getnonuserDatabyMonth(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getuserDatabyYear(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">App Version</span>
 </div>
</th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">Last Used</span>
 </div>
</th>
<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
              if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }
              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


    public function getnonuserDatabyYear(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getDatabyStatus(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
      
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Time</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $subcategoryy = $subcategory[0]['subcategory'];
            } else {
                $subcategoryy = '';
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        if($jobList['status']=='1'){
          $jobList['status'] = 'New Application';
        }else if($jobList['status']=='2'){
          $jobList['status']='No Show';
        }else if($jobList['status']=='3'){
          $jobList['status']='Fall Out';
        }else if($jobList['status']=='4'){
          $jobList['status']='Refer';
        }else if($jobList['status']=='5'){
          $jobList['status']='On Going Application';
        }else if($jobList['status']=='6'){
          $jobList['status']='Accepted JO';
        }else if($jobList['status']=='7'){
          $jobList['status']='Hired';
        }else if($jobList['status']=='8'){
          $jobList['status']='reschedule';
        } else {
          $jobList['status']='';
        }
        if($jobList['status']=='Hired'){
          $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
        }else{
          $hiredDate = '';
        }
             if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategoryy.'</td>
        <td style="width:70px;">'.$jobList['uid'].'</td>
        <td style="width:70px;">'.$jobList['name'].'</td>
        <td style="width:70px;">'.$jobList['email'].'</td>
    <td style="width:70px;">'.$jobList['phone'].'</td>
    <td style="width:70px;">'.$jobList['superpower'].'</td>
    <td style="width:70px;">'.$jobList['interviewdate'].'</td>
    <td style="width:70px;">'.$jobList['interviewtime'].'</td>
    <td style="width:70px;">'.$jobList['created_at'].'</td>
        <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
        <td style="width:70px;">'.$jobList['allowance'].'</td>
              
              <td style="width:70px;">'.$jobList['status'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
        <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
        <td style="width:70px;">'.$hiredDate.'</td>
        <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
              
           </tr>';
           $x1++;
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


    public function getuserDatabyDate(){
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Experience</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Location</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">State</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">City</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Intrested In</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Education</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Nationality</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Superpower</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">App Version</span>
 </div>
</th>
          <th class="secondary-text">
 <div class="table-header">
    <span class="column-title">Last Used</span>
 </div>
</th>
<th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date && date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date) {
              
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
              
              if($jobList['exp_year']>1){
                $exp_year = $jobList['exp_year'].' Years'; 
              }else{
                $exp_year = $jobList['exp_year'].' Year';
              }

              
              if($jobList['exp_month']>1){
                $exp_month = $jobList['exp_month'].' Months'; 
              }else{
                $exp_month = $jobList['exp_month'].' Month';
              }
              if(!empty($jobList['device_type'])){
                $jobList['device_type']=$jobList['device_type'];
              }else{
                $jobList['device_type']='web';
              }
              
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$exp_year.' '.$exp_month. '</td>
              <td style="width:70px;">'.$jobList['location'].'</td>
              <td style="width:70px;">'.$jobList['state'].'</td>
              <td style="width:70px;">'.$jobList['city'].'</td>
              <td style="width:70px;">'.$jobList['jobsInterested'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['nationality'].'</td>
              <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['app_version'].'</td>
              <td style="width:70px;">'.$jobList['last_used'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getnonuserDatabyDate(){
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getnonuserDatabyDay($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Platform</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Signup Via</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date && date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date){
              if($jobList['phone']==0){
                $jobList['phone']='';
              }else{
                $jobList['phone']=$jobList['phone'];
              }
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['device_type'].'</td>
              <td style="width:70px;">'.$jobList['type'].'</td>
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


    public function getDatabyStatusweek(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $status;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo "<pre>";
        //print_r($data);die;
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Time</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory =  $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }
              
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        if($jobList['status']=='1'){
          $jobList['status'] = 'New Application';
        }else if($jobList['status']=='2'){
          $jobList['status']='No Show';
        }else if($jobList['status']=='3'){
          $jobList['status']='Fall Out';
        }else if($jobList['status']=='4'){
          $jobList['status']='Refer';
        }else if($jobList['status']=='5'){
          $jobList['status']='On Going Application';
        }else if($jobList['status']=='6'){
          $jobList['status']='Accepted JO';
        }else if($jobList['status']=='7'){
          $jobList['status']='Hired';
        }else if($jobList['status']=='8'){
          $jobList['status']='reschedule';
        } else {
          $jobList['status']='';
        }
        if($jobList['status']=='Hired'){
          $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
        }else{
          $hiredDate = '';
        }
             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
              <td style="width:70px;">'.$jobList['uid'].'</td>
              <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;">'.$jobList['email'].'</td>
        <td style="width:70px;">'.$jobList['phone'].'</td>
        <td style="width:70px;">'.$jobList['superpower'].'</td>
              <td style="width:70px;">'.$jobList['interviewdate'].'</td>
              <td style="width:70px;">'.$jobList['interviewtime'].'</td>
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              
              <td style="width:70px;">'.$jobList['status'].'</td>
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
              <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
              <td style="width:70px;">'.$hiredDate.'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getDatabyStatusmonth(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Time</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory = $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        if($jobList['status']=='1'){
          $jobList['status'] = 'New Application';
        }else if($jobList['status']=='2'){
          $jobList['status']='No Show';
        }else if($jobList['status']=='3'){
          $jobList['status']='Fall Out';
        }else if($jobList['status']=='4'){
          $jobList['status']='Refer';
        }else if($jobList['status']=='5'){
          $jobList['status']='On Going Application';
        }else if($jobList['status']=='6'){
          $jobList['status']='Accepted JO';
        }else if($jobList['status']=='7'){
          $jobList['status']='Hired';
        }else if($jobList['status']=='8'){
          $jobList['status']='reschedule';
        } else {
          $jobList['status']='';
        }

        if($jobList['status']=='Hired'){
          $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
        }else{
          $hiredDate = '';
        }

             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
        <td style="width:70px;">'.$jobList['uid'].'</td>
        <td style="width:70px;">'.$jobList['name'].'</td>
        <td style="width:70px;">'.$jobList['email'].'</td>
    <td style="width:70px;">'.$jobList['phone'].'</td>
    <td style="width:70px;">'.$jobList['superpower'].'</td>
        <td style="width:70px;">'.$jobList['interviewdate'].'</td>
        <td style="width:70px;">'.$jobList['interviewtime'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['created_at'])).'</td>
        <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
        <td style="width:70px;">'.$jobList['allowance'].'</td>
              
              <td style="width:70px;">'.$jobList['status'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
        <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
        <td style="width:70px;">'.$hiredDate.'</td>
        <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
           </tr>';
           $x1++;
             }
       
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getDatabyStatusyear(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
     
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Time</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>


          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
              $category = $this->Jobpost_Model->categorybyid($jobList['category']);

              $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);

              if($subcategory) {
                  $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
                  
                  $expmonth = $jobList['exp_month'];
                  $expyear = $jobList['exp_year'];

                  if($expyear == 0 && $expmonth == 0) {
                      $expfilter = "1";
                  }else if($expyear == 0 && $expmonth < 6) {
                      $expfilter = "2";
                  } else if($expyear < 1 && $expmonth >= 6) {
                      $expfilter = "3";
                  } else if($expyear < 2 && $expyear >= 1) {
                      $expfilter = "4";
                  } else if($expyear < 3 && $expyear >= 2) {
                      $expfilter = "5";
                  }else if($expyear < 4 && $expyear >= 3) {
                      $expfilter = "6";
                  }else if($expyear < 5 && $expyear >= 4) {
                      $expfilter = "7";
                  }else if($expyear < 6 && $expyear >= 5) {
                      $expfilter = "8";
                  }else if($expyear < 7 && $expyear >= 6) {
                      $expfilter = "9";    
                  } else if($expyear >= 7) { 
                      $expfilter = "10";
                  }

                  $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
                  if($jobList['status']=='1'){
                    $jobList['status'] = 'New Application';
                  }else if($jobList['status']=='2'){
                    $jobList['status']='No Show';
                  }else if($jobList['status']=='3'){
                    $jobList['status']='Fall Out';
                  }else if($jobList['status']=='4'){
                    $jobList['status']='Refer';
                  }else if($jobList['status']=='5'){
                    $jobList['status']='On Going Application';
                  }else if($jobList['status']=='6'){
                    $jobList['status']='Accepted JO';
                  }else if($jobList['status']=='7'){
                    $jobList['status']='Hired';
                  }else if($jobList['status']=='8'){
                    $jobList['status']='reschedule';
                  } else {
                    $jobList['status']='';
                  }
                  
                  if($jobList['status']=='Hired'){
                    $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
                  }else{
                    $hiredDate = '';
                  } 
                 
                 if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
                    $output.=' <tr>
                    <td style="width:40px;">'. $jobList['cname'].'</td>
                    <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
                    <td style="width:100px;">'. $jobList['id'].'</td>
                    <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                    <td style="width:70px;">'.$jobList['jobtitle'].'</td>
                    <td style="width:70px;">'.$jobList['mode'].'</td>
                    <td style="width:70px;">'.$category[0]['category'].'</td>
                    <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
                    <td style="width:70px;">'.$jobList['uid'].'</td>
                    <td style="width:70px;">'.$jobList['name'].'</td>
                    <td style="width:70px;">'.$jobList['email'].'</td>
                    <td style="width:70px;">'.$jobList['phone'].'</td>
                    <td style="width:70px;">'.$jobList['superpower'].'</td>
                    <td style="width:70px;">'.$jobList['interviewdate'].'</td>
                    <td style="width:70px;">'.$jobList['interviewtime'].'</td>
                    <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['created_at'])).'</td>
                    <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
                    <td style="width:70px;">'.$jobList['allowance'].'</td>
                         
                          <td style="width:70px;">'.$jobList['status'].'</td>
                    <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
                    <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
                    <td style="width:70px;">'.$hiredDate.'</td>
                    <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                    
                    </tr>';
                    $x1++;
                 }
              }
          }
          
          $output.= '</tbody>';

        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function getDatabyStatusDate(){
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Super Power</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Time</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Applied Date</span>
             </div>
          </th>
          
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out Reason</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory = $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        if($jobList['status']=='1'){
          $jobList['status'] = 'New Application';
        }else if($jobList['status']=='2'){
          $jobList['status']='No Show';
        }else if($jobList['status']=='3'){
          $jobList['status']='Fall Out';
        }else if($jobList['status']=='4'){
          $jobList['status']='Refer';
        }else if($jobList['status']=='5'){
          $jobList['status']='On Going Application';
        }else if($jobList['status']=='6'){
          $jobList['status']='Accepted JO';
        }else if($jobList['status']=='7'){
          $jobList['status']='Hired';
        }else if($jobList['status']=='8'){
          $jobList['status']='reschedule';
        } else {
          $jobList['status']='';
        }
        if($jobList['status']=='Hired'){
          $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
        }else{
          $hiredDate = '';
        }
             
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date &&  date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
        <td style="width:70px;">'.$jobList['uid'].'</td>
        <td style="width:70px;">'.$jobList['name'].'</td>
        <td style="width:70px;">'.$jobList['email'].'</td>
    <td style="width:70px;">'.$jobList['phone'].'</td>
    <td style="width:70px;">'.$jobList['superpower'].'</td>
        <td style="width:70px;">'.$jobList['interviewdate'].'</td>
        <td style="width:70px;">'.$jobList['interviewtime'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['created_at'])).'</td>
        <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
        <td style="width:70px;">'.$jobList['allowance'].'</td>
              
              <td style="width:70px;">'.$jobList['status'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
        <td style="width:70px;">'.$jobList['fallout_reason'].'</td>
        <td style="width:70px;">'.$hiredDate.'</td>
        <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function invoicebyDate(){
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $data = $this->Recruiteradmin_Model->invoice_lists();
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Company</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Invoice</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Date</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['date'])) >= $from_date && date('Y-m-d', strtotime($jobList['date'])) <= $to_date){
                $invoice_path = base_url().$jobList['invoice'];
                $invoice_date = date("Y-m-d",strtotime($jobList['date']));
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['cname'].'</td>
              <td style="width:100px;"><a href="'.$invoice_path.'">Download</a></td>
              <td style="width:70px;">'.$invoice_date.'</td>
              
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


    public function boostjobs() {
        $data["jobLists"] = $this->Jobpostadmin_Model->boostjob_fetch();
        //echo $this->db->last_query();die;
        $data["boost_amount"] = $this->Jobpostadmin_Model->boost_amount();
        $this->load->view('administrator/boostjobs', $data);
    }

    public function generatepdf()
     {

        $jid = $this->input->get('job_id');
        $amount = $this->input->get('amount');
        $hiredLists = $this->Jobpostadmin_Model->boostjob_fetchbyjobid($jid); 
        //print_r($hiredLists);die;
        $invoice_number = "JobYoDa".rand(10000,99999);
        $output = '<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    

                    <table>
                    <tbody>
                    <tr>
                        <td style="width:300px;">
                         <img src="https://jobyoda.com/recruiterfiles/images/jobyoda.png" width="200px;">
                        </td>
                        <td style="width:400px;">
                        <div style="float:right;">
                        <p class="font-weight-bold mb-1">Invoice #'.$invoice_number.'</p>
                            <p class="text-muted">Invoice Generated Date: '.date("d M, Y").'</p>
                        </div>
                        </td>
                    </tr>
                    </tbody>
                    </table>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6" style="width:300px; margin-bottom:30px; margin-top:30px;">
       <p class="font-weight-bold mb-4" style="font-weight: bold !important; margin:0;">
       Company Information</p>
                            <p class="mb-1" style="margin:0;">'.$hiredLists[0]['cname'].'</p>
                            
                            <p class="mb-1" style="margin:0;">'.$hiredLists[0]['address'].'</p>
                            
                        </div>

                    </div>

                    <div class="row p-5">
                        <div class="col-md-12" style="width:800px;">
                            <table class="table" style="border:1px solid #ddd;">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">S No</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Job Title</th>
                                        
                                        <th class="border-0 text-uppercase small font-weight-bold">Company Name</th>
                                        
                                        
                                        <th class="border-0 text-uppercase small font-weight-bold">Date</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>';
                                $x2=1;
                                $total_amount=0;
                                foreach ($hiredLists as $hiredList) {
                                  $data = ["payment_status" => 1];
                                     $this->Recruiteradmin_Model->job_payment_update($data,$hiredList['rid'],$hiredList['id']);
                                    $hiredDate = date("d-m-Y", strtotime($hiredList['created_at']));
                                    $output.= '<tr>
                                        <td style="width:50px;">'.$x2.'</td>
                                        <td style="width:140px;">'.$hiredList['jobtitle'].'</td>
                                        <td style="width:140px;">'.$hiredList['cname'].'</td>
                                        
                                        <td style="width:140px;">'.$hiredDate.'</td>
                                        <td style="width:140px;">'.$amount.'</td>
                                        
                                    </tr>';
                                $x2++;
                                $total_amount+= $amount;
                                }
                                    
                               $output.= '</tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        <div class="py-3 px-5 text-right" style="margin-top:30px; background:#000; padding:5px;">
                            <div class="mb-2" style="text-align:right; font-size:30px;color:#fff;">Grand Total</div>
                            <div class="h2 font-weight-light" style="text-align:right;color:#fff; margin:0;">'.$total_amount.'</div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            
                        </div>

                        <div class="py-3 px-5 text-right">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

</div>';
       $file_name = "./recruiterupload/".md5(rand()) . '.pdf';
       $this->pdf->loadHtml($output);
       $this->pdf->render();
       
       // echo $this->pdf->stream();die;
       $file = $this->pdf->output();
       file_put_contents($file_name,  $file );
       $this->load->library('email');
            $this->email->from("help@jobyoda.com", "JobYoDa");
            $this->email->to($hiredLists[0]['email']);
            $this->email->subject('JobYoda - Invoice');
            $msg = "Dear ".$hiredLists[0]['cname'] ;
            $msg .= ", Please check the invoice as below: ";
            $this->email->message($msg);
            $this->email->attach($file_name);
            if($this->email->send()){
                $this->pdf->stream($file_name, array("Attachment"=>0));
                $data = ["invoice_id" => $invoice_number, "total_amount" => $total_amount, "company_id" => $hiredLists[0]['rid'] , "invoice" => $file_name , "jobpost_id" => $jid, 'type'=>2];
                $this->Recruiteradmin_Model->invoice_insert($data);
            }else{
                echo $hiredLists[0]['email'];
            }
            
            //$this->email->attach($file_name);
       //$this->pdf->stream("test.pdf", array("Attachment"=>1));
      
     }
    
    public function jobpostviewpage() {
        $jobId = base64_decode($_GET['id']);
        $role = base64_decode($_GET['role']); 
        $data['getJobs'] = $this->Jobpostadmin_Model->jobupdate_detail_fetch($jobId);
        $data['getJobloc'] = $this->Jobpostadmin_Model->jobupdate_location_fetch($role);
        $data['jobExps'] = $this->Jobpostadmin_Model->jobupdate_detailexp_fetch($jobId);
        //echo $this->db->last_query();die;
        $data['recruiterExps'] = $this->Jobpostadmin_Model->jobupdate_recruiterexp_fetch($role);
        $data['industryLists'] = $this->Jobpostadmin_Model->industry_lists($data['getJobs'][0]['industry']);
        $data['levels'] = $this->Jobpostadmin_Model->level_lists($data['getJobs'][0]['level']);
        $data['langs'] = $this->Jobpostadmin_Model->language_lists($data['getJobs'][0]['language']);
        $jobskills = $this->Jobpost_Model->job_skills($jobId);
        
        if(empty($jobskills)){
                $jobskills[]= ['skill'=>$data['getJobs'][0]['skills']];
            }
        $data['jobskills'] = $jobskills;
        $this->load->view('administrator/jobpostview', $data);
    }
    
    public function candidatelistpage() {
        $jobId = base64_decode($_GET['id']);
        $data["jobLists"] = $this->Jobpostadmin_Model->job_fetchsingle($jobId);
        //echo $this->db->last_query();die;
        $this->load->view('administrator/candidatestatuslists', $data);
    }
    
    public function jobpostdelete() {
        $jid = $_GET['id'];
        $this->Jobpostadmin_Model->delete_jobpost($jid);
        $this->Jobpostadmin_Model->delete_jobpostlocation($jid);
        redirect("administrator/jobpost/index");
    }

    public function updateJobpost() {
        $jobId = base64_decode($_GET['id']);
        $role = base64_decode($_GET['role']);
        $data['getJobs'] = $this->Jobpostadmin_Model->jobupdate_detail_fetch($jobId);
        $data['addresses'] = $this->Common_Model->companysite_lists();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists1();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $skills = $this->Jobpostadmin_Model->jobs_skills_single($jobId);
        
        if($skills) { 
            $x1=0;
            foreach ($skills as $skill) {
              $skills1[$x1] = $skill['skill_id'];
              $x1++;
            }
            $data['skills2'] = $skills1;
        } else{
            $data['skills2'] = [];
        }
        $data['skills'] = $this->Jobpostadmin_Model->skill_lists();
        
        $data['getJobloc'] = $this->Jobpostadmin_Model->jobupdate_location_fetch($data['getJobs'][0]['recruiter_id']);
        $data['jobExps'] = $this->Jobpostadmin_Model->jobupdate_detailexp_fetch($jobId);
        //$data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($jobId);
        $data['recruiterExps'] = $this->Jobpostadmin_Model->jobupdate_recruiterexp_fetch($role);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $this->load->view('administrator/updatejob', $data);
    }
    
    public function jobPostUpdate() {
        $jobData = $this->input->post();
        $this->form_validation->set_rules('jobTitle', 'Job Title', 'trim|required');
        $this->form_validation->set_rules('opening', 'Opening', 'trim|required');
        $this->form_validation->set_rules('experience', 'Experience', 'trim|required');
        $this->form_validation->set_rules('level', 'Level', 'trim|required');
        $this->form_validation->set_rules('modecheck', 'Interview Mode', 'trim|required');
        $this->form_validation->set_rules('jobLocation', 'Job Location', 'trim|required');
        $this->form_validation->set_rules('jobDesc', 'Job Description', 'trim|required');
        $this->form_validation->set_rules('category', 'Category', 'trim|required');
        $this->form_validation->set_rules('education', 'Education', 'trim|required');
        $this->form_validation->set_rules('jobExpire', 'Job Expire', 'trim|required');
//        $this->form_validation->set_rules('compDetail', 'Company Detail', 'trim|required');
        $rid = $jobData['rid'];
        //echo $rid;
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            //print_r($data['errors']);die;
            $data['getJobs'] = $this->Jobpostadmin_Model->jobupdate_detail_fetch($rid);
            $data['getJobloc'] = $this->Jobpostadmin_Model->jobupdate_location_fetch($rid);
            $data['jobExps'] = $this->Jobpostadmin_Model->jobupdate_detailexp_fetch($rid);
            //$data['recruiterExps'] = $this->Jobpostadmin_Model->jobupdate_recruiterexp_fetch($role);
            $data['industryLists'] = $this->Common_Model->industry_lists();
            $data['channels'] = $this->Common_Model->channel_lists();
            $data['langs'] = $this->Common_Model->language_lists();
            
            $this->load->view('administrator/updatejob',$data);
        } else {

            $data1 = ["jobtitle"=>$jobData['jobTitle'],"company_id"=>$jobData['jobLocation'], "opening"=> $jobData['opening'], "experience"=> $jobData['experience'], "category" => $jobData['category'],
                        "subcategory" => $jobData['subcategory'],
                        "language" => $jobData['lang'],
                        "other_language" => $jobData['otherlanguage'],
                        "jobDesc" => $jobData['jobDesc'],
                        "jobPitch" => $jobData['jobPitch'],
                        "skills" => $jobData['skill'],
                        "education" => $jobData['education'],
                        "jobexpire" => $jobData['jobExpire'],
                        "certification" => $jobData['certification'],
                        "mode" => $jobData['modecheck'],
                        "modeurl" => $jobData['modeurl']??'',
                        ];
            $jobInserted = $this->Jobpostadmin_Model->job_update($data1, $jobData['rid']);

            /*$addLatLong = $this->getLatLong($jobData['jobLocation']);
            $dataLocation = [
                        "jobLocation" => $jobData['jobLocation'],
                        "latitude" => $addLatLong['latitude'],
                        "longitude" => $addLatLong['longitude'],
                    ];*/
            //$this->Jobpostadmin_Model->jobLocation_update($dataLocation, $jobData['rid']);

            if(isset($jobData['expRange'])) {
                $jobExpRanges[] = $jobData['expRange'];
                $jobExpBasicSalarys[] = $jobData['expBasicSalary'];
                $count = count($jobData['expRange']);
                if($count > 0) {
                    for($i=0;$i<$count;$i++) {
                        $data2[$i] = ["grade_id" => $jobData['expRange'][$i],"jobpost_id"=>$jobData["rid"], "basicsalary" => $jobData['expBasicSalary'][$i]];
                    }
                    $this->Jobpostadmin_Model->job_basicsalary_delete($jobData['rid']);
                    $expsalInserted = $this->Jobpostadmin_Model->basic_salary_insert($data2);
                    //$expsalInserted = $this->Jobpostadmin_Model->basic_salary_update($data1, $jobData['rid']);
                }
            }

            if($jobInserted) {
                 $this->session->set_tempdata('inserted','Job Post Successfully Updated',5);
                 $dataId = $jobData['rid'];
                 $role_id= $jobData['role'];
                 redirect("administrator/jobpost/");
            } else {
                $this->session->set_flashdata('postError','Job Post not able to Updated',5);
                $dataId = $jobData['rid'];
                redirect("administrator/jobpost/updateJobpost?id=base64_encode($dataId)&role=base64_encode($role_id)");
            }
        }
    }

    public function manageJobView() {
        $userSession = $this->session->userdata('userSession');
        $getJobs = $this->Jobpost_Model->job_fetch($userSession['id']);
        foreach($getJobs as $getJob) {
            $getappliedCount = $this->Jobpost_Model->jobappliedCount_fetch($getJob['id']);
            $data['jobFetch'][] = [
                                    "id"=>$getJob['id'],
                                    "jobtitle"=>$getJob['jobtitle'],
                                    "jobLocation"=>$getJob['jobLocation'],
                                    "jobDesc"=>$getJob['jobDesc'],
                                    "companydetail"=>$getJob['companydetail'],
                                    "qualification"=>$getJob['qualification'],
                                    "skills"=>$getJob['skills'],
                                    "salary"=>$getJob['salary'],
                                    "totalApply"=>$getappliedCount[0]['appliedCount'],
                                    ];
        }

        $data['jobApplicationFetch'] = $this->Jobpost_Model->jobApplication_fetch($userSession['id']);
        $data['activejobFetch'] = $this->Jobpost_Model->activeJob_fetch($userSession['id']);
        $this->load->view('recruiter/manage_jobs', $data);
    }

    public function jobpostView() {
        $jobId = $_GET['type'];
        $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobId);
        $data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($data['getJobs'][0]['company_id']);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($data['getJobs'][0]['company_id']);
        $this->load->view('recruiter/updatejob', $data);
    }

    public function getLatLong($address){
        if(!empty($address)){
            //Formatted address
            $formattedAddr = str_replace(' ','+',$address);
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk'); 
            $output = json_decode($geocodeFromAddr);
            //Get latitude and longitute from json data
            $data['latitude']  = $output->results[0]->geometry->location->lat; 
            $data['longitude'] = $output->results[0]->geometry->location->lng;
            //Return latitude and longitude of the given address
            if(!empty($data)) {
                return $data;
            }else{
                return false;
            }
        }else{
            return false;   
        }
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
          return ($miles * 1.609344);
        } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }



        public function getScreenDatabyStatus(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');

        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);

        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title"> Status </span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $subcategoryy = $subcategory[0]['subcategory'];
            } else {
                $subcategoryy = '';
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        
             if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategoryy.'</td>
              <td style="width:70px;">'.$jobList['uid'].'</td>
              <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;">'.$jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>      
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
              <td style="width:70px;"> '. $screenStatus .' </td>
           </tr>';
           $x1++;
             }
          }
        $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }



    public function getScreenDatabyStatusweek(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);

        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Status</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory =  $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }
              
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
             
            
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
              <td style="width:70px;">'.$jobList['uid'].'</td>
              <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;">'.$jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
              <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
              <td style="width:70px;"> '. $screenStatus .' </td>
              
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }


        public function getScreenDatabyStatusmonth(){
        $status = $this->input->post('status');
        $date =  date('Y-m-d');
        //echo $date;die;
        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Status</span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory = $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);

             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
            <td style="width:70px;">'.$jobList['uid'].'</td>
            <td style="width:70px;">'.$jobList['name'].'</td>
            <td style="width:70px;">'.$jobList['email'].'</td>
              <td style="width:70px;">'.$jobList['phone'].'</td>
            <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
        <td style="width:70px;">'.$jobList['allowance'].'</td>
              
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
        <td style="width:70px;"> '. $screenStatus .' </td>
              
           </tr>';
           $x1++;
             }
       
          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

      public function getScreenDatabyStatusyear() {
        $status = $this->input->post('status');
        $date =  date('Y-m-d');

        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);

        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
     
      <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title"> Status </span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
              $category = $this->Jobpost_Model->categorybyid($jobList['category']);

              $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);

              if($subcategory) {
                  $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
                  
                  $expmonth = $jobList['exp_month'];
                  $expyear = $jobList['exp_year'];

                  if($expyear == 0 && $expmonth == 0) {
                      $expfilter = "1";
                  }else if($expyear == 0 && $expmonth < 6) {
                      $expfilter = "2";
                  } else if($expyear < 1 && $expmonth >= 6) {
                      $expfilter = "3";
                  } else if($expyear < 2 && $expyear >= 1) {
                      $expfilter = "4";
                  } else if($expyear < 3 && $expyear >= 2) {
                      $expfilter = "5";
                  }else if($expyear < 4 && $expyear >= 3) {
                      $expfilter = "6";
                  }else if($expyear < 5 && $expyear >= 4) {
                      $expfilter = "7";
                  }else if($expyear < 6 && $expyear >= 5) {
                      $expfilter = "8";
                  }else if($expyear < 7 && $expyear >= 6) {
                      $expfilter = "9";    
                  } else if($expyear >= 7) { 
                      $expfilter = "10";
                  }

                  if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

                  $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
                 
                 if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
                    $output.=' <tr>
                    <td style="width:40px;">'. $jobList['cname'].'</td>
                    <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
                    <td style="width:100px;">'. $jobList['id'].'</td>
                    <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                    <td style="width:70px;">'.$jobList['jobtitle'].'</td>
                    <td style="width:70px;">'.$category[0]['category'].'</td>
                    <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
                    <td style="width:70px;">'.$jobList['uid'].'</td>
                    <td style="width:70px;">'.$jobList['name'].'</td>
                    <td style="width:70px;">'.$jobList['email'].'</td>
                    <td style="width:70px;">'.$jobList['phone'].'</td>
                    <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
                    <td style="width:70px;">'.$jobList['allowance'].'</td>
                    <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
                    <td style="width:70px;"> '. $screenStatus .' </td>
                    
                    </tr>';
                    $x1++;
                 }
              }
          }
          
          $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }   
    echo $output;
    }


    public function getScreenDatabyStatusDate(){
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        
        $data = $this->Jobpostadmin_Model->getScreenDatabyStatus($status,$date);
        
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Email</span>
             </div>
          </th>
      
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>
          
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title"> Status </span>
             </div>
          </th>
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            if($subcategory) {
                $getsubcategory = $subcategory[0]['subcategory'];
            } else {
                $getsubcategory = "";
            }

              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
        $expmonth = $jobList['exp_month'];
        $expyear = $jobList['exp_year'];

        if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

        $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
             
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date &&  date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date ){
                $output.=' <tr>
              <td style="width:40px;">'. $jobList['cname'].'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$getsubcategory.'</td>
        <td style="width:70px;">'.$jobList['uid'].'</td>
        <td style="width:70px;">'.$jobList['name'].'</td>
        <td style="width:70px;">'.$jobList['email'].'</td>
    <td style="width:70px;">'.$jobList['phone'].'</td>
        <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
        <td style="width:70px;">'.$jobList['allowance'].'</td>
        <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
        <td style="width:70px;"> '. $screenStatus .' </td>
              
           </tr>';
           $x1++;
             }
          } 
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        } 
    echo $output;
    }
}
?>