<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Admin extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Admin_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->helper('cookie');
        
    }
    
    public function index() {
        $this->load->view('administrator/login');
    }

    public function logincheck() {
        $adminData = $this->input->post();
        //var_dump($adminData);die;
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/login', $data);
        } else {
            $password = $adminData['password'];
            $data = ["username" => $adminData['username']];
            $adminCheck = $this->Admin_Model->admin_login($data);
            //var_dump($adminCheck);die;
            if($adminCheck) {
                if($adminCheck[0]['status'] == 1) {
                    $getPass = $adminCheck[0]['password'];
                    $vPass = $this->encryption->decrypt($getPass);
                    
                    if($password == $vPass) {
                        $getData = $this->fetchAdminSingleData($adminCheck[0]['id']);
                        $this->session->set_userdata('adminSession', $getData);
                        
                        if(isset($adminData['remember_me'])){
                            $this->input->set_cookie('uemail', $adminData['username'], 86500);
                            $this->input->set_cookie('upassword', $vPass, 86500);
                        }
                        else{
                            delete_cookie('uemail');
                            delete_cookie('upassword'); 
                        }
                        redirect("administrator/dashboard");
                    } else {
                        $this->session->set_tempdata('loginerror','Password is incorrect',5);
                        $this->load->view('administrator/login');
                    }
                } else{
                    $this->session->set_tempdata('loginerror','Your Account is not approved',5);
                    $this->load->view('administrator/login');
                }
            } else{
                $this->session->set_tempdata('loginerror','Email id is incorrect',5);
                $this->load->view('administrator/login');
            }
        }
    }

    public function signup() {
        $this->load->view('administrator/signup'); 
    }

    public function signupInsert() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[admin.username]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[admin.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['userData'] = $userData;
            $this->load->view('administrator/signup',$data);
        } else {
            $hash = $this->encryption->encrypt($userData['password']);
            $data1 = ["name"=>$userData['name'], "username"=>$userData['username'], "email"=> $userData['email'], "password"=> $hash, "status" => 1];

            $adminInserted = $this->Admin_Model->admin_signup($data1);

            if($adminInserted) {
                $email = $userData['email'];
                $name = $userData['name'];
                $this->load->library('email');
                $this->email->from("jobyoda123@gmail.com", "JobYoda");
                $this->email->to($email);
                $this->email->subject('JobYoda Admin');
                $msg = "Dear " . $name;
                $msg .= ". Your Jobyoda Admin is created. Please Login with your username and password. ";
                $msg .= ". Username ". $userData['username'];
                $msg .= ". and Password ". $userData['password'];
                $this->email->message($msg);
                $this->email->send();

                $this->session->set_tempdata('signupSuccess','Account Created',5);
                redirect("administrator/admin/adminLists");
            } else {
                redirect("administrator/admin/signup");
            }
        }
    }

    public function forgot() {
        $this->load->view('administrator/forgot-password'); 
    }
    
    public function passwordchange() {
        $this->load->view('administrator/passwordchange'); 
    }
    
    public function resetPassword() {
        $adminData = $this->input->post();
        $encrypt_id = $adminData['id'];
        //$dec_id = $this->encryption->decrypt($encrypt_id);
        $data = ["id" => $encrypt_id];
        
        $adminTokenCheck = $this->Admin_Model->email_match($data);
        //echo $this->db->last_query();die;
        if($adminTokenCheck) {
            $newPass = $adminData['newpassword'];
            $verifyCode = $adminData['verifycode'];
           // echo $verifyCode; die;
            $forgotPassCheck = $this->Admin_Model->forgotPass_check($adminTokenCheck[0]['id']);
            //echo $this->db->last_query();die;
            if($forgotPassCheck[0]['verifyCode'] == $verifyCode)  {
                $hash = $this->encryption->encrypt($newPass);
                $data = ["password" => $hash];
                $this->Admin_Model->password_update($data, $adminTokenCheck[0]['id']);
                $vCode = ["verifyCode"=> " "];
                $this->Admin_Model->forgotPass_update($vCode, $adminTokenCheck[0]['id']);
                redirect("administrator/admin/");
            } else{
                $data1['forgoterror'] = "Verification code incorrect";
                $this->load->view('administrator/passwordchange', $data1);    
            }

        } else{
            $data1['forgoterror'] = "Please fill in the fields";
            $this->load->view('administrator/passwordchange', $data1);
        }
    }
    
    public function forgotPassword(){
        $adminData = $this->input->post();
       // print_r($adminData);die;
        if(isset($adminData['email'])) {
            $data = ["email" => $adminData['email']];
            $adminTokenCheck = $this->Admin_Model->email_match($data);
            if($adminTokenCheck) {
                $email = $adminTokenCheck[0]['email'];
                $name = $adminTokenCheck[0]['name'];
                $this->load->library('email');
                $this->email->from("jobyoda123@gmail.com", "JobYoda");
                $this->email->to($email);
                $this->email->subject('JobYoda Admin- Forgot Password Link');
                $token = $this->getToken();
                $msg = "Dear " . $name;
                $msg .= ". Change your password by clicking on this link : ";
                //$hash = $this->encryption->encrypt($userTokenCheck[0]['id']);
                $msg .= base_url() ."administrator/admin/passwordchange/".$adminTokenCheck[0]['id'];
                $msg .= ". Your verification code is ". $token;
                $this->email->message($msg);
                
                if($this->email->send()) {
                    $forgotPassCheck = $this->Admin_Model->forgotPass_check($adminTokenCheck[0]['id']);
                    
                    if($forgotPassCheck) {
                        $vCode = ["verifyCode"=>$token];
                        $this->Admin_Model->forgotPass_update($vCode, $adminTokenCheck[0]['id']);
                    } else {
                        $vCode = ["admin_id"=>$adminTokenCheck[0]['id'], "verifyCode"=>$token];
                        $this->Admin_Model->forgotPass_insert($vCode);
                    }
                    //$data1['forgotsuccess'] = "";
                    $this->session->set_tempdata('forgotsuccess','A verification link and code is sent to your registered email id. Please check your email id to change your password.',1);
                    $this->load->view('administrator/forgot-password');
                }
            } else{
                $data1['forgoterror'] = "Bad Request";
                $this->load->view('administrator/login', $data1);
            }
        } else{
            redirect('administrator/admin/index');
        }
    }

    public function verifysignupemail() {
        $userData = $this->input->post();
        $verifyCheck = $this->Recruit_Model->verification_check($userData['ids']);
        
        if($verifyCheck) {
            if($verifyCheck[0]['verifyCode'] == $userData['codeverify']) {
                redirect("recruiter/recruiter/loginverify");
            } else {
                $data1['verifyerror'] = "Verification code is incorrect";
                $data1["userId"] = $userData['ids'];
                $this->load->view('recruiter/newverification', $data1);
            }
        } else{
            $data1['verifyerror'] = "Check your Email for change password";
            $data1["userId"] = $userData['ids'];
            $this->load->view('recruiter/newverification', $data1);
        }
    }

    public function loginverify(){
        $data1['verifysuccess'] = "Successfully rRegister. Now Admin will approve your account within 24 hours!";
        $this->load->view('recruiter/login', $data1);
    }

    public function adminLists() {
        $data['adminLists'] = $this->Admin_Model->admin_all();
        $this->load->view('administrator/adminLists', $data);
    }

    public function reset() {
        $this->load->view('administrator/reset-password');
    }

    public function changePassword() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('newPass', 'New Password', 'trim|required');
        $this->form_validation->set_rules('confirmPass', 'Confirm Password', 'trim|required|matches[newPass]');
        
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/reset-password', $data);
        } else {
            $adminSession = $this->session->userdata('adminSession');
            $data = ["email" => $adminSession['email']];
            $adminCheck = $this->Admin_Model->admin_login($data);
            
            if($adminCheck) {
                $newPass = $userData['newPass'];
                $hash = $this->encryption->encrypt($newPass);
                $data = ["password" => $hash];
                $this->Admin_Model->password_update($data, $adminCheck[0]['id']);
                $this->session->set_tempdata('success','Password changed Successfully',5);
                $this->load->view('administrator/reset-password');
            } else{
                $this->load->view('administrator/reset-password');
            }
        }
    }

    public function fetchAdminSingleData($id) {
        $adminData = $this->Admin_Model->admin_single($id);
        $data = ["id" => $adminData[0]['id'], "name" => $adminData[0]['fname'], "email" => $adminData[0]['email']];
        return $data;
    }
    
    public function adminUpdate() {
        $adminId = $_GET['id'];
        $data['getAdmin'] = $this->Admin_Model->admin_detail_fetch($adminId);
        $this->load->view('administrator/updateadmin', $data);
    }
    
    public function updateAdmin() {
        $adminData = $this->input->post();
        //print_r($userData);die;
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]');
        $rid = $adminData['rid'];
        
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            //print_r($data['errors']);die;
            
            $data['getAdmin'] = $this->Admin_Model->admin_detail_fetch($rid);
            $this->load->view('administrator/updateadmin',$data);
        } else {
            $hash = $this->encryption->encrypt($userData['password']);
            $data1 = ["name"=>$adminData['name'], "username"=> $adminData['username'], "email"=> $adminData['email'], "password"=> $hash];
            $adminInserted = $this->Admin_Model->admin_update($data1, $rid);
            //echo $this->db->last_query();die;
            if($adminInserted) {
                
                redirect("administrator/admin/adminUpdate/?id=$rid");
            } else {
                redirect("administrator/admin/adminUpdate/?id=$rid");
            }
        }
    }
    
    function getToken(){
        $string = "";
        $chars = "0123456789";
        for($i=0;$i<4;$i++)
        $string.=substr($chars,rand(0,strlen($chars)),1);
        return $string;
    }

}
?>