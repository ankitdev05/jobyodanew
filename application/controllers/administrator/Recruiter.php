<?php
ob_start();
ini_set('display_errors', 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Recruiter extends CI_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Jobpostadmin_Model');
        $this->load->model('Recruiteradmin_Model');
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('Subscription_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('pdf');
        if($this->session->userdata('adminSession')) {
            
        } else {
            redirect("administrator/admin/index");
        }
    }
    
    public function getsite() {
        $cname = $this->input->post('cname');
        //echo $cname;die;
        if ($cname != '') {
          
                $site_list = $this->Jobpostadmin_Model->company_sites($cname);
                //echo $this->db->last_query();die;
                if ($site_list) {
                    echo "<option value=''>Select Site</option>";
                    foreach ($site_list as $site_lists) {
                        echo "<option value='" . $site_lists['id'] . "'>" . $site_lists['cname'] . "</option>";
                    }
                } else {
                    echo "<option value=''>Select Site</option>";
                }
            
        } else {
            echo "<option value=''>Select Site</option>";
        }
    }

    public function adadvertise() {
      if(!empty($_GET['id'])){
        $id = $_GET['id'];
        $data['advertiseLists'] = $this->Recruiteradmin_Model->advertise_single($id);

      }
      $data['company'] = $this->Jobpostadmin_Model->company();
      //echo $this->db->last_query();die;
      $this->load->view('administrator/adadvertise',$data);
    }

    public function editadvertise() {
      if(!empty($_GET['id'])){
        $id = $_GET['id'];
        $data['advertiseLists'] = $this->Recruiteradmin_Model->advertise_single($id);
      }
      $data['company'] = $this->Jobpostadmin_Model->company();
      $data['company_site'] = $this->Jobpostadmin_Model->company_site();
      $this->load->view('administrator/editadvertise',$data);
    }

    public function recruiterListing() {

        $data["Lists"] = $this->Recruiteradmin_Model->recruiter_lists(); 
        
        if(!empty($data["Lists"])){
            
            $data['subscriptions'] = $this->Subscription_Model->list();

            $this->load->view('administrator/recruiterLists', $data);
        
        } else {
            $this->session->set_flashdata('invoice_error','Recruiter Not available');
            redirect("administrator/recruiter/recruiterListing");
        }
    }

    public function pending_recruiters() {
        $data["Lists"] = $this->Recruiteradmin_Model->pending_recruiters(); 
        /*echo "hi";die;*/
        /*if(!empty($data["Lists"])){*/
            $this->load->view('administrator/pending_recruiters', $data);
        /*}*/
        /*else{
            $this->session->set_flashdata('invoice_error','Invoice Not available');
            redirect("administrator/recruiter/pending_recruiters");
        }*/
    }

    public function invoice() {
        $data["Lists"] = $this->Recruiteradmin_Model->invoice_lists(); 
        //echo $this->db->last_query();die;
        $this->load->view('administrator/invoice1', $data);
        
    }

    public function referralreport() {
        $data["Lists"] = $this->Recruiteradmin_Model->referral_lists(); 
        //echo $this->db->last_query();die;
        $this->load->view('administrator/referralreport', $data);
        
    }

    public function referralbyDate(){
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $data = $this->Recruiteradmin_Model->referral_lists();
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">SNo</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Jobseeker Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Jobseeker Email</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Jobseeker Phone</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Registration Date</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Referral Code</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {

             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if(date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date && date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date){
                $invoice_date = date("Y-m-d",strtotime($jobList['created_at']));
                $output.=' <tr>
              <td style="width:40px;">'. $x1.'</td>
              <td style="width:100px;">'. $jobList['name'].'</td>
              <td style="width:100px;">'. $jobList['email'].'</td>
              <td style="width:100px;">'. $jobList['country_code'].$jobList['phone'].'</td>
              <td style="width:70px;">'.$invoice_date.'</td>
              <td style="width:100px;">'. $jobList['referral_used'].'</td>
              
           </tr>';
           $x1++;
             }

          }
          
    $output.= '</tbody>';
        }else{
            $output='No Data Found';
        }
        
    echo $output;
    }

    public function bonusamountInsert() {
        $appId = $this->input->post('appId');
        $compid = $this->input->post('rId');
        $bonus_amount = $this->input->post('bonus_amount');
    
        $data = ["special_price" => $bonus_amount];
        $this->Recruiteradmin_Model->price_update($data,$appId);
        //echo $this->db->last_query();die;
        redirect("administrator/recruiter/hiredListing?id=$compid");
       
    }
    
    public function hiredListing(){
        $rid = base64_decode($this->input->get('id'));
        $hiredLists = $this->Recruiteradmin_Model->hiredcandidates_lists($rid); 
        //echo $this->db->last_query();die;
        //echo "<pre>";
        //print_r($hiredLists);die;
        $x=1;
        if($hiredLists){
        
            foreach ($hiredLists as $hiredlist) {
                $expmonth = $hiredlist['exp_month'];
                $expyear = $hiredlist['exp_year'];
                if($expyear == 0 && $expmonth < 6) {
                        $expfilter = "1";
                        
                    } else if($expyear < 1 && $expmonth >= 6) {
                        $expfilter = "2";
                        
                    } else if($expyear < 2 && $expyear >= 1) {
                        $expfilter = "3";
                        
                    } else if($expyear < 3 && $expyear >= 2) {
                        $expfilter = "4";
                        
                    } else if($expyear >= 3) { 
                        $expfilter = "5";
                        
                    }
                $site_name = $this->Recruiteradmin_Model->recruiter_name($hiredlist['company_id']);
                $hiredCost = $this->Recruiteradmin_Model->fetchcost($hiredlist['recruiter_id']);
                $salaryy = $this->Jobpostadmin_Model->getsalaryexp($hiredlist['id'],$expfilter);
                 
                //echo $this->db->last_query();
                //print_r($spclcategory);
                //die;
                $SubCategory= $this->Jobpostadmin_Model->subcategorybyid($hiredlist['subcategory']);
                //print_r($SubCategory);
                $spclcategory = $this->Recruiteradmin_Model->fetchspecial($rid,$SubCategory[0]['subcategory']);
                $total_allow = $salaryy[0]['basicsalary']+$hiredlist['allowance'];
                if(!empty($spclcategory[0]['price'])){
                    $spcl = $spclcategory[0]['price'];
                }else{
                    $spcl = '';
                }
                if(!empty($hiredCost[0]['cost'])){
                    $hiredCost[0]['cost']=$hiredCost[0]['cost'];
                }else{
                    $hiredCost[0]['cost']='';
                }
               	if(!empty($hiredCost[0]['percentage'])){
                	$hiredCost[0]['percentage'] = $hiredCost[0]['percentage'];
                }else{
                	$hiredCost[0]['percentage']=1;
                }
                if($spcl){
                $Special= "Yes";
                $costing = $spcl;
                
                }else{
                    $Special = "No";
                    if($total_allow<=50000){
                        $costing = $hiredCost[0]['cost'];
                    }else{
                        $costing = $total_allow/$hiredCost[0]['percentage'];
                    }
                    
                }
                
                if($spcl){
                    $costing= $spcl;
                }else{
                    $costing=$costing;
                }

                  $hired_data[$x]=['jobtitle'=>$hiredlist['jobtitle'], 'site_name'=>$site_name[0]['cname'], 'name'=>$hiredlist['name'], 'salary'=>$salaryy[0]['basicsalary'], 'allowance'=>$hiredlist['allowance'], 'subcategory'=>$SubCategory[0]['subcategory'], 'cname'=>$hiredlist['cname'], 'updated_at'=>$hiredlist['updated_at'], 'special'=>$Special, 'costing'=>$costing, 'rid'=>$rid, 'fname'=>$hiredlist['fname'], 'lname'=>$hiredlist['lname'], 'app_id'=>$hiredlist['app_id'], 'spcl'=>$spcl];
                $x++; 
            }
           
               
               
           
       }    
           else{
            $hired_data=[];
            }
            $data['hired_data'] = $hired_data;
            $this->load->view('administrator/hiredList',$data);
                        
    }


    public function fetchhired(){
        $rid = $this->input->post('rid');
        $hiredLists = $this->Recruiteradmin_Model->hiredcandidates_lists($rid); 
        //echo $this->db->last_query();die;
        $x=1;
        if($hiredLists){
        $output ='<div class="modal-header">
                <h5 class="modal-title" id="create_invoice">Create Invoice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div><div class="modal-body" >
                
          <style>
          .table th, .table td{
            font-size:10px;
          }
          </style>     
               
            <div class="table-responsive"> 
                   <table class="table">
                       <thead>
                           <tr>
                               <th>SNo.</th>
                               <th>Job Title</th>
                               <th>Site Name</th>
                               <th>Candidate Name</th>
                               <th>Basic Salary</th>
                               <th>Total Guaranteed Allowance</th>
                               <th>Job SubCategory</th>
                               <th>Job Posted By</th>
                               <th>Candidate Hired By</th>
                               <th>Hired Date</th>
                               <th>Special Profile</th>
                               <th>Final Cost Per Hire</th>
                           </tr>
                       </thead>
                       <tbody >';
                       
                        foreach ($hiredLists as $hiredlist) {
                            $expmonth = $hiredlist['exp_month'];
                            $expyear = $hiredlist['exp_year'];
                            if($expyear == 0 && $expmonth < 6) {
                                    $expfilter = "1";
                                    
                                } else if($expyear < 1 && $expmonth >= 6) {
                                    $expfilter = "2";
                                    
                                } else if($expyear < 2 && $expyear >= 1) {
                                    $expfilter = "3";
                                    
                                } else if($expyear < 3 && $expyear >= 2) {
                                    $expfilter = "4";
                                    
                                } else if($expyear >= 3) { 
                                    $expfilter = "5";
                                    
                                }
                            $site_name = $this->Recruiteradmin_Model->recruiter_name($hiredlist['company_id']);
                            $hiredCost = $this->Recruiteradmin_Model->fetchcost($hiredlist['recruiter_id']);
                            $salaryy = $this->Jobpostadmin_Model->getsalaryexp($hiredlist['id'],$expfilter); 
                            
                            $SubCategory= $this->Jobpostadmin_Model->subcategorybyid($hiredlist['subcategory']);
                            $spclcategory = $this->Recruiteradmin_Model->fetchspecial($rid,$SubCategory[0]['subcategory']); 
                            $total_allow = $salaryy[0]['basicsalary']+$hiredlist['allowance'];
                            if(!empty($spclcategory[0]['price'])){
			                    $spcl = $spclcategory[0]['price'];
			                }else{
			                    $spcl = '';
			                }
			                if(!empty($hiredCost[0]['percentage'])){
			                	$hiredCost[0]['percentage'] = $hiredCost[0]['percentage'];
			                }else{
			                	$hiredCost[0]['percentage']=1;
			                }
                            if($spcl){
				                $Special= "Yes";
				                $costing = $spcl;
				                }else{
                                $Special = "No";
                                if($total_allow<=50000){
                                  if(!empty($hiredCost[0]['cost'])){
                                    $costing = $hiredCost[0]['cost'];
                                  }else{
                                    $costing = '';
                                  }
                                }else{
                                    $costing = $total_allow/$hiredCost[0]['percentage'];
                                }
                                
                            }
                            if($spcl){
                                $costing=$spcl;
                            }else{
                                $costing=$costing;
                            }

                              $output.= '<tr>
                                       <td>'.$x.'</td>
                                       <td>'.$hiredlist['jobtitle'].'</td>
                                       <td>'.$site_name[0]['cname'].'</td>
                                       <td>'.$hiredlist['name'].'</td>
                                       <td>'.$salaryy[0]['basicsalary'].'</td>
                                       <td>'.$hiredlist['allowance'].'</td>
                                       <td>'.$SubCategory[0]['subcategory'].'</td>
                                       <td>'.$hiredlist['fname'].' '.$hiredlist['lname'].'</td>
                                       <td>'.$hiredlist['fname'].' '.$hiredlist['lname'].'</td>
                                       <td>'.$hiredlist['updated_at'].'</td>
                                       <td>'.$Special??"".'</td>
                                       <td>'.$costing.'</td>
                                       
                                    </tr>';
                            $x++; 
                        }
                       
                           
                           
                       $output.= '</tbody>
                   </table>
               </div><div class="form-group">
                  
                  <input type="hidden" name="recruiter_id" id="recruiter_id" value="">
               </div></div><div class="modal-footer">
                <a href="#" onclick="generate_invoice()" class="btn btn-secondary" id="createiLink">Generate Invoice</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>';
                   }    
                       else{
                        $output= '<div class="modal-body" >No Candidate Hired</div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>';
                        }
                        echo $output;
    }

    public function fetchhired1(){
        $rid = $this->input->post('rid');
        $amount = $this->input->post('amount');
        $hiredLists = $this->Recruiteradmin_Model->hiredcandidates_lists($rid); 
        //print_r($hiredLists);die;
        $invoice_number = date("Ymd") .$rid.rand(10,100);
        $output = '<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">
                            <img src="http://mobuloustech.com/jobyoda/recruiterfiles/images/Final.png" width="100%">
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-1">Invoice #'.$invoice_number.'</p>
                            <p class="text-muted">Date: '.date("d M, Y").'</p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-4">Company Information</p>
                            <p class="mb-1">'.$hiredLists[0]['cname'].'</p>
                            
                            <p class="mb-1">'.$hiredLists[0]['address'].'</p>
                            
                        </div>

                    </div>

                    <div class="row p-2">
                        <div class="col-md-12">
                        <div class="srcljobyd">
                            <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">S No</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Job Title</th>
                                        
                                        <th class="border-0 text-uppercase small font-weight-bold">Company Name</th>
                                        
                                        <th class="border-0 text-uppercase small font-weight-bold">Candidate Name</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Date</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>';
                                $x2=1;
                                $total_amount=0;
                                foreach ($hiredLists as $hiredList) {
                                    $hiredDate = date("d-m-Y", strtotime($hiredList['created_at']));
                                    $output.= '<tr>
                                        <td>'.$x2.'</td>
                                        <td>'.$hiredList['jobtitle'].'</td>
                                        <td>'.$hiredList['cname'].'</td>
                                        <td>'.$hiredList['name'].'</td>
                                        <td>'.$hiredDate.'</td>
                                        <td>'.$amount.'</td>
                                        
                                    </tr>';
                                $x2++;
                                $total_amount+= $amount;
                                }
                                    
                               $output.= '</tbody>
                            </table>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white">
                        <div class="text-right">
                            <div class="mb-2">Grand Total</div>
                            <div class="h2 font-weight-light">'.$total_amount.'</div>
                        </div>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

</div>';
echo $output;
//return $output;        
}


    public function pdfdetails()
     {

        $rid = $this->input->get('rid');
        //$amount = $this->input->get('amount');
        $hiredLists = $this->Recruiteradmin_Model->hiredcandidates_lists($rid);
        //echo $this->db->last_query();die; 
        //print_r($hiredLists);die;
        $invoice_number = "JobYoDa".rand(10000,99999);
        $output = '<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    

                    <table>
                    <tbody>
                    <tr>
                        <td style="width:300px;">
                         <img src="https://jobyoda.com/recruiterfiles/images/jobyoda.png" width="200px;">
                        </td>
                        <td style="width:700px;">
                        <div style="float:right;">
                        <p class="font-weight-bold mb-1" style="margin:0 0 3px 0;">Invoice #'.$invoice_number.'</p>
                            <p class="text-muted" style="margin:0;">Invoice Generated Date: '.date("d M, Y").'</p>
                        </div>
                        </td>
                    </tr>
                    </tbody>
                    </table>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6" style="width:300px; margin-bottom:30px; margin-top:30px;">
       <p class="font-weight-bold mb-4" style="font-weight: bold !important; margin:0;">
       Company Information</p>
                            <p class="mb-1" style="margin:0;">'.$hiredLists[0]['cname'].'</p>
                            
                            <p class="mb-1" style="margin:0;">'.$hiredLists[0]['address'].'</p>
                            
                        </div>

                    </div>

                    <div class="row p-5">
                        <div class="col-md-12" style="width:100%;">
                            <table class="table" style="border:1px solid #ddd;">
                                <thead>
                                    <tr>
                                        
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px;">Job Title</th>
                                        
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px;">Site Name</th>
                                        
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px;">Candidate Name</th>
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px;">Basic Salary</th>
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px;">Total Guaranteed Allowance</th>
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px; width:35px;">Job SubCategory</th>
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px;">Job Posted By</th>
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px;">Candidate Hired By</th>
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px;">Hired Date</th>
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px; width:20px;">Special Profile</th>
                                        <th class="border-0 text-uppercase small font-weight-bold" style="font-size:12px; width:25px;">Final Cost 
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>'; 
                                $x2=1;
                                $total_amount=0;
                                foreach ($hiredLists as $hiredList) {
                                    $expmonth = $hiredList['exp_month'];
                                    $expyear = $hiredList['exp_year'];
                                    if($expyear == 0 && $expmonth < 6) {
                                            $expfilter = "1";
                                            
                                        } else if($expyear < 1 && $expmonth >= 6) {
                                            $expfilter = "2";
                                            
                                        } else if($expyear < 2 && $expyear >= 1) {
                                            $expfilter = "3";
                                            
                                        } else if($expyear < 3 && $expyear >= 2) {
                                            $expfilter = "4";
                                            
                                        } else if($expyear >= 3) { 
                                            $expfilter = "5";
                                            
                                        }
                                    $site_name = $this->Recruiteradmin_Model->recruiter_name($hiredList['company_id']);
                                    $hiredCost = $this->Recruiteradmin_Model->fetchcost($hiredList['recruiter_id']);
                                    $salaryy = $this->Jobpostadmin_Model->getsalaryexp($hiredList['id'],$expfilter); 
                                    
                                    $SubCategory= $this->Jobpostadmin_Model->subcategorybyid($hiredList['subcategory']);
                                    $spclcategory = $this->Recruiteradmin_Model->fetchspecial($rid,$SubCategory[0]['subcategory']); 
                                    $total_allow = $salaryy[0]['basicsalary']+$hiredList['allowance'];
                                    if(!empty($spclcategory[0]['price'])){
                                        $spcl = $spclcategory[0]['price'];
                                    }else{
                                        $spcl = '';
                                    }
                                    if(!empty($hiredCost[0]['percentage'])){
                                      $hiredCost[0]['percentage'] = $hiredCost[0]['percentage'];
                                    }else{
                                      $hiredCost[0]['percentage']=1;
                                    }
                                    if($spcl){
                                      $Special= "Yes";
                                      $costing = $spcl;
                                      }else{
                                        $Special = "No";
                                        if($total_allow<=50000){
                                            $costing = $hiredCost[0]['cost'];
                                        }else{
                                            $costing = $total_allow/$hiredCost[0]['percentage'];
                                        }
                                        
                                    }
                                    

                                    $data = ["payment" => 1];
                                     $this->Recruiteradmin_Model->payment_update($data,$hiredList['uid'],$hiredList['id']);
                                    $hiredDate = date("d-m-Y", strtotime($hiredList['created_at']));
                                    $output.= '<tr>
                                        <td style="width:50px; font-size:12px;">'.$hiredList['jobtitle'].'</td>
                                        <td style="width:50px; font-size:12px;">'.$site_name[0]['cname'].'</td>
                                        <td style="width:50px; font-size:12px;">'.$hiredList['name'].'</td>
                                        <td style="width:50px; font-size:12px;">'.$salaryy[0]['basicsalary'].'</td>
                                        <td style="width:50px; font-size:12px;">'.$hiredList['allowance'].'</td>
                                        <td style="width:50px; font-size:12px;">'.$SubCategory[0]['subcategory'].'</td>
                                        <td style="width:80px; font-size:12px;">'.$hiredList['fname'].' '.$hiredList['lname'].'</td>
                                        <td style="width:80px; font-size:12px;">'.$hiredList['fname'].' '.$hiredList['lname'].'</td>
                                        <td style="width:50px; font-size:12px;">'.$hiredList['updated_at'].'</td>
                                        <td style="width:50px; font-size:12px;">'.$Special.'</td>
                                        <td style="width:50px; font-size:12px;">'.$costing.'</td>
                                        
                                    </tr>';
                                $x2++;
                                $total_amount+= $costing;
                                }
                                    
                               $output.= '</tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        <div class="py-3 px-5 text-right" style="margin-top:30px; background:#000; padding:5px;">
                            <div class="mb-2" style="text-align:right; font-size:30px;color:#fff;">Grand Total: '.$total_amount.' </div>
                            
                        </div>

                        <div class="py-3 px-5 text-right">
                            
                        </div>

                        <div class="py-3 px-5 text-right">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

</div>';
//echo $output;die;
       $file_name = "./recruiterupload/".md5(rand()) . '.pdf';
       $this->pdf->loadHtml($output);
       $this->pdf->setPaper('A4', 'landscape');
       $this->pdf->render();
       
       // echo $this->pdf->stream();die;
       $file = $this->pdf->output();
       file_put_contents($file_name,  $file );
       $this->load->library('email');
            $this->email->from("jobyoda123@gmail.com", "JobYoDa");
            $this->email->to($hiredLists[0]['email'],'testmobulous123@gmail.com');
            $this->email->subject('JobYoDa - Invoice');
            $msg = "Dear ".$hiredLists[0]['cname'] ;
            $msg .= ", Please check the invoice as below: ";
            $this->email->message($msg);
            $this->email->attach($file_name);
            if($this->email->send()){
                $this->pdf->stream($file_name, array("Attachment"=>0));
                $data = ["invoice_id" => $invoice_number, "total_amount" => $total_amount, "company_id" => $rid , "invoice" => $file_name , "no_of_candidates" => count($hiredLists),"type"=>1];
                $this->Recruiteradmin_Model->invoice_insert($data);
            }else{
                echo $hiredLists[0]['email'];
            }
            
            //$this->email->attach($file_name);
       //$this->pdf->stream("test.pdf", array("Attachment"=>1));
      
     }
    
    public function recruiterview() {
        $rid = base64_decode($_GET['id']);
        $data["phonecodes"] = $this->Common_Model->phonecode_lists(); 
        $data['companie'] = $this->Recruiteradmin_Model->company_single($rid);
        $data['companieDetail'] = $this->Recruiteradmin_Model->company_details_single($rid);
        $data['password'] = $this->encryption->decrypt($data['companie'][0]['password']); 
        $topPicks = $this->Recruit_Model->company_topicks_single($rid);
        if($topPicks) { $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];$x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($rid);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($rid);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }
        
        $leaves = $this->Recruit_Model->company_leaves_single($rid);
        //echo $this->db->last_query();die;
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leave1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leave2'] = $leave1;
        } else{
            $data['leave2'] = [];
        }
        
        $workshifts = $this->Recruit_Model->company_workshifts_single($rid);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        $this->load->view('administrator/recruiterview', $data);
    }
    
    public function companysiteview() {
        $view = base64_decode($_GET['id']);
        $data['companyDetails'] = $this->Recruiteradmin_Model->companysite_details_single($view);
        //echo $this->db->last_query();die;
        $data['companySites'] = $this->Recruiteradmin_Model->companyaddress($data['companyDetails'][0]['parent_id']);
        //echo $this->db->last_query();die;
		$topPicks = $this->Recruit_Model->company_topicks_single($view);
        if($topPicks) { $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];$x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($view);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($view);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }

        $leaves = $this->Recruit_Model->company_leaves_single($view);
        //echo $this->db->last_query();die;
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leave1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leave2'] = $leave1;
        } else{
            $data['leave2'] = [];
        }
        //print_r($data['$leave2'] );die;
        $workshifts = $this->Recruit_Model->company_workshifts_single($view);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        $this->load->view('administrator/companysiteview', $data);
    }
    
    public function companysitelisting() {
        $id = base64_decode($_GET['id']);
        $data["Lists"] = $this->Recruiteradmin_Model->companysite_lists($id); 
        //$data["siteDetails"] = $this->Recruiteradmin_Model->companysite_details($id); 
        $this->load->view('administrator/companysitelisting', $data);
    }

    public function subrecruiterlist() {
        $id = base64_decode($_GET['id']);
        $data["Lists"] = $this->Recruiteradmin_Model->subrecruiter_lists($id); 
        //$data["siteDetails"] = $this->Recruiteradmin_Model->companysite_details($id); 
        $this->load->view('administrator/subrecruiterlist', $data);
    }
    
    
    
    

    public function addRecruiter() {
        $data["phonecodes"] = $this->Common_Model->phonecode_lists(); 
        $this->load->view('administrator/registerrecruiter', $data);
    }

    public function recruiterInsert() {
        $userData = $this->input->post();
        //print_r($userData);die;
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|callback_alpha_dash_space|max_length[25]');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|callback_alpha_dash_space|max_length[25]');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[recruiter.email]');
        $this->form_validation->set_rules('phonecode', 'Phone Code', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_valid_password');
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            
            $data["phonecodes"] =  $this->Common_Model->phonecode_lists();
            $data['userData'] = $userData;
            $this->load->view('administrator/registerrecruiter',$data);
        } else {
            $hash = $this->encryption->encrypt($userData['password']);
            $data1 = ["fname"=>$userData['fname'], "lname"=> $userData['lname'], "cname"=> $userData['cname'], "email"=> $userData['email'], "password"=> $hash, "status"=>1, "active"=>1];

            $recruiterInserted = $this->Recruiteradmin_Model->recruiter_stepone_insert($data1);
            $addLatLong = $this->getLatLong($userData['address']);

            $data2 = ["recruiter_id"=>$recruiterInserted, "phonecode"=>$userData['phonecode'], "phone"=> $userData['phone'], "address"=> $userData['address'], "latitude" => $addLatLong['latitude'], "longitude" => $addLatLong['longitude']];

            $this->Recruiteradmin_Model->recruiter_steptwo_insert($data2);

            if($recruiterInserted) {
                $email = $userData['email'];
                $name = $userData['fname'];
                $this->load->library('email');
                
                $datas['full_name'] = ucfirst($name).' '.ucfirst($userData['lname']);
                $datas['email'] = $email;
                $datas['password'] = $userData['password'];
                $msg = $this->load->view('administrator/email',$datas,TRUE);
                $config=array(
                'charset'=>'utf-8',
                'wordwrap'=> TRUE,
                'mailtype' => 'html'
                );

                $this->email->initialize($config);
                $this->email->from("help@jobyoda.com", "JobYoDA");
                $this->email->to($email);
                $this->email->subject('JobYoDA Recruiter - Account Created');
                /*$msg = "Dear " . $name.' '.$userData['lname'];
                $msg .= "You have Successfully Registered. You can login into your account once Admin approves your account within 24 hours!";*/

                $this->email->message($msg);
                if($this->email->send()){

                }else{
                    echo $this->email->print_debugger();
                }

                redirect("administrator/recruiter/recruiterListing");
            } else {
                redirect("administrator/Recruiter/addRecruiter");
            }
        }
    }

    public function advertiseInsert() {
      //print_r($_FILES);die;
        $userData = $this->input->post();
        //print_r($userData);die;
        $this->form_validation->set_rules('location', 'Location', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('company', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('site', 'Site Name', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            
            $data['company'] = $this->Jobpostadmin_Model->company();
            $data['userData'] = $userData;
            $this->load->view('administrator/adadvertise',$data);
        } else {
            $config = array(
                'upload_path' => "./recruiterupload/",
                'allowed_types' => "jpg|png|jpeg",
                //'max_size' => '500000',
                'max_width' => '450',
                'min_width' => '400',
                'max_height' => '250',
                'min_height' => '200',
            );
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('banner')) {

                $data = array('upload_data' => $this->upload->data());
                $picPath = base_url() ."recruiterupload/". $data['upload_data']['file_name'];
                
            } else {
                $data['errors']['profilePic'] = strip_tags($this->upload->display_errors());
                $data['company'] = $this->Jobpostadmin_Model->company();
                $data['userData'] = $userData;
                $data['recruitData'][0] = $userData;

                $this->load->view('administrator/adadvertise',$data);
                exit;
            }


            $addLatLong = $this->getLatLong($userData['location']);
            $data1 = ["banner"=>$picPath, "location"=> $userData['location'],'city'=>$addLatLong['city'], 'latitude'=>$addLatLong['latitude'] , 'longitude'=>$addLatLong['longitude'], "company_id"=> $userData['company'],'site_id'=>$userData['site'], "start_date"=> $userData['start_date'], "end_date"=> $userData['end_date'], "description"=>$userData['desc']];

            $recruiterInserted = $this->Recruiteradmin_Model->advertise_insert($data1);
            
            if($recruiterInserted) {
                redirect("administrator/recruiter/advertiselist");
            } else {
                redirect("administrator/Recruiter/adadvertise");
            }
        }
    }

    public function advertiseUpdate() {
      //print_r($_FILES);die;
        $userData = $this->input->post();
        //print_r($userData);die;
        $this->form_validation->set_rules('location', 'Location', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('company', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            
            $id = $userData['id'];
            $data['advertiseLists'][0] = $this->Recruiteradmin_Model->advertise_single($id);
            $data['company'] = $this->Jobpostadmin_Model->company();
            $data['company_site'] = $this->Jobpostadmin_Model->company_site();
            $this->load->view('administrator/editadvertise',$data);
        } else {
            

          if (empty($_FILES['banner']['name'])) {

            $config = array(
                'upload_path' => "./recruiterupload/",
                'allowed_types' => "jpg|png|jpeg",
                //'max_size' => '500000',
                'max_width' => '450',
                'min_width' => '400',
                'max_height' => '250',
                'min_height' => '200',
            );
            $this->load->library('upload', $config);
            $addLatLong = $this->getLatLong($userData['location']);
            
            if($this->upload->do_upload('banner')) {
                $data = array('upload_data' => $this->upload->data());
                $picPath = base_url() ."recruiterupload/". $data['upload_data']['file_name'];
                $data1 = ["banner"=>$picPath, "location"=> $userData['location'],'city'=>$addLatLong['city'], 'latitude'=>$addLatLong['latitude'] , 'longitude'=>$addLatLong['longitude'], "company_id"=> $userData['company'], "start_date"=> $userData['start_date'], "end_date"=> $userData['end_date'], "description"=>$userData['desc']];
                
            } else {
                $data['errors']['profilePic'] = strip_tags($this->upload->display_errors());
                $id = $userData['id'];
                $data['advertiseLists'][0] = $this->Recruiteradmin_Model->advertise_single($id);
                $data['company'] = $this->Jobpostadmin_Model->company();
                $data['company_site'] = $this->Jobpostadmin_Model->company_site();
                $this->load->view('administrator/editadvertise',$data);
                exit;

            }

          } else {
              $data1 = ["location"=> $userData['location'],'city'=>$addLatLong['city'], 'latitude'=>$addLatLong['latitude'] , 'longitude'=>$addLatLong['longitude'], "company_id"=> $userData['company'], "start_date"=> $userData['start_date'], "end_date"=> $userData['end_date'], "description"=>$userData['desc']];
          }
            

            $recruiterInserted = $this->Recruiteradmin_Model->advertise_update($data1, $userData['id']);
            //echo $this->db->last_query();die;
            if($recruiterInserted) {
                redirect("administrator/recruiter/advertiselist");
            } else {
                redirect("administrator/Recruiter/editadvertise");
            }
        }
    }
    public function RecruiterAdded() {
        $data['success'] = "Account Successfully Created";
        $data["phonecodes"] = $this->Common_Model->phonecode_lists(); 
        $this->load->view('administrator/registerrecruiter', $data);
    }

    public function updateRecruiter() {
        $rid = base64_decode($_GET['id']);
        $data["phonecodes"] = $this->Common_Model->phonecode_lists(); 
        $data['companie'] = $this->Recruiteradmin_Model->company_single($rid);
        $data['companieDetail'] = $this->Recruiteradmin_Model->company_details_single($rid);
        $data['password'] = $this->encryption->decrypt($data['companie'][0]['password']);
        $this->load->view('administrator/updateRecruiter', $data);
    }

    public function companyupdate() {
        $userData = $this->input->post();
        //print_r($userData);die;
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phonecode', 'Phone Code', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $rid = $userData['rid'];
        
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            //print_r($data['errors']);die;
            $data["phonecodes"] = $this->Common_Model->phonecode_lists(); 
            $data['companie'] = $this->Recruiteradmin_Model->company_single($rid);
            $data['companieDetail'] = $this->Recruiteradmin_Model->company_details_single($rid);
            
            $this->load->view('administrator/updateRecruiter',$data);
        } else {
            $data1 = ["fname"=>$userData['fname'], "lname"=> $userData['lname'], "cname"=> $userData['cname'], "email"=> $userData['email']];
            $recruiterInserted = $this->Recruiteradmin_Model->recruiter_stepone_update($data1, $rid);
            //echo $this->db->last_query();die;
            if($recruiterInserted) {
                $addLatLong = $this->getLatLong($userData['address']);
                $data2 = ["phone"=> $userData['phone'], "address"=> $userData['address'], "latitude" => $addLatLong['latitude'], "longitude" => $addLatLong['longitude']];

                $this->Recruiteradmin_Model->recruiter_steptwo_update($data2, $rid);
                if($userData['status']=='1'){
                    redirect("administrator/recruiter/recruiterListing");
                }else{
                    redirect("administrator/recruiter/pending_recruiters");
                }
                
            } else {
                redirect("administrator/recruiter/updateRecruiter");
            }
        }
    }

    public function RecruiterUpdated() {
        $rid = $this->uri->segment(4);
        $data['success'] = "Account Successfully Updated";
        $data["phonecodes"] = $this->Common_Model->phonecode_lists(); 
        $data['companie'] = $this->Recruiteradmin_Model->company_single($rid);
        $data['companieDetail'] = $this->Recruiteradmin_Model->company_details_single($rid);
        $this->load->view('administrator/updateRecruiter', $data);
    }
    
    public function deleteRecruiter() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->delete_recruiter($rid);
        //echo $this->db->last_query();die;
        $this->Recruiteradmin_Model->delete_recruiterDetails($rid);
        $this->Recruiteradmin_Model->delete_recruiterAddExp($rid);
        $this->Recruiteradmin_Model->delete_recruiterallowances($rid);
        $this->Recruiteradmin_Model->delete_recruitermedical($rid);
        $this->Recruiteradmin_Model->delete_recruitertoppicks($rid);
        $this->Recruiteradmin_Model->delete_recruiterworkshift($rid);
        $this->Recruiteradmin_Model->delete_jobs($rid);
        $this->Recruiteradmin_Model->delete_advertise($rid);
        $this->Recruiteradmin_Model->delete_comapny_rating($rid);
        $this->Recruiteradmin_Model->delete_notifications($rid);
        $this->Recruiteradmin_Model->delete_reviews($rid);
        redirect("administrator/recruiter/recruiterListing");
    }

    public function deletepRecruiter() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->delete_recruiter($rid);
        //echo $this->db->last_query();die;
        $this->Recruiteradmin_Model->delete_recruiterDetails($rid);
        $this->Recruiteradmin_Model->delete_recruiterAddExp($rid);
        $this->Recruiteradmin_Model->delete_recruiterallowances($rid);
        $this->Recruiteradmin_Model->delete_recruitermedical($rid);
        $this->Recruiteradmin_Model->delete_recruitertoppicks($rid);
        $this->Recruiteradmin_Model->delete_recruiterworkshift($rid);
        $this->Recruiteradmin_Model->delete_jobs($rid);
        $this->Recruiteradmin_Model->delete_advertise($rid);
        $this->Recruiteradmin_Model->delete_comapny_rating($rid);
        $this->Recruiteradmin_Model->delete_notifications($rid);
        $this->Recruiteradmin_Model->delete_reviews($rid);
        redirect("administrator/recruiter/pending_recruiters");
    }
    
    public function recruiterBlock() {
        $rid = $_GET['id'];
        $data1 = ["active"=>0];
        $jobseekerInserted = $this->Recruiteradmin_Model->recruiterStatus($data1, $rid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/recruiterListing");
        } else {
            redirect("administrator/recruiter/recruiterListing");
        }
        
    }
    
    public function recruiterActive() {
        $rid = $_GET['id'];
        $userdetail = $this->Recruiteradmin_Model->recruiter_name($rid);
        $data1 = ["active"=>1];
        $jobseekerInserted = $this->Recruiteradmin_Model->recruiterStatus($data1, $rid);
        $email = $userdetail[0]['email'];
        $name = $userdetail[0]['fname'];
        $this->load->library('email');
        
        $datas['full_name'] = ucfirst($name).' '.ucfirst($userdetail[0]['lname']);
        $datas['email'] = $email;
        
        $msg = $this->load->view('administrator/email2',$datas,TRUE);
        $config=array(
        'charset'=>'utf-8',
        'wordwrap'=> TRUE,
        'mailtype' => 'html'
        );

        $this->email->initialize($config);
        $this->email->from("help@jobyoda.com", "JobYoDA");
        $this->email->to($email);
        $this->email->subject('JobYoDA Recruiter - Account Created');
        /*$msg = "Dear " . $name.' '.$userData['lname'];
        $msg .= "You have Successfully Registered. You can login into your account once Admin approves your account within 24 hours!";*/

        $this->email->message($msg);
        if($this->email->send()){

        }else{
            echo $this->email->print_debugger();
        }
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/recruiterListing");
        } else {
            redirect("administrator/recruiter/recruiterListing");
        }
        
    }

    public function adActive() {
        $rid = $_GET['id'];
        $data1 = ["status"=>1];
        $jobseekerInserted = $this->Recruiteradmin_Model->adStatus($data1, $rid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/advertiselist");
        } else {
            redirect("administrator/recruiter/advertiselist");
        }
        
    }

    public function adBlock() {
        $rid = $_GET['id'];
        $data1 = ["status"=>2];
        $jobseekerInserted = $this->Recruiteradmin_Model->adStatus($data1, $rid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/advertiselist");
        } else {
            redirect("administrator/recruiter/advertiselist");
        }
        
    }

    public function salaryApprove() {
        $rid = $_GET['id'];
        $data1 = ["salary_status"=>1];
        $jobseekerInserted = $this->Recruiteradmin_Model->recruiterStatus($data1, $rid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/recruiterListing");
        } else {
            redirect("administrator/recruiter/recruiterListing");
        }
        
    }

    public function salaryBlock() {
        $rid = $_GET['id'];
        $data1 = ["salary_status"=>0];
        $jobseekerInserted = $this->Recruiteradmin_Model->recruiterStatus($data1, $rid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/recruiterListing");
        } else {
            redirect("administrator/recruiter/recruiterListing");
        }
        
    }
    
    public function recruiterApprove() {
        $rid = $_GET['id'];
        $data1 = ["status"=>1];
        $jobseekerInserted = $this->Recruiteradmin_Model->recruiterStatus($data1, $rid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/pending_recruiters");
        } else {
            redirect("administrator/recruiter/pending_recruiters");
        }
        
    }

    public function getLatLong($address){
        if(!empty($address)){
            //Formatted address
            $formattedAddr = str_replace(' ','+',$address);
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk'); 
            $output = json_decode($geocodeFromAddr);
            
            //Get latitude and longitute from json data
            $data['latitude']  = $output->results[0]->geometry->location->lat; 
            $data['longitude'] = $output->results[0]->geometry->location->lng;
            for($j=0;$j<count($output->results[0]->address_components);$j++){
                $city = $output->results[0]->address_components[$j]->types;
                if($city[0]=="locality"){
                     $data['city'] = $output->results[0]->address_components[$j]->long_name;    
                }
            }
            //Return latitude and longitude of the given address
            if(!empty($data)) {
                return $data;
            }else{
                return false;
            }
        }else{
            return false;   
        }
    }

    public function addJobTitle() {
        $data['jobtitleLists'] = $this->Recruiteradmin_Model->jobtitle_listing();
        $this->load->view('administrator/addjobtitle',$data);
    }

    public function addprice() {
        $data['jobtitleLists'] = $this->Recruiteradmin_Model->adprice_listing();
        $this->load->view('administrator/addprice',$data);
    }

    public function addvideoprice() {
        $data['jobtitleLists'] = $this->Recruiteradmin_Model->advideoprice_listing();
        $this->load->view('administrator/addvideoprice',$data);
    }

     public function addreferral() {
        $data['referLists'] = $this->Recruiteradmin_Model->refercode_listing();
        $this->load->view('administrator/addreferral',$data);
    }

    public function addstaticcontent() {
        $id = base64_decode($this->input->get('id'));
        if(!empty($id)){
            $data['contentSingle'] = $this->Recruiteradmin_Model->staticcontent_single($id);
        }
        $data['contentLists'] = $this->Recruiteradmin_Model->staticcontent_listing();
        $this->load->view('administrator/addstaticcontent',$data);
    }

    public function addfaq() {
        $id = $this->input->get('id');
        if(!empty($id)){
            $data['contentSingle'] = $this->Recruiteradmin_Model->faq_single($id);
        }
        $data['contentLists'] = $this->Recruiteradmin_Model->faq_listing();
        $this->load->view('administrator/addfaq',$data);
    }

    public function addtestimonials() {
        $this->load->view('administrator/testimonials');
    }

    public function testimonialInsert() {
        $userData = $this->input->post();

        //$data['contentLists'] = $this->Recruiteradmin_Model->staticcontent_listing();
        $this->form_validation->set_rules('client_name', 'Client Name', 'trim|required');
        //$this->form_validation->set_rules('image', 'Image', 'trim|required');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            //print_r($data['errors']);die;
            $this->load->view('administrator/testimonials',$data);
        } else {
            if(!empty($userData['content_id'])){

                $config = array(
                'upload_path' => "./recruiterupload/",
                'allowed_types' => "jpg|png|jpeg",
                );
                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image'))
                {
                    
                    $data = array('upload_data' => $this->upload->data());
                    $this->resizeImage($data['upload_data']['file_name']);
                    $picPath = base_url() ."recruiterupload/". $data['upload_data']['file_name'];
                    
                } else {
                    $picPath = "";
                }
                $data = ["title" => $userData['title'], "content" => $userData['content']];
                $this->Recruiteradmin_Model->staticontent_update($data,$userData['content_id']);
            }else{
                 $config = array(
                'upload_path' => "./recruiterupload/",
                'allowed_types' => "jpg|png|jpeg",
                );
                $this->load->library('upload', $config);
                
                if($this->upload->do_upload('image'))
                {
                    
                    $data = array('upload_data' => $this->upload->data());
                    $picPath = base_url() ."recruiterupload/". $data['upload_data']['file_name'];
                    
                } else {
                    $picPath = "";
                }
                $data = ["name" => $userData['client_name'], "pic" => $picPath , "title"=> $userData['title'] , "description"=> $userData['description'] ];
                $this->Recruiteradmin_Model->testimonial_insert($data);
            }
            redirect("administrator/recruiter/addtestimonials");
        }
    }

    public function staticcontentlist() {
        $id = $this->input->get('id');
        if(!empty($id)){
            $data['contentSingle'] = $this->Recruiteradmin_Model->staticcontent_single($id);
        }
        $data['contentLists'] = $this->Recruiteradmin_Model->staticcontent_listing();
        $this->load->view('administrator/content_list',$data);
    }

    public function faqlist() {
        $id = $this->input->get('id');
        if(!empty($id)){
            $data['contentSingle'] = $this->Recruiteradmin_Model->faq_single($id);
        }
        $data['contentLists'] = $this->Recruiteradmin_Model->faq_listing();
        $this->load->view('administrator/faqlist',$data);
    }

    public function advertiselist() {
        /*$id = $this->input->get('id');
        if(!empty($id)){
            $data['contentSingle'] = $this->Recruiteradmin_Model->faq_single($id);
        }*/
        $data['advertise_price']= $this->Recruiteradmin_Model->advertise_price();
        
        if(!empty($_GET['status']) && $_GET['status']=='pending'){
          $data['advertiseLists'] = $this->Recruiteradmin_Model->pending_advertise_list();
        }else{
          $data['advertiseLists'] = $this->Recruiteradmin_Model->advertise_listing();
        }
        
        $this->load->view('administrator/advertiselist',$data);
    }

    public function generatepdf()
     {

        $jid = $this->input->get('job_id');
        $amount = $this->input->get('amount');
        $hiredLists = $this->Recruiteradmin_Model->advertise_listingbyid($jid); 
        //print_r($hiredLists);die;
        $invoice_number = "JobYoDA".rand(10000,99999);
        $output = '<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    

                    <table>
                    <tbody>
                    <tr>
                        <td style="width:300px;">
                         <img src="https://jobyoda.com/recruiterfiles/images/jobyoda.png" width="200px;">
                        </td>
                        <td style="width:400px;">
                        <div style="float:right;">
                        <p class="font-weight-bold mb-1">Invoice #'.$invoice_number.'</p>
                            <p class="text-muted">Invoice Generated Date: '.date("d M, Y").'</p>
                        </div>
                        </td>
                    </tr>
                    </tbody>
                    </table>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6" style="width:300px; margin-bottom:30px; margin-top:30px;">
       <p class="font-weight-bold mb-4" style="font-weight: bold !important; margin:0;">
       Company Information</p>
                            <p class="mb-1" style="margin:0;">'.$hiredLists[0]['cname'].'</p>
                            
                            <p class="mb-1" style="margin:0;">'.$hiredLists[0]['address'].'</p>
                            
                        </div>

                    </div>

                    <div class="row p-5">
                        <div class="col-md-12" style="width:800px;">
                            <table class="table" style="border:1px solid #ddd;">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">S No</th>
                                        
                                        <th class="border-0 text-uppercase small font-weight-bold">Company Name</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Site Name</th>
                                        
                                        <th class="border-0 text-uppercase small font-weight-bold">Start Date</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">End Date</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>';
                                $x2=1;
                                $total_amount=0;
                                foreach ($hiredLists as $hiredList) {
                                    $sitename = $this->Jobpostadmin_Model->site_name($hiredList['site_id']);
                                    $startDate = date("d-m-Y", strtotime($hiredList['start_date']));
                                    $endDate = date("d-m-Y", strtotime($hiredList['end_date']));
                                    $diff = strtotime($hiredList['end_date'])- strtotime($hiredList['start_date']);
                                    $daydiff = floor($diff / (60 * 60 * 24));
                                    $output.= '<tr>
                                        <td style="width:50px;">'.$x2.'</td>
                                        <td style="width:140px;">'.$hiredList['cname'].'</td>
                                        <td style="width:140px;">'.$sitename[0]['cname'].'</td>
                                        
                                        <td style="width:140px;">'.$startDate.'</td>
                                        <td style="width:140px;">'.$endDate.'</td>
                                        <td style="width:140px;">'.$amount*$daydiff.'</td>
                                        
                                    </tr>';
                                $x2++;
                                $total_amount+= $amount*$daydiff;
                                }
                                    
                               $output.= '</tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        <div class="py-3 px-5 text-right" style="margin-top:30px; background:#000; padding:5px;">
                            <div class="mb-2" style="text-align:right; font-size:30px;color:#fff;">Grand Total</div>
                            <div class="h2 font-weight-light" style="text-align:right;color:#fff; margin:0;">'.$total_amount.'</div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            
                        </div>

                        <div class="py-3 px-5 text-right">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

</div>';
       $file_name = "./recruiterupload/".md5(rand()) . '.pdf';
       $this->pdf->loadHtml($output);
       $this->pdf->render();
       
       // echo $this->pdf->stream();die;
       $file = $this->pdf->output();
       file_put_contents($file_name,  $file );
       $this->load->library('email');
            $this->email->from("help@jobyoda.com", "JobYoDA");
            $this->email->to($hiredLists[0]['email']);
            $this->email->subject('JobYodA - Invoice');
            $msg = "Dear ".$hiredLists[0]['cname'] ;
            $msg .= ", Please check the invoice as below: ";
            $this->email->message($msg);
            $this->email->attach($file_name);
            if($this->email->send()){
                $this->pdf->stream($file_name, array("Attachment"=>0));
                $data = ["invoice_id" => $invoice_number, "total_amount" => $total_amount, "company_id" => $hiredLists[0]['company_id'] , "invoice" => $file_name , 'type'=>3];
                $this->Recruiteradmin_Model->invoice_insert($data);
            }else{
                echo $hiredLists[0]['email'];
            }
            
            //$this->email->attach($file_name);
       //$this->pdf->stream("test.pdf", array("Attachment"=>1));
      
     }

    public function jobtitleInsert() {
        $userData = $this->input->post();
        $data['jobtitleLists'] = $this->Recruiteradmin_Model->jobtitle_listing();
        $this->form_validation->set_rules('title', 'Job Title', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/addjobtitle',$data);
        } else {
            $data = ["jobtitle" => $userData['title']];
            $this->Recruiteradmin_Model->jobtitle_insert($data);
            redirect("administrator/recruiter/addJobTitle");
        }
    }

    public function adpriceInsert() {
        $userData = $this->input->post();
        $data['jobtitleLists'] = $this->Recruiteradmin_Model->adprice_listing();
        $this->form_validation->set_rules('price', 'Price', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/addprice',$data);
        } else {
            $data = ["price" => $userData['price']];
            $this->Recruiteradmin_Model->adprice_insert($data);
            redirect("administrator/recruiter/addprice");
        }
    }

    public function advideopriceInsert() {
        $userData = $this->input->post();
        $data['jobtitleLists'] = $this->Recruiteradmin_Model->adprice_listing();
        $this->form_validation->set_rules('price', 'Price', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/addvideoprice',$data);
        } else {
            $data = ["price" => $userData['price']];
            $this->Recruiteradmin_Model->advideoprice_insert($data);
            redirect("administrator/recruiter/addvideoprice");
        }
    }

    public function refercodeInsert() {
        $userData = $this->input->post();
        $data['referLists'] = $this->Recruiteradmin_Model->refercode_listing();
        $this->form_validation->set_rules('refer_code', 'Referral Code', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['referLists'] = $this->Recruiteradmin_Model->refercode_listing();
            $this->load->view('administrator/addreferral',$data);
        } else {
            $data = ["refer_code" => $userData['refer_code']];
            $this->Recruiteradmin_Model->refercode_insert($data);
            redirect("administrator/recruiter/addreferral");
        }
    }
    
    public function hirecost(){
      $id = base64_decode($this->input->get('id')); 
      $data["Lists"] = $this->Recruiteradmin_Model->recruitercost_single($id); 
      $data["special"] = $this->Recruiteradmin_Model->specialcost_single($id); 
      $data['company'] = $this->Jobpostadmin_Model->company();
      $data['subcategory'] = $this->Common_Model->subcategory_lists();
        $this->load->view('administrator/hirecost1',$data);
    }

    public function hirecostInsert() {
        $userData = $this->input->post();
        //print_r($userData);die;
        //$special_profile= $userData['special_profile'];
        //$special_profile = implode(',', $special_profile);
        $data['jobtitleLists'] = $this->Recruiteradmin_Model->jobtitle_listing();
       // $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('cost', 'Cost', 'trim|required');
        $this->form_validation->set_rules('effect_date', 'Effective Date', 'trim|required');
        $this->form_validation->set_rules('billing_date', 'Billing Date', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['company'] = $this->Jobpostadmin_Model->company();
            $this->load->view('administrator/hirecost',$data);
        } else {
            $data = ["company_id" => $userData['company_name'], "cost" => $userData['cost'],"percentage"=> $userData['percentage'], "effective_date" => $userData['effect_date'], "billing_date" => $userData['billing_date']];
            $cost_data = $this->Recruiteradmin_Model->recruitercost_single($userData['company_name']); 
            if(empty($cost_data)){
                $this->Recruiteradmin_Model->hirecost_insert($data);
                if(isset($userData['expRange'])) {
                    
                    $jobExpRanges[] = $userData['expRange'];
                    $jobExpBasicSalarys[] = $userData['expBasicSalary'];
                    $count = count($userData['expRange']);
                    if($count > 0) {
                        for($i=0;$i<$count;$i++) {

                            $data1[$i] = ["company_id"=>$userData['company_name'],"special_category"=>$userData['expRange'][$i], "price" => $userData['expBasicSalary'][$i]];
                        }
                        
                        $this->Recruiteradmin_Model->specialprofile_delete($userData['company_name']);
                        $expsalInserted = $this->Recruiteradmin_Model->special_profile_insert($data1);
                    }
                }
            }else{
                $this->Recruiteradmin_Model->hirecost_update($data,$userData['company_name']);
                if(isset($userData['expRange'])) {
                    
                    $jobExpRanges[] = $userData['expRange'];
                    $jobExpBasicSalarys[] = $userData['expBasicSalary'];
                    $count = count($userData['expRange']);
                    if($count > 0) {
                        for($i=0;$i<$count;$i++) {

                            $data1[$i] = ["company_id"=>$userData['company_name'],"special_category"=>$userData['expRange'][$i], "price" => $userData['expBasicSalary'][$i]];
                        }
                        
                        $this->Recruiteradmin_Model->specialprofile_delete($userData['company_name']);
                        $expsalInserted = $this->Recruiteradmin_Model->special_profile_insert($data1);
                    }
                }
            }
            
            redirect("administrator/Jobpost/hirecost");
        }
    }

    public function staticcontentInsert() {
        $userData = $this->input->post();

        $data['contentLists'] = $this->Recruiteradmin_Model->staticcontent_listing();
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('content', 'Content', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/addstaticcontent',$data);
        } else {
            if(!empty($userData['content_id'])){
                $data = ["title" => $userData['title'], "content" => $userData['content']];
                $this->Recruiteradmin_Model->staticontent_update($data,$userData['content_id']);
            }else{
                $data = ["title" => $userData['title'], "content" => $userData['content']];
                $this->Recruiteradmin_Model->staticontent_insert($data);
            }
            redirect("administrator/recruiter/staticcontentlist");
        }
    }

    public function faqInsert() {
        $userData = $this->input->post();

        $data['contentLists'] = $this->Recruiteradmin_Model->faq_listing();
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('content', 'Content', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/addfaq',$data);
        } else {
            if(!empty($userData['content_id'])){
                $data = ["title" => $userData['title'], "content" => $userData['content']];
                $this->Recruiteradmin_Model->faq_update($data,$userData['content_id']);
            }else{
                $data = ["title" => $userData['title'], "content" => $userData['content']];
                $this->Recruiteradmin_Model->faq_insert($data);
            }
            redirect("administrator/recruiter/faqlist");
        }
    }
    
    public function addIndustry() {
        $data['industryLists'] = $this->Recruiteradmin_Model->industry_listing();
        $this->load->view('administrator/addindustry',$data);
    }

    public function addPower() {
        $data['poweLists'] = $this->Recruiteradmin_Model->power_listing();
        $this->load->view('administrator/addpower',$data);
    }

    public function industryInsert() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('industry', 'Industry', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['industryLists'] = $this->Recruiteradmin_Model->industry_listing();
            $this->load->view('administrator/addindustry',$data);
        } else {
            $data = ["name" => $userData['industry']];
            $this->Recruiteradmin_Model->industry_insert($data);
            redirect("administrator/recruiter/addIndustry");
        }
    }

    public function powerInsert() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('power', 'SuperPower', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['poweLists'] = $this->Recruiteradmin_Model->power_listing();
            $this->load->view('administrator/addpower',$data);
        } else {
            $data = ["power" => $userData['power']];
            $this->Recruiteradmin_Model->power_insert($data);
            redirect("administrator/recruiter/addPower");
        }
    }

    public function companyListing() {
        $compListing = $this->Recruiteradmin_Model->company_lists();
        foreach($compListing as $compList) {
            $renames = $this->Recruiteradmin_Model->recruiter_name($compList['parent_id']);
            $companyReviews = $this->Jobpost_Model->fetch_companyRating($compList['id']);
            $glassdoor_rating = $this->Recruiteradmin_Model->rating_lists($compList['id']);
                //echo $this->db->last_query();
                //print_r($glassdoor_rating);
                if(!empty($companyReviews[0]['average'])){
                    $rating = $companyReviews[0]['average'];
                }else{
                    $rating="";
                }

                if(!empty($glassdoor_rating[0]['rating'])){
                    $grating = $glassdoor_rating[0]['rating'];
                }else{
                    $grating="";
                }
            foreach($renames as $rename) {
              $nameRe = $rename['cname'];
            }
            $data["CompLists"][] = ["rid"=>$compList['id'], "rating"=>$rating, "reName"=>$renames[0]['fname'].' '.$renames[0]['lname'], "email"=>$compList['email'], "address"=>$compList['address'], "cname"=> $compList['cname'], "grating"=>$grating];
        }
        $this->load->view('administrator/companyLists', $data);
    }


    public function get_glassdoor_rating(){
        $cid = $this->input->post('cid');
        $glassdoor_rating = $this->Recruiteradmin_Model->rating_lists($cid);
        if(!empty($glassdoor_rating[0]['rating'])){
            $output = $glassdoor_rating[0]['rating'];
        }else{
            $output='';
        }
        echo $output;
    }

    public function addRating() {
      $data ["cid"] = $_GET['id'];
      $this->load->view('administrator/addcompanyrating', $data);
    }

    public function insertRating() {
      $data = $this->input->post();
      $this->Recruiteradmin_Model->rating_insert($data);
      redirect("administrator/recruiter/companyListing");
    }


    public function deleteCompany() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->delete_recruiter($rid);
        $this->Recruiteradmin_Model->delete_recruiterDetails($rid);
        $this->Recruiteradmin_Model->delete_recruiterAddExp($rid);
        $this->Recruiteradmin_Model->delete_recruiterallowances($rid);
        $this->Recruiteradmin_Model->delete_recruitermedical($rid);
        $this->Recruiteradmin_Model->delete_recruitertoppicks($rid);
        $this->Recruiteradmin_Model->delete_recruiterworkshift($rid);
        redirect("administrator/recruiter/companyListing");
    }
    
    public function deletetitle() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletetitle($rid);
        redirect("administrator/recruiter/addJobTitle");
    }

     public function deleterefercode() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deleterefercode($rid);
        redirect("administrator/recruiter/addreferral");
    }

    public function deletecontent() {
        $rid = base64_decode($_GET['id']);
        $this->Recruiteradmin_Model->deletecontent($rid);
        redirect("administrator/recruiter/staticcontentlist");
    }

    public function deletefaq() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletefaq($rid);
        redirect("administrator/recruiter/faqlist");
    }

    public function deletead() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletead($rid);
        redirect("administrator/recruiter/advertiselist");
    }
    
    public function deleteindustry() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deleteindustry($rid);
        redirect("administrator/recruiter/addIndustry");
    }

    public function deletepower() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletepower($rid);
        redirect("administrator/recruiter/addPower");
    }

    public function storyListing() {
        $listings = $this->Recruiteradmin_Model->company_lists();
        /*echo "<pre>";
        print_r($listings);die; */
        //$data["Lists"] = $this->Recruiteradmin_Model->story_lists();
        $listget = array();

        if($listings) {
            foreach($listings as $listing) {
               $Listcount = $this->Recruiteradmin_Model->story_lists($listing['parent_id']);
               if(count($Listcount) >0){
                  $cc= 1;
               } else{
                  $cc=0;
               }
               $listget[] = ["cname"=> $listing['cname'], "email"=>$listing['email'], "id"=> $listing['id'], "haveList"=> $cc, "count_story" => count($Listcount)];
            }
        }
        $data["Lists"] = $listget;
        $this->load->view('administrator/storyLists', $data);
    }

    public function companystoryview() { 
        $companyid = base64_decode($this->input->get('id'));
        $getParent = $this->Recruiteradmin_Model->companysite_detailsget($companyid);

        $data["Lists"] = $this->Recruiteradmin_Model->story_lists($getParent[0]['parent_id']); 
        $data['companyId']=$companyid;
        if(!empty($data["Lists"])) {
            $this->load->view('administrator/storyView', $data);
        }else{
            $this->session->set_flashdata('story_error','Story Not available');
            redirect("administrator/recruiter/storyListing");
        }
        
    }

    public function storyBlock() {
      $eid = $_GET['compid'];
        $rid = base64_decode($_GET['id']);
        $compid = base64_decode($_GET['compid']);
        $data1 = ["status"=>'1'];
        $jobseekerInserted = $this->Recruiteradmin_Model->storyStatus($data1, $rid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/companystoryview?id=$eid");
        } else {
            redirect("administrator/recruiter/companystoryview?id=$eid");
        }
        
    }
    
    public function storyActive() {
       $eid = $_GET['compid'];
        $rid = base64_decode($_GET['id']);
        $compid = base64_decode($_GET['compid']);
        $data1 = ["status"=>'2'];
        $jobseekerInserted = $this->Recruiteradmin_Model->storyStatus($data1, $rid);
        
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/companystoryview?id=$eid");
        } else {
            redirect("administrator/recruiter/companystoryview?id=$eid");
        }
        
    }

    public function storycommentview() { 
        $storyid = $this->input->get('id');
        $data['storyId']=$storyid;
        $compid = $_GET['compid'];
        $data["Lists"] = $this->Recruiteradmin_Model->comment_lists($storyid); 
        if(!empty($data["Lists"])){
            $this->load->view('administrator/storycommentview', $data);
        }else{
            //echo $compid;die;
            $this->session->set_flashdata('comment_error','Comments Not available');
            redirect("administrator/recruiter/companystoryview?id=$compid");
        }
        
    }

    public function deleteComment() {
        $rid = $_GET['id'];
        $storyId = $_GET['storyId'];
        $this->Recruiteradmin_Model->delete_comment($rid);
        redirect("administrator/recruiter/storycommentview?id=$storyId");
    }

    public function addJobLevel() {
        $data['joblevelLists'] = $this->Recruiteradmin_Model->joblevel_listing();
        $this->load->view('administrator/addjoblevel',$data);
    }

    public function joblevelInsert() {
        $userData = $this->input->post();
        //print_r($userData);die;
        $data['joblevelLists'] = $this->Recruiteradmin_Model->joblevel_listing();
        $this->form_validation->set_rules('level', 'Job Level', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/addjoblevel',$data);
        } else {
            $data1 = ["level" => $userData['level']];
            $this->Recruiteradmin_Model->joblevel_insert($data1);
            //echo $this->db->last_query();die;
            redirect("administrator/recruiter/addJobLevel");
        }
    }

    public function deletejoblevel() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletejoblevel($rid);
        redirect("administrator/recruiter/addJobLevel");
    }

    public function addJobCategory() {
        $data['jobcategoryLists'] = $this->Recruiteradmin_Model->jobcategory_listing();
        $this->load->view('administrator/addJobCategory',$data);
    }

    public function addJobSubCategory() {
        $data['jobcategoryLists'] = $this->Recruiteradmin_Model->jobcategory_listing();
        $data['jobsubcategoryLists'] = $this->Recruiteradmin_Model->jobsubcategory_listing();
        $this->load->view('administrator/addJobSubCategory',$data);
    }

    public function addSkills() {
        $data['jobskillLists'] = $this->Recruiteradmin_Model->jobskills_listing();
        $this->load->view('administrator/addSkills',$data);
    }

    public function jobskillInsert() {
        $userData = $this->input->post();
        //print_r($userData);die;
        $data['jobskillLists'] = $this->Recruiteradmin_Model->jobskills_listing();
        $this->form_validation->set_rules('skill', 'Job Skill', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/addSkills',$data);
        } else {
            $data1 = ["skill" => $userData['skill']];
            $this->Recruiteradmin_Model->jobskill_insert($data1);
            //echo $this->db->last_query();die;
            redirect("administrator/recruiter/addSkills");
        }
    }

    public function jobcategoryInsert() {
        $userData = $this->input->post();
        //print_r($userData);die;
        $data['jobcategoryLists'] = $this->Recruiteradmin_Model->jobcategory_listing();
        $data['jobsubcategoryLists'] = $this->Recruiteradmin_Model->jobsubcategory_listing();
        $this->form_validation->set_rules('category', 'Job Category', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/addJobCategory',$data);
        } else {
            $data1 = ["category" => $userData['category']];
            $this->Recruiteradmin_Model->jobcategory_insert($data1);
            //echo $this->db->last_query();die;
            redirect("administrator/recruiter/addJobCategory");
        }
    }

    public function jobsubcategoryInsert() {
        $userData = $this->input->post();
        //print_r($userData);die;
        $data['jobcategoryLists'] = $this->Recruiteradmin_Model->jobcategory_listing();
        $this->form_validation->set_rules('category', 'Job Category', 'trim|required');
        $this->form_validation->set_rules('subcategory', 'Job SubCategory', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['jobcategoryLists'] = $this->Recruiteradmin_Model->jobcategory_listing();
            $data['jobsubcategoryLists'] = $this->Recruiteradmin_Model->jobsubcategory_listing();
            $this->load->view('administrator/addJobSubCategory',$data);
        } else {
            $data1 = ["category_id" => $userData['category'], "subcategory" => $userData['subcategory']];
            $this->Recruiteradmin_Model->jobsubcategory_insert($data1);
            //echo $this->db->last_query();die;
            redirect("administrator/recruiter/addJobSubCategory");
        }
    }

    public function deletejobcategory() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletejobcategory($rid);
        redirect("administrator/recruiter/addJobCategory");
    }

    public function deletejobskill() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletejobskill($rid);
        redirect("administrator/recruiter/addSkills");
    }

    public function deletejobsubcategory() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletejobsubcategory($rid);
        redirect("administrator/recruiter/addJobSubCategory");
    }

    function getToken(){
        $string = "";
        $chars = "0123456789";
        for($i=0;$i<4;$i++)
        $string.=substr($chars,rand(0,strlen($chars)),1);
        return $string;
    }

    public function valid_password($password = '')
    {
        $password = trim($password);

        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

        if (empty($password))
        {
            $this->form_validation->set_message('valid_password', 'Password is required.');

            return FALSE;
        }

        if (preg_match_all($regex_lowercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets,numeric and special characters.');

            return FALSE;
        }

        /*if (preg_match_all($regex_uppercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password must have at least one uppercase letter.');

            return FALSE;
        }*/

        if (preg_match_all($regex_number, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets,numeric and special characters.');

            return FALSE;
        }

        if (preg_match_all($regex_special, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets,numeric and special characters.');

            return FALSE;
        }

        /*if (strlen($password) < 5)
        {
            $this->form_validation->set_message('valid_password', 'Password must be at least 5 characters in length.');

            return FALSE;
        }

        if (strlen($password) > 32)
        {
            $this->form_validation->set_message('valid_password', 'Password cannot exceed 32 characters in length.');

            return FALSE;
        }*/

        return TRUE;
    }

    function alpha_dash_space($str_in = '')
    {
        $str_in = trim($str_in);
        if (empty($str_in))
        {
            $this->form_validation->set_message('alpha_dash_space', 'Name field is required.');

            return FALSE;
        }
        if (! preg_match("/^([a-z, ])+$/i", $str_in))
        {
            $this->form_validation->set_message('alpha_dash_space', 'The name field may only contain characters and spaces.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function recruitertransfer() {
        $data['jobcategoryLists'] = $this->Recruiteradmin_Model->recruiter_transferfetch();
        $this->load->view('administrator/recruitertransfer',$data);
    }

    public function recruitertransferaccepted() {
        $tid = $_GET['id'];
        $getTransfer = $this->Recruiteradmin_Model->recruiter_transferfetchsingle($tid);
        $data1= array();
        //var_dump($getTransfer);die;
        $config =[
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtpout.asia.secureserver.net',
                    'smtp_user' => 'help@jobyoda.com',
                    'smtp_pass' => 'Usa@1234567',
                    'smtp_port' => 465,
                    'smtp_crypto' => 'ssl',
                    'charset'=>'utf-8',
                    'mailtype' => 'html',
                    'crlf' => "\r\n",
                    'newline' => "\r\n"
                  ];
        $data1['reqdata'] = $getTransfer;

        if($getTransfer[0]['request_type'] == 1) {
            $getSub = $this->Recruit_Model->recruiter_single($getTransfer[0]['request_to']);
            $data1['subreqdata'] = ["email"=>$getSub[0]['email'], "name"=>$getSub[0]['fname'].' '.$getSub[0]['lname']];
        } else {
            $data1['subreqdata'] = ["email"=>$getTransfer[0]['email'], "name"=>$getTransfer[0]['fname'].' '.$getTransfer[0]['lname']];
        }

        $data1['password'] = $this->random_strings(10);

        $msg = $this->load->view('transferacceptemail',$data1,TRUE);  //mailing -1
        $msg1 = $this->load->view('transfersubacceptemail',$data1,TRUE);  //mailing -2

        $this->load->library('email');
        $subject = "JobYoDA Transfer of Ownership Complete";

        $this->email->initialize($config);
        $this->email->from("help@jobyoda.com", "JobYoDA");
        $this->email->to($getTransfer[0]['emailby']);
        $this->email->subject($subject);
        $this->email->message($msg);
        
        
        if($this->email->send()) {
            $subject = "JobYoDA Transfer of Ownership Approval";
            $this->email->initialize($config);
            $this->email->from("help@jobyoda.com", "JobYoDA");
            $this->email->to($data1['subreqdata']['email']);
            $this->email->subject($subject);
            $this->email->message($msg1);
            
            if($this->email->send()) {
                $this->load->library('encryption');

                if($getTransfer[0]['request_type'] == 1) {
                    
                    $dataUpdate = [
                        "email"=>$getSub[0]['email'], "fname"=>$getSub[0]['fname'], "lname"=>$getSub[0]['lname'], "password"=>$this->encryption->encrypt($data1['password'])
                    ];
                    $recruDetails = $this->Recruit_Model->company_details_single($getTransfer[0]['request_to']);
                    $dataMore = ["phone"=>$recruDetails[0]['phone']];

                } else {
                    $dataUpdate = [
                        "email"=>$getTransfer[0]['email'], "fname"=>$getTransfer[0]['fname'], "lname"=>$getTransfer[0]['lname'], "password"=>$this->encryption->encrypt($data1['password'])
                    ];
                    $dataMore = ["phone"=>$getTransfer[0]['phone']];
                }

                $this->Recruiteradmin_Model->recruiter_singleupdate($getTransfer[0]['request_by'], $dataUpdate);
                $this->Recruiteradmin_Model->recruiter_singlemoreupdate($getTransfer[0]['request_by'], $dataMore);


                $this->Recruiteradmin_Model->subrecruiter_delete($getTransfer[0]['request_to']);
                $this->Recruiteradmin_Model->subrecruiter_advertiseupdate($getTransfer[0]['request_to'], $getTransfer[0]['request_by']);
                $this->Recruiteradmin_Model->subrecruiter_permissiondelete($getTransfer[0]['request_to']);
                $this->Recruiteradmin_Model->request_delete($tid);

                $this->session->set_tempdata('item', 'Request accepted Successfully', 5);
                redirect("administrator/Recruiter/recruitertransfer");
            
            } else {
                $this->session->set_tempdata('postError', 'Mail not send', 2);
                redirect("administrator/Recruiter/recruitertransfer");
            }

        } else {
            $this->session->set_tempdata('postError', 'Mail not send', 2);
            redirect("administrator/Recruiter/recruitertransfer");
        }
    }

    public function random_strings($length_of_string) { 
      
        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
      
        // Shufle the $str_result and returns substring 
        // of specified length 
        return substr(str_shuffle($str_result),  
                           0, $length_of_string); 
    }

    public function testimonial_view() {
        $getDatas = $this->Recruit_Model->testimonial_all();
        foreach($getDatas as $getData) {
            $getSub = $this->Recruit_Model->recruiter_single($getData['company_id']);
            
            $data['recruitFetchs'][] = [
                "id"=>$getData['id'],
                "postedby"=>$getSub[0]['fname'].' '.$getSub[0]['lname'],
                "cname"=>$getSub[0]['cname'],
                "title"=>$getData['name'],
                "description"=>$getData['description'],
                "status"=>$getData['status']
            ];
        }
        $this->load->view('administrator/testimoniallists', $data);
    }

    public function testimonial_status() {
        
        $cid = $this->input->post('cid');

        $checkStatus = $this->Recruit_Model->testimonial_single($cid);

        if($checkStatus[0]['status'] == 0) {
            $data = ["status"=>1];
        } else {
            $data = ["status"=>0];
        }

        $this->Recruit_Model->testimonial_update($cid, $data);

        redirect("administrator/Recruiter/testimonial_view");
    }


    public function videoadvertise() {
        if(!empty($_GET['id'])) {
          $id = $_GET['id'];
          $data['advertiseLists'] = $this->Recruiteradmin_Model->advertise_single($id);
        }
        $data['advertiseLists'] = [];

        $this->load->view('administrator/adadvertisevideo',$data);
    }
    public function videoadvertiselist() {
        $getVideos = $this->Recruiteradmin_Model->videoadvertise_listing();

        foreach($getVideos as $getVideo) {
            $getRecruit = $this->Recruiteradmin_Model->companysite_detailsget($getVideo['recruiter_id']);
            if($getRecruit) {
              $videoArray[] = ["id"=>$getVideo['id'], "cname"=>$getRecruit[0]['cname'], "thumbnail"=>$getVideo['thumbnail'], "video"=>$getVideo['video'],"title"=>$getVideo['title'],"desc"=>$getVideo['description'],"start_date"=>$getVideo['start_date'],"end_date"=>$getVideo['end_date'],"status"=>$getVideo['status']];
            } else {
              $videoArray[] = ["id"=>$getVideo['id'], "cname"=>"Admin", "thumbnail"=>$getVideo['thumbnail'], "video"=>$getVideo['video'],"title"=>$getVideo['title'],"desc"=>$getVideo['description'],"start_date"=>$getVideo['start_date'],"end_date"=>$getVideo['end_date'],"status"=>$getVideo['status']];
            }
        }
        $data['advertiseLists'] = $videoArray;
        $data['advertise_price']= $this->Recruiteradmin_Model->videoadvertise_price();
        $this->load->view('administrator/advertisevideolist',$data);
    }
    public function videoadvertiseInsert() {
      //print_r($_FILES);die;
        $userData = $this->input->post();
        //print_r($userData);die;
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/adadvertise',$data);
        } else {

            $data1 = ["video"=>$userData['videourl'], "thumbnail"=>$userData['videothumb'], 'title'=>$userData['title'], 'recruiter_id' => 0, 'start_date' => date("Y-m-d", strtotime($userData['start_date'])), 'end_date'=>date("Y-m-d", strtotime($userData['end_date'])), 'description' => $userData['desc'], "status"=>2, "publicid"=>$userData['publicid']];

            $recruiterInserted = $this->Recruiteradmin_Model->videoadvertise_insert($data1);
            
            if($recruiterInserted) {
                redirect("administrator/recruiter/videoadvertiselist");
            } else {
                redirect("administrator/Recruiter/videoadvertise");
            }
        }
    }
    public function deletevideo() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletevideo($rid);
        redirect("administrator/recruiter/videoadvertiselist");
    }
    public function videoBlock() {
        $rid = $_GET['id'];
        $data1 = ["status"=>1];
        $jobseekerInserted = $this->Recruiteradmin_Model->videoStatus($data1, $rid);
        if($jobseekerInserted) {   
            redirect("administrator/recruiter/videoadvertiselist");
        } else {
            redirect("administrator/recruiter/videoadvertiselist");
        }   
    }
    public function videoActive() {
        $rid = $_GET['id'];
        $data1 = ["status"=>2];
        $jobseekerInserted = $this->Recruiteradmin_Model->videoStatus($data1, $rid);
        if($jobseekerInserted) {
            
            redirect("administrator/recruiter/videoadvertiselist");
        } else {
            redirect("administrator/recruiter/videoadvertiselist");
        }
    }
    public function editvideoadvertise() {
      if(!empty($_GET['id'])){
        $id = $_GET['id'];
        $data['advertiseLists'] = $this->Recruiteradmin_Model->videoadvertise_single($id);
      }
      $this->load->view('administrator/editadvertisevideo',$data);
    }
    public function videoadvertiseUpdate() {
      //print_r($_FILES);die;
        $userData = $this->input->post();
        //print_r($userData);die;
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $id = $userData['id'];
            $data['advertiseLists'] = $this->Recruiteradmin_Model->videoadvertise_single($id);     
            $this->load->view('administrator/editvideoadvertise',$data);

        } else {
            
            if (strlen($userData['videourl']) < 1) {
                # Upload Failed
                $data1 = ['title'=>$userData['title'], 'start_date' => date("Y-m-d", strtotime($userData['start_date'])), 'end_date'=>date("Y-m-d", strtotime($userData['end_date'])), 'description' => $userData['desc']];
            }
            else {

                $data1 = ["video"=>$userData['videourl'], "thumbnail"=>$userData['videothumb'], 'title'=>$userData['title'], 'start_date' => date("Y-m-d", strtotime($userData['start_date'])), 'end_date'=>date("Y-m-d", strtotime($userData['end_date'])), 'description' => $userData['desc'], "publicid"=>$userData['publicid']];
            }
            
            $recruiterInserted = $this->Recruiteradmin_Model->videoadvertise_update($data1, $userData['id']);

            if($recruiterInserted) {
                redirect("administrator/recruiter/videoadvertiselist");
            } else {
                redirect("administrator/recruiter/videoadvertiselist");
            }
        }
    }

    public function generatepdfforvideo() {

        $jid = $this->input->get('job_id');
        $amount = $this->input->get('amount');

        $getVideos = $this->Recruiteradmin_Model->videoadvertise_single($jid);
        $getRecruit = $this->Recruiteradmin_Model->companysite_detailsget($getVideos[0]['recruiter_id']);
        
        $invoice_number = "JobYoDA".rand(10000,99999);
        $output = '<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <table>
                    <tbody>
                    <tr>
                        <td style="width:300px;">
                         <img src="https://jobyoda.com/recruiterfiles/images/jobyoda.png" width="200px;">
                        </td>
                        <td style="width:400px;">
                        <div style="float:right;">
                        <p class="font-weight-bold mb-1">Invoice #'.$invoice_number.'</p>
                            <p class="text-muted">Invoice Generated Date: '.date("d M, Y").'</p>
                        </div>
                        </td>
                    </tr>
                    </tbody>
                    </table>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6" style="width:300px; margin-bottom:30px; margin-top:30px;">
       <p class="font-weight-bold mb-4" style="font-weight: bold !important; margin:0;">
       Company Information</p>
                            <p class="mb-1" style="margin:0;">'.$getRecruit[0]['cname'].'</p>
                            
                        </div>

                    </div>

                    <div class="row p-5">
                        <div class="col-md-12" style="width:800px;">
                            <table class="table" style="border:1px solid #ddd;">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">S No</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Company Name</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Start Date</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">End Date</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>';
                                $x2=1;
                                $total_amount=0;
                                foreach ($getVideos as $getVideo) {

                                    $startDate = date("d-m-Y", strtotime($getVideo['start_date']));
                                    $endDate = date("d-m-Y", strtotime($getVideo['end_date']));
                                    $diff = strtotime($getVideo['end_date'])- strtotime($getVideo['start_date']);
                                    $daydiff = floor($diff / (60 * 60 * 24));
                                    $output.= '<tr>
                                        <td style="width:50px;">'.$x2.'</td>
                                        <td style="width:140px;">'.$getRecruit[0]['cname'].'</td>
                                        <td style="width:140px;">'.$startDate.'</td>
                                        <td style="width:140px;">'.$endDate.'</td>
                                        <td style="width:140px;">'.$amount*$daydiff.'</td>
                                        
                                    </tr>';
                                $x2++;
                                $total_amount+= $amount*$daydiff;
                                }
                                    
                               $output.= '</tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        <div class="py-3 px-5 text-right" style="margin-top:30px; background:#000; padding:5px;">
                            <div class="mb-2" style="text-align:right; font-size:30px;color:#fff;">Grand Total</div>
                            <div class="h2 font-weight-light" style="text-align:right;color:#fff; margin:0;">'.$total_amount.'</div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            
                        </div>

                        <div class="py-3 px-5 text-right">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

</div>';


       $file_name = "./videoinvoiceupload/".md5(rand()) . '.pdf';
       $this->pdf->loadHtml($output);
       $this->pdf->render();
      
       $file = $this->pdf->output();
       file_put_contents($file_name,  $file );
        
        $this->load->library('email');
        $config=array(
              'protocol' => 'smtp',
              'smtp_host' => 'smtpout.asia.secureserver.net',
              'smtp_user' => 'help@jobyoda.com',
              'smtp_pass' => 'Usa@1234567',
              'smtp_port' => 465,
              'smtp_crypto' => 'ssl',
              'charset'=>'utf-8',
              'mailtype' => 'html',
              'crlf' => "\r\n",
              'newline' => "\r\n"
        );
        $this->email->initialize($config);
        $this->email->from("help@jobyoda.com", "JobYoDA");
        $this->email->to($getRecruit[0]['email']);
        $this->email->subject('JobYodA - Invoice');
        $msg = "Dear ".$getRecruit[0]['cname'] ;
        $msg .= ", Please check the invoice as below: ";
        $this->email->message($msg);
        $this->email->attach($file_name);
        
        if($this->email->send()) {

            $this->pdf->stream($file_name, array("Attachment"=>0));
            $data = ["invoice_id" => $invoice_number, "total_amount" => $total_amount, "company_id" => $getRecruit[0]['id'] , "invoice" => $file_name , 'type'=>5];
            $this->Recruiteradmin_Model->invoice_insert($data);
        
        } else {
            echo $hiredLists[0]['email'];
        }
      
     }


  public function newsadvertise() {
        $this->load->view('administrator/aadnews');
  }
  public function newsadvertiselist() {
      $data['advertiseLists'] = $this->Recruiteradmin_Model->newsadvertise_listing();
      $this->load->view('administrator/newslist',$data);
  }
    public function newsadvertiseInsert() {
      //print_r($_FILES);die;
        $userData = $this->input->post();
        //print_r($userData);die;
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('banner', 'Image', 'callback_file_check');
        
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('administrator/aadnews',$data);
        } else {
            $config = array(
                'upload_path' => "./newsupload/",
                'allowed_types' => "jpg|png|jpeg",
                //'max_size' => '1',
                //'max_width' => '1376',
                //'max_height' => '588',
            );
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('banner')) {
                $data = array('upload_data' => $this->upload->data());
                $picPath = base_url() ."newsupload/". $data['upload_data']['file_name'];

                $data1 = ["banner"=>$picPath, 'title'=>$userData['title'], 'description' => $userData['desc']];
                $recruiterInserted = $this->Recruiteradmin_Model->newsadvertise_insert($data1);
                
                if($recruiterInserted) {
                    redirect("administrator/recruiter/newsadvertiselist");
                } else {
                    redirect("administrator/Recruiter/newsadvertiselist");
                }

            } else {
                $dataerrors = $this->upload->display_errors();
                $this->form_validation->set_message('banner', $dataerrors);
                $this->load->view('administrator/aadnews',$data);
            }
        }
    }
    public function file_check($str){
        $allowed_mime_type_arr = array('image/jpeg','image/jpg','image/png','image/x-png');
        $mime = get_mime_by_extension($_FILES['banner']['name']);
        
        if($_FILES["banner"]["size"] < 2000000) {

            $fileinfo = @getimagesize($_FILES["banner"]["tmp_name"]);
            $width = $fileinfo[0];
            $height = $fileinfo[1];

            if (($width > "350" || $height > "250" ) && ($width < "250" || $height > "150" ) ) {
                $this->form_validation->set_message('file_check', 'Image dimension should be within 300X200');
                return false;
            
            } else {

                if(isset($_FILES['banner']['name']) && $_FILES['banner']['name']!=""){
                    if(in_array($mime, $allowed_mime_type_arr)){
                        return true;
                    }else{
                        $this->form_validation->set_message('file_check', 'Please select only gif/jpg/png file.');
                        return false;
                    }
                } else {
                    $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
                    return false;
                }
            }
        } else {
            $this->form_validation->set_message('file_check', 'Please choose a valid file size.');
            return false;
        }
    }
    public function deletenews() {
        $rid = $_GET['id'];
        $this->Recruiteradmin_Model->deletenews($rid);
        redirect("administrator/recruiter/newsadvertiselist");
    }
    public function editnewsadvertise() {
      if(!empty($_GET['id'])){
        $id = $_GET['id'];
        $data['advertiseLists'] = $this->Recruiteradmin_Model->newsadvertise_single($id);
      }
      $this->load->view('administrator/editnews',$data);
    }
    public function newsUpdate() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $id = $userData['id'];
            $data['advertiseLists'] = $this->Recruiteradmin_Model->newsadvertise_single($id);
            $this->load->view('administrator/aadnews',$data);

        } else {
            $config = array(
                'upload_path' => "./newsupload/",
                'allowed_types' => "jpg|png|jpeg",
                //'max_size' => '500000',
                //'max_width' => '1376',
                //'max_height' => '588',
            );
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('banner')) {
                $data = array('upload_data' => $this->upload->data());
                $picPath = base_url() ."newsupload/". $data['upload_data']['file_name'];

                $data1 = ["banner"=>$picPath, 'title'=>$userData['title'], 'description' => $userData['desc']];
            } else {
                $data1 = ['title'=>$userData['title'], 'description' => $userData['desc']];
            }

            $recruiterInserted = $this->Recruiteradmin_Model->newsadvertise_update($data1, $userData['id']);

            if($recruiterInserted) {
                redirect("administrator/recruiter/newsadvertiselist");
            } else {
                redirect("administrator/recruiter/newsadvertiselist");
            }
        }
    }
    public function generateString($n) { 
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
      
        for ($i = 0; $i < $n; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
      
        return $randomString; 
    }
}
?>	
