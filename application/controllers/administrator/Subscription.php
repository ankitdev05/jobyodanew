<?php
ob_start();
ini_set('display_errors', 1);

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Subscription extends CI_Controller {
    
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Subscription_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('pdf');
        if($this->session->userdata('adminSession')) {
            
        } else {
            redirect("administrator/admin/index");
        }
    }

    public function subscriptionlist() {

        $data['subscriptions'] = $this->Subscription_Model->list();
        $this->load->view('administrator/subscriptions', $data);
    }

    public function subscriptionadd() {

        $this->load->view('administrator/subscription_add');
    }

    public function subscriptioncreate() {

        $userData = $this->input->post();
        
        $this->form_validation->set_rules('plan_name', 'Plan Name', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('duration', 'Duration', 'trim|required');
        $this->form_validation->set_rules('cost', 'Cost', 'trim|required');
        $this->form_validation->set_rules('total_mail', 'Total Mail', 'trim|required');
        $this->form_validation->set_rules('total_job_post', 'Total Job Post', 'trim|required');
        $this->form_validation->set_rules('total_resume_access', 'Total Resume Access', 'trim|required');
        $this->form_validation->set_rules('total_video_post', 'Total Video Post', 'trim|required');
        $this->form_validation->set_rules('total_advertise', 'Total Advertise', 'trim|required');
        $this->form_validation->set_rules('total_job_boost', 'Total Job Boost', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            
            $data['errors'] = $this->form_validation->error_array();
            $data['userData'] = $userData;
            $this->load->view('administrator/subscription_add',$data);

        } else {

            $record = ["plan_name"=>$userData['plan_name'], "description"=> $userData['desc'], 'duration'=>$userData['duration'], 'cost'=>$userData['cost'] , 'total_mail'=>$userData['total_mail'], "total_job_post"=> $userData['total_job_post'], 'total_resume_access'=>$userData['total_resume_access'], "total_video_post"=> $userData['total_video_post'], "total_advertise"=> $userData['total_advertise'], "total_job_boost"=>$userData['total_job_boost'], "status"=>$userData['status']];

            $this->Subscription_Model->subscribe_insert($record);
            return redirect('administrator/subscription_plans');
        }
    }

    public function subscriptionedit() {
        $id = base64_decode($_GET['id']);
        
        $userData = $this->Subscription_Model->view($id);
        $data['userData'] = $userData[0];
        $data['id'] = base64_encode($id);
        $this->load->view('administrator/subscription_edit', $data);
    }

    public function subscriptionupdate() {
        $userData = $this->input->post();
        
        $this->form_validation->set_rules('plan_name', 'Plan Name', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('duration', 'Duration', 'trim|required');
        $this->form_validation->set_rules('cost', 'Cost', 'trim|required');
        $this->form_validation->set_rules('total_mail', 'Total Mail', 'trim|required');
        $this->form_validation->set_rules('total_job_post', 'Total Job Post', 'trim|required');
        $this->form_validation->set_rules('total_resume_access', 'Total Resume Access', 'trim|required');
        $this->form_validation->set_rules('total_video_post', 'Total Video Post', 'trim|required');
        $this->form_validation->set_rules('total_advertise', 'Total Advertise', 'trim|required');
        $this->form_validation->set_rules('total_job_boost', 'Total Job Boost', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            
            $data['errors'] = $this->form_validation->error_array();
            $data['userData'] = $userData;
            $data['id'] = base64_encode($_GET['id']);
            $this->load->view('administrator/subscription_edit',$data);

        } else {

            $id = base64_decode($_GET['id']);

            $record = ["plan_name"=>$userData['plan_name'], "description"=> $userData['desc'], 'duration'=>$userData['duration'], 'cost'=>$userData['cost'] , 'total_mail'=>$userData['total_mail'], "total_job_post"=> $userData['total_job_post'], 'total_resume_access'=>$userData['total_resume_access'], "total_video_post"=> $userData['total_video_post'], "total_advertise"=> $userData['total_advertise'], "total_job_boost"=>$userData['total_job_boost'], "status"=>$userData['status']];

            $this->Subscription_Model->subscribe_update($record, $id);
            return redirect('administrator/subscription_plans');
        }
    }

    public function deletePlan() {
        $rid = $_GET['id'];
        $this->Subscription_Model->delete_plan($rid);

        redirect("administrator/subscription_plans");
    }

    public function ajaxplan() {
        $id = $_POST['pid'];
        
        $userData = $this->Subscription_Model->view($id);
        $datauserData = $userData[0];
        
        echo json_encode($datauserData);
        exit;
    }

    public function ajaxexistingplan() {
        $id = $_POST['rid'];
        
        $userData = $this->Subscription_Model->view_existing($id);
        $datauserData = $userData[0];
        
        echo json_encode($datauserData);
        exit;
    }

    public function subscribed() {

        $userData = $this->input->post();

        $plans = $this->Subscription_Model->view($userData['plan']);
        $plan =  $plans[0];

        $current_date = date('d-m-Y');
        $expiry_date = date('d-m-Y', strtotime("+".$plan['duration']." months"));

        if($userData['plan_action'] == "renew") {
            
            $record = ["status"=>2];
            $this->Subscription_Model->recruiter_subscribe_update($record, $userData['recruiter_id']);

            $record = ["recruiter_id" => $userData['recruiter_id'], 
                       "plan_id" => $userData['plan'], 
                       'start_date' => $current_date, 
                       'expiry_date' => $expiry_date,
                       'actual_cost' => $plan['cost'], 
                       "plan_cost"=> $userData['cost'],
                       'total_mail'=>$plan['total_mail'], 
                       "remaining_mail"=> $userData['total_mail'],
                       "total_job_post"=> $plan['total_job_post'], 
                       "remaining_job_post"=>$userData['total_job_post'], 
                       "total_resume_access"=>$plan['total_resume_access'],
                       "remaining_resume_access"=>$userData['total_resume_access'],
                       "total_video_post"=>$plan['total_video_post'],
                       "remaining_video_post"=>$userData['total_video_post'],
                       "total_advertise"=>$plan['total_advertise'],
                       "remaning_advertise"=>$userData['total_advertise'],
                       "total_job_boost"=>$plan['total_job_boost'],
                       "remaining_job_boost"=>$userData['total_job_boost'],
                       "status"=>1,
                   ];

            $this->Subscription_Model->recruiter_subscribe_insert($record);
        
        } else if($userData['plan_action'] == "new") {
            
            $record = ["recruiter_id" => $userData['recruiter_id'], 
                       "plan_id" => $userData['plan'], 
                       'start_date' => $current_date, 
                       'expiry_date' => $expiry_date,
                       'actual_cost' => $plan['cost'], 
                       "plan_cost"=> $userData['cost'],
                       'total_mail'=>$plan['total_mail'], 
                       "remaining_mail"=> $userData['total_mail'],
                       "total_job_post"=> $plan['total_job_post'], 
                       "remaining_job_post"=>$userData['total_job_post'], 
                       "total_resume_access"=>$plan['total_resume_access'],
                       "remaining_resume_access"=>$userData['total_resume_access'],
                       "total_video_post"=>$plan['total_video_post'],
                       "remaining_video_post"=>$userData['total_video_post'],
                       "total_advertise"=>$plan['total_advertise'],
                       "remaning_advertise"=>$userData['total_advertise'],
                       "total_job_boost"=>$plan['total_job_boost'],
                       "remaining_job_boost"=>$userData['total_job_boost'],
                       "status"=>1,
                   ];

            $this->Subscription_Model->recruiter_subscribe_insert($record);
        
        } else {

            $userPlan = $this->Subscription_Model->view($id);

            $remaining_mail = $userPlan[0]['remaining_mail'] + $userData['total_mail'] - $userPlan[0]['total_mail'];
            $remaining_job_post = $userPlan[0]['remaining_job_post'] + $userData['total_job_post'] - $userPlan[0]['total_job_post'];
            $remaning_advertise = $userPlan[0]['remaning_advertise'] + $userData['total_advertise'] - $userPlan[0]['total_advertise'];
            $remaining_video_post = $userPlan[0]['remaining_video_post'] + $userData['total_video_post'] - $userPlan[0]['total_video_post'];
            $remaining_resume_access = $userPlan[0]['remaining_resume_access'] + $userData['total_resume_access'] - $userPlan[0]['total_resume_access'];
            $remaining_job_boost = $userPlan[0]['remaining_job_boost'] + $userData['total_job_boost'] - $userPlan[0]['total_job_boost'];
            
            $record = [
                       'start_date' => $current_date, 
                       'expiry_date' => $expiry_date,
                       'actual_cost' => $plan['cost'], 
                       "plan_cost" => $userData['cost'],
                       'total_mail' => $plan['total_mail'], 
                       "remaining_mail" => $remaining_mail,
                       "total_job_post" => $plan['total_job_post'], 
                       "remaining_job_post" => $remaining_job_post, 
                       "total_resume_access" => $plan['total_resume_access'],
                       "remaining_resume_access" => $remaining_resume_access,
                       "total_video_post" => $plan['total_video_post'],
                       "remaining_video_post" => $remaining_video_post,
                       "total_advertise" => $plan['total_advertise'],
                       "remaning_advertise" => $remaning_advertise,
                       "total_job_boost" => $plan['total_job_boost'],
                       "remaining_job_boost" => $remaining_job_boost,
                       "status"=>1,
                   ];

            $this->Subscription_Model->recruiter_subscribe_update($record, $userData['recruiter_id']);

        }
        return redirect('administrator/recruiterList');
    }


}

?>