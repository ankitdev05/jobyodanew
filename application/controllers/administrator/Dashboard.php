<?php
ob_start();
ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Dashboard extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('Admin_Model');
        $this->load->model('Jobseekeradmin_Model');
        $this->load->model('Jobpostadmin_Model');
        $this->load->model('Recruiteradmin_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        if($this->session->userdata('adminSession')) {
            
        } else {
            redirect("administrator/admin/index");
        }
    }
    
    public function index() {
        $data["adminLists"] = $this->Recruiteradmin_Model->admin_listing();
        $data["boostamount"] = $this->Recruiteradmin_Model->boost_amount();
        $data["appLists"] = $this->Recruiteradmin_Model->recruiter_lists_approved();
        $data["pendingLists"] = $this->Recruiteradmin_Model->pending_recruiters();
        $data["jobLists"] = $this->Jobpostadmin_Model->job_fetchAll();
        $data["appliedJobs"] = $this->Jobpostadmin_Model->appliedJobs();
        $data["openings"] = $this->Jobpost_Model->job_openings();
        //echo $this->db->last_query();die;
        $data["candidates"] = $this->Recruiteradmin_Model->candidatesLists();
        $data['compListing'] = $this->Recruiteradmin_Model->company_lists();
        $data['storyListing'] = $this->Recruiteradmin_Model->all_stories();
        $data["invoiceLists"] = $this->Recruiteradmin_Model->invoice_lists(); 
        $data["contactLists"] = $this->Jobseekeradmin_Model->contact_lists1();
        $data["adLists"] = $this->Jobseekeradmin_Model->advertise_lists();

        $data["transferRequest"] = $this->Recruiteradmin_Model->recruiter_transferfetch();
        $data["testimonialRequest"] = $this->Recruit_Model->testimonial_all();

        /*foreach($compListing as $compList) {
            $renames = $this->Recruiteradmin_Model->recruiter_name($compList['parent_id']);
            foreach($renames as $rename) {
              $nameRe = $rename['cname'];
            }
            $data["CompLists"][] = ["rid"=>$compList['id'], "cname"=>$compList['cname'], "email"=>$compList['email'], "address"=>$compList['address'], "reName"=> $nameRe];
        }*/
        $this->load->view('administrator/index', $data);
    }
    
     public function logout() {
        //unset($_SESSION['userSession']);
        $this->session->unset_userdata('adminSession');
        redirect("administrator/admin/index");
    }
}
?>