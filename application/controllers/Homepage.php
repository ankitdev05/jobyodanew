<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittall
 * @license         Mobulous
 */
class Homepage extends CI_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Newpoints_Model');
        $this->load->model('recruiter/Homejob_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('google');
        $this->load->library('Facebook');
        
    }

    public function contact() {
        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data["states"] = $this->User_Model->getownstate();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();

        if($this->session->userdata('contactsuccessmsg')) {
            $this->session->set_tempdata('successmsg', "Submitted Successfully.");
            $this->session->unset_userdata('contactsuccessmsg');
        }

        $data['meta_title'] = "Contact Jobyoda | Jobyoda BPO Jobs Inquiries | Call us now";
        $data['meta_description'] = "For any inquiries on any BPO job placement, call JobYoDa, a BPO Job Platform that helps jobseekers find the best BPO jobs in the Philippines. Apply now!";

        $this->load->view('contact', $data);
    }

    public function thankyou() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data['industries'] = $this->Common_Model->industry_lists();
        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;

        $data['ad_list'] = $ad_lists1;
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['popcat'] = $this->Common_Model->popular_category();
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["states"] = $this->User_Model->getownstate();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        $data['openings'] = $this->Jobpost_Model->job_openings();

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter);
        $data["workFromhomeJobs"] = $featuredTopPicks['workfromhome'];
        $data["day1hmoJobs"] = $featuredTopPicks['day1hmo'];
        $data["freefoodJobs"] = $featuredTopPicks['freefood'];
        $data["monthpayJobs"] = $featuredTopPicks['monthpay'];

        $data["hotJobs"] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long);
        $data["itJobs"] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long);
        $data["leadershipJobs"] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long);
        $data["instantJobs"] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long);



        $data['meta_title'] = "";
        $data['meta_description'] = "";

        $this->load->view('thankyoupage', $data);
    }

    public function sitemap() {
        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data["states"] = $this->User_Model->getownstate();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $this->load->view('sitemap', $data);
    }

    public function fetch_location(){
        $location =$this->input->post();
        $lat = (float)$location['currentLatitude'];
        $long = (float)$location['currentLongitude'];
        $locationdata = array(
                'current_lat'  => $lat,
                'current_long'     => $long
        );

        $this->session->set_userdata('locationdata',$locationdata);

        echo json_encode($location);
    }

    /*public function index(){
        $this->load->view('index');
    }*/


    public function ourpartners() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data['industries'] = $this->Common_Model->industry_lists();
        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;

        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)) {
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']             

                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['popcat'] = $this->Common_Model->popular_category();
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["states"] = $this->User_Model->getownstate();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];

            $expmonth = $exp_month;
            $expyear = $exp_year;

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        $data['meta_title'] = "Top BPO Companies Philippines | Jobyoda Partner Company Near You";
        $data['meta_description'] = "Be a part of the top BPO companies in the Philippines with JobyoDa, helping jobseekers find the best BPO jobs available today. Apply now!";

        $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
        $this->load->view('partners', $data);
    }


    public function citysearch() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data['industries'] = $this->Common_Model->industry_lists();
        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;

        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)) {
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']             

                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['popcat'] = $this->Common_Model->popular_category();
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["states"] = $this->User_Model->getownstate();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];

            $expmonth = $exp_month;
            $expyear = $exp_year;

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }        

        $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);

        $data['meta_title'] = "City Search BPO Jobs | Call Center Jobs near me | Jobyoda Location";
        $data['meta_description'] = "Find jobs based on location with JobYoDa ,helping jobseekers find the best BPO jobs available today. Apply now!";
        
        $this->load->view('citysearch', $data);
    }

    public function yourexpertise() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data['industries'] = $this->Common_Model->industry_lists();
        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;

        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)) {
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']             

                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['popcat'] = $this->Common_Model->popular_category();
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["states"] = $this->User_Model->getownstate();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];

            $expmonth = $exp_month;
            $expyear = $exp_year;

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }        
        $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);

        $data['meta_title'] = "Jobyoda Expertise | BPO Job Recruitment Expert | Jobyoda Philippines";
        $data['meta_description'] = "Find jobs based on your skills with JobYoDa ,helping jobseekers find the best BPO jobs available today. Apply now!";

        $this->load->view('expertise', $data);
    }


    public function newindex() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;

        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)) {
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']
                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['popcat'] = $this->Common_Model->popular_category();
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["states"] = $this->User_Model->getownstate();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        $data['openings'] = $this->Jobpost_Model->job_openings();

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        //var_dump($expfilter);die;

        $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
        $companyidsarray = array();
        foreach($data["ourPartners"] as $ourPartners) {
            $companyidsarray[] = $ourPartners['parent_id'];
        }
        $companyidsarray = array_unique($companyidsarray);
        
        $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);
        $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);
        $data["nearByJobs"] = $this->getnearByJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter, $companyidsarray);
        //$data["workFromhomeJobs"] = $featuredTopPicks['workfromhome'];
        $data["bonusJobs"] = $featuredTopPicks['bonus'];
        $data["shiftJobs"] = $featuredTopPicks['shiftjobs'];
        $data["monthpayJobs"] = $featuredTopPicks['monthpay'];
        //$data["day1hmoJobs"] = $featuredTopPicks['day1hmo'];
        //$data["freefoodJobs"] = $featuredTopPicks['freefood'];
        //$data["freefoodJobs"] = $featuredTopPicks['freefood'];

        $data["hotJobs"] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);

        //$data["itJobs"] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);

        //$data["leadershipJobs"] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);

        // $data["instantJobs"] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);

        //Fetch videos
        $videosArray = array();
        $fetchvideos = $this->Newpoints_Model->fetchvideos();
        foreach($fetchvideos as $fetchvideo) {
            $videosArray[] = ["id"=>$fetchvideo['id'],"video"=>$fetchvideo['video'],"banner"=>$fetchvideo['thumbnail'],"title"=>$fetchvideo['title'],"desc"=>$fetchvideo['description'],"posted"=>date("d F Y", strtotime($fetchvideo['updated_at']))];
        }
        $data["videoListings"] = $videosArray;

        $data['meta_title'] = "BPO Jobs Philippines | Call Center Jobs | Jobyoda Dream Jobs Site";
        $data['meta_description'] = "Find work from home, part time jobs online, and full time BPO and call center positions at JobYoDa. See jobs near you and apply for your dream job today!";

        $this->load->view('homepage', $data);
    }

    public function fetchpopularsearch() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat = '14.5676285';
            $current_long = '120.7397551';
        }

        $data = $this->input->post();
        $result = array();

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
        $companyidsarray = array();
        foreach($data["ourPartners"] as $ourPartners) {
            $companyidsarray[] = $ourPartners['parent_id'];
        }
        $companyidsarray = array_unique($companyidsarray);

        $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter, $companyidsarray);

        if($data['keyword'] == 2) {
            
            $result['popular'] = $featuredTopPicks['workfromhome'];

        } else if($data['keyword'] == 3) {

            $result['popular'] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 4) {

            $result['popular'] = $featuredTopPicks['day1hmo'];
        
        } else if($data['keyword'] == 5) {

            $result['popular'] = $featuredTopPicks['freefood'];
        
        } else if($data['keyword'] == 6) {

            $result['popular'] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 7) {

            $result['popular'] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        
        } else if($data['keyword'] == 8) {

            $result['popular'] = $featuredTopPicks['monthpay'];
        
        } else if($data['keyword'] == 1) {

            $result['popular'] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray);
        }
        
        $result['extra'] = ["keyword"=>$data['keyword']]; 
        
        $response = $this->load->view('homepage_popular', $result, TRUE);
        
        //print_r($response);die;

        echo $response;

        exit;
    }

    public function getourpartners($userID, $expfilter, $current_lat, $current_long) {

        $company_lists = $this->Newpoints_Model->companysite_lists($current_lat, $current_long);

        if(!empty($company_lists)) {
            $x=0;

            foreach ($company_lists as $company_list) {
                $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userID, $company_list['id'], $expfilter, $current_lat, $current_long);
                if($jobcountfetch > 0) {
                    
                    if(strlen($company_list['companyPic']) > 0) {
                        
                        $companylist[$x] = [
                              "id" => $company_list['id'],
                              "image" => $company_list['companyPic'],
                              "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                              "jobcount" => $jobcountfetch,
                              "parent_id"=>$company_list['parent_id']
                        ];
                    
                    } else {

                        $siteimgs = $this->Jobpost_Model->fetch_companyPic($company_list['parent_id']);
                        if($siteimgs) {
                            $companylist[$x] = [
                                  "id" => $company_list['id'],
                                  "image" => $siteimgs[0]['companyPic'],
                                  "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                  "jobcount" => $jobcountfetch,
                                  "parent_id"=>$company_list['parent_id']
                            ];
                        }
                    }
                    $x++;
                }
            }
        } else {
            $companylist = [];
        }

        $jobcount = array();
        foreach ($companylist as $key => $row)
        {
            $jobcount[$key] = $row['jobcount'];
        }
        array_multisort($jobcount, SORT_DESC, $companylist);

        return $companylist;
    }

    public function getcitysearch($userID, $expfilter, $current_lat, $current_long) {

        $city_lists = $this->Jobpost_Model->homecity_lists();
        if(!empty($city_lists)) {

            $c=0;
            $citylist = array();

            if ($this->session->userdata('usersess')) {
                $userSess = $this->session->userdata('usersess');  
                $tokendata = ['id'=>$userSess['id']];
                $userData = $this->User_Model->token_match($tokendata);
                if($userData) {
                    $usercity = $userData[0]['city'];
                } else {
                    $usercity = "";
                }
            } else {
                $usercity = "";
            }
            
            foreach ($city_lists as $city_list) {
                
                if(strpos($usercity, $city_list['name']) !== false && strlen($usercity) > 0) {
                    if($city_list['name'] == "Metro Manila") {
                        $city_list['name'] = "Manila";
                    }
                    $jobcountfetch = $this->Newpoints_Model->city_jobcount($userID, $city_list['name'], $expfilter, $current_lat, $current_long);
                    if($jobcountfetch > 0) {
                        if($city_list['name'] == "Manila") {
                            $city_list['name'] = "Metro Manila";
                        }
                        $citylist[$c] = [
                              "id" => $city_list['id'],
                              "cityname" => $city_list['name'],
                              "cityslug" => $city_list['slug'],
                              "image" => $city_list['image'],
                              "jobcount" => $jobcountfetch
                        ];
                        $c++;
                    }
                }
            }
            if(count($citylist) > 0) {
                $cc = $c;
            } else {
                $cc = 0;
            }
            foreach ($city_lists as $city_list) {

                if(strpos($usercity, $city_list['name']) !== false && strlen($usercity) > 0) {
                } else {
                    if($city_list['name'] == "Metro Manila") {
                        $city_list['name'] = "Manila";
                    }
                    $jobcountfetch = $this->Newpoints_Model->city_jobcount($userID, $city_list['name'], $expfilter, $current_lat, $current_long);
                    if($jobcountfetch > 0) {

                        if($city_list['name'] == "Manila") {
                            $city_list['name'] = "Metro Manila";
                        }
                        $citylist[$cc] = [
                              "id" => $city_list['id'],
                              "cityname" => $city_list['name'],
                              "cityslug" => $city_list['slug'],
                              "image" => $city_list['image'],
                              "jobcount" => $jobcountfetch
                        ];
                        $cc++;
                    }
                }
            }
        } else {
            $citylist = [];
        }

        return $citylist;
    }

    public function getexpertise($userID, $expfilter, $current_lat, $current_long) {

        $cat_lists = $this->Jobpost_Model->category_lists();

        if(!empty($cat_lists)) {
            $x=0;

            foreach ($cat_lists as $cat_list) {
                $jobcountfetch = $this->Newpoints_Model->category_jobcount($userID, $cat_list['id'], $expfilter, $current_lat, $current_long);
                
                $categorylist[$x] = [
                      "id" => $cat_list['id'],
                      "catname" => $cat_list['category'],
                      "jobcount" => $jobcountfetch
                ];
                $x++;
            }
        } else {
            $categorylist = [];
        }
        return $categorylist;
    }

    public function getnearByJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {
        $data1 = array();
        $neglectarr = array();
            
        foreach($companyidsarray as $companyidsarr) {
            $jobfetchs = $this->Homejob_Model->job_fetch_homeforappnearby_groupby($userID, $current_lat, $current_long, $expfilter, $companyidsarr);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $neglectarr[] = $jobfetch['id'];
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);

                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                    $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = '1';
                    } else {
                        $jobstatus = '0';
                    }

                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                           
                    $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
                }
                $data1;
            } else{
                $data1=[];
            }
        }

        usort($data1, function($a, $b) {
            return $a['distance'] <=> $b['distance'];
        });

        $jobfetchs = $this->Jobpost_Model->job_fetch_homeforappnearby($userID, $current_lat, $current_long, $expfilter);
        if ($jobfetchs) {
            foreach ($jobfetchs as $jobfetch) {

                if(is_array($jobfetch['id'], $neglectarr)) { } else {

                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                    $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = '1';
                    } else {
                        $jobstatus = '0';
                    }

                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                           
                    $data1[] = ["jobpost_id" => $jobfetch['id'], "recruiter_id" => $jobfetch['recruiter_id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
                }
            }
            return $data1;
        } else{
            $data1=[];
            return $data1;
        }
        
    }

    public function gethotJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $hotjobss = array();
        $bostArr = array();
        $neglectArr = array();

        foreach($companyidsarray as $companyidsarr) {

            $hotjobData = $this->Homejob_Model->hotjob_fetch_latlongbylimit_groupby($userID, $expfilter, $current_lat, $current_long, $companyidsarr);
            foreach ($hotjobData as $hotjobsData) {

                $neglectArr[] = $hotjobsData['id'];

                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                if(!empty($jobTop) && count($jobTop)>=1) {
                      
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $hotjobss[] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "recruiter_id" =>$hotjobsData['recruiter_id'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "jobPitch"=>$hotjobsData['jobPitch'],
                            "salary" => number_format($basicsalary),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode" => $hotjobsData['mode'],
                    ];
                }
            }
        }

        usort($hotjobss, function($a, $b) {
            return $a['distance'] <=> $b['distance'];
        });

        $boostjobData = $this->Jobpost_Model->boostjob_fetch_latlongbylimit($userID,$expfilter, $current_lat,$current_long);
        foreach ($boostjobData as $boostjobsData) {

            if(is_array($boostjobsData['id'], $neglectArr)) { } else {
                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                if(!empty($jobTop) && count($jobTop)>=1) {
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }                
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $hotjobss[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance
                    ];
                }
            }
        }

        $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlongbylimit($userID, $expfilter, $current_lat, $current_long);
        foreach ($hotjobData as $hotjobsData) {

            if(in_array($hotjobsData['id'], $bostArr)) {
            } else {

                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                if(!empty($jobTop) && count($jobTop)>=1) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $hotjobss[] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "recruiter_id" =>$hotjobsData['recruiter_id'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "jobPitch"=>$hotjobsData['jobPitch'],
                            "salary" => number_format($basicsalary),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode" => $hotjobsData['mode'],
                    ];
                }
            }
        }

        return $hotjobss;

    }

    public function getitJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $itcatjobs = array();
        $negletarr = array();

        foreach($companyidsarray as $companyidsarr) {
            $itcatjobDatas = $this->Homejob_Model->catjob_fetch_information_technology($userID, $expfilter, $current_lat, $current_long, $companyidsarr);
            foreach ($itcatjobDatas as $itcatjobData) {
                $negletarr[] = $itcatjobData['id'];

                $jobTop = $this->Jobpost_Model->job_toppicks($itcatjobData['id']);
          
                if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($itcatjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($itcatjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($itcatjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($itcatjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($itcatjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($itcatjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($itcatjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $itcatjobs[] = [
                            "jobpost_id" => $itcatjobData['id'],
                            "comapnyId" =>$itcatjobData['compId'],
                            "recruiter_id" =>$itcatjobData['recruiter_id'],
                            "job_title" => $itcatjobData['jobtitle'],
                            "jobDesc" => $itcatjobData['jobDesc'],
                            "jobPitch"=>$itcatjobData['jobPitch'],
                            "salary" => number_format($itcatjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $itcatjobData['latitude'], 
                            "longitude"=>$itcatjobData['longitude'],
                            "jobexpire"=> $itcatjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$itcatjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=>$itcatjobData['mode'],
                    ];
                }
            }
        }

        usort($itcatjobs, function($a, $b) {
            return $a['distance'] <=> $b['distance'];
        });

        $itcatjobDatas = $this->Jobpost_Model->catjob_fetch_information_technology($userID, $expfilter, $current_lat, $current_long);
        foreach ($itcatjobDatas as $itcatjobData) {

            if(is_array($itcatjobData['id'], $negletarr)) { } else {
                $jobTop = $this->Jobpost_Model->job_toppicks($itcatjobData['id']);
          
                if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($itcatjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($itcatjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($itcatjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($itcatjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($itcatjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($itcatjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($itcatjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $itcatjobs[] = [
                            "jobpost_id" => $itcatjobData['id'],
                            "comapnyId" =>$itcatjobData['compId'],
                            "recruiter_id" =>$itcatjobData['recruiter_id'],
                            "job_title" => $itcatjobData['jobtitle'],
                            "jobDesc" => $itcatjobData['jobDesc'],
                            "jobPitch"=>$itcatjobData['jobPitch'],
                            "salary" => number_format($itcatjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $itcatjobData['latitude'], 
                            "longitude"=>$itcatjobData['longitude'],
                            "jobexpire"=> $itcatjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$itcatjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=>$itcatjobData['mode'],
                    ];
                }

            }
        }
        return $itcatjobs;
    }

    public function getleadershipJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $leadercatjobs = array();
        $negletarr = array();

        foreach($companyidsarray as $companyidsarr) {
            $catjobDatas = $this->Homejob_Model->catjob_fetch_leadership($userID, $expfilter, $current_lat, $current_long, $companyidsarr);
            foreach ($catjobDatas as $catjobData) {
                $negletarr[] = $catjobData['id'];
                $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
          
                if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $leadercatjobs[] = [
                            "jobpost_id" => $catjobData['id'],
                            "comapnyId" =>$catjobData['compId'],
                            "recruiter_id" =>$catjobData['recruiter_id'],
                            "job_title" => $catjobData['jobtitle'],
                            "jobDesc" => $catjobData['jobDesc'],
                            "jobPitch"=>$catjobData['jobPitch'],
                            "salary" => number_format($catjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $catjobData['latitude'], 
                            "longitude"=>$catjobData['longitude'],
                            "jobexpire"=> $catjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$catjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $catjobData['mode'], 
                    ];
                }
            }
        }

        usort($leadercatjobs, function($a, $b) {
            return $a['distance'] <=> $b['distance'];
        });

        $catjobDatas = $this->Jobpost_Model->catjob_fetch_leadership($userID, $expfilter, $current_lat, $current_long);
        foreach ($catjobDatas as $catjobData) {

            if(is_array($catjobData['id'], $negletarr)) { } else {

                $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
          
                if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $leadercatjobs[] = [
                            "jobpost_id" => $catjobData['id'],
                            "comapnyId" =>$catjobData['compId'],
                            "recruiter_id" =>$catjobData['recruiter_id'],
                            "job_title" => $catjobData['jobtitle'],
                            "jobDesc" => $catjobData['jobDesc'],
                            "jobPitch"=>$catjobData['jobPitch'],
                            "salary" => number_format($catjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $catjobData['latitude'], 
                            "longitude"=>$catjobData['longitude'],
                            "jobexpire"=> $catjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$catjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $catjobData['mode'], 
                    ];
                }
            }
        }
        return $leadercatjobs;
    }

    public function getinstantJobs($userID, $expfilter, $current_lat, $current_long, $companyidsarray) {

        $instantjobs = array();
        $neglectarr = array();

        foreach($companyidsarray as $companyidsarr) {
            $catjobDatas = $this->Homejob_Model->instantscreening_fetch_groupby($userID, $expfilter, $current_lat, $current_long, $companyidsarr);
            foreach ($catjobDatas as $catjobData) {

                $neglectarr[] = $catjobData['id'];

                $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
                //if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $instantjobs[] = [
                            "jobpost_id" => $catjobData['id'],
                            "comapnyId" =>$catjobData['compId'],
                            "job_title" => $catjobData['jobtitle'],
                            "jobDesc" => $catjobData['jobDesc'],
                            "jobPitch"=>$catjobData['jobPitch'],
                            "salary" => number_format($catjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $catjobData['latitude'], 
                            "longitude"=>$catjobData['longitude'],
                            "jobexpire"=> $catjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$catjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $catjobData['mode'],
                    ];
                //}
            }
        }

        usort($instantjobs, function($a, $b) {
            return $a['distance'] <=> $b['distance'];
        });

        $catjobDatas = $this->Jobpost_Model->instantscreening_fetch($userID, $expfilter, $current_lat, $current_long);
        foreach ($catjobDatas as $catjobData) {

            if(in_array($catjobData['id'], $neglectarr)) { } else {
                $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
          
                if(!empty($jobTop) && count($jobTop)>=0) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $instantjobs[] = [
                            "jobpost_id" => $catjobData['id'],
                            "comapnyId" =>$catjobData['compId'],
                            "job_title" => $catjobData['jobtitle'],
                            "jobDesc" => $catjobData['jobDesc'],
                            "jobPitch"=>$catjobData['jobPitch'],
                            "salary" => number_format($catjobData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $catjobData['latitude'], 
                            "longitude"=>$catjobData['longitude'],
                            "jobexpire"=> $catjobData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$catjobData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $catjobData['mode'],
                    ];
                }
            }
        }

        return $instantjobs;

    }

    public function getFeaturedTopPicks($uid, $current_lat, $current_long, $expfilter, $companyidsarray) {

        $dataTopicks["monthpay"] = array();
        $dataTopicks["shiftjobs"] = array();
        $dataTopicks["retirement"] = array();
        $dataTopicks["day1hmo"] = array();
        $dataTopicks["freefood"] = array();
        $dataTopicks["bonus"] = array();
        $dataTopicks["workfromhome"] = array();


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr = array();
            foreach($companyidsarray as $companyidsarr) {

                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                if ($hotjobDataonegroup) {
                    foreach ($hotjobDataonegroup as $hotjobsData) {
                        $neglectarr[] = $hotjobsData['id'];
                        $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 1);
                        $datatoppicks1 = "1"; 
                        
                        if(isset($jobTopseleted[0]['picks_id'])) {
                            $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTopseleted[1]['picks_id'])) {
                            $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                        
                        $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                        $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobsavedStatus[0]['status'])) {
                            $jobsstatus = '1';
                        } else {
                            $jobsstatus = '0';
                        }
                        
                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = '';
                        }
                        $dataTopicks["bonus"][] = ["jobpost_id" => $hotjobsData['id'], 
                                  "comapnyId" => $hotjobsData['compId'],
                                  "recruiter_id" => $hotjobsData['recruiter_id'],
                                  "job_title" => $hotjobsData['jobtitle'], 
                                  "jobDesc" => $hotjobsData['jobDesc'], 
                                  "salary" => number_format($basicsalary), 
                                  "companyName" => $companydetail[0]["cname"], 
                                  "companyAddress" => $companyaddress,
                                  "job_image" => $jobImageData,
                                  "jobPitch"=>$hotjobsData['jobPitch'], 
                                  "cname"=>$cname, 
                                  "distance" =>$distance, 
                                  'job_image' =>$jobImageData,
                                  "savedjob"=> $jobsstatus,
                                  "toppicks1" => $datatoppicks1,
                                  "toppicks2" => $datatoppicks2,
                                  "toppicks3" => $datatoppicks3,
                                  "mode"=>$hotjobsData['mode'], 
                        ];

                    }
                }
            }

            usort($dataTopicks["bonus"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            foreach ($hotjobDataone as $hotjobsData) {
                
                if(is_array($hotjobsData['id'], $neglectarr)) { } else {

                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 1);
                    $datatoppicks1 = "1"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["bonus"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_freefood($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $neglectarr1 = array();
            foreach($companyidsarray as $companyidsarr) {

                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_freefood_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);

                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr1[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 2);
                    $datatoppicks1 = "2"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["freefood"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["freefood"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr1)) { } else {

                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 2);
                    $datatoppicks1 = "2"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["freefood"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_dayihmo($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr2 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_dayihmo_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr2[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 3);
                    $datatoppicks1 = "3"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["day1hmo"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["day1hmo"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr2)) { } else {
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 3);
                    $datatoppicks1 = "3"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["day1hmo"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_shift($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr3 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_shift_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr3[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                    $datatoppicks1 = "5"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["shiftjobs"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["shiftjobs"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr3)) { } else {
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                    $datatoppicks1 = "5"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["shiftjobs"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_monthpay($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr4 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_monthpay_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr4[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 6);
                    $datatoppicks1 = "6"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["monthpay"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["monthpay"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr4)) { } else {

                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 6);
                    $datatoppicks1 = "6"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["monthpay"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }

        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_workfromhome($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr5 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_workfromhome_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr5[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 7);
                    $datatoppicks1 = "7"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["workfromhome"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["workfromhome"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr5)) { } else {
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 7);
                    $datatoppicks1 = "7"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["workfromhome"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_allowances($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr6 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_allowances_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr6[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                    $datatoppicks1 = "5"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["retirement"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }
            
            usort($dataTopicks["retirement"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            foreach ($hotjobDataone as $hotjobsData) {

                if(is_array($hotjobsData['id'], $neglectarr6)) { } else {
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                    $datatoppicks1 = "5"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["retirement"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];
                }
            }
        }

        return $dataTopicks;

    }

    public function old_index() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        
    	//google login url
        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data['industries'] = $this->Common_Model->industry_lists();
        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;
        $ad_list = $this->Jobpost_Model->advertise_lists($current_lat,$current_long);
        if(!empty($ad_list)){
            $x=0;
            foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']             

                      ];
            $x++;
            }
        }else{
            $ad_lists1 = [];
        }
        $data['ad_list'] = $ad_lists1;
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['userlist'] = $this->Common_Model->user_list();
        $data['popcat'] = $this->Common_Model->popular_category();
        //echo $this->db->last_query();die;
        $data['getJobs'] = $this->Common_Model->getJobs();
        $data['hiredjobs'] = $this->Jobpost_Model->job_fetch_hired();
        $data['openings'] = $this->Jobpost_Model->job_openings();
        
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();

        $data["states"] = $this->User_Model->getownstate();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            }   
            $data['profileDetail'] = $this->fetchUserSingleData($userSess['id']);
            $data['featuredJob'] = $this->getfeauredjoblistingss1($userSess['id']);
            $data['recentJob'] = $this->getrecentjoblistingss1($userSess['id']);
            $data['nearJob'] = $this->nearJobs1($userSess['id'],$current_lat,$current_long);
            $data['checkResume'] = $this->User_Model->resume_single($userSess['id']);
            $jobcountfetchs = $this->Jobpost_Model->group_locateHome($userSess['id'],$expfilter);
            //echo $this->db->last_query();die;
            $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

            $jobcountfetchs = $returnnewarray;
            //echo $this->db->last_query();die;
        } else{
            $data['featuredJob'] = $this->getfeauredjoblistings($current_lat, $current_long);
            $data['recentJob'] = $this->getRecentjoblisting($current_lat, $current_long);
            $data['nearJob'] = $this->nearJobs($current_lat, $current_long);
            //print_r($data['nearJob']);die;
            $jobcountfetchs = $this->Jobpost_Model->jobfetchhomeall('Noida');
            $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

            $jobcountfetchs = $returnnewarray;
        }

        //Get Map jobs
        /*$ip = $_SERVER['REMOTE_ADDR'];
        $location = file_get_contents('http://ip-api.com/json/' . $ip);
        $data_loc = json_decode($location);*/
        
        //$Address = $data_loc->city;

        
        
        
        if ($jobcountfetchs) {
            $xx = 1;
            foreach ($jobcountfetchs as $jobcountfetch) {
                $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                //print_r($jobTop);       
                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $one = (string)$jobTop[0]['picks_id'];
                        $dataJobList[0] = "$one";
                    } else{
                        $dataJobList[0] = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $two = (string)$jobTop[1]['picks_id'];
                        $dataJobList[1] = "$two";
                    } else {
                        $dataJobList[1] = "";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $three = (string)$jobTop[2]['picks_id'];
                        $dataJobList[2] = "$three";
                    } else{
                        $dataJobList[2] = "";
                    }
                } else {
                    $dataJobList[0] = "";
                    $dataJobList[1] = "";
                    $dataJobList[2] = "";
                }
                if(!empty($dataJobList[0])){
                    if($dataJobList[0]=='1'){
                        $url = base_url()."markericon/bonus.png";
                    }else if($dataJobList[0]=='2'){
                        $url = base_url()."markericon/j_free_food.png";
                    }else if($dataJobList[0]=='3'){
                        $url = base_url()."markericon/j_dependent_hmo.png";
                    }else if($dataJobList[0]=='4'){
                        $url = base_url()."markericon/j_day_1_hmo.png";
                    }else if($dataJobList[0]=='5'){
                        $url = base_url()."markericon/j_dayshift.png";
                    }else if($dataJobList[0]=='6'){
                        $url = base_url()."markericon/j_14th_pay.png";
                    }
                }

                if(!empty($dataJobList[1])){
                    if($dataJobList[1]=='1'){
                        $url = base_url()."markericon/bonus.png";
                    }else if($dataJobList[1]=='2'){
                        $url = base_url()."markericon/j_free_food.png";
                    }else if($dataJobList[1]=='3'){
                        $url = base_url()."markericon/j_dependent_hmo.png";
                    }else if($dataJobList[1]=='4'){
                        $url = base_url()."markericon/j_day_1_hmo.png";
                    }else if($dataJobList[1]=='5'){
                        $url = base_url()."markericon/j_dayshift.png";
                    }else if($dataJobList[1]=='6'){
                        $url = base_url()."markericon/j_14th_pay.png";
                    }
                }

                if(!empty($dataJobList[2])){
                    if($dataJobList[2]=='1'){
                        $url = base_url()."markericon/bonus.png";
                    }else if($dataJobList[2]=='2'){
                        $url = base_url()."markericon/j_free_food.png";
                    }else if($dataJobList[2]=='3'){
                        $url = base_url()."markericon/j_dependent_hmo.png";
                    }else if($dataJobList[2]=='4'){
                        $url = base_url()."markericon/j_day_1_hmo.png";
                    }else if($dataJobList[2]=='5'){
                        $url = base_url()."markericon/j_dayshift.png";
                    }else if($dataJobList[2]=='6'){
                        $url = base_url()."markericon/j_14th_pay.png";
                    }
                }
                
                if(count($jobTop)>1){
                    $url = base_url()."markericon/j_hot_job.png";
                }

                if(empty($dataJobList[0]) && empty($dataJobList[1]) && empty($dataJobList[2])){
                    $url = base_url()."markericon/maps.png";
                }
                if ($this->session->userdata('usersess')) {
                    $urll = base_url()."dashboard/jobLists?lat=".$jobcountfetch['latitude']."&long=".$jobcountfetch['longitude'];
                }else{
                     $urll = base_url()."user/jobLists?lat=".$jobcountfetch['latitude']."&long=".$jobcountfetch['longitude'];
                }    
                if($jobcountfetch["salary"]>0)
                {
                    $salary = ($jobcountfetch["salary"]/1000).'K';
                }else{
                    $salary = '<img src="https://jobyoda.com/webfiles/img/peso_green.png" width="10px">';
                } 
                if($jobcountfetch['totaljobs']==1){
                    $inform = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'"><span style="color:#32aa60;">' .$jobcountfetch['totaljobs'] . ' Job'.'<br>'.'   '.$salary.' </span></a></span>';
                }else{
                    $inform = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'"><span style="color:#32aa60;">' .$jobcountfetch['totaljobs'] . ' Jobs'.'<br>'.'   '.$salary.'</span> </a></span>';
                }

                 
                $dataget[$xx] = ["info" => $inform, "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], "jobid" => $jobcountfetch['id'], "url" => $url];
                $xx++;
            }
         $dataget1 = json_encode($dataget);
            $data['jobcount'] = $dataget;
        } else {
            $data['jobcount'] = [];
        }
        $testimonials = $this->Jobpost_Model->testimonials_fetch();
        if ($testimonials) {
            $data['testimonial'] = $testimonials;
        } else {
            $data['testimonial'] = [];
        }
        //print_r($data);die;
        $this->load->view('home', $data);
    }

    public function getownstate() {
        $data = $this->input->post();
        $result = $this->User_Model->getowncity($data['state']);
        if ($result) {
            echo "<option value=''>Select City</option>";
            foreach ($result as $resultcity) {
                echo "<option value='" . $resultcity['city'] . "'>" . $resultcity['city'] . "</option>";
            }
        } else {
            echo "<option value=''>Select City</option>";
        }  
    }


    public function fetchcompany() {
        $searchTerm = $this->input->post('keyword');
        if(!empty($searchTerm)){
        	 $company_list = $this->Common_Model->companyname_lists($searchTerm);
        if (!empty($company_list)) {
            ?>
            <ul id="country-list">
            <?php
            foreach ($company_list as $company_lists) { ?>
            <?php
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    ?>
                    <li onClick="selectCountry('<?php echo $company_lists["cname"]; ?>');"><?php echo $company_lists["cname"]; ?></li>
                    <?php
                }
            }?>
            </ul>
        <?php
        }
        }
       
    }

    public function usercontact(){
        $userData = $this->input->post();
        
        $data = ["name"=>$userData['fname'], "email"=> $userData['email'], 'phone'=>$userData['phone'], "message"=>$userData['message']];
        $contact_inserted =  $this->User_Model->insert_contact($data);
        $contact_inserted = true;
        if($contact_inserted) {
            $this->session->set_userdata('contactsuccessmsg', "Submitted Successfully.");
            redirect("contact");
        }
        
    }

    public function login() {
        $userData = $this->input->post();
        $password = $userData['password'];
        //echo $password; die;
        $email = $userData['email'];
        $data = ["email" => $userData['email']];
        
        if (empty($email)) {
         echo $error = 'Please enter your email.';
         
        //$error = ["error" => "Please enter your email."];
        //echo json_encode($error);

        } else if (empty($password)) {
            echo $error = 'Please enter your password.';
        } else {
            $userCheck = $this->User_Model->user_login($data);
            
            if ($userCheck) {
                $getPass = $userCheck[0]['password'];
                $vPass = $this->encryption->decrypt($getPass);
                //echo $vPass;die;
                if ($password == $vPass) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userCheck[0]['id']);
                    
                    if ($getCompleteness) {
                        
                        if ($getCompleteness[0]['signup'] == 0) {
                            $profileComplete = 14;
                            $comProfile = ["signup" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userCheck[0]['id']);
                        }
                    } else {
                        $profileComplete = 14;
                        $comProfile = ["user_id" => $userCheck[0]['id'], "signup" => $profileComplete];
                        $this->User_Model->insert_profileComplete($comProfile);
                    }
                    $getData = $this->fetchUserSingleData($userCheck[0]['id']);
                    $getCompletenes = $this->fetchUserCompleteData($userCheck[0]['id']);
                    $email = $userData['email'];
                    $remeber = $this->input->post('rememberval');
                    
                    if ($remeber == 1) {
                        $hour = time() + 3600 * 24 * 30;
                        setcookie('emaill', $email, $hour);
                        setcookie('passwordd', $vPass, $hour);
                        setcookie('checkval', $remeber, $hour);
                    }
                    $this->session->set_userdata('usersess', $getData);
                    $success = ["success" => "logindone"];
                    echo json_encode($success);
                    die;
                } else {
                    echo $error = 'Password is incorrect.';
                    die;
                }
            } else {
                echo $error = 'Email id is incorrect.';
                die;
            }
        }
    }
    
    public function pushweb() {
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $notification_list = $this->Common_Model->getnotification($userTokenCheck[0]['id']);
            if (!empty($notification_list)) {
                foreach ($notification_list as $notification_lists) {
                    $companyName = $this->Jobpost_Model->company_detail_fetch($notification_lists['recruiter_id']);
                    if (!empty($companyName[0]['cname'])) {
                        $companynamee = $companyName[0]['cname'];
                    } else {
                        $companynamee = "";
                    }
                    /*$data['notification_listss'][] = [
                          "id" => $notification_lists['id'],
                          "notification" => $notification_lists['notification'],
                          "status" => $notification_lists['status'],
                          "recruiter_id" => $notification_lists['recruiter_id'],          
                          "job_status" =>   $notification_lists['job_status'],
                          "companyname" =>  $companynamee,
                          "date" => date("d-M-Y h:i:s", strtotime($notification_lists['createdon'])),          
                    
                      ];*/
                    echo "1#" . $notification_lists['id'] . '#' . $companynamee . '#' . $notification_lists['notification'] . '#' . date("d-M-Y h:i:s", strtotime($notification_lists['createdon']));
                }
            }
        }
    }
    public function dashboard() {
        $userSess = $this->session->userdata('usersess');
        if (!empty($userSess)) {
            $dataToken = ["id" => $userSess['id']];
            $userInfo = $this->User_Model->user_match($dataToken);
            $data['profileDetail'] = $this->fetchUserSingleData($userInfo[0]['id']);
        }
        $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
        if($notification_list){
            $data['badge'] = count($notification_list);
         }
        $data['getCompletenes'] = $this->fetchUserCompleteData($userInfo[0]['id']);
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['industrylists'] = $this->Common_Model->industry_lists();
        $this->load->view('dashboard', $data);
    }

    public function signup() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }

        $userData = $this->input->post();

        if ($userData['signupType'] == "normal") {

            $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[70]|callback_webemail_check');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[4]|is_unique[user.phone]');
            $this->form_validation->set_rules('country_code', 'Country Code', 'trim|required');
            $this->form_validation->set_rules('exp_year', 'Years of experience', 'trim|required');
            $this->form_validation->set_rules('exp_month', 'Months of experience', 'trim|required');
            $this->form_validation->set_rules('education', 'Education', 'trim|required');
            $this->form_validation->set_rules('location', 'Location', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('intrested', 'Intrested In', 'callback_check_default');
            $this->form_validation->set_rules('nationality', 'Nationality', 'trim|required');
            $this->form_validation->set_rules('superpower', 'Superpower', 'trim|required');
            $this->form_validation->set_rules('pass', 'password', 'trim|required|callback_valid_password');
            $this->form_validation->set_rules('cpass', 'confirm password', 'trim|required|matches[pass]');
            $this->form_validation->set_rules('option1', 'Privacy Policy', 'trim|required');
            
            if ($this->form_validation->run() == FALSE) {

                $company_list = $this->Common_Model->company_lists();
                if ($company_list) {
                    $x = 0;
                    foreach ($company_list as $company_lists) {
                        $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                        if ($checkjob) {
                            $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                            $x++;
                        }
                    }
                } else {
                    $company_lists1 = [];
                }
                $data['companynamelist'] = $company_lists1;
                $data['category_list'] = $this->Common_Model->category_lists();
                $data['level_list'] = $this->Common_Model->level_lists();
                $data['popcat'] = $this->Common_Model->popular_category();
                $data['userlist'] = $this->Common_Model->user_list();
                $data["phonecodes"] = $this->Common_Model->phonecode_lists();
                $data["nations"] = $this->Common_Model->nation_lists();
                $data["powers"] = $this->Common_Model->power_lists();
                $data['signuperrors'] = $this->form_validation->error_array();
                $data['userData'] = $userData;
                $data["states"] = $this->User_Model->getownstate();
                $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);

                if ($this->session->userdata('usersess')) {
                    $userSess = $this->session->userdata('usersess');  
                    $tokendata = ['id'=>$userSess['id']];
                    $userID = $userSess['id'];

                    $userData = $this->User_Model->token_match($tokendata);
                    $expmonth = $userData[0]['exp_month'];
                    $expyear = $userData[0]['exp_year'];

                    $expmonth = $exp_month;
                    $expyear = $exp_year;

                    if($expyear == 0 && $expmonth == 0) {
                        $expfilter = "1";        
                    } else if($expyear == 0 && $expmonth < 6) {
                        $expfilter = "2";
                    } else if($expyear < 1 && $expmonth >= 6) {
                        $expfilter = "3";  
                    } else if($expyear < 2 && $expyear >= 1) {
                        $expfilter = "4";
                    } else if($expyear < 3 && $expyear >= 2) {
                        $expfilter = "5";
                    } else if($expyear < 4 && $expyear >= 3) {
                        $expfilter = "6";  
                    } else if($expyear < 5 && $expyear >= 4) {
                        $expfilter = "7";  
                    } else if($expyear < 6 && $expyear >= 5) {
                        $expfilter = "8";  
                    } else if($expyear < 7 && $expyear >= 6) {
                        $expfilter = "9";  
                    } else if($expyear >= 7) { 
                        $expfilter = "10";
                    } else {
                        $expfilter = "";
                    }
                } else {
                    $expfilter = "";
                    $userID = 0;
                }

                $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);

                $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);

                $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);

                $data["nearByJobs"] = $this->getnearByJobs($userID, $expfilter, $current_lat, $current_long);

                $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter);
                $data["workFromhomeJobs"] = $featuredTopPicks['workfromhome'];
                $data["bonusJobs"] = $featuredTopPicks['bonus'];
                $data["shiftJobs"] = $featuredTopPicks['shiftjobs'];
                $data["monthpayJobs"] = $featuredTopPicks['monthpay'];
                $data["day1hmoJobs"] = $featuredTopPicks['day1hmo'];
                $data["freefoodJobs"] = $featuredTopPicks['freefood'];
                //$data["freefoodJobs"] = $featuredTopPicks['freefood'];

                $data["hotJobs"] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long);

                $data["itJobs"] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long);

                $data["leadershipJobs"] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long);

                $data["instantJobs"] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long);

                //Fetch videos
                $videosArray = array();
                $fetchvideos = $this->Newpoints_Model->fetchvideos();
                foreach($fetchvideos as $fetchvideo) {
                    $videosArray[] = ["id"=>$fetchvideo['id'],"video"=>$fetchvideo['video'],"banner"=>$fetchvideo['thumbnail'],"title"=>$fetchvideo['title'],"desc"=>$fetchvideo['description'],"posted"=>date("d F Y", strtotime($fetchvideo['updated_at']))];
                }
                $data["videoListings"] = $videosArray;


                $this->load->view("homepage", $data);
            
            } else {
                
                $hash = $this->encryption->encrypt($userData['pass']);
                $token = $this->User_Model->randomstring();
                $randomnumber = rand(9, 9999);
                $company_list = $this->Common_Model->company_lists();
                if ($company_list) {
                    $x = 0;
                    foreach ($company_list as $company_lists) {
                        $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                        if ($checkjob) {
                            $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                            $x++;
                        }
                    }
                } else {
                    $company_lists1 = [];
                }
        	   
                $data['loginURL'] = $this->google->loginURL();
                $data['authUrl'] = $this->facebook->login_url();
                $data['companynamelist'] = $company_lists1;
                $data['category_list'] = $this->Common_Model->category_lists();
                $data['level_list'] = $this->Common_Model->level_lists();
                $data['popcat'] = $this->Common_Model->popular_category();
                $data['featuredJob'] = $this->getfeauredjoblistings($current_lat, $current_long);
                $data['recentJob'] = $this->getRecentjoblisting($current_lat, $current_long);
                $data["phonecodes"] = $this->Common_Model->phonecode_lists();
                $data["nations"] = $this->Common_Model->nation_lists();
                $data["powers"] = $this->Common_Model->power_lists();
                $data["states"] = $this->User_Model->getownstate();
                $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);

                if ($this->session->userdata('usersess')) {
                    $userSess = $this->session->userdata('usersess');  
                    $tokendata = ['id'=>$userSess['id']];
                    $userID = $userSess['id'];

                    $userData = $this->User_Model->token_match($tokendata);
                    $expmonth = $userData[0]['exp_month'];
                    $expyear = $userData[0]['exp_year'];

                    $expmonth = $exp_month;
                    $expyear = $exp_year;

                    if($expyear == 0 && $expmonth == 0) {
                        $expfilter = "1";        
                    } else if($expyear == 0 && $expmonth < 6) {
                        $expfilter = "2";
                    } else if($expyear < 1 && $expmonth >= 6) {
                        $expfilter = "3";  
                    } else if($expyear < 2 && $expyear >= 1) {
                        $expfilter = "4";
                    } else if($expyear < 3 && $expyear >= 2) {
                        $expfilter = "5";
                    } else if($expyear < 4 && $expyear >= 3) {
                        $expfilter = "6";  
                    } else if($expyear < 5 && $expyear >= 4) {
                        $expfilter = "7";  
                    } else if($expyear < 6 && $expyear >= 5) {
                        $expfilter = "8";  
                    } else if($expyear < 7 && $expyear >= 6) {
                        $expfilter = "9";  
                    } else if($expyear >= 7) { 
                        $expfilter = "10";
                    } else {
                        $expfilter = "";
                    }
                } else {
                    $expfilter = "";
                    $userID = 0;
                }

                $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
                $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);
                $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);
                $data["nearByJobs"] = $this->getnearByJobs($userID, $expfilter, $current_lat, $current_long);
                $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter);
                $data["workFromhomeJobs"] = $featuredTopPicks['workfromhome'];
                $data["bonusJobs"] = $featuredTopPicks['bonus'];
                $data["shiftJobs"] = $featuredTopPicks['shiftjobs'];
                $data["monthpayJobs"] = $featuredTopPicks['monthpay'];
                $data["day1hmoJobs"] = $featuredTopPicks['day1hmo'];
                $data["freefoodJobs"] = $featuredTopPicks['freefood'];

                $data["hotJobs"] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long);
                $data["itJobs"] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long);
                $data["leadershipJobs"] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long);
                $data["instantJobs"] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long);

                //Fetch videos
                $videosArray = array();
                $fetchvideos = $this->Newpoints_Model->fetchvideos();
                foreach($fetchvideos as $fetchvideo) {
                    $videosArray[] = ["id"=>$fetchvideo['id'],"video"=>$fetchvideo['video'],"banner"=>$fetchvideo['thumbnail'],"title"=>$fetchvideo['title'],"desc"=>$fetchvideo['description'],"posted"=>date("d F Y", strtotime($fetchvideo['updated_at']))];
                }
                $data["videoListings"] = $videosArray;

                $data1 = ['socialToken'=>'', 'name' => ucwords(strtolower($userData['fname'])) . ' ' . ucwords(strtolower($userData['lname'])), 'email' => $userData['email'], 'phone' => $userData['phone'], 'country_code' => $userData['country_code'],'exp_year' => $userData['exp_year'],'exp_month' => $userData['exp_month'],'education' => $userData['education'],'location' => $userData['location'],'nationality' => $userData['nationality'],'superpower' => $userData['superpower'], 'password' => $hash, 'type' => "normal", 'token' => $token, 'status' => 1, 'verify_status' => 0, 'verify_code' => $randomnumber, 'referral_used'=>$userData['refer_code'], "state"=> $userData['state'], "city"=> $userData['city'], "jobsInterested"=> implode(',',$userData['intrested'])];

                $mobileNumber = $userData['country_code'].$userData['phone'];
                $msgege = "Your verification otp is " . $randomnumber;
                
                $this->SendAwsSms($mobileNumber, $msgege);

                $this->session->set_userdata($data1);
                $data['signupsuccesss'] = ["success" => "Please enter your verification code"];
                $this->load->view("homepage", $data);
                
                
            }
        } else if ($userData['signupType'] == "") {
            redirect('user/index');

        } else {
            
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[4]|is_unique[user.phone]');
            $this->form_validation->set_rules('country_code', 'Country Code', 'trim|required');
            $this->form_validation->set_rules('exp_year', 'Years of experience', 'trim|required');
            $this->form_validation->set_rules('exp_month', 'Months of experience', 'trim|required');
            $this->form_validation->set_rules('education', 'Education', 'trim|required');
            $this->form_validation->set_rules('location', 'Location', 'trim|required');
            $this->form_validation->set_rules('state', 'State', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('intrested', 'Intrested In', 'callback_check_default');
            $this->form_validation->set_rules('nationality', 'Nationality', 'trim|required');
            $this->form_validation->set_rules('superpower', 'Superpower', 'trim|required');
            //$this->form_validation->set_rules('pass', 'password', 'trim|required|callback_valid_password');
            //$this->form_validation->set_rules('cpass', 'confirm password', 'trim|required|matches[pass]');
            $this->form_validation->set_rules('option1', 'Privacy Policy', 'trim|required');
            
            if ($this->form_validation->run() == FALSE) {

                $company_list = $this->Common_Model->company_lists();
                if ($company_list) {
                    $x = 0;
                    foreach ($company_list as $company_lists) {
                        $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                        if ($checkjob) {
                            $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                            $x++;
                        }
                    }
                } else {
                    $company_lists1 = [];
                }
                $data['companynamelist'] = $company_lists1;
                $data['category_list'] = $this->Common_Model->category_lists();
                $data['level_list'] = $this->Common_Model->level_lists();
                $data['popcat'] = $this->Common_Model->popular_category();
                $data['userlist'] = $this->Common_Model->user_list();
                $data["phonecodes"] = $this->Common_Model->phonecode_lists();
                $data["nations"] = $this->Common_Model->nation_lists();
                $data["powers"] = $this->Common_Model->power_lists();
                $data['signuperrors'] = $this->form_validation->error_array();
                $data['userData'] = $userData;
                $data["states"] = $this->User_Model->getownstate();
                $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);

                if ($this->session->userdata('usersess')) {
                    $userSess = $this->session->userdata('usersess');  
                    $tokendata = ['id'=>$userSess['id']];
                    $userID = $userSess['id'];

                    $userData = $this->User_Model->token_match($tokendata);
                    $expmonth = $userData[0]['exp_month'];
                    $expyear = $userData[0]['exp_year'];

                    $expmonth = $exp_month;
                    $expyear = $exp_year;

                    if($expyear == 0 && $expmonth == 0) {
                        $expfilter = "1";        
                    } else if($expyear == 0 && $expmonth < 6) {
                        $expfilter = "2";
                    } else if($expyear < 1 && $expmonth >= 6) {
                        $expfilter = "3";  
                    } else if($expyear < 2 && $expyear >= 1) {
                        $expfilter = "4";
                    } else if($expyear < 3 && $expyear >= 2) {
                        $expfilter = "5";
                    } else if($expyear < 4 && $expyear >= 3) {
                        $expfilter = "6";  
                    } else if($expyear < 5 && $expyear >= 4) {
                        $expfilter = "7";  
                    } else if($expyear < 6 && $expyear >= 5) {
                        $expfilter = "8";  
                    } else if($expyear < 7 && $expyear >= 6) {
                        $expfilter = "9";  
                    } else if($expyear >= 7) { 
                        $expfilter = "10";
                    } else {
                        $expfilter = "";
                    }
                } else {
                    $expfilter = "";
                    $userID = 0;
                }

                $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);

                $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);

                $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);

                $data["nearByJobs"] = $this->getnearByJobs($userID, $expfilter, $current_lat, $current_long);

                $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter);
                $data["workFromhomeJobs"] = $featuredTopPicks['workfromhome'];
                $data["bonusJobs"] = $featuredTopPicks['bonus'];
                $data["shiftJobs"] = $featuredTopPicks['shiftjobs'];
                $data["monthpayJobs"] = $featuredTopPicks['monthpay'];
                $data["day1hmoJobs"] = $featuredTopPicks['day1hmo'];
                $data["freefoodJobs"] = $featuredTopPicks['freefood'];
                //$data["freefoodJobs"] = $featuredTopPicks['freefood'];

                $data["hotJobs"] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long);

                $data["itJobs"] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long);

                $data["leadershipJobs"] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long);

                $data["instantJobs"] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long);

                //Fetch videos
                $videosArray = array();
                $fetchvideos = $this->Newpoints_Model->fetchvideos();
                foreach($fetchvideos as $fetchvideo) {
                    $videosArray[] = ["id"=>$fetchvideo['id'],"video"=>$fetchvideo['video'],"banner"=>$fetchvideo['thumbnail'],"title"=>$fetchvideo['title'],"desc"=>$fetchvideo['description'],"posted"=>date("d F Y", strtotime($fetchvideo['updated_at']))];
                }
                $data["videoListings"] = $videosArray;

                $this->load->view("homepage", $data);
            
            } else {

                $hash = $this->encryption->encrypt($userData['pass']);
                $token = $this->User_Model->randomstring();

                    $randomnumber = rand(9, 9999);
                    $company_list = $this->Common_Model->company_lists();
                    if ($company_list) {
                        $x = 0;
                        foreach ($company_list as $company_lists) {
                            $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                            if ($checkjob) {
                                $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                                $x++;
                            }
                        }
                    } else {
                        $company_lists1 = [];
                    }
                    
                    $data['loginURL'] = $this->google->loginURL();
                    $data['authUrl'] = $this->facebook->login_url();
                    $data['companynamelist'] = $company_lists1;
                    $data['category_list'] = $this->Common_Model->category_lists();
                    $data['level_list'] = $this->Common_Model->level_lists();
                    $data['popcat'] = $this->Common_Model->popular_category();
                    $data['featuredJob'] = $this->getfeauredjoblistings($current_lat, $current_long);
                    $data['recentJob'] = $this->getRecentjoblisting($current_lat, $current_long);
                    $data["phonecodes"] = $this->Common_Model->phonecode_lists();
                    $data["nations"] = $this->Common_Model->nation_lists();
                    $data["powers"] = $this->Common_Model->power_lists();
                    $data["states"] = $this->User_Model->getownstate();
                    $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);

                    if ($this->session->userdata('usersess')) {
                        $userSess = $this->session->userdata('usersess');  
                        $tokendata = ['id'=>$userSess['id']];
                        $userID = $userSess['id'];

                        $userData = $this->User_Model->token_match($tokendata);
                        $expmonth = $userData[0]['exp_month'];
                        $expyear = $userData[0]['exp_year'];

                        $expmonth = $exp_month;
                        $expyear = $exp_year;

                        if($expyear == 0 && $expmonth == 0) {
                            $expfilter = "1";        
                        } else if($expyear == 0 && $expmonth < 6) {
                            $expfilter = "2";
                        } else if($expyear < 1 && $expmonth >= 6) {
                            $expfilter = "3";  
                        } else if($expyear < 2 && $expyear >= 1) {
                            $expfilter = "4";
                        } else if($expyear < 3 && $expyear >= 2) {
                            $expfilter = "5";
                        } else if($expyear < 4 && $expyear >= 3) {
                            $expfilter = "6";  
                        } else if($expyear < 5 && $expyear >= 4) {
                            $expfilter = "7";  
                        } else if($expyear < 6 && $expyear >= 5) {
                            $expfilter = "8";  
                        } else if($expyear < 7 && $expyear >= 6) {
                            $expfilter = "9";  
                        } else if($expyear >= 7) { 
                            $expfilter = "10";
                        } else {
                            $expfilter = "";
                        }
                    } else {
                        $expfilter = "";
                        $userID = 0;
                    }

                    $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
                    $data["citySearchs"] = $this->getcitysearch($userID, $expfilter, $current_lat, $current_long);
                    $data["expertises"] = $this->getexpertise($userID, $expfilter, $current_lat, $current_long);
                    $data["nearByJobs"] = $this->getnearByJobs($userID, $expfilter, $current_lat, $current_long);
                    $featuredTopPicks = $this->getFeaturedTopPicks($userID, $current_lat, $current_long, $expfilter);
                    $data["workFromhomeJobs"] = $featuredTopPicks['workfromhome'];
                    $data["bonusJobs"] = $featuredTopPicks['bonus'];
                    $data["shiftJobs"] = $featuredTopPicks['shiftjobs'];
                    $data["monthpayJobs"] = $featuredTopPicks['monthpay'];
                    $data["day1hmoJobs"] = $featuredTopPicks['day1hmo'];
                    $data["freefoodJobs"] = $featuredTopPicks['freefood'];

                    $data["hotJobs"] = $this->gethotJobs($userID, $expfilter, $current_lat, $current_long);
                    $data["itJobs"] = $this->getitJobs($userID, $expfilter, $current_lat, $current_long);
                    $data["leadershipJobs"] = $this->getleadershipJobs($userID, $expfilter, $current_lat, $current_long);
                    $data["instantJobs"] = $this->getinstantJobs($userID, $expfilter, $current_lat, $current_long);

                    //Fetch videos
                    $videosArray = array();
                    $fetchvideos = $this->Newpoints_Model->fetchvideos();
                    foreach($fetchvideos as $fetchvideo) {
                        $videosArray[] = ["id"=>$fetchvideo['id'],"video"=>$fetchvideo['video'],"banner"=>$fetchvideo['thumbnail'],"title"=>$fetchvideo['title'],"desc"=>$fetchvideo['description'],"posted"=>date("d F Y", strtotime($fetchvideo['updated_at']))];
                    }
                    $data["videoListings"] = $videosArray;

                $userfsess = $this->session->userdata('usersocialsession');

                $data1 = ['socialToken'=>$userfsess['socialToken'], 'name' => ucwords(strtolower($userData['fname'])) . ' ' . ucwords(strtolower($userData['lname'])), 'email' => $userData['email'], 'phone' => $userData['phone'], 'country_code' => $userData['country_code'], 'password' => $hash, 'type' => $userfsess['type'], 'referral_used'=>$userData['refer_code'],'exp_year' => $userData['exp_year'],'exp_month' => $userData['exp_month'],'education' => $userData['education'],'location' => $userData['location'],'nationality' => $userData['nationality'],'superpower' => $userData['superpower'], 'status' => 1, 'token' => $token, 'verify_status' => 0, 'verify_code' => $randomnumber, "state"=> $userData['state'], "city"=> $userData['city'], "jobsInterested"=> implode(',',$userData['intrested'])];

                $mobileNumber = $userData['country_code'].$userData['phone'];
                $msgege = "Your verification otp is " . $randomnumber;
                
                $this->SendAwsSms($mobileNumber, $msgege);

                $this->session->set_userdata($data1);
                 $this->session->unset_userdata('userfsess');
                $data['signupsuccesss'] = ["success" => "Please enter your verification code"];

                $this->load->view("homepage", $data);
                
                //$userfsess = $this->session->userdata('userfsess');
                //$res = $this->User_Model->user_update1($data, $userfsess['token']);

                //echo $this->db->last_query();die;
                // if (!empty($res) && $res == "true") {
                //     $getData = $this->fetchUserSingleData1($userData['email']);
                //     $this->session->set_userdata('usersess', $getData);
                //     $this->session->unset_userdata('userfsess');
                //     if($previous_url = $this->session->userdata('previous_url')) {
                //         redirect($previous_url);
                //     } else {
                //         redirect('user/homeafterlogin');
                //     }
                // } else {
                //     if($previous_url = $this->session->userdata('previous_url')) {
                //         redirect($previous_url);
                //     } else {
                //         redirect('');
                //     }
                // }
            }
        }
    }

    public function webemail_check($email) {

        $emailMatch = $this->User_Model->checkwebemail($email);

        if ($emailMatch) {

            if($emailMatch[0]['type'] == "normal") {

                $this->form_validation->set_message('webemail_check', 'This email already exists');
                return FALSE;

            } else if($emailMatch[0]['type'] == "gmail" || $emailMatch[0]['type'] == "google" || $emailMatch[0]['type'] == "facebook") {

                if(!empty($emailMatch[0]['education']) && !empty($emailMatch[0]['superpower']) && !empty($emailMatch[0]['location'])) {

                    $this->form_validation->set_message('webemail_check', 'This email already exists with social account');
                    return FALSE;
                } else {

                    return TRUE;       
                }
            } else {

                $this->form_validation->set_message('webemail_check', 'This email already exists');
                return FALSE;
            }
        } else {
            return TRUE;
        }
    }

    function check_default()
    {
        $choice = $this->input->post("intrested");
        if(is_null($choice))
        {
            $choice = array();
        }
        $travel_cat = implode(',', $choice);

        if($travel_cat != '')
            return true;
        else
            return false;   
    }


    public function sendSms($to, $sms_message) {
        $this->load->library('twilio');
        //$sms_sender = trim($this->input->post('sms_sender'));
        //$sms_reciever = $this->input->post('sms_recipient');
        //$sms_message = trim($this->input->post('sms_message'));
        //$from = '+'.$sms_sender; //trial account twilio number
        //$to = '+'.$sms_reciever; //sms recipient number

        $from = 'Jobyoda';
        $to = $to;
        $response = $this->twilio->sms($from, $to, $sms_message);
        //print_r($response);
        if ($response->IsError) {
            return 'Sms Has been Not sent';
        } else {
            return 'Sms Has been sent';
        }
    }

    public function SendAwsSms($to,$sms_message){
      require 'vendor/autoload.php';
      //echo 'hi';die;
        $params = array(
                'credentials'=> array(
                'key'=>'AKIA2PEGBF6OC7RXCBWM',
                'secret'=>'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',
            ),
            'region'=>'us-west-2',
            'version'=>'latest'
        );
        $sns = new \Aws\Sns\SnsClient($params);
        $args = array(
            "SenderID"=>"JobYoDA",
            "SMSType"=>"Transactional",
            "Message"=>$sms_message,
            "PhoneNumber"=>$to,
        );
        //print_r($args);die;
        $result = $sns->publish($args);
        //print_r($result);die;
        return true;
    }

    public function forgetpassword() {
        $userData = $this->input->post('email');
        $data = ["email" => $userData];
        $userTokenCheck = $this->User_Model->email_match($data);
        if ($userTokenCheck) {
            $email = $userTokenCheck[0]['email'];
            $name = $userTokenCheck[0]['name'];
            $this->load->library('email');
            /*$this->email->from("ayush.jain@mobulous.com", "JobYoda");
            $this->email->to($email);
            $this->email->subject('JobYodha - Forgot Password Link');*/
            $token = $this->getToken();
            $data['full_name'] = ucfirst($name);
                    $data['token'] = $token;
                    $data['user_id'] = $userTokenCheck[0]['id'];
                    $msg = $this->load->view('passwordemailweb',$data,TRUE);
                    $config=array(
                    'charset'=>'utf-8',
                    'wordwrap'=> TRUE,
                    'mailtype' => 'html'
                    );

                    $this->email->initialize($config);
                    $this->email->from("help@jobyoda.com", "JobYoDA");
                    $this->email->to($email);
                    $this->email->subject('JobYoDA - Forgot Password Link');
            /*$msg = "Dear " . $name;
            $msg.= ", Please click on the below link to change your account password: ";
            $msg.= base_url() . "user/forgotpasswordview/" . $userTokenCheck[0]['token'];
            $msg.= ". Your verification code is " . $token;*/
            $this->email->message($msg);
            // send email
            if ($this->email->send()) {
                $forgotPassCheck = $this->User_Model->forgotPass_check($userTokenCheck[0]['id']);
                //print_r($forgotPassCheck);die;
                if ($forgotPassCheck) {
                    $vCode = ["verifyCode" => $token];
                    $this->User_Model->forgotPass_update($vCode, $userTokenCheck[0]['id']);
                } else {
                    $vCode = ["user_id" => $userTokenCheck[0]['id'], "verifyCode" => $token];
                    $this->User_Model->forgotPass_insert($vCode);
                }
                echo $success = "We have sent you a link and verification code to reset your password.";
            } else {
                echo $failure = "Please enter your email.";
            }
        } else {
            echo $failure = "Plese enter registered email id.";
        }
    }
    public function forgotpasswordview() {
        $data['uri_data'] = $this->uri->segment(2);
        $this->load->view("forgetview", $data);
    }
    public function forgotpasswordviewapp() {
        $data['uri_data'] = $this->uri->segment(2);
        $this->load->view("forgetviewapp", $data);
    }
    public function show() {
        $userData = $this->input->post();
        $uri_data = $this->input->post("uri_data");
        $new_pass1 = $this->input->post("new_pass");
        $con_pass1 = $this->input->post("con_pass");
        $verify_code = $this->input->post("verify_code");
        $this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|callback_valid_password');
        $this->form_validation->set_rules('con_pass', 'Confirm Password', 'trim|required|matches[new_pass]');
        $this->form_validation->set_rules('verify_code', 'Verify Code', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['signuperrors'] = $this->form_validation->error_array();
            $data['uri_data'] = base64_encode($uri_data);
            $data['userData'] = $userData;
            $this->load->view('forgetview',$data);
        } else {
            /*
            if($new_pass1 == "" || $con_pass1 == "") {
            $this->session->set_flashdata('msgg','<div class="alert alert-danger">Plese enter your password</div>');
            redirect('user/forgotpasswordview');
            }   */
            /*if ($new_pass1 != $con_pass1) {
                $this->session->set_flashdata('msgg', '<div class="alert alert-danger">New password must be equal to confirm password</div>');
                redirect('user/forgotpasswordviewapp');
            }*/
            $conToken = array('id' => $uri_data);
            $tokenmatch = $this->User_Model->token_match($conToken);
            if (!empty($tokenmatch)) {
                $condVerifycode = array('verifyCode' => $verify_code);
                $forgotPassCheck = $this->User_Model->forgotPass_check($tokenmatch[0]['id']);
                if ($forgotPassCheck && ($forgotPassCheck[0]['verifyCode'] == $verify_code)) {
                    $new_pass = $this->encryption->encrypt($new_pass1);
                    $connd = array("id" => $uri_data);
                    $updatedata = array('password' => $new_pass);
                    $update = $this->User_Model->update_data('user', $connd, $updatedata);
                    //echo $this->db->last_query();
                    if ($update) {
                        $this->session->set_flashdata('msg', '<div class="alert alert-success">Password has been successfully updated,Please Login here.</div>');
                        redirect('user/forgotpasswordview/'. base64_encode($uri_data));
                    }
                } else {
                    $data['mismatch_err'] = 'Verification code is incorrect.';
                    $data['uri_data'] = base64_encode($uri_data);
                    $data['userData'] = $userData;
                    $this->load->view('forgetview',$data);
                    /*$this->session->set_flashdata('msgg', '<div class="alert alert-danger">Mismatched verification code!!</div>');
                    redirect('user/forgotpasswordviewapp/' . base64_encode($uri_data));*/
                }
            } else {
                $this->session->set_flashdata('msgg', '<div class="alert alert-danger">Token mismatch!!</div>');
                redirect('user/forgotpasswordview/' . base64_encode($uri_data));
            }
        }
    }
    public function showapp() {
        $userData = $this->input->post();
        $uri_data = $this->input->post("uri_data");
        $new_pass1 = $this->input->post("new_pass");
        $con_pass1 = $this->input->post("con_pass");
        $verify_code = $this->input->post("verify_code");
        $this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|callback_valid_password');
        $this->form_validation->set_rules('con_pass', 'Confirm Password', 'trim|required|matches[new_pass]');
        $this->form_validation->set_rules('verify_code', 'Verify Code', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['signuperrors'] = $this->form_validation->error_array();
            $data['uri_data'] = base64_encode($uri_data);
            $data['userData'] = $userData;
            $this->load->view('forgetviewapp',$data);
        } else {
            /*
            if($new_pass1 == "" || $con_pass1 == "") {
            $this->session->set_flashdata('msgg','<div class="alert alert-danger">Plese enter your password</div>');
            redirect('user/forgotpasswordview');
            }   */
            /*if ($new_pass1 != $con_pass1) {
                $this->session->set_flashdata('msgg', '<div class="alert alert-danger">New password must be equal to confirm password</div>');
                redirect('user/forgotpasswordviewapp');
            }*/
            $conToken = array('id' => $uri_data);
            $tokenmatch = $this->User_Model->token_match($conToken);
            if (!empty($tokenmatch)) {
                $condVerifycode = array('verifyCode' => $verify_code);
                $forgotPassCheck = $this->User_Model->forgotPass_check($tokenmatch[0]['id']);
                if ($forgotPassCheck && ($forgotPassCheck[0]['verifyCode'] == $verify_code)) {
                    $new_pass = $this->encryption->encrypt($new_pass1);
                    $connd = array("id" => $uri_data);
                    $updatedata = array('password' => $new_pass);
                    $update = $this->User_Model->update_data('user', $connd, $updatedata);
                    //echo $this->db->last_query();
                    if ($update) {
                        $this->session->set_flashdata('msg', '<div class="alert alert-success">Password has been successfully updated,Please Login here.</div>');
                        redirect('user/forgotpasswordviewapp/'. base64_encode($uri_data));
                    }
                } else {
                    $data['mismatch_err'] = 'Verification code is incorrect.';
                    $data['uri_data'] = base64_encode($uri_data);
                    $data['userData'] = $userData;
                    $this->load->view('forgetviewapp',$data);
                    /*$this->session->set_flashdata('msgg', '<div class="alert alert-danger">Mismatched verification code!!</div>');
                    redirect('user/forgotpasswordviewapp/' . base64_encode($uri_data));*/
                }
            } else {
                $this->session->set_flashdata('msgg', '<div class="alert alert-danger">Token mismatch!!</div>');
                redirect('user/forgotpasswordviewapp/' . base64_encode($uri_data));
            }
        }
    }
    public function changepass() {
        $userSess = $this->session->userdata('usersess');
        $user_id = $userSess['id'];
        $oldpass = $this->input->post('oldpass');
        $newPass = $this->input->post('newpass');
        $confPass = $this->input->post('confpass');
        //$EncryptoldPass     =   $this->encryption->encrypt($oldpassword);
        $Encryptnewpass = $this->encryption->encrypt($newPass);
        $EncryptconfPass = $this->encryption->encrypt($confPass);
        $condition_array = array('id' => $user_id,);
        $lowercase = preg_match('@[a-z]@', $newPass);
        $number    = preg_match('@[0-9]@', $newPass);
        $specialChars = preg_match('@[^\w]@', $newPass);
        if($oldpass == '') {
            echo $error = "<span style='color:red'><center>Please enter old password.</center></span>";
        }
        else if ($newPass == '') {
            echo $error = "<span style='color:red'><center>Please enter new password.</center></span>";
        }else if(!$lowercase || !$number){
            echo $error = "<span style='color:red'><center>New Password should be mixture of alphabets and numeric characters.</center></span>";
        } else if ($confPass == '') {
            echo $error = "<span style='color:red'><center>Please enter confirm password.</center></span>";
        } else {
            $result = $this->User_Model->token_match($condition_array);
            $userPass = $this->encryption->decrypt($result[0]['password']);
            //echo $userPass;die;    
            if($userPass == $oldpass) {
            //print_r($result);die;
            /*if (count($result) == 1) {*/
                $data_array = array("password" => $Encryptnewpass);
                if ($newPass == $confPass) {
                    $update = $this->User_Model->update_data('user', $condition_array, $data_array);
                    echo $success = '<span style="color:green"><center>Password changed successfully.</center></span>';
                } else {
                    echo $error = '<span style="color:red"><center>Password Mismatch.</center></span>';
                }
            } else {
                echo $error = '<span style="color:red"><center>Old Password is incorrect.</center></span>';
            }
        }
    }
    public function otpverification() {
        $mobile_number = $this->input->post('mobile_number');
        $otp = $this->input->post('otp');
        $mobileCheck = $this->User_Model->mobile_check($mobile_number);
        if ($otp == '') {
            echo $error = '<span style="color:red"><center>Please enter verification code!!</center></span>';
        } else if ($mobileCheck[0]['verify_code'] != $otp) {
            echo $error = '<span style="color:red"><center>Please enter correct verification code!!</center></span>';
        } else {
            $condition_array = array('phone' => $mobile_number,);
            $data_array = array('verify_status' => 1);
            $update = $this->User_Model->update_data('user', $condition_array, $data_array);
            echo $success = '<span style="color:green"><center>Yor are successfully verified please login.</center></span>';
        }
    }
    public function verify_User() {

        $mobile_number = $this->input->post('mobile_number');
        $socialToken = $this->session->userdata('socialToken');

        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();

        if(strlen($socialToken) > 0) {

            $emailMatch = $this->User_Model->checkwebemailsocial($this->session->userdata('socialToken'));
            if($emailMatch) {
                
                $data = ['name' => $this->session->userdata('name'), 'email' => $this->session->userdata('email'), 'phone' => $this->session->userdata('phone'), 'country_code' => $this->session->userdata('country_code'), 'password' => $this->session->userdata('password'), 'token' => $this->session->userdata('token'), 'status' => $this->session->userdata('status'),'referral_used'=> $this->session->userdata('referral_used'),'exp_month'=> $this->session->userdata('exp_month'),'exp_year'=> $this->session->userdata('exp_year'),'education'=> $this->session->userdata('education'),'nationality'=> $this->session->userdata('nationality'),'superpower'=> $this->session->userdata('superpower'),'location'=> $this->session->userdata('location'),'state'=> $this->session->userdata('state'),'city'=> $this->session->userdata('city'),'jobsInterested'=> $this->session->userdata('jobsInterested')];
                
                $this->User_Model->user_update($data, $emailMatch[0]['id']);
                $userInserted = $emailMatch[0]['id'];
            
            } else {
                $data = ['name' => $this->session->userdata('name'), 'email' => $this->session->userdata('email'), 'phone' => $this->session->userdata('phone'), 'country_code' => $this->session->userdata('country_code'), 'password' => $this->session->userdata('password'), 'type' => $this->session->userdata('type'), 'token' => $this->session->userdata('token'), 'status' => $this->session->userdata('status'),' referral_used'=> $this->session->userdata('referral_used'),'exp_month'=> $this->session->userdata('exp_month'),'exp_year'=> $this->session->userdata('exp_year'),'education'=> $this->session->userdata('education'),'nationality'=> $this->session->userdata('nationality'),'superpower'=> $this->session->userdata('superpower'),'location'=> $this->session->userdata('location'),'state'=> $this->session->userdata('state'),'city'=> $this->session->userdata('city'),'jobsInterested'=> $this->session->userdata('jobsInterested')];
                
                $userInserted = $this->User_Model->user_insert($data);
            }
            $loginType = "social";
        } else {
            $data = ['name' => $this->session->userdata('name'), 'email' => $this->session->userdata('email'), 'phone' => $this->session->userdata('phone'), 'country_code' => $this->session->userdata('country_code'), 'password' => $this->session->userdata('password'), 'type' => $this->session->userdata('type'), 'token' => $this->session->userdata('token'), 'status' => $this->session->userdata('status'),'referral_used'=> $this->session->userdata('referral_used'),'exp_month'=> $this->session->userdata('exp_month'),'exp_year'=> $this->session->userdata('exp_year'),'education'=> $this->session->userdata('education'),'nationality'=> $this->session->userdata('nationality'),'superpower'=> $this->session->userdata('superpower'),'location'=> $this->session->userdata('location'),'state'=> $this->session->userdata('state'),'city'=> $this->session->userdata('city'),'jobsInterested'=> $this->session->userdata('jobsInterested')];
            
            $userInserted = $this->User_Model->user_insert($data);
            $loginType = "normal";
        }   
            
            if ($userInserted) {
                $getCompleteness = $this->User_Model->check_profileComplete($userInserted);
                if ($getCompleteness) {
                    if ($getCompleteness[0]['signup'] == 0) {
                        $profileComplete = 14;
                        $comProfile = ["signup" => $profileComplete];
                        $this->User_Model->update_profileComplete($comProfile, $userInserted);
                    }
                } else {
                    $profileComplete = 14;
                    $comProfile = ["user_id" => $userInserted, "signup" => $profileComplete];
                    $this->User_Model->insert_profileComplete($comProfile);
                }
                // if($this->session->userdata('previous_url_again')) {
                //     $previous_url = $this->session->userdata('previous_url_again');
                //     $preURL = $previous_url;
                // } else {
                //     $previous_url = $this->session->userdata('previous_url');
                //     $preURL = $previous_url;
                // }
                //echo "1";
                echo json_encode(["success"=>1, "google"=>$data['loginURL'], "facebook"=>$data['authUrl']]);
            } else {   
                echo "2";
            }
        
    }

    public function checkreferral(){
        $refer_code = $this->input->post('refer_code');
        if(!empty($refer_code)){
               $refer_match = $this->User_Model->referral_code($refer_code);
               if(empty($refer_match)){
                echo "1";
               }else{
                echo "2";
               }
        }else{

        }       
    }

    public function facebookSignup() {
        $userData = array();
        if ($this->facebook->is_authenticated()) {
            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = $userProfile['id'];
            $userData['first_name'] = $userProfile['first_name'];
            $userData['last_name'] = $userProfile['last_name'];
            $userData['email'] = $userProfile['email'];
            //$userData['gender'] = $userProfile['gender'];
            //$userData['locale'] = $userProfile['locale'];
            $userData['profile_url'] = 'https://www.facebook.com/' . $userProfile['id'];
            $userData['picture_url'] = $userProfile['picture']['data']['url'];
            // Insert or update user data
            //var_dump($userData);die;
            $token = $this->User_Model->randomstring();
            $data = ['name' => ucwords(strtolower($userData['first_name'])) . ' ' . ucwords(strtolower($userData['last_name'])), 'email' => $userData['email'], 'password' => "", 'type' => "facebook", 'token' => $token, 'socialToken'=>$userData['oauth_uid'], 'status' => 0];

            $socialCheck = $this->User_Model->user_socialagain($userData['oauth_uid'], "facebook");
            //echo $this->db->last_query();die;
            // Check user data insert or update status
            if (!$socialCheck) {

                $userInserted = $this->User_Model->user_insert($data);
                $getCompleteness = $this->User_Model->check_profileComplete($userInserted);
                if ($getCompleteness) {
                    if ($getCompleteness[0]['signup'] == 0) {
                        $profileComplete = 14;
                        $comProfile = ["signup" => $profileComplete];
                        $this->User_Model->update_profileComplete($comProfile, $userInserted);
                    }
                } else {
                    $profileComplete = 14;
                    $comProfile = ["user_id" => $userInserted, "signup" => $profileComplete];
                    $this->User_Model->insert_profileComplete($comProfile);
                }

                $getCompletenes = $this->fetchUserCompleteData($userInserted);
                $getData = $this->fetchUserSingleData($userInserted);
                $this->session->set_userdata('usersocialsession', $data);
                $this->session->set_userdata('userfsess', $getData);
                if($previous_url = $this->session->userdata('previous_url')) {
                    redirect($previous_url);
                } else {
                    redirect('');
                }
            } else {

                if($socialCheck[0]['status'] == 0) {
                    
                    $getCompleteness = $this->User_Model->check_profileComplete($socialCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['signup'] == 0) {
                            $profileComplete = 14;
                            $comProfile = ["signup" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $socialCheck[0]['id']);
                        }
                    } else {
                        $profileComplete = 14;
                        $comProfile = ["user_id" => $socialCheck[0]['id'], "signup" => $profileComplete];
                        $this->User_Model->insert_profileComplete($comProfile);
                    }
                 
                    $getCompletenes = $this->fetchUserCompleteData($socialCheck[0]['id']);
                    $getData = $this->fetchUserSingleData($socialCheck[0]['id']);
                    //print_r($getData);die;
                    $this->session->set_userdata('usersocialsession', $data);
                    $this->session->set_userdata('userfsess', $getData);
                    if($previous_url = $this->session->userdata('previous_url')) {
                        redirect($previous_url);
                    } else {
                        redirect('');
                    }

                } else {

                    $getCompletenes = $this->fetchUserCompleteData($socialCheck[0]['email']);
                    $getData = $this->fetchUserSingleData1($socialCheck[0]['email']);
                    $this->session->set_userdata('usersess', $getData);
                    
                    if($this->session->userdata('previous_url_again')) {
                        $previous_url = $this->session->userdata('previous_url_again');
                        redirect($previous_url);
                    } else {
                        if($previous_url = $this->session->userdata('previous_url')) {
                            redirect($previous_url);
                        } else {
                            redirect('user/homeafterlogin');
                        }
                    }
                }
            }
        }
    }

    public function google_login() {
        if (isset($_GET['code'])) {
            //authenticate user
            $this->google->getAuthenticate();
            //get user info from google
            $gpInfo = $this->google->getUserInfo();
            //preparing data for database insertion
            $userData['oauth_provider'] = 'google';
            $userData['oauth_uid'] = $gpInfo['id'];
            $userData['first_name'] = $gpInfo['given_name'];
            $userData['last_name'] = $gpInfo['family_name'];
            $userData['email'] = $gpInfo['email'];
            $userData['gender'] = !empty($gpInfo['gender']) ? $gpInfo['gender'] : '';
            $userData['locale'] = !empty($gpInfo['locale']) ? $gpInfo['locale'] : '';
            $userData['profile_url'] = !empty($gpInfo['link']) ? $gpInfo['link'] : '';
            $userData['picture_url'] = !empty($gpInfo['picture']) ? $gpInfo['picture'] : '';
            $userData['password'] = "pass";
            //insert or update user data to the database
            //var_dump($userData);die;
            $token = $this->User_Model->randomstring();
            
            $data = ['name' => ucwords(strtolower($userData['first_name'])) . ' ' . ucwords(strtolower($userData['last_name'])), 'email' => $userData['email'], 'password' => $userData['password'], 'type' => "gmail", 'token' => $token, 'socialToken'=>$userData['oauth_uid'], 'status' => 0];

            $socialCheck = $this->User_Model->user_socialagain($userData['oauth_uid'], 'gmail');
            if (!$socialCheck) {

                $userInserted = $this->User_Model->user_insert($data);

                $getCompleteness = $this->User_Model->check_profileComplete($userInserted);
                
                if ($getCompleteness) {
                    if ($getCompleteness[0]['signup'] == 0) {
                        $profileComplete = 14;
                        $comProfile = ["signup" => $profileComplete];
                        $this->User_Model->update_profileComplete($comProfile, $userInserted);
                    }
                } else {
                    $profileComplete = 14;
                    $comProfile = ["user_id" => $userInserted, "signup" => $profileComplete];
                    $this->User_Model->insert_profileComplete($comProfile);
                }
             
                $getCompletenes = $this->fetchUserCompleteData($userInserted);
                $getData = $this->fetchUserSingleData($userInserted);
                //print_r($getData);die;
                $this->session->set_userdata('usersocialsession', $data);
                $this->session->set_userdata('userfsess', $getData);
                if($previous_url = $this->session->userdata('previous_url')) {
                    redirect($previous_url);
                } else {
                    redirect('');
                }

            } else {

                if($socialCheck[0]['status'] == 0) {

                    $getCompleteness = $this->User_Model->check_profileComplete($socialCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['signup'] == 0) {
                            $profileComplete = 14;
                            $comProfile = ["signup" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $socialCheck[0]['id']);
                        }
                    } else {
                        $profileComplete = 14;
                        $comProfile = ["user_id" => $socialCheck[0]['id'], "signup" => $profileComplete];
                        $this->User_Model->insert_profileComplete($comProfile);
                    }
                 
                    $getCompletenes = $this->fetchUserCompleteData($socialCheck[0]['id']);
                    $getData = $this->fetchUserSingleData($socialCheck[0]['id']);
                    //print_r($getData);die;
                    $this->session->set_userdata('usersocialsession', $data);
                    $this->session->set_userdata('userfsess', $getData);
                    if($previous_url = $this->session->userdata('previous_url')) {
                        redirect($previous_url);
                    } else {
                        redirect('');
                    }

                } else {

                    $getCompletenes = $this->fetchUserCompleteData($userData['email']);
                    $getData = $this->fetchUserSingleData1($userData['email']);
                    $this->session->set_userdata('usersess', $getData);
                    
                    //var_dump($this->session->userdata('previous_url_again'));die;
                    if($this->session->userdata('previous_url_again')) {
                        $previous_url = $this->session->userdata('previous_url_again');
                        redirect($previous_url);
                    } else {
                        if($previous_url = $this->session->userdata('previous_url')) {
                            redirect($previous_url);
                        } else {
                            redirect('user/homeafterlogin');
                        }
                    }
                }
            }
        }
    }
    public function userDetailUpdate() {
        $profileData = $this->input->post();

       //print_r($profileData);die;
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->load->helper(array('form', 'url'));
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $data['updateerrors'] = $this->form_validation->error_array();
            $userSess = $this->session->userdata('usersess');
            $dataToken = ["token" => $userSess['token']];
            $userInfo = $this->User_Model->token_match($dataToken);
            $data['profileDetail'] = $this->fetchUserSingleData($userInfo[0]['id']);
            $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
            $data["states"] = $this->User_Model->getownstate();
            $this->load->view("myprofile", $data);
       
        } else {
            $data = ["token" => $profileData['token']];
            $userTokenCheck = $this->User_Model->token_match($data);
            if ($userTokenCheck) {
                if (!empty($profileData['verbal'])) {
                    $verbal = $profileData['verbal'];
                } else {
                    $verbal = 0;
                }
                if (isset($profileData['Written'])) {
                    $written = $profileData['Written'];
                } else {
                    $written = 0;
                }
                if (isset($profileData['Listening'])) {
                    $listening = $profileData['Listening'];
                } else {
                    $listening = 0;
                }
                if (isset($profileData['Problem'])) {
                    $problem = $profileData['Problem'];
                } else {
                    $problem = 0;
                }
                $data2 = ["user_id" => $userTokenCheck[0]['id'], "verbal" => $verbal, "written" => $written, "listening" => $listening, "problem" => $problem];
                $checkAssessment = $this->User_Model->user_assessment($userTokenCheck[0]['id']);
                if ($checkAssessment) {
                    if (isset($profileData['verbal']) && $profileData['verbal'] != 0 && $profileData['Written'] != 0 && $profileData['Listening'] != 0 && $profileData['Problem'] != 0) {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if ($getCompleteness) {
                            if ($getCompleteness[0]['assessment'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["assessment" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                    }
                    $checkAssessmentUpdate = $this->User_Model->user_assessment_update($data2, $userTokenCheck[0]['id']);
                } else {
                    if (!empty($profileData['verbal']) && !empty($profileData['Written']) && !empty($profileData['Listening']) && !empty($profileData['Problem'])) {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if ($getCompleteness) {
                            if ($getCompleteness[0]['assessment'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["assessment" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                    }
                    $checkAssessmentInsert = $this->User_Model->user_assessment_insert($data2);
                }
                if(isset($profileData['expert'])){
                    $profileData['expert']= $profileData['expert'];
                    $data3 = ["user_id" => $userTokenCheck[0]['id'], "expert" => implode(",", $profileData['expert']) ];
                    $checkExpert = $this->User_Model->user_expert($userTokenCheck[0]['id']);
                    if ($checkExpert) {
                        if (!empty($profileData['expert'])) {
                            $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                            if ($getCompleteness) {
                                if ($getCompleteness[0]['expert'] == 0) {
                                    $profileComplete = 10;
                                    $comProfile = ["expert" => $profileComplete];
                                    $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                                }
                            }
                        }
                        $checkExpertUpdate = $this->User_Model->user_expert_update($data3, $userTokenCheck[0]['id']);
                    } else {
                        if (!empty($profileData['expert'])) {
                            $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                            if ($getCompleteness) {
                                if ($getCompleteness[0]['expert'] == 0) {
                                    $profileComplete = 10;
                                    $comProfile = ["expert" => $profileComplete];
                                    $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                                }
                            }
                        }
                        $checkExpertInsert = $this->User_Model->user_expert_insert($data3);
                    }
                }
                

                $data_more = ["user_id" => $userTokenCheck[0]['id'], "strength" => $profileData['strength'], "weakness" => $profileData['weakness'], "achievements" => $profileData['achievements']];
                $checkMore = $this->User_Model->user_more($userTokenCheck[0]['id']);
                if ($checkMore) {
                    $checkMoreUpdate = $this->User_Model->user_more_update($data_more, $userTokenCheck[0]['id']);
                } else {
                    $checkMoreInsert = $this->User_Model->user_more_insert($data_more);
                }

                if (!empty($profileData['dob'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['dob'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["dob" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                if (!empty($profileData['location'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['location'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["location" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                if (!empty($profileData['designation'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['designation'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["designation" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                if (!empty($profileData['current_salary'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['current_salary'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["current_salary" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                if (!empty($profileData['exp_month']) || !empty($profileData['exp_year'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['exp'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["exp" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }

                $profileData = ["name" => $profileData['name'], "dob" => $profileData['dob'], "location" => $profileData['location'], "phone" => $profileData['phone'], "designation" => $profileData['designation'], "country_code" => $profileData['country_code'], "current_salary" => $profileData['current_salary'], "exp_month" => $profileData['exp_month'], "exp_year" => $profileData['exp_year'], "education" => $profileData['education'], "nationality" => $profileData['nationality'], "superpower" => $profileData['superpower'], "jobsInterested" => implode(',', $profileData['intrested']), "state" => $profileData['state'], "city" => $profileData['city'] ];

                // var_dump($profileData);die;
                
                $userUpdated = $this->User_Model->user_update($profileData, $userTokenCheck[0]['id']);
                
                if (!empty($_FILES['profilePic']['name'])) {
                    $config = array('upload_path' => "./uploads/", 'allowed_types' => "jpg|png|jpeg",);
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('profilePic')) {
                        $data = array('upload_data' => $this->upload->data());
                        $picPath = base_url() . "uploads/" . $data['upload_data']['file_name'];
                        $profilePicData = ["profilePic" => $picPath];
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if ($getCompleteness) {
                            if ($getCompleteness[0]['profilePic'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["profilePic" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                        $this->User_Model->user_update($profilePicData, $userTokenCheck[0]['id']);
                    } else {
                        $data['error'] = array('error' => $this->upload->display_errors());
                        var_dump($data['error']);
                        die;
                        redirect("dashboard/myprofile");
                    }
                }
                
                $this->session->set_tempdata('updatemsg','Profile updated successfully.');
                redirect("dashboard/myprofile");
            } else {
                session_destroy();
                redirect("user/index");
            }
        }
    }
    public function resumeUpload() {
        $resumeData = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["token" => $userSess['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        $redirectURL = $this->clean_url($resumeData['currentURL']);

        if ($userTokenCheck) {
            $checkResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);
            if ($checkResume) {
                if (!empty($_FILES['resumeFile']['name'])) {

                    $config = array('upload_path' => "./resumeuploads/", 'allowed_types' => "pdf|doc|docx",);
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('resumeFile')) {
                        $data = array('upload_data' => $this->upload->data());
                        $picPath = base_url() . "resumeuploads/" . $data['upload_data']['file_name'];
                    
                    } else {
                        $errors = $this->upload->display_errors();
                        $data1['errors'] = strip_tags($errors);
                        redirect($redirectURL."/". $data1['errors']);
                    }
                
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['resume'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["resume" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                    $userResumeUpdate = $this->User_Model->user_resume_update($picPath, $userTokenCheck[0]['id']);
                }
            } else {

                if (!empty($_FILES['resumeFile']['name'])) {
                    $config = array('upload_path' => "./resumeuploads/", 'allowed_types' => "pdf|doc|docx",);
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('resumeFile')) {
                        $data = array('upload_data' => $this->upload->data());
                        $picPath = base_url() . "resumeuploads/" . $data['upload_data']['file_name'];
                    
                    } else {
                        //$data1['errors'] = "Upload Failed";
                        $errors = $this->upload->display_errors();
                        $data1['errors'] = strip_tags($errors);
                        $redirectURL = $this->clean_url($resumeData['currentURL']);
                        redirect($redirectURL."/". $data1['errors']);
                    }
                
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['resume'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["resume" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                    $userResumeInsert = $this->User_Model->user_resume_insert($picPath, $userTokenCheck[0]['id']);
                }
            }
            redirect($redirectURL);
        } else {
            $data1['errors'] = "Bad Request";
            redirect($redirectURL);
        }
    }

    public function clean_url($url) {
        $parts = parse_url($url);
        $explodeparts = explode('/', $parts['path']);
        return $parts['scheme'] . '://' . $parts['host'] .'/'. $explodeparts[1] .'/'. $explodeparts[2];
    }

    public function userDetailUpdatemore() {
        $profileData = $this->input->post();
        $data = ["token" => $profileData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        if ($userTokenCheck) {
            if ($profileData['verbal']) {
                $verbal = $profileData['verbal'];
            } else {
                $verbal = 0;
            }
            if ($profileData['Written']) {
                $written = $profileData['Written'];
            } else {
                $written = 0;
            }
            if ($profileData['Listening']) {
                $listening = $profileData['Listening'];
            } else {
                $listening = 0;
            }
            if ($profileData['Problem']) {
                $problem = $profileData['Problem'];
            } else {
                $problem = 0;
            }
            $data2 = ["user_id" => $userTokenCheck[0]['id'], "verbal" => $verbal, "written" => $written, "listening" => $listening, "problem" => $problem];
            $checkAssessment = $this->User_Model->user_assessment($userTokenCheck[0]['id']);
            if ($checkAssessment) {
                if ($profileData['verbal'] != 0 && $profileData['Written'] != 0 && $profileData['Listening'] != 0 && $profileData['Problem'] != 0) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['assessment'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["assessment" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                $checkAssessmentUpdate = $this->User_Model->user_assessment_update($data2, $userTokenCheck[0]['id']);
            } else {
                if (!empty($profileData['verbal']) && !empty($profileData['Written']) && !empty($profileData['Listening']) && !empty($profileData['Problem'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['assessment'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["assessment" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                $checkAssessmentInsert = $this->User_Model->user_assessment_insert($data2);
            }

            $data3 = ["user_id" => $userTokenCheck[0]['id'], "expert" => implode(",", $profileData['expert']) ];
            $checkExpert = $this->User_Model->user_expert($userTokenCheck[0]['id']);
            if ($checkExpert) {
                if (!empty($profileData['expert'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['expert'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["expert" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                $checkExpertUpdate = $this->User_Model->user_expert_update($data3, $userTokenCheck[0]['id']);
            } else {
                if (!empty($profileData['expert'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['expert'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["expert" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                $checkExpertInsert = $this->User_Model->user_expert_insert($data3);
            }
            redirect("dashboard/myprofile");
        } else {
            session_destroy();
            redirect("user/index");
        }
    }
    public function workExperience() {
        $workData = $this->input->post();
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('company', 'company', 'trim|required');
        $this->form_validation->set_rules('desc', 'desc', 'trim|required');
        $this->form_validation->set_rules('from', 'from', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            redirect("dashboard/myprofile", $data);
        } else {
            $userSess = $this->session->userdata('usersess');
            $data = ["token" => $userSess['token']];
            $userTokenCheck = $this->User_Model->token_match($data);
            if ($userTokenCheck) {
                $jobFrom = date("d-m-Y", strtotime($workData['from']));
                if ($workData['to']) {
                    $jobTo = date("d-m-Y", strtotime($workData['to']));
                } else {
                    $jobTo = " ";
                }
                if ($workData['regularcheck']) {
                    $regularcheck = $workData['regularcheck'];
                } else {
                    $regularcheck = 0;
                }
                $data = ["user_id" => $userTokenCheck[0]['id'], "title" => $workData['title'], "company" => $workData['company'], "jobDesc" => $workData['desc'], "workFrom" => $jobFrom, "workTo" => $jobTo, "category" => $workData['category'], "subcategory" => $workData['subcategory'], "level" => $workData['joblevel'], "currently_working" => $regularcheck];
                if (empty($workData['id'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['experience'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["experience" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                        if ($getCompleteness[0]['designation'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["designation" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                    $workInserted = $this->User_Model->work_insert($data);
                } else {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['experience'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["experience" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                        if ($getCompleteness[0]['designation'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["designation" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                    $workInserted = $this->User_Model->work_update($data, $workData['id']);
                }
                if ($workInserted) {
                    $this->session->set_flashdata('msg', '<div class="alert alert-success">Data Updated successfully</div>');
                    redirect("dashboard/myprofile");
                } else {
                    $data['errors'] = "Data not inserted";
                    redirect("dashboard/myprofile", $data);
                }
            } else {
                $data['errors'] = "Bad Request";
                redirect("dashboard/myprofile", $data);
            }
        }
    }
    public function SubcategoryListing() {
        $categoryid = $this->input->post('categoryid');
        $subcategory_list = $this->Common_Model->subcategory_listsbycatid($categoryid);
        if ($subcategory_list) {
            echo "<option value=''>Select Subcategory</option>";
            foreach ($subcategory_list as $subcategory_lists) {
                echo "<option value='" . $subcategory_lists['id'] . "'>" . $subcategory_lists['subcategory'] . "</option>";
            }
        } else {
            echo "<option value=''>Select Subcategory</option>";
        }
    }
    public function getsubcategory() {
        $categoryId = $this->input->post('catid');
        $catidimp = implode(',', $categoryId);
        if ($catidimp != '') {
            if ($catidimp == 9) {
                $subcategory_list = $this->Common_Model->subcategory_lists();
                if ($subcategory_list) {
                    echo "<option value=''>Select Subcategory</option>";
                    foreach ($subcategory_list as $subcategory_lists) {
                        echo "<option value='" . $subcategory_lists['id'] . "'>" . $subcategory_lists['subcategory'] . "</option>";
                    }
                } else {
                    echo "<option value=''>Select Subcategory</option>";
                }
            } else {
                $subcategory_list = $this->Common_Model->filtersubcategory_listsbycatid($catidimp);
                //echo $this->db->last_query();die;
                if ($subcategory_list) {
                    echo "<option value=''>Select Subcategory</option>";
                    foreach ($subcategory_list as $subcategory_lists) {
                        echo "<option value='" . $subcategory_lists['id'] . "'>" . $subcategory_lists['subcategory'] . "</option>";
                    }
                } else {
                    echo "<option value=''>Select Subcategory</option>";
                }
            }
        } else {
            echo "<option value=''>Select Subcategory</option>";
        }
    }

    public function getsubcategoryInner() {
        $categoryId = $this->input->post('catid');
        if ($categoryId != '') {
            if ($categoryId == 9) {
                $subcategory_list = $this->Common_Model->subcategory_lists();
                if ($subcategory_list) {
                    echo "<option value=''>Select Job Subcategory</option>";
                    foreach ($subcategory_list as $subcategory_lists) {
                        echo "<option value='" . $subcategory_lists['id'] . "'>" . $subcategory_lists['subcategory'] . "</option>";
                    }
                } else {
                    echo "<option value=''>Select Job Subcategory</option>";
                }
            } else {
                $subcategory_list = $this->Common_Model->filtersubcategory_listsbycatid($categoryId);
                if ($subcategory_list) {
                    echo "<option value=''>Select Job Subcategory</option>";
                    foreach ($subcategory_list as $subcategory_lists) {
                        echo "<option value='" . $subcategory_lists['id'] . "'>" . $subcategory_lists['subcategory'] . "</option>";
                    }
                } else {
                    echo "<option value=''>Select Job Subcategory</option>";
                }
            }
        } else {
            echo "<option value=''>Select Job Subcategory</option>";
        }
    }

    public function delete_single_work() {

        $userSess = $this->session->userdata('usersess');
        $data = ["token" => $userSess['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        $work_id = $this->input->post('user_id');
        $tbname = $this->input->post('tbname');
        $delete = $this->User_Model->delete_user($work_id, $tbname);
        $workData = $this->fetchWorkSingleData($userTokenCheck[0]['id']);
        $eduData = $this->fetchEduSingleData($userTokenCheck[0]['id']);
        $langData = $this->fetchLangSingleData($userTokenCheck[0]['id']);
        $clientData = $this->fetchClientSingleData($userTokenCheck[0]['id']);
        if (empty($clientData)) {
            $comProfile = ["topClient" => 0];
        }
        if (empty($workData)) {
            $comProfile = ["experience" => 0];
        }
        if (empty($eduData)) {
            $comProfile = ["education" => 0];
        }
        if (empty($langData)) {
            $comProfile = ["language" => 0];
        }
        if (!empty($comProfile)) {
            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
        }
        if ($delete) {
            echo '1';
        } else {
            echo '2';
        }
    }
    
    public function updateworkview() {
        $id = $this->input->post('id');
        $getData = $this->User_Model->user_single_work($id);
        echo json_encode($getData);
    }
    
    public function updateeduview() {
        $id = $this->input->post('id');
        $getData = $this->User_Model->user_single_education($id);
        echo json_encode($getData);
    }
    
    public function education() {
        $eduData = $this->input->post();
        $this->form_validation->set_rules('attainment', 'attainment', 'trim|required');
        $this->form_validation->set_rules('degree', 'degree', 'trim|required');
        $this->form_validation->set_rules('university', 'university', 'trim|required');
        $this->form_validation->set_rules('from', 'from', 'trim|required');
        $this->form_validation->set_rules('to', 'to', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            redirect("dashboard/myprofile", $data);
        } else {
            $userSess = $this->session->userdata('usersess');
            $data = ["token" => $userSess['token']];
            $userTokenCheck = $this->User_Model->token_match($data);
            if ($userTokenCheck) {
                $jobFrom = date("d-m-Y", strtotime($eduData['from']));
                $jobTo = date("d-m-Y", strtotime($eduData['to']));
                $data = ["user_id" => $userTokenCheck[0]['id'], "attainment" => $eduData['attainment'], "degree" => $eduData['degree'], "university" => $eduData['university'], "degreeFrom" => $jobFrom, "degreeTo" => $jobTo];
                if (empty($eduData['id'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if ($getCompleteness) {
                        if ($getCompleteness[0]['education'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["education" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                    $eduInserted = $this->User_Model->edu_insert($data);
                } else {
                    $eduInserted = $this->User_Model->edu_update($data, $eduData['id']);
                }
                if ($eduInserted) {
                    $this->session->set_flashdata('msg', '<div class="alert alert-success">Data Updated successfully</div>');
                    redirect("dashboard/myprofile");
                } else {
                    $data['errors'] = "Data not inserted";
                    redirect("dashboard/myprofile", $data);
                }
            } else {
                $data['errors'] = "Bad Request";
                redirect("dashboard/myprofile", $data);
            }
        }
    }
    public function filters() {
        $filterData = $this->input->post();
        //print_r($filterData);die;
        /*if ($filterData['locationn'] == "" && $filterData['cname'] == "" && $filterData['jobsubcategory'] == "" && $filterData['joblevell'] == "" && $filterData['jobcategoryy'] == "") {
            header('Location: http://mobuloustech.com/jobyoda/');
            exit;
        }*/
        
        if (!empty($filterData['joblevell'][0]) && $filterData['joblevell'][0] == 9) {
            $filterData['joblevell'] = array(6, 4, 7, 8);
        }
        if (!empty($filterData['jobcategoryy'][0]) && $filterData['jobcategoryy'][0] == 9) {
            $filterData['jobcategoryy'] = array(5, 3, 4, 6, 7, 8);
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        $data["jobList"] = [];
        if ($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($jobfetch['id']);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['company_id']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['pg_insert(connection, table_name, assoc_array)cks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
/*                    $ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                     
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;
                    $distance = $this->distance($dataLatitude, $dataLongitude, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');*/
                        $distance = '';
                    if ($jobApplyfetch) {
                    } else {
                        $data["jobList"][] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'], "salary" => $basicsalary, "companyName" => $jobfetch['cname'],"cname"=>$cname, "company_id" => $jobfetch['company_id'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "latitude" => $jobfetch['latitude'], "longitude" => $jobfetch['longitude'], "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppick3" => $datatoppicks3, "save_status" => $jobsstatus, "job_image" => $jobImageData, "jobPitch"=>$jobfetch['jobPitch'], "distance" => $distance];
                    }
                }
                if (count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else {
                $data["jobList"] = [];
            }
            $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            $returnnewarray = array();
            $latlogcheck = array();
            foreach ($jobcountfetchs as $value123) {
                
                if(in_array($value123['latitude'],$latlogcheck)) {
                    
                        $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                } else {

                    $latlogcheck[] = $value123['latitude'];
                    $returnnewarray[] = $value123;
                }
            }

            $jobcountfetchs = $returnnewarray;
            //echo $this->db->last_query();die;
            if ($jobcountfetchs) {
                $xx = 1;

                foreach ($jobcountfetchs as $jobcountfetch) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        if(!empty($dataJobList[0])){
                            if($dataJobList[0]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[0]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[0]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[0]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[0]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[0]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[1])){
                            if($dataJobList[1]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[1]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[1]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[1]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[1]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[1]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[2])){
                            if($dataJobList[2]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[2]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[2]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[2]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[2]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[2]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }
                        
                        if(count($jobTop)>1){
                            $url = base_url()."markericon/j_hot_job.png";
                        }

                        if(empty($dataJobList[0]) && empty($dataJobList[1]) && empty($dataJobList[2])){
                            $url = base_url()."markericon/maps.png";
                        }

                        $urll = base_url()."dashboard/jobLists?lat=".$jobcountfetch['latitude']."&long=".$jobcountfetch['longitude'];
                        if($jobcountfetch["salary"]>0)
                        {
                            $salary = ($jobcountfetch["salary"]/1000).'K';
                        }else{
                            $salary = '<img src="https://jobyoda.com/webfiles/img/peso_green.png" width="10px">';
                        }
                        if($jobcountfetch['jobcount']==1){
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'"><span style="color:#32aa60;">' .$jobcountfetch['jobcount'] . ' Job'.'<br>'.'   '.$salary.' </span></a></span>';
                        }else{
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'"><span style="color:#32aa60;">' .$jobcountfetch['jobcount'] . ' Jobs'.'<br>'.'   '.$salary.'</span> </a></span>';
                        }
                        
                    $dataget[$xx] = ["info" => $info, "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], 'jobid' => $jobcountfetch['id'], "url" => $url];
                    $xx++;
                }
                //$dataget1 = json_encode($dataget);
                $data['jobcount'] = $dataget;
            } else {
                $data['jobcount'] = [];
            }
            $data['jobtitle'] = $this->Common_Model->job_title_list();
            // echo "<PRE>";
            // var_dump($data);die;
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['languagelist'] = $this->Common_Model->language_lists();
            $data['level_list'] = $this->Common_Model->level_lists();
            $data['category_list'] = $this->Common_Model->category_lists();
             $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
            $this->load->view("search1", $data);
        } else {
            session_destroy();
            redirect("user/index");
        }
    }
    public function advancesearch() {
        $filterData = $this->input->post();
        //print_r($filterData);die;
        /*if ($filterData['locationn'] == "" && $filterData['cname'] == "" && $filterData['jobsubcategory'] == "" && $filterData['joblevell'] == "" && $filterData['jobcategoryy'] == "") {
            header('Location: http://mobuloustech.com/jobyoda/');
            exit;
        }*/
        if (empty($filterData['locationn']) &&  $filterData['locationn'] == "" && empty($filterData['cname']) && $filterData['cname'] == "" && $filterData['jobsubcategory'] == "" && empty($filterData['joblevell'])  &&  empty($filterData['jobcategoryy'])) {
            $this->session->set_tempdata('filtererr','At least one field is required to search.');
            redirect('user/index');
        }else{
        if (!empty($filterData['joblevell'][0]) && $filterData['joblevell'][0] == 9) {
            $filterData['joblevell'] = array(6, 4, 7, 8);
        }
        if (!empty($filterData['jobcategoryy'][0]) && $filterData['jobcategoryy'][0] == 9) {
            $filterData['jobcategoryy'] = array(5, 3, 4, 6, 7, 8);
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        $data["jobList"] = [];
        if ($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            
            if ($jobfetchs) {
            
                foreach ($jobfetchs as $jobfetch) {
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($jobfetch['id']);
            
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['company_id']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['pg_insert(connection, table_name, assoc_array)cks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }
                        $ip = $_SERVER['REMOTE_ADDR'];
                       $location = file_get_contents('http://ip-api.com/json/'.$ip);
                       $data_loc = json_decode($location);
                       
                       $Address = $data_loc->city;
                       $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                       $output = json_decode($geocodeFromAddr);
                          //print_r($output);die;       
                       //Get latitude and longitute from json data
                       $dataLatitude  = $output->results[0]->geometry->location->lat; 
                       $dataLongitude = $output->results[0]->geometry->location->lng;
                        $distance = $this->distance($dataLatitude, $dataLongitude, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                    if ($jobApplyfetch) {
                    } else {
                        $data["jobList"][] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'], "salary" => $basicsalary, "companyName" => $jobfetch['cname'], "company_id" => $jobfetch['company_id'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "latitude" => $jobfetch['latitude'], "longitude" => $jobfetch['longitude'], "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppick3" => $datatoppicks3, "save_status" => $jobsstatus, "job_image" => $jobImageData,"cname"=>$cname, "distance"=>$distance, "jobPitch" => $jobfetch['jobPitch']];
                    }
                }
                if (count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else {
                $data["jobList"] = [];
            }
            $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

            $jobcountfetchs = $returnnewarray;
            //print_r($jobcountfetchs);die;
            if ($jobcountfetchs) {
                $xx = 1;
                foreach ($jobcountfetchs as $jobcountfetch) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        if(!empty($dataJobList[0])){
                            if($dataJobList[0]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[0]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[0]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[0]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[0]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[0]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[1])){
                            if($dataJobList[1]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[1]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[1]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[1]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[1]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[1]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[2])){
                            if($dataJobList[2]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[2]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[2]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[2]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[2]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[2]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }
                        
                        if(count($jobTop)>1){
                            $url = base_url()."markericon/j_hot_job.png";
                        }

                        if(empty($dataJobList[0]) && empty($dataJobList[1]) && empty($dataJobList[2])){
                            $url = base_url()."markericon/maps.png";
                        }
                        $urll = base_url()."dashboard/jobLists?lat=".$jobcountfetch['latitude']."&long=".$jobcountfetch['longitude'];
                        if($jobcountfetch["salary"]>0)
                        {
                            $salary = ($jobcountfetch["salary"]/1000).'K';
                        }else{
                            $salary = '<img src="https://jobyoda.com/webfiles/img/peso_green.png" width="10px">';
                        }
                        if($jobcountfetch['jobcount']==1){
                            $inform = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'"><span style="color:#32aa60;">' .$jobcountfetch['jobcount'] . ' Job'.'<br>'.'   '.$salary.' </span></a></span>';
                        }else{
                            $inform = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'"><span style="color:#32aa60;">' .$jobcountfetch['jobcount'] . ' Jobs'.'<br>'.'   '.$salary.'</span> </a></span>';
                        }
                        
                    $dataget[$xx] = ["info" => $inform, "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], 'jobid' => $jobcountfetch['id'], "url" => $url];
                    $xx++;
                }
                //$dataget1 = json_encode($dataget);
                $data['jobcount'] = $dataget;
            } else {
                $data['jobcount'] = [];
            }
            $data['jobtitle'] = $this->Common_Model->job_title_list();
            // echo "<PRE>";
            // var_dump($data);die;
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['languagelist'] = $this->Common_Model->language_lists();
            $data['level_list'] = $this->Common_Model->level_lists();
            $data['category_list'] = $this->Common_Model->category_lists();
            $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
            $this->load->view("search1", $data);
        } else {
            session_destroy();
            redirect("user/index");
        }}
    }
    public function fetchFeaturedJob() {
        $jobDatas = $this->Jobpost_Model->fetchFeatured_Job();
        $data = array();
        foreach ($jobDatas as $jobData) {
            $data[] = ["id" => $jobData['id'], "title" => $jobData['jobtitle'], "companyName" => $jobData['cname'], "salary" => $jobData['salary'], "jobLocation" => $jobData['jobLocation']];
        }
        return $data;
    }
    public function fetchRecentJob() {
        $jobDatas = $this->Jobpost_Model->fetchRecent_Job();
        $data = array();
        foreach ($jobDatas as $jobDatlistinga) {
            $data[] = ["id" => $jobData['id'], "title" => $jobData['jobtitle'], "jobDesc" => $jobData['jobtitle'], "companyName" => $jobData['cname'], "salary" => $jobData['salary'], "jobLocation" => $jobData['jobLocation'], "companyPic" => $jobData['companyPic']];
        }
        return $data;
    }
    public function fetchUserSingleData($id) {
        $userData = $this->User_Model->user_single($id);
        $data = ["id" => $userData[0]['id'], "token" => $userData[0]['token'], "name" => $userData[0]['name'], "email" => $userData[0]['email'], "type" => $userData[0]['type'], "profilePic" => $userData[0]['profilePic'], "phone" => $userData[0]['phone'], "dob" => $userData[0]['dob']];
        return $data;
    }
    public function fetchUserSingleData1($email) {
        $userData = $this->User_Model->user_single1($email);
        $data = ["id" => $userData[0]['id'], "token" => $userData[0]['token'], "name" => $userData[0]['name'], "email" => $userData[0]['email'], "type" => $userData[0]['type'], "profilePic" => $userData[0]['profilePic'], "phone" => $userData[0]['phone'], "dob" => $userData[0]['dob']];
        return $data;
    }
    public function fetchUserCompleteData($id) {
        $userData = $this->User_Model->userComplete_single($id);
        if ($userData) {
            $data['comp'] = $userData[0]['signup'] + $userData[0]['profile'] + $userData[0]['profilePic'] + $userData[0]['resume'] + $userData[0]['experience'] + $userData[0]['education'] + $userData[0]['assessment'] + $userData[0]['language'] + $userData[0]['expert'] + $userData[0]['topClient']+ $userData[0]['dob']+ $userData[0]['location']+ $userData[0]['designation']+ $userData[0]['current_salary']+ $userData[0]['exp'];
        } else {
            $data['comp'] = 0;
        }
        return $data;
    }

    public function getfeauredjoblistings($current_lat, $current_long) {
        /*if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }*/
        $data["hotjobss"] = [];
        $cat =0;
        $hotjobData = $this->Common_Model->hotjob_fetch_latlongg($cat);
        //echo $this->db->last_query();die;
        if (!empty($hotjobData)) {
            foreach ($hotjobData as $hotjobsData) {
                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);

                if (!empty($jobTop) && count($jobTop) > 1) {
                    if (isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id'];
                    } else {
                        $datatoppicks1 = "";
                    }
                    if (isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id'];
                    } else {
                        $datatoppicks2 = "";
                    }
                    if (isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id'];
                    } else {
                        $datatoppicks3 = "";
                    }
                    
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    /*$ip = $_SERVER['REMOTE_ADDR'];
                    $location = file_get_contents('http://ip-api.com/json/'.$ip);
                    $data_loc = json_decode($location);
                   
                    $Address = $data_loc->city;
                    $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                    $output = json_decode($geocodeFromAddr);
                    $dataLatitude  = $output->results[0]->geometry->location->lat; 
                    $dataLongitude = $output->results[0]->geometry->location->lng;*/
                   /*echo $latitud= cur_lat;
                    echo "<br>";
                    $intlat =  (int)cur_lat;
                    echo $intlat; die;*/
                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';
                    $data["hotjobss"][] = ["jobpost_id" => $hotjobsData['id'], "comapnyId" => $hotjobsData['compId'], "job_title" => $hotjobsData['jobtitle'], "jobDesc" => $hotjobsData['jobDesc'], "salary" => $basicsalary, "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData,"jobPitch"=>$hotjobsData['jobPitch'], "cname"=>$cname, "distance" =>$distance, "savedjob"=> ''];
                }
            }
        } else {
            $data["hotjobss"][] = ["jobpost_id" => "", "comapnyId" => "", "job_title" => "", "jobDesc" => "", "salary" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "", "save_status" => "" ,"savedjob"=> ''];
        }
        return $data["hotjobss"];
    }
    public function getfeauredjoblistingss1($id) {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        $tokendata = ['id'=>$id];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        //echo $id;die;
        $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlong($id,$expfilter,$current_lat, $current_long);
        // echo $this->db->last_query();die;
        if ($hotjobData) {
            foreach ($hotjobData as $hotjobsData) {
                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                if (!empty($jobTop) && count($jobTop) > 1) {
                    if (isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id'];
                    } else {
                        $datatoppicks1 = "";
                    }
                    if (isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id'];
                    } else {
                        $datatoppicks2 = "";
                    }
                    if (isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id'];
                    } else {
                        $datatoppicks3 = "";
                    }
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $id);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    /*$ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                      //print_r($output);die;       
                   //Get latitude and longitute from json data
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = '';
                        }
                    $data["hotjobss"][] = ["jobpost_id" => $hotjobsData['id'], "comapnyId" => $hotjobsData['compId'], "job_title" => $hotjobsData['jobtitle'], "jobDesc" => $hotjobsData['jobDesc'], "salary" => $basicsalary, "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData,"jobPitch"=>$hotjobsData['jobPitch'], "cname"=>$cname, "distance" =>$distance, 'job_image' => $jobImageData , "savedjob"=> $jobsstatus ];
                }
            }
        } else {
            $data["hotjobss"][] = ["jobpost_id" => "", "comapnyId" => "", "job_title" => "", "jobDesc" => "", "salary" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "", "save_status" => "", 'job_image'=> '', 'cname'=> '', 'distance'=>'', 'jobPitch' => '', "savedjob"=> $jobstatus];
        }
        return $data["hotjobss"];
        }
    }

    public function getrecentjoblistingss1($id) {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        $tokendata = ['id'=>$id];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        //echo $id;die;
        $hotjobData = $this->Jobpost_Model->recentjob_fetch_latlong($id,$expfilter);
        //print_r($hotjobData);die;
        // echo $this->db->last_query();die;
        if ($hotjobData) {
            foreach ($hotjobData as $hotjobsData) {
                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                //print_r($jobTop);
                /*if (!empty($jobTop) && count($jobTop) > 1) {*/
                    if (isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id'];
                    } else {
                        $datatoppicks1 = "";
                    }
                    if (isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id'];
                    } else {
                        $datatoppicks2 = "";
                    }
                    if (isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id'];
                    } else {
                        $datatoppicks3 = "";
                    }
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $id);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                   /* $ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                      //print_r($output);die;       
                   //Get latitude and longitute from json data
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = '';
                        }
                    $data["hotjobss"][] = ["jobpost_id" => $hotjobsData['id'], "comapnyId" => $hotjobsData['compId'], "job_title" => $hotjobsData['jobtitle'], "jobDesc" => $hotjobsData['jobDesc'], "salary" => $basicsalary, "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData,"jobPitch"=>$hotjobsData['jobPitch'], "cname"=>$cname, "distance" =>$distance, 'jobImage' => $jobImageData  ];
                /*}*/
            }
        } else {
            $data["hotjobss"][] = ["jobpost_id" => "", "comapnyId" => "", "job_title" => "", "jobDesc" => "", "salary" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "", "save_status" => "", 'jobImage'=> '', 'cname'=> '', 'distance'=>'', 'jobPitch' => ''];
        }
        return $data["hotjobss"];
        }
    }
    public function getRecentjoblisting($current_lat, $current_long) {
        /*if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }*/
        $data["recentjobs"] = [];
        $recentjobData = $this->Common_Model->recentjob_fetch_latlongg();
        if ($recentjobData) {
            foreach ($recentjobData as $recentjobsData) {
                $jobTop = $this->Jobpost_Model->job_toppicks($recentjobsData['id']);
                if (!empty($jobTop)) {
                    if (isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id'];
                    } else {
                        $datatoppicks1 = "";
                    }
                    if (isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id'];
                    } else {
                        $datatoppicks2 = "";
                    }
                    if (isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id'];
                    } else {
                        $datatoppicks3 = "";
                    }
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($recentjobsData['compId']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($recentjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($recentjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($recentjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($recentjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($recentjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    /*$ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                      //print_r($output);die;       
                   //Get latitude and longitute from json data
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
                    $distance = $this->distance($current_lat, $current_long, $recentjobsData['latitude'], $recentjobsData['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';
                    $data["recentjobs"][] = ["jobpost_id" => $recentjobsData['id'], "comapnyId" => $recentjobsData['compId'], "job_title" => $recentjobsData['jobtitle'], "jobDesc" => $recentjobsData['jobDesc'], "salary" => $basicsalary, "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData, "cname" =>$cname, "distance" => $distance, "jobPitch" => $recentjobsData['jobPitch']];
                }
            }
        } else {
            $data["recentjobs"][] = ["jobpost_id" => "", "comapnyId" => "", "job_title" => "", "jobDesc" => "", "salary" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "", "save_status" => "", "cname" => "", "distance" => "", "jobPitch" => ""];
        }
        return $data["recentjobs"];
    }
    public function topclients() {
        $clientData = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["token" => $userSess['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        if ($userTokenCheck) {
            $data = ["user_id" => $userTokenCheck[0]['id'], "clients" => $clientData['clients']];
            $clientCheck = $this->User_Model->client_beforeinsertcheck($data);
            if (!empty($clientCheck)) {
                echo $errors = "<p class='text-danger'>client already added</p>";
                die;
            } else {
                if (empty($clientCheck)) {
                    $clientCount = $this->User_Model->client_beforeInsertCount($userTokenCheck[0]['id']);
                    if (count($clientCount) < 5) {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if ($getCompleteness) {
                            if ($getCompleteness[0]['topClient'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["topClient" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                        $data = ["user_id" => $userTokenCheck[0]['id'], "clients" => $clientData['clients']];
                        $clientInserted = $this->User_Model->topClient_insert($data);
                        if ($clientInserted) {
                            echo $success = "<p class='text-success'>client added successfully</p>";
                            die;
                        } else {
                            echo $errors = "Data not inserted";
                            die;
                        }
                    } else {
                        echo $errors = "<p class='text-danger'>You cannot add more than 5 clients.</p>";
                        die;
                    }
                }
            }
        }
    }
    public function topclientsedit() {
        $clientname = $this->input->post('clients');
        $clientid = $this->input->post('clientId');
        $userSess = $this->session->userdata('usersess');
        $data = ["token" => $userSess['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        if ($userTokenCheck) {
            $data = ["user_id" => $userTokenCheck[0]['id'], "clients" => $clientname];
            $clientCheck = $this->User_Model->client_beforeinsertcheck($data);
            if (!empty($clientCheck)) {
                echo $errors = "<p class='text-danger'>client already added</p>";
                die;
            }
            $datacli = ["clients" => $clientname];
            $clientUpdated = $this->User_Model->topClient_update($datacli, $clientid);
            if ($clientUpdated) {
                echo "<p class='text-success'>Client updated successfully</p>";
                die;
            } else {
                echo "failure";
                die;
            }
        }
    }
    public function languageadd() {
        $clientData = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["token" => $userSess['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        if ($userTokenCheck) {
            $data = ["user_id" => $userTokenCheck[0]['id'], "lang_id" => $clientData['language']];
            $langCheck = $this->User_Model->lang_beforeinsertcheck($data);
            if (!empty($langCheck)) {
                echo $errors = "<p class='text-danger'>Language already added</p>";
                die;
            } else {
                if (empty($clientCheck)) {
                    $clientCount = $this->User_Model->lang_beforeInsertCount($userTokenCheck[0]['id']);
                    if (count($clientCount) < 5) {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if ($getCompleteness) {
                            if ($getCompleteness[0]['language'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["language" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                        $data = ["user_id" => $userTokenCheck[0]['id'], "lang_id" => $clientData['language']];
                        $clientInserted = $this->User_Model->lang_insert($data);
                        if ($clientInserted) {
                            echo $success = "<p class='text-success'>Language added successfully</p>";
                            die;
                        } else {
                            echo $errors = "Data not inserted";
                            die;
                        }
                    } else {
                        echo $errors = "<p class='text-danger'>Language not accepted more than 5</p>";
                        die;
                    }
                }
            }
        }
    }
    public function languageedit() {
        $langname = $this->input->post('langname');
        $langId = $this->input->post('langId');
        $userSess = $this->session->userdata('usersess');
        $data = ["token" => $userSess['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        if ($userTokenCheck) {
            $data = ["user_id" => $userTokenCheck[0]['id'], "lang_id" => $langname];
            $clientCheck = $this->User_Model->lang_beforeinsertcheck($data);
            if (!empty($clientCheck)) {
                echo $errors = "<p class='text-danger'>Client already added</p>";
                die;
            }
            $datacli = ["lang_id" => $langname];
            $langUpdated = $this->User_Model->lang_update($datacli, $langId);
            if ($langUpdated) {
                echo "<p class='text-success'>Client updated successfully</p>";
                die;
            } else {
                echo "failure";
                die;
            }
        }
    }
    public function skilladd() {
        $skillData = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["token" => $userSess['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        if ($userTokenCheck) {
            $data = ["user_id" => $userTokenCheck[0]['id'], "skills" => $skillData['skill']];
            $skillCheck = $this->User_Model->skills_beforeinsertcheck($data);
            if (!empty($skillCheck)) {
                echo $errors = "<p class='text-danger'>Skill already added</p>";
                die;
            } else {
                if (empty($skillCheck)) {
                    $clientCount = $this->User_Model->skills_beforeInsertCount($userTokenCheck[0]['id']);
                    if (count($clientCount) < 5) {
                        $data = ["user_id" => $userTokenCheck[0]['id'], "skills" => $skillData['skill']];
                        $clientInserted = $this->User_Model->topSkills_insert($data);
                        if ($clientInserted) {
                            echo $success = "<p class='text-success'>Skill added successfully</p>";
                            die;
                        } else {
                            echo $errors = "Data not inserted";
                            die;
                        }
                    } else {
                        echo $errors = "<p class='text-danger'>skills not accepted more than 5</p>";
                        die;
                    }
                }
            }
        }
    }
    public function skilledit() {
        $skill = $this->input->post('skill');
        $skillId = $this->input->post('skillId');
        $userSess = $this->session->userdata('usersess');
        $data = ["token" => $userSess['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        if ($userTokenCheck) {
            $data = ["user_id" => $userTokenCheck[0]['id'], "skills" => $skill];
            $clientCheck = $this->User_Model->skills_beforeinsertcheck($data);
            if (!empty($clientCheck)) {
                echo $errors = "<p class='text-danger'>Skill alredy added</p>";
                die;
            }
            $datacli = ["skills" => $skill];
            $langUpdated = $this->User_Model->topSkills_update($datacli, $skillId);
            if ($langUpdated) {
                echo "<p class='text-success'>Skill updated successfully</p>";
                die;
            } else {
                echo "failure";
                die;
            }
        }
    }
    public function addprofilemore() {
        $profileData = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["token" => $userSess['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        if ($userTokenCheck) {
            $data = ["user_id" => $userTokenCheck[0]['id'], "strength" => $profileData['strength'], "weakness" => $profileData['weakness'], "achievements" => $profileData['achievements']];
            $checkMore = $this->User_Model->user_more($userTokenCheck[0]['id']);
            if ($checkMore) {
                $checkMoreUpdate = $this->User_Model->user_more_update($data, $userTokenCheck[0]['id']);
            } else {
                $checkMoreInsert = $this->User_Model->user_more_insert($data);
            }
            redirect('dashboard/myprofile');
        } else {
            redirect('user/index');
        }
    }
    public function changeUnReadstatus() {
        $notificationId = $this->input->post('id');
        $status = $this->input->post('Status');
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            if ($status == '2') {
                $data = ["status" => '1'];
            }
            if ($status == '1') {
                $data = ["status" => '1'];
            }
            $notification_status = $this->Jobpost_Model->read_notification($data, $notificationId);
            echo '1';
        }
    }
    public function addReview() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('rating', 'rating', 'trim|required');
        $this->form_validation->set_rules('recommend', 'recommend', 'trim|required');
        $this->form_validation->set_rules('feedback', 'feedback', 'trim|required');
        $this->form_validation->set_rules('recruiter_id', 'recruiter_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', 'Your review has not been added');
            redirect('dashboard/notifications');
        } else {
            $userSess = $this->session->userdata('usersess');
            $data = ["id" => $userSess['id']];
            $userTokenCheck = $this->User_Model->user_match($data);
            if ($userTokenCheck) {
                $data = ["user_id" => $userTokenCheck[0]['id'], "recruiter_id" => $userData['recruiter_id'], "rating" => $userData['rating'], "recommend" => $userData['recommend'], "feedback" => $userData['feedback']];
                $review_inserted = $this->Jobpost_Model->insert_review($data);
                if ($review_inserted) {
                    $this->session->set_flashdata('message', 'Your review has been send');
                    redirect('dashboard/notifications');
                }
            }
        }
    }
    public function addStory() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('story', 'Story', 'trim|required');
        $this->form_validation->set_rules('recruiter_id', 'recruiter_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', 'Your story has not been added');
            redirect('dashboard/notifications');
        } else {
            $userSess = $this->session->userdata('usersess');
            $data = ["id" => $userSess['id']];
            $userTokenCheck = $this->User_Model->user_match($data);
            if ($userTokenCheck) {
                $data = ["user_id" => $userTokenCheck[0]['id'], "recruiter_id" => $userData['recruiter_id'], "story" => $userData['story']];
                $story_inserted = $this->Jobpost_Model->insert_story($data);
                //echo $this->db->last_query();die;
                if ($story_inserted) {
                    $this->session->set_flashdata('message', 'Your story has been added');
                    redirect('dashboard/notifications');
                }
            }
        }
    }

    public function jobDescription() {
        if($this->session->userdata('usersess')) {
            //$decrypt_id = $this->uri->segment(3);
            //$jobpost_id= base64_decode($decrypt_id);
            //echo $jobpost_id;die;
            $descURL = "dashboard/jobDescription?listing=".$this->uri->segment(3);
            redirect($descURL);
        }

        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        $decrypt_id = $this->uri->segment(3);
        $jobpost_id= base64_decode($decrypt_id);
        //echo $decrypt_id;die;
        $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobpost_id);
        $data['jobskills'] = $this->Jobpost_Model->job_skills($jobpost_id);
      
        $companydetail = $this->Jobpost_Model->company_detail_fetch($jobDetails[0]['company_id']);
        if($jobDetails[0]['subrecruiter_id']){
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['subrecruiter_id']);
            }else{
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['recruiter_id']);
            }
        $jobLocation = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
        $recruiteDetail = $this->Jobpost_Model->recruiter_detail_fetch($jobDetails[0]['recruiter_id']);

        if (isset($jobLocation[0]['address'])) {
            $companyaddress = $jobLocation[0]['address'];
        } else {
            $companyaddress = '';
        }
        
        if (isset($jobLocation[0]['phone'])) {
            $companyphone = $jobLocation[0]['phone'];
        } else {
            $companyphone = '';
        }
        
        $data['jobTop'] = $this->Jobpost_Model->job_toppicks($jobDetails[0]['id']);
        $data['jobAllowance'] = $this->Jobpost_Model->job_allowances($jobDetails[0]['id']);
        $data['jobMedical'] = $this->Jobpost_Model->job_medical($jobDetails[0]['id']);
        $data['jobShift'] = $this->Jobpost_Model->job_workshift($jobDetails[0]['id']);
        $data['jobLeaves'] = $this->Jobpost_Model->job_leaves($jobDetails[0]['id']);
        
        
        $jobsal = $this->Jobpost_Model->getExpJob($jobpost_id);
        if ($jobsal[0]['basicsalary']) {
            $basicsalary = $jobsal[0]['basicsalary'];
        } else {
            $basicsalary = "0";
        }

        $jobarrImg = $this->Jobpost_Model->job_images_single($jobpost_id);

        if(count($jobarrImg) > 0) {
            //var_dump($jobImg);die;
            foreach($jobarrImg as $jobImgg) {
                $data['jobImg'][] = ['pic'=>$jobImgg['pic']];
            }

            $companyPic = $this->Jobpost_Model->fetch_companyPic1($jobDetails[0]['company_id']);
            if(!empty($companyPic)) {
                $comPic = $companyPic;

                foreach($comPic as $compImgg) {
                    $data['jobImg'][] = ['pic'=>$compImgg['pic']];
                }    
            }

            $companyPic = $this->Jobpost_Model->fetch_companyPic($jobDetails[0]['company_id']);
            if(!empty($companyPic[0]['companyPic'])) {
                $data['jobImg'][] = ['pic'=>trim($companyPic[0]['companyPic'])];
            }
                
        } else {

            $companyPic = $this->Jobpost_Model->fetch_companyPic1($jobDetails[0]['company_id']);
            if(!empty($companyPic)) {
                $comPic = $companyPic;

                foreach($comPic as $compImgg) {
                    $data['jobImg'][] = ['pic'=>$compImgg['pic']];
                }    
            } else {

                $companyPic = $this->Jobpost_Model->fetch_companyPic($jobDetails[0]['company_id']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $data['jobImg'][] = ['pic'=>trim($companyPic[0]['companyPic'])];
                } else{
                    $data['jobImg'] = [];
                }
            }
        }
        
        $data["jobdetail"] = ["jobpost_id" => $jobDetails[0]['id'], "job_title" => $jobDetails[0]['jobtitle'],"phonecode"=>$companydetail1[0]['phonecode'],"landline"=>$companydetail1[0]['landline'], "education" => $jobDetails[0]['education'], "location" => $companyaddress, "opening" => $jobDetails[0]['opening'], "basicSalary"=>$jobDetails[0]['salary'], "allowances"=>$jobDetails[0]['allowance'], "experience" => $jobDetails[0]['experience'], "jobDesc" => $jobDetails[0]['jobDesc'], "jobPitch" => $jobDetails[0]['jobPitch'],"mode" => $jobDetails[0]['mode'],"modeurl" => $jobDetails[0]['modeurl'], "salary" => $basicsalary,   "companyphone" => $companydetail1[0]["phone"], "email" => $companydetail1[0]["email"] ];

        $data["comapnyDetail"] = ['comapnyId'=> $companydetail[0]['id'], "name" => $companydetail[0]["cname"],"recruiterName" => $recruiteDetail[0]["cname"] ,"recruiter_id" => $jobDetails[0]['recruiter_id'], "phone" => $companyphone, "email" => $companydetail[0]["email"]];
        
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data["states"] = $this->User_Model->getownstate();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        if($this->session->userdata('usersocialsession')) {
            $userfsess = $this->session->userdata('usersocialsession');
        }   
        $data["type"] = $userfsess['type'];

        $this->load->view('jobdetail', $data);
    }

    public function companyProfile() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }

            $company_id = base64_decode($this->uri->segment(3));
            $data['company_pic'] = $this->Jobpost_Model->fetch_companyPic($company_id);
            $data['cname'] = $this->Jobpost_Model->company_detail_fetch($company_id);
            
            $jobDetails = $this->Jobpost_Model->fetch_jobDetail1($company_id);
            if ($jobDetails) {
                
                foreach ($jobDetails as $hotjobsData) {
                
                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['company_id']);
                                
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                        
                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['company_id']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['company_id']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);

                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = '';
                            }
                        $data["jobDetails"][] = ["jobpost_id" => $hotjobsData['id'], "company_id" => $hotjobsData['company_id'], "jobtitle" => $hotjobsData['jobtitle'], "jobDesc" => $hotjobsData['jobDesc'], "salary" => $basicsalary, "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData,"jobPitch"=>$hotjobsData['jobPitch'], "cname"=>$cname, "distance" =>$distance, 'jobImage' => $jobImageData  ];
                    
                }
            } else {
                $data["jobDetails"][] = ["jobpost_id" => "", "company_id" => "", "jobtitle" => "", "jobDesc" => "", "salary" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "", "save_status" => "", 'jobImage'=> '', 'cname'=> '', 'distance'=>'', 'jobPitch' => ''];
            }
            $data['review_list'] = $this->Jobpost_Model->review_lists($company_id);
            $data['glassdoor_review'] = $this->Jobpost_Model->fetch_gcompanyRating($company_id);
            $data['company_toppicks'] = $this->Jobpost_Model->recruiter_toppickss($company_id);
            $data['companyPic'] = $this->Jobpost_Model->fetch_companyPic1($company_id);
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data["nations"] = $this->Common_Model->nation_lists();
            $data["powers"] = $this->Common_Model->power_lists();
            $data['loginURL'] = $this->google->loginURL();
            $data['authUrl'] = $this->facebook->login_url();
            $data['companyDesc'] = $this->Jobpost_Model->company_detail1_fetch($company_id);
            $this->load->view('companydetail', $data);
    }

    public function companyProfileDetails() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                //var_dump($expmonth, $expyear);die;    
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

            $datajobDetails = array();

            $company_id = base64_decode($this->uri->segment(2));

            $datacname = $this->Jobpost_Model->company_detail_fetch($company_id);
            
            $datacompany_pic = $this->Jobpost_Model->fetch_companyPic($company_id);


            $jobDetails = $this->Jobpost_Model->fetch_jobDetail1($userID, $company_id, $expfilter, $current_lat, $current_long);
            //echo count($jobDetails);die;
            if ($jobDetails) {
                $openingsCount = 0;
                foreach ($jobDetails as $hotjobsData) {

                    $openingsCount = $openingsCount + $hotjobsData['opening'];
                
                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData1 = $jobImage[0]['pic'];
                        } else {
                            $jobImageData1 = $comPic;
                        }
                        
                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['company_id']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['company_id']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);

                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = '';
                            }
                        $datajobDetails[] = ["jobpost_id" => $hotjobsData['id'], "company_id" => $hotjobsData['company_id'], "recruiter_id" => $hotjobsData['recruiter_id'], "jobtitle" => $hotjobsData['jobtitle'], "jobDesc" => $hotjobsData['jobDesc'], "salary" => $basicsalary, "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData,"jobPitch"=>$hotjobsData['jobPitch'], "cname"=>$cname, "distance" =>$distance, 'jobImage' => $jobImageData1  ];
                }
            } else {
                $datajobDetails = [];
            }

            if($this->session->userdata('usersess')) {
                $userSess = $this->session->userdata('usersess');
                $data = ["id" => $userSess['id']];
            } else {
                $data = ["id" => 0];
            }

            $catArray = array();
            $getCompanyCategory = $this->Jobpost_Model->category_ids_bycompany($company_id);

            foreach($getCompanyCategory as $getCompanyCat) {

                $getCatData = $this->Jobpost_Model->category_lists_byid($getCompanyCat['category']);
                $jobcountfetch = $this->Newpoints_Model->companycategory_jobcount($data['id'], $company_id, $getCatData[0]['id'], '', $current_lat, $current_long);
                
                $catArray[] = [
                      "id" => $getCatData[0]['id'],
                      "catname" => $getCatData[0]['category'],
                      "jobcount" => $jobcountfetch
                ];
            }

            $data['expertises'] = $catArray;

            $data['review_list'] = $this->Jobpost_Model->review_lists($company_id);
            $data['glassdoor_review'] = $this->Jobpost_Model->fetch_gcompanyRating($company_id);
            $data['company_toppicks'] = $this->Jobpost_Model->recruiter_toppickss($company_id);
            
            $data['companyPic'] = $this->Jobpost_Model->fetch_companyPic1($company_id);

            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data["nations"] = $this->Common_Model->nation_lists();
            $data["powers"] = $this->Common_Model->power_lists();
            $data['loginURL'] = $this->google->loginURL();
            $data['authUrl'] = $this->facebook->login_url();
            $data['companyDesc'] = $this->Jobpost_Model->company_detail1_fetch($company_id);
            $data["jobDetails"] = $datajobDetails;
            $data['cname'] = $datacname;
            $data['company_pic'] = $datacompany_pic;
            $data['jobOpenings'] = $openingsCount;
            $data["states"] = $this->User_Model->getownstate();
            $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
            
            if($this->session->userdata('usersocialsession')) {
                $userfsess = $this->session->userdata('usersocialsession');
            }   
            $data["type"] = $userfsess['type'];

            //var_dump($data['company_pic']);die;

            $this->load->view('company_detail_new', $data);
    }

    public function recruiterProfileDetails() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];

            $expmonth = $exp_month;
            $expyear = $exp_year;

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }
        } else {
            $expfilter = "";
            $userID = 0;
        }

        $dataRecruiterDetails = array();
        $company_id = base64_decode($this->uri->segment(2));
        //$company_id = 615;

        $recruiterDetails = $this->Jobpost_Model->my_recruiter_detail_fetch($company_id);
        
        $datareview_list = $this->Jobpost_Model->review_lists($company_id);
        if(!empty($datareview_list)) {
            $reviewRatingcount = count($datareview_list);
            $reviewRatingsum = array_sum($datareview_list)/$reviewRatingcount;
        }

        $datacompany_toppicks = $this->Jobpost_Model->recruiter_toppickss($company_id);
        $dataJobCount = $this->Jobpost_Model->my_recruiter_jobcount($company_id);
        $dataOpportunities = $this->Jobpost_Model->my_recruiter_opportunities($company_id);
        $dataLocations = $this->Jobpost_Model->my_recruiter_sites_fetch($company_id);
        if($dataLocations) {
            $sitArray = array();
            foreach($dataLocations as $dataLocation) {
                $dataSiteJobCount = $this->Jobpost_Model->my_recruiter_Sitejobcount($dataLocation['id']);
                $dataSiteImages = $this->Jobpost_Model->fetch_companyPic1($dataLocation['id']);

                if(strlen($dataLocation['site_name']) > 2) {
                    $compName = $dataLocation['site_name']; 
                } else {
                    $compName = $dataLocation['cname'];
                }
                $sitArray[] = [
                        "id"=>$dataLocation['id'],
                        "cname"=>$compName,
                        "companyDesc"=>$dataLocation['companyDesc'],
                        "address"=>$dataLocation['address'],
                        "companyPic"=>$dataLocation['companyPic'],
                        "jobcount"=>$dataSiteJobCount[0]['newcount'],
                        "siteimages"=>$dataSiteImages
                ];
            }
        } else {
            $sitArray = [];
        }

        $catArray = array();
        $getCompanyCategory = $this->Jobpost_Model->my_category_ids_byrecruiter($company_id);

        foreach($getCompanyCategory as $getCompanyCat) {
            $getCatData = $this->Jobpost_Model->category_lists_byid($getCompanyCat['category']);
            $jobcountfetch = $this->Newpoints_Model->recruitercategory_jobcount($userID, $company_id, $getCatData[0]['id'], $expfilter, $current_lat, $current_long);
            
            $catArray[] = [
                  "id" => $getCatData[0]['id'],
                  "catname" => $getCatData[0]['category'],
                  "jobcount" => $jobcountfetch
            ];
        }

        // $data["states"] = $this->User_Model->getownstate();
        // $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        if($this->session->userdata('usersocialsession')) {
            $userfsess = $this->session->userdata('usersocialsession');
        }   
        $datatype = $userfsess['type'];

        $dataRecruiterDetails = [
            "id"=>$recruiterDetails[0]['id'],
            "cname"=>$recruiterDetails[0]['cname'],
            "companyDesc"=>$recruiterDetails[0]['companyDesc'],
            "address"=>$recruiterDetails[0]['address'],
            "companyPic"=>$recruiterDetails[0]['companyPic'],
            "reviews"=>$datareview_list,
            "rating"=>$reviewRatingsum,
            "company_toppicks"=>$datacompany_toppicks,
            "jobcount"=>$dataJobCount[0]['newcount'],
            "opportunities"=>$dataOpportunities[0]['newcount'],
            "locationCount"=>count($sitArray),
            "location"=>$sitArray,
            "expertises"=>$catArray,
            "jobLists"=>$this->getRecruiterByJobs($company_id, $userID, $expfilter, $current_lat, $current_long),
            "type"=>$datatype,
            "states" =>$this->User_Model->getownstate(),
            "intrestedin" => $this->Jobpost_Model->filtersubcategory_listsbycatid(9)
        ];

        //var_dump($dataRecruiterDetails);die;

        $this->load->view('recruiter_company', $dataRecruiterDetails);
    }

    public function getRecruiterByJobs($company_id, $userID, $expfilter, $current_lat, $current_long) {

        $jobfetchs = $this->Jobpost_Model->job_fetch_for_recruiter($company_id, $userID, $current_lat, $current_long, $expfilter);
        if ($jobfetchs) {
            foreach ($jobfetchs as $jobfetch) {
                $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);

                if ($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else {
                    $basicsalary = "0";
                }
                
                $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userID);
                $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                if (isset($jobstatus[0]['status'])) {
                    $jobstatus = '1';
                } else {
                    $jobstatus = '0';
                }

                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                if (isset($companydetail1[0]['address'])) {
                    $companyaddress = $companydetail1[0]['address'];
                } else {
                    $companyaddress = '';
                }
                
                $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                if ($jobTop) {
                    if (isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id'];
                    } else {
                        $datatoppicks1 = "";
                    }
                    if (isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id'];
                    } else {
                        $datatoppicks2 = "";
                    }
                    if (isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id'];
                    } else {
                        $datatoppicks3 = "";
                    }
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', '');
                $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }

                $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                if (isset($jobImage[0]['pic'])) {
                    $jobImageData = $jobImage[0]['pic'];
                } else {
                    $jobImageData = $comPic;
                }
                       
                $data1[] = ["jobpost_id" => $jobfetch['id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $cname, "cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$jobfetch['cname'],"distance"=>$distance, "savedjob" => $jobstatus, "mode" => $jobfetch['mode']];
            }
            return $data1;
        } else{
            $data1=[];
            return $data1;
        }
    }

    public function job_listing(){
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        if(!empty($this->input->get('cat'))){
            $category = base64_decode($this->input->get('cat'));
        }else{
            $category = 0;
        }
       
        $hotjobData = $this->Common_Model->hotjob_fetch_latlongg($category);

            if ($hotjobData) {
                foreach ($hotjobData as $hotjobsData) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                    
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                        
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                         $distance = number_format((float)$distance, 2, '.', '');
                        $data["hotjobss"][] = ["jobpost_id" => $hotjobsData['id'],"jobPitch"=>$hotjobsData['jobPitch'], "comapnyId" => $hotjobsData['compId'], "job_title" => $hotjobsData['jobtitle'], "jobDesc" => $hotjobsData['jobDesc'], "mode" => $hotjobsData['mode'], "salary" => $basicsalary, "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData, "cname"=>$cname,"distance"=>$distance];
                    
                }
            } 
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data["nations"] = $this->Common_Model->nation_lists();
            $data["powers"] = $this->Common_Model->power_lists();
            $data['loginURL'] = $this->google->loginURL();
            $data['authUrl'] = $this->facebook->login_url();
        $this->load->view('job_listing',$data);
    }


    public function find_jobs() {
        if($this->session->userdata('locationdata')) {
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $data['checkResume'] = $this->User_Model->resume_single($userID);
            $data['checkStatus'] = $this->User_Model->user_single($userID);

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = (int)$userData[0]['exp_month'];
            $expyear = (int)$userData[0]['exp_year'];

            // $expmonth = $exp_month;
            // $expyear = $exp_year;


            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        

            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }

        } else {
            $expfilter = "";
            $userID = 0;
        }

        if(!empty($this->uri->segment(2))) {
            $getParams = $this->uri->segment(2);
            
            if(!empty($this->uri->segment(3))) {
                
                if($getParams == "city") {
                    $getParamType = $this->uri->segment(3);
                } else {
                    $getParamType = base64_decode($this->uri->segment(3));    
                }
            } else {
                
                if($getParams == "work_from_home" || $getParams == "hmo" || $getParams == "free_food" || $getParams == "14_month_pay" || $getParams == "signing_bonus" || $getParams == "day_shift") {

                    if($getParams == "work_from_home") {
                        $getParamType = 7;
                    
                    } else if($getParams == "free_food") {
                        $getParamType = 2;

                    } else if($getParams == "14_month_pay") {
                        $getParamType = 6;

                    } else if($getParams == "signing_bonus") {
                        $getParamType = 1;

                    } else if($getParams == "day_shift") {
                        $getParamType = 5;

                    } else if($getParams == "hmo") {
                        $getParamType = 3;
                    }
                    $getParams = "toppicks";

                } else {

                    $getParamType = 1;
                }
            }

        } else {
            $getParams = "nearby";
            $getParamType = 1;
        }

        $data["ourPartners"] = $this->getourpartners($userID, $expfilter, $current_lat, $current_long);
        $companyidsarray = array();
        foreach($data["ourPartners"] as $ourPartners) {
            $companyidsarray[] = $ourPartners['parent_id'];
        }
        $companyidsarray = array_unique($companyidsarray);


        $data["hotjobss"] = $this->viewallListings($getParams, $getParamType, $userID, $current_lat, $current_long, $expfilter, $companyidsarray);

        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data['openings'] = $this->Jobpost_Model->job_openings();
        $data['ad_lists'] = $this->Jobpost_Model->advertise_lists_random($current_lat, $current_long);

        $data["states"] = $this->User_Model->getownstate();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        if($this->session->userdata('usersocialsession')) {
            $userfsess = $this->session->userdata('usersocialsession');
        }   
        $data["type"] = $userfsess['type'];

        $this->load->view('job_listing_new',$data);
    }

    public function viewallListings($getParams, $getParamType, $userID, $userLat, $userLong, $expfilter, $companyidsarray) {

        $data1 = array();
        $negletarr = array();

        if($getParams == "hotjob") {
            
            $bostArr = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobData = $this->Homejob_Model->hotjob_fetch_latlongbylimit_groupby($userID, $expfilter, $userLat, $userLong, $companyidsarr);
                foreach ($hotjobData as $hotjobsData) {
                    $negletarr[] = $hotjobsData['id'];

                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                    if(!empty($jobTop) && count($jobTop)>1) {

                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else {
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else {
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else {
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address']))
                        {
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else
                        {
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                        if(isset($jobImage[0]['pic']))
                        {
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else
                        {
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1[] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "recruiter_id" => $hotjobsData['recruiter_id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "salary" => number_format($hotjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $hotjobsData['mode'], 
                            "modeurl"=> $hotjobsData['modeurl'], 
                        ];
                    }
                }
            }

            usort($data1, function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            $boostjobData = $this->Jobpost_Model->boostjob_fetch_latlong($userID, $expfilter, $userLat, $userLong);
            foreach ($boostjobData as $boostjobsData) {
                if(is_array($boostjobsData['id'], $negletarr)) { } else {
                    $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                    if(!empty($jobTop) && count($jobTop)>=1) {

                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $bostArr[] = $boostjobsData['id'];

                        $data1[] = [
                                "jobpost_id" => $boostjobsData['id'],
                                "recruiter_id" => $boostjobsData['recruiter_id'],
                                "comapnyId" =>$boostjobsData['compId'],
                                "job_title" => $boostjobsData['jobtitle'],
                                "jobDesc" => $boostjobsData['jobDesc'],
                                "jobPitch"=>$boostjobsData['jobPitch'],
                                "salary" => number_format($boostjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $boostjobsData['latitude'], 
                                "longitude"=>$boostjobsData['longitude'],
                                "jobexpire"=> $boostjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$boostjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $boostjobsData['mode'],
                                "modeurl"=> $boostjobsData['modeurl'],
                        ];
                    }
                }
            }

            $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlong($userID, $expfilter, $userLat, $userLong);
            foreach ($hotjobData as $hotjobsData) {

                if(in_array($hotjobsData['id'], $bostArr)) {
                } else {

                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                
                    if(!empty($jobTop) && count($jobTop)>1) {

                          //echo "</pre>";    
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else {
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else {
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else {
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);

                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                         $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address']))
                        {
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else
                        {
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                        if(isset($jobImage[0]['pic']))
                        {
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else
                        {
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1[] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "recruiter_id" => $hotjobsData['recruiter_id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "salary" => number_format($hotjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $hotjobsData['mode'], 
                            "modeurl"=> $hotjobsData['modeurl'], 
                        ];
                    }
                }
            }
            return $data1;
        
        } else if($getParams == "company") {

            $company_lists = $this->Newpoints_Model->companysite_lists($userLat, $userLong);
            if(!empty($company_lists)) {
                $x=0;

                foreach ($company_lists as $company_list) {
                    $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userID, $company_list['id']);
                    if($jobcountfetch > 0) {
                        if(strlen($company_list['companyPic']) > 0) {
                            $companylist[$x] = [
                                  "id" => $company_list['id'],
                                  "image" => $company_list['companyPic'],
                                  "cname" => $company_list['cname'],
                                  "jobcount" => $jobcountfetch
                            ];
                            $x++;
                        }
                    }
                }
            } else {
                $companylist = [];
            }

            $jobcount = array();
            foreach ($companylist as $key => $row)
            {
                $jobcount[$key] = $row['jobcount'];
            }
            array_multisort($jobcount, SORT_DESC, $companylist);
            $data1 = $companylist;

            return $data1;

        } else if($getParams == "expertise") {

            $getParamType = (int)$getParamType;
            $hotjobData = $this->Newpoints_Model->categoryjob_fetch_latlong($userID, $expfilter, $getParamType, $userLat, $userLong);

            $data1 = array();
            foreach ($hotjobData as $hotjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);

                if(isset($jobTop[0]['picks_id'])){
                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                } else {
                    $datatoppicks1 = "";
                }
                if(isset($jobTop[1]['picks_id'])) {
                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                } else {
                    $datatoppicks2 = "";
                }
                if(isset($jobTop[2]['picks_id'])) {
                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                } else {
                    $datatoppicks3 = "";
                }
                
                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);

                if(!empty($savedjob[0]['id'])) {
                    $savedjob = '1';
                } else{
                    $savedjob = "0";
                }

                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else{
                    $basicsalary = 0;
                }

                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if(isset($companydetail1[0]['address']))
                {
                    $companyaddress=$companydetail1[0]['address'];
                }else{
                    $companyaddress='';
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                if(isset($jobImage[0]['pic'])){
                    $jobImageData=$jobImage[0]['pic'];
                }else{
                    $jobImageData=$comPic;
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', ''); 

                $data1[] = [
                    "jobpost_id" => $hotjobsData['id'],
                    "recruiter_id" => $hotjobsData['recruiter_id'],
                    "comapnyId" =>$hotjobsData['compId'],
                    "job_title" => $hotjobsData['jobtitle'],
                    "jobDesc" => $hotjobsData['jobDesc'],
                    "salary" => number_format($hotjobsData['salary']),
                    "companyName"=> $companydetail[0]["cname"],
                    "cname"=> $cname,
                    "companyAddress"=>$companyaddress,
                    "toppicks1" => $datatoppicks1,
                    "toppicks2" => $datatoppicks2,
                    "toppicks3" => $datatoppicks3,
                    "job_image" =>$jobImageData,
                    "latitude"=> $hotjobsData['latitude'], 
                    "longitude"=>$hotjobsData['longitude'],
                    "jobexpire"=> $hotjobsData['jobexpire'],
                    "companyPic"=> $comPic,
                    "savedjob" =>$savedjob,
                    "boostjob" =>$hotjobsData['boost_status'],
                    "distance" => $distance,
                    "mode"=> $hotjobsData['mode'],
                    "modeurl"=> $hotjobsData['modeurl'],
                ];
            }

            return $data1;
        
        } else if($getParams == "nearby") {
        
            foreach($companyidsarray as $companyidsarr) {
                
                $hotjobData = $this->Homejob_Model->job_fetch_homeforappnearby_groupby($userID, $userLat, $userLong, $expfilter, $companyidsarr);
                foreach ($hotjobData as $hotjobsData) {
                    $negletarr[] = $hotjobsData['id'];
                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else {
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else {
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else {
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if(isset($companydetail1[0]['address'])) {
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else {
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if(isset($jobImage[0]['pic'])) {
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else {
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $data1[] = [
                        "jobpost_id" => $hotjobsData['id'],
                        "recruiter_id" => $hotjobsData['recruiter_id'],
                        "comapnyId" =>$hotjobsData['compId'],
                        "job_title" => $hotjobsData['jobtitle'],
                        "jobDesc" => $hotjobsData['jobDesc'],
                        "salary" => number_format($hotjobsData['salary']),
                        "companyName"=> $companydetail[0]["cname"],
                        "cname"=> $cname,
                        "companyAddress"=>$companyaddress,
                        "toppicks1" => $datatoppicks1,
                        "toppicks2" => $datatoppicks2,
                        "toppicks3" => $datatoppicks3,
                        "job_image" =>$jobImageData,
                        "latitude"=> $hotjobsData['latitude'], 
                        "longitude"=>$hotjobsData['longitude'],
                        "jobexpire"=> $hotjobsData['jobexpire'],
                        "companyPic"=> $comPic,
                        "savedjob" =>$savedjob,
                        "boostjob" =>$hotjobsData['boost_status'],
                        "distance" => $distance,
                        "mode"=> $hotjobsData['mode'],
                        "modeurl"=> $hotjobsData['modeurl'],
                    ];
                }    
            }

            usort($data1, function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            $hotjobData = $this->Newpoints_Model->nearbyjob_fetch_latlong($userID, $userLat, $userLong, $expfilter);
            foreach ($hotjobData as $hotjobsData) {

                if(is_array($hotjobsData['id'], $negletarr)) { } else {
                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else {
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else {
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else {
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if(isset($companydetail1[0]['address'])) {
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else {
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if(isset($jobImage[0]['pic'])) {
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else {
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $data1[] = [
                        "jobpost_id" => $hotjobsData['id'],
                        "recruiter_id" => $hotjobsData['recruiter_id'],
                        "comapnyId" =>$hotjobsData['compId'],
                        "job_title" => $hotjobsData['jobtitle'],
                        "jobDesc" => $hotjobsData['jobDesc'],
                        "salary" => number_format($hotjobsData['salary']),
                        "companyName"=> $companydetail[0]["cname"],
                        "cname"=> $cname,
                        "companyAddress"=>$companyaddress,
                        "toppicks1" => $datatoppicks1,
                        "toppicks2" => $datatoppicks2,
                        "toppicks3" => $datatoppicks3,
                        "job_image" =>$jobImageData,
                        "latitude"=> $hotjobsData['latitude'], 
                        "longitude"=>$hotjobsData['longitude'],
                        "jobexpire"=> $hotjobsData['jobexpire'],
                        "companyPic"=> $comPic,
                        "savedjob" =>$savedjob,
                        "boostjob" =>$hotjobsData['boost_status'],
                        "distance" => $distance,
                        "mode"=> $hotjobsData['mode'],
                        "modeurl"=> $hotjobsData['modeurl'],
                    ];
                }
            }

            return $data1;

        } else if($getParams == "toppicks") {

            foreach($companyidsarray as $companyidsarr) {

                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_all_groupby($userID, $expfilter, $userLat, $userLong, $companyidsarr, $getParamType);
                if ($hotjobDataonegroup) {
                    foreach ($hotjobDataonegroup as $hotjobsData) {
                        $neglectarr[] = $hotjobsData['id'];
                        $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $getParamType);
                        $datatoppicks1 = $getParamType; 
                        
                        if(isset($jobTopseleted[0]['picks_id'])) {
                            $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTopseleted[1]['picks_id'])) {
                            $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                        
                        $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $userID);
                        $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobsavedStatus[0]['status'])) {
                            $jobsstatus = '1';
                        } else {
                            $jobsstatus = '0';
                        }
                        
                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = '';
                        }
                        $data1[] = ["jobpost_id" => $hotjobsData['id'], 
                                  "comapnyId" => $hotjobsData['compId'],
                                  "recruiter_id" => $hotjobsData['recruiter_id'],
                                  "job_title" => $hotjobsData['jobtitle'], 
                                  "jobDesc" => $hotjobsData['jobDesc'], 
                                  "salary" => number_format($basicsalary), 
                                  "companyName" => $companydetail[0]["cname"], 
                                  "companyAddress" => $companyaddress,
                                  "job_image" => $jobImageData,
                                  "jobPitch"=>$hotjobsData['jobPitch'], 
                                  "cname"=>$cname, 
                                  "distance" =>$distance, 
                                  'job_image' =>$jobImageData,
                                  "savedjob"=> $jobsstatus,
                                  "toppicks1" => $datatoppicks1,
                                  "toppicks2" => $datatoppicks2,
                                  "toppicks3" => $datatoppicks3,
                                  "mode"=>$hotjobsData['mode'], 
                        ];

                    }
                }
            }

            usort($data1, function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            $hotjobData = $this->Newpoints_Model->feturedtoppicks_fetch_latlong($userID, $expfilter, $userLat, $userLong);
            foreach ($hotjobData as $hotjobsData) {
                
                if(is_array($hotjobsData['id'], $negletarr)) { } else {
                    $jobTop = $this->Jobpost_Model->job_toppicks1($hotjobsData['id']);
                    $pids = array(); 
                    foreach($jobTop as $jobTopp) {
                        $pids[] = $jobTopp['picks_id'];
                    }

                    if(in_array($getParamType, $pids)) {
                        if(!empty($jobTop) && count($jobTop)>=1) {
                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $getParamType);
                            $datatoppicks1 = $getParamType;
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);

                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if(isset($companydetail1[0]['address']))
                            {
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else
                            {
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                            if(isset($jobImage[0]['pic']))
                            {
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else
                            {
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $data1[] = [
                                "jobpost_id" => $hotjobsData['id'],
                                "recruiter_id" => $hotjobsData['recruiter_id'],
                                "comapnyId" =>$hotjobsData['compId'],
                                "job_title" => $hotjobsData['jobtitle'],
                                "jobDesc" => $hotjobsData['jobDesc'],
                                "salary" => number_format($hotjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $hotjobsData['latitude'], 
                                "longitude"=>$hotjobsData['longitude'],
                                "jobexpire"=> $hotjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$hotjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $hotjobsData['mode'],
                                "modeurl"=> $hotjobsData['modeurl'],
                            ];
                        }
                    }
                }
            }

            return $data1;
        
        } else if($getParams == "Allowance") {

            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_allowances_groupby($userID, $expfilter, $userLat, $userLong, $companyidsarr);
                if ($hotjobDataonegroup) {
                    foreach ($hotjobDataonegroup as $hotjobsData) {
                        $neglectarr[] = $hotjobsData['id'];
                        $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $getParamType);
                        $datatoppicks1 = $getParamType; 
                        
                        if(isset($jobTopseleted[0]['picks_id'])) {
                            $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTopseleted[1]['picks_id'])) {
                            $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                        
                        $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $userID);
                        $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobsavedStatus[0]['status'])) {
                            $jobsstatus = '1';
                        } else {
                            $jobsstatus = '0';
                        }
                        
                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = '';
                        }
                        $data1[] = ["jobpost_id" => $hotjobsData['id'], 
                                  "comapnyId" => $hotjobsData['compId'],
                                  "recruiter_id" => $hotjobsData['recruiter_id'],
                                  "job_title" => $hotjobsData['jobtitle'], 
                                  "jobDesc" => $hotjobsData['jobDesc'], 
                                  "salary" => number_format($basicsalary), 
                                  "companyName" => $companydetail[0]["cname"], 
                                  "companyAddress" => $companyaddress,
                                  "job_image" => $jobImageData,
                                  "jobPitch"=>$hotjobsData['jobPitch'], 
                                  "cname"=>$cname, 
                                  "distance" =>$distance, 
                                  'job_image' =>$jobImageData,
                                  "savedjob"=> $jobsstatus,
                                  "toppicks1" => $datatoppicks1,
                                  "toppicks2" => $datatoppicks2,
                                  "toppicks3" => $datatoppicks3,
                                  "mode"=>$hotjobsData['mode'], 
                        ];

                    }
                }
            }

            usort($data1, function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            $hotjobData = $this->Newpoints_Model->feturedtoppicks_fetch_latlong($userID, $expfilter, $userLat, $userLong);
            foreach ($hotjobData as $hotjobsData) {

                if(is_array($hotjobsData['id'], $negletarr)) { } else {
                    $jobTop = $this->Jobpost_Model->job_allowancesbyid($hotjobsData['id'], $getParamType);
                    if(!empty($jobTop) && count($jobTop)>=1) {

                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else {
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else {
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else {
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1[] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "recruiter_id" => $hotjobsData['recruiter_id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "salary" => number_format($hotjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $hotjobsData['mode'],
                            "modeurl"=> $hotjobsData['modeurl'],
                        ];
                    }
                }
            }

            return $data1;

        } else if($getParams == "leadership") {

            foreach($companyidsarray as $companyidsarr) {
                $boostjobData = $this->Homejob_Model->catjob_fetch_leadership($userID, $expfilter, $userLat, $userLong, $companyidsarr);
                foreach ($boostjobData as $boostjobsData) {
                    $negletarr[] = $boostjobsData['id'];
                    $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                    if(!empty($jobTop) && count($jobTop)>=0) {

                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $bostArr[] = $boostjobsData['id'];

                        $data1[] = [
                                "jobpost_id" => $boostjobsData['id'],
                                "recruiter_id" => $boostjobsData['recruiter_id'],
                                "comapnyId" =>$boostjobsData['compId'],
                                "job_title" => $boostjobsData['jobtitle'],
                                "jobDesc" => $boostjobsData['jobDesc'],
                                "jobPitch"=>$boostjobsData['jobPitch'],
                                "salary" => number_format($boostjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $boostjobsData['latitude'], 
                                "longitude"=>$boostjobsData['longitude'],
                                "jobexpire"=> $boostjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$boostjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $boostjobsData['mode'], 
                                "modeurl"=> $boostjobsData['modeurl'], 
                        ];
                    }
                }    
            }

            usort($data1, function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            $boostjobData = $this->Jobpost_Model->catjob_fetch_leadership_all($userID, $expfilter, $userLat, $userLong);
            foreach ($boostjobData as $boostjobsData) {

                if(is_array($boostjobsData['id'], $negletarr)) { } else {
                    $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                    if(!empty($jobTop) && count($jobTop)>=0) {

                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 
                        $data1[] = [
                                "jobpost_id" => $boostjobsData['id'],
                                "recruiter_id" => $boostjobsData['recruiter_id'],
                                "comapnyId" =>$boostjobsData['compId'],
                                "job_title" => $boostjobsData['jobtitle'],
                                "jobDesc" => $boostjobsData['jobDesc'],
                                "jobPitch"=>$boostjobsData['jobPitch'],
                                "salary" => number_format($boostjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $boostjobsData['latitude'], 
                                "longitude"=>$boostjobsData['longitude'],
                                "jobexpire"=> $boostjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$boostjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $boostjobsData['mode'], 
                                "modeurl"=> $boostjobsData['modeurl'], 
                        ];
                    }
                }
            }

            return $data1;

        }  else if($getParams == "information_technology") {

            foreach($companyidsarray as $companyidsarr) {
                $boostjobData = $this->Homejob_Model->catjob_fetch_information_technology($userID,$expfilter, $userLat, $userLong, $companyidsarr);
                foreach ($boostjobData as $boostjobsData) {

                    $negletarr[] = $boostjobsData['id'];
                    $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                    if(!empty($jobTop) && count($jobTop)>=0) {

                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $bostArr[] = $boostjobsData['id'];

                        $data1[] = [
                                "jobpost_id" => $boostjobsData['id'],
                                "recruiter_id" => $boostjobsData['recruiter_id'],
                                "comapnyId" =>$boostjobsData['compId'],
                                "job_title" => $boostjobsData['jobtitle'],
                                "jobDesc" => $boostjobsData['jobDesc'],
                                "jobPitch"=>$boostjobsData['jobPitch'],
                                "salary" => number_format($boostjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $boostjobsData['latitude'], 
                                "longitude"=>$boostjobsData['longitude'],
                                "jobexpire"=> $boostjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$boostjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $boostjobsData['mode'], 
                                "modeurl"=> $boostjobsData['modeurl'], 
                        ];
                    }
                }
            }

            usort($data1, function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            $boostjobData = $this->Jobpost_Model->catjob_fetch_information_technology_all($userID,$expfilter, $userLat, $userLong);
            foreach ($boostjobData as $boostjobsData) {

                if(is_array($boostjobsData['id'], $negletarr)) { } else {
                    $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                    if(!empty($jobTop) && count($jobTop)>=0) {

                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $bostArr[] = $boostjobsData['id'];

                        $data1[] = [
                                "jobpost_id" => $boostjobsData['id'],
                                "recruiter_id" => $boostjobsData['recruiter_id'],
                                "comapnyId" =>$boostjobsData['compId'],
                                "job_title" => $boostjobsData['jobtitle'],
                                "jobDesc" => $boostjobsData['jobDesc'],
                                "jobPitch"=>$boostjobsData['jobPitch'],
                                "salary" => number_format($boostjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $boostjobsData['latitude'], 
                                "longitude"=>$boostjobsData['longitude'],
                                "jobexpire"=> $boostjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$boostjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $boostjobsData['mode'], 
                                "modeurl"=> $boostjobsData['modeurl'], 
                        ];
                    }
                }
            }

            return $data1;
        
        }  else if($getParams == "city") {

            $getCity = $this->Jobpost_Model->singlecity_fetch($getParamType);
            $boostjobData = $this->Jobpost_Model->cityjob_fetch($userID, $expfilter, $userLat, $userLong, $getCity[0]['name']);
            $data1 = array();
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                //if(!empty($jobTop) && count($jobTop)>=0) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "recruiter_id" => $boostjobsData['recruiter_id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                            "modeurl"=> $boostjobsData['modeurl'], 
                    ];
                //}
            }

            return $data1;
        
        }  else if($getParams == "instant_screening") {

            foreach($companyidsarray as $companyidsarr) {
                $boostjobData = $this->Jobpost_Model->instantscreening_fetch_groupby($userID,$expfilter, $userLat, $userLong, $companyidsarr);
                foreach ($boostjobData as $boostjobsData) {
                    $negletarr[] = $boostjobsData['id'];
                    $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                    if(!empty($jobTop) && count($jobTop)>=0) {

                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $bostArr[] = $boostjobsData['id'];

                        $data1[] = [
                                "jobpost_id" => $boostjobsData['id'],
                                "recruiter_id" => $boostjobsData['recruiter_id'],
                                "comapnyId" =>$boostjobsData['compId'],
                                "job_title" => $boostjobsData['jobtitle'],
                                "jobDesc" => $boostjobsData['jobDesc'],
                                "jobPitch"=>$boostjobsData['jobPitch'],
                                "salary" => number_format($boostjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $boostjobsData['latitude'], 
                                "longitude"=>$boostjobsData['longitude'],
                                "jobexpire"=> $boostjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$boostjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $boostjobsData['mode'], 
                                "modeurl"=> $boostjobsData['modeurl'], 
                        ];
                    }                    
                }
            }

            usort($data1, function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            $boostjobData = $this->Jobpost_Model->instantscreening_fetch_all($userID,$expfilter, $userLat, $userLong);
            foreach ($boostjobData as $boostjobsData) {

                if(is_array($boostjobsData['id'], $negletarr)) { } else {
                    $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                    if(!empty($jobTop) && count($jobTop)>=0) {

                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $bostArr[] = $boostjobsData['id'];

                        $data1[] = [
                                "jobpost_id" => $boostjobsData['id'],
                                "recruiter_id" => $boostjobsData['recruiter_id'],
                                "comapnyId" =>$boostjobsData['compId'],
                                "job_title" => $boostjobsData['jobtitle'],
                                "jobDesc" => $boostjobsData['jobDesc'],
                                "jobPitch"=>$boostjobsData['jobPitch'],
                                "salary" => number_format($boostjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $boostjobsData['latitude'], 
                                "longitude"=>$boostjobsData['longitude'],
                                "jobexpire"=> $boostjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$boostjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $boostjobsData['mode'], 
                                "modeurl"=> $boostjobsData['modeurl'], 
                        ];
                    }
                }
            }

            return $data1;
        
        }  else if($getParams == "site") {

            $boostjobData = $this->Jobpost_Model->sitejobs_fetch_all($userID, $expfilter, $userLat, $userLong, $getParamType);

            $data1 = array();
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                if(count($jobTop)>=0) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "recruiter_id" => $boostjobsData['recruiter_id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                            "modeurl"=> $boostjobsData['modeurl'], 
                    ];
                }
            }

            return $data1;
        
        }  else if($getParams == "company_jobs") {

            $boostjobData = $this->Jobpost_Model->companyjobs_fetch_all($userID, $expfilter, $userLat, $userLong, $getParamType);

            $data1 = array();
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                if(!empty($jobTop) && count($jobTop)>=0) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "recruiter_id" => $boostjobsData['recruiter_id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                            "modeurl"=> $boostjobsData['modeurl'], 
                    ];
                }
            }

            return $data1;
        
        }

    }


    public function jobLists() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
            $userLat = $_GET['lat'];
            $userLong = $_GET['long'];
            //$job_id = $_GET['job_id'];
            $jobfetchs = $this->Jobpost_Model->job_fetch_latlongg($userLat, $userLong);
            //echo $this->db->last_query();die;
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    //print_r($jobfetch);
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    //print_r($jobsal);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    //print_r($jobstatus);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    //$jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);
                    //print_r($jobApplyfetch);die;
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    /*$ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);*/
                     
                   /*$dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
                    $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    //$distance = '';
                    $distance = number_format((float)$distance, 2, '.', '');
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                           
                        $data1["hotjobss"][] = ["jobpost_id" => $jobfetch['id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => $basicsalary, "companyName" => $jobfetch['cname'],"cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$cname,"distance"=>$distance];
                    
                }
                $data1["phonecodes"] = $this->Common_Model->phonecode_lists();
                $data["nations"] = $this->Common_Model->nation_lists();
                $data["powers"] = $this->Common_Model->power_lists();
                $data['loginURL'] = $this->google->loginURL();
                $data['authUrl'] = $this->facebook->login_url();
                //$data1['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
                $this->load->view("job_listing", $data1);
            } 
    }

    public function home_search() {
        //print_r($locationdata);die;
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
            
            $filterData = $this->input->post();

            if(empty($filterData['lat'])) {
                $filterData['lat'] = $current_lat;
            }
            if(empty($filterData['long'])) {
                $filterData['long'] = $current_long;
            }
            
            if (empty($filterData['locationn']) &&  $filterData['locationn'] == "" && empty($filterData['cname']) && $filterData['cname'] == "" && $filterData['jobsubcategory'] == "" && empty($filterData['joblevell'])  &&  empty($filterData['jobcategoryy'])) {
            
                $this->session->set_tempdata('homeerr','At least one field is required to search.');
                redirect('');
            
            } else {
                if (!empty($filterData['joblevell'][0]) && $filterData['joblevell'][0] == 9) {
                    $filterData['joblevell'] = array(6, 4, 7, 8);
                }
                if (!empty($filterData['jobcategoryy'][0]) && $filterData['jobcategoryy'][0] == 9) {
                    $filterData['jobcategoryy'] = array(5, 3, 4, 6, 7, 8);
                }


            $jobfetchs = $this->Jobpost_Model->homesearch($filterData);
            //var_dump($jobfetchs);die;

            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }

                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }

                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }

                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['company_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($filterData['lat'], $filterData['long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");

                    $distance = number_format((float)$distance, 2, '.', '');
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                           
                        $data1["hotjobss"][] = ["jobpost_id" => $jobfetch['id'], "comapnyId" => $jobfetch['company_id'], "recruiter_id" => $jobfetch['recruiter_id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'], "distance"=>$distance, "salary" => $basicsalary, "companyName" => $cname,"cname"=> $jobfetch['cname'],"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData,"distance"=>$distance, "mode" => $jobfetch['mode'], "modeurl" => $jobfetch['modeurl']];
                    
                }
                $data["phonecodes"] = $this->Common_Model->phonecode_lists();
                $data["nations"] = $this->Common_Model->nation_lists();
                $data["powers"] = $this->Common_Model->power_lists();
                $data['loginURL'] = $this->google->loginURL();
                $data['authUrl'] = $this->facebook->login_url();
                $data['openings'] = $this->Jobpost_Model->job_openings();
                $data['ad_lists'] = $this->Jobpost_Model->advertise_lists_random($current_lat, $current_long);
                $data["states"] = $this->User_Model->getownstate();
                $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
                
                if($this->session->userdata('usersocialsession')) {
                    $userfsess = $this->session->userdata('usersocialsession');
                }   
                $data["type"] = $userfsess['type'];
                
                $this->load->view("job_listing_new", $data1);
            
            }else{
                redirect('');
            } 
        }
    }

    public function search1() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        $filterData = $this->input->post();

        if(empty($filterData['lat'])) {
            $filterData['lat'] = $current_lat;
        }
        if(empty($filterData['long'])) {
            $filterData['long'] = $current_long;
        }

        if (!empty($filterData['joblevel']) && $filterData['joblevel'] == 9) {
            $joblevel = arrar(6, 4, 7, 8);
            $joblevell = implode(",", $joblevel);
            $filterData['joblevel'] = $joblevell;
        }
        if (!empty($filterData['toppicks'])) {
            $jobtopiks = implode(",", $filterData['toppicks']);
            $filterData['toppicks'] = $jobtopiks;
        }
        if (!empty($filterData['medical'])) {
            $jobmedical = implode(",", $filterData['medical']);
            $filterData['medical'] = $jobmedical;
        }
        if (!empty($filterData['allowances'])) {
            $joballowance = implode(",", $filterData['allowances']);
            $filterData['allowances'] = $joballowance;
        }
        if (!empty($filterData['workshift'])) {
            $jobworkshift = implode(",", $filterData['workshift']);
            $filterData['workshift'] = $jobworkshift;
        }
        if (!empty($filterData['leaves'])) {
            $jobleaves = implode(",", $filterData['leaves']);
            $filterData['leaves'] = $jobleaves;
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if(!empty($userTokenCheck[0]['exp_year'])){
            $expyear = $userTokenCheck[0]['exp_year'];
        }else{
            $expyear='';
        }
        if(!empty($userTokenCheck[0]['exp_month'])){
            $expmonth = $userTokenCheck[0]['exp_month'];
        }else{
            $expmonth='';
        }
        /*if(  $filterData['exp_year']==''  && $filterData['exp_month']==''){
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];
        }else{
            $expmonth = $filterData['exp_month'];
            $expyear = $filterData['exp_year'];
            
        }*/

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $data["jobList"] = [];
        
        if ($userTokenCheck) {
            
            if (empty($filterData['locationn']) &&  $filterData['locationn'] == "" && empty($filterData['cname']) && $filterData['cname'] == "" && $filterData['jobsubcategory'] == "" && empty($filterData['joblevell'])  &&  empty($filterData['jobcategoryy'])) {
                $this->session->set_tempdata('homeerr','At least one field is required to search.');
                redirect('');
            } else {
            
            $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            //echo $this->db->last_query();die;
            //print_r($resultstatus);die;
            
            if ($jobfetchs) {
            
                foreach ($jobfetchs as $jobfetch) {
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($jobfetch['id']);
            
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    /*$jobsal = $this->Jobpost_Model->getExpJob1($jobfetch['id'],$expfilter);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = " ";
                    }*/
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['company_id']);
                    //print_r($companydetail1);die;
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if (!empty($jobTop)) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    
                    $distance = $this->distance($current_lat,$current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //    $distance = '';
                    $jobListing[] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'], "salary" => $jobfetch['salary'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "company_id" => $jobfetch['company_id'], "companyName" => $jobfetch['cname'],"cname"=> $cname, "jobPitch"=>$jobfetch['jobPitch'], "mode"=>$jobfetch['mode'], "modeurl"=>$jobfetch['modeurl'], "latitude" => $jobfetch['latitude'], "longitude" => $jobfetch['longitude'], "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "save_status" => $jobsstatus, "job_image" => $jobImageData,"distance"=>$distance];
                }
                $data["jobList"] = $jobListing;
                if (count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else {
                $data["jobList"] = [];
            }
            if (!empty($filterData['salarySort']) && $filterData['salarySort'] == 1) {
                $price = array();
                foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
                }
                array_multisort($price, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            }
            $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            //echo $this->db->last_query();die;
            $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

            $jobcountfetchs = $returnnewarray;
            if ($jobcountfetchs) {
                $xx = 1;
                foreach ($jobcountfetchs as $jobcountfetch) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        if(!empty($dataJobList[0])){
                            if($dataJobList[0]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[0]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[0]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[0]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[0]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[0]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[1])){
                            if($dataJobList[1]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[1]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[1]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[1]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[1]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[1]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[2])){
                            if($dataJobList[2]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[2]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[2]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[2]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[2]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[2]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }
                        
                        if(count($jobTop)>1){
                            $url = base_url()."markericon/j_hot_job.png";
                        }

                        if(empty($dataJobList[0]) && empty($dataJobList[1]) && empty($dataJobList[2])){
                            $url = base_url()."markericon/maps.png";
                        }
                        $urll = base_url()."dashboard/jobLists?lat=".$jobcountfetch['latitude']."&long=".$jobcountfetch['longitude'];
                        if($jobcountfetch['jobcount']==1){
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'">' .$jobcountfetch['jobcount'] . ' Job'.'<br>'.'   '.($jobcountfetch["salary"]/1000).'K </a></span>';
                        }else{
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'">' .$jobcountfetch['jobcount'] . ' Jobs'.'<br>'.'   '.($jobcountfetch["salary"]/1000).'K </a></span>';
                        }
                        
                        
                    $dataget[$xx] = ["info" => $info, "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], "jobid" => $jobcountfetch['id'], "url" => $url];
                    $xx++;
                }
                //$dataget1 = json_encode($dataget);
                $data['jobcount'] = $dataget;
            } else {
                $data['jobcount'] = [];
            }
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['languagelist'] = $this->Common_Model->language_lists();
            $data['level_list'] = $this->Common_Model->level_lists();
            $data['category_list'] = $this->Common_Model->category_lists();
            if (!empty($filterData['salarySort'])) {
                $data['salarysort'] = $filterData['salarySort'];
            }
            if (!empty($filterData['locationSort'])) {
                $data['locationsort'] = $filterData['locationSort'];
            }
            if (!empty($filterData['ratingSort'])) {
                $data['ratingsort'] = $filterData['ratingSort'];
            }
             $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
             $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['id']); 
             $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
            if($userTokenCheck[0]['exp_month']>=0   && $userTokenCheck[0]['exp_year']>=0){
                $exp_added = "No";
            }  else{
                $exp_added = 'Yes';
            }
            $completeness = $getCompletenes['comp'];
            if($completeness<=14 || $exp_added== 'Yes'){
                $data['completeness'] = "Yes";
            }else{
                $data['completeness'] = "No";
            }
             $data['resultstatus'] = $this->checkStatus($userTokenCheck);
             if($notification_list){
                $data['badge'] = count($notification_list);
             }
            $this->load->view("search1", $data);
        }} else {
            session_destroy();
            redirect("dashboard/JobListings");
        }
    }

    public function homeafterlogin() {
        if($this->session->userdata('locationdata')) {
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }

        //var_dump($current_lat);die;
        $filterData = $this->input->post();
        if(empty($filterData['lat'])) {
            $filterData['lat'] = $current_lat;
        }
        if(empty($filterData['long'])) {
            $filterData['long'] = $current_long;
        }

        //print_r($filterData);die;
        /*if(empty($filterData)){
            $filterData['lat']=$current_lat;
            $filterData['long']=$current_long;
        }*/
        //print_r($filterData);die;
        /*$ip = $_SERVER['REMOTE_ADDR'];
       $location = file_get_contents('http://ip-api.com/json/'.$ip);
       $data_loc = json_decode($location);
       
       $Address = $data_loc->city;
       $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
       $output = json_decode($geocodeFromAddr);
          //print_r($output);die;       
       //Get latitude and longitute from json data
       $dataLatitude  = $output->results[0]->geometry->location->lat; 
       $dataLongitude = $output->results[0]->geometry->location->lng;*/
        /*if(empty($filterData['lat'])){
            $filterData['lat'] = $dataLatitude;
            $filterData['long'] = $dataLongitude;
        }*/
        //print_r($filterData);die;
        if (!empty($filterData['joblevel']) && $filterData['joblevel'] == 9) {
            $joblevel = arrar(6, 4, 7, 8);
            $joblevell = implode(",", $joblevel);
            $filterData['joblevel'] = $joblevell;
        }
        if (!empty($filterData['toppicks'])) {
            $jobtopiks = implode(",", $filterData['toppicks']);
            $filterData['toppicks'] = $jobtopiks;
        }
        if (!empty($filterData['medical'])) {
            $jobmedical = implode(",", $filterData['medical']);
            $filterData['medical'] = $jobmedical;
        }
        if (!empty($filterData['allowances'])) {
            $joballowance = implode(",", $filterData['allowances']);
            $filterData['allowances'] = $joballowance;
        }
        if (!empty($filterData['workshift'])) {
            $jobworkshift = implode(",", $filterData['workshift']);
            $filterData['workshift'] = $jobworkshift;
        }
        if (!empty($filterData['leaves'])) {
            $jobleaves = implode(",", $filterData['leaves']);
            $filterData['leaves'] = $jobleaves;
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if(!empty($userTokenCheck[0]['exp_year'])){
            $expyear = $userTokenCheck[0]['exp_year'];
        }else{
            $expyear='';
        }
        if(!empty($userTokenCheck[0]['exp_month'])){
            $expmonth = $userTokenCheck[0]['exp_month'];
        }else{
            $expmonth='';
        }
        /*if(  $filterData['exp_year']==''  && $filterData['exp_month']==''){
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];
        }else{
            $expmonth = $filterData['exp_month'];
            $expyear = $filterData['exp_year'];
            
        }*/

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $data["jobList"] = [];
        if ($userTokenCheck) {
            $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            
            //print_r($resultstatus);die;
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userTokenCheck[0]['id']);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    /*$jobsal = $this->Jobpost_Model->getExpJob1($jobfetch['id'],$expfilter);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = " ";
                    }*/
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['company_id']);
                    //print_r($companydetail1);die;
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if (!empty($jobTop)) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    
                    $distance = $this->distance($current_lat,$current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                      $distance = number_format((float)$distance, 2, '.', '');
                    //    $distance = '';
                    $jobListing[] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'], "salary" => $jobfetch['salary'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "company_id" => $jobfetch['company_id'], "recruiter_id" => $jobfetch['recruiter_id'], "companyName" => $jobfetch['cname'],"cname"=> $cname, "jobPitch"=>$jobfetch['jobPitch'], "latitude" => $jobfetch['latitude'], "longitude" => $jobfetch['longitude'], "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "save_status" => $jobsstatus, "job_image" => $jobImageData,"distance"=>$distance, "mode"=>$jobfetch['mode'], "modeurl"=>$jobfetch['modeurl']];
                }
                $data["jobList"] = $jobListing;
                if (count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else {
                $data["jobList"] = [];
            }
            if (!empty($filterData['salarySort']) && $filterData['salarySort'] == 1) {
                $price = array();
                foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
                }
                array_multisort($price, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            }
            $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

            $jobcountfetchs = $returnnewarray;
            if ($jobcountfetchs) {
                $xx = 1;
                foreach ($jobcountfetchs as $jobcountfetch) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        if(!empty($dataJobList[0])){
                            if($dataJobList[0]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[0]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[0]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[0]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[0]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[0]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[1])){
                            if($dataJobList[1]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[1]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[1]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[1]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[1]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[1]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[2])){
                            if($dataJobList[2]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[2]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[2]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[2]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[2]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[2]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }
                        
                        if(count($jobTop)>1){
                            $url = base_url()."markericon/j_hot_job.png";
                        }

                        if(empty($dataJobList[0]) && empty($dataJobList[1]) && empty($dataJobList[2])){
                            $url = base_url()."markericon/maps.png";
                        }
                        $urll = base_url()."dashboard/jobLists?lat=".$jobcountfetch['latitude']."&long=".$jobcountfetch['longitude'];
                        if($jobcountfetch['jobcount']==1){
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'">' .$jobcountfetch['jobcount'] . ' Job'.'<br>'.'   '.($jobcountfetch["salary"]/1000).'K </a></span>';
                        }else{
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'">' .$jobcountfetch['jobcount'] . ' Jobs'.'<br>'.'   '.($jobcountfetch["salary"]/1000).'K </a></span>';
                        }
                        
                        
                    $dataget[$xx] = ["info" => $info, "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], "jobid" => $jobcountfetch['id'], "url" => $url];
                    $xx++;
                }
                //$dataget1 = json_encode($dataget);
                $data['jobcount'] = $dataget;
            } else {
                $data['jobcount'] = [];
            }
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['languagelist'] = $this->Common_Model->language_lists();
            $data['level_list'] = $this->Common_Model->level_lists();
            $data['category_list'] = $this->Common_Model->category_lists();
            if (!empty($filterData['salarySort'])) {
                $data['salarysort'] = $filterData['salarySort'];
            }
            if (!empty($filterData['locationSort'])) {
                $data['locationsort'] = $filterData['locationSort'];
            }
            if (!empty($filterData['ratingSort'])) {
                $data['ratingsort'] = $filterData['ratingSort'];
            }
             $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
             $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['id']); 
             $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
            if($userTokenCheck[0]['exp_month']>=0   && $userTokenCheck[0]['exp_year']>=0){
                $exp_added = "No";
            }  else{
                $exp_added = 'Yes';
            }
            $completeness = $getCompletenes['comp'];
            if($completeness<=14 || $exp_added== 'Yes'){
                $data['completeness'] = "Yes";
            }else{
                $data['completeness'] = "No";
            }
             $data['resultstatus'] = $this->checkStatus($userTokenCheck);
             if($notification_list){
                $data['badge'] = count($notification_list);
             }
            $this->load->view("search1", $data);

        } else {
            //session_destroy();
            redirect("dashboard/JobListings");
        }
    }

    public function search2() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        $filterData = $this->input->post();

        if(empty($filterData['lat'])) {
            $filterData['lat'] = $current_lat;
        }
        if(empty($filterData['long'])) {
            $filterData['long'] = $current_long;
        }
        
        //print_r($filterData);
        if (empty($filterData['locationn']) &&  $filterData['locationn'] == "" && empty($filterData['cname']) && $filterData['cname'] == "" && $filterData['jobsubcategory'] == "" && empty($filterData['joblevell'])  &&  empty($filterData['jobcategoryy'])) {
            $this->session->set_tempdata('searcherr','At least one field is required to search.');
            redirect('user/search1');
        }else{
        if (!empty($filterData['joblevel']) && $filterData['joblevel'] == 9) {
            $joblevel = arrar(6, 4, 7, 8);
            $joblevell = implode(",", $joblevel);
            $filterData['joblevel'] = $joblevell;
        }
        if (!empty($filterData['toppicks'])) {
            $jobtopiks = implode(",", $filterData['toppicks']);
            $filterData['toppicks'] = $jobtopiks;
        }
        if (!empty($filterData['medical'])) {
            $jobmedical = implode(",", $filterData['medical']);
            $filterData['medical'] = $jobmedical;
        }
        if (!empty($filterData['allowances'])) {
            $joballowance = implode(",", $filterData['allowances']);
            $filterData['allowances'] = $joballowance;
        }
        if (!empty($filterData['workshift'])) {
            $jobworkshift = implode(",", $filterData['workshift']);
            $filterData['workshift'] = $jobworkshift;
        }
        if (!empty($filterData['leaves'])) {
            $jobleaves = implode(",", $filterData['leaves']);
            $filterData['leaves'] = $jobleaves;
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if(!empty($filterData['exp_year'])){
            $expyear = $filterData['exp_year'];
        }else{
            $expyear = $userTokenCheck[0]['exp_year'];
        }
        if(!empty($filterData['exp_month'])){
            $expmonth = $filterData['exp_month'];
        }else{
            $expmonth = $userTokenCheck[0]['exp_month'];
        }
        /*if(  $filterData['exp_year']==''  && $filterData['exp_month']==''){
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];
        }else{
            $expmonth = $filterData['exp_month'];
            $expyear = $filterData['exp_year'];
            
        }*/

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $data["jobList"] = [];
        if ($userTokenCheck) {
            $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userTokenCheck[0]['id']);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    for($itlku=$expfilter; $itlku >-1 ; $itlku--) { 
                        $jobsal = $this->Jobpost_Model->getExpJob1($jobfetch['id'],$itlku);
                        //echo $this->db->last_query();
                        if(!empty($jobsal)){
                            break;
                        }
                    }
                    //$jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['company_id']);
                    //print_r($companydetail1);die;
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if (!empty($jobTop)) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    /*$ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                      
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
                    $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';
                    $jobListing[] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'], "salary" => $jobfetch['salary'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "company_id" => $jobfetch['company_id'], "recruiter_id" => $jobfetch['recruiter_id'], "companyName" => $jobfetch['cname'],"cname"=> $cname, "jobPitch"=>$jobfetch['jobPitch'], "latitude" => $jobfetch['latitude'], "longitude" => $jobfetch['longitude'], "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "save_status" => $jobsstatus, "job_image" => $jobImageData,"distance"=>$distance, "mode"=>$jobfetch['mode'], "modeurl"=>$jobfetch['modeurl']];
                }
                $data["jobList"] = $jobListing;
                if (count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else {
                $data["jobList"] = [];
            }
            if (!empty($filterData['salarySort']) && $filterData['salarySort'] == 1) {
                $price = array();
                foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
                }
                array_multisort($price, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            }
            $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

            $jobcountfetchs = $returnnewarray;
            if ($jobcountfetchs) {
                $xx = 1;
                foreach ($jobcountfetchs as $jobcountfetch) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        if(!empty($dataJobList[0])){
                            if($dataJobList[0]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[0]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[0]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[0]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[0]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[0]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[1])){
                            if($dataJobList[1]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[1]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[1]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[1]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[1]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[1]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[2])){
                            if($dataJobList[2]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[2]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[2]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[2]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[2]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[2]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }
                        
                        if(count($jobTop)>1){
                            $url = base_url()."markericon/j_hot_job.png";
                        }

                        if(empty($dataJobList[0]) && empty($dataJobList[1]) && empty($dataJobList[2])){
                            $url = base_url()."markericon/maps.png";
                        }
                        $urll = base_url()."dashboard/jobLists?lat=".$jobcountfetch['latitude']."&long=".$jobcountfetch['longitude'];
                        if($jobcountfetch['jobcount']==1){
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'">' .$jobcountfetch['jobcount'] . ' Job'.'<br>'.'   '.($jobcountfetch["salary"]/1000).'K </a></span>';
                        }else{
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'">' .$jobcountfetch['jobcount'] . ' Jobs'.'<br>'.'   '.($jobcountfetch["salary"]/1000).'K </a></span>';
                        }

                    $dataget[$xx] = ["info" => $info, "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], "jobid" => $jobcountfetch['id'], "url" => $url];
                    $xx++;
                }
                //$dataget1 = json_encode($dataget);
                $data['jobcount'] = $dataget;
            } else {
                $data['jobcount'] = [];
            }
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['languagelist'] = $this->Common_Model->language_lists();
            $data['level_list'] = $this->Common_Model->level_lists();
            $data['category_list'] = $this->Common_Model->category_lists();
            $data['languagelists'] = $this->Common_Model->language_lists();
            if (!empty($filterData['salarySort']) && $filterData['salarySort']=='1') {
                $data['salarysort'] = $filterData['salarySort'];
            }
            if (!empty($filterData['salarySort']) && $filterData['salarySort']=='2') {
                $data['locationsort'] = $filterData['salarysort'];
            }
            if (!empty($filterData['ratingSort'])) {
                $data['ratingsort'] = $filterData['ratingSort'];
            }
             $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
             $data['filterData'] = $filterData;
            $this->load->view("search2", $data);
        } else {
            session_destroy();
            redirect("dashboard/JobListings");
        }}
    }


    public function search3() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        $filterData = $this->input->post();
        //print_r($filterData);
        if(empty($filterData['lat'])) {
            $filterData['lat'] = $current_lat;
        }
        if(empty($filterData['long'])) {
            $filterData['long'] = $current_long;
        }


        if (!empty($filterData['joblevel']) && $filterData['joblevel'] == 9) {
            $joblevel = arrar(6, 4, 7, 8);
            $joblevell = implode(",", $joblevel);
            $filterData['joblevel'] = $joblevell;
        }
        if (!empty($filterData['toppicks'])) {
            $jobtopiks = implode(",", $filterData['toppicks']);
            $filterData['toppicks'] = $jobtopiks;
        }
        if (!empty($filterData['medical'])) {
            $jobmedical = implode(",", $filterData['medical']);
            $filterData['medical'] = $jobmedical;
        }
        if (!empty($filterData['allowances'])) {
            $joballowance = implode(",", $filterData['allowances']);
            $filterData['allowances'] = $joballowance;
        }
        if (!empty($filterData['workshift'])) {
            $jobworkshift = implode(",", $filterData['workshift']);
            $filterData['workshift'] = $jobworkshift;
        }
        if (!empty($filterData['leaves'])) {
            $jobleaves = implode(",", $filterData['leaves']);
            $filterData['leaves'] = $jobleaves;
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if(!empty($filterData['exp_year'])){
            $expyear = $filterData['exp_year'];
        }else{
            $expyear = $userTokenCheck[0]['exp_year'];
        }
        if(!empty($filterData['exp_month'])){
            $expmonth = $filterData['exp_month'];
        }else{
            $expmonth = $userTokenCheck[0]['exp_month'];
        }
        /*if(  $filterData['exp_year']==''  && $filterData['exp_month']==''){
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];
        }else{
            $expmonth = $filterData['exp_month'];
            $expyear = $filterData['exp_year'];
            
        }*/

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $data["jobList"] = [];
        if ($userTokenCheck) {
            $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userTokenCheck[0]['id']);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    //echo $this->db->last_query();die;
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    for($itlku=$expfilter; $itlku >-1 ; $itlku--) { 
                        $jobsal = $this->Jobpost_Model->getExpJob1($jobfetch['id'],$itlku);
                        //echo $this->db->last_query();
                        if(!empty($jobsal)){
                            break;
                        }
                    }
                    //$jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['company_id']);
                    //print_r($companydetail1);die;
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if (!empty($jobTop)) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    /*$ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                      
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
                    $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';
                    $jobListing[] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'], "salary" => $jobfetch['salary'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "company_id" => $jobfetch['company_id'], "companyName" => $jobfetch['cname'],"cname"=> $cname, "jobPitch"=>$jobfetch['jobPitch'], "latitude" => $jobfetch['latitude'], "longitude" => $jobfetch['longitude'], "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "save_status" => $jobsstatus, "job_image" => $jobImageData,"distance"=>$distance, "mode"=>$jobfetch['mode'], "modeurl"=>$jobfetch['modeurl']];
                }
                $data["jobList"] = $jobListing;
                if (count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else {
                $data["jobList"] = [];
            }
            if (!empty($filterData['salarySort']) && $filterData['salarySort'] == 1) {
                $price = array();
                foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
                }
                array_multisort($price, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            }
            $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

            $jobcountfetchs = $returnnewarray;
            if ($jobcountfetchs) {
                $xx = 1;
                foreach ($jobcountfetchs as $jobcountfetch) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        if(!empty($dataJobList[0])){
                            if($dataJobList[0]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[0]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[0]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[0]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[0]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[0]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[1])){
                            if($dataJobList[1]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[1]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[1]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[1]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[1]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[1]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[2])){
                            if($dataJobList[2]=='1'){
                                $url = base_url()."markericon/bonus.png";
                            }else if($dataJobList[2]=='2'){
                                $url = base_url()."markericon/j_free_food.png";
                            }else if($dataJobList[2]=='3'){
                                $url = base_url()."markericon/j_dependent_hmo.png";
                            }else if($dataJobList[2]=='4'){
                                $url = base_url()."markericon/j_day_1_hmo.png";
                            }else if($dataJobList[2]=='5'){
                                $url = base_url()."markericon/j_dayshift.png";
                            }else if($dataJobList[2]=='6'){
                                $url = base_url()."markericon/j_14th_pay.png";
                            }
                        }
                        
                        if(count($jobTop)>1){
                            $url = base_url()."markericon/j_hot_job.png";
                        }

                        if(empty($dataJobList[0]) && empty($dataJobList[1]) && empty($dataJobList[2])){
                            $url = base_url()."markericon/maps.png";
                        }
                        $urll = base_url()."dashboard/jobLists?lat=".$jobcountfetch['latitude']."&long=".$jobcountfetch['longitude'];
                        
                        if($jobcountfetch['jobcount']==1){
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'">' .$jobcountfetch['jobcount'] . ' Job'.'<br>'.'   '.($jobcountfetch["salary"]/1000).'K </a></span>';
                        }else{
                            $info = '<span style="color:#EA2E49;font-weight:bold"><a href="'.$urll.'">' .$jobcountfetch['jobcount'] . ' Jobs'.'<br>'.'   '.($jobcountfetch["salary"]/1000).'K </a></span>';
                        }
                    $dataget[$xx] = ["info" => $info, "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], "jobid" => $jobcountfetch['id'], "url" => $url];
                    $xx++;
                }
                //$dataget1 = json_encode($dataget);
                $data['jobcount'] = $dataget;
            } else {
                $data['jobcount'] = [];
            }
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['languagelist'] = $this->Common_Model->language_lists();
            $data['level_list'] = $this->Common_Model->level_lists();
            $data['category_list'] = $this->Common_Model->category_lists();
            
            if (!empty($filterData['salarySort'])) {
                $data['salarysort'] = $filterData['salarySort'];
            }
            if (!empty($filterData['locationSort'])) {
                $data['locationsort'] = $filterData['locationSort'];
            }
            if (!empty($filterData['ratingSort'])) {
                $data['ratingsort'] = $filterData['ratingSort'];
            }
             $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
             $data['filterData'] = $filterData;
            $this->load->view("search2", $data);
        } else {
            session_destroy();
            redirect("dashboard/JobListings");
        }
    }

    public function fetchWorkSingleData($id) {
        $workDatas = $this->User_Model->work_single($id);
        if ($workDatas) {
            foreach ($workDatas as $workData) {
                $data[] = ["id" => $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "category" => $workData['category'], "categoryid" => $workData['categoryid'], "subcategory" => $workData['subcategory'], "subcategoryid" => $workData['subcatid'], "level" => $workData['level'], "levelid" => $workData['levelid'], "currently_working" => $workData['currently_working']];
            }
            return $data;
        } else {
            $data = [];
            return $data;
        }
    }
    public function fetchEduSingleData($id) {
        $eduDatas = $this->User_Model->edu_single($id);
        if ($eduDatas) {
            foreach ($eduDatas as $eduData) {
                $data[] = ["id" => $eduData['id'], "attainment" => $eduData['attainment'], "degree" => $eduData['degree'], "university" => $eduData['university'], "from" => $eduData['degreeFrom'], "to" => $eduData['degreeTo']];
            }
            return $data;
        } else {
            $data = [];
            return $data;
        }
    }

    public function checkStatus($uid){
        //print_r($uid);
        $now = time();
        $jobApplyfetch = $this->Jobpost_Model->job_fetch_applied($uid[0]['id']);
        //echo $this->db->last_query();die;
            
            if($jobApplyfetch) {
                    
              foreach ($jobApplyfetch as $jobfetch) {

                  $jobApplyfetchcheck = $this->Jobpost_Model->job_fetch_appliedCheck11($jobfetch['jobpost_id'],$uid[0]['id']);
                  //echo $this->db->last_query();die;
                  //print_r($jobApplyfetchcheck);die;
                  if($jobApplyfetchcheck) {
                      
                     $data1 = [];
                     

                  } else{

                     $companyName = $this->Jobpost_Model->fetch_companyName($jobfetch['recruiter_id']);

                      $jobDate = strtotime($jobfetch['updated_at']);
                      //echo $jobDate;die;
                      $datediff = $now - $jobDate;
                      $daydiff = floor($datediff / (60 * 60 * 24));
                      //$hourdiff = round(($now - $jobDate)/3600, 1);
                      //echo $daydiff;die;
                      if($daydiff >= 2) {
                           $data1[] = [
                                "jobpost_id" => $jobfetch['jobpost_id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "companyName" => $companyName[0]['cname'],
                                "companyId" => $jobfetch['recruiter_id'],
                                "msg" => "open popup",
                                "name" => $uid[0]['name']
                            ];

                            
                            
                      } else {
                          $data1 = [];

                      }
                      return $data1;
                  }
              }
                
            } else{
                $data1 = [];
                return $data1;
            }
    }

    public function fetchLangSingleData($id) {
        $langDatas = $this->User_Model->lang_single($id);
        if ($langDatas) {
            foreach ($langDatas as $langData) {
                $data[] = ["id" => $langData['id'], "lang_name" => $langData['name']];
            }
            return $data;
        } else {
            $data = [];
            return $data;
        }
    }
    public function fetchClientSingleData($id) {
        $clientDatas = $this->User_Model->client_single($id);
        if ($clientDatas) {
            return $clientDatas;
        } else {
            $data = [];
            return $data;
        }
    }

    public function nearJobs($userLat, $userLong){

        $jobfetchs = $this->Jobpost_Model->job_fetch_home($userLat, $userLong);
        //print_r($jobfetchs);die;
          //  echo $this->db->last_query();die;
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    //print_r($jobfetch);
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    //print_r($jobsal);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    //print_r($jobstatus);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    $distance = $this->distance($userLat, $userLong, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    //$distance = '';
                    $distance = number_format((float)$distance, 2, '.', '');
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                           
                        $data1[] = ["jobpost_id" => $jobfetch['id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$jobfetch['distance'], "salary" => $basicsalary, "companyName" => $jobfetch['cname'],"cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$cname,"distance"=>$distance, "savedjob"=>''];
                    
                }
                return $data1;
            } else{
                $data1=[];
                return $data1;
            }

    }

    public function nearJobs1($id,$userLat,$userLong){
        $tokendata = ['id'=>$id];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $jobfetchs = $this->Jobpost_Model->job_fetch_home1($userTokenCheck[0]['id'],$userLat, $userLong,$expfilter);
        //print_r($jobfetchs);die;
          //  echo $this->db->last_query();die;
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    //print_r($jobfetch);
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    //print_r($jobsal);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $id);
                    $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    //$jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    //print_r($jobstatus);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    $distance = $this->distance($userLat,$userLong, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    //$distance = '';
                    $distance = number_format((float)$distance, 2, '.', '');
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                           
                        $data1[] = ["jobpost_id" => $jobfetch['id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => $basicsalary, "companyName" => $jobfetch['cname'],"cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$cname,"distance"=>$distance, "savedjob" => $jobstatus];
                    
                }
                return $data1;
            } else{
                $data1=[];
                return $data1;
            }
        }
    }

     public function distance($lat1, $lon1, $lat2, $lon2, $unit) 
            {

            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) *sin(deg2rad($lat2)) + cos(deg2rad($lat1))* cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist* 60 *1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
            return ($miles * 1.609344);
            } else if ($unit == "N") {
            return ($miles * 0.8684);
            } else {
            return $miles;
            }
            }

    function getToken(){
        $string = "";
        $chars = "0123456789";
        for($i=0;$i<4;$i++)
        $string.=substr($chars,(rand()%(strlen($chars))), 1);
        return $string;
    }
    public function valid_password($password = '')
    {
        $password = trim($password);

        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

        if (empty($password))
        {
            $this->form_validation->set_message('valid_password', 'Password is required.');

            return FALSE;
        }

        if (preg_match_all($regex_lowercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets and numeric characters.');

            return FALSE;
        }

        if (preg_match_all($regex_uppercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets and numeric characters.');

            return FALSE;
        }

        if (preg_match_all($regex_number, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets and numeric characters.');

            return FALSE;
        }

        /*if (preg_match_all($regex_special, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets,numeric and special characters.');

            return FALSE;
        }*/

        /*if (strlen($password) < 5)
        {
            $this->form_validation->set_message('valid_password', 'Password must be at least 5 characters in length.');

            return FALSE;
        }

        if (strlen($password) > 32)
        {
            $this->form_validation->set_message('valid_password', 'Password cannot exceed 32 characters in length.');

            return FALSE;
        }*/

        return TRUE;
    }
}
?>

