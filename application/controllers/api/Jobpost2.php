<?php
ob_start();
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
define('API_ACCESS_KEY','AIzaSyCTzjJxETlJxp18hCwYHLFETLZRkbGFiGw');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */

class Jobpost2 extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('old/User_Model');
        $this->load->model('old/recruiter/Jobpost_Model');
        $this->load->model('old/recruiter/Candidate_Model');
        $this->load->model('old/recruiter/Recruit_Model');
        $this->load->model('old/Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $key = $this->encryption->create_key(10);
    }
    // http://localhost/jobyodha/api/example/users/id/1
    
    public function jobLocateHome_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);

        if($userTokenCheck) {
            
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];
            //$temparray = array("less than 6 months","6mo to 1 yr","1 yr to 2 yr","2yr to 3 yr","3yr and up");
            //echo $expyear;die;
            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }



            $jobLocates = $this->Jobpost_Model->group_locateHome($userTokenCheck[0]['id'],$expfilter);
                
                 $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobLocates as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

                 
            $x=0;
            foreach ($returnnewarray as $jobLocate) {
                              
                $salary_f = (int)$jobLocate['salary']+ (int)$jobLocate['allowance'];
                $returnnewarray[$x]= ["recruiter_id"=> $jobLocate['recruiter_id'], "latitude"=> $jobLocate['latitude'], "longitude"=>$jobLocate['longitude'], "totaljobs"=>(string)$jobLocate['totaljobs'], "salary"=> (int)$jobLocate['salary']];
                
                $jobTop = $this->Jobpost_Model->job_toppicks($jobLocate['id']);
                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $one = (string)$jobTop[0]['picks_id'];
                        $returnnewarray[$x]['toppicks1'] = "$one";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $two = (string)$jobTop[1]['picks_id'];
                        $returnnewarray[$x]['toppicks2'] =  "$two";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $three = (string)$jobTop[2]['picks_id'];
                        $returnnewarray[$x]['toppicks3'] = "$three";
                    }
                } else {
                    $returnnewarray[$x]['toppicks1'] = "";
                    $returnnewarray[$x]['toppicks2'] = "";
                    $returnnewarray[$x]['toppicks3'] = "";
                }
                $x++;
            }


            $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['id']);  
            if($userTokenCheck[0]['exp_month']>=0   && $userTokenCheck[0]['exp_year']>=0){
                $exp_added = "No";
            }  else{
                $exp_added = 'Yes';
            }
            $completeness = $getCompletenes['comp'];
            if($completeness<=14 || $exp_added== 'Yes'){
                $completeness = "Yes";
            }else{
                $completeness = "No";
            }
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
            $response = ['jobLocateHome' => $returnnewarray,'completeness'=>$completeness, 'notification_count' => count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function matchVersion_post(){
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);

            if($userTokenCheck) {
                $versionData1 = $this->User_Model->fetch_version();
                //print_r($versionData);die;
                $data1['versionData']=[
                    'api_version'=>$versionData1[0]['api_version'],
                    'recommend_update'=>$versionData1[0]['recommend_update'],
                    'force_update'=>$versionData1[0]['force_update'],
                    'message'=>urldecode($versionData1[0]['message'])
                ];
                if($versionData1[0]['api_version']<=$userData['api_version']){
                    $response = ['message' => 'Ignore', 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }else{
                    if($versionData1[0]['recommend_update']==0 && $versionData1[0]['force_update']==0){
                        $response = ['message' => 'Ignore', 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }else{
                        $response = ['matchVersion' =>$data1, 'status'=> "SUCCESS" ];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                    
                }
                
            }
        }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }    
    }

    public function fetchUserCompleteData($id) {
        $userData = $this->User_Model->userComplete_single($id);
        
        if($userData) {
          $data['comp'] = $userData[0]['signup'] + $userData[0]['profile'] + $userData[0]['profilePic'] + $userData[0]['resume'] + $userData[0]['experience'] + $userData[0]['education'] + $userData[0]['assessment'] + $userData[0]['language'] + $userData[0]['expert'] + $userData[0]['topClient']+ $userData[0]['dob']+ $userData[0]['location']+ $userData[0]['designation']+ $userData[0]['current_salary']+ $userData[0]['exp'];
        } else{
          $data['comp'] = 0;
        }
        return $data;
    }
    public function jobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {

            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            
            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }


            $userLat = $userData['lat'];
            $userLong = $userData['long'];
            $jobpost_id = $userData['jobpost_id'];

            $jobfetchs = $this->Jobpost_Model->job_fetch_latlong($userLat, $userLong, $jobpost_id,$expfilter); 
            //echo $this->db->last_query();die;
            if($jobfetchs) {
                $x=0;
                foreach ($jobfetchs as $jobfetch) {
                    //print_r($jobfetch);die;
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if($jobTop) {

                    } else{
                        $jobTop = [];
                    }
                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob1($jobfetch['id'],$expfilter);
                       
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = $comPic;
                        }
                        
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $jobfetch['salary'],
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "cname" => $cname,
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob,
                                "distance" => $distance 
                            ];
                    }
                    $x++;
                }
                
                $response = ['jobListing' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else{
                $errors = "No data found";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companydetail_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $company_id = $userData['companyId'];

            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);

            if($compfetchs) {
                $jobTop = $this->Jobpost_Model->job_fetch_TopPicks($compfetchs[0]['compId']);
                $compDesc = $this->Jobpost_Model->fetch_companyaddress($compfetchs[0]['parent_id']);
                //echo $this->db->last_query();die;

                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id'];    
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $datatoppicks2 =  $jobTop[1]['picks_id'];    
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $datatoppicks3 = $jobTop[2]['picks_id'];    
                    } else{
                        $datatoppicks3 = "";
                    }
                    $topickss = $jobTop;
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                    $topickss = [];
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic1($compfetchs[0]['compId']);
//var_dump($companyPic);die;
                $companyReviews = $this->Jobpost_Model->fetch_companyRating($compfetchs[0]['compId']);
                //echo $this->db->last_query();
                //print_r($companyReviews);die;
                if(!empty($companyReviews[0]['average'])){
                    $rating = $companyReviews[0]['average'];
                }else{
                    $rating="";
                }
                        
                if(!empty($companyPic)) {
                    $comPic = $companyPic;
                } else{
                    $comPic = [["pic"=>'']];
                }

                $glassdoor = $this->Jobpost_Model->job_fetch_glass($compfetchs[0]['compId']);
                
                if($glassdoor) {
                    $glassrating = $glassdoor[0]['rating'];
                    $glassdate = $glassdoor[0]['created_at'];
                } else{
                  $glassrating = "0";
                  $glassdate = "0";
                }

                $reviewLast = $this->Jobpost_Model->job_fetch_lastrate($compfetchs[0]['compId']);
                if($reviewLast) { 
                  $reviewLast1 = $reviewLast[0]['feedback'];
                } else{
                  $reviewLast1 = "";
                }
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "address" => $compfetchs[0]['address'],
                                "companyName" => $compfetchs[0]['cname'],
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compDesc[0]['companyDesc'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "topPicks" =>$topickss,
                                "rating" => $rating,
                                "review" => $reviewLast1,
                                "glassdoorRating" =>$glassrating,
                                "glassdoorDate" =>$glassdate
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_bycompId($company_id,$expfilter);
            //echo $this->db->last_query();die;
            if($jobfetchs) {
                $data["jobList"] = [];
                $data["jobLists"] = [];
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        /*$jobsal = $this->Jobpost_Model->getExpJob1($jobfetch['id'],$expfilter);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = " ";
                        }*/

                        $jobTop1 = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if($jobTop1) {
                            if(isset($jobTop1[0]['picks_id'])){
                                $datatoppicks1 = $jobTop1[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop1[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop1[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop1[2]['picks_id'])){
                                $datatoppicks3 = $jobTop1[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                         $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                         if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }
                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $jobfetch['salary'],
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "cname" => $cname,
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob,
                                "distance" => $distance
                            ];
                    }
                }

                if(count($data["jobLists"]) < 1) {
                    $data["jobLists"] = $data["jobList"];
                    if(isset($data["jobList"][0])) {
                        $data["jobList"] = $data["jobList"][0];
                    } else{
                        $data["jobList"] = $data["jobList"];
                    }
                } else{
                    $data["jobList"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];
                    $data["jobLists"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];    
                }

            } else{
                $data["jobList"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];
            }
            
            $response = ['companydetail' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companydetailad_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $company_id = $userData['companyId'];
            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);

            if($compfetchs) {
                $jobTop = $this->Jobpost_Model->job_fetch_TopPicks($compfetchs[0]['compId']);


                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id'];    
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $datatoppicks2 =  $jobTop[1]['picks_id'];    
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $datatoppicks3 = $jobTop[2]['picks_id'];    
                    } else{
                        $datatoppicks3 = "";
                    }
                    $topickss = $jobTop;
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                    $topickss = [];
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic1($compfetchs[0]['compId']);
//var_dump($companyPic);die;
                $companyReviews = $this->Jobpost_Model->fetch_companyRating($compfetchs[0]['compId']);
                //echo $this->db->last_query();
                //print_r($companyReviews);die;
                if(!empty($companyReviews[0]['average'])){
                    $rating = $companyReviews[0]['average'];
                }else{
                    $rating="";
                }
                        
                if(!empty($companyPic)) {
                    $comPic = $companyPic;
                } else{
                    $comPic = [["pic"=>'']];
                }

                $glassdoor = $this->Jobpost_Model->job_fetch_glass($compfetchs[0]['compId']);
                
                if($glassdoor) {
                    $glassrating = $glassdoor[0]['rating'];
                    $glassdate = $glassdoor[0]['created_at'];
                } else{
                  $glassrating = "0";
                  $glassdate = "0";
                }

                $reviewLast = $this->Jobpost_Model->job_fetch_lastrate($compfetchs[0]['compId']);
                if($reviewLast) { 
                  $reviewLast1 = $reviewLast[0]['feedback'];
                } else{
                  $reviewLast1 = "";
                }
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "address" => $compfetchs[0]['address'],
                                "companyName" => $compfetchs[0]['cname'],
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compfetchs[0]['companyDesc'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "topPicks" =>$topickss,
                                "rating" => $rating,
                                "review" => $reviewLast1,
                                "glassdoorRating" =>$glassrating,
                                "glassdoorDate" =>$glassdate
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_byrId($company_id);
            
            if($jobfetchs) {
                $data["jobList"] = [];
                $data["jobLists"] = [];
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $jobTop1 = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if($jobTop1) {
                            if(isset($jobTop1[0]['picks_id'])){
                                $datatoppicks1 = $jobTop1[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop1[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop1[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop1[2]['picks_id'])){
                                $datatoppicks3 = $jobTop1[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                         $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                         if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }
                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $basicsalary,
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $cname,
                                "cname" => $jobfetch['cname'],
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob,
                                "distance" => $distance
                            ];
                    }
                }

                if(count($data["jobLists"]) < 1) {
                    $data["jobLists"] = $data["jobList"];
                    if(isset($data["jobList"][0])) {
                        $data["jobList"] = $data["jobList"][0];
                    } else{
                        $data["jobList"] = $data["jobList"];
                    }
                } else{
                    $data["jobList"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];
                    $data["jobLists"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];    
                }

            } else{
                $data["jobList"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];
            }
            
            $response = ['companydetail' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function availableJobs_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $company_id = $userData['companyId'];
            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);
            $companyname = $this->Jobpost_Model->company_name_fetch($compfetchs[0]['parent_id']);
             if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }
            if($compfetchs) {
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "companyName" => $compfetchs[0]['cname'],
                                "cname" => $cname,
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compfetchs[0]['companyDesc']
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_bycompId($company_id,$expfilter);
            
            if($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        /*$jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = " ";
                        }*/

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                         if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }
                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $jobfetch['salary'],
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "cname" => $cname,
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob,
                                "distance" => $distance
                            ];
                    }
                }
            } else{
                $data["jobList"] =[];
            }
            $response = ['availableJobs' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function availableJobsad_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $company_id = $userData['companyId'];
            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);
            $companyname = $this->Jobpost_Model->company_name_fetch($compfetchs[0]['parent_id']);
             if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }
            if($compfetchs) {
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "companyName" => $compfetchs[0]['cname'],
                                "cname" => $cname,
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compfetchs[0]['companyDesc']
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_byrId($company_id);
            
            if($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                         if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }
                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $basicsalary,
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $cname,
                                "cname" => $jobfetch['cname'],
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob,
                                "distance" => $distance
                            ];
                    }
                }
            } else{
                $data["jobList"] =[];
            }
            $response = ['availableJobs' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobdetail_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];
            
           if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $jobpost_id = $userData['jobpost_id'];
            $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobpost_id);
            //print_r($jobDetails);die;
            $jobLocation = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
            $jobSaved = $this->Jobpost_Model->applied_status_fetch($jobpost_id, $userTokenCheck[0]['id']);

            if($jobSaved) {
                $savestatus = 1;
            } else{
                $savestatus = 0;
            }

            $appliedStatus = $this->Jobpost_Model->applied_status_fetch($jobDetails[0]['id'], $userTokenCheck[0]['id']);
            if($appliedStatus) {
                $jobstatus = $appliedStatus[0]['status'];
                $jobapplieddate = date("Y-m-d",strtotime($appliedStatus[0]['interviewdate']));
                $jobappliedtiming = date("H:i",strtotime($appliedStatus[0]['interviewtime']));
                $jobappliedtime = $jobapplieddate.",".$jobappliedtiming;
                $date_day1 = date("Y-m-d",strtotime($appliedStatus[0]['date_day1']));
                //$jobappliedtime = $appliedStatus[0]['created_at'];
            } else{
                $jobstatus = '0';
                $jobappliedtime = '0';
            }

            for($itlku=$expfilter; $itlku >-1 ; $itlku--) { 
                $jobsal = $this->Jobpost_Model->getExpJob1($jobDetails[0]['id'],$itlku);
                //echo $this->db->last_query();
                if(!empty($jobsal)){
                    break;
                }
            }
            
            
            
            if($jobsal[0]['basicsalary']) {
                $basicsalary = $jobsal[0]['basicsalary'];
            } else{
                $basicsalary = '0';
            }

            if($jobsal[0]['grade_id']) {
                $grade = $jobsal[0]['grade_id'];
                if($grade==0){
                    $basicexp="All Tenure";
                }if($grade==1){
                    $basicexp="Minimum Experience";
                }if($grade==2){
                    $basicexp="less than 6 months";
                }if($grade==3){
                    $basicexp="6mo to 1 yr";
                }if($grade==4){
                    $basicexp="1 yr to 2 yr";
                }if($grade==5){
                    $basicexp="2 yr to 3 yr";
                }if($grade==6){
                    $basicexp="3 yr to 4 yr";
                }if($grade==7){
                    $basicexp="4 yr to 5 yr";
                }if($grade==8){
                    $basicexp="5 yr to 6 yr";
                }if($grade==9){
                    $basicexp="6 yr to 7 yr";
                }if($grade==10){
                    $basicexp="7 yr & up";
                }
            } else{
                $basicexp = " ";
            }

            $jobTop = $this->Jobpost_Model->job_toppicks($jobDetails[0]['id']);
            $jobAllowance = $this->Jobpost_Model->job_allowances($jobDetails[0]['id']);
            $jobMedical = $this->Jobpost_Model->job_medical($jobDetails[0]['id']);
            $jobShift = $this->Jobpost_Model->job_workshift($jobDetails[0]['id']);
            $jobLeaves = $this->Jobpost_Model->job_leaves($jobDetails[0]['id']);
            $jobImg = $this->Jobpost_Model->job_images_single($jobpost_id);
            $savedjob = $this->Jobpost_Model->job_saved($jobpost_id, $userTokenCheck[0]['id']);
            if(!empty($savedjob[0]['id'])) {
                $savedjob = '1';
            } else{
                $savedjob = "0";
            }

            if(count($jobImg)>0){
                $jobImg = $jobImg;
            }else{
                $jobImg = [['pic'=>'']];
            }
           // echo $this->db->last_query();
            if($jobTop) {
                // if(isset($jobTop[0]['picks_id'])){
                //     $datatoppicks1 = $jobTop[0]['picks_id'];    
                // } else{
                //     $datatoppicks1 = "";
                // }
                // if(isset($jobTop[1]['picks_id'])){
                //     $datatoppicks2 =  $jobTop[1]['picks_id'];    
                // } else{
                //     $datatoppicks2 = "";
                // }
                // if(isset($jobTop[2]['picks_id'])){
                //     $datatoppicks3 = $jobTop[2]['picks_id'];    
                // } else{
                //     $datatoppicks3 = "";
                // }
                $toppickss = $jobTop;
            } else {
                $toppickss = [];
            }
            if($jobAllowance) {
                $allowancess = $jobAllowance;
            } else {
                $allowancess = [];
            }
            if($jobMedical) {
                $medicals = $jobMedical;
            } else {
                $medicals = [];
            }if($jobShift) {
                $shifts = $jobShift;
            } else {
                $shifts = [];
            }if($jobLeaves) {
                $leavess = $jobLeaves;
            } else {
                $leavess = [];
            }
            if(!empty($jobDetails[0]['jobPitch'])){
                $jobPitch = $jobDetails[0]['jobPitch'];
            }else{
                $jobPitch = '';
            }
            $jobskills = $this->Jobpost_Model->job_skills($jobDetails[0]['id']);
            if(empty($jobskills)){
                $jobskills[]= ['skill'=>$jobDetails[0]['skills']];
            }
            //print_r($jobskills);die;
            /*if(empty($jobskills)){
                $jobskills = explode(',', $jobDetails[0]['skills']);
                $jobskills=['skill'=>$jobskills];
            }*/
            
            if(!empty($jobDetails[0]['allowance'])){
                $jobDetails[0]['allowance'] = $jobDetails[0]['allowance'];
            }else{
                $jobDetails[0]['allowance'] = '';
            }
            if(!empty($jobDetails[0]['joining_bonus'])){
                $jobDetails[0]['joining_bonus'] = $jobDetails[0]['joining_bonus'];
            }else{
                $jobDetails[0]['joining_bonus'] = '';
            }
            //echo($jobDetails[0]['allowance']);die;
            if($jobDetails[0]['allowance']>0 && $basicsalary>0){
                $total_salary = $basicsalary+$jobDetails[0]['allowance'];
            }else{
                $total_salary = $basicsalary;
            }

            if($jobDetails[0]['subrecruiter_id']){
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['subrecruiter_id']);
            }else{
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['recruiter_id']);
            }

             if(!empty($date_day1) && $jobstatus=='6'){
                        $date_day1 = $date_day1;
                    }else{
                        $date_day1 = '';
                    }
            // echo $this->db->last_query();die;
             //print_r($companydetail1);die;
             $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobLocation[0]['latitude'], $jobLocation[0]['longitude'], "K");
             $distance = number_format((float)$distance, 2, '.', '');
            $data["jobdetail"] = [
                        "jobpost_id" => $jobDetails[0]['id'],
                        "job_title" => $jobDetails[0]['jobtitle'],
                        "location" => $jobLocation[0]['address'],
                        "opening" => $jobDetails[0]['opening'],
                        "experience" => $basicexp,
                        "jobDesc" => $jobDetails[0]['jobDesc'],
                        "allowance" => $jobDetails[0]['allowance'],
                        "joining_bonus" => $jobDetails[0]['joining_bonus'],
                        "salary" => $basicsalary,
                        "total_salary" => (string)$total_salary,
                        "jobexpire" => $jobDetails[0]['jobexpire'],
                        "skills" => $jobskills,
                        "qualification" => $jobDetails[0]['education'],
                        "jobSaved" => $savestatus,
                        "status" => $jobstatus,
                        "topPicks" => $toppickss,
                        "allowances" => $allowancess,
                        "medical" => $medicals,
                        "workshift" => array_merge($shifts,$leavess),
                        //"leaves" => $leavess,
                        "images" => $jobImg,
                        "savedjob" => $savedjob,
                        "jobPitch" => $jobPitch,
                        'jobappliedtime' => $jobappliedtime,
                        'distance' => $distance,
                        'date_day1' => $date_day1
                    ];

            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobDetails[0]['company_id']);
            //print($companydetail);die;
            //$companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['company_id']);
            $companyname = $this->Jobpost_Model->company_name_fetch($jobDetails[0]['recruiter_id']);


            if($jobTop) {

            } else{
                $jobTop = [];
            }

              

            $companyPic = $this->Jobpost_Model->fetch_companyPic($companydetail[0]["id"]);            
            if(!empty($companyPic[0]['companyPic'])) {
                $comPic = trim($companyPic[0]['companyPic']);
            } else{
                $comPic = "";
            }
            if($companydetail1[0]["comm_channel"]){
                $comm_channel = explode(',', $companydetail1[0]["comm_channel"]);
            }else{
                $comm_channel = [];
            }
            if(!empty($companyname[0]["cname"])){
                $cname = $companyname[0]["cname"];
            }else{
                $cname = '';
            }

            if(!empty($companydetail1[0]["landline"])){
                $landline = $companydetail1[0]["landline"];
            }else{
                $landline = '';
            }
            $data["comapnyDetail"] = [
                                    "name"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "phonecode" => $companydetail1[0]["phonecode"],
                                    "phone" => $companydetail1[0]["phone"],
                                    "landline" => $landline,
                                    "comm_channel" => $comm_channel,
                                    "email" => $companydetail1[0]["email"],
                                    "company_id" => $companydetail[0]["id"],
                                    "companyPic" => $comPic
                                    ];

            $response = ['jobdetail' => $data, 'status' => "SUCCESS"];

            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function applyJob_post() {
        $job = $this->input->post();
        $date = date('Y-m-d H:i:s');
        $data = ["token" => $job['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $job_detail = $this->Jobpost_Model->job_detail_fetch($job['jobpost_id']);
                //print_r($job_detail);die;
                $companyname = $this->Candidate_Model->fetchcomp($job_detail[0]['recruiter_id']);

            $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');
            $this->form_validation->set_rules('interviewdate', 'Interview Date', 'trim|required');
            $this->form_validation->set_rules('interviewtime', 'Interview Time', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {

                $jobexpire = date("Y-m-d", strtotime($job['jobexpire']));
                $schedule = date("Y-m-d", strtotime($job['interviewdate']));
                
                $jobexpire1 = strtotime($jobexpire);
                $schedule1 = strtotime($schedule);
                $currentDate = strtotime(date('Y-m-d'));
               
                if($job_detail[0]['education_status']=='1'){
                    $user_education_match = $this->User_Model->user_single_educationbyuserid1($userTokenCheck[0]['id'],$job_detail[0]['education']);
                    if($user_education_match){
                        $education_match = "True";
                    } else{
                        $education_match = "False";
                    }

                }else{
                    $education_match = "True";
                }

                if($job_detail[0]['experience_status']=='1'){
                    
                    $user_experience_match = $this->User_Model->user_single_exp($userTokenCheck[0]['id'],$job_detail[0]['experience']);
                    //echo $this->db->last_query();die;
                    if($user_experience_match){
                        $experience_match = "True";
                    } else{
                        $experience_match = "False";
                    }

                }else{
                    $experience_match = "True";
                }
                if($jobexpire1 >= $schedule1) {
                    
                    if($schedule1 >= $currentDate) {
                      $jobapply = [
                          "jobpost_id" => $job['jobpost_id'],
                          "user_id" => $userTokenCheck[0]['id'],
                          "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                          "interviewtime" => $job['interviewtime'],
                          "status" => 1,
                          "created_at" => $date,
                          
                      ];

                      $resjobapply = [
                          "jobpost_id" => $job['jobpost_id'],
                          "user_id" => $userTokenCheck[0]['id'],
                          "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                          "interviewtime" => $job['interviewtime'],
                          "status" => 1,
                          "created_at" => $date,
                          "message" => 'Congratulations. Your interview date and time are now confirmed. You can check the address and Recruiter details in the Applied tab on our app if needed. 
Your Hiring Manager would wait for you at the selected date and time.Please be there on time.And dont forget to look your best and put JobYoDA as your source of application when asked by Recruiter. Wish you the best and we hope you get the job!'
                      ];
                      
                        
                        if($education_match == "True" && $experience_match== "True"){
                            /*if($level_match == "True" && $education_match == "True" && $experience_match== "True"){*/
                            $appliedJobs = $this->Jobpost_Model->job_fetch_appliedbycompid($job['jobpost_id'],$userTokenCheck[0]['id']);
                            if(!empty($appliedJobs))
                            {
                                $errors = "You have already applied for this job";
                                $response = ['message' => $errors, 'status' => "FAILURE"];
                                $this->set_response($response, REST_Controller::HTTP_CREATED);
                            } else{
                                $this->Jobpost_Model->appliedjob_insert($jobapply);
                            $data2 = ['message'=>'You have got a new application from candidate'];
                            $this->Recruit_Model->Notificationmessageinsert($data2);
                            $response = ['jobdetail' => $resjobapply, 'status' => "SUCCESS"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                            if($schedule1 == $currentDate){
                                $this->load->library('email');
                                $data['interviewtime'] = $job['interviewtime'];
                                $data['cname'] = $companyname[0]['cname'];
                                $data['rname'] = $companyname[0]['fname'].' '.$companyname[0]['lname'];
                                $data['jobtitle'] = $job_detail[0]['jobtitle'];
                                $data['name'] =$userTokenCheck[0]['name'];
                                $data['phone'] =$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone'];
                                $data['email'] =$userTokenCheck[0]['email'];
                                $data['uid'] =$userTokenCheck[0]['id'];
                                $msg = $this->load->view('recruiter/recruiteremail2',$data,TRUE);
                                $config=array(
                                'charset'=>'utf-8',
                                'wordwrap'=> TRUE,
                                'mailtype' => 'html'
                                );

                                $this->email->initialize(
                                            [
                                              'protocol' => 'smtp',
                                              'smtp_host' => 'smtpout.asia.secureserver.net',
                                              'smtp_user' => 'help@jobyoda.com',
                                              'smtp_pass' => 'Usa@1234567',
                                              'smtp_port' => 465,
                                              'smtp_crypto' => 'ssl',
                                              'charset'=>'utf-8',
                                              'mailtype' => 'html',
                                              'crlf' => "\r\n",
                                              'newline' => "\r\n"
                                            ]
                                    );
                                $this->email->from('help@jobyoda.com', "JobYoDA");
                                $this->email->to($companyname[0]['email']);
                                //$this->email->to('sunaina.singhal@mobulous.com');
                                $this->email->subject('JobYoDA New Interview Alert - '.$companyname[0]['cname'].' - Date '.date('Y-m-d'));

                                $this->email->message($msg);
                                $this->email->send(); 
                                $mobileNumber = '+'.$companyname[0]['phonecode'].$companyname[0]['phone'];
                                  $msgege = "New Interview Alert: ".$userTokenCheck[0]['name']." just scheduled for an interview today at ".$job['interviewtime']." for ".$job_detail[0]['jobtitle']." in ".$companyname[0]['cname'].". Get in touch via ".$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone']." or ".$userTokenCheck[0]['email']."";
                                $this->SendAwsSms($mobileNumber,utf8_encode($msgege));  
                                   //$this->sendSms($mobileNumber, $msgege);
                                  
                                  /*if(($this->sendSms($mobileNumber, $msgege))=='1')
                                      {
                                        
                                      }else{
                                        $this->SendAwsSms($mobileNumber, $msgege);
                                      }*/ 
                                if(!empty($job_detail[0]['subrecruiter_id'])){
                                    $subrecruiter_data = $this->Candidate_Model->fetchcomp($job_detail[0]['subrecruiter_id']);
                                    $subrecruiter_comp = $this->Candidate_Model->fetchcomp($subrecruiter_data[0]['parent_id']);
                                    //echo $this->db->last_query();die;
                                    $this->load->library('email');
                                    $data1['interviewtime'] = $job['interviewtime'];
                                    $data1['cname'] = $subrecruiter_comp[0]['cname'];
                                    $data1['rname'] = $subrecruiter_data[0]['fname'].' '.$subrecruiter_data[0]['lname'];
                                    $data1['jobtitle'] = $job_detail[0]['jobtitle'];
                                    $data1['name'] =$userTokenCheck[0]['name'];
                                    $data1['phone'] =$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone'];
                                    $data1['email'] =$userTokenCheck[0]['email'];
                                    $data1['uid'] =$userTokenCheck[0]['id'];
                                    $msg = $this->load->view('recruiter/recruiteremail2',$data1,TRUE);
                                    $config=array(
                                    'charset'=>'utf-8',
                                    'wordwrap'=> TRUE,
                                    'mailtype' => 'html'
                                    );

                                    $this->email->initialize(
                                                    [
                                                      'protocol' => 'smtp',
                                                      'smtp_host' => 'smtpout.asia.secureserver.net',
                                                      'smtp_user' => 'help@jobyoda.com',
                                                      'smtp_pass' => 'Usa@1234567',
                                                      'smtp_port' => 465,
                                                      'smtp_crypto' => 'ssl',
                                                      'charset'=>'utf-8',
                                                      'mailtype' => 'html',
                                                      'crlf' => "\r\n",
                                                      'newline' => "\r\n"
                                                    ]
                                            );
                                    $this->email->from('help@jobyoda.com', "JobYoDA");
                                    $this->email->to($subrecruiter_data[0]['email']);
                                    //$this->email->to('sunaina.singhal@mobulous.com');
                                    $this->email->subject('JobYoDA New Interview Alert - '.$subrecruiter_data[0]['cname'].' - Date '.date('Y-m-d'));

                                    $this->email->message($msg);
                                    $this->email->send(); 
                                    $mobileNumber1 = '+'.$subrecruiter_data[0]['phonecode'].$subrecruiter_data[0]['phone'];
                                      $msgege1 = "New Interview Alert: ".$userTokenCheck[0]['name']." just scheduled for an interview today at ".$job['interviewtime']." for ".$job_detail[0]['jobtitle']." in ".$subrecruiter_comp[0]['cname'].". Get in touch via ".$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone']." or ".$userTokenCheck[0]['email']."";

                                       //$this->sendSms($mobileNumber1, $msgege1);
                                      $this->SendAwsSms($mobileNumber1, utf8_encode($msgege1));
                                      /* if(($this->sendSms($mobileNumber1, $msgege1))=='1')
                                      {
                                        
                                      }else{
                                        $this->SendAwsSms($mobileNumber1, $msgege1);
                                      }*/ 
                                }   
                            }
                            }
                            
                          }else{
                            $appliedJobs = $this->Jobpost_Model->job_fetch_appliedbycompid($job['jobpost_id'],$userTokenCheck[0]['id']);
                            $jobapply1 = [
                              "jobpost_id" => $job['jobpost_id'],
                              "user_id" => $userTokenCheck[0]['id'],
                              "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                              "interviewtime" => $job['interviewtime'],
                              "status" => 3,
                              "fallout_status" => 2,
                              "fallout_reason" => "Did not meet requirement",
                              "created_at" => $date
                          ];
                            if(!empty($appliedJobs))
                            {
                                $errors = "You have already applied for this job";
                                $response = ['message' => $errors, 'status' => "FAILURE"];
                                $this->set_response($response, REST_Controller::HTTP_CREATED);
                            } else{
                                $this->Jobpost_Model->appliedjob_insert($jobapply1);
                             $user_data = $this->User_Model->user_single($userTokenCheck[0]['id']);
                             $msg="Your application for "." ". $job_detail[0]['jobtitle'] ." "." at "." ". $companyname[0]['cname'] ." was rejected for not meeting the hiring requirement. Make sure your profile is up-to-date for better chances!";
                             $data1 = ["user_id"=>$userTokenCheck[0]['id'],"jobapp_id"=>$job['jobpost_id'], "recruiter_id" =>$job_detail[0]['company_id'], "notification"=>$msg, "status_changed"=>3, "job_status"=>'0', "badge_count"=>1, "createdon"=>$date];

                             $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                             if($notificationInserted){
                                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                             }
                             $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
                             
                             $msg1="Your application for "." ". $job_detail[0]['jobtitle'] ." "." at "." ". $companyname[0]['cname'] ." was rejected for not meeting the hiring requirement. Make sure your profile is up-to-date for better chances!";
                                if($user_data[0]['notification_status']=='1'){
                                   if($user_data[0]['device_type']=="android"){
                                        $res1 = $this->push_notification_android_status($user_data[0]['device_token'],$job_detail[0]['jobtitle'], $msg1,'applied',$job_detail[0]['recruiter_id'],$companyname[0]['cname'],$notificationInserted,count($fetchnotifications)+count($fetchpromonotifications));
                                    }else if($user_data[0]['device_type']=="ios"){
                                        $this->pushiphon(array($user_data[0]['device_token'],$msg,'applied',$job_detail[0]['jobtitle'], $job_detail[0]['id'],count($fetchnotifications)+count($fetchpromonotifications),$companyname[0]['cname'],$notificationInserted));
                                    
                                    } 
                                }
                            $response = ['jobdetail' => $jobapply1, 'status' => "SUCCESS"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                            }
                          }
                    
                    } else{
                        $errors = "Please select correct date";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                } else{
                    $errors = "Interview date expire on $jobexpire.";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }/*}else{
                $errors = "You are applying for a job for which your profile doesn’t match the requirements. Please update your profile.";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
            }*/


        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobAppliedListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $jobAppliedpasts = $this->Jobpost_Model->jobappliedpast_fetch_byuserid($userTokenCheck[0]['id']);
            if($jobAppliedpasts) {

                foreach ($jobAppliedpasts as $jobAppliedpast) {
                    $jobsal = $this->Jobpost_Model->getExpJob($jobAppliedpast['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = '0';
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedpast['company_id']);
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedpast['id']);

                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id'];    
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])){
                            $datatoppicks2 =  $jobTop[1]['picks_id'];    
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $datatoppicks3 = $jobTop[2]['picks_id'];    
                        } else{
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobAppliedpast['company_id']);            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    
                    $image = $this->Jobpost_Model->job_image($jobAppliedpast['id']);
                        
                        
                    if(!empty($image[0]['pic'])) {
                        $jobpic = $image[0]['pic'];
                    } else{
                        $jobpic = $comPic;
                    }
                    $savedjob = $this->Jobpost_Model->job_saved($jobAppliedpast['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($jobAppliedpast["recruiter_id"]);
                        if(!empty($companyname[0]["cname"])){
                        	$cname = $companyname[0]["cname"];
                        }else{
                        	$cname = '';
                        }
                    $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $companyPic[0]['latitude'], $companyPic[0]['longitude'], "K");
                    if(!empty($jobAppliedpast['date_day1']) && $jobAppliedpast['status']=='6'){
                        $jobAppliedpast['date_day1'] = $jobAppliedpast['date_day1'];
                    }else{
                        $jobAppliedpast['date_day1'] = '';
                    }
                    $distance = number_format((float)$distance, 2, '.', '');
                    $data["jobPast"][] = [
                            "jobpost_id" => $jobAppliedpast['id'],
                            "job_title" => $jobAppliedpast['jobtitle'],
                            "jobDesc" => $jobAppliedpast['jobDesc'],
                            "salary" => $basicsalary,
                            "status" => $jobAppliedpast['status'],
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyId"=> $companydetail[0]["id"],
                            "toppicks1" => "$datatoppicks1",
                            "toppicks2" => "$datatoppicks2",
                            "toppicks3" => "$datatoppicks3",
                            "companyPic" => $comPic,
                            "jobpic" => $jobpic,
                            "savedjob" => $savedjob,
                            "distance" => $distance,
                            "date_day1" => $jobAppliedpast['date_day1']
                        ];
                }
            } else {
                $data["jobPast"][] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "status" => "",
                            "companyName"=> "",
                            "cname"=> "",
                            "companyPic" => "",
                            "distance" => "",
                            "date_day1" => ''
                                ];
            }

            $jobAppliedupcomings = $this->Jobpost_Model->jobappliedupcoming_fetch_byuserid($userTokenCheck[0]['id']);
            //echo $this->db->last_query();die;
            //print_r($jobAppliedupcomings);die;
            if($jobAppliedupcomings) {

                foreach ($jobAppliedupcomings as $jobAppliedupcoming) {
                    $jobsal1 = $this->Jobpost_Model->getExpJob($jobAppliedupcoming['id']);
                    if($jobsal1[0]['basicsalary']) {
                        $basicsalary1 = $jobsal1[0]['basicsalary'];
                    } else{
                        $basicsalary1 = '0';
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedupcoming['company_id']);
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedupcoming['id']);

                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id'];    
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])){
                            $datatoppicks2 =  $jobTop[1]['picks_id'];    
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $datatoppicks3 = $jobTop[2]['picks_id'];    
                        } else{
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobAppliedupcoming['company_id']);            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    
                    $image = $this->Jobpost_Model->job_image($jobAppliedupcoming['id']);
                        
                        
                    if(!empty($image[0]['pic'])) {
                        $jobpic = $image[0]['pic'];
                    } else{
                        $jobpic = " ";
                    }
                    $savedjob = $this->Jobpost_Model->job_saved($jobAppliedupcoming['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }
                        
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobAppliedupcoming["recruiter_id"]);
                    if(!empty($companyname[0]["cname"])){
                    	$cname = $companyname[0]["cname"];
                    }else{
                    	$cname = '';
                    }
                     $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $companyPic[0]['latitude'], $companyPic[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    $data["jobUpcoming"][] = [
                            "jobpost_id" => $jobAppliedupcoming['id'],
                            "job_title" => $jobAppliedupcoming['jobtitle'],
                            "jobDesc" => $jobAppliedupcoming['jobDesc'],
                            "interviewdate" => $jobAppliedupcoming['interviewdate'],
                            "interviewtime" => date("H:i", strtotime($jobAppliedupcoming['interviewtime'])) ,
                            "salary" => $basicsalary1,
                            "status" => $jobAppliedupcoming['status'],
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyId"=> $companydetail[0]["id"],
                            "toppicks1" => "$datatoppicks1",
                            "toppicks2" => "$datatoppicks2",
                            "toppicks3" => "$datatoppicks3",
                            "companyPic" => $comPic,
                            "jobpic" => $jobpic,
                            "savedjob" => $savedjob,
                            "distance" => $distance
                        ];
                }
            } else {
                $data["jobUpcoming"][] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "status" => "",
                            "companyName"=> "",
                            "cname"=> "",
                            "companyPic" => "",
                            "interviewdate" => "",
                            "interviewtime" => "",
                            "distance" => ""
                ];
            }
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
            $response = ['jobAppliedListing' => $data,'notification_count'=>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function saveJob_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $data1 = ['user_id' => $userTokenCheck[0]['id'], 'jobpost_id'=> $job['jobpost_id']];
                $jobSaveds = $this->Jobpost_Model->getstatusSavedJob($data1);
                if(empty($jobSaveds)){
                    $jobsave = [
                            "jobpost_id" => $job['jobpost_id'],
                            "user_id" => $userTokenCheck[0]['id'],
                        ];
                    $this->Jobpost_Model->savejob_insert($jobsave);
                    $response = ['saveJob' => $jobsave, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }else{
                    $response = ['message' => 'already saved', 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
                
            }
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function saveJobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $jobSaveds = $this->Jobpost_Model->jobSaved_fetch_byuserid($userTokenCheck[0]['id']);
            //print_r($jobSaveds);die;
            if($jobSaveds) {

                foreach ($jobSaveds as $jobSaved) {
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobSaved['company_id']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobSaved['company_id']);
                    $appliedStatus = $this->Jobpost_Model->applied_status_fetch($jobSaved['id'], $userTokenCheck[0]['id']);
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobSaved['id']);
                        
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else{
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                    if($appliedStatus) {
                        $jobstatus = $appliedStatus[0]['status'];
                    } else{
                        $jobstatus = 0;
                    }
                    
                    $image = $this->Jobpost_Model->job_image($jobSaved['id']);
                        
                        
                    if(!empty($image[0]['pic'])) {
                        $jobpic = $image[0]['pic'];
                    } else{
                        $jobpic = " ";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($jobSaved['id'], $userTokenCheck[0]['id']);
                    //print_r($savedjob);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }
                    
                     $jobsal = $this->Jobpost_Model->getExpJob($jobSaved['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = '0';
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($jobSaved['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }
                    $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');    
                    $data["jobList"][] = [
                            "jobpost_id" => $jobSaved['id'],
                            "job_title" => $jobSaved['jobtitle'],
                            "jobDesc" => $jobSaved['jobDesc'],
                            "salary" => $basicsalary,
                            "status" => $jobstatus,
                            "company_id" =>$jobSaved['company_id'],
                            "jobexpire" =>$jobSaved['jobexpire'],
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "toppicks1" => $dataJobList[0],
                            "toppicks2" => $dataJobList[1],
                            "toppicks3" => $dataJobList[2],
                            "jobpic" => $jobpic,
                            "savedjob" => $savedjob,
                            "distance" => $distance
                        ];
                }
            } else {
                $data["jobList"] = [];
            }
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
            $response = ['saveJobListing' => $data, 'notification_count' => count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

   

   public function filters_post() {
        $filterData = $this->input->post();
        $data = ["token" => $filterData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        $data["jobList"] = [];
        if($userTokenCheck) {

            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            
            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'], $expfilter);
            //echo $this->db->last_query();die;
            // for($ikj = array_search($expfilter,$temparray);$ikj > -1;$ikj--){
            //         $expfilter = $temparray[$ikj];
                    
            //         //echo $this->db->last_query();
            //         if(!empty($jobfetchs)){
            //             break;
            //         }
            //      }
                             
            //$duplicatejobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],"All Tenure");
            //$jobfetchs = array_merge($jobfetchs,$duplicatejobfetchs);
            //$jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'], $expfilter);
            //print_r($jobfetchs);die;
           // echo $this->db->last_query();die;
            if($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {

                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    
                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $one = (string)$jobTop[0]['picks_id'];
                            $dataJobList[0] = "$one";
                        } else{
                            $dataJobList[0] = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $two = (string)$jobTop[1]['picks_id'];
                            $dataJobList[1] = "$two";
                        } else {
                            $dataJobList[1] = "";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $three = (string)$jobTop[2]['picks_id'];
                            $dataJobList[2] = "$three";
                        } else{
                            $dataJobList[2] = "";
                        }
                    } else {
                        $dataJobList[0] = "";
                        $dataJobList[1] = "";
                        $dataJobList[2] = "";
                    }
                    //print_r($jobfetch);die;
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    $sitename = $this->Jobpost_Model->company_name_fetch($jobfetch['company_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    if(!empty($sitename[0]["cname"])){
                        $companyName = $sitename[0]["cname"];
                    }else{
                        $companyName = '';
                    }
                    //echo $this->db->last_query();die;
                    $sal = $this->Jobpost_Model->fetch_highsalary123($jobfetch['id'],$expfilter);
                    //echo $this->db->last_query();die;
                     $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                     if(!empty($companyPic[0]['companyPic'])) {
                        $comPic =trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                     if(!empty($image[0]['pic'])) {
                         $jobpic =$image[0]['pic'];
                     } else{
                         $jobpic =$comPic;
                     }    
                    
                    
                    $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    if($jobfetch['jobPitch']){
                        $jobfetch['jobPitch']=$jobfetch['jobPitch'];
                      }else{
                          $jobfetch['jobPitch']="";
                      }
                      //echo $jobfetch['allowance'];die;
                      $total_salary = (int)$sal[0]['salary'] + (int)$jobfetch['allowance'];
                      $distance = $this->distance($filterData['cur_lat'], $filterData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                    $data["jobList"][] = [
                            "jobpost_id" => $jobfetch['id'],
                            "job_title" => $jobfetch['jobtitle'],
                            "jobDesc" => $jobfetch['jobPitch'],
                            "salary" => (int)$jobfetch['salary'],
                            "jobExpire" => $jobfetch['jobexpire'],
                            "address" => $jobfetch['address'],
                            "companyName" => $companyName,
                            "cname" => $cname,
                            "company_id" => $jobfetch['company_id'],
                            "latitude" => $jobfetch['latitude'],
                            "longitude" => $jobfetch['longitude'],
                            "jobcount" => 1,
                            "toppicks1" => $dataJobList[0],
                            "toppicks2" => $dataJobList[1],
                            "toppicks3" => $dataJobList[2],
                            "companyPic" => $comPic,
                            "jobpic" => $jobpic,
                            "savedjob" => $savedjob,
                            "distance" => $distance
                        ];
                }
                
                if(count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
                $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'], $expfilter);
                //echo $this->db->last_query();die;
                //print_r($jobcountfetchs);die;
                /*for($ikj = array_search($expfilter,$temparray);$ikj > -1;$ikj--){
                    $expfilter = $temparray[$ikj];
                    $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'], $expfilter);
                    //echo $this->db->last_query();
                    if(!empty($jobcountfetchs)){
                        break;
                    }
                 }*/


               // $duplicatejobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'],"All Tenure");   
            
                //$jobcountfetchs = array_merge($jobcountfetchs,$duplicatejobcountfetchs);
               // print_r($jobcountfetchs);die;
                  $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

                 //  echo "<pre>";

                 // print_r($returnnewarray);

                 // exit();
                 $jobcountfetchs = $returnnewarray;

                if($jobcountfetchs) {
                    foreach ($jobcountfetchs as $jobcountfetch) {

                        
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }

                        $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobcountfetch['latitude'], $jobcountfetch['longitude'], $userTokenCheck[0]['id'],$expfilter);
                        //print_r($getlatlognids);die;
                        $getmaxsal = array();
                        foreach($getlatlognids as $getlatlognid) {
                            //echo $expfilter;
                            $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary123($getlatlognid['id'],$expfilter);
                            //echo $this->db->last_query();
                            $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                        }
                        //echo $getmaxsal;die;
                        
                        if($getmaxsal){
                            $gotsalmax =  max($getmaxsal);
                        }else{
                            $sal = $this->Jobpost_Model->fetch_highsalary123($jobcountfetch['id'],$expfilter);
                            $gotsalmax = (int)$sal[0]['salary'];
                        }
                        
                        $total_salary = (int)$gotsalmax + (int)$jobcountfetch['allowance'];
                        $data["jobcount"][] = [
                                "company_id" => $jobcountfetch['company_id'],
                                "salary" => (int)$jobcountfetch['salary'],
                                //"salary" => (string)$total_salary,
                                "latitude" => $jobcountfetch['latitude'],
                                "longitude" => $jobcountfetch['longitude'],
                                "address" => $jobcountfetch['address'],
                                "jobcount" => (string)$jobcountfetch['jobcount'],
                                "toppicks1" => $dataJobList[0],
                                "toppicks2" => $dataJobList[1],
                                "toppicks3" => $dataJobList[2]
                            ];
                }
                        
                } else{
                    $data['jobcount'] = [];
                }
            
                $response = ['filters' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else{
                $expfilter ="";
                $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'], $expfilter);
                //print_r($jobfetchs);die;
               // echo $this->db->last_query();die;
                if($jobfetchs) {
                    foreach ($jobfetchs as $jobfetch) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        //print_r($jobfetch);die;
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                        //echo $this->db->last_query();die;
                        $sal = $this->Jobpost_Model->fetch_highsalary($jobfetch['id'],$expfilter);
                         $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                            
                         if(!empty($companyPic[0]['companyPic'])) {
                            $comPic =trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                         if(!empty($image[0]['pic'])) {
                             $jobpic =$image[0]['pic'];
                         } else{
                             $jobpic =$comPic;
                         }    
                        
                        
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        if($jobfetch['jobPitch']){
                            $jobfetch['jobPitch']=$jobfetch['jobPitch'];
                          }else{
                              $jobfetch['jobPitch']="";
                          }
                          //echo $jobfetch['allowance'];die;
                          $total_salary = (int)$sal[0]['salary'] + (int)$jobfetch['allowance'];
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobPitch'],
                                "salary" => $sal[0]['salary'],
                                //"salary" => (string)$total_salary,
                                "address" => $jobfetch['address'],
                                "companyName" => $jobfetch['cname'],
                                "latitude" => $jobfetch['latitude'],
                                "longitude" => $jobfetch['longitude'],
                                "jobcount" => 1,
                                "toppicks1" => $dataJobList[0],
                                "toppicks2" => $dataJobList[1],
                                "toppicks3" => $dataJobList[2],
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob
                            ];
                    }
                    
                    if(count($data["jobList"]) <= 0) {
                        $data["jobList"][] = [];
                    }

                    $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
                   // print_r($jobcountfetchs);die;
                    if($jobcountfetchs) {
                        foreach ($jobcountfetchs as $jobcountfetch) {

                            
                            $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                           
                            if($jobTop) {
                                if(isset($jobTop[0]['picks_id'])){
                                    $one = (string)$jobTop[0]['picks_id'];
                                    $dataJobList[0] = "$one";
                                } else{
                                    $dataJobList[0] = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $two = (string)$jobTop[1]['picks_id'];
                                    $dataJobList[1] = "$two";
                                } else {
                                    $dataJobList[1] = "";
                                }
                                if(isset($jobTop[2]['picks_id'])){
                                    $three = (string)$jobTop[2]['picks_id'];
                                    $dataJobList[2] = "$three";
                                } else{
                                    $dataJobList[2] = "";
                                }
                            } else {
                                $dataJobList[0] = "";
                                $dataJobList[1] = "";
                                $dataJobList[2] = "";
                            }

                            $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobcountfetch['latitude'], $jobcountfetch['longitude'], $userTokenCheck[0]['id']);
                            //print_r($getlatlognids);die;
                            $getmaxsal = array();
                            foreach($getlatlognids as $getlatlognid) {
                                $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary($getlatlognid['id'],$expfilter);
                                $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                            }
                            //echo $getmaxsal;die;
                            
                            if($getmaxsal){
                                $gotsalmax =  max($getmaxsal);
                            }else{
                                $sal = $this->Jobpost_Model->fetch_highsalary($jobcountfetch['id'],$expfilter);
                                $gotsalmax = (int)$sal[0]['salary'];
                            }
                            
                            $total_salary = (int)$gotsalmax + (int)$jobcountfetch['allowance'];
                            $data["jobcount"][] = [
                                    "company_id" => $jobcountfetch['company_id'],
                                    "salary" => $gotsalmax,
                                    //"salary" => (string)$total_salary,
                                    "latitude" => $jobcountfetch['latitude'],
                                    "longitude" => $jobcountfetch['longitude'],
                                    "address" => $jobcountfetch['address'],
                                    "jobcount" => $jobcountfetch['jobcount'],
                                    "toppicks1" => $dataJobList[0],
                                    "toppicks2" => $dataJobList[1],
                                    "toppicks3" => $dataJobList[2]
                                ];
                    }
                            
                    } else{
                        $data['jobcount'] = [];
                    }
                
                    $response = ['filters' => $data, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);

                } else{
                    $errors = "No Jobs available. Please update your experience to get more relevant Jobs.";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobFilters_post() {
        $filterData = $this->input->post();
        //print_r($filterData);exit();
        $data = ["token" => $filterData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
       // print_r($userTokenCheck);exit();
        $data["jobList"] = [];
        $jobListing = [];

        if($userTokenCheck) {
            
            if(isset($filterData['exp_year']) && isset($filterData['exp_month'])){
                $expmonth = $filterData['exp_month'];
                $expyear = $filterData['exp_year'];
                
            }else{
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];
                
            }
            
            //echo $expyear;die;
            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            

            if($filterData['save_notify']=='1'){
                $save_keys_array=['user_id'=>$userTokenCheck[0]['id'],'salarySort'=>$filterData['salarySort'], 'locationSort'=>$filterData['locationSort'], 'basicSalary'=>$filterData['basicSalary'], 'joblevel'=>$filterData['joblevel'], 'industry'=>$filterData['industry'], 'jobcategory'=>$filterData['jobcategory'], 'location'=>$filterData['location'], 'jobsubcategory'=>$filterData['jobsubcategory'], 'language'=>$filterData['language'], 'toppicks'=>$filterData['toppicks'], 'allowances'=>$filterData['allowances'], 'medical'=>$filterData['medical'], 'workshift'=>$filterData['workshift'], 'lat'=>$filterData['lat'], 'long'=>$filterData['long']];
                $key_data = $this->Jobpost_Model->fetch_keys($userTokenCheck[0]['id']);
                if(empty($key_data)){
                    $this->Jobpost_Model->insert_keys($save_keys_array);
                }else{
                    $this->Jobpost_Model->update_keys($save_keys_array, $userTokenCheck[0]['id']);
                }
                
            }
            
            $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
           // echo $this->db->last_query();die;
            /*
            for($ikj = array_search($expfilter,$temparray);$ikj > -1;$ikj--){
                    $expfilter = $temparray[$ikj];
                    $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);

                    if(!empty($jobfetchs)){
                        break;
                    }
                 }*/
            

                 
            //$duplicatejobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],"All Tenure");   
            
            //$jobfetchs = array_merge($jobfetchs,$duplicatejobfetchs);  
           //echo $this->db->last_query();die;


            if($jobfetchs) {
                $x=1;
                foreach ($jobfetchs as $jobfetch) {


                        
                  $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                  //print_r($jobTop);die;  

                   $array_first = array();

                   foreach ($jobTop as $cgvalue) {
                       $array_first[] = $cgvalue['picks_id'];
                   }

                   $array_second = explode(",",$filterData['toppicks']);

                   $jobfetch['countbytemp'] = sizeof(array_intersect($array_first,$array_second));

                  if($jobTop) {
                      if(isset($jobTop[0]['picks_id'])) {
                          $one = (string)$jobTop[0]['picks_id'];
                          $dataJobList[0] = "$one";
                      } else{
                          $dataJobList[0] = "";
                      }
                      if(isset($jobTop[1]['picks_id'])){
                          $two = (string)$jobTop[1]['picks_id'];
                          $dataJobList[1] = "$two";
                      } else{
                          $dataJobList[1] = "";
                      }
                      if(isset($jobTop[2]['picks_id'])){
                          $three = (string)$jobTop[2]['picks_id'];
                          $dataJobList[2] = "$three";
                      } else{
                          $dataJobList[2] = "";
                      }
                  } else {
                      $dataJobList[0] = "";
                      $dataJobList[1] = "";
                      $dataJobList[2] = "";
                  }


                  if($jobfetch['id']){
                      $jobfetch['id']=$jobfetch['id'];
                  }else{
                      $jobfetch['id']="";
                  }
                  if($jobfetch['jobtitle']){
                      $jobfetch['jobtitle']=$jobfetch['jobtitle'];
                  }else{
                      $jobfetch['jobtitle']="";
                  }
                  if($jobfetch['jobPitch']){
                      $jobfetch['jobPitch']=$jobfetch['jobPitch'];
                  }else{
                      $jobfetch['jobPitch']="";
                  }
                  
                  if($jobfetch['cname']){
                      $jobfetch['cname']=$jobfetch['cname'];
                  }else{
                      $jobfetch['cname']="";
                  }
                  if($jobfetch['latitude']){
                      $jobfetch['latitude']=$jobfetch['latitude'];
                  }else{
                      $jobfetch['latitude']="";
                  }
                  if($jobfetch['longitude']){
                      $jobfetch['longitude']=$jobfetch['longitude'];
                  }else{
                      $jobfetch['longitude']="";
                  }

                  $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                  
                  if(!empty($companyPic[0]['companyPic'])) {
                      $comPic = trim($companyPic[0]['companyPic']);
                  } else{
                      $comPic = "";
                  }

                  $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);

                  if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                  if($filterData['basicSalary']) {
                     $sal = $this->Jobpost_Model->fetch_highsalary12($jobfetch['id'], $filterData['basicSalary'],$expfilter);
                  } else{
                     $sal = $this->Jobpost_Model->fetch_highsalary123($jobfetch['id'],$expfilter);
                    //echo $jobfetch['id']."<br>";
                  // print_r($sal); 
                  }


                  $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                  
                  
                      if(!empty($image[0]['pic'])) {
                          $jobpic = $image[0]['pic'];
                      } else{
                          $jobpic = $comPic;
                      }
                      $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                      if(!empty($savedjob[0]['id'])) {
                          $savedjob = '1';
                      } else{
                          $savedjob = "0";
                      }
                      $total_salary = (int)$jobfetch['salary']+(int)$jobfetch['allowance'];
                       $distance = $this->distance($filterData['cur_lat'], $filterData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                     $jobListing[] = [
                          "jobpost_id" => $jobfetch['id'],
                          "job_title" => $jobfetch['jobtitle'],
                          "jobDesc" => $jobfetch['jobPitch'],
                          "job_count"=>$jobfetch['countbytemp'],
                          "salary" => (int)$jobfetch['salary'],
                          "company_id" => $jobfetch['company_id'],
                          "jobExpire" => $jobfetch['jobexpire'],
                          "companyName" => $jobfetch['cname'],
                          "cname"=> $cname,
                          "latitude" => $jobfetch['latitude'],
                          "longitude" => $jobfetch['longitude'],
                          "address" => $jobfetch['address'],
                          "jobcount" =>1,
                          "toppicks1" => $dataJobList[0],
                          "toppicks2" => $dataJobList[1],
                          "toppicks3" => $dataJobList[2],
                          "companyPic" => $comPic,
                          "jobpic" => $jobpic,
                          "savedjob" => $savedjob,
                          "distance" => $distance
                      ];
                    $x++;
                }
                if(count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else{
                $data["jobList"] = [];
            }

          //  die();

            
            if($filterData['salarySort'] == 1) {
               $price = array();
               foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
               }
               array_multisort($price, SORT_DESC, $jobListing);
               $data["jobList"] = $jobListing;
            }

            else{

                 $price = array();
               foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['job_count'];
               }
               array_multisort($price, SORT_DESC, $jobListing);
               
                $data["jobList"] = $jobListing;
            }
            
            $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
/*
            for($ikj = array_search($expfilter,$temparray);$ikj > -1;$ikj--){
                    $expfilter = $temparray[$ikj];
                    $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);

                    if(!empty($jobcountfetchs)){
                        break;
                    }
                 }*/
            //$duplicatejobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id'],"All Tenure");   
            
            //$jobcountfetchs = array_merge($jobcountfetchs,$duplicatejobcountfetchs); 
            //print_r($jobcountfetchs);die;
                 $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

                  $jobcountfetchs = $returnnewarray;
            if($jobcountfetchs && $jobcountfetchs[0]['jobcount'] != 0) {
                
               foreach ($jobcountfetchs as $jobcountfetch) {
                        
                  $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                  
                  if($jobTop) {
                      if(isset($jobTop[0]['picks_id'])){
                          $one = (string)$jobTop[0]['picks_id'];
                          $dataJobList[0] = "$one";
                      } else{
                          $dataJobList[0] = "";
                      }
                      if(isset($jobTop[1]['picks_id'])) {
                          $two = (string)$jobTop[1]['picks_id'];
                          $dataJobList[1] = "$two";
                      } else {
                          $dataJobList[1] = "";
                      }
                      if(isset($jobTop[2]['picks_id'])){
                          $three = (string)$jobTop[2]['picks_id'];
                          $dataJobList[2] = "$three";
                      } else{
                          $dataJobList[2] = "";
                      }
                  } else {
                      $dataJobList[0] = "";
                      $dataJobList[1] = "";
                      $dataJobList[2] = "";
                  }

                  $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobcountfetch['latitude'], $jobcountfetch['longitude'], $userTokenCheck[0]['id'],$expfilter);

                 // print_r($getlatlognids);

                  $getmaxsal = array();

                  foreach($getlatlognids as $getlatlognid) {

                     if($filterData['basicSalary']) {
                        $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary12($getlatlognid['id'],$filterData['basicSalary'],$expfilter);
                     } else{
                        $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary123($getlatlognid['id'],$expfilter);
                     }
                     if(!empty($salaryarrays[0]['basesalary'])){
                        $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                     } 

                    // print_r($salaryarrays);
                      
                  }

                  
                  //$gotsalmax =  max($getmaxsal);
                 // $total_salary = (int)$gotsalmax + (int)$jobcountfetch['allowance'];
                  $data["jobcount"][] = [
                          "company_id" => $jobcountfetch['company_id'],
                          "salary" => (int)$jobcountfetch['salary'],
                          "latitude" => $jobcountfetch['latitude'],
                          "longitude" => $jobcountfetch['longitude'],
                          "address" => $jobcountfetch['address'],
                          "jobcount" => (string)($jobcountfetch['jobcount']),
                          "toppicks1" => $dataJobList[0],
                          "toppicks2" => $dataJobList[1],
                          "toppicks3" => $dataJobList[2]
                      ];
               } 

               //die();

            } else{
                
                $expfilter ="";
                $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
           
                if($jobfetchs) {
                    $x=1;
                    foreach ($jobfetchs as $jobfetch) {


                            
                      $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    

                       $array_first = array();

                       foreach ($jobTop as $cgvalue) {
                           $array_first[] = $cgvalue['picks_id'];
                       }

                       $array_second = explode(",",$filterData['toppicks']);

                       $jobfetch['countbytemp'] = sizeof(array_intersect($array_first,$array_second));

                      if($jobTop) {
                          if(isset($jobTop[0]['picks_id'])) {
                              $one = (string)$jobTop[0]['picks_id'];
                              $dataJobList[0] = "$one";
                          } else{
                              $dataJobList[0] = "";
                          }
                          if(isset($jobTop[1]['picks_id'])){
                              $two = (string)$jobTop[1]['picks_id'];
                              $dataJobList[1] = "$two";
                          } else{
                              $dataJobList[1] = "";
                          }
                          if(isset($jobTop[2]['picks_id'])){
                              $three = (string)$jobTop[2]['picks_id'];
                              $dataJobList[2] = "$three";
                          } else{
                              $dataJobList[2] = "";
                          }
                      } else {
                          $dataJobList[0] = "";
                          $dataJobList[1] = "";
                          $dataJobList[2] = "";
                      }


                      if($jobfetch['id']){
                          $jobfetch['id']=$jobfetch['id'];
                      }else{
                          $jobfetch['id']="";
                      }
                      if($jobfetch['jobtitle']){
                          $jobfetch['jobtitle']=$jobfetch['jobtitle'];
                      }else{
                          $jobfetch['jobtitle']="";
                      }
                      if($jobfetch['jobPitch']){
                          $jobfetch['jobPitch']=$jobfetch['jobPitch'];
                      }else{
                          $jobfetch['jobPitch']="";
                      }
                      
                      if($jobfetch['cname']){
                          $jobfetch['cname']=$jobfetch['cname'];
                      }else{
                          $jobfetch['cname']="";
                      }
                      if($jobfetch['latitude']){
                          $jobfetch['latitude']=$jobfetch['latitude'];
                      }else{
                          $jobfetch['latitude']="";
                      }
                      if($jobfetch['longitude']){
                          $jobfetch['longitude']=$jobfetch['longitude'];
                      }else{
                          $jobfetch['longitude']="";
                      }

                      $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                      
                      if(!empty($companyPic[0]['companyPic'])) {
                          $comPic = trim($companyPic[0]['companyPic']);
                      } else{
                          $comPic = "";
                      }

                      $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);

                      if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }
                      if($filterData['basicSalary']) {
                         $sal = $this->Jobpost_Model->fetch_highsalary12($jobfetch['id'], $filterData['basicSalary']);
                      } else{
                         $sal = $this->Jobpost_Model->fetch_highsalary($jobfetch['id'],$expfilter);
                      }
                      $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                      
                      
                          if(!empty($image[0]['pic'])) {
                              $jobpic = $image[0]['pic'];
                          } else{
                              $jobpic = $comPic;
                          }
                          $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                          if(!empty($savedjob[0]['id'])) {
                              $savedjob = '1';
                          } else{
                              $savedjob = "0";
                          }
                          $total_salary = (int)$sal[0]['salary']+(int)$jobfetch['allowance'];
                         $jobListing[] = [
                              "jobpost_id" => $jobfetch['id'],
                              "job_title" => $jobfetch['jobtitle'],
                              "jobDesc" => $jobfetch['jobPitch'],
                              "job_count"=>$jobfetch['countbytemp'],
                              "salary" => $sal[0]['salary'],
                              //"salary" => (string)$total_salary,
                              "companyName" => $jobfetch['cname'],
                              "cname"=> $cname,
                              "latitude" => $jobfetch['latitude'],
                              "longitude" => $jobfetch['longitude'],
                              "address" => $jobfetch['address'],
                              "jobcount" =>1,
                              "toppicks1" => $dataJobList[0],
                              "toppicks2" => $dataJobList[1],
                              "toppicks3" => $dataJobList[2],
                              "companyPic" => $comPic,
                              "jobpic" => $jobpic,
                              "savedjob" => $savedjob
                          ];
                        $x++;
                    }
                    if(count($data["jobList"]) <= 0) {
                        $data["jobList"][] = [];
                    }
                } else{
                    $data["jobList"] = [];
                }



                
                if($filterData['salarySort'] == 1) {
                   $price = array();
                   foreach ($jobListing as $key => $row) {
                        $price[$key] = $row['salary'];
                   }
                   array_multisort($price, SORT_DESC, $jobListing);
                   $data["jobList"] = $jobListing;
                }

                else{

                     $price = array();
                   foreach ($jobListing as $key => $row) {
                        $price[$key] = $row['job_count'];
                   }
                   array_multisort($price, SORT_DESC, $jobListing);
                   
                    $data["jobList"] = $jobListing;
                }




                $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
     
                if($jobcountfetchs && $jobcountfetchs[0]['jobcount'] != 0) {
                    
                   foreach ($jobcountfetchs as $jobcountfetch) {
                            
                      $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                      
                      if($jobTop) {
                          if(isset($jobTop[0]['picks_id'])){
                              $one = (string)$jobTop[0]['picks_id'];
                              $dataJobList[0] = "$one";
                          } else{
                              $dataJobList[0] = "";
                          }
                          if(isset($jobTop[1]['picks_id'])) {
                              $two = (string)$jobTop[1]['picks_id'];
                              $dataJobList[1] = "$two";
                          } else {
                              $dataJobList[1] = "";
                          }
                          if(isset($jobTop[2]['picks_id'])){
                              $three = (string)$jobTop[2]['picks_id'];
                              $dataJobList[2] = "$three";
                          } else{
                              $dataJobList[2] = "";
                          }
                      } else {
                          $dataJobList[0] = "";
                          $dataJobList[1] = "";
                          $dataJobList[2] = "";
                      }

                      $getmaxsal = array();
                      $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobcountfetch['latitude'], $jobcountfetch['longitude'], $userTokenCheck[0]['id'],$expfilter);
                      foreach($getlatlognids as $getlatlognid) {

                         if($filterData['basicSalary']) {
                            $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary1($getlatlognid['id'],$filterData['basicSalary']);
                         } else{
                            $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary($getlatlognid['id'],$expfilter);
                         }
                          $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                      }
                      $gotsalmax =  max($getmaxsal);
                      $total_salary = (int)$gotsalmax + (int)$jobcountfetch['allowance'];
                      $data["jobcount"][] = [
                              "company_id" => $jobcountfetch['company_id'],
                              "salary" => $gotsalmax,
                              "latitude" => $jobcountfetch['latitude'],
                              "longitude" => $jobcountfetch['longitude'],
                              "address" => $jobcountfetch['address'],
                              "jobcount" => $jobcountfetch['jobcount'],
                              "toppicks1" => $dataJobList[0],
                              "toppicks2" => $dataJobList[1],
                              "toppicks3" => $dataJobList[2]
                          ];
                   } 

                } else{
                    $data['jobcount'] = [];
                }

            }
            
            $response = ['jobFilters' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobtitlecache_post() {
        $cacheData = $this->input->post();
        $data = ["token" => $cacheData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {
            
            if(!empty($cacheData['title'])) {
              $cachecheck = $this->Jobpost_Model->jobtitlecache_check($cacheData['title'], $userTokenCheck[0]['id']);
              
              if(!$cachecheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'], "title"=> $cacheData['title']];
                  $this->Jobpost_Model->jobtitlecache_insert($data);
              }
            }
            $cacheGet = $this->Jobpost_Model->jobtitlecache_fetch($userTokenCheck[0]['id']);
            if($cacheGet) {

            } else{
              $cacheGet = [];
            }
            $response = ['jobtitlecache' => $cacheGet, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function keywordcache_post() {
        $cacheData = $this->input->post();
        $data = ["token" => $cacheData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {
            
            if(!empty($cacheData['keyword'])) {
              $cachecheck = $this->Jobpost_Model->keywordcache_check($cacheData['keyword'], $userTokenCheck[0]['id']);
              
              if(!$cachecheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'], "keyword"=> $cacheData['keyword']];
                  $this->Jobpost_Model->keywordcache_insert($data);
              }
            }
            $cacheGet = $this->Jobpost_Model->keywordcachee_fetch($userTokenCheck[0]['id']);
            if($cacheGet) {

            } else{
              $cacheGet = [];
            }
            $response = ['keywordcache' => $cacheGet, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companySchedule_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
            $getDays = $this->Jobpost_Model->getCompDetails($jobData['companyId']);
            
            $days1 = $getDays[0]['dayfrom'];
            $days2 = $getDays[0]['dayto'];
            $z=1;

            for($i=$days1; $i<=$days2; $i++) {
                if($i == 1) {
                    $dayname = "Monday";
                } else if($i == 2) {
                    $dayname = "Tuesday";
                } else if($i == 3) {
                    $dayname = "Wednesday";
                } else if($i == 4) {
                    $dayname = "Thursday";
                } else if($i == 5) {
                    $dayname = "Friday";
                } else if($i == 6) {
                    $dayname = "Saturday";
                } else if($i == 7) {
                    $dayname = "Sunday";
                }
                $data['days'][] = $dayname;
              $z++;
            }

            $time1 = $getDays[0]['from_time'];
            $time11 = explode(":", $time1);
            $time111 = $time11[0];
            $time2 = $getDays[0]['to_time'];
            $time22 = explode(":", $time2);
            $time222 = $time22[0];

            $data['timeFrom'] = $time111;
            $data['timeTo'] = $time222;

            //var_dump($data);die;
            
            $response = ['companySchedule' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobtitlelist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {  
            $jobtitle = $this->Common_Model->getCompJobTitle();
            $response = ['jobtitlelist' => $jobtitle, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }
    
    public function deletesaveJob_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {

                $this->Jobpost_Model->savejob_delete($job['jobpost_id'],$userTokenCheck[0]['id']);
                $response = ['message' => "Deleted Successfully", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }
    
    public function hotjobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $userLat = $userData['lat'];
            $userLong = $userData['long'];
            $jobfetchs = $this->Jobpost_Model->job_fetch_latlong($userLat, $userLong);
            //echo $this->db->last_query();die;
            if($jobfetchs) {
                $x=0;
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    //print_r($jobTop);
                    if(count($jobTop)>1) {

                    } else{
                        $jobTop = [];
                    }
                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = $comPic;
                        }
                        
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }
                        if(count($jobTop)>1){
                            $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $basicsalary,
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob
                            ];
                        }/*else{
                            $data["jobList"][] = [];
                        }*/
                        
                    }
                    $x++;
                }
                
                $response = ['jobListing' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else{
                $errors = "No data found";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }
    
    public function hotjobListings_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter);
            //print_r($hotjobData);die;
            //echo $this->db->last_query();    die;
            //$x=0;
            //$return_array = array();
            $data1 = array();
            $data1["hotjobss"]=[];
            foreach ($hotjobData as $hotjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
              
                if(!empty($jobTop) && count($jobTop)>1) {
                      //echo "</pre>";    
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                      $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                         $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address']))
                        {
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else
                        {
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                        if(isset($jobImage[0]['pic']))
                        {
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else
                        {
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1["hotjobss"][] = [
                        "jobpost_id" => $hotjobsData['id'],
                        "comapnyId" =>$hotjobsData['compId'],
                        "job_title" => $hotjobsData['jobtitle'],
                        "jobDesc" => $hotjobsData['jobDesc'],
                        "salary" => $hotjobsData['salary'],
                        "companyName"=> $companydetail[0]["cname"],
                        "cname"=> $cname,
                        "companyAddress"=>$companyaddress,
                        "toppicks1" => $datatoppicks1,
                        "toppicks2" => $datatoppicks2,
                        "toppicks3" => $datatoppicks3,
                        "job_image" =>$jobImageData,
                        "latitude"=> $hotjobsData['latitude'], 
                        "longitude"=>$hotjobsData['longitude'],
                        "jobexpire"=> $hotjobsData['jobexpire'],
                        "companyPic"=> $comPic,
                        "savedjob" =>$savedjob,
                        "boostjob" =>$hotjobsData['boost_status'],
                        "distance" => $distance
                        ];

                }
            }
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
            $response = ['hotjobslist' => $data1,'notification_count' =>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function notificationlist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);
        
        if($userTokenCheck) {
            //$tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            //$userTokenCheck = $this->User_Model->token_match($tokendata);
        
            //if($userTokenCheck) {  
            //var_dump($userTokenCheck);die;
                $data2 = ["badge_count"=>0];
                      //var_dump($data2);die;
                $notificationUpdate = $this->Candidate_Model->notificationbadge_update($userTokenCheck[0]['user_id'],$data2);

                $notification_list = $this->Common_Model->getnotification($userTokenCheck[0]['user_id']);

                

                $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['user_id']);
                //print_r($notification_list);die;
                $x=0;
                
                if(!empty($notification_list)){
                    foreach ($notification_list as $notification_lists) {
                        $companyName = $this->Jobpost_Model->company_detail_fetch($notification_lists['recruiter_id']);
                        $job_detail = $this->Jobpost_Model->job_fetch_appliedid($notification_lists['jobapp_id']);
                        $reviews = $this->Jobpost_Model->review_listsbyuser($notification_lists['recruiter_id'],$userTokenCheck[0]['id']);
                        $story = $this->Jobpost_Model->story_listsbyuser($notification_lists['recruiter_id'],$userTokenCheck[0]['id']);
                        if($reviews){
                            $reviewstatus = '1';
                        }else{
                            $reviewstatus = '0';
                        }
                        if($story){
                            $storystatus = '1';
                        }else{
                            $storystatus = '0';
                        }
                        //print_r($reviews);
                        //print_r($job_detail);die;
                        if(!empty($companyName[0]['cname'])){
                            $companyName[0]['cname']=$companyName[0]['cname'];
                        }else{
                            $companyName[0]['cname']='';
                        }
                        if(!empty($job_detail[0]['jobpost_id'])){
                            $job_detail[0]['jobpost_id'] = $job_detail[0]['jobpost_id'];
                        }else{
                            $job_detail[0]['jobpost_id']='';
                        }


                        //print_r($companyName);
                        $notification_lists1[$x] = [
                              "id" => $notification_lists['id'],
                              "notification" => $notification_lists['notification'],
                              "status" => $notification_lists['status'],
                              "recruiter_id" => $notification_lists['recruiter_id'],          
                              "job_status" =>   $notification_lists['job_status'],
                              "companyname" =>  $companyName[0]['cname'],
                              "jobpost_id" =>  $job_detail[0]['jobpost_id'],
                              "date" => date("F d, Y h:i a", strtotime($notification_lists['createdon'])),
                              "reviewstatus" => $reviewstatus,
                              "storystatus" => $storystatus
                            ];
                        $x++;
                    }
                }else{
                    $notification_lists1=[];
                }  
                
                $response = ['notificationlist' => $notification_lists1,"promo_count" => (string)count($fetchpromonotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            //}
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function promonotificationlist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);
        
        if($userTokenCheck) {
            //$tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            //$userTokenCheck = $this->User_Model->token_match($tokendata);
            //if($userTokenCheck) {  
            
                $data2 = ["badge_count"=>0];
                $notificationUpdate = $this->Candidate_Model->promonotificationbadge_update($userTokenCheck[0]['user_id'],$data2);
                $notification_list = $this->Common_Model->getpromonotification($userTokenCheck[0]['user_id']);
                //print_r($notification_list);die;
                $x=0;
                
                if(!empty($notification_list)) {
                    foreach ($notification_list as $notification_lists) {
                        $notification_lists1[$x] = [
                              "id" => $notification_lists['id'],
                              "title" => urldecode($notification_lists['title']),
                              "message" => urldecode($notification_lists['message']),
                              "date" => date("F d, Y h:i a", strtotime($notification_lists['added_at'])),
                            ];
                        $x++;
                    }
                }else{
                    $notification_lists1=[];
                }  
                
                $response = ['promonotificationlist' => $notification_lists1, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            //}
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function promonotification_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
            $data2 = ["badge_count"=>0];
            $notificationUpdate = $this->Candidate_Model->promobadge_update($userTokenCheck[0]['id'],$data2,$jobData['promo_id']);
            $notification_list = $this->Common_Model->promonotification($userTokenCheck[0]['id'],$jobData['promo_id']);
            
            if(!empty($notification_list)){
                    $notification_lists1 = [
                          "id" => $notification_list[0]['id'],
                          "title" => $notification_list[0]['title'],
                          "message" => $notification_list[0]['message'],
                          "date" => date("F d, Y h:i a", strtotime($notification_list[0]['added_at'])),
                        ];
                 
            }else{
                $notification_lists1=[];
            }  
            
            $response = ['promonotification' => $notification_lists1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function readnotification_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('notification_id', 'notification_id', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            $userTokenCheck = $this->User_Model->token_match1($data);
        
            if($userTokenCheck) {
                // $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
                // $userTokenCheck = $this->User_Model->token_match($tokendata);
                // if($userTokenCheck) {

                    $data = ["status" => '1' ];
                    $notification_status =  $this->Jobpost_Model->read_notification($data,$userData['notification_id'] );

                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                    $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
                    if($notification_status){
                        $response = ['message' => 'read Successfully','notification_count'=>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                //}
            } else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function addstory_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('story', 'story', 'trim|required');
        $this->form_validation->set_rules('recruiter_id', 'recruiter_id', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
            if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            //print_r($userTokenCheck);die;
            if($userTokenCheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'],"recruiter_id"=>$userData['recruiter_id'], "story"=> $userData['story'] ];
                  $story_inserted =  $this->Jobpost_Model->insert_story($data);
                  //echo $this->db->last_query();die;
                  if($story_inserted){
                    $response = ['message' => 'Story added Successfully', 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
                    
                 
            } }else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function storylist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        //print_r($userTokenCheck);die;
        if($userTokenCheck) {  
            $story_list = $this->Jobpost_Model->story_lists();
            //print_r($story_list);die;
            if(!empty($story_list)){
                $x=0;
                foreach ($story_list as $story_lists) {
                    $like_story = $this->Jobpost_Model->liked_stories_count($story_lists['id']);
                    $story_list_count = $this->Jobpost_Model->comment_lists($story_lists['id']);
                    $like_story_status = $this->Jobpost_Model->liked_stories_status($story_lists['id'],$userTokenCheck[0]['id']);
                    if(!empty($like_story_status[0]['status'])){
                        $like_status = $like_story_status[0]['status'];

                    }else{
                        $like_status = "";
                    }
                    //print_r($like_story_status);
                    //echo count($like_story);
                    $story_lists1[$x] = [
                              "story_id" => $story_lists['id'],
                              "user_id" => $userTokenCheck[0]['id'],
                              "date" => date("y-m-d",strtotime($story_lists['createdon'])),
                              "story" => $story_lists['story'],
                              "recruiter_id" => $story_lists['recruiter_id'],
                              "name" => $story_lists['name'],
                              "profilePic" => $story_lists['profilePic'],
                              "like_count" =>   count($like_story),
                              "comment_count" => count($story_list_count),                 
                              "like_status" =>   $like_status ,              
                              "companyName" =>   $story_lists['cname']             

                          ];
                $x++;
                }
            }else{
                $story_lists1 = [];
            }
            
            
            $response = ['storylist' => $story_lists1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function addreviews_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('rating', 'rating', 'trim|required');
        $this->form_validation->set_rules('recommend', 'recommend', 'trim|required');
        $this->form_validation->set_rules('feedback', 'feedback', 'trim|required');
        $this->form_validation->set_rules('recruiter_id', 'recruiter_id', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            //$userTokenCheck = $this->User_Model->token_match($data);
            //print_r($userTokenCheck);die;
            if($userTokenCheck) {
                    $data = ["user_id" => $userTokenCheck[0]['id'],"recruiter_id"=> $userData['recruiter_id'], "rating"=>$userData['rating'],"recommend"=>$userData['recommend'], "feedback"=>$userData['feedback'] ];
                  $review_inserted =  $this->Jobpost_Model->insert_review($data);
                  //echo $this->db->last_query();die;
                  if($review_inserted){
                    $response = ['feedback' => $userData['feedback'], 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
                    
                 
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }
    
    public function reviewlist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        //print_r($userTokenCheck);die;
        if($userTokenCheck) {  
            $review_list = $this->Jobpost_Model->review_lists($jobData['recruiter_id']);
            
            if($review_list){
                $x=0;
              foreach ($review_list as $review_lists) {
                $review_list1[$x] = [
                          "rating" => $review_lists['rating'],
                          "recommend" => $review_lists['recommend'],
                          "date" => date("y-m-d",strtotime($review_lists['created_at'])),
                          "feedback" => $review_lists['feedback'],
                          "name" => $review_lists['name'],
                          "profilePic" => $review_lists['profilePic']                    

                      ];
            $x++;
            }  
            }else{
                $review_list1=[];
            }
            
            $response = ['reviewlist' => $review_list1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }
       
    public function likestory_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('story_id', 'story_id', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $data['liked_stories'] = $this->Jobpost_Model->liked_stories($userData['story_id'], $userTokenCheck[0]['id']);
                if(empty($data['liked_stories'])){
                    $data = ["user_id" => $userTokenCheck[0]['id'],"story_id"=>$userData['story_id'], "status"=> $userData['status'] ];
                      $story_like =  $this->Jobpost_Model->like_story($data);
                      if($story_like){
                        $response = ['message' => 'Story updated Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }else{
                    $data = ["status"=> $userData['status'] ];
                      $story_like =  $this->Jobpost_Model->unlike_story($userTokenCheck[0]['id'],$userData['story_id'],$data);
                      if($story_like){
                        $response = ['message' => 'Story updated Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
                 
            } }else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }


    public function commentstory_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('story_id', 'story_id', 'trim|required');
        $this->form_validation->set_rules('comment', 'comment', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
           // print_r($userTokenCheck);die;
            if($userTokenCheck) {

                $single_story = $this->Jobpost_Model->fetchstory($userData['story_id']);
                //print_r($single_story);die;
                if($single_story[0]['user_id'] != $userTokenCheck[0]['id']){
                    $msg ="You have a new comment on your story";
                    $user_data = $this->User_Model->user_single($single_story[0]['user_id']);
                    //print_r($user_data);die;
                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($user_data[0]['id']);
                    $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($user_data[0]['id']);
                   // print_r($user_data);die;
                    $data = ["user_id" => $userTokenCheck[0]['id'],"story_id"=>$userData['story_id'], "comments"=> $userData['comment'] ];
                      $story_commnet =  $this->Jobpost_Model->add_storycomment($data);
                    if($user_data[0]['notification_status']=='1'){
                        $tokendata = ['user_id'=>$user_data[0]['id']];
                        $userTokenCheck = $this->User_Model->user_token_check($tokendata);
                        //print_r($userTokenCheck);die;
                        foreach ($userTokenCheck as $usertoken) {
                       if($usertoken['device_type']=="android"){
                            $res1 = $this->push_notification_android_customer($usertoken['device_token'],"New Comment", $msg,'comment',$userData['story_id'],count($fetchnotifications)+count($fetchpromonotifications));
                        }else if($usertoken['device_type']=="ios"){
                            $this->pushiphonforcomment(array($usertoken['device_token'],$msg,'comment','New Comment',$userData['story_id'],count($fetchnotifications)+count($fetchpromonotifications)));
                        
                         } }
                    }
                    
                    
                }
                
                //$data['story_comments'] = $this->Jobpost_Model->story_comments($userData['story_id'], $userTokenCheck[0]['id']);
                /*if(empty($data['story_comments'])){*/
                    
                      if($story_commnet){
                        $response = ['message' => 'Comment added Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                /*}else{
                    $data = ["comments"=> $userData['comment'] ];
                      $story_commnet =  $this->Jobpost_Model->update_comment($userTokenCheck[0]['id'],$userData['story_id'],$data);
                      if($story_commnet){
                        $response = ['message' => 'Comment updated Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }*/
                 
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function commentlist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        //print_r($userTokenCheck);die;
        if($userTokenCheck) {  
             $single_story = $this->Jobpost_Model->fetch_singlestory($jobData['story_id']);
             $liked_stories = $this->Jobpost_Model->liked_stories_count($jobData['story_id']);
             $story_list = $this->Jobpost_Model->comment_lists($jobData['story_id']);
             $like_story_status = $this->Jobpost_Model->liked_stories_status($jobData['story_id'],$userTokenCheck[0]['id']);
                if(!empty($like_story_status[0]['status'])){
                    $like_status = $like_story_status[0]['status'];

                }else{
                    $like_status = "";
                }
             //print_r($liked_stories);die;
             $data1['story_list'] = ["story" => $single_story[0]['story'],
                                        "date" => date("y-m-d",strtotime($single_story[0]['createdon'])),
                                        "name" => $single_story[0]['name'],
                                        "profilePic" => $single_story[0]['profilePic'],
                                        "comment_count" => count($story_list),
                                        "like_count" => count($liked_stories),
                                        "like_status" => $like_status
                                    ];
            
            $x=0;
            foreach ($story_list as $story_lists) {
                $data1['story_comments'][$x] = [
                          "comment_id" => $story_lists['id'],
                          "date" => date("y-m-d",strtotime($story_lists['createdon'])),
                          "comments" => $story_lists['comments'],
                          "name" => $story_lists['name'],
                          "profilePic" => $story_lists['profilePic']                    

                      ];
            $x++;
            }
            $response = ['commentlist' => $data1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function deletestorycomment_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $this->form_validation->set_rules('comment_id', 'comment_id', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {

                $this->Jobpost_Model->storycomment_delete($job['comment_id'],$userTokenCheck[0]['id']);
                $response = ['message' => "Deleted Successfully", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function categorylist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        //print_r($userTokenCheck);die;
        if($userTokenCheck) {  
            $category_list = $this->Jobpost_Model->category_lists();
            
            if($category_list){
                $x=0;
              foreach ($category_list as $category_lists) {
                $category_list1[$x] = [
                          "id" => $category_lists['id'],
                          "category" => $category_lists['category']                    

                      ];
            $x++;
            }  
            }else{
                $category_list1=[];
            }
            
            $response = ['categorylist' => $category_list1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function levellist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        //print_r($userTokenCheck);die;
        if($userTokenCheck) {  
            $level_list = $this->Jobpost_Model->level_lists();
            
            if($level_list){
                $x=0;
              foreach ($level_list as $level_lists) {
                $level_list1[$x] = [
                          "id" => $level_lists['id'],
                          "level" => $level_lists['level']                    

                      ];
            $x++;
            }  
            }else{
                $level_list1=[];
            }
            
            $response = ['levellist' => $level_list1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companylist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        //print_r($userTokenCheck);die;
        if($userTokenCheck) {  
            $company_list = $this->Common_Model->company_lists();
            //print_r($company_list);die;
            if($company_list){
                $x=0;
              foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if($checkjob) {
                     $company_lists1[$x] = [
                          "id" => $company_lists['id'],
                          "cname" => $company_lists['cname']                    

                      ];
                     $x++;
                  }
            }  
            }else{
                $company_lists1=[];
            }
            
            $response = ['companylist' => $company_lists1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function subcategorylist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
            $subcategory_list = $this->Jobpost_Model->subcategory_listsbycatid($jobData['category']);
            
            if($subcategory_list){
                $x=0;
              foreach ($subcategory_list as $subcategory_lists) {
                $subcategory_list1[$x] = [
                          "id" => $subcategory_lists['id'],
                          "category" => $subcategory_lists['category_id'],                    
                          "subcategory" => $subcategory_lists['subcategory']                    

                      ];
            $x++;
            }  
            }else{
                $subcategory_list1=[];
            }
            
            $response = ['subcategorylist' => $subcategory_list1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }


    public function filtersubcategorylist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {  
            $subcategory_list = $this->Jobpost_Model->filtersubcategory_listsbycatid($jobData['catids']);
            if($subcategory_list){
                $x=0;
              foreach ($subcategory_list as $subcategory_lists) {
                $subcategory_list1[$x] = [
                          "id" => $subcategory_lists['id'],
                          "category" => $subcategory_lists['category_id'],                    
                          "subcategory" => $subcategory_lists['subcategory']                    

                      ];
            $x++;
            }  
            }else{
                $subcategory_list1=[];
            }
            
            $response = ['filtersubcategorylist' => $subcategory_list1, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function addreconciliation_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('job_id', 'job_id', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                    $data = ["user_id" => $userTokenCheck[0]['id'],"job_id"=>$userData['job_id'], "status"=> $userData['status'] ];
                      $reconciliation =  $this->Jobpost_Model->add_reconciliation($data);
                      if($reconciliation){
                        $response = ['message' => 'Added Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                
                 
            } }else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function statuspopup_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        $now = time();
        
        //echo floor($datediff / (60 * 60 * 24));
        if($userTokenCheck) {
            
            $jobApplyfetch = $this->Jobpost_Model->job_fetch_applied($userTokenCheck[0]['id']);
            //echo $this->db->last_query();die;
            //print_r($jobApplyfetch);die;
            if($jobApplyfetch) {
                    
              foreach ($jobApplyfetch as $jobfetch) {

                  $jobApplyfetchcheck = $this->Jobpost_Model->job_fetch_appliedCheck11($jobfetch['jobpost_id'],$userTokenCheck[0]['id']);
                  //echo $this->db->last_query();die;
                  //print_r($jobApplyfetchcheck);die;
                  if($jobApplyfetchcheck) {
                      
                     $data1["jobList"] = [];
                     $response = ['jobListing' => $data1, 'status' => "SUCCESS"];
                     $this->set_response($response, REST_Controller::HTTP_CREATED);

                  } else{

                     $companyName = $this->Jobpost_Model->fetch_companyName($jobfetch['recruiter_id']);

                      $jobDate = strtotime($jobfetch['updated_at']);
                      //echo $jobDate;die;
                      $datediff = $now - $jobDate;
                      $daydiff = floor($datediff / (60 * 60 * 24));
                      //$hourdiff = round(($now - $jobDate)/3600, 1);
                      //echo $daydiff;die;
                      if($daydiff >= 2) {
                           $data1["jobList"][] = [
                                "jobpost_id" => $jobfetch['jobpost_id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "companyName" => $companyName[0]['cname'],
                                "companyId" => $jobfetch['recruiter_id'],
                                "msg" => "open popup",
                                "name" => $userTokenCheck[0]['name']
                            ];

                           $response = ['jobListing' => $data1, 'status' => "SUCCESS"];
                           $this->set_response($response, REST_Controller::HTTP_CREATED); 
                            
                      } else {
                          $data1["jobList"] = [];
                          $response = ['jobListing' => $data1, 'status' => "SUCCESS"];
                          $this->set_response($response, REST_Controller::HTTP_CREATED);
                      }
                  }
              }
                
            } else{
                $data1["jobList"] = [];
                $response = ['jobListing' => $data1, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function savedate_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('job_id', 'job_id', 'trim|required');
        $this->form_validation->set_rules('date', 'date', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                    $data = ["accepted_date" =>$userData['date']];
                      $saveddate =  $this->Jobpost_Model->save_date($userTokenCheck[0]['id'], $userData['job_id'], $data);
                      if($saveddate){
                        $response = ['message' => 'Date saved Successfully', 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                
                 
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function advertiselist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);

            if($userTokenCheck) {  
                $ad_list = $this->Jobpost_Model->advertise_lists($jobData['cur_lat'],$jobData['cur_long']);
                //echo $this->db->last_query();die;
               // print_r($story_list);die;
                if(!empty($ad_list)){
                    $x=0;
                    foreach ($ad_list as $ad_lists) {
                        $ad_lists1[$x] = [
                              "ad_id" => $ad_lists['id'],
                              "banner" => $ad_lists['banner'],
                              //"banner" => $ad_lists['banner'],
                              "description" => $ad_lists['description'],
                              "recruiter_id" => $ad_lists['site_id'],
                              "profilePic" => $ad_lists['companyPic'],             
                              "companyName" =>   $ad_lists['cname']             

                          ];
                        $x++;
                    }
                }else{
                    $ad_lists1 = [];
                }
                
                
                $response = ['adlist' => $ad_lists1, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    function push_notification_android_customer($regid,$title,$body,$type,$storyid,$badgecount){
        date_default_timezone_set('Asia/Manila');
        $access_key = API_ACCESS_KEY;        
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
//echo date('H:i:s');die;
        $notification = [
            'title' =>$title,
            'body' => $body,
            'type' => $type,
            'story_id' => $storyid,
            'recruiter_id' => '23',
            'companyname' => '22',
            'notification_id' => '11',
            'timestamp' => date('H:i:s'),
            'suggestid' => '2',
            'badgecount' => $badgecount,
            'promo_id' => '002'
        ];

        $extraNotificationData = $notification;
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $regid,
            //'notification' => $notification,
            'data' => $extraNotificationData
        ];
        //print_r($fcmNotification);die;
        $headers1 = [
            'Authorization: key=' . $access_key,
            'Content-Type: application/json'
        ];

        //print_r($fcmNotification);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
      }

      function push_notification_android_status($regid,$title,$body,$type,$storyid,$companyname,$notification_id,$badgecount){
        date_default_timezone_set('Asia/Manila');
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' =>$title,
            'body' => $body,
            'type' => $type,
            'story_id' => '23',
            'recruiter_id' => $storyid,
            'companyname' => $companyname,
            'notification_id' => $notification_id,
            'timestamp' => date('H:i:s'),
            'suggestid' => '2',
            'badgecount' => $badgecount,
            'promo_id' => '002'
        ];
        $extraNotificationData = $notification;
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $regid,
            //'notification' => $notification,
            'data' => $extraNotificationData
        ];
        $headers1 = [
            'Authorization: key=' . $access_key,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
      }

      
      function pushiphon($array)       
      {      //print_r($array);die;                           
           $deviceToken = $array[0];           
           $passphrase = '1234';  
           $this->autoRender = false;
           $this->layout = false;               
           $basePath = "JobYoda.pem";                 
             //echo $basePath;            
           if(file_exists($basePath))             
           {       
           $ctx = stream_context_create(); 
           stream_context_set_option($ctx, 'ssl', 'local_cert', $basePath);
           stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
           $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,  
           $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); 
           if (!$fp)     
           exit("Failed to connect: $err $errstr" . PHP_EOL);
          $body['aps'] = array('alert' => array('title' => $array[3],"body"=>$array[1]),'sound' =>'default','badge'=>$array[5]);
          $body['body'] = $array[1];
          $body['type'] = $array[2];
          $body['recruiter_id'] = $array[4];
          $body['companyname'] = $array[6];
          $body['notification_id'] = $array[7];
          //$body['aps'] = array('alert' => array("body"=>$array[1],'type'=>$array[2],'story_id'=>$array[3]), 'badge' => 1,'sound' =>'default');
          //echo"<pre>";print_r($body['aps']);die; 
          $payload = json_encode($body); 
           $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
          $result = fwrite($fp, $msg, strlen($msg));

           if (!$result)
           {           
                    
           }     
            else  
           {    
            //echo "<pre>";print_r($body['aps']); die;   
              
           }       
             fclose($fp); 
           }    
      }

      function pushiphonforcomment($array)       
      {      //print_r($array);die;                           
           $deviceToken = $array[0];           
           $passphrase = '1234';  
           $this->autoRender = false;
           $this->layout = false;               
           $basePath = "JobYoda.pem";                 
             //echo $basePath;            
           if(file_exists($basePath))             
           {       
           $ctx = stream_context_create(); 
           stream_context_set_option($ctx, 'ssl', 'local_cert', $basePath);
           stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
           $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,  
           $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); 
           if (!$fp)     
           exit("Failed to connect: $err $errstr" . PHP_EOL);
          $body['aps'] = array('alert' => array('title' => $array[3],"body"=>$array[1]),'sound' =>'default','badge'=>$array[5]);
          $body['body'] = $array[1];
          $body['type'] = $array[2];
          $body['story_id'] = $array[4];
          //$body['aps'] = array('alert' => array("body"=>$array[1],'type'=>$array[2],'story_id'=>$array[3]), 'badge' => 1,'sound' =>'default');
          //echo"<pre>";print_r($body['aps']);die; 
          $payload = json_encode($body); 
           $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
          $result = fwrite($fp, $msg, strlen($msg));

           if (!$result)
           {           
                    
           }     
            else  
           {    
            //echo "<pre>";print_r($body['aps']); die;   
              
           }       
             fclose($fp); 
           }    
      }

            public function distance($lat1, $lon1, $lat2, $lon2, $unit) 
            {

            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) *sin(deg2rad($lat2)) + cos(deg2rad($lat1))* cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist* 60 *1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
            return ($miles * 1.609344);
            } else if ($unit == "N") {
            return ($miles * 0.8684);
            } else {
            return $miles;
            }
            }

            public function sendSms($to, $sms_message) {
            $this->load->library('twilio');
            //$sms_sender = trim($this->input->post('sms_sender'));
            //$sms_reciever = $this->input->post('sms_recipient');
            //$sms_message = trim($this->input->post('sms_message'));
            //$from = '+'.$sms_sender; //trial account twilio number
            //$to = '+'.$sms_reciever; //sms recipient number
            $from = 'JobYoDA';
            $to = $to;
            $response = $this->twilio->sms($from, $to, $sms_message);
            //print_r($response);die;
            if ($response->ResponseXml->Message->Status[0] =="failed") {
                return '2';
            } else {
                return '1';
            }
        }

        public function SendAwsSms($to,$sms_message){
          require 'vendor/autoload.php';
          //echo 'hi';die;
        $params = array(
        'credentials'=> array(
        'key'=>'AKIA2PEGBF6OC7RXCBWM',
        'secret'=>'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',
        ),
        'region'=>'us-west-2',
        'version'=>'latest'
        );
        $sns = new \Aws\Sns\SnsClient($params);
        $args = array(
        "SenderID"=>"JobYoDA",
        "SMSType"=>"Transactional",
        "Message"=>$sms_message,
        "PhoneNumber"=>$to,
        );
        //print_r($args);die;
        $result = $sns->publish($args);
        //print_r($result);
        }

        public function testsms_post(){
          require 'vendor/autoload.php';
          //echo 'hi';die;
        $params = array(
        'credentials'=> array(
        'key'=>'AKIA2PEGBF6OC7RXCBWM',
        'secret'=>'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',
        ),
        'region'=>'us-west-2',
        'version'=>'latest'
        );
        $sns = new \Aws\Sns\SnsClient($params);
        $args = array(
        "SenderID"=>"JobYoDA",
        "SMSType"=>"Transactional",
        "Message"=>"test",
        "PhoneNumber"=>'+918218524174',
        );
        //print_r($args);die;
        $result = $sns->publish($args);
        print_r($result);
        }

}
?>
