<?php
ob_start();
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */

class Jobpost extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('old/User_Model');
        $this->load->model('old/recruiter/Jobpost_Model');
        $this->load->model('old/recruiter/Candidate_Model');
        $this->load->model('old/Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $key = $this->encryption->create_key(10);
    }
    // http://localhost/jobyodha/api/example/users/id/1
    
    public function jobLocateHome_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $jobLocates = $this->Jobpost_Model->group_locateHome($userTokenCheck[0]['id']);

            $x=0;
            foreach ($jobLocates as $jobLocate) {
                
                $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobLocate['latitude'], $jobLocate['longitude']);
                $getmaxsal = array();
                foreach($getlatlognids as $getlatlognid) {
                    $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary($getlatlognid['id']);
                    $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                }
                $gotsalmax =  max($getmaxsal);

                // $jobsal = $this->Jobpost_Model->jobfetchbymaxsalary($jobLocate['id']);

                // if($jobsal[0]['basicsalary']) {
                //     $basicsalary = $jobsal[0]['basicsalary'];
                // } else{
                //     $basicsalary = " ";
                // }
               

                $jobLocates[$x]= ["recruiter_id"=> $jobLocate['recruiter_id'], "latitude"=> $jobLocate['latitude'], "longitude"=>$jobLocate['longitude'], "totaljobs"=>$jobLocate['totaljobs'], "salary"=> $gotsalmax];
                
                $jobTop = $this->Jobpost_Model->job_toppicks($jobLocate['id']);
                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $one = (string)$jobTop[0]['picks_id'];
                        $jobLocates[$x]['toppicks1'] = "$one";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $two = (string)$jobTop[1]['picks_id'];
                        $jobLocates[$x]['toppicks2'] =  "$two";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $three = (string)$jobTop[2]['picks_id'];
                        $jobLocates[$x]['toppicks3'] = "$three";
                    }
                } else {
                    $jobLocates[$x]['toppicks1'] = "";
                    $jobLocates[$x]['toppicks2'] = "";
                    $jobLocates[$x]['toppicks3'] = "";
                }
                $x++;
            }
            
            $response = ['jobLocateHome' => $jobLocates, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $userLat = $userData['lat'];
            $userLong = $userData['long'];
            $jobfetchs = $this->Jobpost_Model->job_fetch_latlong($userLat, $userLong);
            
            if($jobfetchs) {
                $x=0;
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if($jobTop) {

                    } else{
                        $jobTop = [];
                    }
                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = " ";
                        }
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = $companyPic[0]['companyPic'];
                        } else{
                            $comPic = " ";
                        }
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $basicsalary,
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic
                            ];
                    }
                    $x++;
                }
                
                $response = ['jobListing' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else{
                $errors = "No data found";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companydetail_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $company_id = $userData['companyId'];
            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);

            if($compfetchs) {
                $jobTop = $this->Jobpost_Model->job_fetch_TopPicks($compfetchs[0]['compId']);

                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id'];    
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $datatoppicks2 =  $jobTop[1]['picks_id'];    
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $datatoppicks3 = $jobTop[2]['picks_id'];    
                    } else{
                        $datatoppicks3 = "";
                    }
                    $topickss = $jobTop;
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                    $topickss = [];
                }
                $companyPic = $this->Jobpost_Model->fetch_companyPic($compfetchs[0]['compId']);
                        
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic[0] = $companyPic[0]['companyPic'];
                } else{
                    $comPic = [];
                }
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "address" => $compfetchs[0]['address'],
                                "companyName" => $compfetchs[0]['cname'],
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compfetchs[0]['companyDesc'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "topPicks" =>$topickss
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_bycompId($company_id);
            
            if($jobfetchs) {
                $data["jobList"] = [];
                $data["jobLists"] = [];
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = " ";
                        }

                        $jobTop1 = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if($jobTop1) {
                            if(isset($jobTop1[0]['picks_id'])){
                                $datatoppicks1 = $jobTop1[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop1[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop1[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop1[2]['picks_id'])){
                                $datatoppicks3 = $jobTop1[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = $companyPic[0]['companyPic'];
                        } else{
                            $comPic = " ";
                        }
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }

                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $basicsalary,
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic
                            ];
                    }
                }

                if(count($data["jobLists"]) < 1) {
                    $data["jobLists"] = $data["jobList"];
                    if(isset($data["jobList"][0])) {
                        $data["jobList"] = $data["jobList"][0];
                    } else{
                        $data["jobList"] = $data["jobList"];
                    }
                } else{
                    $data["jobList"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];
                    $data["jobLists"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];    
                }

            } else{
                $data["jobList"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];
            }
            
            $response = ['companydetail' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function availableJobs_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $company_id = $userData['companyId'];
            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);

            if($compfetchs) {
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "companyName" => $compfetchs[0]['cname'],
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compfetchs[0]['companyDesc']
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_bycompId($company_id);
            
            if($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = " ";
                        }

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = $companyPic[0]['companyPic'];
                        } else{
                            $comPic = " ";
                        }
                        
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $basicsalary,
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic
                            ];
                    }
                }
            } else{
                $data["jobList"] =[];
            }
            $response = ['availableJobs' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobdetail_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $jobpost_id = $userData['jobpost_id'];
            $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobpost_id);
            $jobLocation = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
            $jobSaved = $this->Jobpost_Model->applied_status_fetch($jobpost_id, $userTokenCheck[0]['id']);

            if($jobSaved) {
                $savestatus = 1;
            } else{
                $savestatus = 0;
            }

            $appliedStatus = $this->Jobpost_Model->applied_status_fetch($jobDetails[0]['id'], $userTokenCheck[0]['id']);
            if($appliedStatus) {
                $jobstatus = $appliedStatus[0]['status'];
            } else{
                $jobstatus = 0;
            }

            $jobsal = $this->Jobpost_Model->getExpJob($jobDetails[0]['id']);
            if($jobsal[0]['basicsalary']) {
                $basicsalary = $jobsal[0]['basicsalary'];
            } else{
                $basicsalary = " ";
            }

            $jobTop = $this->Jobpost_Model->job_toppicks($jobDetails[0]['id']);
            $jobImg = $this->Jobpost_Model->job_images_single($jobpost_id);
           // echo $this->db->last_query();
            if($jobTop) {
                // if(isset($jobTop[0]['picks_id'])){
                //     $datatoppicks1 = $jobTop[0]['picks_id'];    
                // } else{
                //     $datatoppicks1 = "";
                // }
                // if(isset($jobTop[1]['picks_id'])){
                //     $datatoppicks2 =  $jobTop[1]['picks_id'];    
                // } else{
                //     $datatoppicks2 = "";
                // }
                // if(isset($jobTop[2]['picks_id'])){
                //     $datatoppicks3 = $jobTop[2]['picks_id'];    
                // } else{
                //     $datatoppicks3 = "";
                // }
                $toppickss = $jobTop;
            } else {
                $toppickss = [];
            }

            $arrSkills = explode(",",$jobDetails[0]['skills']);
            $arrQualify = explode(",",$jobDetails[0]['qualification']);
            $data["jobdetail"] = [
                        "jobpost_id" => $jobDetails[0]['id'],
                        "job_title" => $jobDetails[0]['jobtitle'],
                        "location" => $jobLocation[0]['address'],
                        "opening" => $jobDetails[0]['opening'],
                        "experience" => $jobDetails[0]['experience'],
                        "jobDesc" => $jobDetails[0]['jobDesc'],
                        "salary" => $basicsalary,
                        "jobexpire" => $jobDetails[0]['jobexpire'],
                        "skills" => $arrSkills,
                        "qualification" => $arrQualify,
                        "jobSaved" => $savestatus,
                        "status" => $jobstatus,
                        "topPicks" => $toppickss,
                        "images" => $jobImg
                    ];

            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobDetails[0]['company_id']);
            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['company_id']);
    
            if($jobTop) {

            } else{
                $jobTop = [];
            }
            $companyPic = $this->Jobpost_Model->fetch_companyPic($companydetail[0]["id"]);            
            if(!empty($companyPic[0]['companyPic'])) {
                $comPic = $companyPic[0]['companyPic'];
            } else{
                $comPic = " ";
            }
            $data["comapnyDetail"] = [
                                    "name"=> $companydetail[0]["cname"],
                                    "phone" => $companydetail1[0]["recruiter_contact"],
                                    "email" => $companydetail1[0]["recruiter_email"],
                                    "company_id" => $companydetail[0]["id"],
                                    "companyPic" => $comPic
                                    ];
            $response = ['jobdetail' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

     public function applyJob_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1)
        {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) 
            {
                $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');
                $this->form_validation->set_rules('interviewdate', 'Interview Date', 'trim|required');
                $this->form_validation->set_rules('interviewtime', 'Interview Time', 'trim|required');

                if ($this->form_validation->run() == FALSE) 
                {
                    $errors = $this->form_validation->error_array();
                    $error = current($errors);
                    $response = ['message' => $error, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);

                } else {

                $jobexpire = date("Y-m-d", strtotime($job['jobexpire']));
                $schedule = date("Y-m-d", strtotime($job['interviewdate']));
                
                $jobexpire1 = strtotime($jobexpire);
                $schedule1 = strtotime($schedule);
                $currentDate = strtotime(date('Y-m-d'));
                $job_detail = $this->Jobpost_Model->job_detail_fetch($job['jobpost_id']);
                $companyname = $this->Candidate_Model->fetchcomp($job_detail[0]['company_id']);
                if($job_detail[0]['level_status']=='1')
                {
                    $user_level_match = $this->User_Model->user_single_workbyuserid($userTokenCheck[0]['id'],$job_detail[0]['level']);
                    if($user_level_match){
                        $level_match = "True";
                    } else{
                        $level_match = "False";
                    }

                }
                if($job_detail[0]['education_status']=='1')
                {
                    $user_education_match = $this->User_Model->user_single_educationbyuserid($userTokenCheck[0]['id'],$job_detail[0]['education']);
                    if($user_education_match){
                        $education_match = "True";
                    } else{
                        $education_match = "False";
                    }

                }
                if($job_detail[0]['experience_status']=='1')
                {

                    $user_experience_match = $this->User_Model->user_single_exp($userTokenCheck[0]['id'],$job_detail[0]['experience']);
                    
                    if($user_experience_match){
                        $experience_match = "True";
                    } else{
                        $experience_match = "False";
                    }

                }
                if($jobexpire1 > $schedule1) 
                {
                    if($schedule1 >= $currentDate) 
                    {
                      $jobapply = [
                          "jobpost_id" => $job['jobpost_id'],
                          "user_id" => $userTokenCheck[0]['id'],
                          "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                          "interviewtime" => $job['interviewtime'],
                          "status" => 1
                      ];
                      if($level_match == "True" && $education_match == "True" && $experience_match== "True")
                      {
                        $appliedJobs = $this->Jobpost_Model->job_fetch_applied($userTokenCheck[0]['id']);
                        $this->Jobpost_Model->appliedjob_insert($jobapply);
                        $this->Recruit_Model->Notificationmessageinsert('You have got a new application from candidate');
                        $response = ['jobdetail' => $jobapply, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                      }else{
                        $jobapply1 = [
                          "jobpost_id" => $job['jobpost_id'],
                          "user_id" => $userTokenCheck[0]['id'],
                          "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                          "interviewtime" => $job['interviewtime'],
                          "status" => 3,
                          "fallout_status" => 2,
                          "fallout_reason" => "Did not meet requirement"
                        ];
                        $this->Jobpost_Model->appliedjob_insert($jobapply1);
                         $user_data = $this->User_Model->user_single($userTokenCheck[0]['id']);
                         $msg="Your Job application for "." ". $job_detail[0]['jobtitle'] ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Fall Out";
                         $data1 = ["user_id"=>$userTokenCheck[0]['id'],"jobapp_id"=>$job['jobpost_id'], "recruiter_id" =>$job_detail[0]['company_id'], "notification"=>$msg, "status_changed"=>3, "job_status"=>'0'];

                         $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                         $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                         $msg="Your Job application for "." ". $job_detail[0]['jobtitle'] ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Fall Out";
                         if($user_data[0]['notification_status']=='1')
                         {
                           if($user_data[0]['device_type']=="android"){
                                $res1 = $this->push_notification_android_status($user_data[0]['device_token'],$job_detail[0]['jobtitle'], $msg,'applied',$job_detail[0]['recruiter_id'],$companyname[0]['cname'],$notificationInserted);
                            }else if($user_data[0]['device_type']=="ios"){
                                $this->pushiphon(array($user_data[0]['device_token'],$msg,'applied',$job_detail[0]['jobtitle'], $job_detail[0]['id'],count($fetchnotifications),$companyname[0]['cname'],$notificationInserted));
                            
                            } 
                        }
                        $errors = "Update your profile first";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                      }
                      
                    
                    } else{
                        $errors = "Please select correct date";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                } else{
                    $errors = "Interview date expire on $jobexpire.";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
           } }
        else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    

    public function jobAppliedListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $jobAppliedpasts = $this->Jobpost_Model->jobappliedpast_fetch_byuserid($userTokenCheck[0]['id']);
            if($jobAppliedpasts) {

                foreach ($jobAppliedpasts as $jobAppliedpast) {
                    $jobsal = $this->Jobpost_Model->getExpJob($jobAppliedpast['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = " ";
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedpast['company_id']);
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedpast['id']);

                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id'];    
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])){
                            $datatoppicks2 =  $jobTop[1]['picks_id'];    
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $datatoppicks3 = $jobTop[2]['picks_id'];    
                        } else{
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobAppliedpast['company_id']);            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = $companyPic[0]['companyPic'];
                    } else{
                        $comPic = " ";
                    }
                    
                    $image = $this->Jobpost_Model->job_image($jobAppliedpast['id']);
                        
                        
                    if(!empty($image[0]['pic'])) {
                        $jobpic = $image[0]['pic'];
                    } else{
                        $jobpic = " ";
                    }
                    $data["jobPast"][] = [
                            "jobpost_id" => $jobAppliedpast['id'],
                            "job_title" => $jobAppliedpast['jobtitle'],
                            "jobDesc" => $jobAppliedpast['jobDesc'],
                            "salary" => $basicsalary,
                            "status" => $jobAppliedpast['status'],
                            "companyName"=> $companydetail[0]["cname"],
                            "toppicks1" => "$datatoppicks1",
                            "toppicks2" => "$datatoppicks2",
                            "toppicks3" => "$datatoppicks3",
                            "companyPic" => $comPic,
                            "jobpic" => $jobpic
                        ];
                }
            } else {
                $data["jobPast"][] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "status" => "",
                            "companyName"=> "",
                            "companyPic" => ""
                                ];
            }

            $jobAppliedupcomings = $this->Jobpost_Model->jobappliedupcoming_fetch_byuserid($userTokenCheck[0]['id']);
            if($jobAppliedupcomings) {

                foreach ($jobAppliedupcomings as $jobAppliedupcoming) {
                    $jobsal1 = $this->Jobpost_Model->getExpJob($jobAppliedupcoming['id']);
                    if($jobsal1[0]['basicsalary']) {
                        $basicsalary1 = $jobsal1[0]['basicsalary'];
                    } else{
                        $basicsalary1 = " ";
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedupcoming['company_id']);
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedupcoming['id']);

                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id'];    
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])){
                            $datatoppicks2 =  $jobTop[1]['picks_id'];    
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $datatoppicks3 = $jobTop[2]['picks_id'];    
                        } else{
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobAppliedupcoming['company_id']);            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = $companyPic[0]['companyPic'];
                    } else{
                        $comPic = " ";
                    }
                    
                    $image = $this->Jobpost_Model->job_image($jobAppliedupcoming['id']);
                        
                        
                    if(!empty($image[0]['pic'])) {
                        $jobpic = $image[0]['pic'];
                    } else{
                        $jobpic = " ";
                    }
                    $data["jobUpcoming"][] = [
                            "jobpost_id" => $jobAppliedupcoming['id'],
                            "job_title" => $jobAppliedupcoming['jobtitle'],
                            "jobDesc" => $jobAppliedupcoming['jobDesc'],
                            "salary" => $basicsalary1,
                            "status" => $jobAppliedupcoming['status'],
                            "companyName"=> $companydetail[0]["cname"],
                            "toppicks1" => "$datatoppicks1",
                            "toppicks2" => "$datatoppicks2",
                            "toppicks3" => "$datatoppicks3",
                            "companyPic" => $comPic,
                            "jobpic" => $jobpic
                        ];
                }
            } else {
                $data["jobUpcoming"][] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "status" => "",
                            "companyName"=> "",
                            "companyPic" => ""
                ];
            }

            $response = ['jobAppliedListing' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function saveJob_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {

                $jobsave = [
                            "jobpost_id" => $job['jobpost_id'],
                            "user_id" => $userTokenCheck[0]['id'],
                        ];
                $this->Jobpost_Model->savejob_insert($jobsave);
                $response = ['saveJob' => $jobsave, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function saveJobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $jobSaveds = $this->Jobpost_Model->jobSaved_fetch_byuserid($userTokenCheck[0]['id']);
            if($jobSaveds) {

                foreach ($jobSaveds as $jobSaved) {
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobSaved['company_id']);
                    $appliedStatus = $this->Jobpost_Model->applied_status_fetch($jobSaved['id'], $userTokenCheck[0]['id']);
                    
                    if($appliedStatus) {
                        $jobstatus = $appliedStatus[0]['status'];
                    } else{
                        $jobstatus = 0;
                    }
                    $data["jobList"][] = [
                            "jobpost_id" => $jobSaved['id'],
                            "job_title" => $jobSaved['jobtitle'],
                            "jobDesc" => $jobSaved['jobDesc'],
                            "salary" => $jobSaved['salary'],
                            "status" => $jobstatus,
                            "companyName"=> $companydetail[0]["cname"]
                        ];
                }
            } else {
                $data["jobList"][] = [];
            }

            $response = ['saveJobListing' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function filters_post() {
        $filterData = $this->input->post();
        $data = ["token" => $filterData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        $data["jobList"] = [];
        if($userTokenCheck) {

            $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id']);
            //echo $this->db->last_query();exit();
           // print_r($jobfetchs);die;
            if($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {

                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    
                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $one = (string)$jobTop[0]['picks_id'];
                            $dataJobList[0] = "$one";
                        } else{
                            $dataJobList[0] = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $two = (string)$jobTop[1]['picks_id'];
                            $dataJobList[1] = "$two";
                        } else {
                            $dataJobList[1] = "";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $three = (string)$jobTop[2]['picks_id'];
                            $dataJobList[2] = "$three";
                        } else{
                            $dataJobList[2] = "";
                        }
                    } else {
                        $dataJobList[0] = "";
                        $dataJobList[1] = "";
                        $dataJobList[2] = "";
                    }
                    //print_r($jobfetch);die;
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                    //echo $this->db->last_query();die;
                    $sal = $this->Jobpost_Model->fetch_highsalary($jobfetch['id']);
                     $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }    
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = $companyPic[0]['companyPic'];
                    } else{
                        $comPic = " ";
                    }
                    $data["jobList"][] = [
                            "jobpost_id" => $jobfetch['id'],
                            "job_title" => $jobfetch['jobtitle'],
                            "jobDesc" => $jobfetch['jobDesc'],
                            "salary" => $sal[0]['salary'],
                            "address" => $jobfetch['address'],
                            "companyName" => $jobfetch['cname'],
                            "latitude" => $jobfetch['latitude'],
                            "longitude" => $jobfetch['longitude'],
                            "jobcount" => 1,
                            "toppicks1" => $dataJobList[0],
                            "toppicks2" => $dataJobList[1],
                            "toppicks3" => $dataJobList[2],
                            "companyPic" => $comPic,
                            "jobpic" => $jobpic
                        ];
                }
                
                if(count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }

                $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id']);
                //print_r($jobcountfetchs);die;
                if($jobcountfetchs) {
                    foreach ($jobcountfetchs as $jobcountfetch) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }

                        $sal = $this->Jobpost_Model->fetch_highsalary($jobcountfetch['id']);

                        $data["jobcount"][] = [
                                "company_id" => $jobcountfetch['company_id'],
                                "salary" => $sal[0]['salary'],
                                "latitude" => $jobcountfetch['latitude'],
                                "longitude" => $jobcountfetch['longitude'],
                                "address" => $jobcountfetch['address'],
                                "jobcount" => $jobcountfetch['jobcount'],
                                "toppicks1" => $dataJobList[0],
                                "toppicks2" => $dataJobList[1],
                                "toppicks3" => $dataJobList[2]
                            ];
                }
                        
                } else{
                    $data['jobcount'] = [];
                }
            
                $response = ['filters' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else{
                $errors = "No Data Found";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobFilters_post() {
        $filterData = $this->input->post();
        //print_r($filterData);exit();
        $data = ["token" => $filterData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        //print_r($userTokenCheck);exit();
        $data["jobList"] = [];
        $jobListing = [];

        if($userTokenCheck) {
            $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id']);
            //echo $this->db->last_query();exit();
            //print_r($jobfetchs);exit();
            if($jobfetchs) {
                $x=1;
                foreach ($jobfetchs as $jobfetch) {
                        
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else{
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        if($jobfetch['id']){
                            $jobfetch['id']=$jobfetch['id'];
                        }else{
                            $jobfetch['id']="";
                        }
                        if($jobfetch['jobtitle']){
                            $jobfetch['jobtitle']=$jobfetch['jobtitle'];
                        }else{
                            $jobfetch['jobtitle']="";
                        }
                        if($jobfetch['jobDesc']){
                            $jobfetch['jobDesc']=$jobfetch['jobDesc'];
                        }else{
                            $jobfetch['jobDesc']="";
                        }
                        
                        if($jobfetch['cname']){
                            $jobfetch['cname']=$jobfetch['cname'];
                        }else{
                            $jobfetch['cname']="";
                        }
                        if($jobfetch['latitude']){
                            $jobfetch['latitude']=$jobfetch['latitude'];
                        }else{
                            $jobfetch['latitude']="";
                        }
                        if($jobfetch['longitude']){
                            $jobfetch['longitude']=$jobfetch['longitude'];
                        }else{
                            $jobfetch['longitude']="";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = $companyPic[0]['companyPic'];
                        } else{
                            $comPic = " ";
                        }
                        $sal = $this->Jobpost_Model->fetch_highsalary($jobfetch['id']);
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                            if(!empty($image[0]['pic'])) {
                                $jobpic = $image[0]['pic'];
                            } else{
                                $jobpic = " ";
                            }
                        $jobListing[] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $sal[0]['salary'],
                                "companyName" => $jobfetch['cname'],
                                "latitude" => $jobfetch['latitude'],
                                "longitude" => $jobfetch['longitude'],
                                "address" => $jobfetch['address'],
                                "jobcount" =>1,
                                "toppicks1" => $dataJobList[0],
                                "toppicks2" => $dataJobList[1],
                                "toppicks3" => $dataJobList[2],
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic
                            ];
                    $x++;
                }
                if(count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else{
                $data["jobList"] = [];
            }
            
            //print_r($jobListing);exit();
            if($filterData['salarySort'] == 1) {
                        
                foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
                }
                array_multisort($price, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            }
             /*else if($filterData['locationSort'] == 1) {
                foreach ($jobListing as $key => $row) {
                    $locate[$key] = $row['address'];
                }
                array_multisort($locate, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            } */
            else{
                $data["jobList"] = $jobListing;
            }


            $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id']);
            //print_r($jobcountfetchs);exit();
            if($jobcountfetchs && $jobcountfetchs[0]['jobcount'] != 0) {
                
                foreach ($jobcountfetchs as $jobcountfetch) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }

                        $sal = $this->Jobpost_Model->fetch_highsalary($jobcountfetch['id']);

                        $jobcount[] = [
                                "company_id" => $jobcountfetch['company_id'],
                                "salary" => $sal[0]['salary'],
                                "latitude" => $jobcountfetch['latitude'],
                                "longitude" => $jobcountfetch['longitude'],
                                "jobcount" => $jobcountfetch['jobcount'],
                                "toppicks1" => $dataJobList[0],
                                "toppicks2" => $dataJobList[1],
                                "toppicks3" => $dataJobList[2]
                            ];
                }  

                if($filterData['salarySort'] == 1) {
                    
                    $price = array();
        
                    foreach ($jobcount as $key => $row) {
                        $price[$key] = $row['salary'];
                    }
                   
                    array_multisort($price, SORT_DESC, $jobcount);
                    $data["jobcount"] = $jobListing;

                }else{
                    $data["jobcount"] = $jobListing;
                }
                
            } else{
                $data['jobcount'] = [];
            }
            
            $response = ['jobFilters' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobtitlecache_post() {
        $cacheData = $this->input->post();
        $data = ["token" => $cacheData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {
            
            if(!empty($cacheData['title'])) {
              $cachecheck = $this->Jobpost_Model->jobtitlecache_check($cacheData['title'], $userTokenCheck[0]['id']);
              
              if(!$cachecheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'], "title"=> $cacheData['title']];
                  $this->Jobpost_Model->jobtitlecache_insert($data);
              }
            }
            $cacheGet = $this->Jobpost_Model->jobtitlecache_fetch($userTokenCheck[0]['id']);
            if($cacheGet) {

            } else{
              $cacheGet = [];
            }
            $response = ['jobtitlecache' => $cacheGet, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function keywordcache_post() {
        $cacheData = $this->input->post();
        $data = ["token" => $cacheData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {
            
            if(!empty($cacheData['keyword'])) {
              $cachecheck = $this->Jobpost_Model->keywordcache_check($cacheData['keyword'], $userTokenCheck[0]['id']);
              
              if(!$cachecheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'], "keyword"=> $cacheData['keyword']];
                  $this->Jobpost_Model->keywordcache_insert($data);
              }
            }
            $cacheGet = $this->Jobpost_Model->keywordcachee_fetch($userTokenCheck[0]['id']);
            if($cacheGet) {

            } else{
              $cacheGet = [];
            }
            $response = ['keywordcache' => $cacheGet, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companySchedule_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {  
            $getDays = $this->Jobpost_Model->getCompDetails($jobData['companyId']);
            
            $days1 = $getDays[0]['dayfrom'];
            $days2 = $getDays[0]['dayto'];
            $z=1;

            for($i=$days1; $i<=$days2; $i++) {
                if($i == 1) {
                    $dayname = "Monday";
                } else if($i == 2) {
                    $dayname = "Tuesday";
                } else if($i == 3) {
                    $dayname = "Wednesday";
                } else if($i == 4) {
                    $dayname = "Thursday";
                } else if($i == 5) {
                    $dayname = "Friday";
                } else if($i == 6) {
                    $dayname = "Saturday";
                } else if($i == 7) {
                    $dayname = "Sunday";
                }
                $data['days'][] = $dayname;
              $z++;
            }

            $time1 = $getDays[0]['from_time'];
            $time11 = explode(":", $time1);
            $time111 = $time11[0];
            $time2 = $getDays[0]['to_time'];
            $time22 = explode(":", $time2);
            $time222 = $time22[0];

            $data['timeFrom'] = $time111;
            $data['timeTo'] = $time222;

            //var_dump($data);die;
            
            $response = ['companySchedule' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobtitlelist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {  
            $jobtitle = $this->Common_Model->getCompJobTitle();
            $response = ['jobtitlelist' => $jobtitle, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

}
?>
