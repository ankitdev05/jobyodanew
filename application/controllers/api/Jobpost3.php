<?php
ob_start();
ini_set('display_errors', 1);
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
define('API_ACCESS_KEY','AIzaSyCTzjJxETlJxp18hCwYHLFETLZRkbGFiGw');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */

class Jobpost3 extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('old/User_Model');
        $this->load->model('old/recruiter/Jobpost_Model');
        $this->load->model('old/recruiter/Candidate_Model');
        $this->load->model('old/recruiter/Recruit_Model');
        $this->load->model('old/recruiter/Newpoints_Model');
        $this->load->model('old/Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $key = $this->encryption->create_key(10);
    }


    public function homedata_post() {
        $ad_list = array();
        $companylist = array();

        $userData = $this->input->post();

        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        
        if($userTokenCheck1) {
            
            // Section 1
            $ad_list = $this->Jobpost_Model->advertise_lists($userData['cur_lat'],$userData['cur_long']);
            if(!empty($ad_list)) {
                $x=0;
                foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],             
                          "companyName" =>   $ad_lists['cname']             

                    ];
                    $x++;
                }
            } else {
                $ad_lists1 = [];
            }

            //section 2
            $company_lists = $this->Newpoints_Model->companysite_lists();
            var_dump($company_lists);die;

            if(!empty($company_lists)) {
                $x=0;
                foreach ($company_lists as $company_list) {
                    $jobcount = $this->Newpoints_Model->companysite_jobcount($company_lists['id']);

                    $companylist[$x] = [
                          "id" => $company_lists['id'],
                          "image" => $company_lists['companyPic'],
                          "cname" => $company_lists['cname'],
                          "jobcount" => $jobcount
                    ];
                    $x++;
                }
            } else {
                $companylist = [];
            }

            $response = ['status'=>"SUCCESS", 'section1'=>$ad_lists1, 'section2'=>$companylist];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }
    
    public function jobLocateHome_post() {
    
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);
    
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);

            if($userTokenCheck) {

                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                    
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                    
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                    
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                    
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                    
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                    
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                    
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                    
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                    
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                    
                }

                $jobLocates = $this->Jobpost_Model->group_locateHome($userTokenCheck[0]['id'],$expfilter);
                    
                $returnnewarray = array();
                $latlogcheck =array();

                foreach ($jobLocates as $value123) {
                
                    if(in_array($value123['latitude'],$latlogcheck)){
                        $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['totaljobs'] + 1;
                    } else {
                        $latlogcheck[] = $value123['latitude'];
                        $returnnewarray[] = $value123;
                  }
                }

                     
                $x=0;
                foreach ($returnnewarray as $jobLocate) {
                                  
                    $salary_f = $jobLocate['salary']+ $jobLocate['allowance'];
                    $returnnewarray[$x]= ["recruiter_id"=> $jobLocate['recruiter_id'], "latitude"=> $jobLocate['latitude'], "longitude"=>$jobLocate['longitude'], "totaljobs"=>(string)$jobLocate['totaljobs'], "salary"=> (int)$jobLocate['salary']];
                    
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobLocate['id']);
                    
                    if($jobTop) {
                    
                        if(isset($jobTop[0]['picks_id'])) {
                            $one = (string)$jobTop[0]['picks_id'];
                            $returnnewarray[$x]['toppicks1'] = "$one";
                        }
                    
                        if(isset($jobTop[1]['picks_id'])) {
                            $two = (string)$jobTop[1]['picks_id'];
                            $returnnewarray[$x]['toppicks2'] =  "$two";
                        }
                    
                        if(isset($jobTop[2]['picks_id'])) {
                            $three = (string)$jobTop[2]['picks_id'];
                            $returnnewarray[$x]['toppicks3'] = "$three";
                        }
                    } else {
                        $returnnewarray[$x]['toppicks1'] = "";
                        $returnnewarray[$x]['toppicks2'] = "";
                        $returnnewarray[$x]['toppicks3'] = "";
                    }
                    $x++;
                }

                $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['id']);  

                if($userTokenCheck[0]['exp_month']>=0   && $userTokenCheck[0]['exp_year']>=0) {
                    $exp_added = "No";
                } else {
                    $exp_added = 'Yes';
                }
                $completeness = $getCompletenes['comp'];
                
                if($completeness<=14 || $exp_added== 'Yes') {
                    $completeness = "Yes";
                } else {
                    $completeness = "No";
                }

                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                $response = ['jobLocateHome' => $returnnewarray,'completeness'=>$completeness, 'notification_count' => count($fetchnotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {

            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            
            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }


            $userLat = $userData['lat'];
            $userLong = $userData['long'];
            $jobpost_id = $userData['jobpost_id'];



            $jobfetchs = $this->Jobpost_Model->job_fetch_latlong($userLat, $userLong, $jobpost_id,$expfilter);
            //echo $this->db->last_query();die;
            // for($ikj = array_search($expfilter,$temparray);$ikj > -1;$ikj--){
            //         $expfilter = $temparray[$ikj];
                    
            //         //echo $this->db->last_query();
            //         if(!empty($jobfetchs)){
            //             break;
            //         }
            //      }

              
            
            if($jobfetchs) {
                $x=0;
                foreach ($jobfetchs as $jobfetch) {
                    //print_r($jobfetch);die;
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if($jobTop) {

                    } else{
                        $jobTop = [];
                    }
                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob1($jobfetch['id'],$expfilter);
                       
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                        $savedjob = $this->Jobpost_Model->job_saved($jobfetch['id'], $userTokenCheck[0]['id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = $comPic;
                        }
                        
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'], $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $jobfetch['salary'],
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "cname" => $cname,
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic,
                                "savedjob" => $savedjob,
                                "distance" => $distance 
                            ];
                    }
                    $x++;
                }
                
                $response = ['jobListing' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else{
                $errors = "No data found";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

}
?>
