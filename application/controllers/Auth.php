<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittall
 * @license         Mobulous
 */
class Auth extends CI_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Newpoints_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('google');
        $this->load->library('Facebook');
        
    }

    public function login() {

        $this->load->view('new_design_login');
    }

    public function registerstepone() {
        
        $this->load->view('new_design_register_one');
    }

    public function registersteptwo() {
        
        $this->load->view('new_design_register_two');
    }

    public function verifyotp() {
        
        $this->load->view('new_design_otp');
    }

    public function forgotpassword() {
        
        $this->load->view('new_design_forogt_password');
    }

    public function setpassword() {
        
        $this->load->view('new_design_set_password');
    }

    public function successreset() {
        
        $this->load->view('new_design_success_reset');
    }

    public function successsignup() {
        
        $this->load->view('new_design_success_signup');
    }


}
?>