<?php
ob_start();
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class User extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->model('Common_Model');
         $this->load->model('recruiter/Jobpost_Model');
         $this->load->model('recruiter/Candidate_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $key = $this->encryption->create_key(10);
    }
    // http://localhost/jobyodha/api/example/users/id/1
    
    public function signup_post()
    {
        $userData = $this->input->post();
        
        if($userData['signupType'] == "normal") {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[user.email]');
            $this->form_validation->set_rules('country_code', 'country_code', 'trim|required');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('password', 'password', 'trim|required');
            $this->form_validation->set_rules('confirmPassword', 'confirm password', 'trim|required|matches[password]');
            $this->form_validation->set_rules('device_type', 'device type', 'trim|required');
            $this->form_validation->set_rules('device_token', 'device token', 'trim|required');

            $this->form_validation->set_rules('location', 'Location', 'trim|required');
            $this->form_validation->set_rules('exp_month', 'Exp Month', 'trim|required');
            $this->form_validation->set_rules('exp_year', 'Exp Year', 'trim|required');
            $this->form_validation->set_rules('education', 'Education', 'trim|required');
            $this->form_validation->set_rules('superpower', 'superpower', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {
                $hash = $this->encryption->encrypt($userData['password']);
                
                $token = $this->User_Model->randomstring();
                $data = [
                    'name' => ucwords(strtolower($userData['name'])),
                    'email' => $userData['email'],
                    'country_code' => $userData['country_code'],
                    'phone' => $userData['phone'],
                    'password' => $hash,
                    'type' => "normal",
                    'token' => $token,
                    'status' => 1,
                    'device_type' => $userData['device_type'],
                    'device_token' => $userData['device_token'],
                    'referral_used' => $userData['referral_code'],
                    'exp_month' => $userData['exp_month'],
                    'exp_year' => $userData['exp_year'],
                    'education' => $userData['education'],
                    'superpower' => $userData['superpower'],
                    'location' => $userData['location'],
                    'nationality' => $userData['nationality']
                ];
                
                $userInserted = $this->User_Model->user_insert($data);
                
                if($userInserted) {
                    $tokenData = ['user_id'=> $userInserted,
                                'token' => $token,
                                'device_type' => $userData['device_type'],
                                'device_token' => $userData['device_token'],
                    ];
                    $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                    
                    //$getData = $this->fetchUserSingleData($userInserted);

                    $getCompleteness = $this->User_Model->check_profileComplete($userInserted);
                    if($getCompleteness) {
                        if($getCompleteness[0]['signup'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["signup" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userInserted);
                        }
                    } else{
                        $profileComplete = 10;
                        $comProfile = ["user_id"=>$userInserted, "signup" => $profileComplete];
                        $this->User_Model->insert_profileComplete($comProfile);
                    }

                    //$getData = $this->fetchUserSingleData($userInserted);

                    $getData = $this->fetchUserSingleDataByToken($userInserted, $userData['device_token'], $userData['device_type']);

                    $getCompletenes = $this->fetchUserCompleteData($userInserted);

                    $getData['completeness'] = $getCompletenes['comp'];
                    $rating = $this->calculateRating($userInserted);
                    $response = ['signup' => $getData, 'rating'=>$rating, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else{
                    $errors = "Data not inserted";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
        } else {
            $token = $this->User_Model->randomstring();

            //$socialCheck = $this->User_Model->user_social($userData['email'], $userData['signupType']);

            $socialCheck = $this->User_Model->user_socialagain($userData['socialToken'], $userData['signupType']);
            
            if(!$socialCheck) {
                $data = [
                    'name' => ucwords(strtolower($userData['name'])),
                    'email' => '',
                    'type' => $userData['signupType'],
                    'token' => $token,
                    'socialToken' => $userData['socialToken'],
                    'profilePic' => $userData['profilePic'],
                    'status' => 0,
                    'device_type' => $userData['device_type'],
                    'device_token' => $userData['device_token']
                ];
                $userInserted = $this->User_Model->user_insert($data);
                
                if($userInserted) {
                    $tokenData = ['user_id'=> $userInserted,
                                'token' => $token,
                                'device_type' => $userData['device_type'],
                                'device_token' => $userData['device_token'],
                    ];

                    $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                    
                    //$getData = $this->fetchUserSingleData($userInserted);
                    $getData = $this->fetchUserSingleDataByToken($userInserted, $userData['device_token'], $userData['device_type']);
                    
                    $rating = $this->calculateRating($userInserted);
                    $response = ['signup' => $getData, 'rating' => $rating, 'status' => "ALREADY"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else{
                    $errors = "Data not inserted";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } else {
                if($socialCheck[0]['status'] == 0) {

                    $getData = $this->fetchUserSingleData($socialCheck[0]['id']);

                    $dataCheck = ['user_id'=>$socialCheck[0]['id'],'device_token'=>$userData['device_token'],'device_type'=>$userData['device_type']];
                    $checkToken = $this->User_Model->user_token_check($dataCheck);

                    if($checkToken) {
                        $data = ["token" => $token];
                        $this->User_Model->update_tokenagain($data, $socialCheck[0]['id'], $userData['device_token']);
                    } else {
                        $tokenData = ['user_id'=> $socialCheck[0]['id'],
                                'token' => $token,
                                'device_type' => $userData['device_type'],
                                'device_token' => $userData['device_token'],
                            ];
                        $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                    }
                    
                    //$getData = $this->fetchUserSingleData($socialCheck[0]['id']);
                    $getData = $this->fetchUserSingleDataByToken($socialCheck[0]['id'], $userData['device_token'], $userData['device_type']);
                    $response = ['signup' => $getData, 'status' => "ALREADY"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);

                } else {
                    $dataCheck = ['user_id'=>$socialCheck[0]['id'],'device_token'=>$userData['device_token'],'device_type'=>$userData['device_type']];
                    $checkToken = $this->User_Model->user_token_check($dataCheck);

                    if($checkToken) {
                        $data = ["token" => $token];
                        $this->User_Model->update_tokenagain($data, $socialCheck[0]['id'], $userData['device_token']);
                    } else {
                        $tokenData = ['user_id'=> $socialCheck[0]['id'],
                                'token' => $token,
                                'device_type' => $userData['device_type'],
                                'device_token' => $userData['device_token'],
                            ];
                        $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                    }

	       			// $tokenData = ['user_id'=> $socialCheck[0]['id'],
	          //           'token' => $getData['token'],
	          //           'device_type' => $userData['device_type'],
	          //           'device_token' => $userData['device_token'],
	          //       ];
	          //       $tokenInserted = $this->User_Model->usertoken_insert($tokenData);

                    //$getData = $this->fetchUserSingleData($socialCheck[0]['id']);
                    $getData = $this->fetchUserSingleDataByToken($socialCheck[0]['id'], $userData['device_token'], $userData['device_type']);
                    $rating = $this->calculateRating($socialCheck[0]['id']);
                    $response = ['signup' => $getData, 'rating' => $rating , 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
        }
    }

    public function socialSignup_post() {
        $userData = $this->input->post();
        $data = [
                'name' => ucwords(strtolower($userData['name'])),
                'socialToken' => $userData['socialToken'],
                'type' => $userData['signupType']
        ];

        $token = $this->User_Model->randomstring();
        $socialCheck = $this->User_Model->user_social($data['socialToken'], $data['type']);
        
        //print_r($socialCheck);die;
        
        if($socialCheck) {
            
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('country_code', 'country_code', 'trim|required');
            //$this->form_validation->set_rules('password', 'password', 'trim|required');
            //$this->form_validation->set_rules('confirmPassword', 'confirm password', 'trim|required|matches[password]');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            
            } else {
                //$hash = $this->encryption->encrypt($userData['password']);
                
                $hash = "12weqw";
                $moreData = ["name"=> $userData['name'], "email"=> $userData['email'],  "country_code"=>$userData['country_code'], "phone"=> $userData['phone'], "password"=> $hash, "status" => 1, 'device_type' => $userData['device_type'],'device_token' => $userData['device_token'], 'referral_used' => $userData['referral_code']];
                

                //$tokenInserted = $this->User_Model->usertoken_insert($tokenData);

                //$getData = $this->fetchUserSingleData($socialCheck[0]['id']);
                
                $getCompleteness = $this->User_Model->check_profileComplete($socialCheck[0]['id']);
                if($getCompleteness) {
                    if($getCompleteness[0]['signup'] == 0) {
                        $profileComplete = 10;
                        $comProfile = ["signup" => $profileComplete];
                        $this->User_Model->update_profileComplete($comProfile, $socialCheck[0]['id']);
                    }
                } else{
                    $profileComplete = 10;
                    $comProfile = ["user_id"=>$socialCheck[0]['id'], "signup" => $profileComplete];
                    $this->User_Model->insert_profileComplete($comProfile);
                }

                $getData = $this->fetchUserSingleData($socialCheck[0]['id']);
                $getCompletenes = $this->fetchUserCompleteData($socialCheck[0]['id']);
                $rating = $this->calculateRating($socialCheck[0]['id']);
                $getData['completeness'] = $getCompletenes['comp'];
                // $getData['token'] = $token;
				$userUpdated = $this->User_Model->user_update($moreData, $socialCheck[0]['id']);
                
                // $tokenData = ['user_id'=> $socialCheck[0]['id'],
                //     'token' => $getData['token'],
                //     'device_type' => $userData['device_type'],
                //     'device_token' => $userData['device_token'],
                // ];

                $response = ['socialSignup' => $getData, 'rating' => $rating , 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
        }
    }

    public function login_post()
    {
        $userData = $this->input->post();
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('device_token', 'device token', 'trim|required');
        $this->form_validation->set_rules('device_type', 'device type', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $password = $userData['password'];
            $data = ["email" => $userData['email']];
            $userCheck = $this->User_Model->user_login($data);
            //echo $this->db->last_query();die;
            
            if($userCheck) {
                $getPass = $userCheck[0]['password'];
                $vPass = $this->encryption->decrypt($getPass);
                
                if($password == $vPass) {
                    
                    if($userCheck[0]['active'] == 1) {
                        //$getDeviceToken = $this->User_Model->user_single($userCheck[0]['id']);
                        /*if(!empty($getDeviceToken[0]['device_token'])){*/
                    
                           // $datadevicetoken = ["device_token"=>$userData['device_token'], "device_type"=>$userData['device_type']];
                           //  $this->User_Model->update_token($datadevicetoken, $userCheck[0]['id']); 

                        /*}*/
                        //print_r($getDeviceToken);die;
                        $getCompleteness = $this->User_Model->check_profileComplete($userCheck[0]['id']);
                        if($getCompleteness) {
                            if($getCompleteness[0]['signup'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["signup" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userCheck[0]['id']);
                            }
                        } else{
                            $profileComplete = 10;
                            $comProfile = ["user_id"=>$userCheck[0]['id'], "signup" => $profileComplete];
                            $this->User_Model->insert_profileComplete($comProfile);
                        }

                        $token = $this->User_Model->randomstring();
                        $tokenData = ['user_id'=> $userCheck[0]['id'],
                            'token' => $token,
                            'device_type' => $userData['device_type'],
                            'device_token' => $userData['device_token'],
                        ];

                        $tokens = ["device_token" => $userData['device_token'], "user_id" => $userCheck[0]['id']];                        
                        $userTokenCheck1 = $this->User_Model->token_match1($tokens);
                        if(empty($userTokenCheck1)) {
                            
                            $this->User_Model->usertoken_delete($userCheck[0]['id'], $tokenData['device_token']);

                            $tokenInserted = $this->User_Model->usertoken_insert($tokenData);
                        } else {
                            $tokenData = ['token' => $token];
                            $tokenUpdated = $this->User_Model->update_usertoken($tokenData, $userData['device_token'], $userCheck[0]['id']);
                        }
                        
                        //$getData = $this->fetchUserSingleData($userCheck[0]['id']);
                        $getData = $this->fetchUserSingleDataByToken($userCheck[0]['id'], $userData['device_token'], $userData['device_type']);

                        $getCompletenes = $this->fetchUserCompleteData($userCheck[0]['id']);
                        $getData['completeness'] = $getCompletenes['comp'];
                        $rating = $this->calculateRating($userCheck[0]['id']);
                        $response = ['login' => $getData, 'rating' => $rating , 'status' => "SUCCESS"];
                    } else{
                        $response = ['message' => "User is blocked", 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                } else {
                    $response = ['message' => "Password is incorrect", 'status' => "FAILURE"];
                }
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else{
                $response = ['message' => "Email is not yet registered", 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function changePassword_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('oldPassword', 'oldPassword', 'trim|required');
        $this->form_validation->set_rules('newPassword', 'newPassword', 'trim|required');
        $this->form_validation->set_rules('confirmPassword', 'confirmPassword', 'trim|required|matches[newPassword]');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $userPass = $this->encryption->decrypt($userTokenCheck[0]['password']);
                
                if($userPass == $userData['oldPassword']) {
                    $hash = $this->encryption->encrypt($userData['newPassword']);
                    $data = ["password" => $hash];
                    $this->User_Model->password_update($data, $userData['token']);

                    $success = "Password changed successfully";
                    $response = ['changePassword' => $success, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else {
                    $errors = "Old Password does not match";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
            }
        }
    }

    public function phoneVerify_post() {
        $userData = $this->input->post();   
        $this->form_validation->set_rules('phone', 'phone', 'trim|required|is_unique[user.phone]');
        
        if(isset($userData['email']) && !empty($userData['email'])){
            $this->form_validation->set_rules('email', 'email', 'trim|required');
        }
        
        if(!empty($userData['phone'])){
            $phoneMatch = $this->User_Model->checkphone($userData['phone']);
	        
            if(count($phoneMatch) > 0){
	         		
                if($phoneMatch[0]['type']=='normal') {
	                $type='You have already registered this phone number using your email. Please login with your email address.';
	            }else if($phoneMatch[0]['type']=='gmail'){
	                $type = "You have already registered this Phone # with your Google ID.Please login with your Google ID";
	            }else if($phoneMatch[0]['type']=='facebook'){
	                $type = "You have already registered this Phone # with your Facebook ID.Please login with your Facebook ID";
	            }else if($phoneMatch[0]['type']=='google'){
	                $type = "You have already registered this Phone # with your Google ID.Please login with your Google ID";
	            }
	        }
        }
        
        if(!empty($userData['referral_code'])){
               $refer_match = $this->User_Model->referral_code($userData['referral_code']);
               if(empty($refer_match)){
                $errors = "This referral code does not exist";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
               }else{
                if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                //var_dump($errors);die;
                
                if(isset($errors['email'])) {
                    $response = ['message' => "Email already exists", 'status' => "FAILURE"];    
                }else if($errors['phone']){
                    $response = ['message' => $type, 'status' => "FAILURE"];
                }
                
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            
            } else {
                $response = ['message' => "Phone number does not exists", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } 
               }
        }else{
            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                //var_dump($errors);die;
                
                if(isset($errors['email'])) {
                    $response = ['message' => "Email already exists", 'status' => "FAILURE"];    
                } else if($errors['phone']) {
                    if(strlen($type) > 0) {
                        $response = ['message' => $type, 'status' => "FAILURE"];
                    } else {
                        $response = ['message' => "Phone number does not exists", 'status' => "SUCCESS"];
                    }
                }
                
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            
            } else {
                $response = ['message' => "Phone number does not exists", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } 
        } 
       
    }
    
    public function industryList_get() {
        $data = $this->Common_Model->industry_lists();
        $response = ['industryList' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function channelList_get() {
        $data = $this->Common_Model->channel_lists();
        $response = ['channelList' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function languageList_get() {
        $data = $this->Common_Model->language_lists();
        $response = ['languageList' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function workExperience_post() {
        $workData = $this->input->post();
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('company', 'company', 'trim|required');
        $this->form_validation->set_rules('desc', 'desc', 'trim|required');
        $this->form_validation->set_rules('from', 'from', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $workData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $jobFrom = $workData['from'];
                $jobTo = $workData['to'];

                $data = ["user_id"=>$userTokenCheck[0]['id'], "title"=>$workData['title'], "company"=>$workData['company'], "jobDesc"=>$workData['desc'], "workFrom"=>$jobFrom, "workTo"=>$jobTo,"currently_working"=>$workData['currently_working'],"level"=>$workData['level'], "category"=>$workData['category'], "subcategory"=>$workData['subcategory']];

                if(empty($workData['id'])) {

                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if($getCompleteness) {
                        if($getCompleteness[0]['experience'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["experience" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                    $workInserted = $this->User_Model->work_insert($data);
                } else {
                    $workInserted = $this->User_Model->work_update($data, $workData['id']);
                }

                if($workInserted) {
                    $userId = $userTokenCheck[0]['id'];
                    $workData = $this->fetchWorkSingleData($userId);
                    $response = ['workExperience' => $workData, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else{
                    $errors = "Data not inserted";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } }else{
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function education_post() {
        $eduData = $this->input->post();
        $this->form_validation->set_rules('attainment', 'title', 'trim|required');
        $this->form_validation->set_rules('degree', 'company', 'trim|required');
        $this->form_validation->set_rules('university', 'desc', 'trim|required');
        $this->form_validation->set_rules('from', 'from', 'trim|required');
        $this->form_validation->set_rules('to', 'to', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $eduData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $jobFrom = $eduData['from'];
                $jobTo = $eduData['to'];

                $data = ["user_id"=>$userTokenCheck[0]['id'], "attainment"=>$eduData['attainment'], "degree"=>$eduData['degree'], "university"=>$eduData['university'], "degreeFrom"=>$jobFrom, "degreeTo"=>$jobTo];

                if(empty($eduData['id'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if($getCompleteness) {
                        if($getCompleteness[0]['education'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["education" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                    $eduInserted = $this->User_Model->edu_insert($data);
                } else {
                    $eduInserted = $this->User_Model->edu_update($data, $eduData['id']);
                }

                if($eduInserted) {
                    $userId = $userTokenCheck[0]['id'];
                    $eduData = $this->fetchEduSingleData($userId);
                    $response = ['education' => $eduData, 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                } else{
                    $errors = "Data not inserted";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            } }else{
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function languageAdd_post() {
        $langData = $this->input->post();
        $this->form_validation->set_rules('lang_id', 'Language Id', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $langData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $langids = $langData['lang_id'];
                $langids = explode(',', $langids);
                
                //echo $langids;die;
                
                  
                    if(empty($langData['id'])) {
                        $deleteData = $this->User_Model->delete_user1($userTokenCheck[0]['id'], 'user_language');
                        foreach($langids as $langid) {  
                        $data = ["user_id"=>$userTokenCheck[0]['id'], "lang_id"=>$langid];
                        $langCount = $this->User_Model->lang_beforeInsertCount($userTokenCheck[0]['id']);
                        if(count($langCount) < 5) {
                          $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                          
                          if($getCompleteness) {
                              if($getCompleteness[0]['language'] == 0) {
                                  $profileComplete = 10;
                                  $comProfile = ["language" => $profileComplete];
                                  $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                              }
                          }
                          $langInserted = $this->User_Model->lang_insert($data);

                          if($langInserted) {
                            $userId = $userTokenCheck[0]['id'];
                            $langData = $this->fetchLangSingleData($userId);
                            $response = ['languageAdd' => $langData, 'langCount'=>count($langData), 'status' => "SUCCESS"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                          } else{
                            $errors = "Data not inserted";
                            $response = ['message' => $errors, 'status' => "FAILURE"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                          }
                        } else {
                            $errors = "Language not accepted more than 5";
                            $response = ['message' => $errors, 'status' => "FAILURE"];
                            $this->set_response($response, REST_Controller::HTTP_CREATED);
                        }
                    }} else {
                        $data = ["user_id"=>$userTokenCheck[0]['id'], "lang_id"=>$langData['lang_id']];
                        $langInserted = $this->User_Model->lang_update($data, $langData['id']);
                        //echo $this->db->last_query();
                        if($langInserted) {
                          $userId = $userTokenCheck[0]['id'];
                          $langData = $this->fetchLangSingleData($userId);
                          $response = ['languageAdd' => $langData, 'status' => "SUCCESS"];
                          $this->set_response($response, REST_Controller::HTTP_CREATED);
                        } else{
                          $errors = "Data not inserted";
                          $response = ['message' => $errors, 'status' => "FAILURE"];
                          $this->set_response($response, REST_Controller::HTTP_CREATED);
                        }
                    }
               
            }} else{
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function topclients_post() {
        $clientData = $this->input->post();
        
        $data = ["token" => $clientData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            
            $data = ["user_id"=>$userTokenCheck[0]['id'], "clients"=>$clientData['clients']];
            $clientCheck = $this->User_Model->client_beforeinsertcheck($data);

            if($clientCheck) {
                $errors = "Client already added";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else {

                if(empty($clientData['id'])) {
                    $clientCount = $this->User_Model->client_beforeInsertCount($userTokenCheck[0]['id']);

                    if(count($clientCount) < 5) {
                      $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                      if($getCompleteness) {
                          if($getCompleteness[0]['topClient'] == 0) {
                              $profileComplete = 10;
                              $comProfile = ["topClient" => $profileComplete];
                              $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                          }
                      }
                      $data = ["user_id"=>$userTokenCheck[0]['id'], "clients"=>$clientData['clients']];
                      $clientInserted = $this->User_Model->topClient_insert($data);

                      if($clientInserted) {
                          $userId = $userTokenCheck[0]['id'];
                          $clientData = $this->fetchClientSingleData($userId);
                          $response = ['topclients' => $clientData, 'status' => "SUCCESS"];
                          $this->set_response($response, REST_Controller::HTTP_CREATED);
                      } else{
                          $errors = "Data not inserted";
                          $response = ['message' => $errors, 'status' => "FAILURE"];
                          $this->set_response($response, REST_Controller::HTTP_CREATED);
                      }
                    } else{
                        $errors = "Clients not accepted more than 5";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }

                } else {
                    $data = ["clients"=>$clientData['clients']];
                    $clientInserted = $this->User_Model->topClient_update($data, $clientData['id']);

                    if($clientInserted) {
                        $userId = $userTokenCheck[0]['id'];
                        $clientData = $this->fetchClientSingleData($userId);
                        $response = ['topclients' => $clientData, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else{
                        $errors = "Data not inserted";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
            }
        }} else{
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function addSkills_post() {
        $skillsData = $this->input->post();
        
        $data = ["token" => $skillsData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {            
            
            $data = ["user_id"=>$userTokenCheck[0]['id'], "skills"=>$skillsData['skills']];
            $skillsCheck = $this->User_Model->skills_beforeinsertcheck($data);

            if($skillsCheck) {
                $errors = "Skills already added";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else {

                if(empty($skillsData['id'])) {
                    $skillsCount = $this->User_Model->skills_beforeInsertCount($userTokenCheck[0]['id']);
                  //print_r($skillsCount);die;
                    //echo count($skillsCount);die;
                    if(count($skillsCount)<5) {         
                      $data = ["user_id"=>$userTokenCheck[0]['id'], "skills"=>$skillsData['skills']];
                      $skillsInserted = $this->User_Model->topSkills_insert($data);

                      if($skillsInserted) {
                          $userId = $userTokenCheck[0]['id'];
                          $skillsData = $this->fetchSkillsSingleData($userId);
                          //var_dump($getClientData);die;
                          $response = ['addSkills' => $skillsData, 'status' => "SUCCESS"];
                          $this->set_response($response, REST_Controller::HTTP_CREATED);
                      } else{
                          $errors = "Data not inserted";
                          $response = ['message' => $errors, 'status' => "FAILURE"];
                          $this->set_response($response, REST_Controller::HTTP_CREATED);
                      }
                    } else{
                        $errors = "Skills not accepted more than 5";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                } else {
                    $data = ["skills"=>$skillsData['skills']];
                    $skillsInserted = $this->User_Model->topSkills_update($data, $skillsData['id']);

                    if($skillsInserted) {
                        $userId = $userTokenCheck[0]['id'];
                        $skillsData = $this->fetchSkillsSingleData($userId);
                        //var_dump($getClientData);die;
                        $response = ['addSkills' => $skillsData, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else{
                        $errors = "Data not inserted";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
            }
        }} else{
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function profileUpdate_post() {
        $profileComplete = 0;
        $profileData = $this->input->post();
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        
        if($profileData['profilePic'] != ""){
            $this->form_validation->set_rules('profilePic', 'profilePic', 'required');
            $this->form_validation->set_rules('type', 'type', 'required');
        }

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $profileData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {

                if(!empty($profileData['dob'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if($getCompleteness) {
                        if($getCompleteness[0]['dob'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["dob" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                if(!empty($profileData['location'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if($getCompleteness) {
                        if($getCompleteness[0]['location'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["location" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                if(!empty($profileData['designation'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if($getCompleteness) {
                        if($getCompleteness[0]['designation'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["designation" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                if(!empty($profileData['current_salary'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if($getCompleteness) {
                        if($getCompleteness[0]['current_salary'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["current_salary" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }
                if(!empty($profileData['exp_month']) || !empty($profileData['exp_year'])) {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if($getCompleteness) {
                        if($getCompleteness[0]['exp'] == 0) {
                            $profileComplete = 2;
                            $comProfile = ["exp" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                }

                $data1 = ["name"=> $profileData['name'],"country_code"=>$profileData['country_code'], "phone"=> $profileData['phone'],"dob"=>$profileData['dob'], "location"=> $profileData['location'], "designation"=> $profileData['designation'], "current_salary"=> $profileData['current_salary'], "exp_month"=> $profileData['exp_month'], "exp_year"=> $profileData['exp_year']];
                $userUpdated = $this->User_Model->user_update($data1, $userTokenCheck[0]['id']);

                    $data2 = ["user_id"=> $userTokenCheck[0]['id'], "verbal"=> $profileData['verbal'], "written"=>$profileData['written'], "listening"=>$profileData['listening'], "problem"=> $profileData['problem']];

                    $checkAssessment = $this->User_Model->user_assessment($userTokenCheck[0]['id']);
                    
                    if($checkAssessment) {
                        if($profileData['verbal'] != 0 && $profileData['written'] != 0 && $profileData['listening'] != 0 && $profileData['problem'] != 0)
                        {
                            $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                            if($getCompleteness) {
                                if($getCompleteness[0]['assessment'] == 0) {
                                    $profileComplete = 10;
                                    $comProfile = ["assessment" => $profileComplete];
                                    $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                                }
                            }
                        }   
                        $checkAssessmentUpdate = $this->User_Model->user_assessment_update($data2, $userTokenCheck[0]['id']);
                    } else{

                        if(!empty($profileData['verbal']) && !empty($profileData['written']) && !empty($profileData['listening']) && !empty($profileData['problem']))
                        {
                            $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                            if($getCompleteness) {
                                if($getCompleteness[0]['assessment'] == 0) {
                                    $profileComplete = 10;
                                    $comProfile = ["assessment" => $profileComplete];
                                    $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                                }
                            }
                        }

                        $checkAssessmentInsert = $this->User_Model->user_assessment_insert($data2);
                    }

                    $data3 = ["user_id"=> $userTokenCheck[0]['id'], "expert" => $profileData['expert']];

                    $checkExpert = $this->User_Model->user_expert($userTokenCheck[0]['id']);
                    
                    if($checkExpert) {

                        if(!empty($profileData['expert'])) {
                            $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                            if($getCompleteness) {
                                if($getCompleteness[0]['expert'] == 0) {
                                    $profileComplete = 10;
                                    $comProfile = ["expert" => $profileComplete];
                                    $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                                }
                            }
                        }
                        $checkExpertUpdate = $this->User_Model->user_expert_update($data3, $userTokenCheck[0]['id']);
                    } else{

                        if(!empty($profileData['expert'])) {
                            $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                            if($getCompleteness) {
                                if($getCompleteness[0]['expert'] == 0) {
                                    $profileComplete = 10;
                                    $comProfile = ["expert" => $profileComplete];
                                    $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                                }
                            }
                        }
                        $checkExpertInsert = $this->User_Model->user_expert_insert($data3);
                    }

                if($profileData['profilePic'] != "" && !empty($profileData['profilePic'])) {
                    $userPic = $this->Common_Model->image_upload($profileData['profilePic'], $profileData['type']);

                    if($userPic) {
                        $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                        if($getCompleteness) {
                            if($getCompleteness[0]['profilePic'] == 0) {
                                $profileComplete = 10;
                                $comProfile = ["profilePic" => $profileComplete];
                                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                            }
                        }
                        $userPicUpdate = $this->User_Model->user_pic_update($userPic, $userTokenCheck[0]['id']);
                    }
                }

                $userData = $this->User_Model->user_single($userTokenCheck[0]['id']);
                if($userData[0]['profilePic'] != "") {
                    $userProfilePic = $userData[0]['profilePic'];
                } else{
                    $userProfilePic = "";
                }
                $assessData = $this->User_Model->user_assessment($userTokenCheck[0]['id']);
                $expertData = $this->User_Model->user_expert($userTokenCheck[0]['id']);
                $workData = $this->fetchWorkSingleData($userTokenCheck[0]['id']);
                $eduData = $this->fetchEduSingleData($userTokenCheck[0]['id']);
                $langData = $this->fetchLangSingleData($userTokenCheck[0]['id']);
                $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['id']);    
                $profile['completeness'] = $getCompletenes['comp'];

                $profile['user'] = ["name"=>$userData[0]['name'], "dob"=>$userData[0]['dob'], "email"=>$userData[0]['email'],"country_code"=>$userData[0]['country_code'], "phone"=>$userData[0]['phone'], "location"=>$userData[0]['location'],"designation"=>$userData[0]['designation'],"current_salary"=>$userData[0]['current_salary'],"exp_month"=>$userData[0]['exp_month'],"exp_year"=>$userData[0]['exp_year'], "profilePic" => $userProfilePic];

                if($assessData) {
                    $profile['assessment'] = ["verbal"=>$assessData[0]['verbal'], "written"=>$assessData[0]['written'], "listening"=>$assessData[0]['listening'], "problem"=>$assessData[0]['problem']];
                } else{
                    $profile['assessment'] = [];
                }
                if($workData) {
                    $profile['experience'] = $workData;    
                } else {
                    $profile['experience'] = [];
                }
                if($eduData) {
                    $profile['education'] = $eduData;    
                } else{
                    $profile['education'] = [];
                }
                if($langData){
                    $profile['language'] = $langData;    
                } else{
                    $profile['language'] = [];
                }
                if($expertData) {
                    $expert = $expertData[0]['expert'];
                    $extractExport = explode(',', $expert); 
                    $profile['expert'] = $extractExport;
                } else{
                    $profile['expert'] = [];
                }
                
                $clientData = $this->fetchClientSingleData($userTokenCheck[0]['id']);
                if($clientData) {
                    $profile['clients'] = $clientData;
                } else{
                    $profile['clients'][] = [
                                "id" =>"",
                                "clients"=>""
                            ];
                }
                $getResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);

                if($getResume) {
                    $profile["resume"] = $getResume[0]['resume'];
                } else{
                    $profile["resume"] = "";
                }
                $profile['rating'] = $this->calculateRating($userTokenCheck[0]['id']);
                
                $response = ['profileupdate' => $profile, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function profileMore_post() {
        $profileData = $this->input->post();
        
        $data = ["token" => $profileData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {

            $data = ["user_id"=> $userTokenCheck[0]['id'], "strength"=>$profileData['strength'], "weakness"=> $profileData['weakness'], "achievements"=> $profileData['achievements']];

            $checkMore = $this->User_Model->user_more($userTokenCheck[0]['id']);
                
            if($checkMore) {
                $checkMoreUpdate = $this->User_Model->user_more_update($data, $userTokenCheck[0]['id']);
            } else{
                $checkMoreInsert = $this->User_Model->user_more_insert($data);
            }

            $moreData = $this->fetchMoreSingleData($userTokenCheck[0]['id']);
            if($moreData) {
                $profile['more'] = $moreData;
            } else{
                $profile['more'] = [
                            "id" =>"",
                            "strength"=>"",
                            "weakness"=>"",
                            "achievements"=>""
                        ];
            }

            $response = ['profileMore' => $profile, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function profileMoreView_post() {
        $profileData = $this->input->post();
        $data = ["token" => $profileData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
          $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);  
        if($userTokenCheck) {
            $skillsData = $this->fetchSkillsSingleData($userTokenCheck[0]['id']);

            if($skillsData) {
                $profile['skills'] = $skillsData;
            } else{
                $profile['skills'][] = [
                            "id" =>"",
                            "skills"=>""
                        ];
            }
            $moreData = $this->fetchMoreSingleData($userTokenCheck[0]['id']);
            if($moreData) {
                $profile['more'] = $moreData;
            } else{
                $profile['more'] = [
                            "id" =>"",
                            "strength"=>"",
                            "weakness"=>"",
                            "achievements"=>""
                        ];
            }
            $response = ['profileMoreView' => $profile, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function profileView_post() {
        $profileData = $this->input->post();
        $data = ["token" => $profileData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
          $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);  
        if($userTokenCheck) {

            $userData = $this->User_Model->user_single($userTokenCheck[0]['id']);
            if($userData[0]['profilePic'] != "") {
                $userProfilePic = $userData[0]['profilePic'];
            } else{
                $userProfilePic = "";
            }
            $assessData = $this->User_Model->user_assessment($userTokenCheck[0]['id']);
            $expertData = $this->User_Model->user_expert($userTokenCheck[0]['id']);
            
            $workData = $this->fetchWorkSingleData($userTokenCheck[0]['id']);
            $eduData = $this->fetchEduSingleData($userTokenCheck[0]['id']);
            $langData = $this->fetchLangSingleData($userTokenCheck[0]['id']);
            $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['id']);    
            $profile['completeness'] = $getCompletenes['comp'];
            if($userData[0]['designation']){
                $userData[0]['designation'] = $userData[0]['designation'];
            }  else{
               $userData[0]['designation'] = ""; 
            }
            if($userData[0]['current_salary']){
                $userData[0]['current_salary'] = $userData[0]['current_salary'];
            }  else{
               $userData[0]['current_salary'] = ""; 
            }
            if($userData[0]['exp_month']>="0"){
                $userData[0]['exp_month'] = $userData[0]['exp_month'];
            }  else{
               $userData[0]['exp_month'] = ""; 
            }
            if($userData[0]['exp_year']>="0"){
                $userData[0]['exp_year'] = $userData[0]['exp_year'];
            }  else{
               $userData[0]['exp_year'] = ""; 
            }

            if($userData[0]['country_code']){
                $userData[0]['country_code'] = $userData[0]['country_code'];
            }  else{
               $userData[0]['country_code'] = ""; 
            }
            $profile['user'] = ["name"=>$userData[0]['name'], "dob"=>$userData[0]['dob'], "email"=>$userData[0]['email'],"country_code"=>$userData[0]['country_code'], "phone"=>$userData[0]['phone'], "location"=>$userData[0]['location'],"designation"=>$userData[0]['designation'],"current_salary"=>$userData[0]['current_salary'],"exp_month"=>$userData[0]['exp_month'],"exp_year"=>$userData[0]['exp_year'], "profilePic" => $userProfilePic];
            
            if($assessData) {
                $profile['assessment'] = ["verbal"=>$assessData[0]['verbal'], "written"=>$assessData[0]['written'], "listening"=>$assessData[0]['listening'], "problem"=>$assessData[0]['problem']];
            } else{
                $profile['assessment'] = [
                                "verbal"=> "",
                                "written"=> "",
                                "listening"=> "",
                                "problem"=> ""
                            ];
            }
            if($workData) {
                $profile['experience'] = $workData;
            } else{
                $profile['experience'][] = [
                            "id" => "",
                            "title" => "",
                            "company" => "",
                            "desc" => "",
                            "from" => "",
                            "to" => "",
                            "industry" => "",
                            "channel" => ""
                        ];
            }
            if($eduData) {
                $profile['education'] = $eduData;
            } else{
                $profile['education'][] = [
                                    "id" => "",
                                    "attainment" => "",
                                    "degree" => "",
                                    "university" => "",
                                    "from" => "",
                                    "to" => ""
                                ];
            }
            if($langData) {
                $profile['language'] = $langData;
            } else{
                $profile['language'][] = [
                        "id"=> "",
                        "lang_name"=> ""
                    ];
            }
            if($expertData) {
                if(empty($expertData[0]['expert'])){
                    $profile['expert'] =[];
                } else {
                    $expert = $expertData[0]['expert'];
                    $extractExport = explode(',', $expert); 
                    $profile['expert'] = $extractExport;
                }
                
            } else {
                $profile['expert'] = [];
            }

            $clientData = $this->fetchClientSingleData($userTokenCheck[0]['id']);
            if($clientData) {
                $profile['clients'] = $clientData;
            } else{
                $profile['clients'][] = [
                                "id" =>"",
                                "clients"=>""
                                ];
            }
            $getResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);

            if($getResume) {
                $profile["resume"] = $getResume[0]['resume'];
            } else{
                $profile["resume"] = "";
            }
            $profile['rating'] = $this->calculateRating($userTokenCheck[0]['id']);
            $response = ['profileview' => $profile, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function userResume_post() {
        $resumeData = $this->input->post();
        $this->form_validation->set_rules('resume', '', 'required');
        $this->form_validation->set_rules('type', '', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $data = ["token" => $resumeData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $checkResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);

                if($checkResume) {
                    $userResume = $this->Common_Model->file_upload($resumeData['resume'], $resumeData['type']);
                    $userResumeUpdate = $this->User_Model->user_resume_update($userResume, $userTokenCheck[0]['id']);
                } else {
                    $getCompleteness = $this->User_Model->check_profileComplete($userTokenCheck[0]['id']);
                    if($getCompleteness) {
                        if($getCompleteness[0]['resume'] == 0) {
                            $profileComplete = 10;
                            $comProfile = ["resume" => $profileComplete];
                            $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
                        }
                    }
                    $userResume = $this->Common_Model->file_upload($resumeData['resume'], $resumeData['type']);
                    $userResumeInsert = $this->User_Model->user_resume_insert($userResume, $userTokenCheck[0]['id']);
                }
                $getResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);
                $data = ["resume" => $getResume[0]['resume']];
                $response = ['userresume' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function logout_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck = $this->User_Model->token_match1($data);
       // echo $this->db->last_query();die;
        if($userTokenCheck) {
            $userLogout = $this->User_Model->user_logout1($userTokenCheck[0]['user_id'],$userData['token']);
            //echo $this->db->last_query();die;
            if($userLogout) {
                $response = ['message' => "Logout successfully", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else{
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function forgotPassword_post() {
        $userData = $this->input->post();
        $data = ["email" => $userData['email']];
        $userTokenCheck = $this->User_Model->email_match($data);
        
        if($userTokenCheck) {
            $email = $userTokenCheck[0]['email'];
            $name = $userTokenCheck[0]['name'];
            $token = $this->getToken();
            $data['full_name'] = ucfirst($name);
            $data['token'] = $token;
            $data['user_id'] = $userTokenCheck[0]['id'];
            $msg = $this->load->view('passwordemail',$data,TRUE);
            $config=array(
                  'protocol' => 'smtp',
                  'smtp_host' => 'smtpout.asia.secureserver.net',
                  'smtp_user' => 'help@jobyoda.com',
                  'smtp_pass' => 'Usa@1234567',
                  'smtp_port' => 465,
                  'smtp_crypto' => 'ssl',
                  'charset'=>'utf-8',
                  'mailtype' => 'html',
                  'crlf' => "\r\n",
                  'newline' => "\r\n"
            );

            $this->email->initialize($config);
            $this->email->from("help@jobyoda.com", "JobYoDA");
            $this->email->to($email);
            $this->email->subject('JobYoDA - Forgot Password Link');


            /*$this->load->library('email');
            $this->email->from("ayush.jain@mobulous.com", "JobYoda");
            $this->email->to($email);
            $this->email->subject('JobYoda - Forgot Password Link');
            $token = $this->getToken();
            $msg = "Dear " . $name;
            $msg .= ", Please click on the below link to change your account password: ";
            $msg .= base_url() ."user/forgotpasswordviewapp/".$userTokenCheck[0]['token'];
            $msg .= ". Your verification code is ". $token;*/
            
            $this->email->message($msg);
            
            if($this->email->send()) {
               $forgotPassCheck = $this->User_Model->forgotPass_check($userTokenCheck[0]['id']);     
               //echo $this->db->last_query();die;
               if($forgotPassCheck) {
                  $vCode = ["verifyCode"=>$token];
                  $this->User_Model->forgotPass_update($vCode, $userTokenCheck[0]['id']);
               } else {
                  $vCode = ["user_id"=>$userTokenCheck[0]['id'], "verifyCode"=>$token];
                  $this->User_Model->forgotPass_insert($vCode);
               }

                $success = "Check your Email for change password";
                $response = ['message' => $success, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else {
                $errors = "Email is not send";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else{
            $errors = "Email is not yet registered";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function checkResume_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            $job_detail = $this->Jobpost_Model->job_detail_fetch($userData['jobpost_id']);
                //print_r($job_detail);die;
                /*if($job_detail[0]['level_status']=='1'){
                    $user_level_match = $this->User_Model->user_single_workbyuserid1($userTokenCheck[0]['id']);
                    //print_r($user_level_match);die;
                    if($user_level_match){
                        $level_match = "True";
                    } else{
                        $level_match = "False";
                    }

                }
                else{
                    $level_match = "True";
                }*/
                if($job_detail[0]['education_status']=='1'){
                    $user_education_match = $this->User_Model->user_single_educationbyuserid1($userTokenCheck[0]['id']);

                    if($user_education_match){
                        $education_match = "True";
                    } else{
                        $education_match = "False";
                    }

                }else{
                    $education_match = "True";
                }
                    if($job_detail[0]['experience_status']=='1'){
                    $expmonth = $userTokenCheck[0]['exp_month'];
                    $expyear = $userTokenCheck[0]['exp_year'];

                     if($userTokenCheck[0]['exp_month']>=0   && $userTokenCheck[0]['exp_year']>=0){
                        $experience_match = "True";
                    }  else{
                        $experience_match = 'False';
                    }

                }else{
                    $experience_match = "True";
                }
                if($education_match == "True" && $experience_match== "True"){
                    /*if($level_match == "True" && $education_match == "True" && $experience_match== "True"){*/
                    $checkResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);
                    if($checkResume) {
                        $response = ['message' => "Resume found", 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else{
                        $response = ['message' => "Resume not found", 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }else{
                    $errors = "Please update your Total Years of Experience and Highest Education Level to qualify for this job.";
                    /*$errors = "You are applying for a job for which your profile doesn’t match the requirements. Please update your profile.";*/
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                }    
        }} else{
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }
    
    public function deleteuserData_post(){
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck){
            if($userData['type'] == "clients") {
                $tableName = "user_topclients";
                //$comProfile = ["topClient" => 0];
            } else if($userData['type'] == "language") {
                $tableName = "user_language";
                //$comProfile = ["language" => 0];
            } else if($userData['type'] == "work_experience") {
                $tableName = "user_work_experience";
                //$comProfile = ["experience" => 0];
            } else if($userData['type'] == "education") {
                $tableName = "user_education";
                //$comProfile = ["education" => 0];
            } else if($userData['type'] == "skills") {
                $tableName = "user_skills";
            }
            $deleteData = $this->User_Model->delete_user($userData['id'], $tableName);
            $workData = $this->fetchWorkSingleData($userTokenCheck[0]['id']);
            $eduData = $this->fetchEduSingleData($userTokenCheck[0]['id']);
            $langData = $this->fetchLangSingleData($userTokenCheck[0]['id']);
            $clientData = $this->fetchClientSingleData($userTokenCheck[0]['id']); 
            if(empty($clientData)){
                $comProfile = ["topClient" => 0];
            }
            if(empty($workData)){
                $comProfile = ["experience" => 0];
            } 
            if(empty($eduData)){
                $comProfile = ["education" => 0];
            } 
            if(empty($langData)){
                $comProfile = ["language" => 0];
            }   
            if(!empty($comProfile)){
                $this->User_Model->update_profileComplete($comProfile, $userTokenCheck[0]['id']);
            }
            
            if($deleteData=="true"){
                $response = ['message' => "Successfully deleted", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }else{
                $response = ['message' => "Unable to Delete", 'status' => 'SUCCESS'];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } }else{
            $errors ="Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }


    public function jobsearchstatus_post() {
        $jobData = $this->input->post();
        $this->form_validation->set_rules('jobsearchstatus', 'jobsearchstatus', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $data = ["token" => $jobData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $statusdata = $jobData['jobsearchstatus'];
                $statusUpdate = $this->User_Model->jobsearchstatus_update($statusdata, $userTokenCheck[0]['id']);
             
                $response = ['jobsearchstatus' => "Status changed Successfully", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } }else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function getjobsearchstatus_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
        $checkStatus = $this->User_Model->user_single($userTokenCheck[0]['id']);
        $jobstatus = ['jobsearchstatus'=>$checkStatus[0]['jobsearch_status']];
        $response = ['jobsearch_status' => $jobstatus, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
            
        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function notificationstatus_post() {
        $jobData = $this->input->post();
        $this->form_validation->set_rules('notificationstatus', 'notification status', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $data = ["token" => $jobData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) {
                $notificationstatusdata = $jobData['notificationstatus'];
                $statusUpdate = $this->User_Model->notificationstatus_update($notificationstatusdata, $userTokenCheck[0]['id']);
             
                $response = ['notificationstatus' => "Status changed Successfully", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }} else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function getnotificationstatus_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
        $checkStatus = $this->User_Model->user_single($userTokenCheck[0]['id']);
        //print_r($checkStatus);die;
        $jobstatus = ['notificationstatus'=>$checkStatus[0]['notification_status'], 'type' =>$checkStatus[0]['type']];
        $response = ['notification_status' => $jobstatus, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
            
        }} else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

     public function getuseremail_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
        $checkStatus = $this->User_Model->user_single($userTokenCheck[0]['id']);
        $useremail = $checkStatus[0]['email'];
        $response = ['user_email' => $useremail, 'status' => "SUCCESS"];
        $this->set_response($response, REST_Controller::HTTP_CREATED);
            
        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function usercontact_post() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('token', 'token', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('message', 'description', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {
            $data = ["token" => $userData['token']];
            //$userTokenCheck = $this->User_Model->token_match($data);
            $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            //print_r($userTokenCheck);die;
            if($userTokenCheck) {
                    $data1 = ["user_id" => $userTokenCheck[0]['id'],"email"=> $userData['email'], "message"=>$userData['message'] ];
                    $this->load->library('email');
                    $data['name'] = ucfirst($userTokenCheck[0]['name']);
                    $data['message'] = $userData['message'];
                    $caseid = rand(10000,99999);
                    $data['caseId'] = 'JobYoDA-Jobseeker'.$caseid;
                    $msg = $this->load->view('contactappemail',$data,TRUE);
                    $config=array(
                    'charset'=>'utf-8',
                    'wordwrap'=> TRUE,
                    'mailtype' => 'html'
                    );

                    $this->email->initialize($config);
                    $this->email->from($userTokenCheck[0]['email'], "JobYoDA");
                    //$this->email->to('villaverred@gmail.com','digvijayankoti29@gmail.com');
                    $this->email->to('help@jobyoda.com');
                    //$this->email->to('sunaina.singhal@mobulous.com');
                    $this->email->subject('Ticket ID:'.$caseid);
                    
                    $this->email->message($msg);
                    $this->email->send();
                  $contact_inserted =  $this->User_Model->insert_contact($data1);
                  //echo $this->db->last_query();die;
                  if($contact_inserted){
                    $response = ['message' => "Submitted Successfully", 'status' => "SUCCESS"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
                    
                 
            } }else {
                $errors = "Bad Request";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        }
    }

    public function signinbonus_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        //$userTokenCheck = $this->User_Model->token_match($data);
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
        if($userTokenCheck) {
            //$data['bonus_history']=[];
            $bonusamount =  $this->User_Model->fetch_bonusamount($userTokenCheck[0]['id']);
            $data['bonus_amount'] = $bonusamount[0]['sign_in_bonus'];

            $bonushistorys =  $this->User_Model->fetch_bonushistory($userTokenCheck[0]['id']);
            $x=0;
            foreach($bonushistorys as $bonushistory) {

               $data['bonus_history'][$x] = ["id"=>$bonushistory['id'], "bonus"=>$bonushistory['bonus'], "notification"=>$bonushistory['notification'], "added_at"=>date("d-M-Y h:i:s",strtotime($bonushistory['added_at']))];
               $x++;
            }

            if($bonushistorys){
                $response = ['signinbonus' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else{
                $data['bonus_amount'] = "0";
                $data['bonus_history'] = []; 
                $response = ['signinbonus' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }           
        } }else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function quotation_post() {
      $userData = $this->input->post();
      $data = ["token" => $userData['token']];
      //$userTokenCheck = $this->User_Model->token_match($data);
      $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
      if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
      if($userTokenCheck) {
            date_default_timezone_set("Asia/Manila");
            if(isset($userData['last_used']) && !empty($userData['last_used']) && !empty($userData['app_version']) && isset($userData['app_version'])){
                $userData['last_used'] = date('Y-m-d H:i:s',strtotime($userData['last_used']));
                $datadevicetoken = ["app_version"=>$userData['app_version'], "last_used"=>$userData['last_used'], "platform"=>$userData['platform']];
                $this->User_Model->update_token($datadevicetoken, $userTokenCheck[0]['id']);
                //echo $this->db->last_query();die;
            }
            
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $data1 = $this->User_Model->quote_single();
            $response = ['quote' => $data1,'notification_count'=>count($fetchnotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
      }}else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
        
    }


    public function send_message_post(){
        $userData = $this->input->post();
        $this->form_validation->set_rules('phone', 'Phone No', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $error = current($errors);
            $response = ['message' => $error, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
                $to = '+91'.$userData['phone'];
                $sms_message = $userData['message'];
                echo $this->sendSms($to, $sms_message);die;
                $response = ['message' => "Message sent successfully.", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }    
    }

    public function quotation_android_post() {
      $userData = $this->input->post();
      $data = ["token" => $userData['token']];
      //$userTokenCheck = $this->User_Model->token_match($data);
      $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
      if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
      if($userTokenCheck) {
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $data1 = $this->User_Model->quote_single();
        $response = ['quote' => $data1,'notification_count'=>count($fetchnotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
      }}else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
        
    }
   public function calculateRating_post() {
      $userData = $this->input->post();
      $data = ["token" => $userData['token']];
      //$userTokenCheck = $this->User_Model->token_match($data);
      $userTokenCheck1 = $this->User_Model->token_match1($data);
        //print_r($userTokenCheck1);die;
        if($userTokenCheck1){
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
      if($userTokenCheck) {
          $this->load->helper('date');
          $attainments = $this->fetchEduSingleData($userTokenCheck[0]['id']);
          //print_r($attainments);die;
          if($attainments) {
             $rate1 = array();
             foreach($attainments as $attainment) {
                if($attainment['attainment'] == "Post Graduate") {
                   $rate1[] = 5;
                } else if($attainment['attainment'] == "College Graduate") {
                   $rate1[] = 5;
                } else if($attainment['attainment'] == "Associate Degree") {
                   $rate1[] = 4;
                } else if($attainment['attainment'] == "Undergraduate") {
                   $rate1[] = 3;
                } else if($attainment['attainment'] == "Vocational") {
                   $rate1[] = 3;
                } else if($attainment['attainment'] == "High School") {
                   $rate1[] = 2;
                } else{
                   $rate1[] = 0;
                }
             }
             $maxVal = max($rate1);
             $star = ($maxVal * 20)/100;  
          
          } else{
             $star =0;
          }

          $experiences = $this->fetchWorkSingleData($userTokenCheck[0]['id']);
          
          if($experiences) {
             $totalDays = 0;
             foreach($experiences as $experience) {
                $from = $experience['from'];
                $to = $experience['to'];
                $datetime1 = strtotime($from);
                $datetime2 = strtotime($to);
                $datediff = $datetime1 - $datetime2;
                $totalDays = round($datediff / (60 * 60 * 24));
             }

             if($totalDays == 0) {
                $rate2 = 1;
             } else if(($totalDays < 360) && ($totalDays > 0)) {
                $rate2 = 2;
             } else if(($totalDays < 720) && ($totalDays > 300)) {
                $rate2 = 3;
             } else if(($totalDays < 1080) && ($totalDays > 720)) {
                $rate2 = 4;
             } else if($totalDays > 1080) {
                $rate2 = 5;
             } else {
                $rate2 = 0;
             }
             $star1 = $rate2 * 30 / 100;
          } else {
             $star1 = 0;
          }

          $tenure = $this->fetchWorkuniqueData($userTokenCheck[0]['id']);
          
          if($tenure) {
             $totalDays1 = 0;
             $countComp = 0;
             foreach($experiences as $experience) {
                $from = $experience['from'];
                $to = $experience['to'];
                $datetime1 = strtotime($from);
                $datetime2 = strtotime($to);
                $datediff = $datetime1 - $datetime2;
                $totalDays1 = round($datediff / (60 * 60 * 24));
                $countComp++;
             }

             $avgWork = $totalDays1 / $countComp;
             if($avgWork == 0) {
                $rate3 = 1;
             } else if($avgWork < 360 && $avgWork > 0) {
                $rate3 = 2;
             } else if($avgWork < 720 && $avgWork > 300) {
                $rate3 = 3;
             } else if($avgWork < 1080 && $avgWork > 720) {
                $rate3 = 4;
             } else if($avgWork >= 1080) {
                $rate3 = 5;
             } else{
                $rate3 = 0;
             }
             $star2 = ($rate3 * 30)/100;
          } else{
             $star2 =0;
          }
          
          $self = $this->User_Model->user_assessment($userTokenCheck[0]['id']);
          
          if($self) {
             $totalSelf = $self[0]['verbal'] + $self[0]['written'] + $self[0]['listening'] + $self[0]['problem'];
             $avgSelf = $totalSelf / 5;
             $star3 = ($avgSelf * 20)/100;
          } else{
             $star3 = 0;
          }

          $totalStar = $star + $star1 + $star2 + $star3;
          $totalStar = number_format((float)$totalStar, 1, '.', '');

          $userData = $this->User_Model->user_single($userTokenCheck[0]['id']);
          if($userData[0]['profilePic'] != "") {
              $userProfilePic = $userData[0]['profilePic'];
          } else{
              $userProfilePic = "";
          }

          $data = ["rating"=> $totalStar, "profilePic"=> $userProfilePic, "name"=>$userData[0]['name']];

          $response = ["calculateRating"=>$data, "status"=>"SUCCESS"];
          $this->set_response($response, REST_Controller::HTTP_CREATED);
      
      } }else{
          $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
      }
   }

   public function calculateRating($id) {

          $this->load->helper('date');
          $attainments = $this->fetchEduSingleData($id);
          if($attainments) {
             $rate1 = array();
             foreach($attainments as $attainment) {
                if($attainment['attainment'] == "Post Graduate") {
                   $rate1[] = 5;
                } else if($attainment['attainment'] == "College Graduate") {
                   $rate1[] = 5;
                } else if($attainment['attainment'] == "Associate Degree") {
                   $rate1[] = 4;
                } else if($attainment['attainment'] == "Undergraduate") {
                   $rate1[] = 3;
                } else if($attainment['attainment'] == "Vocational") {
                   $rate1[] = 3;
                } else if($attainment['attainment'] == "High School") {
                   $rate1[] = 2;
                } else{
                   $rate1[] = 0;
                }
             }
             $maxVal = max($rate1);
             $star = ($maxVal * 20)/100;  
          
          } else{
             $star =0;
          }

          $experiences = $this->fetchWorkSingleData($id);
          
          if($experiences) {
             $totalDays = 0;
             foreach($experiences as $experience) {
                $from = $experience['from'];
                $to = $experience['to'];
                $datetime1 = strtotime($from);
                $datetime2 = strtotime($to);
                $datediff = $datetime1 - $datetime2;
                $totalDays = round($datediff / (60 * 60 * 24));
             }

             if($totalDays == 0) {
                $rate2 = 1;
             } else if(($totalDays < 360) && ($totalDays > 0)) {
                $rate2 = 2;
             } else if(($totalDays < 720) && ($totalDays > 300)) {
                $rate2 = 3;
             } else if(($totalDays < 1080) && ($totalDays > 720)) {
                $rate2 = 4;
             } else if($totalDays > 1080) {
                $rate2 = 5;
             } else {
                $rate2 = 0;
             }
             $star1 = $rate2 * 30 / 100;
          } else {
             $star1 = 0;
          }

          $tenure = $this->fetchWorkuniqueData($id);
          
          if($tenure) {
             $totalDays1 = 0;
             $countComp = 0;
             foreach($experiences as $experience) {
                $from = $experience['from'];
                $to = $experience['to'];
                $datetime1 = strtotime($from);
                $datetime2 = strtotime($to);
                $datediff = $datetime1 - $datetime2;
                $totalDays1 = round($datediff / (60 * 60 * 24));
                $countComp++;
             }

             $avgWork = $totalDays1 / $countComp;
             if($avgWork == 0) {
                $rate3 = 1;
             } else if($avgWork < 360 && $avgWork > 0) {
                $rate3 = 2;
             } else if($avgWork < 720 && $avgWork > 300) {
                $rate3 = 3;
             } else if($avgWork < 1080 && $avgWork > 720) {
                $rate3 = 4;
             } else if($avgWork >= 1080) {
                $rate3 = 5;
             } else{
                $rate3 = 0;
             }
             $star2 = ($rate3 * 30)/100;
          } else{
             $star2 =0;
          }
          
          $self = $this->User_Model->user_assessment($id);
          
          if($self) {
             $totalSelf = $self[0]['verbal'] + $self[0]['written'] + $self[0]['listening'] + $self[0]['problem'];
             $avgSelf = $totalSelf / 5;
             $star3 = ($avgSelf * 20)/100;
          } else{
             $star3 = 0;
          }

          $totalStar = $star + $star1 + $star2 + $star3;
          $totalStar = number_format((float)$totalStar, 1, '.', '');

          return $totalStar;
      
      
   }

    public function getLocation_post() {
        $data = $this->input->post();
        $lat = $data['latitude'];
        $long = $data['longitude'];
        $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
        $output = json_decode($geocodeFromAddr);
        var_dump($output);die;
        $address = array(
            'country' => google_getCountry($output),
            'province' => google_getProvince($output),
            'city' => google_getCity($output),
            'street' => google_getStreet($output),
            'postal_code' => google_getPostalCode($output),
            'country_code' => google_getCountryCode($output),
            'formatted_address' => google_getAddress($output),
        );
        $response = ['getLocation' => $address, 'status' => "SUCCESS"];
    }

    public function fetchUserSingleDataByToken($id, $deviceToken, $deviceType) {
        $userData = $this->User_Model->user_single($id);
        if($userData[0]['designation']){
            $userData[0]['designation']=$userData[0]['designation'];
        }else{
            $userData[0]['designation']="";
        }
        if($userData[0]['current_salary']){
            $userData[0]['current_salary']=$userData[0]['current_salary'];
        }else{
            $userData[0]['current_salary']="";
        }
        if($userData[0]['exp_month']){
            $userData[0]['exp_month']=$userData[0]['exp_month'];
        }else{
            $userData[0]['exp_month']="";
        }
        if($userData[0]['exp_year']){
            $userData[0]['exp_year']=$userData[0]['exp_year'];
        }else{
            $userData[0]['exp_year']="";
        }

        $data = ["user_id"=>$id, "device_token"=>$deviceToken, "device_type"=>$deviceType];
        $userToken = $this->User_Model->user_token_check($data);


        $data = ["token" => $userToken[0]['token'], "name" => $userData[0]['name'], "email" => $userData[0]['email'], "designation" => $userData[0]['designation'], "current_salary" => $userData[0]['current_salary'], "exp_month" => $userData[0]['exp_month'], "exp_year" => $userData[0]['exp_year'], "type" => $userData[0]['type'], "profilePic" => $userData[0]['profilePic'], "completeness" => $userData[0]['completeness']];
        return $data;
    }

    public function fetchUserSingleData($id) {
        $userData = $this->User_Model->user_single($id);
        if($userData[0]['designation']){
            $userData[0]['designation']=$userData[0]['designation'];
        }else{
            $userData[0]['designation']="";
        }
        if($userData[0]['current_salary']){
            $userData[0]['current_salary']=$userData[0]['current_salary'];
        }else{
            $userData[0]['current_salary']="";
        }
        if($userData[0]['exp_month']){
            $userData[0]['exp_month']=$userData[0]['exp_month'];
        }else{
            $userData[0]['exp_month']="";
        }
        if($userData[0]['exp_year']){
            $userData[0]['exp_year']=$userData[0]['exp_year'];
        }else{
            $userData[0]['exp_year']="";
        }
        $data = ["token" => $userData[0]['token'], "name" => $userData[0]['name'], "email" => $userData[0]['email'], "designation" => $userData[0]['designation'], "current_salary" => $userData[0]['current_salary'], "exp_month" => $userData[0]['exp_month'], "exp_year" => $userData[0]['exp_year'], "type" => $userData[0]['type'], "profilePic" => $userData[0]['profilePic'], "completeness" => $userData[0]['completeness']];
        return $data;
    }

    public function fetchUserCompleteData($id) {
        $userData = $this->User_Model->userComplete_single($id);
        
        if($userData) {
          $data['comp'] = $userData[0]['signup'] + $userData[0]['profile'] + $userData[0]['profilePic'] + $userData[0]['resume'] + $userData[0]['experience'] + $userData[0]['education'] + $userData[0]['assessment'] + $userData[0]['language'] + $userData[0]['expert'] + $userData[0]['topClient']+ $userData[0]['dob']+ $userData[0]['location']+ $userData[0]['designation']+ $userData[0]['current_salary']+ $userData[0]['exp'];
        } else{
          $data['comp'] = 0;
        }
        return $data;
    }

    public function fetchWorkSingleData($id) {
        $workDatas = $this->User_Model->work_single($id);
        if($workDatas) {
            foreach ($workDatas as $workData) {
                $data[] = ["id"=> $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "category"=>$workData['category'], "categoryid"=>$workData['categoryid'], "subcategory"=> $workData['subcategory'],  "subcategoryid"=> $workData['subcatid'], "level"=> $workData['level'], "levelid"=> $workData['levelid'], "currently_working"=> $workData['currently_working']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchWorkuniqueData($id) {
      $workDatas = $this->User_Model->work_unique($id);
        if($workDatas) {
            foreach ($workDatas as $workData) {
                $data[] = ["id"=> $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "category"=>$workData['category'], "categoryid"=>$workData['categoryid'], "subcategory"=> $workData['subcategory'],  "subcategoryid"=> $workData['subcatid'], "level"=> $workData['level'], "levelid"=> $workData['levelid'], "currently_working"=> $workData['currently_working']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchEduSingleData($id) {
        $eduDatas = $this->User_Model->edu_single($id);
        if($eduDatas) {
            foreach ($eduDatas as $eduData) {
                $data[] = ["id"=> $eduData['id'], "attainment" => $eduData['attainment'], "degree" => $eduData['degree'], "university" => $eduData['university'], "from" => $eduData['degreeFrom'], "to" => $eduData['degreeTo']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchLangSingleData($id) {
        $langDatas = $this->User_Model->lang_single($id);
        if($langDatas) {
            foreach ($langDatas as $langData) {
                $data[] = ["id"=> $langData['id'],"lang_id"=> $langData['lang_id'], "lang_name" => $langData['name']];
            }
            return $data;
        } else {
            $data = [];
            return $data;
        }
    }

    public function fetchClientSingleData($id) {
        $clientDatas = $this->User_Model->client_single($id);
        if($clientDatas) {
            return $clientDatas;
        } else {
            $data = [];
            return $data;
        }
    }

    public function fetchSkillsSingleData($id) {
        $skillsDatas = $this->User_Model->skills_single($id);
        if($skillsDatas) {
            return $skillsDatas;
        } else {
            $data = [];
            return $data;
        }
    }

    public function fetchMoreSingleData($id) {
        $moreDatas = $this->User_Model->user_more($id);
        if($moreDatas) {
            $dataMore = ["id"=>$moreDatas[0]['id'], "strength"=>$moreDatas[0]['strength'], "weakness"=> $moreDatas[0]['weakness'], "achievements"=>$moreDatas[0]['achievements']];
            return $dataMore;
        } else {
            $data = [];
            return $data;
        }
    }

    

    /*
    * file value and type check during validation
    */
    public function img_check($str){
        $allowed_mime_type_arr = array('jpeg','jpg','png','x-png');
        $image_parts = explode(";base64,", $image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];

        if(in_array($image_type, $allowed_mime_type_arr)){
            return true;
        }else{
            $this->form_validation->set_message('file_check', 'Please select only jpg/jpeg/png file.');
            return false;
        }
    }

    

   function compareDate() {
      $startDate = strtotime($_POST['from']);
      $endDate = strtotime($_POST['to']);

      if ($endDate <= $startDate)
        return True;
      else {
        $this->form_validation->set_message('compareDate', '%s should be greater than Contract Start Date.');
        return False;
      }
   }

   function getToken(){
        $string = "";
        $chars = "0123456789";
        for($i=0;$i<4;$i++)
        $string.=substr($chars,(rand()%(strlen($chars))), 1);
        return $string;
    }

    public function valid_password($password = '')
    {
        $password = trim($password);

        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

        if (empty($password))
        {
            $this->form_validation->set_message('valid_password', 'Password is required.');

            return FALSE;
        }

        if (preg_match_all($regex_lowercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password must have at least one lowercase letter.');

            return FALSE;
        }

        if (preg_match_all($regex_uppercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password must have at least one uppercase letter.');

            return FALSE;
        }

        if (preg_match_all($regex_number, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password must have at least one number.');

            return FALSE;
        }

        if (preg_match_all($regex_special, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password must have at least one special character.' . ' ' . htmlentities('!@#$%^&*()\-_=+{};:,<.>§~'));

            return FALSE;
        }

        if (strlen($password) < 5)
        {
            $this->form_validation->set_message('valid_password', 'Password must be at least 5 characters in length.');

            return FALSE;
        }

        if (strlen($password) > 32)
        {
            $this->form_validation->set_message('valid_password', 'Password cannot exceed 32 characters in length.');

            return FALSE;
        }

        return TRUE;
    }

    public function sendSms($to, $sms_message) {
        $this->load->library('twilio');
        //$sms_sender = trim($this->input->post('sms_sender'));
        //$sms_reciever = $this->input->post('sms_recipient');
        //$sms_message = trim($this->input->post('sms_message'));
        //$from = '+'.$sms_sender; //trial account twilio number
        //$to = '+'.$sms_reciever; //sms recipient number
        $from = 'Jobyoda';
        $to = $to;
        $response = $this->twilio->sms($from, $to, $sms_message);
        //print_r($response);
        if ($response->IsError) {
            return 'Sms Has been Not sent';
        } else {
            return 'Sms Has been sent';
        }
    }


    public function intrestedin_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        if($userTokenCheck1) {

            $userData = $this->User_Model->user_single($userTokenCheck1[0]['user_id']);
            $subcategoryArray = array();
            $subcategoryArray1 = array();
            $subcategory_list = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);

            if($subcategory_list) {
                
                foreach ($subcategory_list as $subcategory_lists) {
                    
                    if (strpos($userData[0]['jobsInterested'], $subcategory_lists['subcategory']) !== false) {
                        
                        $subcategoryArray[] = [
                              "id" => $subcategory_lists['id'],
                              "category" => $subcategory_lists['category_id'],                    
                              "subcategory" => $subcategory_lists['subcategory']
                        ];
                        $subcategoryArray1[] = [
                          "id" => $subcategory_lists['id'],
                          "category" => $subcategory_lists['category_id'],                    
                          "subcategory" => $subcategory_lists['subcategory'],
                          "selected"=>1
                        ];

                    } else {
                        $subcategoryArray1[] = [
                          "id" => $subcategory_lists['id'],
                          "category" => $subcategory_lists['category_id'],                    
                          "subcategory" => $subcategory_lists['subcategory'],
                          "selected"=>0
                        ];
                    }
                }  
            } else {
                $subcategoryArray=[];
            }

            $data = ["token" => $userData[0]['token'], "subcategorylist"=>$subcategoryArray, "subcategorylist1"=>$subcategoryArray1];

            $response = ['msg' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        
        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function intrestedinsave_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        if($userTokenCheck1) {

            $checkIntrested  = explode(',',$userData['intrested']);

            if(count($checkIntrested) > 3) {
                $errors = "You have selected 3 job categories already";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);    
            } else {
                $dataupdate = ["jobsInterested"=>$userData['intrested']];
                $userData = $this->User_Model->user_update($dataupdate, $userTokenCheck1[0]['user_id']);
                $response = ['message' => "Saved Successfully", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function skillList_get() {
        $data = $this->Common_Model->skill_lists();
        $response = ['skillList' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
    }


    public function getownstate_post() {
        $data = $this->input->post();

        if(empty($data['state'])) {
            $result = $this->User_Model->getownstate();
            $response = ['getownstate' => $result, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $result = $this->User_Model->getowncity($data['state']);
            $response = ['getowncity' => $result, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
        
        
        
    }
}
?>
