<?php
ob_start();
ini_set('display_errors', 1);
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */

class Jobpost4 extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Candidate_Model');
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('recruiter/Newpoints_Model');
        $this->load->model('recruiter/Homejob_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $key = $this->encryption->create_key(10);
    }
    // http://localhost/jobyodha/api/example/users/id/1
    

    public function userlatlong_get() {
        $users = $this->User_Model->allusers_lists();
        $x=1;
        foreach($users as $user) {

            if(strlen($user['state']) > 0) {

                if(strlen($user['location']) > 0) {

                    if(strlen($user['latitude']) <= 0) {
                    
                        $addLatLong = $this->getLatLong($user['location']);

                        if($addLatLong) {
                            $comProfile = ["latitude" => $addLatLong['latitude'], "longitude"=>$addLatLong['longitude']];
                            $this->User_Model->update_latlong($comProfile, $user['id']);
                            $x++;
                        }
                    }
                }
            }
        }

        $this->set_response($x, REST_Controller::HTTP_CREATED);
    }


    public function recruiterpassword_get() {
        $userData = $this->input->get();
        $data = ["email" => $userData['email']];
        $recruiterCheck = $this->Recruit_Model->recruiter_login($data);

        $getPass = $recruiterCheck[0]['password'];
        $vPass = $this->encryption->decrypt($getPass);

        $this->set_response($vPass, REST_Controller::HTTP_CREATED);
    }



    public function getLatLong($address){
        if(!empty($address)){
            //Formatted address
            $formattedAddr = str_replace(' ','+',$address);
            //Send request and receive json data by address
            @$geocodeFromAddr = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk'); 
            @$output = json_decode($geocodeFromAddr);
            //Get latitude and longitute from json data
            if(isset($output->results[0])) {
	            $data['latitude']  = $output->results[0]->geometry->location->lat; 
	            $data['longitude'] = $output->results[0]->geometry->location->lng;
	            //Return latitude and longitude of the given address
	            if(!empty($data)) {
	                return $data;
	            }else{
	                return false;
	            }
	        } else {
	        	return false;
	        }
        }else{
            return false;   
        }
    }

    public function homedata_post() {

        $ad_list = array();
        $companylist = array();

        $userData = $this->input->post();
        $usercurlat = $userData['cur_lat'];
        $usercurlong = $userData['cur_long'];

        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);

        if($userTokenCheck) {

            $comProfile = ["latitude" => $usercurlat, "longitude"=>$usercurlong];
            $this->User_Model->update_latlong($comProfile, $userTokenCheck[0]['user_id']);
            
            // Section 1
            $ad_list = $this->Jobpost_Model->advertise_lists($usercurlat,$usercurlong);
            if($ad_list) {
                $x=0;
                foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],
                          "companyName" =>   $ad_lists['cname']
                    ];
                    $x++;
                }
            } else {
                $ad_lists1[] = [
                        "ad_id" => '',
                        "banner" => base_url() ."images/new_banner_Man.png",
                        "description" => '',
                        "recruiter_id" => '',
                        "profilePic" => '',
                        "companyName" => ''
                ];
            }

            //section 2
            $company_lists = $this->Newpoints_Model->companysite_lists($usercurlat,$usercurlong);

            if(!empty($company_lists)) {
                $x=0;
                $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
                $userData = $this->User_Model->token_match($tokendata);
                
                $expmonth = $userData[0]['exp_month'];
                $expyear = $userData[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }
                foreach ($company_lists as $company_list) {
                    $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                    $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userTokenCheck[0]['user_id'], $company_list['id'], $expfilter, $usercurlat,$usercurlong);
                    if($jobcountfetch > 0) {
                        
                        if(strlen($company_list['companyPic']) > 0) {
                            
                            $companylist[$x] = [
                                  "id" => $company_list['id'],
                                  "image" => $company_list['companyPic'],
                                  "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                  "jobcount" => $jobcountfetch
                            ];
                        
                        } else {

                            $siteimgs = $this->Jobpost_Model->fetch_companyPic($company_list['parent_id']);
                            if($siteimgs) {
                                $companylist[$x] = [
                                      "id" => $company_list['id'],
                                      "image" => $siteimgs[0]['companyPic'],
                                      "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                      "jobcount" => $jobcountfetch
                                ];
                            }
                        }
                        $x++;
                    }
                }
            } else {
                $companylist = [];
            }

            $jobcount = array();
            foreach ($companylist as $key => $row)
            {
                $jobcount[$key] = $row['jobcount'];
            }
            array_multisort($jobcount, SORT_DESC, $companylist);

            //section 3

            $cat_lists = $this->Jobpost_Model->category_lists();

            if(!empty($cat_lists)) {
                $x=0;
                $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
                $userData = $this->User_Model->token_match($tokendata);
                $expmonth = $userData[0]['exp_month'];
                $expyear = $userData[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }
                foreach ($cat_lists as $cat_list) {
                    $jobcountfetch = $this->Newpoints_Model->category_jobcount($userTokenCheck[0]['user_id'], $cat_list['id'], $expfilter, $usercurlat, $usercurlong);
                    
                    $categorylist[$x] = [
                          "id" => $cat_list['id'],
                          "catname" => $cat_list['category'],
                          "jobcount" => $jobcountfetch
                    ];
                    $x++;
                }
            } else {
                $categorylist = [];
            }


            //  City listing

            $city_lists = $this->Jobpost_Model->homecity_lists();
            if(!empty($city_lists)) {

                $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
                $userData = $this->User_Model->token_match($tokendata);
                if($userData) {
                    $usercity = $userData[0]['city'];
                } else {
                    $usercity = "";
                }

                $c=0;
                $citylist = array();
                $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
                $userData = $this->User_Model->token_match($tokendata);
                $expmonth = $userData[0]['exp_month'];
                $expyear = $userData[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }

                //var_dump($expfilter);die;
                
                foreach ($city_lists as $city_list) {

                    if(strpos($usercity, $city_list['name']) !== false) {
                        if($city_list['name'] == "Metro Manila") {
                            $city_list['name'] = "Manila";
                        }
                        $jobcountfetch = $this->Newpoints_Model->city_jobcount($userTokenCheck[0]['user_id'], $city_list['name'], $expfilter, $usercurlat, $usercurlong);
                        if($jobcountfetch > 0) {
                            if($city_list['name'] == "Manila") {
                                $city_list['name'] = "Metro Manila";
                            }
                            $citylist[$c] = [
                                  "id" => $city_list['id'],
                                  "cityname" => $city_list['name'],
                                  "image" => $city_list['image'],
                                  "jobcount" => $jobcountfetch
                            ];
                            $c++;
                        }
                    }
                }
                if(count($citylist) > 0) {
                    $cc = $c;
                } else {
                    $cc = 0;
                }
                foreach ($city_lists as $city_list) {

                    if(strpos($usercity, $city_list['name']) !== false) {
                    } else {
                        if($city_list['name'] == "Metro Manila") {
                            $city_list['name'] = "Manila";
                        }
                        $jobcountfetch = $this->Newpoints_Model->city_jobcount($userTokenCheck[0]['user_id'], $city_list['name'], $expfilter, $usercurlat, $usercurlong);
                        if($jobcountfetch > 0) {
                            $citylist[$cc] = [
                                  "id" => $city_list['id'],
                                  "cityname" => $city_list['name'],
                                  "image" => $city_list['image'],
                                  "jobcount" => $jobcountfetch
                            ];
                            $cc++;
                        }
                    }
                }
            } else {
                $citylist = [];
            }

            //section 4, 5
            
            $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
            $userData = $this->User_Model->token_match($tokendata);

            if($userData) {
                $expmonth = $userData[0]['exp_month'];
                $expyear = $userData[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                   $expfilter = ""; 
                }

                //section 4

                    //Boost job

                $boostjobData = $this->Jobpost_Model->boostjob_fetch_latlongbylimit($userTokenCheck[0]['user_id'],$expfilter, $usercurlat,$usercurlong);
                $hotjobss = array();
                $bostArr = array();

                foreach ($boostjobData as $boostjobsData) {

                    $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                  
                    if(!empty($jobTop) && count($jobTop)>=1) {
                          //echo "</pre>";    
                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['user_id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $bostArr[] = $boostjobsData['id'];

                        $hotjobss[] = [
                                "jobpost_id" => $boostjobsData['id'],
                                "comapnyId" =>$boostjobsData['compId'],
                                "job_title" => $boostjobsData['jobtitle'],
                                "jobDesc" => $boostjobsData['jobDesc'],
                                "jobPitch"=>$boostjobsData['jobPitch'],
                                "salary" => number_format($boostjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $boostjobsData['latitude'], 
                                "longitude"=>$boostjobsData['longitude'],
                                "jobexpire"=> $boostjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$boostjobsData['boost_status'],
                                "distance" => $distance
                        ];
                    }
                }

                //Hot job

                $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlongbylimit($userTokenCheck[0]['user_id'],$expfilter, $usercurlat,$usercurlong);
                // /$hotjobss = array();

                foreach ($hotjobData as $hotjobsData) {

                    if(in_array($hotjobsData['id'], $bostArr)) {
                    } else {

                        $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                  
                        if(!empty($jobTop) && count($jobTop)>=3) {
                              //echo "</pre>";    
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $hotjobss[] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "jobPitch"=>$hotjobsData['jobPitch'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance
                            ];
                        }
                    }
                }

                // section 5

                $nearJob = $this->nearJobs1($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expmonth, $expyear);


                //  Job by leadership category

                $catjobDatas = $this->Jobpost_Model->catjob_fetch_leadership($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong);
                $leadercatjobs = array();

                foreach ($catjobDatas as $catjobData) {

                    $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
              
                    if(!empty($jobTop) && count($jobTop)>=0) {
                          //echo "</pre>";    
                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $leadercatjobs[] = [
                                "jobpost_id" => $catjobData['id'],
                                "comapnyId" =>$catjobData['compId'],
                                "job_title" => $catjobData['jobtitle'],
                                "jobDesc" => $catjobData['jobDesc'],
                                "jobPitch"=>$catjobData['jobPitch'],
                                "salary" => number_format($catjobData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $catjobData['latitude'], 
                                "longitude"=>$catjobData['longitude'],
                                "jobexpire"=> $catjobData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$catjobData['boost_status'],
                                "distance" => $distance
                        ];
                    }
                }


                //  Job by Information Technology category

                $itcatjobDatas = $this->Jobpost_Model->catjob_fetch_information_technology($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong);
                $itcatjobs = array();

                foreach ($itcatjobDatas as $itcatjobData) {

                    $jobTop = $this->Jobpost_Model->job_toppicks($itcatjobData['id']);
              
                    if(!empty($jobTop) && count($jobTop)>=0) {
                          //echo "</pre>";    
                        if(isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($itcatjobData['id'], $userTokenCheck[0]['user_id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($itcatjobData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($itcatjobData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($itcatjobData['compId']);
                        if(isset($companydetail1[0]['address'])){
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else{
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($itcatjobData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($itcatjobData['id']);
                        if(isset($jobImage[0]['pic'])){
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else{
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($itcatjobData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $itcatjobs[] = [
                                "jobpost_id" => $itcatjobData['id'],
                                "comapnyId" =>$itcatjobData['compId'],
                                "job_title" => $itcatjobData['jobtitle'],
                                "jobDesc" => $itcatjobData['jobDesc'],
                                "jobPitch"=>$itcatjobData['jobPitch'],
                                "salary" => number_format($itcatjobData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $itcatjobData['latitude'], 
                                "longitude"=>$itcatjobData['longitude'],
                                "jobexpire"=> $itcatjobData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$itcatjobData['boost_status'],
                                "distance" => $distance
                        ];
                    }
                }


                //section 6
                $featuredTopPicks = $this->getFeaturedTopPicks($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter);
            }

            $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['user_id']);  
            if($userData[0]['exp_month']>=0   && $userData[0]['exp_year']>=0){
                $exp_added = "No";
            }  else{
                $exp_added = 'Yes';
            }
            $completeness = $getCompletenes['comp'];
            if($completeness<=14 || $exp_added== 'Yes'){
                $completeness = "Yes";
            }else{
                $completeness = "No";
            }
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['user_id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['user_id']);
            $fetchjobnotifications = $this->Candidate_Model->fetchjobnotifications($userTokenCheck[0]['user_id']);


            //Fetch videos
            $videosArray = array();
            $fetchvideos = $this->Newpoints_Model->fetchvideos();
            foreach($fetchvideos as $fetchvideo) {
                $videosArray[] = ["id"=>$fetchvideo['id'],"video"=>$fetchvideo['video'],"banner"=>$fetchvideo['thumbnail'],"title"=>$fetchvideo['title'],"desc"=>$fetchvideo['description'],"posted"=>date("d F Y", strtotime($fetchvideo['updated_at']))];
            }

            //Fetch news
            $newsArray = array();
            $fetchnews = $this->Newpoints_Model->fetchnews();
            foreach($fetchnews as $fetchnew) {
                $newsArray[] = ["id"=>$fetchnew['id'],"banner"=>$fetchnew['banner'],"title"=>$fetchnew['title'],"desc"=>$fetchnew['description'],"posted"=>date("d F Y", strtotime($fetchnew['updated_at']))];
            }

            $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
            $interestedCheck = $this->User_Model->token_match($tokendata);
            if($interestedCheck) {
                if(strlen($interestedCheck[0]['jobsInterested']) > 1) {
                    $interestedHave = 1;
                } else {
                    $interestedHave = 0;
                }
            } else {
                $interestedHave = 0;
            }


            $response = ['status'=>"SUCCESS", 'section1'=>$ad_lists1, 'section2'=>$companylist, 'section3'=>$categorylist, 'citylist'=>$citylist, 'hotjobslist' => $hotjobss,'nearjob'=>$nearJob, "leadershipjobs"=>$leadercatjobs, "itjobs"=>$itcatjobs, "monthpay"=> $featuredTopPicks['monthpay'] , "shiftjobs"=> $featuredTopPicks['shiftjobs'], "retirement"=> $featuredTopPicks['retirement'], "day1hmo"=> $featuredTopPicks['day1hmo'], "freefood"=> $featuredTopPicks['freefood'], "bonus"=> $featuredTopPicks['bonus'], "workfromhome"=> $featuredTopPicks['workfromhome'], 'videos'=>$videosArray, 'news'=>$newsArray, 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }


    public function homedatanew_post() {

        $ad_list = array();
        $companylist = array();

        $userData = $this->input->post();
        $usercurlat = $userData['cur_lat'];
        $usercurlong = $userData['cur_long'];

        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match1($data);

        if($userTokenCheck) {

            $comProfile = ["latitude" => $usercurlat, "longitude"=>$usercurlong];
            $this->User_Model->update_latlong($comProfile, $userTokenCheck[0]['user_id']);
            
            // Section 1
            $ad_list = $this->Jobpost_Model->advertise_lists($usercurlat,$usercurlong);
            if($ad_list) {
                $x=0;
                foreach ($ad_list as $ad_lists) {
                    $ad_lists1[$x] = [
                          "ad_id" => $ad_lists['id'],
                          "banner" => $ad_lists['banner'],
                          "description" => $ad_lists['description'],
                          "recruiter_id" => $ad_lists['site_id'],
                          "profilePic" => $ad_lists['companyPic'],
                          "companyName" =>   $ad_lists['cname']
                    ];
                    $x++;
                }
            } else {
                $ad_lists1[] = [
                        "ad_id" => '',
                        "banner" => base_url() ."images/new_banner_Man.png",
                        "description" => '',
                        "recruiter_id" => '',
                        "profilePic" => '',
                        "companyName" => ''
                ];
            }

            $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
            $userData = $this->User_Model->token_match($tokendata);
            
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }

            //section 2
            $company_lists = $this->Newpoints_Model->companysite_lists($usercurlat,$usercurlong);

            if(!empty($company_lists)) {
                $x=0;
                
                $companyidsarray = array();
                foreach ($company_lists as $company_list) {
                    $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                    $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userTokenCheck[0]['user_id'], $company_list['id'], $expfilter, $usercurlat,$usercurlong);
                    if($jobcountfetch > 0) {
                        
                        if(strlen($company_list['companyPic']) > 0) {
                            
                            $companylist[$x] = [
                                  "id" => $company_list['id'],
                                  "image" => $company_list['companyPic'],
                                  "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                  "jobcount" => $jobcountfetch,
                                  "parent_id"=>$company_list['parent_id']
                            ];
                        
                        } else {

                            $siteimgs = $this->Jobpost_Model->fetch_companyPic($company_list['parent_id']);
                            if($siteimgs) {
                                $companylist[$x] = [
                                      "id" => $company_list['id'],
                                      "image" => $siteimgs[0]['companyPic'],
                                      "cname" => $parent[0]['cname'].', '.$company_list['cname'],
                                      "jobcount" => $jobcountfetch,
                                      "parent_id"=>$company_list['parent_id']
                                ];
                            }
                        }
                        $companyidsarray[] = $company_list['parent_id'];
                        $x++;
                    }
                }
            } else {
                $companylist = [];
            }
            $companyidsarray = array_unique($companyidsarray);

            $jobcount = array();
            foreach ($companylist as $key => $row)
            {
                $jobcount[$key] = $row['jobcount'];
            }
            array_multisort($jobcount, SORT_DESC, $companylist);

            //section 3

            $cat_lists = $this->Jobpost_Model->category_lists();
            if(!empty($cat_lists)) {
                $x=0;
                foreach ($cat_lists as $cat_list) {
                    $jobcountfetch = $this->Newpoints_Model->category_jobcount($userTokenCheck[0]['user_id'], $cat_list['id'], $expfilter, $usercurlat, $usercurlong);
                    
                    $categorylist[$x] = [
                          "id" => $cat_list['id'],
                          "catname" => $cat_list['category'],
                          "jobcount" => $jobcountfetch
                    ];
                    $x++;
                }
            } else {
                $categorylist = [];
            }


            //  City listing

            $city_lists = $this->Jobpost_Model->homecity_lists();
            if(!empty($city_lists)) {

                $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
                $userData = $this->User_Model->token_match($tokendata);
                if($userData) {
                    $usercity = $userData[0]['city'];
                } else {
                    $usercity = "";
                }

                $c=0;
                $citylist = array();

                foreach ($city_lists as $city_list) {

                    if(strpos($usercity, $city_list['name']) !== false) {
                        if($city_list['name'] == "Metro Manila") {
                            $city_list['name'] = "Manila";
                        }
                        $jobcountfetch = $this->Newpoints_Model->city_jobcount($userTokenCheck[0]['user_id'], $city_list['name'], $expfilter, $usercurlat, $usercurlong);
                        if($jobcountfetch > 0) {
                            if($city_list['name'] == "Manila") {
                                $city_list['name'] = "Metro Manila";
                            }
                            $citylist[$c] = [
                                  "id" => $city_list['id'],
                                  "cityname" => $city_list['name'],
                                  "image" => $city_list['image'],
                                  "jobcount" => $jobcountfetch
                            ];
                            $c++;
                        }
                    }
                }
                if(count($citylist) > 0) {
                    $cc = $c;
                } else {
                    $cc = 0;
                }
                foreach ($city_lists as $city_list) {

                    if(strpos($usercity, $city_list['name']) !== false) {
                    } else {
                        if($city_list['name'] == "Metro Manila") {
                            $city_list['name'] = "Manila";
                        }
                        $jobcountfetch = $this->Newpoints_Model->city_jobcount($userTokenCheck[0]['user_id'], $city_list['name'], $expfilter, $usercurlat, $usercurlong);
                        if($jobcountfetch > 0) {
                            $citylist[$cc] = [
                                  "id" => $city_list['id'],
                                  "cityname" => $city_list['name'],
                                  "image" => $city_list['image'],
                                  "jobcount" => $jobcountfetch
                            ];
                            $cc++;
                        }
                    }
                }
            } else {
                $citylist = [];
            }

            //section 4, 5

                //section 4

                $hotjobss = array();
                $bostArr = array();
                $neglectArr = array();

                // Company hot jobs

                foreach($companyidsarray as $companyidsarr) {

                    $hotjobData = $this->Homejob_Model->hotjob_fetch_latlongbylimit_groupby($userTokenCheck[0]['user_id'],$expfilter, $usercurlat,$usercurlong, $companyidsarr);
                    foreach ($hotjobData as $hotjobsData) {

                        $neglectArr[] = $hotjobsData['id'];
                        $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                        if(!empty($jobTop) && count($jobTop)>=1) {  
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            } else {
                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $hotjobss[] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "jobPitch"=>$hotjobsData['jobPitch'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance
                            ];
                        }
                    }
                }
                usort($hotjobss, function($a, $b) {
                    return $a['distance'] <=> $b['distance'];
                });
                    //Boost job

                if(count($hotjobss) <= 8) {
                    $zz=1;
                    $boostjobData = $this->Jobpost_Model->boostjob_fetch_latlongbylimit($userTokenCheck[0]['user_id'],$expfilter, $usercurlat,$usercurlong);
                    foreach ($boostjobData as $boostjobsData) {

                        if(is_array($boostjobsData['id'], $neglectArr)) { } else {

                            if($zz <= 2) {
                                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                                if(!empty($jobTop) && count($jobTop)>=1) {
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['user_id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }
                                    else{
                                        $companyaddress='';
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    } else {
                                        $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                        if(!empty($companyPic[0]['companyPic'])) {
                                            $comPic = trim($companyPic[0]['companyPic']);
                                        } else{
                                            $comPic = "";
                                        }
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $bostArr[] = $boostjobsData['id'];

                                    $hotjobss[] = [
                                            "jobpost_id" => $boostjobsData['id'],
                                            "comapnyId" =>$boostjobsData['compId'],
                                            "job_title" => $boostjobsData['jobtitle'],
                                            "jobDesc" => $boostjobsData['jobDesc'],
                                            "jobPitch"=>$boostjobsData['jobPitch'],
                                            "salary" => number_format($boostjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $boostjobsData['latitude'], 
                                            "longitude"=>$boostjobsData['longitude'],
                                            "jobexpire"=> $boostjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$boostjobsData['boost_status'],
                                            "distance" => $distance
                                    ];
                                }
                            }
                            $zz++;
                        }
                    }

                    //Hot job
                    $zzz=1;
                    $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlongbylimit($userTokenCheck[0]['user_id'],$expfilter, $usercurlat,$usercurlong);
                    foreach ($hotjobData as $hotjobsData) {

                        if(in_array($hotjobsData['id'], $bostArr)) {
                        } else {

                            if($zzz <= 2){
                                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                                if(!empty($jobTop) && count($jobTop)>=1) {  
                                    if(isset($jobTop[0]['picks_id'])) {
                                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                                    } else{
                                        $datatoppicks1 = "";
                                    }
                                    if(isset($jobTop[1]['picks_id'])) {
                                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTop[2]['picks_id'])) {
                                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['user_id']);
                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                    if(isset($companydetail1[0]['address'])){
                                        $companyaddress=$companydetail1[0]['address'];
                                    }
                                    else{
                                        $companyaddress='';
                                    }

                                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                    if(isset($jobImage[0]['pic'])){
                                        $jobImageData=$jobImage[0]['pic'];
                                    } else {
                                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                        if(!empty($companyPic[0]['companyPic'])) {
                                            $comPic = trim($companyPic[0]['companyPic']);
                                        } else{
                                            $comPic = "";
                                        }
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $hotjobss[] = [
                                            "jobpost_id" => $hotjobsData['id'],
                                            "comapnyId" =>$hotjobsData['compId'],
                                            "job_title" => $hotjobsData['jobtitle'],
                                            "jobDesc" => $hotjobsData['jobDesc'],
                                            "jobPitch"=>$hotjobsData['jobPitch'],
                                            "salary" => number_format($hotjobsData['salary']),
                                            "companyName"=> $companydetail[0]["cname"],
                                            "cname"=> $cname,
                                            "companyAddress"=>$companyaddress,
                                            "toppicks1" => $datatoppicks1,
                                            "toppicks2" => $datatoppicks2,
                                            "toppicks3" => $datatoppicks3,
                                            "job_image" =>$jobImageData,
                                            "latitude"=> $hotjobsData['latitude'], 
                                            "longitude"=>$hotjobsData['longitude'],
                                            "jobexpire"=> $hotjobsData['jobexpire'],
                                            "companyPic"=> $comPic,
                                            "savedjob" =>$savedjob,
                                            "boostjob" =>$hotjobsData['boost_status'],
                                            "distance" => $distance
                                    ];
                                }
                                $zzz++;
                            }
                        }
                    }
                }

                // section 5

                $nearJob = $this->nearJobs2($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter, $companyidsarray);


                //  Job by leadership category

                $leadercatjobs = array();
                $negletarr = array();

                foreach($companyidsarray as $companyidsarr) {
                    $catjobDatas = $this->Homejob_Model->catjob_fetch_leadership($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong, $companyidsarr);
                    foreach ($catjobDatas as $catjobData) {
                        $negletarr[] = $catjobData['id'];
                        $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
                        if(!empty($jobTop) && count($jobTop)>=0) {
                              //echo "</pre>";    
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $leadercatjobs[] = [
                                    "jobpost_id" => $catjobData['id'],
                                    "comapnyId" =>$catjobData['compId'],
                                    "job_title" => $catjobData['jobtitle'],
                                    "jobDesc" => $catjobData['jobDesc'],
                                    "jobPitch"=>$catjobData['jobPitch'],
                                    "salary" => number_format($catjobData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $catjobData['latitude'], 
                                    "longitude"=>$catjobData['longitude'],
                                    "jobexpire"=> $catjobData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$catjobData['boost_status'],
                                    "distance" => $distance
                            ];
                        }
                    }
                }
                usort($leadercatjobs, function($a, $b) {
                    return $a['distance'] <=> $b['distance'];
                });
                $catjobDatas = $this->Jobpost_Model->catjob_fetch_leadership($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong);
                foreach ($catjobDatas as $catjobData) {

                    if(is_array($catjobData['id'], $negletarr)) { } else {
                        $jobTop = $this->Jobpost_Model->job_toppicks($catjobData['id']);
                        if(!empty($jobTop) && count($jobTop)>=0) {
                              //echo "</pre>";    
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($catjobData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($catjobData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($catjobData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($catjobData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($catjobData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($catjobData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($catjobData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $leadercatjobs[] = [
                                    "jobpost_id" => $catjobData['id'],
                                    "comapnyId" =>$catjobData['compId'],
                                    "job_title" => $catjobData['jobtitle'],
                                    "jobDesc" => $catjobData['jobDesc'],
                                    "jobPitch"=>$catjobData['jobPitch'],
                                    "salary" => number_format($catjobData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $catjobData['latitude'], 
                                    "longitude"=>$catjobData['longitude'],
                                    "jobexpire"=> $catjobData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$catjobData['boost_status'],
                                    "distance" => $distance
                            ];
                        }
                    }
                }


                //  Job by Information Technology category
                $itcatjobs = array();
                $negletarr = array();

                foreach($companyidsarray as $companyidsarr) {

                    $itcatjobDatas = $this->Homejob_Model->catjob_fetch_information_technology($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong, $companyidsarr);
                    foreach ($itcatjobDatas as $itcatjobData) {
                        $negletarr[] = $itcatjobData['id'];
                        $jobTop = $this->Jobpost_Model->job_toppicks($itcatjobData['id']);
                        if(!empty($jobTop) && count($jobTop)>=0) {
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($itcatjobData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($itcatjobData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($itcatjobData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($itcatjobData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($itcatjobData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($itcatjobData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($itcatjobData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $itcatjobs[] = [
                                    "jobpost_id" => $itcatjobData['id'],
                                    "comapnyId" =>$itcatjobData['compId'],
                                    "job_title" => $itcatjobData['jobtitle'],
                                    "jobDesc" => $itcatjobData['jobDesc'],
                                    "jobPitch"=>$itcatjobData['jobPitch'],
                                    "salary" => number_format($itcatjobData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $itcatjobData['latitude'], 
                                    "longitude"=>$itcatjobData['longitude'],
                                    "jobexpire"=> $itcatjobData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$itcatjobData['boost_status'],
                                    "distance" => $distance
                            ];
                        }
                    }
                }
                usort($itcatjobs, function($a, $b) {
                    return $a['distance'] <=> $b['distance'];
                });
                $itcatjobDatas = $this->Jobpost_Model->catjob_fetch_information_technology($userTokenCheck[0]['user_id'], $expfilter, $usercurlat,$usercurlong);
                foreach ($itcatjobDatas as $itcatjobData) {
                    if(is_array($itcatjobData['id'], $negletarr)) { } else {
                        $jobTop = $this->Jobpost_Model->job_toppicks($itcatjobData['id']);
                        if(!empty($jobTop) && count($jobTop)>=0) {
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($itcatjobData['id'], $userTokenCheck[0]['user_id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($itcatjobData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($itcatjobData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($itcatjobData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($itcatjobData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($itcatjobData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($itcatjobData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($usercurlat,$usercurlong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $itcatjobs[] = [
                                    "jobpost_id" => $itcatjobData['id'],
                                    "comapnyId" =>$itcatjobData['compId'],
                                    "job_title" => $itcatjobData['jobtitle'],
                                    "jobDesc" => $itcatjobData['jobDesc'],
                                    "jobPitch"=>$itcatjobData['jobPitch'],
                                    "salary" => number_format($itcatjobData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $itcatjobData['latitude'], 
                                    "longitude"=>$itcatjobData['longitude'],
                                    "jobexpire"=> $itcatjobData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$itcatjobData['boost_status'],
                                    "distance" => $distance
                            ];
                        }
                    }
                }


                //section 6
                $featuredTopPicks = $this->getFeaturedTopPicks1($userTokenCheck[0]['user_id'], $usercurlat, $usercurlong, $expfilter, $companyidsarray);


            $getCompletenes = $this->fetchUserCompleteData($userTokenCheck[0]['user_id']);  
            if($userData[0]['exp_month']>=0   && $userData[0]['exp_year']>=0){
                $exp_added = "No";
            }  else{
                $exp_added = 'Yes';
            }
            $completeness = $getCompletenes['comp'];
            if($completeness<=14 || $exp_added== 'Yes'){
                $completeness = "Yes";
            }else{
                $completeness = "No";
            }
            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['user_id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['user_id']);
            $fetchjobnotifications = $this->Candidate_Model->fetchjobnotifications($userTokenCheck[0]['user_id']);


            //Fetch videos
            $videosArray = array();
            $fetchvideos = $this->Newpoints_Model->fetchvideos();
            foreach($fetchvideos as $fetchvideo) {
                $videosArray[] = ["id"=>$fetchvideo['id'],"video"=>$fetchvideo['video'],"banner"=>$fetchvideo['thumbnail'],"title"=>$fetchvideo['title'],"desc"=>$fetchvideo['description'],"posted"=>date("d F Y", strtotime($fetchvideo['updated_at']))];
            }

            //Fetch news
            $newsArray = array();
            $fetchnews = $this->Newpoints_Model->fetchnews();
            foreach($fetchnews as $fetchnew) {
                $newsArray[] = ["id"=>$fetchnew['id'],"banner"=>$fetchnew['banner'],"title"=>$fetchnew['title'],"desc"=>$fetchnew['description'],"posted"=>date("d F Y", strtotime($fetchnew['updated_at']))];
            }

            $tokendata = ['id'=>$userTokenCheck[0]['user_id']];
            $interestedCheck = $this->User_Model->token_match($tokendata);
            if($interestedCheck) {
                if(strlen($interestedCheck[0]['jobsInterested']) > 1) {
                    $interestedHave = 1;
                } else {
                    $interestedHave = 0;
                }
            } else {
                $interestedHave = 0;
            }


            $response = ['status'=>"SUCCESS", 'section1'=>$ad_lists1, 'section2'=>$companylist, 'section3'=>$categorylist, 'citylist'=>$citylist, 'hotjobslist' => $hotjobss,'nearjob'=>$nearJob, "leadershipjobs"=>$leadercatjobs, "itjobs"=>$itcatjobs, "monthpay"=> $featuredTopPicks['monthpay'] , "shiftjobs"=> $featuredTopPicks['shiftjobs'], "retirement"=> $featuredTopPicks['retirement'], "day1hmo"=> $featuredTopPicks['day1hmo'], "freefood"=> $featuredTopPicks['freefood'], "bonus"=> $featuredTopPicks['bonus'], "workfromhome"=> $featuredTopPicks['workfromhome'], 'videos'=>$videosArray, 'news'=>$newsArray, 'completeness'=>$completeness,'notification_count' => count($fetchnotifications)+count($fetchpromonotifications)+count($fetchjobnotifications), "interestedHave"=>$interestedHave];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function cityListings_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        $userLat = $userData['cur_lat'];
        $userLong = $userData['cur_long'];
        $userCity = $userData['city'];
        
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            
            if($userTokenCheck) {
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }
            }

            $boostjobData = $this->Jobpost_Model->cityjob_fetch($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $userCity);

            $data1 = array();
            $data1=[];
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                if(!empty($jobTop) && count($jobTop)>=0) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                    ];
                }
            }

            $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
            $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
            $response = ['cityjobslist' => $data1,'notification_count' =>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }


    public function viewallListings_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        $userLat = $userData['cur_lat'];
        $userLong = $userData['cur_long'];
        
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            
            if($userTokenCheck) {
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }

                if($userData['type'] == "hotjob") {

                    $boostjobData = $this->Jobpost_Model->boostjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong);
                    
                    //var_dump(expression)

                    $data1 = array();
                    $data1["hotjobss"]=[];
                    $bostArr = array();

                    foreach ($boostjobData as $boostjobsData) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                      
                        if(!empty($jobTop) && count($jobTop)>=1) {
                              //echo "</pre>";    
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $bostArr[] = $boostjobsData['id'];

                            $data1["hotjobss"][] = [
                                    "jobpost_id" => $boostjobsData['id'],
                                    "comapnyId" =>$boostjobsData['compId'],
                                    "job_title" => $boostjobsData['jobtitle'],
                                    "jobDesc" => $boostjobsData['jobDesc'],
                                    "jobPitch"=>$boostjobsData['jobPitch'],
                                    "salary" => number_format($boostjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $boostjobsData['latitude'], 
                                    "longitude"=>$boostjobsData['longitude'],
                                    "jobexpire"=> $boostjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$boostjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $boostjobsData['mode'],
                                    "modeurl"=> $boostjobsData['modeurl'],
                            ];
                        }
                    }
                    $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong);

                    foreach ($hotjobData as $hotjobsData) {

                        if(in_array($hotjobsData['id'], $bostArr)) {
                        } else {

                            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                        
                            if(!empty($jobTop) && count($jobTop)>1) {

                                  //echo "</pre>";    
                                if(isset($jobTop[0]['picks_id'])){
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else {
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else {
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else {
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                 $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if(isset($companydetail1[0]['address']))
                                {
                                    $companyaddress=$companydetail1[0]['address'];
                                }
                                else
                                {
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                
                                
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                                if(isset($jobImage[0]['pic']))
                                {
                                    $jobImageData=$jobImage[0]['pic'];
                                }
                                else
                                {
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $data1["hotjobss"][] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $hotjobsData['mode'], 
                                ];
                            }
                        }
                    }
                
                } else if($userData['type'] == "company") {

                    $company_lists = $this->Newpoints_Model->companysite_lists($userLat, $userLong);
                    if(!empty($company_lists)) {
                        $x=0;
                        $tokendata = ['id'=>$userTokenCheck[0]['id']];
                        $userData = $this->User_Model->token_match($tokendata);
                        $expmonth = $userData[0]['exp_month'];
                        $expyear = $userData[0]['exp_year'];

                        if($expyear == 0 && $expmonth == 0) {
                            $expfilter = "1";
                        }else if($expyear == 0 && $expmonth < 6) {
                            $expfilter = "2";
                        } else if($expyear < 1 && $expmonth >= 6) {
                            $expfilter = "3";
                        } else if($expyear < 2 && $expyear >= 1) {
                            $expfilter = "4";
                        } else if($expyear < 3 && $expyear >= 2) {
                            $expfilter = "5";
                        }else if($expyear < 4 && $expyear >= 3) {
                            $expfilter = "6";
                        }else if($expyear < 5 && $expyear >= 4) {
                            $expfilter = "7";
                        }else if($expyear < 6 && $expyear >= 5) {
                            $expfilter = "8";
                        }else if($expyear < 7 && $expyear >= 6) {
                            $expfilter = "9";
                        } else if($expyear >= 7) { 
                            $expfilter = "10";
                        } else {
                            $expfilter = "";
                        }
                        foreach ($company_lists as $company_list) {
                            $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userTokenCheck[0]['id'], $company_list['id']);
                            if($jobcountfetch > 0) {
                                if(strlen($company_list['companyPic']) > 0) {
                                    $companylist[$x] = [
                                          "id" => $company_list['id'],
                                          "image" => $company_list['companyPic'],
                                          "cname" => $company_list['cname'],
                                          "jobcount" => $jobcountfetch
                                    ];
                                    $x++;
                                }
                            }
                        }
                    } else {
                        $companylist = [];
                    }

                    $jobcount = array();
                    foreach ($companylist as $key => $row)
                    {
                        $jobcount[$key] = $row['jobcount'];
                    }
                    array_multisort($jobcount, SORT_DESC, $companylist);
                    $data1["hotjobss"] = $companylist;


                } else if($userData['type'] == "category") {
                    $hotjobData = $this->Newpoints_Model->categoryjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userData['typeid'], $userData['cur_lat'], $userData['cur_long']);
                    $data1 = array();
                    $data1["hotjobss"]=[];
                    foreach ($hotjobData as $hotjobsData) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
 
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else {
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else {
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else {
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                         $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address']))
                        {
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else
                        {
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                        if(isset($jobImage[0]['pic']))
                        {
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else
                        {
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1["hotjobss"][] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "salary" => number_format($hotjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $hotjobsData['mode'],
                            "modeurl"=> $hotjobsData['modeurl'],
                        ];
                    }
                
                } else if($userData['type'] == "nearby") {
                
                    $hotjobData = $this->Newpoints_Model->nearbyjob_fetch_latlong($userTokenCheck[0]['id'], $userLat, $userLong, $expfilter);
                    $data1 = array();
                    $data1["hotjobss"]=[];
                    foreach ($hotjobData as $hotjobsData) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
 
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else {
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else {
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else {
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                         $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address']))
                        {
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else
                        {
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                        if(isset($jobImage[0]['pic']))
                        {
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else
                        {
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1["hotjobss"][] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "salary" => number_format($hotjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $hotjobsData['mode'],
                            "modeurl"=> $hotjobsData['modeurl'],
                        ];
                    }

                } else if($userData['type'] == "toppicks") {
                    $hotjobData = $this->Newpoints_Model->feturedtoppicks_fetch_latlong($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong);

                    $data1 = array();
                    $data1["hotjobss"]=[];

                    foreach ($hotjobData as $hotjobsData) {

                        $jobTop = $this->Jobpost_Model->job_toppicks1($hotjobsData['id']);
                        $pids = array(); 
                        foreach($jobTop as $jobTopp) {
                            $pids[] = $jobTopp['picks_id'];
                        }

                        if(in_array($userData['typeid'], $pids)) {

                            if(!empty($jobTop) && count($jobTop)>=1) {

                                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $userData['typeid']);
                                $datatoppicks1 = $userData['typeid']; 
                                
                                if(isset($jobTopseleted[0]['picks_id'])) {
                                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTopseleted[1]['picks_id'])) {
                                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if(isset($companydetail1[0]['address']))
                                {
                                    $companyaddress=$companydetail1[0]['address'];
                                }
                                else
                                {
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                                if(isset($jobImage[0]['pic']))
                                {
                                    $jobImageData=$jobImage[0]['pic'];
                                }
                                else
                                {
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $data1["hotjobss"][] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $hotjobsData['mode'],
                                    "modeurl"=> $hotjobsData['modeurl'],
                                ];
                            }
                        }
                    }
                
                } else if($userData['type'] == "Allowance") {
                    $hotjobData = $this->Newpoints_Model->feturedtoppicks_fetch_latlong($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong);

                    //var_dump($hotjobData);die;

                    $data1 = array();
                    $data1["hotjobss"]=[];
                    foreach ($hotjobData as $hotjobsData) {

                        $jobTop = $this->Jobpost_Model->job_allowancesbyid($hotjobsData['id'], $userData['typeid']);
                        
                        if(!empty($jobTop) && count($jobTop)>=1) {

                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else {
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else {
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else {
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                             $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if(isset($companydetail1[0]['address']))
                            {
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else
                            {
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            
                            
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                            if(isset($jobImage[0]['pic']))
                            {
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else
                            {
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $data1["hotjobss"][] = [
                                "jobpost_id" => $hotjobsData['id'],
                                "comapnyId" =>$hotjobsData['compId'],
                                "job_title" => $hotjobsData['jobtitle'],
                                "jobDesc" => $hotjobsData['jobDesc'],
                                "salary" => number_format($hotjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $hotjobsData['latitude'], 
                                "longitude"=>$hotjobsData['longitude'],
                                "jobexpire"=> $hotjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$hotjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $hotjobsData['mode'],
                                "modeurl"=> $hotjobsData['modeurl'],
                            ];
                        }
                    }
                } else if($userData['type'] == "leadership") {

                    $boostjobData = $this->Jobpost_Model->catjob_fetch_leadership_all($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong);

                    $data1 = array();
                    $data1["hotjobss"]=[];
                    $bostArr = array();

                    foreach ($boostjobData as $boostjobsData) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                      
                        if(!empty($jobTop) && count($jobTop)>=0) {
  
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $bostArr[] = $boostjobsData['id'];

                            $data1["hotjobss"][] = [
                                    "jobpost_id" => $boostjobsData['id'],
                                    "comapnyId" =>$boostjobsData['compId'],
                                    "job_title" => $boostjobsData['jobtitle'],
                                    "jobDesc" => $boostjobsData['jobDesc'],
                                    "jobPitch"=>$boostjobsData['jobPitch'],
                                    "salary" => number_format($boostjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $boostjobsData['latitude'], 
                                    "longitude"=>$boostjobsData['longitude'],
                                    "jobexpire"=> $boostjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$boostjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $boostjobsData['mode'], 
                                    "modeurl"=> $boostjobsData['modeurl'], 
                            ];
                        }
                    }

                }  else if($userData['type'] == "information") {

                    $boostjobData = $this->Jobpost_Model->catjob_fetch_information_technology_all($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong);
                    $data1 = array();
                    $data1["hotjobss"]=[];
                    $bostArr = array();

                    foreach ($boostjobData as $boostjobsData) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                      
                        if(!empty($jobTop) && count($jobTop)>=0) {
  
                            if(isset($jobTop[0]['picks_id'])) {
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                            if(isset($companydetail1[0]['address'])){
                                $companyaddress=$companydetail1[0]['address'];
                            }else{
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                            if(isset($jobImage[0]['pic'])){
                                $jobImageData=$jobImage[0]['pic'];
                            }else{
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $bostArr[] = $boostjobsData['id'];

                            $data1["hotjobss"][] = [
                                    "jobpost_id" => $boostjobsData['id'],
                                    "comapnyId" =>$boostjobsData['compId'],
                                    "job_title" => $boostjobsData['jobtitle'],
                                    "jobDesc" => $boostjobsData['jobDesc'],
                                    "jobPitch"=>$boostjobsData['jobPitch'],
                                    "salary" => number_format($boostjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $boostjobsData['latitude'], 
                                    "longitude"=>$boostjobsData['longitude'],
                                    "jobexpire"=> $boostjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$boostjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $boostjobsData['mode'], 
                                    "modeurl"=> $boostjobsData['modeurl'], 
                            ];
                        }
                    }
                }

                    
                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
                $response = ['hotjobslist' => $data1,'notification_count' =>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function viewallListingsnew_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        $userLat = $userData['cur_lat'];
        $userLong = $userData['cur_long'];

        $company_lists = $this->Newpoints_Model->companysite_lists($userLat,$userLong);
        if(!empty($company_lists)) {
            $x=0;
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }

            $companyidsarray = array();
            foreach ($company_lists as $company_list) {
                $parent = $this->Newpoints_Model->company_parent($company_list['parent_id']);

                $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userTokenCheck[0]['user_id'], $company_list['id'], $expfilter, $userLat,$userLong);
                if($jobcountfetch > 0) {
                    
                    $companyidsarray[] = $company_list['parent_id'];
                    $x++;
                }
            }
        } else {
            $companylist = [];
        }
        $companyidsarray = array_unique($companyidsarray);

        
        if($userTokenCheck1) {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            
            if($userTokenCheck) {
                $expmonth = $userTokenCheck[0]['exp_month'];
                $expyear = $userTokenCheck[0]['exp_year'];

                if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                } else if($expyear >= 7) { 
                    $expfilter = "10";
                } else {
                    $expfilter = "";
                }

                $data1 = array();
                //$data1["hotjobss"]=[];
                $negletarr = array();

                if($userData['type'] == "hotjob") {

                    $bostArr = array();

                    foreach($companyidsarray as $companyidsarr) {
                        $hotjobData = $this->Homejob_Model->hotjob_fetch_latlongbylimit_groupby($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong, $companyidsarr);
                        foreach ($hotjobData as $hotjobsData) {

                            $negletarr[] = $hotjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                            if(!empty($jobTop) && count($jobTop)>1) {
                                if(isset($jobTop[0]['picks_id'])){
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else {
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else {
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else {
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                 $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if(isset($companydetail1[0]['address']))
                                {
                                    $companyaddress=$companydetail1[0]['address'];
                                }
                                else
                                {
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                
                                
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                                if(isset($jobImage[0]['pic']))
                                {
                                    $jobImageData=$jobImage[0]['pic'];
                                }
                                else
                                {
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $data1[] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $hotjobsData['mode'], 
                                ];
                            }
                            
                        }
                    }

                    usort($data1, function($a, $b) {
                        return $a['distance'] <=> $b['distance'];
                    });

                    $boostjobData = $this->Jobpost_Model->boostjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong);
                    foreach ($boostjobData as $boostjobsData) {

                        if(is_array($boostjobsData['id'], $negletarr)) { } else {
                            $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                            if(!empty($jobTop) && count($jobTop)>=1) {
                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }
                                else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }
                                else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $bostArr[] = $boostjobsData['id'];

                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'],
                                        "modeurl"=> $boostjobsData['modeurl'],
                                ];
                            }
                        }
                    }

                    $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong);
                    foreach ($hotjobData as $hotjobsData) {

                        if(in_array($hotjobsData['id'], $bostArr)) {
                        } else {
                            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                            if(!empty($jobTop) && count($jobTop)>1) {

                                  //echo "</pre>";    
                                if(isset($jobTop[0]['picks_id'])){
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else {
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else {
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else {
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                 $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if(isset($companydetail1[0]['address']))
                                {
                                    $companyaddress=$companydetail1[0]['address'];
                                }
                                else
                                {
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                
                                
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                                if(isset($jobImage[0]['pic']))
                                {
                                    $jobImageData=$jobImage[0]['pic'];
                                }
                                else
                                {
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $data1[] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $hotjobsData['mode'], 
                                ];
                            }
                        }
                    }
                
                } else if($userData['type'] == "company") {

                    $company_lists = $this->Newpoints_Model->companysite_lists($userLat, $userLong);
                    if(!empty($company_lists)) {
                        $x=0;
                        $tokendata = ['id'=>$userTokenCheck[0]['id']];
                        $userData = $this->User_Model->token_match($tokendata);
                        $expmonth = $userData[0]['exp_month'];
                        $expyear = $userData[0]['exp_year'];

                        if($expyear == 0 && $expmonth == 0) {
                            $expfilter = "1";
                        }else if($expyear == 0 && $expmonth < 6) {
                            $expfilter = "2";
                        } else if($expyear < 1 && $expmonth >= 6) {
                            $expfilter = "3";
                        } else if($expyear < 2 && $expyear >= 1) {
                            $expfilter = "4";
                        } else if($expyear < 3 && $expyear >= 2) {
                            $expfilter = "5";
                        }else if($expyear < 4 && $expyear >= 3) {
                            $expfilter = "6";
                        }else if($expyear < 5 && $expyear >= 4) {
                            $expfilter = "7";
                        }else if($expyear < 6 && $expyear >= 5) {
                            $expfilter = "8";
                        }else if($expyear < 7 && $expyear >= 6) {
                            $expfilter = "9";
                        } else if($expyear >= 7) { 
                            $expfilter = "10";
                        } else {
                            $expfilter = "";
                        }
                        foreach ($company_lists as $company_list) {
                            $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userTokenCheck[0]['id'], $company_list['id']);
                            if($jobcountfetch > 0) {
                                if(strlen($company_list['companyPic']) > 0) {
                                    $companylist[$x] = [
                                          "id" => $company_list['id'],
                                          "image" => $company_list['companyPic'],
                                          "cname" => $company_list['cname'],
                                          "jobcount" => $jobcountfetch
                                    ];
                                    $x++;
                                }
                            }
                        }
                    } else {
                        $companylist = [];
                    }

                    $jobcount = array();
                    foreach ($companylist as $key => $row)
                    {
                        $jobcount[$key] = $row['jobcount'];
                    }
                    array_multisort($jobcount, SORT_DESC, $companylist);
                    $data1["hotjobss"] = $companylist;


                } else if($userData['type'] == "category") {

                    $hotjobData = $this->Newpoints_Model->categoryjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $userData['typeid'], $userData['cur_lat'], $userData['cur_long']);
                    foreach ($hotjobData as $hotjobsData) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
 
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else {
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else {
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else {
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);
                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address']))
                        {
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else
                        {
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if(isset($jobImage[0]['pic'])) {
                            $jobImageData=$jobImage[0]['pic'];
                        } else {
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1[] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "salary" => number_format($hotjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $hotjobsData['mode'],
                            "modeurl"=> $hotjobsData['modeurl'],
                        ];
                    }
                
                } else if($userData['type'] == "nearby") {

                    foreach($companyidsarray as $companyidsarr) {
                
                        $hotjobData = $this->Homejob_Model->job_fetch_homeforappnearby_groupby($userTokenCheck[0]['id'], $userLat, $userLong, $expfilter, $companyidsarr);
                        foreach ($hotjobData as $hotjobsData) {
                            $negletarr[] = $hotjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else {
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else {
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else {
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);
                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if(isset($companydetail1[0]['address'])) {
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else {
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if(isset($jobImage[0]['pic'])) {
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else {
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $data1[] = [
                                "jobpost_id" => $hotjobsData['id'],
                                "recruiter_id" => $hotjobsData['recruiter_id'],
                                "comapnyId" =>$hotjobsData['compId'],
                                "job_title" => $hotjobsData['jobtitle'],
                                "jobDesc" => $hotjobsData['jobDesc'],
                                "salary" => number_format($hotjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $hotjobsData['latitude'], 
                                "longitude"=>$hotjobsData['longitude'],
                                "jobexpire"=> $hotjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$hotjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $hotjobsData['mode'],
                                "modeurl"=> $hotjobsData['modeurl'],
                            ];
                        }    
                    }

                    usort($data1, function($a, $b) {
                        return $a['distance'] <=> $b['distance'];
                    });

                    $hotjobData = $this->Newpoints_Model->nearbyjob_fetch_latlong($userTokenCheck[0]['id'], $userLat, $userLong, $expfilter);
                    foreach ($hotjobData as $hotjobsData) {

                        if(is_array($hotjobsData['id'], $negletarr)) { } else {
                        
                            $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id']; 
                            } else {
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $datatoppicks2 = $jobTop[1]['picks_id']; 
                            } else {
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])) {
                                $datatoppicks3 = $jobTop[2]['picks_id']; 
                            } else {
                                $datatoppicks3 = "";
                            }
                            
                            $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                            if(!empty($savedjob[0]['id'])) {
                                $savedjob = '1';
                            } else{
                                $savedjob = "0";
                            }

                             $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                            if($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else{
                                $basicsalary = 0;
                            }

                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if(isset($companydetail1[0]['address']))
                            {
                                $companyaddress=$companydetail1[0]['address'];
                            }
                            else
                            {
                                $companyaddress='';
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            
                            
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                            if(isset($jobImage[0]['pic']))
                            {
                                $jobImageData=$jobImage[0]['pic'];
                            }
                            else
                            {
                                $jobImageData=$comPic;
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', ''); 

                            $data1[] = [
                                "jobpost_id" => $hotjobsData['id'],
                                "comapnyId" =>$hotjobsData['compId'],
                                "job_title" => $hotjobsData['jobtitle'],
                                "jobDesc" => $hotjobsData['jobDesc'],
                                "salary" => number_format($hotjobsData['salary']),
                                "companyName"=> $companydetail[0]["cname"],
                                "cname"=> $cname,
                                "companyAddress"=>$companyaddress,
                                "toppicks1" => $datatoppicks1,
                                "toppicks2" => $datatoppicks2,
                                "toppicks3" => $datatoppicks3,
                                "job_image" =>$jobImageData,
                                "latitude"=> $hotjobsData['latitude'], 
                                "longitude"=>$hotjobsData['longitude'],
                                "jobexpire"=> $hotjobsData['jobexpire'],
                                "companyPic"=> $comPic,
                                "savedjob" =>$savedjob,
                                "boostjob" =>$hotjobsData['boost_status'],
                                "distance" => $distance,
                                "mode"=> $hotjobsData['mode'],
                                "modeurl"=> $hotjobsData['modeurl'],
                            ];
                        }
                    }

                } else if($userData['type'] == "toppicks") {

                    foreach($companyidsarray as $companyidsarr) {

                        $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_all_groupby($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $companyidsarr, $userData['typeid']);

                        if ($hotjobDataonegroup) {
                            foreach ($hotjobDataonegroup as $hotjobsData) {
                                $neglectarr[] = $hotjobsData['id'];
                                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $userData['typeid']);
                                $datatoppicks1 = $userData['typeid']; 
                                
                                if(isset($jobTopseleted[0]['picks_id'])) {
                                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTopseleted[1]['picks_id'])) {
                                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = $comPic;
                                }
                                
                                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $userID);
                                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                                if (isset($jobsavedStatus[0]['status'])) {
                                    $jobsstatus = '1';
                                } else {
                                    $jobsstatus = '0';
                                }
                                
                                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                if ($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else {
                                    $basicsalary = "0";
                                }
                                
                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if (isset($companydetail1[0]['address'])) {
                                    $companyaddress = $companydetail1[0]['address'];
                                } else {
                                    $companyaddress = '';
                                }
                                
                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat, $userLong, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', '');
                                //$distance = '';
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = '';
                                }
                                $data1[] = ["jobpost_id" => $hotjobsData['id'], 
                                          "comapnyId" => $hotjobsData['compId'],
                                          "recruiter_id" => $hotjobsData['recruiter_id'],
                                          "job_title" => $hotjobsData['jobtitle'], 
                                          "jobDesc" => $hotjobsData['jobDesc'], 
                                          "salary" => number_format($basicsalary), 
                                          "companyName" => $companydetail[0]["cname"], 
                                          "companyAddress" => $companyaddress,
                                          "job_image" => $jobImageData,
                                          "jobPitch"=>$hotjobsData['jobPitch'], 
                                          "cname"=>$cname, 
                                          "distance" =>$distance, 
                                          'job_image' =>$jobImageData,
                                          "savedjob"=> $jobsstatus,
                                          "toppicks1" => $datatoppicks1,
                                          "toppicks2" => $datatoppicks2,
                                          "toppicks3" => $datatoppicks3,
                                          "mode"=>$hotjobsData['mode'], 
                                ];

                            }
                        }
                    }
                    usort($data1, function($a, $b) {
                        return $a['distance'] <=> $b['distance'];
                    });

                    $hotjobData = $this->Newpoints_Model->feturedtoppicks_fetch_latlong($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong);
                    foreach ($hotjobData as $hotjobsData) {

                        if(is_array($hotjobsData['id'], $negletarr)) { } else {
                            $jobTop = $this->Jobpost_Model->job_toppicks1($hotjobsData['id']);
                            $pids = array(); 
                            foreach($jobTop as $jobTopp) {
                                $pids[] = $jobTopp['picks_id'];
                            }
                            if(in_array($userData['typeid'], $pids)) {

                                if(!empty($jobTop) && count($jobTop)>=1) {

                                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $userData['typeid']);
                                    $datatoppicks1 = $userData['typeid']; 
                                    
                                    if(isset($jobTopseleted[0]['picks_id'])) {
                                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                                    } else{
                                        $datatoppicks2 = "";
                                    }
                                    if(isset($jobTopseleted[1]['picks_id'])) {
                                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                                    } else{
                                        $datatoppicks3 = "";
                                    }
                                    
                                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                                    if(!empty($savedjob[0]['id'])) {
                                        $savedjob = '1';
                                    } else{
                                        $savedjob = "0";
                                    }

                                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                    if($jobsal[0]['basicsalary']) {
                                        $basicsalary = $jobsal[0]['basicsalary'];
                                    } else{
                                        $basicsalary = 0;
                                    }

                                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                    if(isset($companydetail1[0]['address']))
                                    {
                                        $companyaddress=$companydetail1[0]['address'];
                                    }
                                    else
                                    {
                                        $companyaddress='';
                                    }

                                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                    if(!empty($companyPic[0]['companyPic'])) {
                                        $comPic = trim($companyPic[0]['companyPic']);
                                    } else{
                                        $comPic = "";
                                    }
                                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                                    if(isset($jobImage[0]['pic']))
                                    {
                                        $jobImageData=$jobImage[0]['pic'];
                                    }
                                    else
                                    {
                                        $jobImageData=$comPic;
                                    }

                                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                    if(!empty($companyname[0]["cname"])){
                                        $cname = $companyname[0]["cname"];
                                    }else{
                                        $cname = '';
                                    }

                                    $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    $data1[] = [
                                        "jobpost_id" => $hotjobsData['id'],
                                        "comapnyId" =>$hotjobsData['compId'],
                                        "job_title" => $hotjobsData['jobtitle'],
                                        "jobDesc" => $hotjobsData['jobDesc'],
                                        "salary" => number_format($hotjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $hotjobsData['latitude'], 
                                        "longitude"=>$hotjobsData['longitude'],
                                        "jobexpire"=> $hotjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$hotjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $hotjobsData['mode'],
                                        "modeurl"=> $hotjobsData['modeurl'],
                                    ];
                                }
                            }
                        }
                    }
                
                } else if($userData['type'] == "Allowance") {

                    foreach($companyidsarray as $companyidsarr) {
                        $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_allowances_groupby($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $companyidsarr);
                        if ($hotjobDataonegroup) {
                            foreach ($hotjobDataonegroup as $hotjobsData) {
                                $neglectarr[] = $hotjobsData['id'];
                                $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $getParamType);
                                $datatoppicks1 = $getParamType; 
                                
                                if(isset($jobTopseleted[0]['picks_id'])) {
                                    $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTopseleted[1]['picks_id'])) {
                                    $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = $comPic;
                                }
                                
                                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $userTokenCheck[0]['id']);
                                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                                if (isset($jobsavedStatus[0]['status'])) {
                                    $jobsstatus = '1';
                                } else {
                                    $jobsstatus = '0';
                                }
                                
                                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                if ($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else {
                                    $basicsalary = "0";
                                }
                                
                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if (isset($companydetail1[0]['address'])) {
                                    $companyaddress = $companydetail1[0]['address'];
                                } else {
                                    $companyaddress = '';
                                }
                                
                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat, $userLong, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', '');
                                //$distance = '';
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = '';
                                }
                                $data1[] = ["jobpost_id" => $hotjobsData['id'], 
                                          "comapnyId" => $hotjobsData['compId'],
                                          "recruiter_id" => $hotjobsData['recruiter_id'],
                                          "job_title" => $hotjobsData['jobtitle'], 
                                          "jobDesc" => $hotjobsData['jobDesc'], 
                                          "salary" => number_format($basicsalary), 
                                          "companyName" => $companydetail[0]["cname"], 
                                          "companyAddress" => $companyaddress,
                                          "job_image" => $jobImageData,
                                          "jobPitch"=>$hotjobsData['jobPitch'], 
                                          "cname"=>$cname, 
                                          "distance" =>$distance, 
                                          'job_image' =>$jobImageData,
                                          "savedjob"=> $jobsstatus,
                                          "toppicks1" => $datatoppicks1,
                                          "toppicks2" => $datatoppicks2,
                                          "toppicks3" => $datatoppicks3,
                                          "mode"=>$hotjobsData['mode'], 
                                ];

                            }
                        }
                    }

                    usort($data1, function($a, $b) {
                        return $a['distance'] <=> $b['distance'];
                    });

                    $hotjobData = $this->Newpoints_Model->feturedtoppicks_fetch_latlong($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong);
                    foreach ($hotjobData as $hotjobsData) {

                        if(is_array($hotjobsData['id'], $negletarr)) { } else {
                            $jobTop = $this->Jobpost_Model->job_allowancesbyid($hotjobsData['id'], $userData['typeid']);
                            if(!empty($jobTop) && count($jobTop)>=1) {

                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else {
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else {
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else {
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userTokenCheck[0]['id']);

                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                 $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if(isset($companydetail1[0]['address']))
                                {
                                    $companyaddress=$companydetail1[0]['address'];
                                }
                                else
                                {
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                                
                                
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                                if(isset($jobImage[0]['pic']))
                                {
                                    $jobImageData=$jobImage[0]['pic'];
                                }
                                else
                                {
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userData['cur_lat'], $userData['cur_long'],$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $data1[] = [
                                    "jobpost_id" => $hotjobsData['id'],
                                    "comapnyId" =>$hotjobsData['compId'],
                                    "job_title" => $hotjobsData['jobtitle'],
                                    "jobDesc" => $hotjobsData['jobDesc'],
                                    "salary" => number_format($hotjobsData['salary']),
                                    "companyName"=> $companydetail[0]["cname"],
                                    "cname"=> $cname,
                                    "companyAddress"=>$companyaddress,
                                    "toppicks1" => $datatoppicks1,
                                    "toppicks2" => $datatoppicks2,
                                    "toppicks3" => $datatoppicks3,
                                    "job_image" =>$jobImageData,
                                    "latitude"=> $hotjobsData['latitude'], 
                                    "longitude"=>$hotjobsData['longitude'],
                                    "jobexpire"=> $hotjobsData['jobexpire'],
                                    "companyPic"=> $comPic,
                                    "savedjob" =>$savedjob,
                                    "boostjob" =>$hotjobsData['boost_status'],
                                    "distance" => $distance,
                                    "mode"=> $hotjobsData['mode'],
                                    "modeurl"=> $hotjobsData['modeurl'],
                                ];
                            }
                        }
                    }

                } else if($userData['type'] == "leadership") {

                    foreach($companyidsarray as $companyidsarr) {
                        $boostjobData = $this->Homejob_Model->catjob_fetch_leadership($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $companyidsarr);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                            if(!empty($jobTop) && count($jobTop)>=0) {

                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $bostArr[] = $boostjobsData['id'];

                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "recruiter_id" => $boostjobsData['recruiter_id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'], 
                                        "modeurl"=> $boostjobsData['modeurl'], 
                                ];
                            }
                        }    
                    }

                    usort($data1, function($a, $b) {
                        return $a['distance'] <=> $b['distance'];
                    });

                    $boostjobData = $this->Jobpost_Model->catjob_fetch_leadership_all($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong);
                    foreach ($boostjobData as $boostjobsData) {

                        if(is_array($boostjobsData['id'], $negletarr)) { } else {
                            $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                            if(!empty($jobTop) && count($jobTop)>=0) {
      
                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $bostArr[] = $boostjobsData['id'];

                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'], 
                                        "modeurl"=> $boostjobsData['modeurl'], 
                                ];
                            }
                        }
                    }

                }  else if($userData['type'] == "information") {

                    foreach($companyidsarray as $companyidsarr) {
                        $boostjobData = $this->Homejob_Model->catjob_fetch_information_technology($userTokenCheck[0]['id'], $expfilter, $userLat, $userLong, $companyidsarr);
                        foreach ($boostjobData as $boostjobsData) {
                            $negletarr[] = $boostjobsData['id'];
                            $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                            if(!empty($jobTop) && count($jobTop)>=0) {

                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $bostArr[] = $boostjobsData['id'];

                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "recruiter_id" => $boostjobsData['recruiter_id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'], 
                                        "modeurl"=> $boostjobsData['modeurl'], 
                                ];
                            }
                        }    
                    }

                    usort($data1, function($a, $b) {
                        return $a['distance'] <=> $b['distance'];
                    });

                    $boostjobData = $this->Jobpost_Model->catjob_fetch_information_technology_all($userTokenCheck[0]['id'],$expfilter, $userLat, $userLong);
                    foreach ($boostjobData as $boostjobsData) {

                        if(is_array($boostjobsData['id'], $negletarr)) { } else {
                            $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
                            if(!empty($jobTop) && count($jobTop)>=0) {
      
                                if(isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }
                                
                                $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userTokenCheck[0]['id']);
                                if(!empty($savedjob[0]['id'])) {
                                    $savedjob = '1';
                                } else{
                                    $savedjob = "0";
                                }

                                $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                                if($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else{
                                    $basicsalary = 0;
                                }

                                $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                                if(isset($companydetail1[0]['address'])){
                                    $companyaddress=$companydetail1[0]['address'];
                                }else{
                                    $companyaddress='';
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                                if(isset($jobImage[0]['pic'])){
                                    $jobImageData=$jobImage[0]['pic'];
                                }else{
                                    $jobImageData=$comPic;
                                }

                                $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                $bostArr[] = $boostjobsData['id'];

                                $data1[] = [
                                        "jobpost_id" => $boostjobsData['id'],
                                        "comapnyId" =>$boostjobsData['compId'],
                                        "job_title" => $boostjobsData['jobtitle'],
                                        "jobDesc" => $boostjobsData['jobDesc'],
                                        "jobPitch"=>$boostjobsData['jobPitch'],
                                        "salary" => number_format($boostjobsData['salary']),
                                        "companyName"=> $companydetail[0]["cname"],
                                        "cname"=> $cname,
                                        "companyAddress"=>$companyaddress,
                                        "toppicks1" => $datatoppicks1,
                                        "toppicks2" => $datatoppicks2,
                                        "toppicks3" => $datatoppicks3,
                                        "job_image" =>$jobImageData,
                                        "latitude"=> $boostjobsData['latitude'], 
                                        "longitude"=>$boostjobsData['longitude'],
                                        "jobexpire"=> $boostjobsData['jobexpire'],
                                        "companyPic"=> $comPic,
                                        "savedjob" =>$savedjob,
                                        "boostjob" =>$boostjobsData['boost_status'],
                                        "distance" => $distance,
                                        "mode"=> $boostjobsData['mode'], 
                                        "modeurl"=> $boostjobsData['modeurl'], 
                                ];
                            }
                        }
                    }
                }

                
                $data2["hotjobss"] = array();
                $data2["hotjobss"] = $data1;

                $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                $fetchpromonotifications = $this->Candidate_Model->fetchpromonotifications($userTokenCheck[0]['id']);
                $response = ['hotjobslist' => $data2,'notification_count' =>count($fetchnotifications)+count($fetchpromonotifications), 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }


    public function jobLocateHome_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $jobLocates = $this->Jobpost_Model->group_locateHome($userTokenCheck[0]['id']);

            $x=0;
            foreach ($jobLocates as $jobLocate) {
                
                $getlatlognids = $this->Jobpost_Model->jobfetchbylatlong($jobLocate['latitude'], $jobLocate['longitude']);
                $getmaxsal = array();
                foreach($getlatlognids as $getlatlognid) {
                    $salaryarrays = $this->Jobpost_Model->jobfetchbymaxsalary($getlatlognid['id']);
                    $getmaxsal[] = (int) $salaryarrays[0]['basesalary'];
                }
                $gotsalmax =  max($getmaxsal);

                // $jobsal = $this->Jobpost_Model->jobfetchbymaxsalary($jobLocate['id']);

                // if($jobsal[0]['basicsalary']) {
                //     $basicsalary = $jobsal[0]['basicsalary'];
                // } else{
                //     $basicsalary = " ";
                // }
               

                $jobLocates[$x]= ["recruiter_id"=> $jobLocate['recruiter_id'], "latitude"=> $jobLocate['latitude'], "longitude"=>$jobLocate['longitude'], "totaljobs"=>$jobLocate['totaljobs'], "salary"=> $gotsalmax];
                
                $jobTop = $this->Jobpost_Model->job_toppicks($jobLocate['id']);
                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $one = (string)$jobTop[0]['picks_id'];
                        $jobLocates[$x]['toppicks1'] = "$one";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $two = (string)$jobTop[1]['picks_id'];
                        $jobLocates[$x]['toppicks2'] =  "$two";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $three = (string)$jobTop[2]['picks_id'];
                        $jobLocates[$x]['toppicks3'] = "$three";
                    }
                } else {
                    $jobLocates[$x]['toppicks1'] = "";
                    $jobLocates[$x]['toppicks2'] = "";
                    $jobLocates[$x]['toppicks3'] = "";
                }
                $x++;
            }
            
            $response = ['jobLocateHome' => $jobLocates, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $userLat = $userData['lat'];
            $userLong = $userData['long'];
            $jobfetchs = $this->Jobpost_Model->job_fetch_latlong($userLat, $userLong);
            
            if($jobfetchs) {
                $x=0;
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if($jobTop) {

                    } else{
                        $jobTop = [];
                    }
                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = " ";
                        }
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = $companyPic[0]['companyPic'];
                        } else{
                            $comPic = " ";
                        }
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => $basicsalary,
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic
                            ];
                    }
                    $x++;
                }
                
                $response = ['jobListing' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            } else{
                $errors = "No data found";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companydetail_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $company_id = $userData['companyId'];
            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);

            if($compfetchs) {
                $jobTop = $this->Jobpost_Model->job_fetch_TopPicks($compfetchs[0]['compId']);

                if($jobTop) {
                    if(isset($jobTop[0]['picks_id'])){
                        $datatoppicks1 = $jobTop[0]['picks_id'];    
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])){
                        $datatoppicks2 =  $jobTop[1]['picks_id'];    
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])){
                        $datatoppicks3 = $jobTop[2]['picks_id'];    
                    } else{
                        $datatoppicks3 = "";
                    }
                    $topickss = $jobTop;
                } else {
                    $datatoppicks1 = "";
                    $datatoppicks2 = "";
                    $datatoppicks3 = "";
                    $topickss = [];
                }
                $companyPic = $this->Jobpost_Model->fetch_companyPic($compfetchs[0]['compId']);
                        
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic[0] = $companyPic[0]['companyPic'];
                } else{
                    $comPic = [];
                }
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "address" => $compfetchs[0]['address'],
                                "companyName" => $compfetchs[0]['cname'],
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compfetchs[0]['companyDesc'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "topPicks" =>$topickss
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_bycompId($company_id);
            
            if($jobfetchs) {
                $data["jobList"] = [];
                $data["jobLists"] = [];
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = " ";
                        }

                        $jobTop1 = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        if($jobTop1) {
                            if(isset($jobTop1[0]['picks_id'])){
                                $datatoppicks1 = $jobTop1[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop1[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop1[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop1[2]['picks_id'])){
                                $datatoppicks3 = $jobTop1[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = $companyPic[0]['companyPic'];
                        } else{
                            $comPic = " ";
                        }
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }

                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => number_format($basicsalary),
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic
                            ];
                    }
                }

                if(count($data["jobLists"]) < 1) {
                    $data["jobLists"] = $data["jobList"];
                    if(isset($data["jobList"][0])) {
                        $data["jobList"] = $data["jobList"][0];
                    } else{
                        $data["jobList"] = $data["jobList"];
                    }
                } else{
                    $data["jobList"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];
                    $data["jobLists"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];    
                }

            } else{
                $data["jobList"][] =[
                            "jobpost_id"=> "",
                            "job_title"=> "",
                            "jobDesc"=> "",
                            "salary"=> "",
                            "jobexpire" => "",
                            "companyName"=> "",
                            "companyId"=> ""
                        ];
            }
            
            $response = ['companydetail' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function availableJobs_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $company_id = $userData['companyId'];
            $compfetchs = $this->Jobpost_Model->fetch_companydetails_bycompanyid($company_id);

            if($compfetchs) {
                $data['companyDetail'] = [
                                "companyId" => $compfetchs[0]['compId'],
                                "companyName" => $compfetchs[0]['cname'],
                                "companyPic" => $compfetchs[0]['companyPic'],
                                "companyDesc" => $compfetchs[0]['companyDesc']
                            ];
            } else{
                $data['companyDetail'] = [];
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_bycompId($company_id);
            
            if($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);

                    if($jobApplyfetch) {

                    } else {
                        $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = " ";
                        }

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);

                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $datatoppicks1 = $jobTop[0]['picks_id'];    
                            } else{
                                $datatoppicks1 = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $datatoppicks2 =  $jobTop[1]['picks_id'];    
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $datatoppicks3 = $jobTop[2]['picks_id'];    
                            } else{
                                $datatoppicks3 = "";
                            }
                        } else {
                            $datatoppicks1 = "";
                            $datatoppicks2 = "";
                            $datatoppicks3 = "";
                        }
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['compId']);
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = $companyPic[0]['companyPic'];
                        } else{
                            $comPic = " ";
                        }
                        
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }
                        $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => number_format($basicsalary),
                                "jobexpire" => $jobfetch['jobexpire'],
                                "companyName" => $jobfetch['cname'],
                                "companyId" => $jobfetch['compId'],
                                "toppicks1" => "$datatoppicks1",
                                "toppicks2" => "$datatoppicks2",
                                "toppicks3" => "$datatoppicks3",
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic
                            ];
                    }
                }
            } else{
                $data["jobList"] =[];
            }
            $response = ['availableJobs' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobdetail_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $jobpost_id = $userData['jobpost_id'];
            $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobpost_id);
            $jobLocation = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
            $jobSaved = $this->Jobpost_Model->applied_status_fetch($jobpost_id, $userTokenCheck[0]['id']);

            if($jobSaved) {
                $savestatus = 1;
            } else{
                $savestatus = 0;
            }

            $appliedStatus = $this->Jobpost_Model->applied_status_fetch($jobDetails[0]['id'], $userTokenCheck[0]['id']);
            if($appliedStatus) {
                $jobstatus = $appliedStatus[0]['status'];
            } else{
                $jobstatus = 0;
            }

            $jobsal = $this->Jobpost_Model->getExpJob($jobDetails[0]['id']);
            if($jobsal[0]['basicsalary']) {
                $basicsalary = $jobsal[0]['basicsalary'];
            } else{
                $basicsalary = " ";
            }

            $jobTop = $this->Jobpost_Model->job_toppicks($jobDetails[0]['id']);
            $jobImg = $this->Jobpost_Model->job_images_single($jobpost_id);
           // echo $this->db->last_query();
            if($jobTop) {
                // if(isset($jobTop[0]['picks_id'])){
                //     $datatoppicks1 = $jobTop[0]['picks_id'];    
                // } else{
                //     $datatoppicks1 = "";
                // }
                // if(isset($jobTop[1]['picks_id'])){
                //     $datatoppicks2 =  $jobTop[1]['picks_id'];    
                // } else{
                //     $datatoppicks2 = "";
                // }
                // if(isset($jobTop[2]['picks_id'])){
                //     $datatoppicks3 = $jobTop[2]['picks_id'];    
                // } else{
                //     $datatoppicks3 = "";
                // }
                $toppickss = $jobTop;
            } else {
                $toppickss = [];
            }

            $arrSkills = explode(",",$jobDetails[0]['skills']);
            $arrQualify = explode(",",$jobDetails[0]['qualification']);
            $data["jobdetail"] = [
                        "jobpost_id" => $jobDetails[0]['id'],
                        "job_title" => $jobDetails[0]['jobtitle'],
                        "location" => $jobLocation[0]['address'],
                        "opening" => $jobDetails[0]['opening'],
                        "experience" => $jobDetails[0]['experience'],
                        "jobDesc" => $jobDetails[0]['jobDesc'],
                        "salary" => $basicsalary,
                        "jobexpire" => $jobDetails[0]['jobexpire'],
                        "skills" => $arrSkills,
                        "qualification" => $arrQualify,
                        "jobSaved" => $savestatus,
                        "status" => $jobstatus,
                        "topPicks" => $toppickss,
                        "images" => $jobImg
                    ];

            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobDetails[0]['company_id']);
            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['company_id']);
    
            if($jobTop) {

            } else{
                $jobTop = [];
            }
            $companyPic = $this->Jobpost_Model->fetch_companyPic($companydetail[0]["id"]);            
            if(!empty($companyPic[0]['companyPic'])) {
                $comPic = $companyPic[0]['companyPic'];
            } else{
                $comPic = " ";
            }
            $data["comapnyDetail"] = [
                                    "name"=> $companydetail[0]["cname"],
                                    "phone" => $companydetail1[0]["recruiter_contact"],
                                    "email" => $companydetail1[0]["recruiter_email"],
                                    "company_id" => $companydetail[0]["id"],
                                    "companyPic" => $comPic
                                    ];
            $response = ['jobdetail' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

     public function applyJob_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);
        if($userTokenCheck1)
        {
            $tokendata = ['id'=>$userTokenCheck1[0]['user_id']];
            $userTokenCheck = $this->User_Model->token_match($tokendata);
            if($userTokenCheck) 
            {
                $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');
                $this->form_validation->set_rules('interviewdate', 'Interview Date', 'trim|required');
                $this->form_validation->set_rules('interviewtime', 'Interview Time', 'trim|required');

                if ($this->form_validation->run() == FALSE) 
                {
                    $errors = $this->form_validation->error_array();
                    $error = current($errors);
                    $response = ['message' => $error, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);

                } else {

                $jobexpire = date("Y-m-d", strtotime($job['jobexpire']));
                $schedule = date("Y-m-d", strtotime($job['interviewdate']));
                
                $jobexpire1 = strtotime($jobexpire);
                $schedule1 = strtotime($schedule);
                $currentDate = strtotime(date('Y-m-d'));
                $job_detail = $this->Jobpost_Model->job_detail_fetch($job['jobpost_id']);
                $companyname = $this->Candidate_Model->fetchcomp($job_detail[0]['company_id']);
                if($job_detail[0]['level_status']=='1')
                {
                    $user_level_match = $this->User_Model->user_single_workbyuserid($userTokenCheck[0]['id'],$job_detail[0]['level']);
                    if($user_level_match){
                        $level_match = "True";
                    } else{
                        $level_match = "False";
                    }

                }
                if($job_detail[0]['education_status']=='1')
                {
                    $user_education_match = $this->User_Model->user_single_educationbyuserid($userTokenCheck[0]['id'],$job_detail[0]['education']);
                    if($user_education_match){
                        $education_match = "True";
                    } else{
                        $education_match = "False";
                    }

                }
                if($job_detail[0]['experience_status']=='1')
                {

                    $user_experience_match = $this->User_Model->user_single_exp($userTokenCheck[0]['id'],$job_detail[0]['experience']);
                    
                    if($user_experience_match){
                        $experience_match = "True";
                    } else{
                        $experience_match = "False";
                    }

                }
                if($jobexpire1 > $schedule1) 
                {
                    if($schedule1 >= $currentDate) 
                    {
                      $jobapply = [
                          "jobpost_id" => $job['jobpost_id'],
                          "user_id" => $userTokenCheck[0]['id'],
                          "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                          "interviewtime" => $job['interviewtime'],
                          "status" => 1
                      ];
                      if($level_match == "True" && $education_match == "True" && $experience_match== "True")
                      {
                        $appliedJobs = $this->Jobpost_Model->job_fetch_applied($userTokenCheck[0]['id']);
                        $this->Jobpost_Model->appliedjob_insert($jobapply);
                        $this->Recruit_Model->Notificationmessageinsert('You have got a new application from candidate');
                        $response = ['jobdetail' => $jobapply, 'status' => "SUCCESS"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                      }else{
                        $jobapply1 = [
                          "jobpost_id" => $job['jobpost_id'],
                          "user_id" => $userTokenCheck[0]['id'],
                          "interviewdate" => date("y-m-d",strtotime($job['interviewdate'])),
                          "interviewtime" => $job['interviewtime'],
                          "status" => 3,
                          "fallout_status" => 2,
                          "fallout_reason" => "Did not meet requirement"
                        ];
                        $this->Jobpost_Model->appliedjob_insert($jobapply1);
                         $user_data = $this->User_Model->user_single($userTokenCheck[0]['id']);
                         $msg="Your Job application for "." ". $job_detail[0]['jobtitle'] ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Fall Out";
                         $data1 = ["user_id"=>$userTokenCheck[0]['id'],"jobapp_id"=>$job['jobpost_id'], "recruiter_id" =>$job_detail[0]['company_id'], "notification"=>$msg, "status_changed"=>3, "job_status"=>'0'];

                         $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                         $fetchnotifications = $this->Candidate_Model->fetchnotifications($userTokenCheck[0]['id']);
                         $msg="Your Job application for "." ". $job_detail[0]['jobtitle'] ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Fall Out";
                         if($user_data[0]['notification_status']=='1')
                         {
                           if($user_data[0]['device_type']=="android"){
                                $res1 = $this->push_notification_android_status($user_data[0]['device_token'],$job_detail[0]['jobtitle'], $msg,'applied',$job_detail[0]['recruiter_id'],$companyname[0]['cname'],$notificationInserted);
                            }else if($user_data[0]['device_type']=="ios"){
                                $this->pushiphon(array($user_data[0]['device_token'],$msg,'applied',$job_detail[0]['jobtitle'], $job_detail[0]['id'],count($fetchnotifications),$companyname[0]['cname'],$notificationInserted));
                            
                            } 
                        }
                        $errors = "Update your profile first";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                      }
                      
                    
                    } else{
                        $errors = "Please select correct date";
                        $response = ['message' => $errors, 'status' => "FAILURE"];
                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                } else{
                    $errors = "Interview date expire on $jobexpire.";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
           } }
        else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    

    public function jobAppliedListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $jobAppliedpasts = $this->Jobpost_Model->jobappliedpast_fetch_byuserid($userTokenCheck[0]['id']);
            if($jobAppliedpasts) {

                foreach ($jobAppliedpasts as $jobAppliedpast) {
                    $jobsal = $this->Jobpost_Model->getExpJob($jobAppliedpast['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = " ";
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedpast['company_id']);
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedpast['id']);

                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id'];    
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])){
                            $datatoppicks2 =  $jobTop[1]['picks_id'];    
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $datatoppicks3 = $jobTop[2]['picks_id'];    
                        } else{
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobAppliedpast['company_id']);            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = $companyPic[0]['companyPic'];
                    } else{
                        $comPic = " ";
                    }
                    
                    $image = $this->Jobpost_Model->job_image($jobAppliedpast['id']);
                        
                        
                    if(!empty($image[0]['pic'])) {
                        $jobpic = $image[0]['pic'];
                    } else{
                        $jobpic = " ";
                    }
                    $data["jobPast"][] = [
                            "jobpost_id" => $jobAppliedpast['id'],
                            "job_title" => $jobAppliedpast['jobtitle'],
                            "jobDesc" => $jobAppliedpast['jobDesc'],
                            "salary" => $basicsalary,
                            "status" => $jobAppliedpast['status'],
                            "companyName"=> $companydetail[0]["cname"],
                            "toppicks1" => "$datatoppicks1",
                            "toppicks2" => "$datatoppicks2",
                            "toppicks3" => "$datatoppicks3",
                            "companyPic" => $comPic,
                            "jobpic" => $jobpic
                        ];
                }
            } else {
                $data["jobPast"][] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "status" => "",
                            "companyName"=> "",
                            "companyPic" => ""
                                ];
            }

            $jobAppliedupcomings = $this->Jobpost_Model->jobappliedupcoming_fetch_byuserid($userTokenCheck[0]['id']);
            if($jobAppliedupcomings) {

                foreach ($jobAppliedupcomings as $jobAppliedupcoming) {
                    $jobsal1 = $this->Jobpost_Model->getExpJob($jobAppliedupcoming['id']);
                    if($jobsal1[0]['basicsalary']) {
                        $basicsalary1 = $jobsal1[0]['basicsalary'];
                    } else{
                        $basicsalary1 = " ";
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedupcoming['company_id']);
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedupcoming['id']);

                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id'];    
                        } else{
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])){
                            $datatoppicks2 =  $jobTop[1]['picks_id'];    
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $datatoppicks3 = $jobTop[2]['picks_id'];    
                        } else{
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobAppliedupcoming['company_id']);            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = $companyPic[0]['companyPic'];
                    } else{
                        $comPic = " ";
                    }
                    
                    $image = $this->Jobpost_Model->job_image($jobAppliedupcoming['id']);
                        
                        
                    if(!empty($image[0]['pic'])) {
                        $jobpic = $image[0]['pic'];
                    } else{
                        $jobpic = " ";
                    }
                    $data["jobUpcoming"][] = [
                            "jobpost_id" => $jobAppliedupcoming['id'],
                            "job_title" => $jobAppliedupcoming['jobtitle'],
                            "jobDesc" => $jobAppliedupcoming['jobDesc'],
                            "salary" => $basicsalary1,
                            "status" => $jobAppliedupcoming['status'],
                            "companyName"=> $companydetail[0]["cname"],
                            "toppicks1" => "$datatoppicks1",
                            "toppicks2" => "$datatoppicks2",
                            "toppicks3" => "$datatoppicks3",
                            "companyPic" => $comPic,
                            "jobpic" => $jobpic
                        ];
                }
            } else {
                $data["jobUpcoming"][] = [
                            "jobpost_id" => "",
                            "job_title" => "",
                            "jobDesc" => "",
                            "salary" => "",
                            "status" => "",
                            "companyName"=> "",
                            "companyPic" => ""
                ];
            }

            $response = ['jobAppliedListing' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function saveJob_post() {
        $job = $this->input->post();
        $data = ["token" => $job['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $this->form_validation->set_rules('jobpost_id', 'job post id', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                $error = current($errors);
                $response = ['message' => $error, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {

                $jobsave = [
                            "jobpost_id" => $job['jobpost_id'],
                            "user_id" => $userTokenCheck[0]['id'],
                        ];
                $this->Jobpost_Model->savejob_insert($jobsave);
                $response = ['saveJob' => $jobsave, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function saveJobListing_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);

        if($userTokenCheck) {
            $jobSaveds = $this->Jobpost_Model->jobSaved_fetch_byuserid($userTokenCheck[0]['id']);
            if($jobSaveds) {

                foreach ($jobSaveds as $jobSaved) {
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobSaved['company_id']);
                    $appliedStatus = $this->Jobpost_Model->applied_status_fetch($jobSaved['id'], $userTokenCheck[0]['id']);
                    
                    if($appliedStatus) {
                        $jobstatus = $appliedStatus[0]['status'];
                    } else{
                        $jobstatus = 0;
                    }
                    $data["jobList"][] = [
                            "jobpost_id" => $jobSaved['id'],
                            "job_title" => $jobSaved['jobtitle'],
                            "jobDesc" => $jobSaved['jobDesc'],
                            "salary" => number_format($jobSaved['salary']),
                            "status" => $jobstatus,
                            "companyName"=> $companydetail[0]["cname"]
                        ];
                }
            } else {
                $data["jobList"][] = [];
            }

            $response = ['saveJobListing' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function filters_post() {
        $filterData = $this->input->post();
        $data = ["token" => $filterData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        $data["jobList"] = [];
        if($userTokenCheck) {

            $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id']);
            //echo $this->db->last_query();exit();
           // print_r($jobfetchs);die;
            if($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {

                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    
                    if($jobTop) {
                        if(isset($jobTop[0]['picks_id'])){
                            $one = (string)$jobTop[0]['picks_id'];
                            $dataJobList[0] = "$one";
                        } else{
                            $dataJobList[0] = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $two = (string)$jobTop[1]['picks_id'];
                            $dataJobList[1] = "$two";
                        } else {
                            $dataJobList[1] = "";
                        }
                        if(isset($jobTop[2]['picks_id'])){
                            $three = (string)$jobTop[2]['picks_id'];
                            $dataJobList[2] = "$three";
                        } else{
                            $dataJobList[2] = "";
                        }
                    } else {
                        $dataJobList[0] = "";
                        $dataJobList[1] = "";
                        $dataJobList[2] = "";
                    }
                    //print_r($jobfetch);die;
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                    //echo $this->db->last_query();die;
                    $sal = $this->Jobpost_Model->fetch_highsalary($jobfetch['id']);
                     $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                        if(!empty($image[0]['pic'])) {
                            $jobpic = $image[0]['pic'];
                        } else{
                            $jobpic = " ";
                        }    
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = $companyPic[0]['companyPic'];
                    } else{
                        $comPic = " ";
                    }
                    $data["jobList"][] = [
                            "jobpost_id" => $jobfetch['id'],
                            "job_title" => $jobfetch['jobtitle'],
                            "jobDesc" => $jobfetch['jobDesc'],
                            "salary" => number_format($sal[0]['salary']),
                            "address" => $jobfetch['address'],
                            "companyName" => $jobfetch['cname'],
                            "latitude" => $jobfetch['latitude'],
                            "longitude" => $jobfetch['longitude'],
                            "jobcount" => 1,
                            "toppicks1" => $dataJobList[0],
                            "toppicks2" => $dataJobList[1],
                            "toppicks3" => $dataJobList[2],
                            "companyPic" => $comPic,
                            "jobpic" => $jobpic
                        ];
                }
                
                if(count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }

                $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id']);
                //print_r($jobcountfetchs);die;
                if($jobcountfetchs) {
                    foreach ($jobcountfetchs as $jobcountfetch) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }

                        $sal = $this->Jobpost_Model->fetch_highsalary($jobcountfetch['id']);

                        $data["jobcount"][] = [
                                "company_id" => $jobcountfetch['company_id'],
                                "salary" => number_format($sal[0]['salary']),
                                "latitude" => $jobcountfetch['latitude'],
                                "longitude" => $jobcountfetch['longitude'],
                                "address" => $jobcountfetch['address'],
                                "jobcount" => $jobcountfetch['jobcount'],
                                "toppicks1" => $dataJobList[0],
                                "toppicks2" => $dataJobList[1],
                                "toppicks3" => $dataJobList[2]
                            ];
                }
                        
                } else{
                    $data['jobcount'] = [];
                }
            
                $response = ['filters' => $data, 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else{
                $errors = "No Data Found";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobFilters_post() {
        $filterData = $this->input->post();
        //print_r($filterData);exit();
        $data = ["token" => $filterData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        //print_r($userTokenCheck);exit();
        $data["jobList"] = [];
        $jobListing = [];

        if($userTokenCheck) {
            $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id']);
            //echo $this->db->last_query();exit();
            //print_r($jobfetchs);exit();
            if($jobfetchs) {
                $x=1;
                foreach ($jobfetchs as $jobfetch) {
                        
                        $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                        
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])){
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else{
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        if($jobfetch['id']){
                            $jobfetch['id']=$jobfetch['id'];
                        }else{
                            $jobfetch['id']="";
                        }
                        if($jobfetch['jobtitle']){
                            $jobfetch['jobtitle']=$jobfetch['jobtitle'];
                        }else{
                            $jobfetch['jobtitle']="";
                        }
                        if($jobfetch['jobDesc']){
                            $jobfetch['jobDesc']=$jobfetch['jobDesc'];
                        }else{
                            $jobfetch['jobDesc']="";
                        }
                        
                        if($jobfetch['cname']){
                            $jobfetch['cname']=$jobfetch['cname'];
                        }else{
                            $jobfetch['cname']="";
                        }
                        if($jobfetch['latitude']){
                            $jobfetch['latitude']=$jobfetch['latitude'];
                        }else{
                            $jobfetch['latitude']="";
                        }
                        if($jobfetch['longitude']){
                            $jobfetch['longitude']=$jobfetch['longitude'];
                        }else{
                            $jobfetch['longitude']="";
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['company_id']);
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = $companyPic[0]['companyPic'];
                        } else{
                            $comPic = " ";
                        }
                        $sal = $this->Jobpost_Model->fetch_highsalary($jobfetch['id']);
                        $image = $this->Jobpost_Model->job_image($jobfetch['id']);
                        
                        
                            if(!empty($image[0]['pic'])) {
                                $jobpic = $image[0]['pic'];
                            } else{
                                $jobpic = " ";
                            }
                        $jobListing[] = [
                                "jobpost_id" => $jobfetch['id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "jobDesc" => $jobfetch['jobDesc'],
                                "salary" => number_format($sal[0]['salary']),
                                "companyName" => $jobfetch['cname'],
                                "latitude" => $jobfetch['latitude'],
                                "longitude" => $jobfetch['longitude'],
                                "address" => $jobfetch['address'],
                                "jobcount" =>1,
                                "toppicks1" => $dataJobList[0],
                                "toppicks2" => $dataJobList[1],
                                "toppicks3" => $dataJobList[2],
                                "companyPic" => $comPic,
                                "jobpic" => $jobpic
                            ];
                    $x++;
                }
                if(count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else{
                $data["jobList"] = [];
            }
            
            //print_r($jobListing);exit();
            if($filterData['salarySort'] == 1) {
                        
                foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
                }
                array_multisort($price, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            }
             /*else if($filterData['locationSort'] == 1) {
                foreach ($jobListing as $key => $row) {
                    $locate[$key] = $row['address'];
                }
                array_multisort($locate, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            } */
            else{
                $data["jobList"] = $jobListing;
            }


            $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id']);
            //print_r($jobcountfetchs);exit();
            if($jobcountfetchs && $jobcountfetchs[0]['jobcount'] != 0) {
                
                foreach ($jobcountfetchs as $jobcountfetch) {

                        $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }

                        $sal = $this->Jobpost_Model->fetch_highsalary($jobcountfetch['id']);

                        $jobcount[] = [
                                "company_id" => $jobcountfetch['company_id'],
                                "salary" => number_format($sal[0]['salary']),
                                "latitude" => $jobcountfetch['latitude'],
                                "longitude" => $jobcountfetch['longitude'],
                                "jobcount" => $jobcountfetch['jobcount'],
                                "toppicks1" => $dataJobList[0],
                                "toppicks2" => $dataJobList[1],
                                "toppicks3" => $dataJobList[2]
                            ];
                }  

                if($filterData['salarySort'] == 1) {
                    
                    $price = array();
        
                    foreach ($jobcount as $key => $row) {
                        $price[$key] = $row['salary'];
                    }
                   
                    array_multisort($price, SORT_DESC, $jobcount);
                    $data["jobcount"] = $jobListing;

                }else{
                    $data["jobcount"] = $jobListing;
                }
                
            } else{
                $data['jobcount'] = [];
            }
            
            $response = ['jobFilters' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobtitlecache_post() {
        $cacheData = $this->input->post();
        $data = ["token" => $cacheData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {
            
            if(!empty($cacheData['title'])) {
              $cachecheck = $this->Jobpost_Model->jobtitlecache_check($cacheData['title'], $userTokenCheck[0]['id']);
              
              if(!$cachecheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'], "title"=> $cacheData['title']];
                  $this->Jobpost_Model->jobtitlecache_insert($data);
              }
            }
            $cacheGet = $this->Jobpost_Model->jobtitlecache_fetch($userTokenCheck[0]['id']);
            if($cacheGet) {

            } else{
              $cacheGet = [];
            }
            $response = ['jobtitlecache' => $cacheGet, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function keywordcache_post() {
        $cacheData = $this->input->post();
        $data = ["token" => $cacheData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {
            
            if(!empty($cacheData['keyword'])) {
              $cachecheck = $this->Jobpost_Model->keywordcache_check($cacheData['keyword'], $userTokenCheck[0]['id']);
              
              if(!$cachecheck) {
                  $data = ["user_id" => $userTokenCheck[0]['id'], "keyword"=> $cacheData['keyword']];
                  $this->Jobpost_Model->keywordcache_insert($data);
              }
            }
            $cacheGet = $this->Jobpost_Model->keywordcachee_fetch($userTokenCheck[0]['id']);
            if($cacheGet) {

            } else{
              $cacheGet = [];
            }
            $response = ['keywordcache' => $cacheGet, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function companySchedule_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {  
            $getDays = $this->Jobpost_Model->getCompDetails($jobData['companyId']);
            
            $days1 = $getDays[0]['dayfrom'];
            $days2 = $getDays[0]['dayto'];
            $z=1;

            for($i=$days1; $i<=$days2; $i++) {
                if($i == 1) {
                    $dayname = "Monday";
                } else if($i == 2) {
                    $dayname = "Tuesday";
                } else if($i == 3) {
                    $dayname = "Wednesday";
                } else if($i == 4) {
                    $dayname = "Thursday";
                } else if($i == 5) {
                    $dayname = "Friday";
                } else if($i == 6) {
                    $dayname = "Saturday";
                } else if($i == 7) {
                    $dayname = "Sunday";
                }
                $data['days'][] = $dayname;
              $z++;
            }

            $time1 = $getDays[0]['from_time'];
            $time11 = explode(":", $time1);
            $time111 = $time11[0];
            $time2 = $getDays[0]['to_time'];
            $time22 = explode(":", $time2);
            $time222 = $time22[0];

            $data['timeFrom'] = $time111;
            $data['timeTo'] = $time222;

            //var_dump($data);die;
            
            $response = ['companySchedule' => $data, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function jobtitlelist_post() {
        $jobData = $this->input->post();
        $data = ["token" => $jobData['token']];
        $userTokenCheck = $this->User_Model->token_match($data);
        
        if($userTokenCheck) {  
            $jobtitle = $this->Common_Model->getCompJobTitle();
            $response = ['jobtitlelist' => $jobtitle, 'status' => "SUCCESS"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);

        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) *sin(deg2rad($lat2)) + cos(deg2rad($lat1))* cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist* 60 *1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function nearJobs1($id, $userLat, $userLong, $exp_month, $exp_year) {
        
        $data1 = array();
        $expmonth = $exp_month;
        $expyear = $exp_year;

        if($expyear == 0 && $expmonth == 0) {
            $expfilter = "1";        
        } else if($expyear == 0 && $expmonth < 6) {
            $expfilter = "2";
        } else if($expyear < 1 && $expmonth >= 6) {
            $expfilter = "3";  
        } else if($expyear < 2 && $expyear >= 1) {
            $expfilter = "4";
        } else if($expyear < 3 && $expyear >= 2) {
            $expfilter = "5";
        } else if($expyear < 4 && $expyear >= 3) {
            $expfilter = "6";  
        } else if($expyear < 5 && $expyear >= 4) {
            $expfilter = "7";  
        } else if($expyear < 6 && $expyear >= 5) {
            $expfilter = "8";  
        } else if($expyear < 7 && $expyear >= 6) {
            $expfilter = "9";  
        } else if($expyear >= 7) { 
            $expfilter = "10";
        } else {
            $expfilter = "";
        }

        $jobfetchs = $this->Jobpost_Model->job_fetch_homeforappnearby($id, $userLat, $userLong, $expfilter);
        if ($jobfetchs) {
            foreach ($jobfetchs as $jobfetch) {

                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $id);
                    $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = '1';
                    } else {
                        $jobstatus = '0';
                    }

                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                       
                    $data1[] = ["jobpost_id" => $jobfetch['id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $jobfetch['cname'],"cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$cname,"distance"=>$distance, "savedjob" => $jobstatus];
            }
        }

        return $data1;
    }

    public function nearJobs2($id, $userLat, $userLong, $expfilter, $companyidsarray) {
        $data1 = array();
        $neglectarr = array();

        foreach($companyidsarray as $companyidsarr) {

            $jobfetchs = $this->Homejob_Model->job_fetch_homeforappnearby_groupby($id, $userLat, $userLong, $expfilter, $companyidsarr);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {

                    $neglectarr[] = $jobfetch['id'];
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $id);
                    $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = '1';
                    } else {
                        $jobstatus = '0';
                    }

                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');

                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImageData = $comPic;
                    }
                           
                    $data1[] = ["jobpost_id" => $jobfetch['id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $jobfetch['cname'],"cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$cname,"distance"=>$distance, "savedjob" => $jobstatus];
                }
            }
        }
        usort($data1, function($a, $b) {
                    return $a['distance'] <=> $b['distance'];
                });

        if(count($data1) <= 6) {
            $jobfetchs = $this->Jobpost_Model->job_fetch_homeforappnearby($id, $userLat, $userLong, $expfilter);
            if ($jobfetchs) {

                $aa=1;
                foreach ($jobfetchs as $jobfetch) {

                    if(is_array($jobfetch['id'], $neglectarr)) { } else {

                        if($aa <= 2) {

                            $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $id);
                            $jobstatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobstatus[0]['status'])) {
                                $jobstatus = '1';
                            } else {
                                $jobstatus = '0';
                            }

                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                        
                            $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                            if ($jobTop) {
                                if (isset($jobTop[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop[0]['picks_id'];
                                } else {
                                    $datatoppicks1 = "";
                                }
                                if (isset($jobTop[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop[1]['picks_id'];
                                } else {
                                    $datatoppicks2 = "";
                                }
                                if (isset($jobTop[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop[2]['picks_id'];
                                } else {
                                    $datatoppicks3 = "";
                                }
                            } else {
                                $datatoppicks1 = "";
                                $datatoppicks2 = "";
                                $datatoppicks3 = "";
                            }

                            $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($userLat,$userLong, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            
                            $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);       
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImageData = $comPic;
                            }
                               
                            $data1[] = ["jobpost_id" => $jobfetch['id'], "comapnyId" => $jobfetch['compId'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => number_format($basicsalary), "companyName" => $jobfetch['cname'],"cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData, "cname"=>$cname,"distance"=>$distance, "savedjob" => $jobstatus];
                        }
                        $aa++;
                    }
                }
            }
        }

        return $data1;
    }

    public function getFeaturedTopPicks($uid, $current_lat, $current_long, $expfilter) {

        $dataTopicks["monthpay"] = array();
        $dataTopicks["shiftjobs"] = array();
        $dataTopicks["retirement"] = array();
        $dataTopicks["day1hmo"] = array();
        $dataTopicks["freefood"] = array();
        $dataTopicks["bonus"] = array();
        $dataTopicks["workfromhome"] = array();
        $dataaaaaa = array();

        $hotjobData = $this->Jobpost_Model->feturedtoppicks_fetch_latlong($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobData) {
            $jobarray = array();

            foreach ($hotjobData as $hotjobsData) {
                
                    $jobTop = $this->Jobpost_Model->job_toppicks1($hotjobsData['id']);
                    $toppic = array();

                    foreach($jobTop as $jobT) {
                        $toppic[] = $jobT['picks_id'];
                    }

                    if (!empty($jobTop) && count($jobTop) >= 1) {

                        if(in_array(1, $toppic)) {

                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 1);
                            $datatoppicks1 = "1"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = '';
                            }
                            $dataTopicks["bonus"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'], 
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                    ];
                        }

                        if(in_array(2, $toppic)) {

                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 2);
                            $datatoppicks1 = "2"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = '';
                            }

                            $dataTopicks["freefood"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'], 
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                    ];

                        }

                        if(in_array(3, $toppic)) {

                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 3);
                            $datatoppicks1 = "3"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = '';
                            }

                            $dataTopicks["day1hmo"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'], 
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                    ];
                        
                        }

                        if(in_array(5, $toppic)) {

                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                            $datatoppicks1 = "5"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = '';
                            }

                            $dataTopicks["shiftjobs"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'], 
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                    ];

                        }

                        if(in_array(6, $toppic)) {

                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 6);
                            $datatoppicks1 = "6"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = '';
                            }

                            $dataTopicks["monthpay"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'], 
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                    ];

                        }

                        if(in_array(7, $toppic)) {

                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 7);
                            $datatoppicks1 = "7"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = '';
                            }

                            $dataTopicks["workfromhome"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'], 
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                    ];

                        }
                    }
                        
                        $jobTop1 = $this->Jobpost_Model->job_allowancesbyid($hotjobsData['id'], 5);
                        $toppic1 = array();

                        foreach($jobTop1 as $jobT1) {
                            $toppic1[] = $jobT1['picks_id'];
                        }

                        if (!empty($jobTop1) && count($jobTop1) >= 1) {

                            if(in_array(5, $toppic1)) {

                                $jobarray[] = $hotjobsData['id'];

                                //$jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                                if(isset($jobTop1[0]['picks_id'])) {
                                    $datatoppicks1 = $jobTop1[0]['picks_id']; 
                                } else{
                                    $datatoppicks1 = "";
                                }
                                if(isset($jobTop1[1]['picks_id'])) {
                                    $datatoppicks2 = $jobTop1[1]['picks_id']; 
                                } else{
                                    $datatoppicks2 = "";
                                }
                                if(isset($jobTop1[2]['picks_id'])) {
                                    $datatoppicks3 = $jobTop1[2]['picks_id']; 
                                } else{
                                    $datatoppicks3 = "";
                                }

                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                    
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }

                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = $comPic;
                                }
                                
                                $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                                $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                                if (isset($jobsavedStatus[0]['status'])) {
                                    $jobsstatus = $jobsavedStatus[0]['status'];
                                } else {
                                    $jobsstatus = '';
                                }
                                
                                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                                if ($jobsal[0]['basicsalary']) {
                                    $basicsalary = $jobsal[0]['basicsalary'];
                                } else {
                                    $basicsalary = "0";
                                }
                                
                                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                                if (isset($companydetail1[0]['address'])) {
                                    $companyaddress = $companydetail1[0]['address'];
                                } else {
                                    $companyaddress = '';
                                }
                                
                                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                                if(!empty($companyname[0]["cname"])){
                                    $cname = $companyname[0]["cname"];
                                }else{
                                    $cname = '';
                                }

                                $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', '');
                                //$distance = '';
                                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                                if (isset($jobImage[0]['pic'])) {
                                    $jobImageData = $jobImage[0]['pic'];
                                } else {
                                    $jobImageData = '';
                                }

                                $dataTopicks["retirement"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'], 
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                ];
                            }
                        }
                        
                    
            }

        } else {

            $dataTopicks["bonus"] = [];
            $dataTopicks["freefood"] = [];
            $dataTopicks["day1hmo"] = [];
            $dataTopicks["shiftjobs"] = [];
            $dataTopicks["monthpay"] = [];
            $dataTopicks["retirement"] = [];
            $dataTopicks["workfromhome"] = [];
        }

        return $dataTopicks;

    }

    public function getFeaturedTopPicks1($uid, $current_lat, $current_long, $expfilter, $companyidsarray) {

        $dataTopicks["monthpay"] = array();
        $dataTopicks["shiftjobs"] = array();
        $dataTopicks["retirement"] = array();
        $dataTopicks["day1hmo"] = array();
        $dataTopicks["freefood"] = array();
        $dataTopicks["bonus"] = array();
        $dataTopicks["workfromhome"] = array();


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr = array();
            foreach($companyidsarray as $companyidsarr) {

                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                if ($hotjobDataonegroup) {
                    foreach ($hotjobDataonegroup as $hotjobsData) {
                        $neglectarr[] = $hotjobsData['id'];
                        $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 1);
                        $datatoppicks1 = "1"; 
                        
                        if(isset($jobTopseleted[0]['picks_id'])) {
                            $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTopseleted[1]['picks_id'])) {
                            $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }

                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }
                            $jobImageData = $comPic;
                        }
                        
                        $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                        $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobsavedStatus[0]['status'])) {
                            $jobsstatus = '1';
                        } else {
                            $jobsstatus = '0';
                        }
                        
                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                        //$distance = '';

                        $dataTopicks["bonus"][] = ["jobpost_id" => $hotjobsData['id'], 
                                  "comapnyId" => $hotjobsData['compId'],
                                  "recruiter_id" => $hotjobsData['recruiter_id'],
                                  "job_title" => $hotjobsData['jobtitle'], 
                                  "jobDesc" => $hotjobsData['jobDesc'], 
                                  "salary" => number_format($basicsalary), 
                                  "companyName" => $companydetail[0]["cname"], 
                                  "companyAddress" => $companyaddress,
                                  "job_image" => $jobImageData,
                                  "jobPitch"=>$hotjobsData['jobPitch'], 
                                  "cname"=>$cname, 
                                  "distance" =>$distance, 
                                  'job_image' =>$jobImageData,
                                  "savedjob"=> $jobsstatus,
                                  "toppicks1" => $datatoppicks1,
                                  "toppicks2" => $datatoppicks2,
                                  "toppicks3" => $datatoppicks3,
                                  "mode"=>$hotjobsData['mode'], 
                        ];

                    }
                }
            }

            usort($dataTopicks["bonus"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            if(count($dataTopicks["bonus"]) <= 8) {
                $a=1;
                foreach ($hotjobDataone as $hotjobsData) {
                    
                    if(is_array($hotjobsData['id'], $neglectarr)) { } else {

                        if($a <= 10) {
                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 1);
                            $datatoppicks1 = "1"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';

                            $dataTopicks["bonus"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'],
                                      "recruiter_id" => $hotjobsData['recruiter_id'],
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                      "mode"=>$hotjobsData['mode'], 
                            ];
                        }
                        $a++;
                    }
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_freefood($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $neglectarr1 = array();
            foreach($companyidsarray as $companyidsarr) {

                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_freefood_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);

                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr1[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 2);
                    $datatoppicks1 = "2"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';

                    $dataTopicks["freefood"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["freefood"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            if(count($dataTopicks["freefood"]) <= 8) {
                $b=1;
                foreach ($hotjobDataone as $hotjobsData) {

                    if(is_array($hotjobsData['id'], $neglectarr1)) { } else {

                        if($b <= 6) {
                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 2);
                            $datatoppicks1 = "2"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';

                            $dataTopicks["freefood"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'],
                                      "recruiter_id" => $hotjobsData['recruiter_id'],
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                      "mode"=>$hotjobsData['mode'], 
                            ];
                        }
                        $b++;
                    }
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_dayihmo($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr2 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_dayihmo_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr2[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 3);
                    $datatoppicks1 = "3"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $dataTopicks["day1hmo"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["day1hmo"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            if(count($dataTopicks["day1hmo"]) <= 8) {
                $c=1;
                foreach ($hotjobDataone as $hotjobsData) {

                    if(is_array($hotjobsData['id'], $neglectarr2)) { } else {
                        
                        if($c <= 6) {

                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 3);
                            $datatoppicks1 = "3"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';

                            $dataTopicks["day1hmo"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'],
                                      "recruiter_id" => $hotjobsData['recruiter_id'],
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                      "mode"=>$hotjobsData['mode'], 
                            ];
                        }
                        $c++;
                    }
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_shift($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr3 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_shift_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr3[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                    $datatoppicks1 = "5"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';

                    $dataTopicks["shiftjobs"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["shiftjobs"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            if(count($dataTopicks["shiftjobs"]) <= 8) {
                $d=1;
                foreach ($hotjobDataone as $hotjobsData) {

                    if(is_array($hotjobsData['id'], $neglectarr3)) { } else {

                        if($d <= 10) {

                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                            $datatoppicks1 = "5"; 
                            
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';

                            $dataTopicks["shiftjobs"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'],
                                      "recruiter_id" => $hotjobsData['recruiter_id'],
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                      "mode"=>$hotjobsData['mode'], 
                            ];
                        }
                        $d++;
                    }
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_monthpay($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr4 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_monthpay_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr4[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 6);
                    $datatoppicks1 = "6"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';

                    $dataTopicks["monthpay"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["monthpay"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            if(count($dataTopicks["monthpay"]) <= 8) {
                $e=1;
                foreach ($hotjobDataone as $hotjobsData) {

                    if(is_array($hotjobsData['id'], $neglectarr4)) { } else {

                        if($e <= 10) {
                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 6);
                            $datatoppicks1 = "6"; 
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';

                            $dataTopicks["monthpay"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'],
                                      "recruiter_id" => $hotjobsData['recruiter_id'],
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                      "mode"=>$hotjobsData['mode'], 
                            ];
                        }
                        $e++;
                    }
                }
            }
        }

        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_workfromhome($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr5 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_workfromhome_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr5[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 7);
                    $datatoppicks1 = "7"; 
                    
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';

                    $dataTopicks["workfromhome"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }

            usort($dataTopicks["workfromhome"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            if(count($dataTopicks["workfromhome"]) <= 8) {
                $f=1;
                foreach ($hotjobDataone as $hotjobsData) {

                    if(is_array($hotjobsData['id'], $neglectarr5)) { } else {

                        if($f <= 6) {
                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 7);
                            $datatoppicks1 = "7";
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';

                            $dataTopicks["workfromhome"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'],
                                      "recruiter_id" => $hotjobsData['recruiter_id'],
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                      "mode"=>$hotjobsData['mode'], 
                            ];
                        }
                        $f++;
                    }
                }
            }
        }


        $hotjobDataone = $this->Homejob_Model->feturedtoppicks_fetch_latlong_bonus_allowances($uid, $expfilter, $current_lat, $current_long);
        if ($hotjobDataone) {
            $jobarray = array();
            $neglectarr6 = array();
            foreach($companyidsarray as $companyidsarr) {
                $hotjobDataonegroup = $this->Homejob_Model->feturedtoppicks_fetch_latlong_allowances_groupby($uid, $expfilter, $current_lat, $current_long, $companyidsarr);
                foreach ($hotjobDataonegroup as $hotjobsData) {
                    $neglectarr6[] = $hotjobsData['id'];
                    $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                    $datatoppicks1 = "5";
                    if(isset($jobTopseleted[0]['picks_id'])) {
                        $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTopseleted[1]['picks_id'])) {
                        $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImageData = $comPic;
                    }
                    
                    $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = '1';
                    } else {
                        $jobsstatus = '0';
                    }
                    
                    $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    
                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    //$distance = '';

                    $dataTopicks["retirement"][] = ["jobpost_id" => $hotjobsData['id'], 
                              "comapnyId" => $hotjobsData['compId'],
                              "recruiter_id" => $hotjobsData['recruiter_id'],
                              "job_title" => $hotjobsData['jobtitle'], 
                              "jobDesc" => $hotjobsData['jobDesc'], 
                              "salary" => number_format($basicsalary), 
                              "companyName" => $companydetail[0]["cname"], 
                              "companyAddress" => $companyaddress,
                              "job_image" => $jobImageData,
                              "jobPitch"=>$hotjobsData['jobPitch'], 
                              "cname"=>$cname, 
                              "distance" =>$distance, 
                              'job_image' =>$jobImageData,
                              "savedjob"=> $jobsstatus,
                              "toppicks1" => $datatoppicks1,
                              "toppicks2" => $datatoppicks2,
                              "toppicks3" => $datatoppicks3,
                              "mode"=>$hotjobsData['mode'], 
                    ];

                }
            }
            
            usort($dataTopicks["retirement"], function($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            if(count($dataTopicks["retirement"]) <= 8) {
                $g=1;
                foreach ($hotjobDataone as $hotjobsData) {

                    if(is_array($hotjobsData['id'], $neglectarr6)) { } else {

                        if($g <=5) {

                            $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], 5);
                            $datatoppicks1 = "5"; 
                            if(isset($jobTopseleted[0]['picks_id'])) {
                                $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                            } else{
                                $datatoppicks2 = "";
                            }
                            if(isset($jobTopseleted[1]['picks_id'])) {
                                $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                            } else{
                                $datatoppicks3 = "";
                            }

                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                                if(!empty($companyPic[0]['companyPic'])) {
                                    $comPic = trim($companyPic[0]['companyPic']);
                                } else{
                                    $comPic = "";
                                }
                                $jobImageData = $comPic;
                            }
                            
                            $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $uid);
                            $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                            if (isset($jobsavedStatus[0]['status'])) {
                                $jobsstatus = '1';
                            } else {
                                $jobsstatus = '0';
                            }
                            
                            $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                            if ($jobsal[0]['basicsalary']) {
                                $basicsalary = $jobsal[0]['basicsalary'];
                            } else {
                                $basicsalary = "0";
                            }
                            
                            $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                            $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                            if (isset($companydetail1[0]['address'])) {
                                $companyaddress = $companydetail1[0]['address'];
                            } else {
                                $companyaddress = '';
                            }
                            
                            $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                            if(!empty($companyname[0]["cname"])){
                                $cname = $companyname[0]["cname"];
                            }else{
                                $cname = '';
                            }

                            $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';

                            $dataTopicks["retirement"][] = ["jobpost_id" => $hotjobsData['id'], 
                                      "comapnyId" => $hotjobsData['compId'],
                                      "recruiter_id" => $hotjobsData['recruiter_id'],
                                      "job_title" => $hotjobsData['jobtitle'], 
                                      "jobDesc" => $hotjobsData['jobDesc'], 
                                      "salary" => number_format($basicsalary), 
                                      "companyName" => $companydetail[0]["cname"], 
                                      "companyAddress" => $companyaddress,
                                      "job_image" => $jobImageData,
                                      "jobPitch"=>$hotjobsData['jobPitch'], 
                                      "cname"=>$cname, 
                                      "distance" =>$distance, 
                                      'job_image' =>$jobImageData,
                                      "savedjob"=> $jobsstatus,
                                      "toppicks1" => $datatoppicks1,
                                      "toppicks2" => $datatoppicks2,
                                      "toppicks3" => $datatoppicks3,
                                      "mode"=>$hotjobsData['mode'], 
                            ];
                        }
                        $g++;
                    }
                }
            }
        }

        return $dataTopicks;

    }

    public function fetchUserCompleteData($id) {
        $userData = $this->User_Model->userComplete_single($id);
        
        if($userData) {
          $data['comp'] = $userData[0]['signup'] + $userData[0]['profile'] + $userData[0]['profilePic'] + $userData[0]['resume'] + $userData[0]['experience'] + $userData[0]['education'] + $userData[0]['assessment'] + $userData[0]['language'] + $userData[0]['expert'] + $userData[0]['topClient']+ $userData[0]['dob']+ $userData[0]['location']+ $userData[0]['designation']+ $userData[0]['current_salary']+ $userData[0]['exp'];
        } else{
          $data['comp'] = 0;
        }
        return $data;
    }


    public function instantscreening_post() {
        $userData = $this->input->post();
        $data = ["token" => $userData['token']];
        $userTokenCheck1 = $this->User_Model->token_match1($data);

        $jobpost_id = $userData['jobpost_id'];
        
        if($userTokenCheck1) {
            $user_id = $userTokenCheck1[0]['user_id'];

            $checkScreening = $this->Jobpost_Model->check_phone_screening($user_id, $jobpost_id);
            
            if($checkScreening) {

                $response = ['instantscreening' => "SUCCESS", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);

            } else {

                $data = ["user_id"=>$user_id, "jobpost_id"=>$jobpost_id, "status"=>1];
                $this->Jobpost_Model->job_screening($data);

                $response = ['instantscreening' => "SUCCESS", 'status' => "SUCCESS"];
                $this->set_response($response, REST_Controller::HTTP_CREATED);
            }

        } else {

            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

}
?>
