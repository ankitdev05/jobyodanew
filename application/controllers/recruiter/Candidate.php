<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
define('API_ACCESS_KEY','AIzaSyCTzjJxETlJxp18hCwYHLFETLZRkbGFiGw');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Candidate extends CI_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Candidate_Model');
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Common_Model');
        $this->load->model('User_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');

        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
    }

    public function index() {
        $userSession = $this->session->userdata('userSession');
        $jobtype = $_GET['type'];
        $data['hiredlist'] = $this->Candidate_Model->hired_list($jobtype);
        $data['rejectedlist'] = $this->Candidate_Model->rejected_list($jobtype);
        $data['referedlist'] = $this->Candidate_Model->refered_list($jobtype);
        $data['jobdetail'] = $this->Candidate_Model->job_detail($jobtype);
        
        $newApps = $this->Candidate_Model->newApplication_list($jobtype);
        //echo $this->db->last_query();die;
       $newApp1 = array();
        foreach($newApps as $newApp) {
            $getRate = $this->calculateRating($newApp['user_id']);
            $newApp1[] = ['jobpost_id'=>$newApp['jobpost_id'], 'user_id'=>$newApp['user_id'],'designation'=>$newApp['designation'], 'interviewdate'=>$newApp['interviewdate'], 'interviewtime'=>$newApp['interviewtime'], 'name'=>$newApp['name'], 'email'=>$newApp['email'], 'location'=>$newApp['location'], 'phone'=>$newApp['phone'],'country_code'=>$newApp['country_code'], 'profilePic'=>$newApp['profilePic'], "rating"=>$getRate, "status"=>$newApp['status'], "id"=> $newApp['id'], 'compid'=>$newApp['recruiter_id'],'jobtitle'=>$newApp['jobtitle']];
        }
        $data['newApp'] = $newApp1;

        $goingApps = $this->Candidate_Model->goingApplication_list($jobtype);
        //echo $this->db->last_query();die;
        $goingApp1 = array();
        foreach($goingApps as $goingApp) {
            $getRate = $this->calculateRating($goingApp['user_id']);
            $goingApp1[] = ['jobpost_id'=>$goingApp['jobpost_id'], 'user_id'=>$goingApp['user_id'],'designation'=>$goingApp['designation'], 'interviewdate'=>$goingApp['interviewdate'], 'interviewtime'=>$goingApp['interviewtime'], 'name'=>$goingApp['name'], 'email'=>$goingApp['email'], 'location'=>$goingApp['location'], 'phone'=>$goingApp['phone'],'country_code'=>$goingApp['country_code'], 'profilePic'=>$goingApp['profilePic'], "rating"=>$getRate, "status"=>$goingApp['status'], "id"=> $goingApp['id'], 'compid'=>$goingApp['recruiter_id'],'jobtitle'=>$goingApp['jobtitle']];
        }
        $data['goingApp'] = $goingApp1;

        $acceptedApps = $this->Candidate_Model->acceptedApplication_list($jobtype);
        $acceptedApp1 = array();
        foreach($acceptedApps as $acceptedApp) {
            $getRate = $this->calculateRating($acceptedApp['user_id']);
            $acceptedApp1[] = ['jobpost_id'=>$acceptedApp['jobpost_id'], 'user_id'=>$acceptedApp['user_id'], 'designation'=>$acceptedApp['designation'], 'interviewdate'=>$acceptedApp['date_day1'], 'interviewtime'=>$acceptedApp['interviewtime'], 'name'=>$acceptedApp['name'], 'email'=>$acceptedApp['email'], 'location'=>$acceptedApp['location'], 'phone'=>$acceptedApp['phone'],'country_code'=>$acceptedApp['country_code'], 'profilePic'=>$acceptedApp['profilePic'], "rating"=>$getRate, "status"=>$acceptedApp['status'], "id"=> $acceptedApp['id'], "accepted_date" => $acceptedApp['accepted_date'], 'compid'=>$acceptedApp['recruiter_id'],'acceptedApp'=>$acceptedApp['jobtitle']];
        }
        $data['acceptedApp'] = $acceptedApp1;
        
        $this->load->view('recruiter/candidate_profile_list', $data);
    }

    public function candidate_lists() {
        $jobtype = $_GET['type'];
        $jobstatus = trim($_GET['status']);
        $data['hiredlist'] = $this->Candidate_Model->hired_list($jobtype);
        $data['rejectedlist'] = $this->Candidate_Model->rejected_list($jobtype);
        $data['referedlist'] = $this->Candidate_Model->refered_list($jobtype);
        $data['jobdetail'] = $this->Candidate_Model->job_detail($jobtype);
        $data['goingApp'] = $this->Candidate_Model->goingApplication_list($jobtype);
        $userSession = $this->session->userdata('userSession');
        

        if($jobstatus == 1) {
            $data['alllists'] = $this->Candidate_Model->hiredapplication_list($jobtype);
            
        } else if($jobstatus == 2) {
            $data['alllists'] = $this->Candidate_Model->referapplication_list($jobtype);
            
        } else if($jobstatus == 3) {
            $data['alllists'] = $this->Candidate_Model->falloutapplication_list($jobtype);
        }
        //echo $this->db->last_query();die;
        $this->load->view('recruiter/candidate_profile_list_status', $data);
    }
    
    public function candidate_suggested() {
        $userSession = $this->session->userdata('userSession');
        $jobtype = base64_decode($_GET['type']);

        $data['jobdetail'] = $this->Candidate_Model->job_detail($jobtype);

        $getCatData = $this->Common_Model->subcategory_listsbysubcatid($data['jobdetail'][0]['subcategory']);

        $experience = $data['jobdetail'][0]['experience'];
        if($experience == "No Experience") {
            $expfilterMonth = "0";
            $expfilterYear = "0";

        }else if($experience == "< 6 months") {
            $expfilterMonth = "0";
            $expfilterYear = "0";

        } else if($experience == "> 6 months") {
            $expfilterMonth = "6";
            $expfilterYear = "0";

        } else if($experience == "> 1 yr") {
            $expfilterMonth = "0";
            $expfilterYear = "1";

        } else if($experience == "> 2 yr") {
            $expfilterMonth = "0";
            $expfilterYear = "2";

        }else if($experience == "> 3 yr") {
            $expfilterMonth = "0";
            $expfilterYear = "3";

        }else if($experience == "> 4 yr") {
            $expfilterMonth = "0";
            $expfilterYear = "4";

        }else if($experience == "> 5 yr") {
            $expfilterMonth = "0";
            $expfilterYear = "5";

        }else if($experience == "> 6 yr") {
            $expfilterMonth = "0";
            $expfilterYear = "6";  

        } else if($experience == "> 7 yr") { 
            $expfilterMonth = "0";
            $expfilterYear = "7";
        }

        $designation = $data['jobdetail'][0]['jobtitle'];
        $return_array = array(); 
        
        $suggest = $this->Candidate_Model->suggestedcandidatebyintrested($getCatData[0]['subcategory']);

        foreach($suggest as $value) {
            if(!empty($value)) {
             
              $return_array[]= $value['id'];

              if($expfilterYear == 0) {
                  if($value['exp_month'] <= $expfilterMonth) {
                      $return_array[] =  $value['id'];
                  }
              } else {
                  if($value['exp_year'] <= $expfilterYear) {
                      $return_array[] =  $value['id'];
                  }

              }
            
            }
        }

        // $user_desgexp = $this->Candidate_Model->user_list();
        // foreach($user_desgexp as $key) {

          

        // }

        $return_array = array_unique($return_array);

        foreach ($return_array as $key =>  $value) {
             $fetchcandidatedetails = $this->Candidate_Model->fetchcandidate($value,$jobtype);

             if(!empty($fetchcandidatedetails)) {
                unset($return_array[$key]);
             }
        }
       
        //var_dump($return_array);die;
        $return_array = array_unique($return_array);
        $xx=1;
        foreach($return_array as $userid) {
           $candiade = $this->Candidate_Model->fetchsuggested($userid);
            if(!empty($candiade)) {

                if($xx <= 6) {
                    $data['candidatedetails'][] = $candiade[0];
                }
            }
            $xx++;
        }

        // echo "<PRE>";
        // var_dump($data);die;
        
 
        $this->load->view('recruiter/candidate_suggested', $data);
    }

    public function manageCandidates() {
        $userSession = $this->session->userdata('userSession');
         if(!empty($userSession['label']) && $userSession['label']=='3'){
              $subrecruiter_id = $userSession['id'];
              $userSession['id'] = $userSession['parent_id'];
          }else{
              $userSession['id'] = $userSession['id'];
              $subrecruiter_id = 0;
          }
        if(!empty($_GET['status'])){
            $status = $_GET['status'];
        }
        if(!empty($_GET['id'])){
            $site_id = base64_decode($_GET['id']);
        }
        if(!empty($_GET['app_status'])){
            $app_status = $_GET['app_status'];
        }

        if(!empty($_GET['filters'])){
            $filters = $_GET['filters'];
        }else{
          $filters="Active";
        }
        
        if(!empty($status)){
            $data['candidatesApplied1'] = $this->Candidate_Model->candidateApplied_list1($userSession['id'],$status,$subrecruiter_id,$site_id,$filters );
            //print_r($data['candidatesApplied']);
            //echo $this->db->last_query();
            //die;
        }else{
           $data['candidatesApplied1'] = $this->Candidate_Model->candidateApplied_list($userSession['id'],$subrecruiter_id ); 
           //echo $this->db->last_query();die;
        }

        if(!empty($app_status)){
          $data['candidatesApplied1'] = $this->Candidate_Model->candidateApplied_list2($userSession['id'],$app_status,$subrecruiter_id ); 
        }
        
        $data["languages"] = $this->Common_Model->language_lists();
        $data["industries"] = $this->Common_Model->industry_lists();
        $data['skillData'] = $this->Candidate_Model->skills_fetch();
        $data['clients'] = $this->Recruit_Model->user_clientSupported();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data["site_name"] = $this->Candidate_Model->site_lists($userSession['id']);
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/manage_candidate', $data);
    }
    
    public function filtermanagecandidate() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
              $subrecruiter_id = $userSession['id'];
              $userSession['id'] = $userSession['parent_id'];
          }else{
              $userSession['id'] = $userSession['id'];
              $subrecruiter_id = 0;
          }
         $filterData = $this->input->post();
         //print_r($filterData);die;
        $data['candidatesApplied1'] = $this->Candidate_Model->Applied_listfilter($userSession['id'], $filterData);
        //print_r($data['candidatesApplied1']);die;
        //echo $this->db->last_query();die;
        $data["site_name"] = $this->Candidate_Model->site_lists($userSession['id']);
        $data["languages"] = $this->Common_Model->language_lists();
        $data["industries"] = $this->Common_Model->industry_lists();
        $data['skillData'] = $this->Candidate_Model->skills_fetch();
        $data['clients'] = $this->Recruit_Model->user_clientSupported();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
	       $data['filterdata'] = $filterData;
         $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/manage_candidate', $data);
    }

    public function searchCandidates() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
              $subrecruiter_id = $userSession['id'];
              $userSession['id'] = $userSession['parent_id'];
          }else{
              $userSession['id'] = $userSession['id'];
              $subrecruiter_id = 0;
          }
        $keyword = $this->input->post();
        if(!empty($keyword['keyword'])){
          $key = $keyword['keyword'];
        }
        else{
          $key= '';
        }
        //$data['candidatesApplied1'] = $this->Candidate_Model->candidateApplied_listsearch($userSession['id'], $key);
        $data['candidatesApplied1'] = $this->Candidate_Model->candidateApplied_listsearch_byrecruiterid($userSession['id'], $key);
        //echo $this->db->last_query();die;
        $data["languages"] = $this->Common_Model->language_lists();
        $data["industries"] = $this->Common_Model->industry_lists();
        $data['skillData'] = $this->Candidate_Model->skills_fetch();
        $data['clients'] = $this->Recruit_Model->user_clientSupported();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $data['keyword'] = $key;
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/search_candidate', $data);
    }

    public function sendNotification() {
      $uid = $this->input->post('uid');
      $jid = $this->input->post('job_id');
      $user_data = $this->User_Model->user_single($uid);
      $fetchnotifications = $this->Candidate_Model->fetchnotifications($uid);
      $tokendata = ['user_id'=>$uid];
      $userTokenCheck = $this->User_Model->user_token_check($tokendata);
      //print_r($userTokenCheck);die;
      
      foreach ($userTokenCheck as $usertoken) {
      
        if($usertoken['device_type']=="android") {
            $res1 = $this->push_notification_suggested($usertoken['device_token'],'New Job', 'There is a job suggested for you','suggest job',$jid,count($fetchnotifications));

        }else if($usertoken['device_type']=="ios") {
            //echo $recruiter_id;die;
             $this->pushiphon(array($usertoken['device_token'],'There is a job suggested for you','suggest job','New Job',$jid,count($fetchnotifications)));
        }
      }
    }

    public function jobstatuschange() {
       $date = date('Y-m-d H:i:s');
        $jobData = $this->input->post();
        //print_r($jobData);die;
         $schdate = date('Y-m-d', strtotime($jobData['schedule_date']));
         $jobtype = $jobData['type'];
        $jobtitle = $jobData['jobtitle'];
        $recruiter_id = $jobData['recruiter_id'];
        $companyname = $this->Candidate_Model->fetchcomp($recruiter_id);
        $key_type = $jobData['key_type'];
        //echo $jobtype;die;
        if($jobData['falloutreason']){
            if($jobData['falloutreason'] == 1) {
               $reason = "No show";
            } else if($jobData['falloutreason'] == 2) {
               $reason = "Did not meet requirement";
            } else if($jobData['falloutreason'] == 3) {
               $reason = "Did not Accept Job Offers";
            } else if($jobData['falloutreason'] == 4) {
               $reason = "Refer to another job listing";
            } else if($jobData['falloutreason'] == 5) {
               $reason = "Day 1 No Show";
            }
        }

        if($jobData['statusradio'] == 2) {
           $data = ["status" => 3, "fallout_status" => 1, "fallout_reason" => "No Show", "date_day1"=> ""];
           $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
        
        } else if($jobData['statusradio'] == 3) {
          if(empty($jobData['falloutreason'])){
            $this->session->set_tempdata('fallerr','Fallout reason is required');
          }
            $data = ["status" => $jobData['statusradio'], "fallout_status" => $jobData['falloutreason'], "fallout_reason" => $reason];
            $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
        
        } else if($jobData['statusradio'] == 4) {
          if(empty($jobData['jobid'])){
            $this->session->set_tempdata('jobiderr','Job Id is required');
          }
            $data = ["user_id"=>$jobData['uid'], "status" => 1,"interviewdate"=>$jobData['interview_date'],'interviewtime'=>$jobData['interview_time'],  'jobpost_id' => $jobData['jobid'],'fallout_reason'=>' ','accepted_date'=>''];
            $this->Candidate_Model->refer_status($data);
            $data = ["status" => $jobData['statusradio'], "fallout_status" => 0, "fallout_reason" => " "];
            $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
        } else if($jobData['statusradio'] == 5) {

            $data = ["status" => 5, "fallout_status" => 0, "fallout_reason" => ""];
            $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            /*if($jobData['goingreason'] == 1) {
               $data = ["status" => 5, "fallout_status" => 0, "fallout_reason" => "Open"];
               $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            
            } else if($jobData['goingreason'] == 2) {
               $data = ["status" => 5, "fallout_status" => 0, "fallout_reason" => "Hiring"];
               $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            }*/
        
        } else if($jobData['statusradio'] == 6) {
              $data = ["status" => 6, "fallout_status" => 0, "fallout_reason" => "", "date_day1"=>$schdate];
               $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            /*if($jobData['acceptedreason'] == 1) {
               $data = ["status" => 6, "fallout_status" => 0, "fallout_reason" => "Schedule of day1", "date_day1"=>$schdate];
               $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            
            } else if($jobData['acceptedreason'] == 2) {
               $data = ["status" => 6, "fallout_status" => 0, "fallout_reason" => "ACCEPTED JO Rechedule", "date_day1"=>$schdate];
               $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
            }*/
        
        } else {
            $data = ["status" => $jobData['statusradio'], "fallout_status" =>  0,"fallout_reason" => " "];
            $this->Candidate_Model->candidateApplied_statuschange($jobData['appid'], $jobData['uid'], $data);
        }

        if($jobData['statusradio']=="1") {
            $msg="Your Job application for "." " . $jobtitle ." "."with "." " .$companyname[0]['cname'] ." is "."New Application";
        } if($jobData['statusradio']=="2") {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."No Show";
        
        } if($jobData['statusradio']=="3") {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Fall Out";
        
        } if($jobData['statusradio']=="4") {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Refer";
        
        } if($jobData['statusradio'] == 5) {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."On Going Application";
        
        } if($jobData['statusradio'] == 6) {
            $msg="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Accepted JO";
        
        } if($jobData['statusradio'] == 7) {
            $msg1="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Hired. Please share your success story";
            $msg2="Your Job application for "." ". $jobtitle ." "."with "." ". $companyname[0]['cname'] ." is changed to "."Hired. Please share your review";
        }
        
        $data['candidatesApplied1'] = $this->Candidate_Model->fetchnotification($jobData['uid'], $jobData['appid'], $jobData['statusradio']);
        
        $user_data = $this->User_Model->user_single($jobData['uid']);
        
        if($user_data[0]['notification_status']=="1") {

            if(empty($data['candidatesApplied1'])) {
                
               if($jobData['statusradio'] == 7) {
                  ///var_dump("2");die;
                  $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg1, "status_changed"=>$jobData['statusradio'], "job_status"=>'1', "badge_count"=>1, "createdon"=>$date];
                  
                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                  
                  $data2 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg2,"status_changed"=>$jobData['statusradio'], "job_status"=>'2', "badge_count"=>1, "createdon"=>$date];
                  //var_dump($data2);die;
                  $notificationInserted1 = $this->Candidate_Model->notification_insert($data2);
                  if($notificationInserted1){
                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                  }
                  
                  $tokendata = ['user_id'=>$jobData['uid']];
                  $userTokenCheck = $this->User_Model->user_token_check($tokendata);
                  //print_r($userTokenCheck);die;
                  foreach ($userTokenCheck as $usertoken) {
                  if($usertoken['device_type']=="android") {
                      $res1 = $this->push_notification_android_customer($usertoken['device_token'],$jobtitle, $msg1,'share story',$recruiter_id,$companyname[0]['cname'],$notificationInserted,count($fetchnotifications));
                      //print_r($res1);die;
                  $res2 = $this->push_notification_android_customer($usertoken['device_token'],$jobtitle, $msg2,'share review',$recruiter_id,$companyname[0]['cname'],$notificationInserted1,count($fetchnotifications));

                  }else if($usertoken['device_type']=="ios") {
                      //echo $recruiter_id;die;
                       $this->pushiphon(array($usertoken['device_token'],$msg1,'share story',$jobtitle,$recruiter_id,count($fetchnotifications),$companyname[0]['cname'],$notificationInserted));
                      //print_r($respos);die;
                      $this->pushiphon(array($usertoken['device_token'],$msg2,'share review',$jobtitle,$recruiter_id,count($fetchnotifications),$companyname[0]['cname'],$notificationInserted1));
                  }
                }
           
               } else {
                  $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg, "status_changed"=>$jobData['statusradio'], "job_status"=>'0',"badge_count"=>1, "createdon"=>$date];

                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                  if($notificationInserted){
                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                  }
                  
                  $tokendata = ['user_id'=>$jobData['uid']];
                  $userTokenCheck = $this->User_Model->user_token_check($tokendata);
                  //print_r($userTokenCheck);die;
                  foreach ($userTokenCheck as $usertoken) {
                  if($usertoken['device_type']=="android") {
                      $res = $this->push_notification_android_customer($usertoken['device_token'], $jobtitle, $msg, 'applied', $jobData['jid'],$companyname[0]['cname'],$notificationInserted,count($fetchnotifications));
                  
                  }  else if($usertoken['device_type']=="ios"){
                      $this->pushiphon(array($usertoken['device_token'], $msg, 'applied',$jobtitle, $jobData['jid'],count($fetchnotifications),$companyname[0]['cname'],$notificationInserted));
                      
                  } }
               } 
            } else {
               if($jobData['statusradio'] == 7) {
                    $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg1, "status_changed"=>$jobData['statusradio'], "job_status"=>'1',"badge_count"=>1, "createdon"=>$date];
                  
                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                  
                  $data2 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg2,"status_changed"=>$jobData['statusradio'], "job_status"=>'2',"badge_count"=>1, "createdon"=>$date];
                  //var_dump($data2);die;
                  $notificationInserted1 = $this->Candidate_Model->notification_insert($data2);
                  if($notificationInserted1){
                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                  }
                  
                  $tokendata = ['user_id'=>$jobData['uid']];
                  $userTokenCheck = $this->User_Model->user_token_check($tokendata);
                  //print_r($userTokenCheck);die;
                  foreach ($userTokenCheck as $usertoken) {
                  if($usertoken['device_type']=="android") {
                      $res1 = $this->push_notification_android_customer($usertoken['device_token'],$jobtitle, $msg1,'share story',$recruiter_id,$companyname[0]['cname'],$notificationInserted,count($fetchnotifications));
                      //print_r($res1);die;
                  $res2 = $this->push_notification_android_customer($usertoken['device_token'],$jobtitle, $msg2,'share review',$recruiter_id,$companyname[0]['cname'],$notificationInserted1,count($fetchnotifications));

                  }else if($usertoken['device_type']=="ios") {
                      //echo $recruiter_id;die;
                       $this->pushiphon(array($usertoken['device_token'],$msg1,'share story',$jobtitle,$recruiter_id,count($fetchnotifications),$companyname[0]['cname'],$notificationInserted));
                      //print_r($respos);die;
                      $this->pushiphon(array($usertoken['device_token'],$msg2,'share review',$jobtitle,$recruiter_id,count($fetchnotifications),$companyname[0]['cname'],$notificationInserted1));
                  }
                }
                   /*$data1 = ["notification"=>$msg, "job_status"=>'1'];
                   $notificationInserted = $this->Candidate_Model->notification_update($jobData['uid'],$jobData['statusradio'],$jobData['appid'],$data1);
                   if($notificationInserted){
                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                   }
                   
                   $tokendata = ['user_id'=>$jobData['uid']];
                  $userTokenCheck = $this->User_Model->user_token_check($tokendata);
                  //print_r($userTokenCheck);die;
                  foreach ($userTokenCheck as $usertoken) {
                   if($usertoken['device_type']=="android") {
                       $res1 = $this->push_notification_android_customer($usertoken['device_token'],$jobtitle, $msg1,'share story',$recruiter_id,$companyname[0]['cname'],count($fetchnotifications));
                       $res2 = $this->push_notification_android_customer($usertoken['device_token'],$jobtitle, $msg2,'share review',$recruiter_id,$companyname[0]['cname'],count($fetchnotifications));
                   
                   } else if($user_data[0]['device_type']=="ios") {
                   $this->pushiphon(array($user_data[0]['device_token'],$msg1,'share story',$jobtitle,$recruiter_id),count($fetchnotifications),$companyname[0]['cname']);
                   $this->pushiphon(array($user_data[0]['device_token'],$msg2,'share review',$jobtitle,$recruiter_id),count($fetchnotifications),$companyname[0]['cname']);
               }}*/
                   
                   
               
               }else{
               
                  /*$data1 = ["notification"=>$msg, "job_status"=>'0'];
                  $notificationInserted = $this->Candidate_Model->notification_update($jobData['uid'],$jobData['statusradio'],$jobData['appid'],$data1);
                  if($notificationInserted){
                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                  }
                  
                  $tokendata = ['user_id'=>$jobData['uid']];
                  $userTokenCheck = $this->User_Model->user_token_check($tokendata);
                  //print_r($userTokenCheck);die;
                  foreach ($userTokenCheck as $usertoken) {
                  if($usertoken['device_type']=="android") {

                     $res = $this->push_notification_android_customer($usertoken['device_token'], $jobtitle, $msg, 'applied' ,$jobData['jid'],$companyname[0]['cname'],$notificationInserted,count($fetchnotifications)); 

                  } else if($usertoken['device_type']=="ios"){
                     $this->pushiphon(array($usertoken['device_token'],$msg,'applied',$jobtitle,$jobData['jid'],count($fetchnotifications),$companyname[0]['cname'],$notificationInserted)); 
                  } }*/
                  $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg, "status_changed"=>$jobData['statusradio'], "job_status"=>'0',"badge_count"=>1, "createdon"=>$date];

                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                  if($notificationInserted){
                    $fetchnotifications = $this->Candidate_Model->fetchnotifications($jobData['uid']);
                  }
                  
                  $tokendata = ['user_id'=>$jobData['uid']];
                  $userTokenCheck = $this->User_Model->user_token_check($tokendata);
                  //print_r($userTokenCheck);die;
                  foreach ($userTokenCheck as $usertoken) {
                  if($usertoken['device_type']=="android") {
                      $res = $this->push_notification_android_customer($usertoken['device_token'], $jobtitle, $msg, 'applied', $jobData['jid'],$companyname[0]['cname'],$notificationInserted,count($fetchnotifications));
                  
                  }  else if($usertoken['device_type']=="ios"){
                      $this->pushiphon(array($usertoken['device_token'], $msg, 'applied',$jobtitle, $jobData['jid'],count($fetchnotifications),$companyname[0]['cname'],$notificationInserted));
                      
                  } }
               }
            }
           
        }else{
            
            if(empty($data['candidatesApplied1'])) {
            
               if($jobData['statusradio']=="7") {
                  
                  $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg1, "status_changed"=>$jobData['statusradio'], "job_status"=>'1',"badge_count"=>1, "createdon"=>$date];
                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                  
                  $data2 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg2, "status_changed"=>$jobData['statusradio'], "job_status"=>'2',"badge_count"=>1, "createdon"=>$date];
                  $notificationInserted1 = $this->Candidate_Model->notification_insert($data2);
           
               } else {
                  $data1 = ["user_id"=>$jobData['uid'],"jobapp_id"=>$jobData['appid'], "recruiter_id" =>$jobData['recruiter_id'], "notification"=>$msg, "status_changed"=>$jobData['statusradio'], "job_status"=>'0',"badge_count"=>1, "createdon"=>$date];

                  $notificationInserted = $this->Candidate_Model->notification_insert($data1);
               }
                  
            } else {

               if($jobData['statusradio']=="7") {
                   
                   $data1 = ["notification"=>$msg, "job_status"=>'1'];
                   $notificationInserted = $this->Candidate_Model->notification_update($jobData['uid'],$jobData['statusradio'],$jobData['appid'],$data1);
               
               } else{
               
                  $data1 = ["notification"=>$msg, "job_status"=>'0'];
                  $notificationInserted = $this->Candidate_Model->notification_update($jobData['uid'],$jobData['statusradio'],$jobData['appid'],$data1);
                  
               }
            }
            
         }
       if($key_type=='view_key'){
        //echo "recruiter/candidate?type=$jobtype";die;
        redirect("recruiter/candidate?type=$jobtype");
       }else{
        redirect("recruiter/candidate/manageCandidates");
       }
      
   }

   public function checkjobid(){
    $userSession = $this->session->userdata('userSession');
    $job_id = $this->input->post('job_id');
    $checkjob = $this->Candidate_Model->checkjob($job_id,$userSession['id']);
    //echo $this->db->last_query();
    if($checkjob){
      echo "true";
    }else{
      echo "false";
    }
   }

    public function filtercandidate() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
              $subrecruiter_id = $userSession['id'];
              $userSession['id'] = $userSession['parent_id'];
          }else{
              $userSession['id'] = $userSession['id'];
              $subrecruiter_id = 0;
          }
        $filterData = $this->input->post();
        $data['candidatesApplied1'] = $this->Candidate_Model->search_listfilter($userSession['id'], $filterData);
        //echo $this->db->last_query();die;
        $data["languages"] = $this->Common_Model->language_lists();
        $data["industries"] = $this->Common_Model->industry_lists();
        $data['skillData'] = $this->Candidate_Model->skills_fetch();
        $data['clients'] = $this->Recruit_Model->user_clientSupported();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $data['keyword'] = $filterData['keyword'];
        $data['filterdata'] = $filterData;
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/search_candidate', $data);
    }

    public function singleCandidates() {
        $id = base64_decode($this->input->get('id'));
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
              $subrecruiter_id = $userSession['id'];
              $userSession['id'] = $userSession['parent_id'];
          }else{
              $userSession['id'] = $userSession['id'];
              $subrecruiter_id = 0;
          }
        $data['candidatesApplied1'] = $this->Candidate_Model->candidateApplied_single($id);
        //echo $this->db->last_query();die;
        $data['candidateEducation'] = $this->Candidate_Model->candidateEducation_single($id);
        $data['candidateExp'] = $this->Candidate_Model->candidateExp_single($id);
        $data['candidateSkills'] = $this->Candidate_Model->candidateSkills_single($id);
        $data['candidateClients'] = $this->Candidate_Model->candidateClients_single($id);
        $data['candidateLang'] = $this->Candidate_Model->candidateLanguages_single($id);
        $data['candidateAssessment'] = $this->Candidate_Model->candidateAssessment_single($id);
        $data['candidateExpert'] = $this->Candidate_Model->candidateExpert_single($id);
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );      
        $this->load->view('recruiter/candidate_single', $data);
    }
    
    public function contact(){
        $contactData = $this->input->post();
        
        $data1 = ["name"=>$contactData['name'], "email"=>$contactData['email'], "phone"=> $contactData['phone'], "message"=> $contactData['message']];

        $contactInserted = $this->Candidate_Model->contact_insert($data1);
        $userId = $contactData['user_id'];
        if($contactInserted) {

            // Subscription

            $getplan = $this->Subscription_Model->view_existing($jobData['recruId']);
            $updatePlan = ["remaining_mail"=>$getplan[0]['remaining_mail'] - 1];
            $this->Subscription_Model->view_existing_updation($updatePlan, $getplan[0]['id']);
            
            /*$this->email->from('jobyoda123@gmail.com', "JobYoda");
            $this->email->to($contactData['user_email']);
            $this->email->subject('Contact from JobYodha');
            $msg = "Dear " . $contactData['name'];
            $msg .= ". Jobyoda Recruited contacted you. Please check the message below. ";
            $msg .= ". Name ". $contactData['name'];
            $msg .= ". and Message ". $contactData['message'];
            $this->email->message($msg);
            $this->email->send();*/
            $this->load->library('email');        
            $data['user_name'] = ucfirst($contactData['user_name']);
            $data['name'] = ucfirst($contactData['name']);
            $data['email'] = ucfirst($contactData['email']);
            $data['phone'] = ucfirst($contactData['phone']);
            $data['message'] = ucfirst($contactData['message']);
            $msg = $this->load->view('recruiter/candidate_email',$data,TRUE);
            $config=array(
                'protocol' => 'smtp',
                'smtp_host' => 'smtpout.asia.secureserver.net',
                'smtp_user' => 'help@jobyoda.com',
                'smtp_pass' => 'Usa@1234567',
                'smtp_port' => 465,
                'smtp_crypto' => 'ssl',
                'charset' => 'utf-8',
                'mailtype' => 'html',
                'crlf' => "\r\n",
                'newline' => "\r\n"
            );

            $this->email->initialize($config);
            $this->email->from($contactData['email'], "JobYoDA");
            $this->email->to($contactData['user_email']);
            $this->email->subject('JobYoDA Recruiter - New Message');
            /*$msg = "Dear " . $name.' '.$userData['lname'];
            $msg .= "You have Successfully Registered. You can login into your account once Admin approves your account within 24 hours!";*/

            $this->email->message($msg);
            if($this->email->send()){

            }else{
                echo $this->email->print_debugger();
            }

            $this->session->set_tempdata('contactSuccess','Contacted Successfully');
            
            redirect("recruiter/candidate/singleCandidates?id=$userId");
        } else {
            redirect("recruiter/candidate/singleCandidates?id=$userId");
        }
    
    }

    function push_notification_android_customer($regid,$title,$body,$type,$recruiter_id,$companyname,$notification_id,$badgecount){
      date_default_timezone_set('Asia/Manila');
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' =>$title,
            'body' => $body,
            'type' => $type,
            'recruiter_id' => $recruiter_id,
            'story_id' => '13',
            'companyname' => $companyname,
            'notification_id' => $notification_id,
            'timestamp' => date('H:i:s'),
            'suggestid' => '2',
            'badgecount' => $badgecount,
            'promo_id' => 0
        ];
        $extraNotificationData = $notification;
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $regid,
            //'notification' => $notification,
            'data' => $extraNotificationData
        ];
        //print_r($fcmNotification);die;
        $headers1 = [
            'Authorization: key=' . $access_key,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
        }

        function push_notification_suggested($regid,$title,$body,$type,$recruiter_id,$badgecount){
      date_default_timezone_set('Asia/Manila');
        $access_key = API_ACCESS_KEY;
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' =>$title,
            'body' => $body,
            'type' => $type,
            'recruiter_id' => $recruiter_id,
            'story_id' => '13',
            'companyname' => '22',
            'notification_id' => '32',
            'timestamp' => date('H:i:s'),
            'suggestid' => '1',
            'badgecount' => $badgecount,
            'promo_id' => 0
        ];
        $extraNotificationData = $notification;
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $regid,
            //'notification' => $notification,
            'data' => $extraNotificationData
        ];
        $headers1 = [
            'Authorization: key=' . $access_key,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
        }

        function pushiphon($array)       
         {      //print_r($array);die;                           
           $deviceToken = $array[0];           
           $passphrase = '1234';  
           $this->autoRender = false;
           $this->layout = false; 
           //$basePath = WWW_ROOT."QualititionSolution.pem";                 
           $basePath = "JobYoda.pem";                 
             //echo file_exists($basePath);  die;          
           if(file_exists($basePath))             
           {  
           //echo $basePath." exists";die;     
           $ctx = stream_context_create(); 
           stream_context_set_option($ctx, 'ssl', 'local_cert', $basePath);
           stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
           $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,  
           $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); 
           if (!$fp)     
           exit("Failed to connect: $err $errstr" . PHP_EOL);
          $body['aps'] = array('alert' => array('title' => $array[3],"body"=>$array[1]),'sound' =>'default','badge'=>$array[5]);
          $body['body'] = $array[1];
          $body['type'] = $array[2];
          $body['recruiter_id'] = $array[4];
          $body['companyname'] = $array[6];
          $body['notification_id'] = $array[7];
          //echo"<pre>";print_r($body);die; 
          $payload = json_encode($body);
           
           $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
          
          $result = fwrite($fp, $msg, strlen($msg));
          //print_r($result);die;
           if (!$result)
           {           
                    
           }     
            else  
           {   
            //return $result; 
            //echo "<pre>";print_r($body['aps']); die;   
              
           }       
             fclose($fp); 
           }  
      }

    /*function android_push($deviceToken = null, $message = null, $badge = null)    
    {    
      
        $url              = 'https://fcm.googleapis.com/fcm/send';   
        $message          = array("message" => $message,'sound' => 'default','badge'=>$badge);  
         
        $registatoin_ids  = array($deviceToken);
        $fields           = array('registration_ids' => $registatoin_ids, 'data' => $message);
        
        $GOOGLE_API_KEY   = "AIzaSyCTzjJxETlJxp18hCwYHLFETLZRkbGFiGw";   
        $headers          = array(
            'Authorization: key=' . $GOOGLE_API_KEY,
            'Content-Type: application/json',   
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        
        curl_close($ch);
        return $result;

    }*/

  public function calculateRating($id) {
      
      $attainments = $this->fetchEduSingleData($id);
      if($attainments) {
         $rate1 = array();
         foreach($attainments as $attainment) {
            if($attainment['attainment'] == "Post Graduate") {
               $rate1[] = 5;
            } else if($attainment['attainment'] == "College Graduate") {
               $rate1[] = 5;
            } else if($attainment['attainment'] == "Associate Degree") {
               $rate1[] = 4;
            } else if($attainment['attainment'] == "Undergraduate") {
               $rate1[] = 3;
            } else if($attainment['attainment'] == "Vocational") {
               $rate1[] = 3;
            } else if($attainment['attainment'] == "High School") {
               $rate1[] = 2;
            } else{
               $rate1[] = 0;
            }
         }
         $maxVal = max($rate1);
         $star = ($maxVal * 20)/100;  
      
      } else{
         $star =0;
      }

      $experiences = $this->fetchWorkSingleData($id);
      //print_r($experiences);
      if(!empty($experiences)) {
        //print_r($experiences);
         $totalDays = 0;
         foreach($experiences as $experience) {
            $from = $experience['from'];
            //$from = strtotime($from);
            //echo $from;die;
            $to = $experience['to'];
            $datetime1 = new DateTime($from);
            //$datetime1->setTimestamp(1372622987);
            $datetime2 = new DateTime($to);
            $interval = $datetime1->diff($datetime2);
            $totalDays = $totalDays + $interval->days;
         }

         if($totalDays == 0) {
            $rate2 = 1;
         } else if(($totalDays < 360) && ($totalDays > 0)) {
            $rate2 = 2;
         } else if(($totalDays < 720) && ($totalDays > 300)) {
            $rate2 = 3;
         } else if(($totalDays < 1080) && ($totalDays > 720)) {
            $rate2 = 4;
         } else if($totalDays > 1080) {
            $rate2 = 5;
         } else {
            $rate2 = 0;
         }
         $star1 = $rate2 * 30 / 100;
      } else {
         $star1 = 0;
      }

      $tenure = $this->fetchWorkuniqueData($id);
      
      if($tenure) {
         $totalDays1 = 0;
         $countComp = 0;
         foreach($experiences as $experience) {
            $from = $experience['from'];
            //$from = strtotime($from);
            $to = $experience['to'];
            $datetime1 = new DateTime($from);
            $datetime2 = new DateTime($to);
            $interval = $datetime1->diff($datetime2);
            $totalDays1 = $totalDays + $interval->days;
            $countComp++;
         }

         $avgWork = $totalDays1 / $countComp;
         if($avgWork == 0) {
            $rate3 = 1;
         } else if($avgWork < 360 && $avgWork > 0) {
            $rate3 = 2;
         } else if($avgWork < 720 && $avgWork > 300) {
            $rate3 = 3;
         } else if($avgWork < 1080 && $avgWork > 720) {
            $rate3 = 4;
         } else if($avgWork >= 1080) {
            $rate3 = 5;
         } else{
            $rate3 = 0;
         }
         $star2 = ($rate3 * 30)/100;
      } else{
         $star2 =0;
      }
      
      $self = $this->User_Model->user_assessment($id);
      
      if($self) {
         $totalSelf = $self[0]['verbal'] + $self[0]['written'] + $self[0]['listening'] + $self[0]['problem'];
         $avgSelf = $totalSelf / 5;
         $star3 = ($avgSelf * 20)/100;
      } else{
         $star3 = 0;
      }

      $totalStar = $star + $star1 + $star2 + $star3;
      return $totalStar = number_format((float)$totalStar, 1, '.', '');
   }

   public function fetchEduSingleData($id) {
        $eduDatas = $this->User_Model->edu_single($id);
        if($eduDatas) {
            foreach ($eduDatas as $eduData) {
                $data[] = ["id"=> $eduData['id'], "attainment" => $eduData['attainment'], "degree" => $eduData['degree'], "university" => $eduData['university'], "from" => $eduData['degreeFrom'], "to" => $eduData['degreeTo']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchWorkSingleData($id) {
        $workDatas = $this->User_Model->work_single($id);
        if($workDatas) {
            foreach ($workDatas as $workData) {
                $data[] = ["id"=> $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "category"=>$workData['category'], "categoryid"=>$workData['categoryid'], "subcategory"=> $workData['subcategory'],  "subcategoryid"=> $workData['subcatid'], "level"=> $workData['level'], "levelid"=> $workData['levelid'], "currently_working"=> $workData['currently_working']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchWorkuniqueData($id) {
      $workDatas = $this->User_Model->work_unique($id);
        if($workDatas) {
            foreach ($workDatas as $workData) {
                $data[] = ["id"=> $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "category"=>$workData['category'], "categoryid"=>$workData['categoryid'], "subcategory"=> $workData['subcategory'],  "subcategoryid"=> $workData['subcatid'], "level"=> $workData['level'], "levelid"=> $workData['levelid'], "currently_working"=> $workData['currently_working']];
            }
            return $data;
        } else{
            $data = [];
            return $data;
        }
    }

    public function fetchpending($userSession,$subrecruiter_id){
        $data["pendingData"] = [];
        $pending = $this->Candidate_Model->candidateduenotification_list($userSession,$subrecruiter_id ); 
        if (!empty($pending)) {
            foreach ($pending as $pendings) {
                //echo $pendings['updated_at'];die;
                $enddate =  date('Y-m-d', strtotime($pendings['updated_at']. ' + 3 days'));
                //echo $enddate;die;
                $enddate = strtotime($enddate);
                $updated_date=  strtotime($pendings['updated_at']);
                $now = time();
                $datediff =  $now - $updated_date;

                $daydiff = floor($datediff / (60*60*24));
                //echo $daydiff;die;
                if($daydiff >=3)
                {
                    $data["pendingData"][] =["profilePic"=>$pendings['profilePic'],"user_id"=>$pendings['user_id'], "name"=>$pendings['name'], "email"=>$pendings['email'], 'phone'=>$pendings['phone'], "location"=>$pendings['location'], "status"=>$pendings['status'], "interviewdate" => $pendings['interviewdate'], 'interviewtime' => $pendings['interviewtime'] , "date_day1" => $pendings['date_day1'], 'jobDesc' => $pendings['jobDesc'], "jobtitle"=>$pendings['jobtitle'], "compid"=>$pendings['compid'], "id"=>$pendings['id'], 'user_id'=>$pendings['user_id'],'jobpost_id'=>$pendings['jobpost_id'], 'updated_at'=> $pendings['updated_at'], 'fallout_reason' => $pendings['fallout_reason']];
                }

            }
        }
        return $data["pendingData"];
    }

    public function updateplanresume() {

        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }

        // Subscription

        $getplan = $this->Subscription_Model->view_existing($userSession['id']);
        $updatePlan = ["remaining_resume_access"=>$getplan[0]['remaining_resume_access'] - 1];
        $this->Subscription_Model->view_existing_updation($updatePlan, $getplan[0]['id']);

        return true;
    }

}
?>
