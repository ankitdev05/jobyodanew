<?php
ob_start();
ini_set('display_errors', 1);
ini_set("allow_url_fopen", 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Jobpost extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Subscription_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');

        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
    }
    public function index() {
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $data['recruiterStatus'] = $this->Common_Model->fetch_site_name($userSession['id']);
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists1();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $data['skills'] = $this->Jobpost_Model->skill_lists();

        if(isset($_GET['type'])){
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($_GET['type']);
        } else{
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($userSession['id']);
        }
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/postjob', $data);
    }

    public function index1() {
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $userSession = $this->session->userdata('userSession');
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();

        if(isset($_GET['type'])){
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($_GET['type']);
        } else{
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($userSession['id']);
        }
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $this->load->view('recruiter/postjob1', $data);
    }

    public function fetchSubcategory(){
        $category = $this->input->post('cat_val');
        $data['subcategory'] = $this->Jobpost_Model->subcategory_listsbycatid($category);
        if($data['subcategory']){
            echo "<option value=''>Select SubCategory</option>";
            foreach ($data['subcategory'] as $subcategorys) {
                
                echo '<option value="'.$subcategorys['id'].'">'.$subcategorys['subcategory'].'</option>';
                //echo $category_data;
            }

        }    else{
           echo "<option value=''>Select SubCategory</option>";
        }   
        
    }

    public function giveuspriority($str)
    {
        if($str == "less than 6 months"){
            return 1;
        } else if($str == "6mo to 1 yr"){
            return 2;
        } else if($str == "1 yr to 2 yr"){
            return 3;
        } else if($str == "2yr to 3 yr"){
            return 4;
        } else if($str == "3yr and up"){
            return 5;
        } else {
            return 0;
        } 
    }

    public function gradeIds($str)
    {
        $grid=[];
       if($str == "All Tenure"){
            $grid = ['0'];
        }else if($str == "No Experience"){
            $grid = ['1'];
        } else if($str == "< 6 months   "){
            $grid = ['1','2'];
        } else if($str == "> 6 months"){
            $grid=['3','4','5','6','7','8','9','10'];
        } else if($str == "> 1 yr"){
            $grid=['4','5','6','7','8','9','10'];
        } else if($str == "> 2 yr"){
            $grid=['5','6','7','8','9','10'];
        }else if($str == "> 3 yr"){
            $grid=['6','7','8','9','10'];
        }else if($str == "> 4 yr"){
            $grid=['7','8','9','10'];
        }else if($str == "> 5 yr"){
            $grid=['8','9','10'];
        }else if($str == "> 6 yr"){
            $grid=['9','10'];
        } else {
            $grid=['10'];
        }  
        return $grid;
    }

    public function jobpostInsert() {
        $jobData = $this->input->post();
        //print_r($jobData);die;
        $userSession = $this->session->userdata('userSession');
        //print_r($userSession);die;
            if(!empty($userSession['label']) && $userSession['label']=='3'){
                $parent = $userSession['parent_id'];
                $subrecruit_id = $userSession['id'];
                $recruitid = $userSession['id'];
            }else{
                $parent = $userSession['id'];
                $subrecruit_id = 0;
                $recruitid = $userSession['id'];
            }

        //var_dump($jobData);die;
        $this->form_validation->set_rules('jobTitle', 'Job Title', 'trim|required');

        if(!empty($jobData['bonuscheck']) && $jobData['bonuscheck']=='on') {
            $this->form_validation->set_rules('bonus_amount', 'Bonus amount', 'trim|required');
        }
        $this->form_validation->set_rules('jobLoc', 'Job Location', 'trim|required');
        $this->form_validation->set_rules('opening', 'Opening', 'trim|required|numeric');
        $this->form_validation->set_rules('experience', 'Experience', 'trim|required');
        
        $this->form_validation->set_rules('education', 'Education', 'trim|required');
        $this->form_validation->set_rules('subcategory', 'SubCategory', 'trim|required');
        $this->form_validation->set_rules('category', 'Category', 'trim|required');
        $this->form_validation->set_rules('level', 'Level', 'trim|required');
        $this->form_validation->set_rules('lang', 'Language', 'trim|required');
        $this->form_validation->set_rules('jobPitch', 'Job Pitch', 'trim|required');
        $this->form_validation->set_rules('jobDesc', 'Job Description', 'trim|required');
        $this->form_validation->set_rules('modecheck', 'Interview mode', 'trim|required');
        //$this->form_validation->set_rules('qualification', 'Qualification', 'trim|required');
        $this->form_validation->set_rules('jobExpire', 'Job Expire', 'trim|required');

        if(!empty($jobData['level_status']) && $jobData['level_status']=="on"){
            $jobData['level_status']=1;
        }else{
            $jobData['level_status']=0;
        }    
        if(!empty($jobData['education_status']) && $jobData['education_status']=="on"){
            $jobData['education_status']=1;
        }
        else{
            $jobData['education_status']=0;
        }
        if(!empty($jobData['experience_status']) && $jobData['experience_status']=="on"){
            $jobData['experience_status']=1;
        }
        else{
            $jobData['experience_status']=0;
        }
        if ($this->form_validation->run() == FALSE) {
            //echo $jobData["jobId"];die;
            $data['errors'] = $this->form_validation->error_array();
            
            if(!empty($jobData["jobId"])){
                $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobData["jobId"]);
                $data['jobImg'] = $this->Jobpost_Model->job_images($jobData["jobId"]);
                $topPicks = $this->Recruit_Model->company_topicks_single($jobData["jobId"]);
        
            $skills = $this->Jobpost_Model->jobs_skills_single($jobData["jobId"]);
            if($skills) { 
                $x1=0;
                foreach ($skills as $skill) {
                  $skills1[$x1] = $skill['skill_id'];
                  $x1++;
                }
                $data['skills2'] = $skills1;
            } else{
                $data['skills2'] = [];
            }

            $topPicks = $this->Jobpost_Model->jobs_topicks_single($jobData["jobId"]);
            if($topPicks) { 
                $x1=0;
                foreach ($topPicks as $topPick) {
                  $topPicks1[$x1] = $topPick['picks_id'];
                  $x1++;
                }
                $data['topPicks2'] = $topPicks1;
            } else{
                $data['topPicks2'] = [];
            }
        
            $allowances = $this->Jobpost_Model->jobs_allowances_single($jobData["jobId"]);
            if($allowances) { $x1=0;
                foreach ($allowances as $allowance) {
                  $allowance1[$x1] = $allowance['allowances_id'];$x1++;
                }
                $data['allowance2'] = $allowance1;
            } else{
                $data['allowance2'] = [];
            }

            $medicals = $this->Jobpost_Model->jobs_medical_single($jobData["jobId"]);
            if($medicals) { $x1=0;
                foreach ($medicals as $medical) {
                  $medical1[$x1] = $medical['medical_id'];$x1++;
                }
                $data['medical2'] = $medical1;
            } else{
                $data['medical2'] = [];
            }

            $workshifts = $this->Jobpost_Model->jobs_workshifts_single($jobData["jobId"]);
            if($workshifts) { $x1=0;
                foreach ($workshifts as $workshift) {
                  $workshift1[$x1] = $workshift['workshift_id'];$x1++;
                }
                $data['workshift2'] = $workshift1;
            } else{
                $data['workshift2'] = [];
            }
            $leaves = $this->Jobpost_Model->jobs_leaves_single($jobData["jobId"]);
            //echo $this->db->last_query();die;
            if($leaves) { $x1=0;
                foreach ($leaves as $leave) {
                  $leaves1[$x1] = $leave['leaves_id'];$x1++;
                }
                $data['leaves2'] = $leaves1;
            } else{
                $data['leaves2'] = [];
            }
            }
            //print_r($data['getJobs']);die;
            $data['jobTitle'] = $this->Common_Model->job_title_list();
            $data['industryLists'] = $this->Common_Model->industry_lists();
            
            $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
            $data['levels'] = $this->Jobpost_Model->level_lists();
            $data['category'] = $this->Jobpost_Model->category_lists1();
            $data['skills'] = $this->Jobpost_Model->skill_lists();
            if(isset($jobData["category"])) {
                $data['subcategory'] = $this->Jobpost_Model->subcategory_listsbycatid($jobData["category"]);
            } else{
                $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
            }
            $data['jobdata'] = $jobData;

            if(isset($_GET['type'])){
                $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($_GET['type']);
            } else{
                $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($userSession['id']);
            }

            $data['channels'] = $this->Common_Model->channel_lists();
            $data['langs'] = $this->Common_Model->language_lists();
            $data['recruiterStatus'] = $this->Common_Model->fetch_site_name($userSession['id']);
            if(!empty($jobData["jobId"])){
            $job_id = $jobData["jobId"];                
            }
            if(isset($jobData["jobId"])) {
                redirect('recruiter/jobpost/jobpostView?type=$job_id ');
            }else{
                // echo "<PRE>";
                // var_dump($data);
                $this->load->view('recruiter/postjob', $data);
            }
            
        } else {
            //$userSession = $this->session->userdata('userSession');
            $jobData['jobExpire'] = date("Y-m-d", strtotime($jobData['jobExpire']));
            
            if(isset($jobData["jobId"])) {
                $data = [
                        "recruiter_id" => $jobData['recruId'],
                        //"subrecruiter_id"=> $subrecruit_id,
                        "jobtitle" => $jobData['jobTitle'],
                        "company_id" => $jobData['jobLoc'],
                        "opening" => $jobData['opening'],
                        "experience" => $jobData['experience'],
                        "allowance" => $jobData['allowance']??'',
                        "industry" => 81,
                        "category" => $jobData['category'],
                        "subcategory" => $jobData['subcategory'],
                        "language" => $jobData['lang'],
                        "other_language" => $jobData['otherlanguage'],
                        "jobPitch" => $jobData['jobPitch'],
                        "jobDesc" => $jobData['jobDesc'],
                        "skills" => $jobData['skill'],
                        //"qualification" => $jobData['qualification'],
                        "certification" => $jobData['certification'],
                        "education" => $jobData['education'],
                        "jobexpire" => $jobData['jobExpire'],
                        "level_status" => $jobData['level_status'],
                        "education_status" => $jobData['education_status'],
                        "experience_status" => $jobData['experience_status'],
                        "joining_bonus" => $jobData['bonus_amount'],
                        "mode" => $jobData['modecheck'],
                        "modeurl" => $jobData['modeurl']??'',
                        /*"job_type" => $jobData['job_type'],
                        "walkin_date" => $jobData['walkin_date'],
                        "walkin_from" => $jobData['walkin_from'],
                        "walkin_to" => $jobData['walkin_to']*/
                    ];

                $fetchgetJobs = $this->Jobpost_Model->jobupdate_detail_fetch($jobData['jobId']);

                $jobInserted = $this->Jobpost_Model->job_update($data, $jobData['jobId']);
                $count1 = count($_FILES['job_image']['name']);
                
                if($count1 > 0) {
                
                    for($i=0;$i<$count1;$i++) {
                        $_FILES['file']['name'] = $_FILES['job_image']['name'][$i];
                          $_FILES['file']['type'] = $_FILES['job_image']['type'][$i];
                          $_FILES['file']['tmp_name'] = $_FILES['job_image']['tmp_name'][$i];
                          $_FILES['file']['error'] = $_FILES['job_image']['error'][$i];
                          $_FILES['file']['size'] = $_FILES['job_image']['size'][$i];
                
                          // Set preference
                          $config['upload_path'] = "./recruiterupload/"; 
                          $config['allowed_types'] = 'jpg|jpeg|png|gif';
                          //$config['max_size'] = '60000'; // max_size in kb
                          //$config['max_width'] = '1376'; 
                          //$config['max_height'] = '588'; 
                          $config['file_name'] = $_FILES['job_image']['name'][$i];
                 
                          //Load upload library
                          $this->load->library('upload',$config); 
                 
                          // File upload
                          if($this->upload->do_upload('file')){
                            // Get data about the file
                            $uploadData = $this->upload->data();

                            $this->resizeImage($upload_data['file_name']);
                            $filename = base_url() ."recruiterupload/".$uploadData['file_name'];
                            $data['filenames'][] = $filename;
                          }

                          if(!empty($filename)) {
                              $data2 = ["jobpost_id"=>$jobData['jobId'],"pic" => $filename];
                                $this->Jobpost_Model->job_image_insert($data2);
                          }
                        
                    }
                }

                if(isset($jobData['expRange'])) {
                    
                    $jobExpRanges[] = $jobData['expRange'];
                    $jobExpBasicSalarys[] = $jobData['expBasicSalary'];
                    $count = count($jobData['expRange']);
                    if($count > 0) {
                        for($i=0;$i<$count;$i++) {

                            $data1[$i] = ["grade_id"=>$jobData['expRange'][$i],"jobpost_id"=>$jobData["jobId"], "exp" => '', "basicsalary" => $jobData['expBasicSalary'][$i]];
                        }
                        
                        $this->Jobpost_Model->job_basicsalary_delete($jobData['jobId']);
                        $expsalInserted = $this->Jobpost_Model->basic_salary_insert($data1);
                    }
                }
                if($jobData['skills']) {
                    $skills = $jobData['skills'];
                    $x=0;

                    foreach ($skills as $skill) {
                        $dataskills[$x] = ["jobpost_id"=>$jobData['jobId'], "skill_id"=>$skill];
                        $x++;
                    }
                    $this->Jobpost_Model->job_skills_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_skills_insert($dataskills);
                }
                if($jobData['toppicks']) {
                    $topPicks = $jobData['toppicks'];
                    $x=0;

                    foreach ($topPicks as $topPick) {
                        $dataPicks[$x] = ["jobpost_id"=>$jobData['jobId'], "picks_id"=>$topPick];
                        $x++;
                    }
                    $this->Jobpost_Model->job_toppicks_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_toppicks_insert($dataPicks);
                }
                if($jobData['allowances']) {
                    $topAllos = $jobData['allowances'];
                    $x=0;
                    foreach ($topAllos as $topAllo) {
                        $dataAllos[$x] = ["jobpost_id"=>$jobData['jobId'], "allowances_id"=>$topAllo];
                        $x++;
                    }
                    $this->Jobpost_Model->job_allowances_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_allowances_insert($dataAllos);
                }
                if($jobData['medical']) {
                    $topMedis = $jobData['medical'];
                    $x=0;
                    foreach ($topMedis as $topMedi) {
                        $dataMedis[$x] = ["jobpost_id"=>$jobData['jobId'], "medical_id"=>$topMedi];
                        $x++;
                    }
                    $this->Jobpost_Model->job_medical_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_medical_insert($dataMedis);
                }
                if($jobData['shifts']) {
                    $topShifts = $jobData['shifts'];
                    $x=0;
                    foreach ($topShifts as $topShift) {
                        $dataShifts[$x] = ["jobpost_id"=>$jobData['jobId'], "workshift_id"=>$topShift];
                        $x++;
                    }
                    $this->Jobpost_Model->job_shifts_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_shifts_insert($dataShifts);
                }
                if($jobData['leavs']) {
                    $leavs = $jobData['leavs'];
                    $x=0;
                    foreach ($leavs as $leav) {
                        $dataLeaves[$x] = ["jobpost_id"=>$jobData['jobId'], "leaves_id"=>$leav];
                        $x++;
                    }
                    $this->Jobpost_Model->job_leaves_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_leaves_insert($dataLeaves);
                }
                if($jobInserted) {
                     //$this->session->set_tempdata('inserted', 'Job Post Successfully Updated',5);
                     $dataId = $jobData['jobId'];

                     if($jobData['jobExpire'] == $fetchgetJobs[0]['jobexpire']) {
                     } else {

                        $jobsubcat = $jobData['subcategory'];
                        $joblocationid = $jobData['jobLoc'];
                        $jobtitle = $jobData['jobTitle'];

                        $getLocation = $this->Common_Model->company_address_location($joblocationid);
                        $joblocation = $getLocation[0]['city'];
                        $notifyMsg = "A new job matching your interests was recently posted. Click here to view and apply!";
                        $getCatData = $this->Common_Model->subcategory_listsbysubcatid($jobsubcat);

                        $this->load->model('Jobseekeradmin_Model');
                        $getusers = $this->Jobseekeradmin_Model->jobseeker_bylocationlists($getCatData[0]['subcategory'], $joblocation);

                        $record = ["job_id"=>$dataId, "recruiter_id"=>$recruitid, "title"=>$jobtitle, "notification"=> $notifyMsg, "intrestedIn"=>$getCatData[0]['subcategory']];
                        $notifyid = $this->Jobpost_Model->jobrecord_notifydata($record);

                        if($getusers) {
                        
                            foreach($getusers as $getuser) {
                                
                                if(strlen($getuser['latitude']) > 0) {
                                                      
                                    $distance = $this->distance($getuser['latitude'], $getuser['longitude'], $getLocation[0]['latitude'], $getLocation[0]['longitude'], "K");
                                    $distance = number_format((float)$distance, 2, '.', ''); 

                                    if($distance <= 50) {
                                        $saveNotify = ["notify_id"=>$notifyid, "user_id"=>$getuser['id'], "job_id"=>$jobInserted, "recruiter_id"=>$recruitid, "title"=>$jobtitle, "notification"=> $notifyMsg, "status"=>1];

                                        $this->Jobpost_Model->newjob_notifydata($saveNotify);
                                    }
                                }
                            }
                        }
                     }
                     
                     if(!empty($_GET['key'] && $_GET['key']=='change')){
                        //echo $_GET['key'];die;
                        $this->session->set_tempdata('inserted', 'Job Post Updated Successfully.',5);
                        redirect("recruiter/candidate?type=$dataId");
                     }else{
                        $this->session->set_tempdata('inserted', 'Job Post Updated Successfully.',5);
                        redirect("recruiter/jobpost/manageJobView");
                     }   
                } else {
                    $this->session->set_tempdata('postError',  'Job Post not able to Update',5);
                    $dataId = $jobData['jobId'];
                    redirect("recruiter/jobpost/jobpostViews/$dataId");
                }
            } else {
                $data = [
                        "recruiter_id"=> $jobData['recruId'],
                        "subrecruiter_id"=> $subrecruit_id,
                        "jobtitle" => $jobData['jobTitle'],
                        "company_id" => $jobData['jobLoc'],
                        "opening" => $jobData['opening'],
                        "experience" => $jobData['experience'],
                        "allowance" => $jobData['allowance']??'',
                        "industry" => 81,
                        "category" => $jobData['category'],
                        "subcategory" => $jobData['subcategory'],
                        "level" => $jobData['level'],
                        "language" => $jobData['lang'],
                        "jobPitch" => $jobData['jobPitch'],
                        "jobDesc" => $jobData['jobDesc'],
                        "certification" => $jobData['certification'],
                        "skills" => $jobData['skill'],
                        //"qualification" => $jobData['qualification'],
                        "education" => $jobData['education'],
                        "jobexpire" => $jobData['jobExpire'],
                        "other_language" => $jobData['otherlanguage'],
                        "level_status" => $jobData['level_status'],
                        "education_status" => $jobData['education_status'],
                        "experience_status" => $jobData['experience_status'],
                        "joining_bonus" => $jobData['bonus_amount'],
                        "mode" => $jobData['modecheck'],
                        "modeurl" => $jobData['modeurl']??'',
                        /*"job_type" => $jobData['job_type'],
                        "walkin_date" => $jobData['walkin_date'],
                        "walkin_from" => $jobData['walkin_from'],
                        "walkin_to" => $jobData['walkin_to']*/
                    ];
                $jobInserted = $this->Jobpost_Model->job_insert($data);
                $count = count($_FILES['job_image']['name']);

                if($count > 0) {
                    for($i=0;$i<$count;$i++) {
                        $_FILES['file']['name'] = $_FILES['job_image']['name'][$i];
                          $_FILES['file']['type'] = $_FILES['job_image']['type'][$i];
                          $_FILES['file']['tmp_name'] = $_FILES['job_image']['tmp_name'][$i];
                          $_FILES['file']['error'] = $_FILES['job_image']['error'][$i];
                          $_FILES['file']['size'] = $_FILES['job_image']['size'][$i];
                
                          // Set preference
                          $config['upload_path'] = "./recruiterupload/"; 
                          $config['allowed_types'] = 'jpg|jpeg|png|gif';
                          //$config['max_size'] = '60000'; // max_size in kb
                          //$config['max_width'] = '1376'; 
                          //$config['max_height'] = '588';
                          $config['file_name'] = $_FILES['job_image']['name'][$i];
                 
                          //Load upload library
                          $this->load->library('upload',$config); 
                 
                          // File upload
                          if($this->upload->do_upload('file')){
                            // Get data about the file
                            $uploadData = $this->upload->data();
                            $filename = base_url() ."recruiterupload/".$uploadData['file_name'];
                            $data['filenames'][] = $filename;
                          } else {

                              $filename="";
                              //echo $this->upload->display_errors();
                          }

                        if(!empty($filename)){
                            $data1 = ["jobpost_id"=>$jobInserted,"pic" => $filename];
                            $this->Jobpost_Model->job_image_insert($data1);
                        }   
                    } 
                }
                if(isset($jobData['expRange'])) {
                    $count = count($jobData['expRange']);
                    //echo $count;die;
                    if($count > 0) {
                        for($i=0;$i<$count;$i++) {
                            $data2[$i] = ["grade_id"=>$jobData['expRange'][$i],"jobpost_id"=>$jobInserted, "exp" => '', "basicsalary" => $jobData['expBasicSalary'][$i]];
                        }
                        //print_r($data1);die;
                        $expsalInserted = $this->Jobpost_Model->basic_salary_insert($data2);
                       // echo $this->db->last_query();die;
                    }

                } else {
                    $gradeData = $this->gradeIds($jobData['experience']);
                    $count = count($gradeData);
                   // print_r($gradeData); die;
                    if($count > 0) {
                        for($i=0;$i<$count;$i++) {
                            $data2[$i] = ["grade_id"=>$gradeData[$i],"jobpost_id"=>$jobInserted, "exp" => '', "basicsalary" => 0];
                        }
                        //print_r($data1);die;
                        $expsalInserted = $this->Jobpost_Model->basic_salary_insert($data2);
                       // echo $this->db->last_query();die;
                    }
                }
                if(isset($jobData['skills'])) {
                    $skills = $jobData['skills'];
                    $x=0;
                    foreach ($skills as $skill) {
                        $dataskills[$x] = ["jobpost_id"=>$jobInserted, "skill_id"=>$skill];
                        $x++;
                    }
                    $this->Jobpost_Model->job_skills_insert($dataskills);
                }

                if(isset($jobData['toppicks'])) {
                    $topPicks = $jobData['toppicks'];
                    $x=0;
                    foreach ($topPicks as $topPick) {
                        $dataPicks[$x] = ["jobpost_id"=>$jobInserted, "picks_id"=>$topPick];
                        $x++;
                    }
                    $this->Jobpost_Model->job_toppicks_insert($dataPicks);
                }
                if(isset($jobData['allowances'])) {
                    $topAllos = $jobData['allowances'];
                    $x=0;
                    foreach ($topAllos as $topAllo) {
                        $dataAllos[$x] = ["jobpost_id"=>$jobInserted, "allowances_id"=>$topAllo];
                        $x++;
                    }
                    $this->Jobpost_Model->job_allowances_insert($dataAllos);
                }
                if(isset($jobData['medical'])) {
                    $topMedis = $jobData['medical'];
                    $x=0;
                    foreach ($topMedis as $topMedi) {
                        $dataMedis[$x] = ["jobpost_id"=>$jobInserted, "medical_id"=>$topMedi];
                        $x++;
                    }
                    $this->Jobpost_Model->job_medical_insert($dataMedis);
                }
                if(isset($jobData['shifts'])) {
                    $topShifts = $jobData['shifts'];
                    $x=0;
                    foreach ($topShifts as $topShift) {
                        $dataShifts[$x] = ["jobpost_id"=>$jobInserted, "workshift_id"=>$topShift];
                        $x++;
                    }
                    $this->Jobpost_Model->job_shifts_insert($dataShifts);
                }
                
                if(isset($jobData['leavs'])) {
                    $topLeaves = $jobData['leavs'];
                    $x=0;
                    foreach ($topLeaves as $topLeave) {
                        $dataLeaves[$x] = ["jobpost_id"=>$jobInserted, "leaves_id"=>$topLeave];
                        $x++;
                    }
                    $this->Jobpost_Model->job_leaves_insert($dataLeaves);
                }

                if($jobInserted) {

                    // Subscription

                    $getplan = $this->Subscription_Model->view_existing($jobData['recruId']);

                    $updatePlan = ["remaining_job_post"=>$getplan[0]['remaining_job_post'] - 1];

                    $this->Subscription_Model->view_existing_updation($updatePlan, $getplan[0]['id']);

                    // save record for notifications

                    $jobsubcat = $jobData['subcategory'];
                    $joblocationid = $jobData['jobLoc'];
                    $jobtitle = $jobData['jobTitle'];

                    $getLocation = $this->Common_Model->company_address_location($joblocationid);
                    $joblocation = $getLocation[0]['city'];
                    $notifyMsg = "A new job matching your interests was recently posted. Click here to view and apply!";
                    $getCatData = $this->Common_Model->subcategory_listsbysubcatid($jobsubcat);

                    $this->load->model('Jobseekeradmin_Model');
                    $getusers = $this->Jobseekeradmin_Model->jobseeker_bylocationlists($getCatData[0]['subcategory'], $joblocation);
                    
                    $record = ["job_id"=>$jobInserted, "recruiter_id"=>$recruitid, "title"=>$jobtitle, "notification"=> $notifyMsg, "intrestedIn"=>$getCatData[0]['subcategory']];
                    $notifyid = $this->Jobpost_Model->jobrecord_notifydata($record);

                    if($getusers) {
                        
                        foreach($getusers as $getuser) {
                            
                            if(strlen($getuser['latitude']) > 0) {

                                $distance = $this->distance($getuser['latitude'], $getuser['longitude'], $getLocation[0]['latitude'], $getLocation[0]['longitude'], "K");
                                $distance = number_format((float)$distance, 2, '.', ''); 

                                if($distance <= 50) {
                                    $saveNotify = ["notify_id"=>$notifyid, "user_id"=>$getuser['id'], "job_id"=>$jobInserted, "recruiter_id"=>$recruitid, "title"=>$jobtitle, "notification"=> $notifyMsg, "status"=>1];

                                    $this->Jobpost_Model->newjob_notifydata($saveNotify);
                                }
                            }
                        }
                    }

                    $this->session->set_tempdata('inserted', 'Job Posted Successfully',5);
                    redirect("recruiter/jobpost/manageJobView", $dataId);
                } else {
                    $this->session->set_tempdata('postError', 'Job Post not able to created',5);
                    $this->load->view('recruiter/jobpost/postjob');
                }

            }
        }
    }


    public function getLatLonggg($address)
    {
        if (!empty($address)) {
            //Formatted address
            $formattedAddr = str_replace(' ', '+', $address);
            //Send request and receive json data by address
            @$geocodeFromAddr = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
            if ($geocodeFromAddr) {
                $output = json_decode($geocodeFromAddr);
                //Get latitude and longitute from json data
                $data['latitude']  = $output->results[0]->geometry->location->lat;
                $data['longitude'] = $output->results[0]->geometry->location->lng;
                //var_dump($output->results[0]->address_components[0]->types);die;
                for ($j = 0; $j < count($output->results[0]->address_components); $j++) {
                    $city = $output->results[0]->address_components[$j]->types;
                    if ($city[0] == "locality") {
                        $data['city'] = $output->results[0]->address_components[$j]->long_name;
                    }
                }
                if (!empty($data)) {
                    return $data;
                } else {
                    return false;
                }
            } else {
                $data['latitude']  = '';
                $data['longitude'] = '';
                $data['city'] = '';
            }
        } else {
            return false;
        }
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) *sin(deg2rad($lat2)) + cos(deg2rad($lat1))* cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist* 60 *1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }


    public function jobpostViews() {
        $jobId = $this->uri->segment(4);
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $userSession['id'] = $userSession['parent_id'];
            $subrecruiter_id = $userSession['id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobId);
        $data['jobImg'] = $this->Jobpost_Model->job_images($jobId);
        $data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($jobId);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($data['getJobs'][0]['company_id']);
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists1();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $data['skills'] = $this->Jobpost_Model->skill_lists();

        $skills = $this->Jobpost_Model->jobs_skills_single($jobId);
        if($skills) { 
            $x1=0;
            foreach ($skills as $skill) {
              $skills1[$x1] = $skill['skill_id'];
              $x1++;
            }
            $data['skills2'] = $skills1;
        } else{
            $data['skills2'] = [];
        }

        $topPicks = $this->Recruit_Model->company_topicks_single($jobId);
        
        $topPicks = $this->Jobpost_Model->jobs_topicks_single($jobId);
        
        if($topPicks) { 
            $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];
              $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }
        
        $allowances = $this->Jobpost_Model->jobs_allowances_single($jobId);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Jobpost_Model->jobs_medical_single($jobId);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }

        $workshifts = $this->Jobpost_Model->jobs_workshifts_single($jobId);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        $leaves = $this->Jobpost_Model->jobs_leaves_single($jobId);
        //echo $this->db->last_query();die;
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leaves1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leaves2'] = $leaves1;
        } else{
            $data['leaves2'] = [];
        }
         $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/updatejob', $data);
    }

    public function manageJobView() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($this->input->get('status'))){
            $status = $this->input->get('status');
        }else{
            $status='Active';
        }
        if(!empty($userSession['label']) && $userSession['label']=='3'){
             $subrecruiter_id = $userSession['id'];
             $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        if(!empty($this->input->get('cid'))){
            $cid = base64_decode($this->input->get('cid'));
            //$filter = $this->input->get('status');
            $getJobs = $this->Jobpost_Model->job_fetch1($userSession['id'],$subrecruiter_id,$status,$cid);
        }else{
            $getJobs = $this->Jobpost_Model->job_fetch($userSession['id'],$subrecruiter_id,$status);
        }
        
        //echo $this->db->last_query();die;
        foreach($getJobs as $getJob) {
            
            $experience=$getJob['experience'];
            $designation=$getJob['jobtitle'];
            $suggestData = $this->suggestCandidate($getJob['id'],$experience,$designation);
            //echo "<pre>";
            //print_r($suggestData);
            $getappliedCount = $this->Jobpost_Model->jobappliedCount_fetch($getJob['id']);
            $addresses = $this->Common_Model->fetch_site_name($getJob['company_id']);
            if($getJob['subrecruiter_id']){
                $posted_id = $getJob['subrecruiter_id'];
            }else{
                $posted_id = $userSession['id'];
            }
            $subrecruiter = $this->Jobpost_Model->fetch_recruiter_name($posted_id);
            if(!empty($subrecruiter[0]['fname'])){
                $fname = $subrecruiter[0]['fname'];
            }else{
                $fname = '';
            }
            if(!empty($subrecruiter[0]['lname'])){
                $lname = $subrecruiter[0]['lname'];
            }else{
                $lname = '';
            }
            
            $counthiredjobs = $this->Jobpost_Model->hiredjob_appliedbyyjobid($getJob['id']);
            $countnewjobs = $this->Jobpost_Model->job_appliedbyyjobid($getJob['id']);
            $countgoingjobs = $this->Jobpost_Model->goingjob_appliedbyyjobid($getJob['id']);
            $countacptjobs = $this->Jobpost_Model->acceptedjob_appliedbyyjobid($getJob['id']);
            
            //print_r($candidatesApplied);
            $data['jobFetch'][] = [
                                    "id"=>$getJob['id'],
                                    "address" => $getJob['address'],
                                    "opening" => $getJob['opening'],
                                    "jobtitle"=>$getJob['jobtitle'],
                                    "jobDesc"=>$getJob['jobDesc'],
                                    "companydetail"=>$getJob['companydetail'],
                                    "qualification"=>$getJob['education'],
                                    "skills"=>$getJob['skills'],
                                    "salary"=>$getJob['salary'],
                                    "totalApply"=>$getappliedCount[0]['appliedCount'],
                                    "boost_status"=>$getJob['boost_status'],
                                    "jobExpire"=>$getJob['jobexpire'],
                                    "created_at"=>$getJob['created_at'],
                                    "site_name"=> $addresses[0]['cname'],
                                    "hiredcount" => $counthiredjobs[0]['newcount'],
                                    "newcount" => $countnewjobs[0]['newcount'],
                                    "countgoingjobs" => $countgoingjobs[0]['newcount'],
                                    "acptcount" => $countacptjobs[0]['newcount'],
                                    'subrecruiter_name' => $fname.' '.$lname,                                   
                                    'mode' => $getJob['mode'],
                                    ];
        }

        $data['jobApplicationFetch'] = $this->Jobpost_Model->jobApplication_fetch($userSession['id'],$subrecruiter_id);
        $data['candidateswaiting'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $data['newjobApplicationFetch'] = $this->Jobpost_Model->newjobApplication_fetch($userSession['id'],$subrecruiter_id);
        
        if(!empty($this->input->get('cid'))){
            $cid = base64_decode($this->input->get('cid'));
            $data['activejobFetch'] = $this->Jobpost_Model->activeJob_fetch1($userSession['id'],$subrecruiter_id,$cid);
            //echo $this->db->last_query();die;
        }else{
            $data['activejobFetch'] = $this->Jobpost_Model->activeJob_fetch($userSession['id'],$subrecruiter_id);
        }   
        if(!empty($this->input->get('cid'))){
            $cid = base64_decode($this->input->get('cid'));
            $data['expirejobFetch'] = $this->Jobpost_Model->expireJob_fetch1($userSession['id'],$subrecruiter_id,$cid);
        }else{
            $data['expirejobFetch'] = $this->Jobpost_Model->expireJob_fetch($userSession['id'],$subrecruiter_id);
        }    
        $data['boostAmountFetch'] = $this->Jobpost_Model->boostamount_fetch();
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        //echo $this->db->last_query();

        $data['recruitFetch'] = $this->Recruit_Model->subrecruiters($userSession['id']); 

        $this->load->view('recruiter/manage_jobs', $data);
    }
    
    public function manageJobView1() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
             $subrecruiter_id = $userSession['id'];
             $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $getJobs = $this->Jobpost_Model->job_fetch($userSession['id'],$subrecruiter_id);
        //echo $this->db->last_query();die;
        foreach($getJobs as $getJob) {
            $getappliedCount = $this->Jobpost_Model->jobappliedCount_fetch($getJob['id']);
            $data['jobFetch'][] = [
                                    "id"=>$getJob['id'],
                                    "address" => $getJob['address'],
                                    "opening" => $getJob['opening'],
                                    "jobtitle"=>$getJob['jobtitle'],
                                    "jobDesc"=>$getJob['jobDesc'],
                                    "companydetail"=>$getJob['companydetail'],
                                    "qualification"=>$getJob['education'],
                                    "skills"=>$getJob['skills'],
                                    "salary"=>$getJob['salary'],
                                    "totalApply"=>$getappliedCount[0]['appliedCount'],
                                    ];
        }

        $data['jobApplicationFetch'] = $this->Jobpost_Model->jobApplication_fetch($userSession['id'],$subrecruiter_id);
        $data['activejobFetch'] = $this->Jobpost_Model->activeJob_fetch($userSession['id'],$subrecruiter_id);
        $data['boostAmountFetch'] = $this->Jobpost_Model->boostamount_fetch();
        //echo $this->db->last_query();
        $this->load->view('recruiter/manage_jobs1', $data);
    }

    public function jobpostView() {
        $jobId = base64_decode($_GET['type']);
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $userSession['id'] = $userSession['parent_id'];
            $subrecruiter_id = $userSession['id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobId);
        $data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($jobId);
        //print_r($data['getJobexps']);die;
        $data['jobImg'] = $this->Jobpost_Model->job_images($jobId);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists1();
         $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
         $data['skills'] = $this->Jobpost_Model->skill_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $data['recruiterStatus'] = $this->Common_Model->fetch_site_name($userSession['id']);
        if(isset($data['getJobs'][0]['recruiter_id'])){
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($data['getJobs'][0]['recruiter_id']);
        }
        
        //echo $this->db->last_query();die;
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);

        $skills = $this->Jobpost_Model->jobs_skills_single($jobId);
        if($skills) { 
            $x1=0;
            foreach ($skills as $skill) {
              $skills1[$x1] = $skill['skill_id'];
              $x1++;
            }
            $data['skills2'] = $skills1;
        } else{
            $data['skills2'] = [];
        }

        $topPicks = $this->Jobpost_Model->jobs_topicks_single($jobId);
        
        if($topPicks) { 
            $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];
              $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }
        
        $allowances = $this->Jobpost_Model->jobs_allowances_single($jobId);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Jobpost_Model->jobs_medical_single($jobId);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }

        $workshifts = $this->Jobpost_Model->jobs_workshifts_single($jobId);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        
        $leaves = $this->Jobpost_Model->jobs_leaves_single($jobId);
        //echo $this->db->last_query();die;
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leaves1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leaves2'] = $leaves1;
        } else{
            $data['leaves2'] = [];
        }
         $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/updatejob', $data);
    }
    public function jobpostView1() {
        $jobId = $_GET['type'];
        $userSession = $this->session->userdata('userSession');
        $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobId);
        $data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($jobId);
        $data['jobImg'] = $this->Jobpost_Model->job_images($jobId);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($data['getJobs'][0]['recruiter_id']);
        //echo $this->db->last_query();die;
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        
        $topPicks = $this->Jobpost_Model->jobs_topicks_single($jobId);
        
        if($topPicks) { 
            $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];
              $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }
        
        $allowances = $this->Jobpost_Model->jobs_allowances_single($jobId);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Jobpost_Model->jobs_medical_single($jobId);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }

        $workshifts = $this->Jobpost_Model->jobs_workshifts_single($jobId);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        
        $leaves = $this->Jobpost_Model->jobs_leaves_single($jobId);
        //echo $this->db->last_query();die;
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leaves1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leaves2'] = $leaves1;
        } else{
            $data['leaves2'] = [];
        }
        $this->load->view('recruiter/updatejob1', $data);
    }

    public function booststatus() {
        $jid = $this->input->post('jid');
        $data1 = ["boost_status"=>1, "boost_date"=> date("y-m-d H:i:s")];
        $jobseekerInserted = $this->Jobpost_Model->jobboostStatus($data1, $jid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {

            $userSession = $this->session->userdata('userSession');
            if(!empty($userSession['label']) && $userSession['label']=='3'){
                $subrecruiter_id = $userSession['id'];
                $userSession['id'] = $userSession['parent_id'];
            }else{
                $userSession['id'] = $userSession['id'];
                $subrecruiter_id = 0;
            }

            // Subscription

            $getplan = $this->Subscription_Model->view_existing($userSession['id']);
            $updatePlan = ["remaining_job_boost"=>$getplan[0]['remaining_job_boost'] - 1];
            $this->Subscription_Model->view_existing_updation($updatePlan, $getplan[0]['id']);
            
            redirect("recruiter/jobpost/manageJobView");
        } else {
            redirect("recruiter/jobpost/manageJobView");
        }
        
    }

    public function getLatLong($address){
        if(!empty($address)){
            //Formatted address
            $formattedAddr = str_replace(' ','+',$address);
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk'); 
            $output = json_decode($geocodeFromAddr);
            //Get latitude and longitute from json data
            $data['latitude']  = $output->results[0]->geometry->location->lat; 
            $data['longitude'] = $output->results[0]->geometry->location->lng;
            //Return latitude and longitude of the given address
            if(!empty($data)) {
                return $data;
            }else{
                return false;
            }
        }else{
            return false;   
        }
    }

    function deleteimg(){
        $id = $this->input->post('id');
        $delete = $this->Jobpost_Model->deleteimg($id);
        
    }

    public function deleteJob(){
        $id = $this->input->get('id');
        $this->Jobpost_Model->deleteJob($id);
        $this->Jobpost_Model->deletejob_images($id);
        $this->Jobpost_Model->deleteappliedJob($id);
        $this->Jobpost_Model->job_toppicks_delete($id);
        $this->Jobpost_Model->job_allowances_delete($id);
        $this->Jobpost_Model->job_medical_delete($id);
        $this->Jobpost_Model->job_shifts_delete($id);
        $this->Jobpost_Model->job_leaves_delete($id);
        redirect('recruiter/jobpost/manageJobView');
    }

    public function jobCancel() {
        $rid = $_GET['id'];
        $data1 = ["status"=>2];
        $jobseekerInserted = $this->Jobpost_Model->jobStatus($data1, $rid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
             redirect('recruiter/jobpost/manageJobView');
        } else {
             redirect('recruiter/jobpost/manageJobView');
        }
        
    }

    public function resizeImage($filename)
       {
          $source_path =  './recruiterupload/' . $filename;
          $target_path = './recruiterupload/thumbs/';
          $config_manip = array(
              'image_library' => 'gd2',
              'source_image' => $source_path,
              'new_image' => $target_path,
              'maintain_ratio' => TRUE,
              'create_thumb' => TRUE,
              'width' => 150,
              'height' => 150
          );
    
    
          $this->load->library('image_lib', $config_manip);
          if (!$this->image_lib->resize()) {
              echo $this->image_lib->display_errors();
          }
    
    
          //$this->image_lib->clear();
       }

       public function fetchpending($userSession,$subrecruiter_id){
        $data["pendingData"] = [];
        $pending = $this->Candidate_Model->candidateduenotification_list($userSession,$subrecruiter_id); 
        if (!empty($pending)) {
            foreach ($pending as $pendings) {
                //echo $pendings['updated_at'];die;
                $enddate =  date('Y-m-d', strtotime($pendings['updated_at']. ' + 3 days'));
                //echo $enddate;die;
                $enddate = strtotime($enddate);
                $updated_date=  strtotime($pendings['updated_at']);
                $now = time();
                $datediff =  $now - $updated_date;

                $daydiff = floor($datediff / (60*60*24));
                //echo $daydiff;die;
                if($daydiff >=3)
                {
                    $data["pendingData"][] =["profilePic"=>$pendings['profilePic'],"user_id"=>$pendings['user_id'], "name"=>$pendings['name'], "email"=>$pendings['email'], 'phone'=>$pendings['phone'], "location"=>$pendings['location'], "status"=>$pendings['status'], "interviewdate" => $pendings['interviewdate'], 'interviewtime' => $pendings['interviewtime'] , "date_day1" => $pendings['date_day1'], 'jobDesc' => $pendings['jobDesc'], "jobtitle"=>$pendings['jobtitle'], "compid"=>$pendings['compid'], "id"=>$pendings['id'], 'user_id'=>$pendings['user_id'],'jobpost_id'=>$pendings['jobpost_id'], 'updated_at'=> $pendings['updated_at'], 'fallout_reason' => $pendings['fallout_reason']];
                }

            }
        }
        return $data["pendingData"];
    }

    public function suggestCandidate($jobtype,$exp,$designation){
        $skill = $this->Jobpost_Model->job_skills($jobtype);
        $experience=$exp;
        $designation=$designation;
       //echo $designation;die;
        //$skill = explode(', ',$skills);

        $return_array = array();

         
       
        foreach($skill as $i){
          //echo $i['skill'];die;
           $suggest= $this->Candidate_Model->suggestedcandidate($i['skill']);
           //echo $this->db->last_query();
           //print_r($suggest);
           foreach($suggest as $value) {
           
           if(!empty($value)) {
                $user_desgexp = $this->Candidate_Model->user_list1($value['user_id']);
                //print_r($user_desgexp);
                //echo $this->db->last_query();die;
                if($user_desgexp[0]['exp_year'] == $experience || $user_desgexp[0]['designation'] == $designation){    
                    $return_array[] =  $user_desgexp['id'];
                }
                //$return_array[]= $value['user_id'];
            }
           }
           
           
        }

        // $user_desgexp = $this->Candidate_Model->user_list();
        // //print_r($user_desgexp);die;

        // foreach($user_desgexp as $key){
        //   if($key['exp_year'] == $experience || $key['designation'] == $designation){
              
        //        $return_array[] =  $key['id'];
        //   }                
        // }
        
       // print_r($return_array);die;
        //$return_array = array_unique($return_array);
        //print_r($return_array);
        foreach ($return_array as $key =>  $value) {
             $fetchcandidatedetails = $this->Candidate_Model->fetchcandidate($value,$jobtype);

             if(!empty($fetchcandidatedetails)){
                unset($return_array[$key]);
             }
        }
       
        
        foreach($return_array as $userid){
           $candiade = $this->Candidate_Model->fetchsuggested($userid);
            //echo $this->db->last_query();
        }

        if(!empty($candiade)){

            return $candiade;
        }
    }

    public function assignjob() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }

        $userSession = $this->session->userdata('userSession');

        if($userSession['label'] == 1 || $userSession['label'] == 0) {

            //var_dump($userSession['id']);die;
            $data['recruitFetch'] = $this->Recruit_Model->subrecruitersassignforsuper($userSession['id']);
        
        } else {
            $userSessionid = $userSession['parent_id'];
            $data['recruitFetch'] = $this->Recruit_Model->subrecruitersassign($userSessionid, $userSession['id']);
        }
        
        $this->load->view('recruiter/assignjob',$data);
    }

    public function serchfetchjob() {
        $jobData = $this->input->post();
        $jobId = $jobData['jid']; 

        $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobId);

        if(count($data['getJobs']) > 0) {

            $data['levels'] = $this->Jobpost_Model->levelbyid($data['getJobs'][0]['level']);
            $data['created_at'] = date('d-m-Y', strtotime($data['getJobs'][0]['created_at']));

            if($data['getJobs'][0]['subrecruiter_id'] != 0){
                $posted_id = $data['getJobs'][0]['subrecruiter_id'];
            }else{
                $posted_id = $data['getJobs'][0]['recruiter_id'];
            }
            
            $subrecruiter = $this->Jobpost_Model->fetch_recruiter_name($posted_id);
            if(!empty($subrecruiter[0]['fname'])){
                $fname = $subrecruiter[0]['fname'];
            } else {
                $fname = '';
            }
            if(!empty($subrecruiter[0]['lname'])){
                $lname = $subrecruiter[0]['lname'];
            }else{
                $lname = '';
            }
            $data['postedBy'] = $fname.''.$lname;
            $data['postedid'] = $posted_id;
        
        } else {
            $data['error'] = "No job found";
        }

        $userSession = $this->session->userdata('userSession');
        
        if(!empty($userSession['label']) && $userSession['label']=='3') {
            //$userSession['id'] = $userSession['parent_id'];
            $subrecruiter_id = $userSession['id'];
            $rid = $userSession['id'];

            if($rid == $posted_id) {
               
            } else {
                $data['error'] = "Someone already assign in job";
            }
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
            $rid = $userSession['id'];
        }

        echo json_encode($data);
    }

    public function assignrecruit() {
        $jid = $this->input->post('jid');
        $assignid = $this->input->post('assignid');
        
        // $userSession = $this->session->userdata('userSession');
        
        // if(!empty($userSession['label']) && $userSession['label']=='3') {
        //     //$userSession['id'] = $userSession['parent_id'];
        //     $subrecruiter_id = $userSession['id'];

        //     $rid = $assignid;
            
        // } else {
        //     $userSession['id'] = $userSession['id'];
        //     $subrecruiter_id = 0;

        //     $rid = $assignid;
        //     $data1 = ["recruiter_id"=>$rid,"subrecruiter_id"=>0];
        // }

        $data1 = ["subrecruiter_id"=>$assignid];
        // var_dump($jid);
        // var_dump($data1);die;

        $jobseekerInserted = $this->Jobpost_Model->jobassignchange($data1, $jid);
 
        if($jobseekerInserted) {
            $this->session->set_tempdata('inserted', 'Job Assigned Successfully',5);
            redirect("recruiter/jobpost/assignjob");
        } else {
            redirect("recruiter/jobpost/assignjob");
        }
        
    }

}
?>
