<?php
ob_start();
ini_set("allow_url_fopen", 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Report extends CI_Controller {
  function __construct() {
      // Construct the parent class
      parent::__construct();
      $this->load->model('recruiter/Jobpost_Model');
      $this->load->model('recruiter/Recruit_Model');
      $this->load->model('Common_Model');
      $this->load->library('form_validation');
      $this->load->model('Jobpostrecruiter_Model');
      $this->load->library('encryption');

      if($this->session->userdata('userSession')) {
          
      } else {
          redirect("recruiter/recruiter/index");
      }
  }

  public function index() {
      $userSession = $this->session->userdata('userSession');
      if(!empty($userSession['label']) && $userSession['label']=='3'){
          $subrecruiter_id = $userSession['id'];
          $userSession['id'] = $userSession['parent_id'];
      }else{
          $userSession['id'] = $userSession['id'];
          $subrecruiter_id=0;
      }
      $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
      $this->load->view('recruiter/report-list',$data);
  }
	
	public function jobwise_report() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/jobwise_report',$data);
  }

	public function candidate_report() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/candidate_report',$data);
  }

    public function getDatabyStatus(){
              //$status = $this->input->post('status');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus1($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        }
        //echo $date;die;
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Expiration Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Level</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Education</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Experience</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">For Bilingual, Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Target HC</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">New Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Ongoing Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Accepted Job Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">No Show</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not meet requirement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not accept Job Offer</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day1 No Show</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Refer</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          
          foreach($data as $jobList) {
              $level = $this->Jobpost_Model->levelbyid($jobList['level']);
              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
			  $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
			  $language = $this->Jobpost_Model->languagebyid($jobList['language']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

        if(!empty($subrecruiter_id)){
          $companyname = $compdetail[0]['cname']??'';
        }else{
          $companyname = $jobList['cname']??'';
        }

        if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                $jobList['fname'] = $jobdetail[0]['fname']??'';
                $jobList['lname'] = $jobdetail[0]['lname']??'';
              }
              $total = $this->Jobpost_Model->jobappliedCount_fetch($jobList['id']);
			  $new = $this->Jobpost_Model->jobnewCount_fetch($jobList['id']);
			  $going = $this->Jobpost_Model->jobgoingCount_fetch($jobList['id']);
			  $fall = $this->Jobpost_Model->jobfallCount_fetch($jobList['id']);
              $totalaccpt = $this->Jobpost_Model->totalaccpt($jobList['id']);
			  $totalhired = $this->Jobpost_Model->totalhired($jobList['id']);
              $noshow = $this->Jobpost_Model->noshow($jobList['id']);
              $noshow2 = $this->Jobpost_Model->noshow2($jobList['id']);
              $noshow3 = $this->Jobpost_Model->noshow3($jobList['id']);
              $noshow5 = $this->Jobpost_Model->noshow5($jobList['id']);
              $refer = $this->Jobpost_Model->refer($jobList['id']);             
			  if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['jobexpire'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$level[0]['level'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['experience'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
              <td style="width:70px;">'.$language[0]['name'].'</td>
              <td style="width:70px;">'.$jobList['other_language'].'</td>
              <td style="width:70px;">'.$jobList['opening'].'</td>
              <td style="width:70px;">'.$total[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$new[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$going[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalaccpt[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalhired[0]['appliedCount'].'</td>
              <td style="width:70px;">'	.$fall[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow2[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow3[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow5[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$refer[0]['appliedCount'].'</td>
           </tr>';
           
             }
             
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
       }

   public function getDatabyStatusweek(){
         //$status = $this->input->post('status');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }

        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus1($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Expiration Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Level</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Education</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Experience</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">For Bilingual, Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Target HC</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">New Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Ongoing Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Accepted Job Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">No Show</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not meet requirement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not accept Job Offer</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day1 No Show</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Refer</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          
          foreach($data as $jobList) {
              $level = $this->Jobpost_Model->levelbyid($jobList['level']);
              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
			        $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
			        $language = $this->Jobpost_Model->languagebyid($jobList['language']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              
              if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
                $companyname = $compdetail[0]['cname']??'';
              }else{
                $companyname = $jobList['cname']??'';
              }
       
        if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                if($jobdetail) {
                  $jobList['fname'] = $jobdetail[0]['fname'];
                  $jobList['lname'] = $jobdetail[0]['lname'];
                } else {
                  $jobList['fname'] =  "";
                  $jobList['lname'] = "";
                }
        }
        
        $total = $this->Jobpost_Model->jobappliedCount_fetch($jobList['id']);
			  $new = $this->Jobpost_Model->jobnewCount_fetch($jobList['id']);
			  $going = $this->Jobpost_Model->jobgoingCount_fetch($jobList['id']);
			  $fall = $this->Jobpost_Model->jobfallCount_fetch($jobList['id']);
              $totalaccpt = $this->Jobpost_Model->totalaccpt($jobList['id']);
			  $totalhired = $this->Jobpost_Model->totalhired($jobList['id']);
              $noshow = $this->Jobpost_Model->noshow($jobList['id']);
              $noshow2 = $this->Jobpost_Model->noshow2($jobList['id']);
              $noshow3 = $this->Jobpost_Model->noshow3($jobList['id']);
              $noshow5 = $this->Jobpost_Model->noshow5($jobList['id']);
              $refer = $this->Jobpost_Model->refer($jobList['id']);
             
             //echo date('Y-m-d', strtotime('-7 days'));die;
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
              
              $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['jobexpire'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$level[0]['level'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['experience'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
              <td style="width:70px;">'.$language[0]['name'].'</td>
              <td style="width:70px;">'.$jobList['other_language'].'</td>
              <td style="width:70px;">'.$jobList['opening'].'</td>
              <td style="width:70px;">'.$total[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$new[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$going[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalaccpt[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalhired[0]['appliedCount'].'</td>
              <td style="width:70px;">'	.$fall[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow2[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow3[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow5[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$refer[0]['appliedCount'].'</td>
              
              
           </tr>';
           
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
    
    }    


  public function getDatabyStatusmonth(){
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        //$status = $this->input->post('status');
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus1($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Expiration Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Level</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Education</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Experience</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">For Bilingual, Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Target HC</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">New Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Ongoing Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Accepted Job Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">No Show</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not meet requirement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not accept Job Offer</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day1 No Show</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Refer</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          
          foreach($data as $jobList) {
              $level = $this->Jobpost_Model->levelbyid($jobList['level']);
              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
      			  $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
      			  $language = $this->Jobpost_Model->languagebyid($jobList['language']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              }
              if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                if($jobdetail) {
                  $jobList['fname'] = $jobdetail[0]['fname'];
                  $jobList['lname'] = $jobdetail[0]['lname'];
                } else {
                  $jobList['fname'] = "";
                  $jobList['lname'] = "";
                }
              } 

              if(!empty($subrecruiter_id)){
          $companyname = $compdetail[0]['cname']??'';
        }else{
          $companyname = $jobList['cname']??'';
        }
              $total = $this->Jobpost_Model->jobappliedCount_fetch($jobList['id']);
			  $new = $this->Jobpost_Model->jobnewCount_fetch($jobList['id']);
			  $going = $this->Jobpost_Model->jobgoingCount_fetch($jobList['id']);
			  $fall = $this->Jobpost_Model->jobfallCount_fetch($jobList['id']);
              $totalaccpt = $this->Jobpost_Model->totalaccpt($jobList['id']);
			  $totalhired = $this->Jobpost_Model->totalhired($jobList['id']);
              $noshow = $this->Jobpost_Model->noshow($jobList['id']);
              $noshow2 = $this->Jobpost_Model->noshow2($jobList['id']);
              $noshow3 = $this->Jobpost_Model->noshow3($jobList['id']);
              $noshow5 = $this->Jobpost_Model->noshow5($jobList['id']);
              $refer = $this->Jobpost_Model->refer($jobList['id']);

             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['jobexpire'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$level[0]['level'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['experience'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
              <td style="width:70px;">'.$language[0]['name'].'</td>
              <td style="width:70px;">'.$jobList['other_language'].'</td>
              <td style="width:70px;">'.$jobList['opening'].'</td>
              <td style="width:70px;">'.$total[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$new[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$going[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalaccpt[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalhired[0]['appliedCount'].'</td>
              <td style="width:70px;">'	.$fall[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow2[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow3[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow5[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$refer[0]['appliedCount'].'</td>
              
           </tr>';
           
             }
       
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
    }    

  public function getDatabyStatusyear(){
        //$status = $this->input->post('status');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $date =  date('Y-m-d');
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus1($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)) {
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Expiration Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Level</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Education</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Experience</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">For Bilingual, Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Target HC</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">New Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Ongoing Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Accepted Job Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">No Show</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not meet requirement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not accept Job Offer</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day1 No Show</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Refer</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          
          foreach($data as $jobList) {
              $level = $this->Jobpost_Model->levelbyid($jobList['level']);
              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
			        $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
			        $language = $this->Jobpost_Model->languagebyid($jobList['language']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 
              if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                if($jobdetail) {
                  $jobList['fname'] = $jobdetail[0]['fname'];
                  $jobList['lname'] = $jobdetail[0]['lname'];
                } else {
                    $jobList['fname'] = "";
                    $jobList['lname'] = "";
                }
              }

              if(!empty($subrecruiter_id)){
          $companyname = $compdetail[0]['cname']??'';
        }else{
          $companyname = $jobList['cname']??'';
        }
              $total = $this->Jobpost_Model->jobappliedCount_fetch($jobList['id']);
			  $new = $this->Jobpost_Model->jobnewCount_fetch($jobList['id']);
			  $going = $this->Jobpost_Model->jobgoingCount_fetch($jobList['id']);
			  $fall = $this->Jobpost_Model->jobfallCount_fetch($jobList['id']);
              $totalaccpt = $this->Jobpost_Model->totalaccpt($jobList['id']);
			  $totalhired = $this->Jobpost_Model->totalhired($jobList['id']);
              $noshow = $this->Jobpost_Model->noshow($jobList['id']);
              $noshow2 = $this->Jobpost_Model->noshow2($jobList['id']);
              $noshow3 = $this->Jobpost_Model->noshow3($jobList['id']);
              $noshow5 = $this->Jobpost_Model->noshow5($jobList['id']);
              $refer = $this->Jobpost_Model->refer($jobList['id']);
             
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['jobexpire'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$level[0]['level'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['experience'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
              <td style="width:70px;">'.$language[0]['name'].'</td>
              <td style="width:70px;">'.$jobList['other_language'].'</td>
              <td style="width:70px;">'.$jobList['opening'].'</td>
              <td style="width:70px;">'.$total[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$new[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$going[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalaccpt[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalhired[0]['appliedCount'].'</td>
              <td style="width:70px;">'	.$fall[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow2[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow3[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow5[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$refer[0]['appliedCount'].'</td>
              
              
           </tr>';
      
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;

    }

    public function getDatabyStatusDate(){
        //$status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus1($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus1($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Expiration Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Level</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Education</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Required Level of Experience</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Mode</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">For Bilingual, Language Supported</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Target HC</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">New Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Ongoing Applications</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Accepted Job Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Fall Out</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">No Show</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not meet requirement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Did not accept Job Offer</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day1 No Show</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Refer</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          $x1 =1;
          foreach($data as $jobList) {
            $level = $this->Jobpost_Model->levelbyid($jobList['level']);
              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
			  $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
			  $language = $this->Jobpost_Model->languagebyid($jobList['language']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
          $companyname = $compdetail[0]['cname']??'';
        }else{
          $companyname = $jobList['cname']??'';
        }
        if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                if($jobdetail) {
                  $jobList['fname'] = $jobdetail[0]['fname'];
                  $jobList['lname'] = $jobdetail[0]['lname'];
                } else {
                    $jobList['fname'] = "";
                  $jobList['lname'] = "";
                }
              }
              $total = $this->Jobpost_Model->jobappliedCount_fetch($jobList['id']);
			  $new = $this->Jobpost_Model->jobnewCount_fetch($jobList['id']);
			  $going = $this->Jobpost_Model->jobgoingCount_fetch($jobList['id']);
			  $fall = $this->Jobpost_Model->jobfallCount_fetch($jobList['id']);
              $totalaccpt = $this->Jobpost_Model->totalaccpt($jobList['id']);
			  $totalhired = $this->Jobpost_Model->totalhired($jobList['id']);
              $noshow = $this->Jobpost_Model->noshow($jobList['id']);
              $noshow2 = $this->Jobpost_Model->noshow2($jobList['id']);
              $noshow3 = $this->Jobpost_Model->noshow3($jobList['id']);
              $noshow5 = $this->Jobpost_Model->noshow5($jobList['id']);
              $refer = $this->Jobpost_Model->refer($jobList['id']);
            //echo date('Y-m-d', strtotime($jobList['created_at']));
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date &&   date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['jobexpire'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$level[0]['level'].'</td>
              <td style="width:70px;">'.$jobList['education'].'</td>
              <td style="width:70px;">'.$jobList['experience'].'</td>
              <td style="width:70px;">'.$jobList['mode'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
              <td style="width:70px;">'.$language[0]['name'].'</td>
              <td style="width:70px;">'.$jobList['other_language'].'</td>
              <td style="width:70px;">'.$jobList['opening'].'</td>
              <td style="width:70px;">'.$total[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$new[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$going[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalaccpt[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$totalhired[0]['appliedCount'].'</td>
              <td style="width:70px;">'	.$fall[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow2[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow3[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$noshow5[0]['appliedCount'].'</td>
              <td style="width:70px;">'.$refer[0]['appliedCount'].'</td>
              
              
           </tr>';
           $x1++;
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;

    }

	public function getjobDatabyStatus(){
         //$status = $this->input->post('status');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus2($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">All Tenure</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Minimum Experience</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Less than 6months</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6mos to 1Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">1Year to 2Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">2Year to 3Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">3Year to 4Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">4Year to 5Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">5Year to 6Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6Year to 7Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">7 Year & Up</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Joining Bonus</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Food</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day Shift</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">14th Month Pay</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Critical Illness Benefits</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Life Insurance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Maternity Assistance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Medicine Reimbursement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Cellphone Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Parking</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Shuttle</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Annual Performance Bonus</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Retirement Benefits</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Transport Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Monthly Performance Incentive</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Weekends Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Holidays Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Mid Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Night Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">24/7</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          
          foreach($data as $jobList) {
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
          $companyname = $compdetail[0]['cname'];
        }else{
          $companyname = $jobList['cname'];
        }
			  $tenure = $this->Jobpost_Model->getSalary($jobList['id'],'0');
			  if(!empty($tenure[0]['basicsalary'])){
				  $tenure[0]['basicsalary'] = $tenure[0]['basicsalary'];
			  }else{
				  $tenure[0]['basicsalary'] = '';
			  }
			  $minimum = $this->Jobpost_Model->getSalary($jobList['id'],'1');
        if(!empty($minimum[0]['basicsalary'])){
          $minimum[0]['basicsalary'] = $minimum[0]['basicsalary'];
        }else{
          $minimum[0]['basicsalary'] = '';
        }
        $less = $this->Jobpost_Model->getSalary($jobList['id'],'2');
			  if(!empty($less[0]['basicsalary'])){
				  $less[0]['basicsalary'] = $less[0]['basicsalary'];
			  }else{
				  $less[0]['basicsalary'] = '';
			  }
			  $month = $this->Jobpost_Model->getSalary($jobList['id'],'3');
			  if(!empty($month[0]['basicsalary'])){
				  $month[0]['basicsalary'] = $month[0]['basicsalary'];
			  }else{
				  $month[0]['basicsalary'] = '';
			  }
			  $year = $this->Jobpost_Model->getSalary($jobList['id'],'4');
			  if(!empty($year[0]['basicsalary'])){
				  $year[0]['basicsalary'] = $year[0]['basicsalary'];
			  }else{
				  $year[0]['basicsalary'] = '';
			  }
			  $year2 = $this->Jobpost_Model->getSalary($jobList['id'],'5');
			  if(!empty($year2[0]['basicsalary'])){
				  $year2[0]['basicsalary'] = $year2[0]['basicsalary'];
			  }else{
				  $year2[0]['basicsalary'] = '';
			  }
			  $year3 = $this->Jobpost_Model->getSalary($jobList['id'],'6');
        if(!empty($year3[0]['basicsalary'])){
          $year3[0]['basicsalary'] = $year3[0]['basicsalary'];
        }else{
          $year3[0]['basicsalary'] = '';
        }$year4 = $this->Jobpost_Model->getSalary($jobList['id'],'7');
        if(!empty($year4[0]['basicsalary'])){
          $year4[0]['basicsalary'] = $year4[0]['basicsalary'];
        }else{
          $year4[0]['basicsalary'] = '';
        }$year5 = $this->Jobpost_Model->getSalary($jobList['id'],'8');
        if(!empty($year5[0]['basicsalary'])){
          $year5[0]['basicsalary'] = $year5[0]['basicsalary'];
        }else{
          $year5[0]['basicsalary'] = '';
        }$year6 = $this->Jobpost_Model->getSalary($jobList['id'],'9');
        if(!empty($year6[0]['basicsalary'])){
          $year6[0]['basicsalary'] = $year6[0]['basicsalary'];
        }else{
          $year6[0]['basicsalary'] = '';
        }$year7 = $this->Jobpost_Model->getSalary($jobList['id'],'10');
			  if(!empty($year7[0]['basicsalary'])){
				  $year7[0]['basicsalary'] = $year7[0]['basicsalary'];
			  }else{
				  $year7[0]['basicsalary'] = '';
			  }
			  $toppics = $this->Jobpost_Model->jobs_topicks($jobList['id']);
			  if($toppics){if(in_array(2, $toppics)){
				  $free_food = "Yes";
			  }else{
				  $free_food= 'No';
			  }if(in_array(3, $toppics)){
				  $hmo = "Yes";
			  }else{
				  $hmo= 'No';
			  }if(in_array(4, $toppics)){
				  $hmo_d = "Yes";
			  }else{
				  $hmo_d= 'No';
			  }if(in_array(5, $toppics)){
				  $day_shift = "Yes";
			  }else{
				  $day_shift= 'No';
			  }if(in_array(6, $toppics)){
				  $month_pay = "Yes";
			  }else{
				  $month_pay= 'No';
			  }}else{
			  	$free_food= 'No';
			  	$hmo= 'No';
			  	$day_shift= 'No';
			  	$month_pay= 'No';
          $hmo_d= 'No';
			  }
			  $medical = $this->Jobpost_Model->jobs_medical($jobList['id']);
			  if($medical){if(in_array(1, $medical)){
				  $free_hmo = "Yes";
			  }else{
				  $free_hmo= 'No';
			  }if(in_array(2, $medical)){
				  $critical = "Yes";
			  }else{
				  $critical= 'No';
			  }if(in_array(3, $medical)){
				  $life = "Yes";
			  }else{
				  $life= 'No';
			  }if(in_array(4, $medical)){
				  $maternity = "Yes";
			  }else{
				  $maternity= 'No';
			  }if(in_array(5, $medical)){
				  $medicine = "Yes";
			  }else{
				  $medicine= 'No';
			  }}else{
				$free_hmo= 'No';
				$critical= 'No';
				$life= 'No';
				$maternity= 'No';
				$medicine= 'No';
			  }
			  $allowances = $this->Jobpost_Model->jobs_allowances($jobList['id']);
			  if($allowances){if(in_array(1, $allowances)){
				  $cell_allow = "Yes";
			  }else{
				  $cell_allow= 'No';
			  }if(in_array(2, $allowances)){
				  $free_parking = "Yes";
			  }else{
				  $free_parking= 'No';
			  }if(in_array(3, $allowances)){
				  $free_shuttle = "Yes";
			  }else{
				  $free_shuttle= 'No';
			  }if(in_array(4, $allowances)){
				  $annual = "Yes";
			  }else{
				  $annual= 'No';
			  }if(in_array(5, $allowances)){
				  $retire = "Yes";
			  }else{
				  $retire= 'No';
			  }if(in_array(6, $allowances)){
				  $transport = "Yes";
			  }else{
				  $transport= 'No';
			  }if(in_array(7, $allowances)){
				  $monthly = "Yes";
			  }else{
				  $monthly= 'No';
			  }}else{
			  	$cell_allow= 'No';
			  	$free_parking= 'No';
			  	$free_shuttle= 'No';
			  	$annual= 'No';
			  	$retire= 'No';
			  	$transport= 'No';
			  	$monthly= 'No';
			  }
			  
			  $leaves = $this->Jobpost_Model->jobs_leaves($jobList['id']);
			  if($leaves){if(in_array(41, $leaves)){
				  $weekend = "Yes";
			  }else{
				  $weekend= 'No';
			  }if(in_array(42, $leaves)){
				  $holiday = "Yes";
			  }else{
				  $holiday= 'No';
			  }}else{
			  	$weekend= 'No';
			  	$holiday= 'No';
			  }
			  $shifts = $this->Jobpost_Model->jobs_workshifts_single($jobList['id']);
			  if($shifts){if($shifts[0]['workshift_id']=='1'){
				  $mid = "Yes";
			  }else{
				  $mid= 'No';
			  }if($shifts[0]['workshift_id']=='2'){
				  $night = "Yes";
			  }else{
				  $night= 'No';
			  }if($shifts[0]['workshift_id']=='3'){
				  $shift24 = "Yes";
			  }else{
				  $shift24= 'No';
			  }}else{
			  	$mid= 'No';
			  	$night= 'No';
			  	$shift24= 'No';
			  }
        if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                $jobList['fname'] = $jobdetail[0]['fname']??'';
                $jobList['lname'] = $jobdetail[0]['lname']??'';
              }
			  
              if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$tenure[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$minimum[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$less[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$month[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year2[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year3[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year4[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year5[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year6[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year7[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.$jobList['joining_bonus'].'</td>
              <td style="width:70px;">'.$free_food.'</td>
              <td style="width:70px;">'.$hmo.'</td>
              <td style="width:70px;">'.$hmo_d.'</td>
              <td style="width:70px;">'.$day_shift.'</td>
              <td style="width:70px;">'.$month_pay.'</td>
              <td style="width:70px;">'.$free_hmo.'</td>
              <td style="width:70px;">'	.$critical.'</td>
              <td style="width:70px;">'.$life.'</td>
              <td style="width:70px;">'.$maternity.'</td>
              <td style="width:70px;">'.$medicine.'</td>
              <td style="width:70px;">'.$cell_allow.'</td>
              <td style="width:70px;">'.$free_parking.'</td>
			  <td style="width:70px;">'.$free_shuttle.'</td>
			  <td style="width:70px;">'.$annual.'</td>
              <td style="width:70px;">'.$retire.'</td>
			  <td style="width:70px;">'.$transport.'</td>
              <td style="width:70px;">'.$monthly.'</td>
			  <td style="width:70px;">'.$weekend.'</td>
			  <td style="width:70px;">'.$holiday.'</td>
			  <td style="width:70px;">'.$mid.'</td>
			  <td style="width:70px;">'.$night.'</td>
			  <td style="width:70px;">'.$shift24.'</td>
           </tr>';
           
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
    
    }
	
	public function getjobDatabyStatusweek(){
        // $status = $this->input->post('status');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus2($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">All Tenure</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Minimum Experience</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Less than 6months</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6mos to 1Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">1Year to 2Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">2Year to 3Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">3Year to 4Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">4Year to 5Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">5Year to 6Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6Year to 7Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">7 Year & Up</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Joining Bonus</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Food</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day Shift</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">14th Month Pay</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Critical Illness Benefits</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Life Insurance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Maternity Assistance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Medicine Reimbursement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Cellphone Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Parking</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Shuttle</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Annual Performance Bonus</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Retirement Benefits</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Transport Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Monthly Performance Incentive</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Weekends Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Holidays Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Mid Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Night Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">24/7</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          
          foreach($data as $jobList) {
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
          $companyname = $compdetail[0]['cname'];
        }else{
          $companyname = $jobList['cname'];
        }
        if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                $jobList['fname'] = $jobdetail[0]['fname'];
                $jobList['lname'] = $jobdetail[0]['lname'];
              }
			  $tenure = $this->Jobpost_Model->getSalary($jobList['id'],'0');
        if(!empty($tenure[0]['basicsalary'])){
          $tenure[0]['basicsalary'] = $tenure[0]['basicsalary'];
        }else{
          $tenure[0]['basicsalary'] = '';
        }
        $minimum = $this->Jobpost_Model->getSalary($jobList['id'],'1');
        if(!empty($minimum[0]['basicsalary'])){
          $minimum[0]['basicsalary'] = $minimum[0]['basicsalary'];
        }else{
          $minimum[0]['basicsalary'] = '';
        }
        $less = $this->Jobpost_Model->getSalary($jobList['id'],'2');
        if(!empty($less[0]['basicsalary'])){
          $less[0]['basicsalary'] = $less[0]['basicsalary'];
        }else{
          $less[0]['basicsalary'] = '';
        }
        $month = $this->Jobpost_Model->getSalary($jobList['id'],'3');
        if(!empty($month[0]['basicsalary'])){
          $month[0]['basicsalary'] = $month[0]['basicsalary'];
        }else{
          $month[0]['basicsalary'] = '';
        }
        $year = $this->Jobpost_Model->getSalary($jobList['id'],'4');
        if(!empty($year[0]['basicsalary'])){
          $year[0]['basicsalary'] = $year[0]['basicsalary'];
        }else{
          $year[0]['basicsalary'] = '';
        }
        $year2 = $this->Jobpost_Model->getSalary($jobList['id'],'5');
        if(!empty($year2[0]['basicsalary'])){
          $year2[0]['basicsalary'] = $year2[0]['basicsalary'];
        }else{
          $year2[0]['basicsalary'] = '';
        }
        $year3 = $this->Jobpost_Model->getSalary($jobList['id'],'6');
        if(!empty($year3[0]['basicsalary'])){
          $year3[0]['basicsalary'] = $year3[0]['basicsalary'];
        }else{
          $year3[0]['basicsalary'] = '';
        }$year4 = $this->Jobpost_Model->getSalary($jobList['id'],'7');
        if(!empty($year4[0]['basicsalary'])){
          $year4[0]['basicsalary'] = $year4[0]['basicsalary'];
        }else{
          $year4[0]['basicsalary'] = '';
        }$year5 = $this->Jobpost_Model->getSalary($jobList['id'],'8');
        if(!empty($year5[0]['basicsalary'])){
          $year5[0]['basicsalary'] = $year5[0]['basicsalary'];
        }else{
          $year5[0]['basicsalary'] = '';
        }$year6 = $this->Jobpost_Model->getSalary($jobList['id'],'9');
        if(!empty($year6[0]['basicsalary'])){
          $year6[0]['basicsalary'] = $year6[0]['basicsalary'];
        }else{
          $year6[0]['basicsalary'] = '';
        }$year7 = $this->Jobpost_Model->getSalary($jobList['id'],'10');
        if(!empty($year7[0]['basicsalary'])){
          $year7[0]['basicsalary'] = $year7[0]['basicsalary'];
        }else{
          $year7[0]['basicsalary'] = '';
        }
			  $toppics = $this->Jobpost_Model->jobs_topicks($jobList['id']);
			  if($toppics){if(in_array(2, $toppics)){
				  $free_food = "Yes";
			  }else{
				  $free_food= 'No';
			  }if(in_array(3, $toppics)){
				  $hmo = "Yes";
			  }else{
				  $hmo= 'No';
			  }if(in_array(4, $toppics)){
				  $hmo_d = "Yes";
			  }else{
				  $hmo_d= 'No';
			  }if(in_array(5, $toppics)){
				  $day_shift = "Yes";
			  }else{
				  $day_shift= 'No';
			  }if(in_array(6, $toppics)){
				  $month_pay = "Yes";
			  }else{
				  $month_pay= 'No';
			  }}else{
			  	$free_food= 'No';
			  	$hmo= 'No';
			  	$hmo_d= 'No';
			  	$day_shift= 'No';
			  	$month_pay= 'No';
			  }
			  $medical = $this->Jobpost_Model->jobs_medical($jobList['id']);
			  if($medical){if(in_array(1, $medical)){
				  $free_hmo = "Yes";
			  }else{
				  $free_hmo= 'No';
			  }if(in_array(2, $medical)){
				  $critical = "Yes";
			  }else{
				  $critical= 'No';
			  }if(in_array(3, $medical)){
				  $life = "Yes";
			  }else{
				  $life= 'No';
			  }if(in_array(4, $medical)){
				  $maternity = "Yes";
			  }else{
				  $maternity= 'No';
			  }if(in_array(5, $medical)){
				  $medicine = "Yes";
			  }else{
				  $medicine= 'No';
			  }}else{
			  	$free_hmo="No";
			  	$critical= 'No';
			  	$life= 'No';
			  	$maternity= 'No';
			  	$medicine= 'No';
			  }
			  $allowances = $this->Jobpost_Model->jobs_allowances($jobList['id']);
			  if($allowances){if(in_array(1, $allowances)){
				  $cell_allow = "Yes";
			  }else{
				  $cell_allow= 'No';
			  }if(in_array(2, $allowances)){
				  $free_parking = "Yes";
			  }else{
				  $free_parking= 'No';
			  }if(in_array(3, $allowances)){
				  $free_shuttle = "Yes";
			  }else{
				  $free_shuttle= 'No';
			  }if(in_array(4, $allowances)){
				  $annual = "Yes";
			  }else{
				  $annual= 'No';
			  }if(in_array(5, $allowances)){
				  $retire = "Yes";
			  }else{
				  $retire= 'No';
			  }if(in_array(6, $allowances)){
				  $transport = "Yes";
			  }else{
				  $transport= 'No';
			  }if(in_array(7, $allowances)){
				  $monthly = "Yes";
			  }else{
				  $monthly= 'No';
			  }}else{
			  	$cell_allow= 'No';
			  	$free_parking= 'No';
			  	$free_shuttle= 'No';
			  	 $annual= 'No';
			  	 $retire= 'No';
			  	 $transport= 'No';
			  	 $monthly= 'No';
			  }

			  $leaves = $this->Jobpost_Model->jobs_leaves($jobList['id']);
			  if($leaves){if(in_array(41, $leaves)){
				  $weekend = "Yes";
			  }else{
				  $weekend= 'No';
			  }if(in_array(42, $leaves)){
				  $holiday = "Yes";
			  }else{
				  $holiday= 'No';
			  }}else{
			  	$weekend= 'No';
			  	$holiday= 'No';
			  }
			  $shifts = $this->Jobpost_Model->jobs_workshifts_single($jobList['id']);
			  if($shifts){if($shifts[0]['workshift_id']=='1'){
				  $mid = "Yes";
			  }else{
				  $mid= 'No';
			  }if($shifts[0]['workshift_id']=='2'){
				  $night = "Yes";
			  }else{
				  $night= 'No';
			  }if($shifts[0]['workshift_id']=='3'){
				  $shift24 = "Yes";
			  }else{
				  $shift24= 'No';
			  }}else{
			  	$mid= 'No';
			  	$night= 'No';
			  	$shift24= 'No';
			  }
			  
              if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$tenure[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$minimum[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$less[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$month[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year2[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year3[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year4[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year5[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year6[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year7[0]['basicsalary'].'</td>
              
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.$jobList['joining_bonus'].'</td>
              <td style="width:70px;">'.$free_food.'</td>
              <td style="width:70px;">'.$hmo.'</td>
              <td style="width:70px;">'.$hmo_d.'</td>
              <td style="width:70px;">'.$day_shift.'</td>
              <td style="width:70px;">'.$month_pay.'</td>
              <td style="width:70px;">'.$free_hmo.'</td>
              <td style="width:70px;">'	.$critical.'</td>
              <td style="width:70px;">'.$life.'</td>
              <td style="width:70px;">'.$maternity.'</td>
              <td style="width:70px;">'.$medicine.'</td>
              <td style="width:70px;">'.$cell_allow.'</td>
              <td style="width:70px;">'.$free_parking.'</td>
			  <td style="width:70px;">'.$free_shuttle.'</td>
			  <td style="width:70px;">'.$annual.'</td>
              <td style="width:70px;">'.$retire.'</td>
			  <td style="width:70px;">'.$transport.'</td>
              <td style="width:70px;">'.$monthly.'</td>
			  <td style="width:70px;">'.$weekend.'</td>
			  <td style="width:70px;">'.$holiday.'</td>
			  <td style="width:70px;">'.$mid.'</td>
			  <td style="width:70px;">'.$night.'</td>
			  <td style="width:70px;">'.$shift24.'</td>
           </tr>';
           
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
    
    }
	
	public function getjobDatabyStatusmonth(){
         //$status = $this->input->post('status');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus2($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">All Tenure</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Minimum Experience</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Less than 6months</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6mos to 1Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">1Year to 2Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">2Year to 3Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">3Year to 4Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">4Year to 5Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">5Year to 6Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6Year to 7Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">7 Year & Up</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Joining Bonus</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Food</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day Shift</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">14th Month Pay</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Critical Illness Benefits</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Life Insurance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Maternity Assistance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Medicine Reimbursement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Cellphone Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Parking</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Shuttle</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Annual Performance Bonus</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Retirement Benefits</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Transport Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Monthly Performance Incentive</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Weekends Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Holidays Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Mid Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Night Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">24/7</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          
          foreach($data as $jobList) {
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
               if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
          $companyname = $compdetail[0]['cname'];
        }else{
          $companyname = $jobList['cname'];
        }
        if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                $jobList['fname'] = $jobdetail[0]['fname'];
                $jobList['lname'] = $jobdetail[0]['lname'];
              }
			  $tenure = $this->Jobpost_Model->getSalary($jobList['id'],'0');
        if(!empty($tenure[0]['basicsalary'])){
          $tenure[0]['basicsalary'] = $tenure[0]['basicsalary'];
        }else{
          $tenure[0]['basicsalary'] = '';
        }
        $minimum = $this->Jobpost_Model->getSalary($jobList['id'],'1');
        if(!empty($minimum[0]['basicsalary'])){
          $minimum[0]['basicsalary'] = $minimum[0]['basicsalary'];
        }else{
          $minimum[0]['basicsalary'] = '';
        }
        $less = $this->Jobpost_Model->getSalary($jobList['id'],'2');
        if(!empty($less[0]['basicsalary'])){
          $less[0]['basicsalary'] = $less[0]['basicsalary'];
        }else{
          $less[0]['basicsalary'] = '';
        }
        $month = $this->Jobpost_Model->getSalary($jobList['id'],'3');
        if(!empty($month[0]['basicsalary'])){
          $month[0]['basicsalary'] = $month[0]['basicsalary'];
        }else{
          $month[0]['basicsalary'] = '';
        }
        $year = $this->Jobpost_Model->getSalary($jobList['id'],'4');
        if(!empty($year[0]['basicsalary'])){
          $year[0]['basicsalary'] = $year[0]['basicsalary'];
        }else{
          $year[0]['basicsalary'] = '';
        }
        $year2 = $this->Jobpost_Model->getSalary($jobList['id'],'5');
        if(!empty($year2[0]['basicsalary'])){
          $year2[0]['basicsalary'] = $year2[0]['basicsalary'];
        }else{
          $year2[0]['basicsalary'] = '';
        }
        $year3 = $this->Jobpost_Model->getSalary($jobList['id'],'6');
        if(!empty($year3[0]['basicsalary'])){
          $year3[0]['basicsalary'] = $year3[0]['basicsalary'];
        }else{
          $year3[0]['basicsalary'] = '';
        }$year4 = $this->Jobpost_Model->getSalary($jobList['id'],'7');
        if(!empty($year4[0]['basicsalary'])){
          $year4[0]['basicsalary'] = $year4[0]['basicsalary'];
        }else{
          $year4[0]['basicsalary'] = '';
        }$year5 = $this->Jobpost_Model->getSalary($jobList['id'],'8');
        if(!empty($year5[0]['basicsalary'])){
          $year5[0]['basicsalary'] = $year5[0]['basicsalary'];
        }else{
          $year5[0]['basicsalary'] = '';
        }$year6 = $this->Jobpost_Model->getSalary($jobList['id'],'9');
        if(!empty($year6[0]['basicsalary'])){
          $year6[0]['basicsalary'] = $year6[0]['basicsalary'];
        }else{
          $year6[0]['basicsalary'] = '';
        }$year7 = $this->Jobpost_Model->getSalary($jobList['id'],'10');
        if(!empty($year7[0]['basicsalary'])){
          $year7[0]['basicsalary'] = $year7[0]['basicsalary'];
        }else{
          $year7[0]['basicsalary'] = '';
        }
			  $toppics = $this->Jobpost_Model->jobs_topicks($jobList['id']);
        //echo $this->db->last_query();
        //print_r($toppics);
			  if($toppics){if(in_array(2, $toppics)){
				  $free_food = "Yes";
			  }else{
				  $free_food= 'No';
			  }if(in_array(3, $toppics)){
				  $hmo = "Yes";
			  }else{
				  $hmo= 'No';
			  }if(in_array(4, $toppics)){
				  $hmo_d = "Yes";
			  }else{
				  $hmo_d= 'No';
			  }if(in_array(5, $toppics)){
				  $day_shift = "Yes";
			  }else{
				  $day_shift= 'No';
			  }if(in_array(6, $toppics)){
				  $month_pay = "Yes";
			  }else{
				  $month_pay= 'No';
			  }}else{
			  	$free_food= 'No';
			  	$hmo= 'No';
			  	$day_shift= 'No';
			  	$month_pay= 'No';
			  }
			  $medical = $this->Jobpost_Model->jobs_medical($jobList['id']);
			  if($medical){if(in_array(1, $medical)){
				  $free_hmo = "Yes";
			  }else{
				  $free_hmo= 'No';
			  }if(in_array(2, $medical)){
				  $critical = "Yes";
			  }else{
				  $critical= 'No';
			  }if(in_array(3, $medical)){
				  $life = "Yes";
			  }else{
				  $life= 'No';
			  }if(in_array(4, $medical)){
				  $maternity = "Yes";
			  }else{
				  $maternity= 'No';
			  }if(in_array(5, $medical)){
				  $medicine = "Yes";
			  }else{
				  $medicine= 'No';
			  }}else{
			  	$free_hmo= 'No';
			  	$life= 'No';
			  	$critical= 'No';
			  	$maternity= 'No';
			  	$medicine= 'No';
			  }
			  $allowances = $this->Jobpost_Model->jobs_allowances($jobList['id']);
			  if($allowances){if(in_array(1, $allowances)){
				  $cell_allow = "Yes";
			  }else{
				  $cell_allow= 'No';
			  }if(in_array(2, $allowances)){
				  $free_parking = "Yes";
			  }else{
				  $free_parking= 'No';
			  }if(in_array(3, $allowances)){
				  $free_shuttle = "Yes";
			  }else{
				  $free_shuttle= 'No';
			  }if(in_array(4, $allowances)){
				  $annual = "Yes";
			  }else{
				  $annual= 'No';
			  }if(in_array(5, $allowances)){
				  $retire = "Yes";
			  }else{
				  $retire= 'No';
			  }if(in_array(6, $allowances)){
				  $transport = "Yes";
			  }else{
				  $transport= 'No';
			  }if(in_array(7, $allowances)){
				  $monthly = "Yes";
			  }else{
				  $monthly= 'No';
			  }}else{
			  	$cell_allow= 'No';
			  	$free_parking= 'No';
			  	$free_shuttle= 'No';
			  	$annual= 'No';
			  	$retire= 'No';
			  	$transport= 'No';
			  	$monthly= 'No';
			  }
			  
			  $leaves = $this->Jobpost_Model->jobs_leaves($jobList['id']);
			  if($leaves){if(in_array(41, $leaves)){
				  $weekend = "Yes";
			  }else{
				  $weekend= 'No';
			  }if(in_array(42, $leaves)){
				  $holiday = "Yes";
			  }else{
				  $holiday= 'No';
			  }}else{
			  	$weekend= 'No';
			  	 $holiday= 'No';
			  }
			  $shifts = $this->Jobpost_Model->jobs_workshifts_single($jobList['id']);
			  if($shifts){if($shifts[0]['workshift_id']=='1'){
				  $mid = "Yes";
			  }else{
				  $mid= 'No';
			  }if($shifts[0]['workshift_id']=='2'){
				  $night = "Yes";
			  }else{
				  $night= 'No';
			  }if($shifts[0]['workshift_id']=='3'){
				  $shift24 = "Yes";
			  }else{
				  $shift24= 'No';
			  }}else{
			  	$mid= 'No';
			  	$night= 'No';
			  	$shift24= 'No';
			  }
			  
              if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$tenure[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$minimum[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$less[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$month[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year2[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year3[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year4[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year5[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year6[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year7[0]['basicsalary'].'</td>
              
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.$jobList['joining_bonus'].'</td>
              <td style="width:70px;">'.$free_food.'</td>
              <td style="width:70px;">'.$hmo.'</td>
              <td style="width:70px;">'.$hmo_d.'</td>
              <td style="width:70px;">'.$day_shift.'</td>
              <td style="width:70px;">'.$month_pay.'</td>
              <td style="width:70px;">'.$free_hmo.'</td>
              <td style="width:70px;">'	.$critical.'</td>
              <td style="width:70px;">'.$life.'</td>
              <td style="width:70px;">'.$maternity.'</td>
              <td style="width:70px;">'.$medicine.'</td>
              <td style="width:70px;">'.$cell_allow.'</td>
              <td style="width:70px;">'.$free_parking.'</td>
			  <td style="width:70px;">'.$free_shuttle.'</td>
			  <td style="width:70px;">'.$annual.'</td>
              <td style="width:70px;">'.$retire.'</td>
			  <td style="width:70px;">'.$transport.'</td>
              <td style="width:70px;">'.$monthly.'</td>
			  <td style="width:70px;">'.$weekend.'</td>
			  <td style="width:70px;">'.$holiday.'</td>
			  <td style="width:70px;">'.$mid.'</td>
			  <td style="width:70px;">'.$night.'</td>
			  <td style="width:70px;">'.$shift24.'</td>
           </tr>';
           
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
    
    }
	
	public function getjobDatabyStatusyear(){
        // $status = $this->input->post('status');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus2($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">All Tenure</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Minimum Experience</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Less than 6months</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6mos to 1Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">1Year to 2Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">2Year to 3Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">3Year to 4Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">4Year to 5Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">5Year to 6Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6Year to 7Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">7 Year & Up</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Joining Bonus</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Food</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day Shift</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">14th Month Pay</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Critical Illness Benefits</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Life Insurance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Maternity Assistance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Medicine Reimbursement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Cellphone Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Parking</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Shuttle</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Annual Performance Bonus</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Retirement Benefits</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Transport Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Monthly Performance Incentive</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Weekends Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Holidays Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Mid Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Night Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">24/7</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          
          foreach($data as $jobList) {
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
               if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
          $companyname = $compdetail[0]['cname'];
        }else{
          $companyname = $jobList['cname'];
        }
        if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                $jobList['fname'] = $jobdetail[0]['fname'];
                $jobList['lname'] = $jobdetail[0]['lname'];
              }
			  $tenure = $this->Jobpost_Model->getSalary($jobList['id'],'0');
        if(!empty($tenure[0]['basicsalary'])){
          $tenure[0]['basicsalary'] = $tenure[0]['basicsalary'];
        }else{
          $tenure[0]['basicsalary'] = '';
        }
        $minimum = $this->Jobpost_Model->getSalary($jobList['id'],'1');
        if(!empty($minimum[0]['basicsalary'])){
          $minimum[0]['basicsalary'] = $minimum[0]['basicsalary'];
        }else{
          $minimum[0]['basicsalary'] = '';
        }
        $less = $this->Jobpost_Model->getSalary($jobList['id'],'2');
        if(!empty($less[0]['basicsalary'])){
          $less[0]['basicsalary'] = $less[0]['basicsalary'];
        }else{
          $less[0]['basicsalary'] = '';
        }
        $month = $this->Jobpost_Model->getSalary($jobList['id'],'3');
        if(!empty($month[0]['basicsalary'])){
          $month[0]['basicsalary'] = $month[0]['basicsalary'];
        }else{
          $month[0]['basicsalary'] = '';
        }
        $year = $this->Jobpost_Model->getSalary($jobList['id'],'4');
        if(!empty($year[0]['basicsalary'])){
          $year[0]['basicsalary'] = $year[0]['basicsalary'];
        }else{
          $year[0]['basicsalary'] = '';
        }
        $year2 = $this->Jobpost_Model->getSalary($jobList['id'],'5');
        if(!empty($year2[0]['basicsalary'])){
          $year2[0]['basicsalary'] = $year2[0]['basicsalary'];
        }else{
          $year2[0]['basicsalary'] = '';
        }
        $year3 = $this->Jobpost_Model->getSalary($jobList['id'],'6');
        if(!empty($year3[0]['basicsalary'])){
          $year3[0]['basicsalary'] = $year3[0]['basicsalary'];
        }else{
          $year3[0]['basicsalary'] = '';
        }$year4 = $this->Jobpost_Model->getSalary($jobList['id'],'7');
        if(!empty($year4[0]['basicsalary'])){
          $year4[0]['basicsalary'] = $year4[0]['basicsalary'];
        }else{
          $year4[0]['basicsalary'] = '';
        }$year5 = $this->Jobpost_Model->getSalary($jobList['id'],'8');
        if(!empty($year5[0]['basicsalary'])){
          $year5[0]['basicsalary'] = $year5[0]['basicsalary'];
        }else{
          $year5[0]['basicsalary'] = '';
        }$year6 = $this->Jobpost_Model->getSalary($jobList['id'],'9');
        if(!empty($year6[0]['basicsalary'])){
          $year6[0]['basicsalary'] = $year6[0]['basicsalary'];
        }else{
          $year6[0]['basicsalary'] = '';
        }$year7 = $this->Jobpost_Model->getSalary($jobList['id'],'10');
        if(!empty($year7[0]['basicsalary'])){
          $year7[0]['basicsalary'] = $year7[0]['basicsalary'];
        }else{
          $year7[0]['basicsalary'] = '';
        }
			  $toppics = $this->Jobpost_Model->jobs_topicks($jobList['id']);
			  if($toppics){if(in_array(2, $toppics)){
				  $free_food = "Yes";
			  }else{
				  $free_food= 'No';
			  }if(in_array(3, $toppics)){
				  $hmo = "Yes";
			  }else{
				  $hmo= 'No';
			  }if(in_array(4, $toppics)){
				  $hmo_d = "Yes";
			  }else{
				  $hmo_d= 'No';
			  }if(in_array(5, $toppics)){
				  $day_shift = "Yes";
			  }else{
				  $day_shift= 'No';
			  }if(in_array(6, $toppics)){
				  $month_pay = "Yes";
			  }else{
				  $month_pay= 'No';
			  }}else{
			  	$free_food= 'No';
			  	$hmo= 'No';
			  	$day_shift= 'No';
			  	$month_pay= 'No';
			  }
			  $medical = $this->Jobpost_Model->jobs_medical($jobList['id']);
			  if($medical){if(in_array(1, $medical)){
				  $free_hmo = "Yes";
			  }else{
				  $free_hmo= 'No';
			  }if(in_array(2, $medical)){
				  $critical = "Yes";
			  }else{
				  $critical= 'No';
			  }if(in_array(3, $medical)){
				  $life = "Yes";
			  }else{
				  $life= 'No';
			  }if(in_array(4, $medical)){
				  $maternity = "Yes";
			  }else{
				  $maternity= 'No';
			  }if(in_array(5, $medical)){
				  $medicine = "Yes";
			  }else{
				  $medicine= 'No';
			  }}else{
			  	$free_hmo= 'No';
			  	$critical= 'No';
			  	$life= 'No';
			  	$maternity= 'No';
			  	$medicine= 'No';
			  }
			  $allowances = $this->Jobpost_Model->jobs_allowances($jobList['id']);
			  if($allowances){if(in_array(1, $allowances)){
				  $cell_allow = "Yes";
			  }else{
				  $cell_allow= 'No';
			  }if(in_array(2, $allowances)){
				  $free_parking = "Yes";
			  }else{
				  $free_parking= 'No';
			  }if(in_array(3, $allowances)){
				  $free_shuttle = "Yes";
			  }else{
				  $free_shuttle= 'No';
			  }if(in_array(4, $allowances)){
				  $annual = "Yes";
			  }else{
				  $annual= 'No';
			  }if(in_array(5, $allowances)){
				  $retire = "Yes";
			  }else{
				  $retire= 'No';
			  }if(in_array(6, $allowances)){
				  $transport = "Yes";
			  }else{
				  $transport= 'No';
			  }if(in_array(7, $allowances)){
				  $monthly = "Yes";
			  }else{
				  $monthly= 'No';
			  }}else{
			  	 $cell_allow= 'No';
			  	 $free_parking= 'No';
			  	 $free_shuttle= 'No';
			  	 $annual= 'No';
			  	  $transport= 'No';
			  	  $monthly= 'No';
			  	  $retire= 'No';
			  }
			  
			  $leaves = $this->Jobpost_Model->jobs_leaves($jobList['id']);
			  if($leaves){if(in_array(41, $leaves)){
				  $weekend = "Yes";
			  }else{
				  $weekend= 'No';
			  }if(in_array(42, $leaves)){
				  $holiday = "Yes";
			  }else{
				  $holiday= 'No';
			  }}else{
			  	 $weekend= 'No';
			  	 $holiday= 'No';
			  }
			  $shifts = $this->Jobpost_Model->jobs_workshifts_single($jobList['id']);
			  if($shifts){if($shifts[0]['workshift_id']=='1'){
				  $mid = "Yes";
			  }else{
				  $mid= 'No';
			  }if($shifts[0]['workshift_id']=='2'){
				  $night = "Yes";
			  }else{
				  $night= 'No';
			  }if($shifts[0]['workshift_id']=='3'){
				  $shift24 = "Yes";
			  }else{
				  $shift24= 'No';
			  }}else{
			  	$mid= 'No';
			  	$night= 'No';
			  	$shift24= 'No';
			  }
			  
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$tenure[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$minimum[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$less[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$month[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year2[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year3[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year4[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year5[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year6[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year7[0]['basicsalary'].'</td>
              
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.$jobList['joining_bonus'].'</td>
              <td style="width:70px;">'.$free_food.'</td>
              <td style="width:70px;">'.$hmo.'</td>
              <td style="width:70px;">'.$hmo_d.'</td>
              <td style="width:70px;">'.$day_shift.'</td>
              <td style="width:70px;">'.$month_pay.'</td>
              <td style="width:70px;">'.$free_hmo.'</td>
              <td style="width:70px;">'	.$critical.'</td>
              <td style="width:70px;">'.$life.'</td>
              <td style="width:70px;">'.$maternity.'</td>
              <td style="width:70px;">'.$medicine.'</td>
              <td style="width:70px;">'.$cell_allow.'</td>
              <td style="width:70px;">'.$free_parking.'</td>
			  <td style="width:70px;">'.$free_shuttle.'</td>
			  <td style="width:70px;">'.$annual.'</td>
              <td style="width:70px;">'.$retire.'</td>
			  <td style="width:70px;">'.$transport.'</td>
              <td style="width:70px;">'.$monthly.'</td>
			  <td style="width:70px;">'.$weekend.'</td>
			  <td style="width:70px;">'.$holiday.'</td>
			  <td style="width:70px;">'.$mid.'</td>
			  <td style="width:70px;">'.$night.'</td>
			  <td style="width:70px;">'.$shift24.'</td>
           </tr>';
           
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
    
    }
	
	public function getjobDatabyStatusDate(){
         //$status = $this->input->post('status');
		 $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus2($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus2($date,$userSession['id']);
        //echo $this->db->last_query();die;
        if(!empty($data)){
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">All Tenure</span>
             </div>
          </th>

           <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Minimum Experience</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Less than 6months</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6mos to 1Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">1Year to 2Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">2Year to 3Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">3Year to 4Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">4Year to 5Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">5Year to 6Year</span>
             </div>
          </th><th class="secondary-text">
             <div class="table-header">
                <span class="column-title">6Year to 7Year</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">7 Year & Up</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Joining Bonus</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Food</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day 1 HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Day Shift</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">14th Month Pay</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free HMO for Dependent</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Critical Illness Benefits</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Life Insurance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Maternity Assistance</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Medicine Reimbursement</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Cellphone Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Parking</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Free Shuttle</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Annual Performance Bonus</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Retirement Benefits</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Transport Allowance</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Monthly Performance Incentive</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Weekends Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Holidays Off</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Mid Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Night Shift</span>
             </div>
          </th>
		  <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">24/7</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
          
          foreach($data as $jobList) {
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
          $companyname = $compdetail[0]['cname'];
        }else{
          $companyname = $jobList['cname'];
        }
        if(!empty($jobList['subrecruiter_id'])){
                $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                $jobList['fname'] = $jobdetail[0]['fname'];
                $jobList['lname'] = $jobdetail[0]['lname'];
              }
			  $tenure = $this->Jobpost_Model->getSalary($jobList['id'],'0');
        if(!empty($tenure[0]['basicsalary'])){
          $tenure[0]['basicsalary'] = $tenure[0]['basicsalary'];
        }else{
          $tenure[0]['basicsalary'] = '';
        }
        $minimum = $this->Jobpost_Model->getSalary($jobList['id'],'1');
        if(!empty($minimum[0]['basicsalary'])){
          $minimum[0]['basicsalary'] = $minimum[0]['basicsalary'];
        }else{
          $minimum[0]['basicsalary'] = '';
        }
        $less = $this->Jobpost_Model->getSalary($jobList['id'],'2');
        if(!empty($less[0]['basicsalary'])){
          $less[0]['basicsalary'] = $less[0]['basicsalary'];
        }else{
          $less[0]['basicsalary'] = '';
        }
        $month = $this->Jobpost_Model->getSalary($jobList['id'],'3');
        if(!empty($month[0]['basicsalary'])){
          $month[0]['basicsalary'] = $month[0]['basicsalary'];
        }else{
          $month[0]['basicsalary'] = '';
        }
        $year = $this->Jobpost_Model->getSalary($jobList['id'],'4');
        if(!empty($year[0]['basicsalary'])){
          $year[0]['basicsalary'] = $year[0]['basicsalary'];
        }else{
          $year[0]['basicsalary'] = '';
        }
        $year2 = $this->Jobpost_Model->getSalary($jobList['id'],'5');
        if(!empty($year2[0]['basicsalary'])){
          $year2[0]['basicsalary'] = $year2[0]['basicsalary'];
        }else{
          $year2[0]['basicsalary'] = '';
        }
        $year3 = $this->Jobpost_Model->getSalary($jobList['id'],'6');
        if(!empty($year3[0]['basicsalary'])){
          $year3[0]['basicsalary'] = $year3[0]['basicsalary'];
        }else{
          $year3[0]['basicsalary'] = '';
        }$year4 = $this->Jobpost_Model->getSalary($jobList['id'],'7');
        if(!empty($year4[0]['basicsalary'])){
          $year4[0]['basicsalary'] = $year4[0]['basicsalary'];
        }else{
          $year4[0]['basicsalary'] = '';
        }$year5 = $this->Jobpost_Model->getSalary($jobList['id'],'8');
        if(!empty($year5[0]['basicsalary'])){
          $year5[0]['basicsalary'] = $year5[0]['basicsalary'];
        }else{
          $year5[0]['basicsalary'] = '';
        }$year6 = $this->Jobpost_Model->getSalary($jobList['id'],'9');
        if(!empty($year6[0]['basicsalary'])){
          $year6[0]['basicsalary'] = $year6[0]['basicsalary'];
        }else{
          $year6[0]['basicsalary'] = '';
        }$year7 = $this->Jobpost_Model->getSalary($jobList['id'],'10');
        if(!empty($year7[0]['basicsalary'])){
          $year7[0]['basicsalary'] = $year7[0]['basicsalary'];
        }else{
          $year7[0]['basicsalary'] = '';
        }
			  $toppics = $this->Jobpost_Model->jobs_topicks($jobList['id']);
			  if($toppics){if(in_array(2, $toppics)){
				  $free_food = "Yes";
			  }else{
				  $free_food= 'No';
			  }if(in_array(3, $toppics)){
				  $hmo = "Yes";
			  }else{
				  $hmo= 'No';
			  }if(in_array(4, $toppics)){
				  $hmo_d = "Yes";
			  }else{
				  $hmo_d= 'No';
			  }if(in_array(5, $toppics)){
				  $day_shift = "Yes";
			  }else{
				  $day_shift= 'No';
			  }if(in_array(6, $toppics)){
				  $month_pay = "Yes";
			  }else{
				  $month_pay= 'No';
			  }}else{
			  	$free_food= 'No';
			  	$hmo= 'No';
			  	$day_shift= 'No';
			  	$month_pay= 'No';
			  }
			  
			  $allowances = $this->Jobpost_Model->jobs_allowances($jobList['id']);
			  if($allowances){if(in_array(1, $allowances)){
				  $cell_allow = "Yes";
			  }else{
				  $cell_allow= 'No';
			  }if(in_array(2, $allowances)){
				  $free_parking = "Yes";
			  }else{
				  $free_parking= 'No';
			  }if(in_array(3, $allowances)){
				  $free_shuttle = "Yes";
			  }else{
				  $free_shuttle= 'No';
			  }if(in_array(4, $allowances)){
				  $annual = "Yes";
			  }else{
				  $annual= 'No';
			  }if(in_array(5, $allowances)){
				  $retire = "Yes";
			  }else{
				  $retire= 'No';
			  }if(in_array(6, $allowances)){
				  $transport = "Yes";
			  }else{
				  $transport= 'No';
			  }if(in_array(7, $allowances)){
				  $monthly = "Yes";
			  }else{
				  $monthly= 'No';
			  }}else{
			  	$cell_allow= 'No';
			  	$free_parking= 'No';
			  	$annual= 'No';
			  	$transport= 'No';
			  	$retire= 'No';
			  	$monthly= 'No';
			  	$free_shuttle= 'No';
			  }
			  $medical = $this->Jobpost_Model->jobs_medical($jobList['id']);
			  if($medical){if(in_array(1, $medical)){
				  $free_hmo = "Yes";
			  }else{
				  $free_hmo= 'No';
			  }if(in_array(2, $medical)){
				  $critical = "Yes";
			  }else{
				  $critical= 'No';
			  }if(in_array(3, $medical)){
				  $life = "Yes";
			  }else{
				  $life= 'No';
			  }if(in_array(4, $medical)){
				  $maternity = "Yes";
			  }else{
				  $maternity= 'No';
			  }if(in_array(5, $medical)){
				  $medicine = "Yes";
			  }else{
				  $medicine= 'No';
			  }}else{
			  	$free_hmo= 'No';
			  	$critical= 'No';
			  	$life= 'No';
			  	$maternity= 'No';
			  	$medicine= 'No';
			  }
			  $leaves = $this->Jobpost_Model->jobs_leaves($jobList['id']);
			  if($leaves){if(in_array(41, $leaves)){
				  $weekend = "Yes";
			  }else{
				  $weekend= 'No';
			  }if(in_array(42, $leaves)){
				  $holiday = "Yes";
			  }else{
				  $holiday= 'No';
			  }}else{
			  	$weekend= 'No';
			  	$holiday= 'No';
			  }
			  $shifts = $this->Jobpost_Model->jobs_workshifts_single($jobList['id']);
			  if($shifts){if($shifts[0]['workshift_id']=='1'){
				  $mid = "Yes";
			  }else{
				  $mid= 'No';
			  }if($shifts[0]['workshift_id']=='2'){
				  $night = "Yes";
			  }else{
				  $night= 'No';
			  }if($shifts[0]['workshift_id']=='3'){
				  $shift24 = "Yes";
			  }else{
				  $shift24= 'No';
			  }}else{
			  	$mid= 'No';
			  	$night= 'No';
			  	$shift24= 'No';
			  }
			  
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date &&   date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              <td style="width:70px;">'.date('Y-m-d', strtotime($jobList['created_at'])).'</td>
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$tenure[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$minimum[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$less[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$month[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year2[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year3[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year4[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year5[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year6[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$year7[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.$jobList['joining_bonus'].'</td>
              <td style="width:70px;">'.$free_food.'</td>
              <td style="width:70px;">'.$hmo.'</td>
              <td style="width:70px;">'.$hmo_d.'</td>
              <td style="width:70px;">'.$day_shift.'</td>
              <td style="width:70px;">'.$month_pay.'</td>
              <td style="width:70px;">'.$free_hmo.'</td>
              <td style="width:70px;">'	.$critical.'</td>
              <td style="width:70px;">'.$life.'</td>
              <td style="width:70px;">'.$maternity.'</td>
              <td style="width:70px;">'.$medicine.'</td>
              <td style="width:70px;">'.$cell_allow.'</td>
              <td style="width:70px;">'.$free_parking.'</td>
			  <td style="width:70px;">'.$free_shuttle.'</td>
			  <td style="width:70px;">'.$annual.'</td>
              <td style="width:70px;">'.$retire.'</td>
			  <td style="width:70px;">'.$transport.'</td>
              <td style="width:70px;">'.$monthly.'</td>
			  <td style="width:70px;">'.$weekend.'</td>
			  <td style="width:70px;">'.$holiday.'</td>
			  <td style="width:70px;">'.$mid.'</td>
			  <td style="width:70px;">'.$night.'</td>
			  <td style="width:70px;">'.$shift24.'</td>
           </tr>';
           
             }
          
          }
          
    $output.= '</tbody>';
        }else{
            $output='<tbody>
            <td>No Data Found<td>
            </tbody>';
        }
        
    echo $output;
    
    }

	public function getcandidatebyStatus() {
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $status = $this->input->post('status');
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus($status,$date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        }
        
            $output = '<thead>
               <tr>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Co.Name</span>
                     </div>
                  </th>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Site</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Id</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Posted By</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Title</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Category</span>
                     </div>
                  </th>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Sub-Category</span>
                     </div>
                  </th>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Candidate ID</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Candidate Name</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Candidate Phone</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Salary Offer</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Total Guaranteed Allowance</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Interview Date</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Interview Time</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Latest Status</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Last Update</span>
                     </div>
                  </th>


                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Hired Date</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Candidate Last Managed By</span>
                     </div>
                  </th>
                  
               </tr>
            </thead>
            <tbody>';
       
          if(!empty($data)) {
          foreach($data as $jobList) {

            if($jobList['status'] != 3) {

              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
			        $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
      			  $expmonth = $jobList['exp_month'];
      				$expyear = $jobList['exp_year'];
              if(!empty($subrecruiter_id)){
                $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
                $companyname = $compdetail[0]['cname'];
              }else{
                $companyname = $jobList['cname'];
              }
              if(!empty($jobList['subrecruiter_id'])){
                      $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                      $jobList['fname'] = $jobdetail[0]['fname']??'';
                      $jobList['lname'] = $jobdetail[0]['lname']??'';
              }
      				if($expyear == 0 && $expmonth == 0) {
                      $expfilter = "1";
                      
                  }else if($expyear == 0 && $expmonth < 6) {
                      $expfilter = "2";
                      
                  } else if($expyear < 1 && $expmonth >= 6) {
                      $expfilter = "3";
                      
                  } else if($expyear < 2 && $expyear >= 1) {
                      $expfilter = "4";
                      
                  } else if($expyear < 3 && $expyear >= 2) {
                      $expfilter = "5";
                      
                  }else if($expyear < 4 && $expyear >= 3) {
                      $expfilter = "6";
                      
                  }else if($expyear < 5 && $expyear >= 4) {
                      $expfilter = "7";
                      
                  }else if($expyear < 6 && $expyear >= 5) {
                      $expfilter = "8";
                      
                  }else if($expyear < 7 && $expyear >= 6) {
                      $expfilter = "9";
                      
                  } else if($expyear >= 7) { 
                      $expfilter = "10";
              }
      				
              $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
      				if($jobList['status']=='1'){
      					$jobList['status'] = 'New Application';
      				}else if($jobList['status']=='2'){
      					$jobList['status']='No Show';
      				}else if($jobList['status']=='3'){
      					$jobList['status']='Fall Out';
      				}else if($jobList['status']=='4'){
      					$jobList['status']='Refer';
      				}else if($jobList['status']=='5'){
      					$jobList['status']='On Going Application';
      				}else if($jobList['status']=='6'){
      					$jobList['status']='Accepted JO';
      				}else{
      					$jobList['status']='Hired';
      				}
              if($jobList['status']=='Hired'){
                $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
              }else{
                $hiredDate = '';
              }
             
              if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ) {
                  $output.=' <tr>
                  <td style="width:40px;">'. $companyname.'</td>
                  <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
                  <td style="width:100px;">'. $jobList['id'].'</td>
                  <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                  <td style="width:70px;">'.$jobList['jobtitle'].'</td>
                  <td style="width:70px;">'.$category[0]['category'].'</td>
                  <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
          			  <td style="width:70px;">'.$jobList['uid'].'</td>
          			  <td style="width:70px;">'.$jobList['name'].'</td>
                  <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
          			  <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
          			  <td style="width:70px;">'.$jobList['allowance'].'</td>
                        <td style="width:70px;">'.$jobList['interviewdate'].'</td>
                        <td style="width:70px;">'.$jobList['interviewtime'].'</td>
                        <td style="width:70px;">'.$jobList['status'].'</td>
          			  <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
          			  <td style="width:70px;">'.$hiredDate.'</td>
          			  <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                  </tr>';
              }
            }
          }
          
          $output.= '</tbody>';

      } else {
           $output .='<tr>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            </tr></tbody>';
      }
        
      echo $output;
    }    
	
	public function getcandidatebyStatusweek() {
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $status = $this->input->post('status');
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus($status,$date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        //echo $this->db->last_query();die;
        
            $output = '<thead>
               <tr>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Co.Name</span>
                     </div>
                  </th>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Site</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Id</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Posted By</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Title</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Category</span>
                     </div>
                  </th>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Sub-Category</span>
                     </div>
                  </th>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Candidate ID</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Candidate Name</span>
                     </div>
                  </th>

                   <div class="table-header">
                        <span class="column-title">Candidate Phone</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Salary Offer</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Total Guaranteed Allowance</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Interview Date</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Interview Time</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Latest Status</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Last Update</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Hired Date</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Candidate Last Managed By</span>
                     </div>
                  </th>
                  
               </tr>
            </thead>
            <tbody>';
       
          if(!empty($data)){
          foreach($data as $jobList) {

            if($jobList['status'] != 3) {

              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
			        $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
      			  $expmonth = $jobList['exp_month'];
      				$expyear = $jobList['exp_year'];
              if(!empty($subrecruiter_id)){
                      $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
                $companyname = $compdetail[0]['cname'];
              }else{
                $companyname = $jobList['cname'];
              }
              if(!empty($jobList['subrecruiter_id'])){
                      $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                      $jobList['fname'] = $jobdetail[0]['fname'];
                      $jobList['lname'] = $jobdetail[0]['lname'];
              }
  				    if($expyear == 0 && $expmonth == 0) {
                  $expfilter = "1";
                  
              }else if($expyear == 0 && $expmonth < 6) {
                  $expfilter = "2";
                  
              } else if($expyear < 1 && $expmonth >= 6) {
                  $expfilter = "3";
                  
              } else if($expyear < 2 && $expyear >= 1) {
                  $expfilter = "4";
                  
              } else if($expyear < 3 && $expyear >= 2) {
                  $expfilter = "5";
                  
              }else if($expyear < 4 && $expyear >= 3) {
                  $expfilter = "6";
                  
              }else if($expyear < 5 && $expyear >= 4) {
                  $expfilter = "7";
                  
              }else if($expyear < 6 && $expyear >= 5) {
                  $expfilter = "8";
                  
              }else if($expyear < 7 && $expyear >= 6) {
                  $expfilter = "9";
                  
              } else if($expyear >= 7) { 
                  $expfilter = "10";
              } 
      				$salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
      				if($jobList['status']=='1'){
      					$jobList['status'] = 'New Application';
      				}else if($jobList['status']=='2'){
      					$jobList['status']='No Show';
      				}else if($jobList['status']=='3'){
      					$jobList['status']='Fall Out';
      				}else if($jobList['status']=='4'){
      					$jobList['status']='Refer';
      				}else if($jobList['status']=='5'){
      					$jobList['status']='On Going Application';
      				}else if($jobList['status']=='6'){
      					$jobList['status']='Accepted JO';
      				}else{
      					$jobList['status']='Hired';
      				}
              if($jobList['status']=='Hired'){
                $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
              }else{
                $hiredDate = '';
              }
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
      			  <td style="width:70px;">'.$jobList['uid'].'</td>
      			  <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
      			  <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
      			  <td style="width:70px;">'.$jobList['allowance'].'</td>
                    <td style="width:70px;">'.$jobList['interviewdate'].'</td>
                    <td style="width:70px;">'.$jobList['interviewtime'].'</td>
                    <td style="width:70px;">'.$jobList['status'].'</td>
      			  <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
      			  <td style="width:70px;">'.$hiredDate.'</td>
      			  <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              </tr>';
           
            }
          }       
        }
          
    $output.= '</tbody>';
        }else{
            $output .='<tr>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            </tr></tbody>';
        }
        
    echo $output;
    }    
	
	public function getcandidatebyStatusmonth() {
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $status = $this->input->post('status');
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus($status,$date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        //echo $this->db->last_query();die;
        
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Time</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
        if(!empty($data)){
          foreach($data as $jobList) {

            if($jobList['status'] != 3) {

              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
			        $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
      			  $expmonth = $jobList['exp_month'];
      				$expyear = $jobList['exp_year'];
              if(!empty($subrecruiter_id)){
                      $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
                    } 

                    if(!empty($subrecruiter_id)){
                $companyname = $compdetail[0]['cname'];
              }else{
                $companyname = $jobList['cname'];
              }
              if(!empty($jobList['subrecruiter_id'])){
                      $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                      $jobList['fname'] = $jobdetail[0]['fname'];
                      $jobList['lname'] = $jobdetail[0]['lname'];
              }
  				    if($expyear == 0 && $expmonth == 0) {
                  $expfilter = "1";
                  
              }else if($expyear == 0 && $expmonth < 6) {
                  $expfilter = "2";
                  
              } else if($expyear < 1 && $expmonth >= 6) {
                  $expfilter = "3";
                  
              } else if($expyear < 2 && $expyear >= 1) {
                  $expfilter = "4";
                  
              } else if($expyear < 3 && $expyear >= 2) {
                  $expfilter = "5";
                  
              }else if($expyear < 4 && $expyear >= 3) {
                  $expfilter = "6";
                  
              }else if($expyear < 5 && $expyear >= 4) {
                  $expfilter = "7";
                  
              }else if($expyear < 6 && $expyear >= 5) {
                  $expfilter = "8";
                  
              }else if($expyear < 7 && $expyear >= 6) {
                  $expfilter = "9";
                  
              } else if($expyear >= 7) { 
                  $expfilter = "10";
                  
              }
              //echo $expfilter;die;
      				$salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
              //echo $this->db->last_query();die;
      				if($jobList['status']=='1'){
      					$jobList['status'] = 'New Application';
      				}else if($jobList['status']=='2'){
      					$jobList['status']='No Show';
      				}else if($jobList['status']=='3'){
      					$jobList['status']='Fall Out';
      				}else if($jobList['status']=='4'){
      					$jobList['status']='Refer';
      				}else if($jobList['status']=='5'){
      					$jobList['status']='On Going Application';
      				}else if($jobList['status']=='6'){
      					$jobList['status']='Accepted JO';
      				}else{
      					$jobList['status']='Hired';
      				}
              if($jobList['status']=='Hired'){
                $hiredDate= date('Y-m-d',strtotime($jobList['updated_at'])) ;
              }else{
                $hiredDate = '';
              }
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
      			  <td style="width:70px;">'.$jobList['uid'].'</td>
      			  <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
      			  <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
      			  <td style="width:70px;">'.$jobList['allowance'].'</td>
                    <td style="width:70px;">'.$jobList['interviewdate'].'</td>
                    <td style="width:70px;">'.$jobList['interviewtime'].'</td>
                    <td style="width:70px;">'.$jobList['status'].'</td>
      			  <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
      			  <td style="width:70px;">'.$hiredDate.'</td>
      			  <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              </tr>';
           
            }
          }
        }
          
    $output.= '</tbody>';
        }else{
            $output .='<tr>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            </tr></tbody>';
        }
        
    echo $output;
    }

	public function getcandidatebyStatusyear() {
        $date =  date('Y-m-d');

        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $status = $this->input->post('status');
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus($status,$date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        }
        
        //echo $this->db->last_query();die;
        
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Time</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
        if(!empty($data)){

          foreach($data as $jobList) {

            if($jobList['status'] != 3) {

              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
			        $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);

              if($subcategory) {

                  $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
                  if(!empty($subrecruiter_id)){
                    $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
                  }  
                  if(!empty($jobList['subrecruiter_id'])){
                    $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                    $jobList['fname'] = $jobdetail[0]['fname'];
                    $jobList['lname'] = $jobdetail[0]['lname'];
                  }
          			  $expmonth = $jobList['exp_month'];
          				$expyear = $jobList['exp_year'];

      				    if($expyear == 0 && $expmonth == 0) {
                      $expfilter = "1";
                      
                  }else if($expyear == 0 && $expmonth < 6) {
                      $expfilter = "2";
                      
                  } else if($expyear < 1 && $expmonth >= 6) {
                      $expfilter = "3";
                      
                  } else if($expyear < 2 && $expyear >= 1) {
                      $expfilter = "4";
                      
                  } else if($expyear < 3 && $expyear >= 2) {
                      $expfilter = "5";
                      
                  }else if($expyear < 4 && $expyear >= 3) {
                      $expfilter = "6";
                      
                  }else if($expyear < 5 && $expyear >= 4) {
                      $expfilter = "7";
                      
                  }else if($expyear < 6 && $expyear >= 5) {
                      $expfilter = "8";
                      
                  }else if($expyear < 7 && $expyear >= 6) {
                      $expfilter = "9";
                      
                  } else if($expyear >= 7) { 
                      $expfilter = "10"; 
                  }

          				$salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
          				if($jobList['status']=='1'){
          					$jobList['status'] = 'New Application';
          				}else if($jobList['status']=='2'){
          					$jobList['status']='No Show';
          				}else if($jobList['status']=='3'){
          					$jobList['status']='Fall Out';
          				}else if($jobList['status']=='4'){
          					$jobList['status']='Refer';
          				}else if($jobList['status']=='5'){
          					$jobList['status']='On Going Application';
          				}else if($jobList['status']=='6'){
          					$jobList['status']='Accepted JO';
          				}else{
          					$jobList['status']='Hired';
          				}
                  if($jobList['status']=='Hired'){
                    $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
                  }else{
                    $hiredDate = '';
                  }
                  if(!empty($subrecruiter_id)){
                    $companyname = $compdetail[0]['cname'];
                  }else{
                    $companyname = $jobList['cname'];
                  }

                 if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
                    $output.=' <tr>
                  <td style="width:40px;">'. $companyname.'</td>
                  <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
                  <td style="width:100px;">'. $jobList['id'].'</td>
                  <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                  
                  <td style="width:70px;">'.$jobList['jobtitle'].'</td>
                  <td style="width:70px;">'.$category[0]['category'].'</td>
                  <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
    			  <td style="width:70px;">'.$jobList['uid'].'</td>
    			  <td style="width:70px;">'.$jobList['name'].'</td>
            <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
    			  <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
    			  <td style="width:70px;">'.$jobList['allowance'].'</td>
                  <td style="width:70px;">'.$jobList['interviewdate'].'</td>
                  <td style="width:70px;">'.$jobList['interviewtime'].'</td>
                  <td style="width:70px;">'.$jobList['status'].'</td>
    			  <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
    			  <td style="width:70px;">'.$hiredDate.'</td>
    			  <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
               </tr>';
               
                 }

            }
           }
          }
          
            $output.= '</tbody>';
        }else{
            $output .='<tr>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            </tr></tbody>';
        }
        
    echo $output;
    }

	public function getcandidatebyStatusDate(){
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $status = $this->input->post('status');
    		$from_date = $this->input->post('from_date');
    		$to_date = $this->input->post('to_date');
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getsubDatabyStatus($status,$date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        }
        //$data = $this->Jobpostrecruiter_Model->getDatabyStatus($status,$date,$userSession['id']);
        //echo $this->db->last_query();die;
        
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Interview Time</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Latest Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Last Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Hired Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Last Managed By</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
        if(!empty($data)){
          foreach($data as $jobList) {

            if($jobList['status'] != 3) {

              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
			        $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
      			  $expmonth = $jobList['exp_month'];
      				$expyear = $jobList['exp_year'];
              if(!empty($subrecruiter_id)){
                      $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
                    } 

              if(!empty($subrecruiter_id)){
                $companyname = $compdetail[0]['cname'];
              }else{
                $companyname = $jobList['cname'];
              }
              if(!empty($jobList['subrecruiter_id'])){
                      $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                      $jobList['fname'] = $jobdetail[0]['fname'];
                      $jobList['lname'] = $jobdetail[0]['lname'];
              }
  				    if($expyear == 0 && $expmonth == 0) {
                  $expfilter = "1";
                  
              }else if($expyear == 0 && $expmonth < 6) {
                  $expfilter = "2";
                  
              } else if($expyear < 1 && $expmonth >= 6) {
                  $expfilter = "3";
                  
              } else if($expyear < 2 && $expyear >= 1) {
                  $expfilter = "4";
                  
              } else if($expyear < 3 && $expyear >= 2) {
                  $expfilter = "5";
                  
              }else if($expyear < 4 && $expyear >= 3) {
                  $expfilter = "6";
                  
              }else if($expyear < 5 && $expyear >= 4) {
                  $expfilter = "7";
                  
              }else if($expyear < 6 && $expyear >= 5) {
                  $expfilter = "8";
                  
              }else if($expyear < 7 && $expyear >= 6) {
                  $expfilter = "9";
                  
              } else if($expyear >= 7) { 
                  $expfilter = "10";
                  
              }
				$salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
				if($jobList['status']=='1'){
					$jobList['status'] = 'New Application';
				}else if($jobList['status']=='2'){
					$jobList['status']='No Show';
				}else if($jobList['status']=='3'){
					$jobList['status']='Fall Out';
				}else if($jobList['status']=='4'){
					$jobList['status']='Refer';
				}else if($jobList['status']=='5'){
					$jobList['status']='On Going Application';
				}else if($jobList['status']=='6'){
					$jobList['status']='Accepted JO';
				}else{
					$jobList['status']='Hired';
				}
        if($jobList['status']=='Hired'){
          $hiredDate= date('Y-m-d',strtotime($jobList['updated_at']));
        }else{
          $hiredDate = '';
        }
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date &&   date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
			  <td style="width:70px;">'.$jobList['uid'].'</td>
			  <td style="width:70px;">'.$jobList['name'].'</td>
        <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
			  <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
			  <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.$jobList['interviewdate'].'</td>
              <td style="width:70px;">'.$jobList['interviewtime'].'</td>
              <td style="width:70px;">'.$jobList['status'].'</td>
			  <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
			  <td style="width:70px;">'.$hiredDate.'</td>
			  <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
           </tr>';
           
             }
            }
          }
          
    $output.= '</tbody>';
        }else{
             $output='<tr>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            </tr></tbody>';
        }
        
    echo $output;
    }	

    public function fetchpending($userSession,$subrecruiter_id){
        $data["pendingData"] = [];
        $pending = $this->Candidate_Model->candidateduenotification_list($userSession,$subrecruiter_id ); 
        if (!empty($pending)) {
            foreach ($pending as $pendings) {
                //echo $pendings['updated_at'];die;
                $enddate =  date('Y-m-d', strtotime($pendings['updated_at']. ' + 3 days'));
                //echo $enddate;die;
                $enddate = strtotime($enddate);
                $updated_date=  strtotime($pendings['updated_at']);
                $now = time();
                $datediff =  $now - $updated_date;

                $daydiff = floor($datediff / (60*60*24));
                //echo $daydiff;die;
                if($daydiff >=3)
                {
                    $data["pendingData"][] =["profilePic"=>$pendings['profilePic'],"user_id"=>$pendings['user_id'], "name"=>$pendings['name'], "email"=>$pendings['email'], 'phone'=>$pendings['phone'], "location"=>$pendings['location'], "status"=>$pendings['status'], "interviewdate" => $pendings['interviewdate'], 'interviewtime' => $pendings['interviewtime'] , "date_day1" => $pendings['date_day1'], 'jobDesc' => $pendings['jobDesc'], "jobtitle"=>$pendings['jobtitle'], "compid"=>$pendings['compid'], "id"=>$pendings['id'], 'user_id'=>$pendings['user_id'],'jobpost_id'=>$pendings['jobpost_id'], 'updated_at'=> $pendings['updated_at'], 'fallout_reason' => $pendings['fallout_reason']];
                }

            }
        }
        return $data["pendingData"];
    }


  public function screening_report() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
        $this->load->view('recruiter/screening_report',$data);
  }


  public function getscreeningbyStatus() {
      $date =  date('Y-m-d');
      $userSession = $this->session->userdata('userSession');
      if(!empty($userSession['label']) && $userSession['label']=='3'){
          $subrecruiter_id = $userSession['id'];
          $userSession['id'] = $userSession['parent_id'];
      }else{
          $userSession['id'] = $userSession['id'];
          $subrecruiter_id=0;
      }
      
      if(!empty($subrecruiter_id)){
        $data = $this->Jobpostrecruiter_Model->getscreeningsubDatabyStatus($date,$subrecruiter_id);
      }else{
        $data = $this->Jobpostrecruiter_Model->getscreeningDatabyStatus($date,$userSession['id']);
      }
      
          $output = '<thead>
             <tr>
                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Co.Name</span>
                   </div>
                </th>
                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Site</span>
                   </div>
                </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Job Id</span>
                   </div>
                </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Job Posted By</span>
                   </div>
                </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Job Title</span>
                   </div>
                </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Job Category</span>
                   </div>
                </th>
                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Job Sub-Category</span>
                   </div>
                </th>
                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Candidate ID</span>
                   </div>
                </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Candidate Name</span>
                   </div>
                </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Candidate Phone</span>
                   </div>
                </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Salary Offer</span>
                   </div>
                </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Total Guaranteed Allowance</span>
                   </div>
                </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Screening Date</span>
                   </div>
                </th>

                <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Screening Status</span>
                     </div>
                  </th>

                <th class="secondary-text">
                   <div class="table-header">
                      <span class="column-title">Action</span>
                   </div>
                </th>
                
             </tr>
          </thead>
          <tbody>';
     
        if(!empty($data)) {
        foreach($data as $jobList) {

          //if($jobList['status'] != 3) {

            $category = $this->Jobpost_Model->categorybyid($jobList['category']);
            $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
            $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
            $expmonth = $jobList['exp_month'];
            $expyear = $jobList['exp_year'];
            if(!empty($subrecruiter_id)){
              $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
            } 

            if(!empty($subrecruiter_id)){
              $companyname = $compdetail[0]['cname'];
            }else{
              $companyname = $jobList['cname'];
            }
            if(!empty($jobList['subrecruiter_id'])){
                    $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                    $jobList['fname'] = $jobdetail[0]['fname']??'';
                    $jobList['lname'] = $jobdetail[0]['lname']??'';
            }
            if($expyear == 0 && $expmonth == 0) {
                    $expfilter = "1";
                    
                }else if($expyear == 0 && $expmonth < 6) {
                    $expfilter = "2";
                    
                } else if($expyear < 1 && $expmonth >= 6) {
                    $expfilter = "3";
                    
                } else if($expyear < 2 && $expyear >= 1) {
                    $expfilter = "4";
                    
                } else if($expyear < 3 && $expyear >= 2) {
                    $expfilter = "5";
                    
                }else if($expyear < 4 && $expyear >= 3) {
                    $expfilter = "6";
                    
                }else if($expyear < 5 && $expyear >= 4) {
                    $expfilter = "7";
                    
                }else if($expyear < 6 && $expyear >= 5) {
                    $expfilter = "8";
                    
                }else if($expyear < 7 && $expyear >= 6) {
                    $expfilter = "9";
                    
                } else if($expyear >= 7) { 
                    $expfilter = "10";
            }
            

            if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

            $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
          
            if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ) {
                $output.=' <tr>
                <td style="width:40px;">'. $companyname.'</td>
                <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
                <td style="width:100px;">'. $jobList['id'].'</td>
                <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                <td style="width:70px;">'.$jobList['jobtitle'].'</td>
                <td style="width:70px;">'.$category[0]['category'].'</td>
                <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
                <td style="width:70px;">'.$jobList['uid'].'</td>
                <td style="width:70px;">'.$jobList['name'].'</td>
                <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
                <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
                <td style="width:70px;">'.$jobList['allowance'].'</td>
                <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
                <td style="width:70px;">'.$screenStatus.'</td>
                <td style="width:70px;"> <select onChange="changeFunction('.$jobList['screenid'].')"> <option value="1">Pending</option> <option value="2">Completed</option> </select> </td>
                </tr>';
            }
          //}
        }
        
        $output.= '</tbody>';

    } else {
         $output .='<tr>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          <td>No Data Found</td>
          </tr></tbody>';
    }
      
    echo $output;
  }


  public function getscreeningbyStatusweek() {

        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $status = $this->input->post('status');
        
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getscreeningsubDatabyStatus($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getscreeningDatabyStatus($date,$userSession['id']);
        }
            $output = '<thead>
               <tr>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Co.Name</span>
                     </div>
                  </th>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Site</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Id</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Posted By</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Title</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Category</span>
                     </div>
                  </th>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Job Sub-Category</span>
                     </div>
                  </th>
                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Candidate ID</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Candidate Name</span>
                     </div>
                  </th>

                  <th>
                   <div class="table-header">
                        <span class="column-title">Candidate Phone</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Salary Offer</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Total Guaranteed Allowance</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Screening Date</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Screening Status</span>
                     </div>
                  </th>

                  <th class="secondary-text">
                     <div class="table-header">
                        <span class="column-title">Action</span>
                     </div>
                  </th>
                  
               </tr>
            </thead>
            <tbody>';
       
          if(!empty($data)){
          foreach($data as $jobList) {

            //if($jobList['status'] != 3) {

              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
              $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              $expmonth = $jobList['exp_month'];
              $expyear = $jobList['exp_year'];
              if(!empty($subrecruiter_id)){
                      $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
              } 

              if(!empty($subrecruiter_id)){
                $companyname = $compdetail[0]['cname'];
              }else{
                $companyname = $jobList['cname'];
              }
              if(!empty($jobList['subrecruiter_id'])){
                      $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                      $jobList['fname'] = $jobdetail[0]['fname'];
                      $jobList['lname'] = $jobdetail[0]['lname'];
              }
              if($expyear == 0 && $expmonth == 0) {
                  $expfilter = "1";
                  
              }else if($expyear == 0 && $expmonth < 6) {
                  $expfilter = "2";
                  
              } else if($expyear < 1 && $expmonth >= 6) {
                  $expfilter = "3";
                  
              } else if($expyear < 2 && $expyear >= 1) {
                  $expfilter = "4";
                  
              } else if($expyear < 3 && $expyear >= 2) {
                  $expfilter = "5";
                  
              }else if($expyear < 4 && $expyear >= 3) {
                  $expfilter = "6";
                  
              }else if($expyear < 5 && $expyear >= 4) {
                  $expfilter = "7";
                  
              }else if($expyear < 6 && $expyear >= 5) {
                  $expfilter = "8";
                  
              }else if($expyear < 7 && $expyear >= 6) {
                  $expfilter = "9";
                  
              } else if($expyear >= 7) { 
                  $expfilter = "10";
              } 

              if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

              $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
              
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
              <td style="width:70px;">'.$jobList['uid'].'</td>
              <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
              <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
              <td style="width:70px;">'.$screenStatus.'</td>
              <td style="width:70px;"> <select onChange="changeFunction('.$jobList['screenid'].')"> <option value="1">Pending</option> <option value="2">Completed</option> </select> </td>
              </tr>';
           
            //}
          }       
        }
          
    $output.= '</tbody>';
        }else{
            $output .='<tr>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            </tr></tbody>';
        }
        
    echo $output;
    }


    public function getscreeningbyStatusmonth() {
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $status = $this->input->post('status');
        
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getscreeningsubDatabyStatus($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getscreeningDatabyStatus($date,$userSession['id']);
        }
        
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Update</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Action</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
        if(!empty($data)){
          foreach($data as $jobList) {

            //if($jobList['status'] != 3) {

              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
              $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              $expmonth = $jobList['exp_month'];
              $expyear = $jobList['exp_year'];
              if(!empty($subrecruiter_id)){
                      $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
                    } 

                    if(!empty($subrecruiter_id)){
                $companyname = $compdetail[0]['cname'];
              }else{
                $companyname = $jobList['cname'];
              }
              if(!empty($jobList['subrecruiter_id'])){
                      $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                      $jobList['fname'] = $jobdetail[0]['fname'];
                      $jobList['lname'] = $jobdetail[0]['lname'];
              }
              if($expyear == 0 && $expmonth == 0) {
                  $expfilter = "1";
                  
              }else if($expyear == 0 && $expmonth < 6) {
                  $expfilter = "2";
                  
              } else if($expyear < 1 && $expmonth >= 6) {
                  $expfilter = "3";
                  
              } else if($expyear < 2 && $expyear >= 1) {
                  $expfilter = "4";
                  
              } else if($expyear < 3 && $expyear >= 2) {
                  $expfilter = "5";
                  
              }else if($expyear < 4 && $expyear >= 3) {
                  $expfilter = "6";
                  
              }else if($expyear < 5 && $expyear >= 4) {
                  $expfilter = "7";
                  
              }else if($expyear < 6 && $expyear >= 5) {
                  $expfilter = "8";
                  
              }else if($expyear < 7 && $expyear >= 6) {
                  $expfilter = "9";
                  
              } else if($expyear >= 7) { 
                  $expfilter = "10";
                  
              }
              
              if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

              $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);

             if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
              <td style="width:70px;">'.$jobList['uid'].'</td>
              <td style="width:70px;">'.$jobList['name'].'</td>
              <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
              <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
              <td style="width:70px;">'.$jobList['allowance'].'</td>
              <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
              <td style="width:70px;">'.$screenStatus.'</td>
              <td style="width:70px;"> <select onChange="changeFunction('.$jobList['screenid'].')"> <option value="1">Pending</option> <option value="2">Completed</option> </select> </td>
              </tr>';
           
            //}
          }
        }
          
    $output.= '</tbody>';
        }else{
            $output .='<tr>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            </tr></tbody>';
        }
        
    echo $output;
    }


    public function getscreeningbyStatusyear() {
        $date =  date('Y-m-d');

        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $status = $this->input->post('status');
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getscreeningsubDatabyStatus($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getscreeningDatabyStatus($date,$userSession['id']);
        }
        
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Action</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
        if(!empty($data)){

          foreach($data as $jobList) {

            //if($jobList['status'] != 3) {

              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
              $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);

              if($subcategory) {

                  $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
                  if(!empty($subrecruiter_id)){
                    $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
                  }  
                  if(!empty($jobList['subrecruiter_id'])){
                    $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                    $jobList['fname'] = $jobdetail[0]['fname'];
                    $jobList['lname'] = $jobdetail[0]['lname'];
                  }
                  $expmonth = $jobList['exp_month'];
                  $expyear = $jobList['exp_year'];

                  if($expyear == 0 && $expmonth == 0) {
                      $expfilter = "1";
                      
                  }else if($expyear == 0 && $expmonth < 6) {
                      $expfilter = "2";
                      
                  } else if($expyear < 1 && $expmonth >= 6) {
                      $expfilter = "3";
                      
                  } else if($expyear < 2 && $expyear >= 1) {
                      $expfilter = "4";
                      
                  } else if($expyear < 3 && $expyear >= 2) {
                      $expfilter = "5";
                      
                  }else if($expyear < 4 && $expyear >= 3) {
                      $expfilter = "6";
                      
                  }else if($expyear < 5 && $expyear >= 4) {
                      $expfilter = "7";
                      
                  }else if($expyear < 6 && $expyear >= 5) {
                      $expfilter = "8";
                      
                  }else if($expyear < 7 && $expyear >= 6) {
                      $expfilter = "9";
                      
                  } else if($expyear >= 7) { 
                      $expfilter = "10"; 
                  }

                  if($jobList['status'] == 1) {
                      $screenStatus = "Pending";
                  } else {
                      $screenStatus = "Completed";
                  }

                  $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);

                  if(!empty($subrecruiter_id)){
                    $companyname = $compdetail[0]['cname'];
                  }else{
                    $companyname = $jobList['cname'];
                  }

                 if( date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){
                    $output.=' <tr>
                  <td style="width:40px;">'. $companyname.'</td>
                  <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
                  <td style="width:100px;">'. $jobList['id'].'</td>
                  <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
                  
                  <td style="width:70px;">'.$jobList['jobtitle'].'</td>
                  <td style="width:70px;">'.$category[0]['category'].'</td>
                  <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
            <td style="width:70px;">'.$jobList['uid'].'</td>
            <td style="width:70px;">'.$jobList['name'].'</td>
            <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
            <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
            <td style="width:70px;">'.$jobList['allowance'].'</td>
            <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
            <td style="width:70px;">'.$screenStatus.'</td>
              <td style="width:70px;"> <select onChange="changeFunction('.$jobList['screenid'].')"> <option value="1">Pending</option> <option value="2">Completed</option> </select> </td>
               </tr>';
               
                 }

            //}
           }
          }
          
            $output.= '</tbody>';
        }else{
            $output .='<tr>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            </tr></tbody>';
        }
        
    echo $output;
    }


      public function getscreeningbyStatusDate(){
        $date =  date('Y-m-d');
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $status = $this->input->post('status');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        //echo $date;die;
        if(!empty($subrecruiter_id)){
          $data = $this->Jobpostrecruiter_Model->getscreeningsubDatabyStatus($date,$subrecruiter_id);
        }else{
          $data = $this->Jobpostrecruiter_Model->getscreeningDatabyStatus($date,$userSession['id']);
        }
        
            $output = '<thead>
       <tr>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Co.Name</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Site</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Id</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Posted By</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Title</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Job Sub-Category</span>
             </div>
          </th>
          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate ID</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Name</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Candidate Phone</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Salary Offer</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Total Guaranteed Allowance</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Date</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Screening Status</span>
             </div>
          </th>

          <th class="secondary-text">
             <div class="table-header">
                <span class="column-title">Action</span>
             </div>
          </th>
          
       </tr>
    </thead>
    <tbody>';
       
        if(!empty($data)){
          foreach($data as $jobList) {

            //if($jobList['status'] != 3) {

              $category = $this->Jobpost_Model->categorybyid($jobList['category']);
              $subcategory = $this->Jobpost_Model->subcategorybyid($jobList['subcategory']);
              $companydetail = $this->Jobpost_Model->company_detail_fetch($jobList['company_id']);
              $expmonth = $jobList['exp_month'];
              $expyear = $jobList['exp_year'];
              if(!empty($subrecruiter_id)){
                      $compdetail = $this->Jobpost_Model->company_detail_fetch($jobList['recruiter_id']);
                    } 

              if(!empty($subrecruiter_id)){
                $companyname = $compdetail[0]['cname'];
              }else{
                $companyname = $jobList['cname'];
              }
              if(!empty($jobList['subrecruiter_id'])){
                      $jobdetail = $this->Jobpost_Model->company_detail_fetch($jobList['subrecruiter_id']);
                      $jobList['fname'] = $jobdetail[0]['fname'];
                      $jobList['lname'] = $jobdetail[0]['lname'];
              }
              if($expyear == 0 && $expmonth == 0) {
                  $expfilter = "1";
                  
              }else if($expyear == 0 && $expmonth < 6) {
                  $expfilter = "2";
                  
              } else if($expyear < 1 && $expmonth >= 6) {
                  $expfilter = "3";
                  
              } else if($expyear < 2 && $expyear >= 1) {
                  $expfilter = "4";
                  
              } else if($expyear < 3 && $expyear >= 2) {
                  $expfilter = "5";
                  
              }else if($expyear < 4 && $expyear >= 3) {
                  $expfilter = "6";
                  
              }else if($expyear < 5 && $expyear >= 4) {
                  $expfilter = "7";
                  
              }else if($expyear < 6 && $expyear >= 5) {
                  $expfilter = "8";
                  
              }else if($expyear < 7 && $expyear >= 6) {
                  $expfilter = "9";
                  
              } else if($expyear >= 7) { 
                  $expfilter = "10";
                  
              }

              if($jobList['status'] == 1) {
                  $screenStatus = "Pending";
              } else {
                  $screenStatus = "Completed";
              }

              $salary = $this->Jobpost_Model->getsalaryexp($jobList['id'],$expfilter);
        
             if( date('Y-m-d', strtotime($jobList['created_at'])) >= $from_date &&   date('Y-m-d', strtotime($jobList['created_at'])) <= $to_date){
                $output.=' <tr>
              <td style="width:40px;">'. $companyname.'</td>
              <td style="width:100px;">'. $companydetail[0]['cname'].'</td>
              <td style="width:100px;">'. $jobList['id'].'</td>
              <td style="width:70px;">'.$jobList['fname'].' '.$jobList['lname'].'</td>
              
              <td style="width:70px;">'.$jobList['jobtitle'].'</td>
              <td style="width:70px;">'.$category[0]['category'].'</td>
              <td style="width:70px;">'.$subcategory[0]['subcategory'].'</td>
            <td style="width:70px;">'.$jobList['uid'].'</td>
            <td style="width:70px;">'.$jobList['name'].'</td>
            <td style="width:70px;" class="pincts"><a href="tel:'.$jobList['country_code'].' '.$jobList['phone'].'" >'.$jobList['country_code'].$jobList['phone'].'</a></td>
            <td style="width:70px;">'.$salary[0]['basicsalary'].'</td>
            <td style="width:70px;">'.$jobList['allowance'].'</td>
            <td style="width:70px;">'.date('Y-m-d',strtotime($jobList['updated_at'])).'</td>
            <td style="width:70px;">'.$screenStatus.'</td>
              <td style="width:70px;"> <select onChange="changeFunction('.$jobList['screenid'].')"> <option value="1">Pending</option> <option value="2">Completed</option> </select> </td>
               </tr>';
           
             }
            //}
          }
          
    $output.= '</tbody>';
        }else{
             $output='<tr>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            <td>No Data Found</td>
            </tr></tbody>';
        }
        
    echo $output;
    }

    public function changescreenstatus() {

        $statusId = $this->input->post('statusid');
        $screening = $this->Jobpostrecruiter_Model->checkScreening($statusId);
        if($screening) {

          if($screening[0]['status'] == 1) {
              $this->Jobpostrecruiter_Model->updateScreening($statusId, 2);
              echo True;
              die;
          } else {
              $this->Jobpostrecruiter_Model->updateScreening($statusId, 1);
              echo True;
              die;
          }
        } else {
             echo False;
             die;
        }

    }


}
?>
