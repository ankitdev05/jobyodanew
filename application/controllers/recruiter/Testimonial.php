<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Testimonial extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Common_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->helper('cookie');
        /*if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }*/
    }
    
    public function testimonial_view() {
        $data['recruitFetch'] = $this->Recruit_Model->testimonial_allbystatus();

        $data['meta_title'] = "Jobyoda Recruiter Panel | Jobyoda Partners Recruiters Near You";
        $data['meta_description'] = "Partner with JobYoDa and find the best candidates that fit your vacant job position. Improve your roster of talents with JobYoDa today!";
        
        $this->load->view('recruiter/testimonial_view', $data);
    }

    public function testimonial_list() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }

        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
          $//subrecruiter_id = $userSession['id'];
          $userSession['id'] = $userSession['id'];
        }else{
          $userSession['id'] = $userSession['id'];
          //$subrecruiter_id = 0;
        }
        
        $data['recruitFetch'] = $this->Recruit_Model->testimonial_list($userSession['id']);
        $this->load->view('recruiter/testimonial_list', $data);
    }

    public function testimonial_add() {
        $this->load->view('recruiter/testimonial_add');
    }

    public function create() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('recruiter/testimonial_add', $data);
        } else {
            $userSession = $this->session->userdata('userSession');
            if(!empty($userSession['label']) && $userSession['label']=='3'){
              //$subrecruiter_id = $userSession['id'];
              $userSession['id'] = $userSession['id'];
            }else{
              $userSession['id'] = $userSession['id'];
            }
            
            $data1 = ['company_id' => $userSession['id'],'name'=>$userData['name'],'cname'=>$userData['cname'], 'description' => $userData['description'] ];
            
            $recruiterInserted = $this->Recruit_Model->testimonial_insert($data1);
            
            
            if($recruiterInserted) {    
                $data1['addsuccess'] = "Testimonial posted successfully";
                redirect("recruiter/testimonial/testimonial_list");
            } else {
                redirect("recruiter/testimonial/testimonial_add");
            }
        }
    }

    public function delete() {
        
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
          $subrecruiter_id = $userSession['id'];
          $userSession['id'] = $userSession['parent_id'];
        }else{
          $userSession['id'] = $userSession['id'];
          $subrecruiter_id = 0;
        }
        
        $userData = $this->input->post();

        $recruiterInserted = $this->Recruit_Model->testimonial_delete($userData['tid']);
        
        //var_dump($recruiterInserted);die;

        if($recruiterInserted) {    
            $data1['addsuccess'] = "Testimonial deleted successfully";
            redirect("recruiter/testimonial/testimonial_list");
        } else {
            redirect("recruiter/testimonial/testimonial_list");
        }
    }

    public function testimonial_edit() {
        $userSession = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
          $subrecruiter_id = $userSession['id'];
          $userSession['id'] = $userSession['parent_id'];
        }else{
          $userSession['id'] = $userSession['id'];
          $subrecruiter_id = 0;
        }
        
        $tid = $_GET['testimonial'];

        $data['testDatas'] = $this->Recruit_Model->testimonial_single($tid);        
        $this->load->view('recruiter/testimonial_update', $data);
    }

    public function update() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $this->load->view('recruiter/testimonial_add', $data);
        } else {
            $userSession = $this->session->userdata('userSession');
            if(!empty($userSession['label']) && $userSession['label']=='3'){
              $subrecruiter_id = $userSession['id'];
              $userSession['id'] = $userSession['parent_id'];
            }else{
              $userSession['id'] = $userSession['id'];
              $subrecruiter_id = 0;
            }
            
            $data1 = ['name'=>$userData['name'], 'cname'=>$userData['cname'], 'description' => $userData['description']];
            $recruiterInserted = $this->Recruit_Model->testimonial_update($userData['tid'], $data1);
            
            if($recruiterInserted) {    
                $data1['addsuccess'] = "Testimonial updated successfully";
                redirect("recruiter/testimonial/testimonial_list");
            } else {
                redirect("recruiter/testimonial/testimonial_list");
            }
        }
    }

}
?>  
