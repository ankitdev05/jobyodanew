<?php
ob_start();
ini_set('display_errors', 1);
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'third_party/cloudinary/Cloudinary.php';
require APPPATH . 'third_party/cloudinary/Uploader.php';
require APPPATH . 'third_party/cloudinary/Api.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Recruiter extends CI_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Common_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('Subscription_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->helper('cookie');
        $this->load->library('aws3');
        /*if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }*/
    }

    public function index()
    {
        $this->load->view('recruiter/login');
    }

    public function add_advertisement()
    {
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $this->load->view('recruiter/add_advertisement', $data);
    }

    public function insertadvertise()
    {
        $userData = $this->input->post();
        //echo $domain = $userData['domain'];die;
        $this->form_validation->set_rules('location', 'Location', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim');
        $this->form_validation->set_rules('company', 'Company', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            $data['errors'] = $this->form_validation->error_array();
            $userSession = $this->session->userdata('userSession');
            $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
            $data['recruitData'][0] = $userData;
            $this->load->view('recruiter/add_advertisement', $data);
        } else {
            $userSession = $this->session->userdata('userSession');
            $config = array(
                'upload_path' => "./recruiterupload/",
                'allowed_types' => "jpg|png|jpeg",
                //'max_size' => '500000',
                // 'min_width' => '550',
                // 'min_height' => '450',
                // 'max_width' => '650',
                // 'max_height' => '550',
            );
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('profilePic')) {
                $data = array('upload_data' => $this->upload->data());
                $this->resizeImage($data['upload_data']['file_name']);
                $picPath = base_url() . "recruiterupload/" . $data['upload_data']['file_name'];
            } else {
                $data['errors']['profilePic'] = strip_tags($this->upload->display_errors());
                $userSession = $this->session->userdata('userSession');
                $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
                $data['recruitData'][0] = $userData;

                $this->load->view('recruiter/add_advertisement', $data);
                exit;
            }

            $addLatLong = $this->getLatLong($userData['location']);

            $data1 = ["banner" => $picPath, 'location' => $userData['location'],  'company_id' => $userSession['id'], 'site_id' => $userData['company'], 'start_date' => date("Y-m-d", strtotime($userData['start_date'])), 'end_date' => date("Y-m-d", strtotime($userData['end_date'])), 'city' => $addLatLong['city'], 'latitude' => $addLatLong['latitude'], 'longitude' => $addLatLong['longitude'], 'description' => " "];
            $recruiterInserted = $this->Recruit_Model->advertise_insert($data1);


            if ($recruiterInserted) {

                // Subscription

                $getplan = $this->Subscription_Model->view_existing($jobData['recruId']);
                $updatePlan = ["remaning_advertise"=>$getplan[0]['remaning_advertise'] - 1];
                $this->Subscription_Model->view_existing_updation($updatePlan, $getplan[0]['id']);

                $data1['addsuccess'] = "Advertisement posted successfully";
                redirect("recruiter/recruiter/advertise_list");
            } else {
                redirect("recruiter/recruiter/add_advertisement");
            }
        }
    }

    public function getLatLongCurl($address)
    {
        if (!empty($address)) {
            //Formatted address
            $formattedAddr = str_replace(' ', '+', $address);
            //Send request and receive json data by address


            $url =    "https://maps.googleapis.com/maps/api/geocode/json?address=" . $formattedAddr . "&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);

            $response = curl_exec($ch);

            if (curl_errno($ch)) {
                echo 'cURL error: ' . curl_error($ch);
            }

            curl_close($ch);

            $response_a = json_decode($response);

            var_dump($response_a);
            die;

            $data['latitude'] = $response_a->results[0]->geometry->location->lat;
            $data['longitude'] = $response_a->results[0]->geometry->location->lng;

            for ($j = 0; $j < count($response_a->results[0]->address_components); $j++) {
                $city = $response_a->results[0]->address_components[$j]->types;
                if ($city[0] == "locality") {
                    $data['city'] = $response_a->results[0]->address_components[$j]->long_name;
                }
            }

            //Return latitude and longitude of the given address
            if (!empty($data)) {
                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public function advertise_list()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['recruitFetch'] = $this->Recruit_Model->advertisement_list($userSession['id']);
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        //echo $this->db->last_query();die;
        //print_r($data['recruitFetch']);die;
        $this->load->view('recruiter/advertise_list', $data);
    }

    public function editadvertise()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $id = base64_decode($_GET['id']);

        $data['recruitFetch'] = $this->Recruit_Model->advertisement_single($id);
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $this->load->view('recruiter/advertise_edit', $data);
    }

    public function updateadvertise()
    {
        $userData = $this->input->post();
        $advertiseid = $userData['advertiseid'];

        $this->form_validation->set_rules('location', 'Location', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim');
        $this->form_validation->set_rules('company', 'Company', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $userSession = $this->session->userdata('userSession');
            $data['recruitFetch'] = $this->Recruit_Model->advertisement_single($advertiseid);
            $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
            $this->load->view('recruiter/advertise_edit', $data);
        } else {
            $userSession = $this->session->userdata('userSession');
            $addLatLong = $this->getLatLong($userData['location']);

            if (file_exists($_FILES['profilePic']['tmp_name'])) {

                $config = array(
                    'upload_path' => "./recruiterupload/",
                    'allowed_types' => "jpg|png|jpeg",
                    // 'min_width' => '550',
                    // 'min_height' => '450',
                    // 'max_width' => '650',
                    // 'max_height' => '550',
                );
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('profilePic')) {
                    $data = array('upload_data' => $this->upload->data());
                    $this->resizeImage($data['upload_data']['file_name']);
                    $picPath = base_url() . "recruiterupload/" . $data['upload_data']['file_name'];

                    $data1 = ["banner" => $picPath, 'location' => $userData['location'], 'site_id' => $userData['company'], 'start_date' => date("Y-m-d", strtotime($userData['start_date'])), 'end_date' => date("Y-m-d", strtotime($userData['end_date'])), 'city' => $addLatLong['city'], 'latitude' => $addLatLong['latitude'], 'longitude' => $addLatLong['longitude'], 'description' => $userData['desc']];
                } else {
                    $data['errors']['profilePic'] = $this->upload->display_errors();
                    $userSession = $this->session->userdata('userSession');
                    $data['recruitFetch'] = $this->Recruit_Model->advertisement_single($advertiseid);
                    $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
                    $this->load->view('recruiter/advertise_edit', $data);
                    exit;
                }
            } else {
                $data1 = ['location' => $userData['location'], 'site_id' => $userData['company'], 'start_date' => date("Y-m-d", strtotime($userData['start_date'])), 'end_date' => date("Y-m-d", strtotime($userData['end_date'])), 'city' => $addLatLong['city'], 'latitude' => $addLatLong['latitude'], 'longitude' => $addLatLong['longitude'], 'description' => $userData['desc']];
            }

            $recruiterInserted = $this->Recruit_Model->advertise_update($data1, $advertiseid);

            if ($recruiterInserted) {
                $data1['addsuccess'] = "Advertisement updated successfully";
                redirect("recruiter/recruiter/advertise_list");
            } else {
                redirect("recruiter/recruiter/advertise_edit");
            }
        }
    }

    public function deleteAdvertise()
    {
        $id = $_GET['id'];
        $this->Recruit_Model->advertisement_delete($id);
        redirect("recruiter/recruiter/advertise_list");
    }


    public function add_videoadvertisement()
    {
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }

        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $this->load->view('recruiter/add_videoadvertisement', $data);
    }

    public function insertvideoadvertise()
    {
        $userData = $this->input->post();
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['recruitData'] = $userData;
            $this->load->view('recruiter/add_videoadvertisement', $data);
        } else {

            $userSession = $this->session->userdata('userSession');

            $data1 = ["video" => $userData['videourl'], "thumbnail" => $userData['videothumb'], 'title' => $userData['title'], 'recruiter_id' => $userSession['id'], 'start_date' => date("Y-m-d", strtotime($userData['start_date'])), 'end_date' => date("Y-m-d", strtotime($userData['end_date'])), 'description' => $userData['desc'], "publicid" => $userData['publicid']];

            $recruiterInserted = $this->Recruit_Model->videoadvertise_insert($data1);

            if ($recruiterInserted) {

                // Subscription

                $getplan = $this->Subscription_Model->view_existing($jobData['recruId']);
                $updatePlan = ["remaining_video_post"=>$getplan[0]['remaining_video_post'] - 1];
                $this->Subscription_Model->view_existing_updation($updatePlan, $getplan[0]['id']);
                
                $data1['addsuccess'] = "Video Advertisement posted successfully";
                redirect("recruiter/recruiter/videoadvertise_list");
            } else {
                redirect("recruiter/recruiter/add_videoadvertisement");
            }
        }
    }

    public function generateString($n)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }


    public function deleteVideoadvertise()
    {
        $id = $_GET['id'];
        $this->Recruit_Model->videoadvertisement_delete($id);
        redirect("recruiter/recruiter/videoadvertise_list");
    }

    public function videoadvertise_list()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['recruitFetch'] = $this->Recruit_Model->videoadvertisement_list($userSession['id']);
        $this->load->view('recruiter/videoadvertise_list', $data);
    }


    public function contact()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $this->load->view('recruiter/contact', $data);
    }

    public function contact_form()
    {
        $userSession = $this->session->userdata('userSession');
        //print_r($userSession);die;
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $contactData = $this->input->post();

        //$data1 = ["name"=>$contactData['name'], "email"=>$contactData['email'], "phone"=> $contactData['phone'], "message"=> $contactData['message']];

        //$contactInserted = $this->Candidate_Model->contact_insert($data1);
        //$userId = $contactData['user_id'];
        /*if($contactInserted) {*/
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['contactData'] = $contactData;
            $this->load->view('recruiter/contact', $data);
        } else {
            $this->load->library('email');
            $data['subject'] = $contactData['subject'];
            $data['message'] = $contactData['message'];
            $data['name'] = $userSession['fname'] . ' ' . $userSession['lname'];
            $caseid = rand(10000, 99999);
            $data['caseId'] = 'JobYoDA-Recruiter' . $caseid;
            if (!empty($contactData['other_subject'])) {
                $data['other_subject'] = $contactData['other_subject'];
            }
            $msg = $this->load->view('recruiter/contactemail', $data, TRUE);
            $msg1 = $this->load->view('recruiter/contactemailr', $data, TRUE);
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'smtpout.asia.secureserver.net',
                'smtp_user' => 'help@jobyoda.com',
                'smtp_pass' => 'Usa@1234567',
                'smtp_port' => 465,
                'smtp_crypto' => 'ssl',
                'charset' => 'utf-8',
                'mailtype' => 'html',
                'crlf' => "\r\n",
                'newline' => "\r\n"
            );

            $this->email->initialize($config);
            $this->email->from($userSession['email'], "JobYoDA");
            //$this->email->to('villaverred@gmail.com','digvijayankoti29@gmail.com');
            $this->email->to('help@jobyoda.com');
            //$this->email->to('sunaina.singhal@mobulous.com');
            $this->email->subject('Ticket ID:' . $caseid);

            $this->email->message($msg);
            $this->email->send();

            $this->email->initialize($config);
            $this->email->from('help@jobyoda.com', "JobYoDA");
            //$this->email->to('villaverred@gmail.com','digvijayankoti29@gmail.com');
            $this->email->to($userSession['email']);
            //$this->email->to('sunaina.singhal@mobulous.com');
            $this->email->subject('Ticket ID:' . $caseid);

            $this->email->message($msg1);
            $this->email->send();

            $this->session->set_tempdata('contactSuccess', 'Email sent Successfully');

            redirect("recruiter/recruiter/contact");
        }
        /*} else {*/
        //redirect("recruiter/candidate/singleCandidates?id=$userId");
        //}

    }
    public function faq()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['faqs'] = $this->Recruit_Model->faqlisting();
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $this->load->view('recruiter/faq', $data);
    }



    public function logincheck()
    {
        $userData = $this->input->post();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_valid_password');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['userData'] = $userData;
            $this->load->view('recruiter/login', $data);
        } else {
            $password = $userData['password'];
            $data = ["email" => $userData['email']];
            $recruiterCheck = $this->Recruit_Model->recruiter_login($data);
            //var_dump($recruiterCheck);die;
            if (!empty($recruiterCheck)) {
                if ($recruiterCheck[0]['status'] == 1) {
                    if ($recruiterCheck[0]['active'] == 1) {
                        if ($recruiterCheck) {
                            $getPass = $recruiterCheck[0]['password'];
                            $vPass = $this->encryption->decrypt($getPass);
                            // echo $vPass;die;
                            if ($password == $vPass) {
                                $getData = $this->fetchRecruiterSingleData($recruiterCheck[0]['id']);

                                $logincount = $getData['logincount'];
                                $logincount = $logincount + 1;
                                $dataupdate = ["logincount" => $logincount];
                                $this->Recruit_Model->recruiter_update($dataupdate, $recruiterCheck[0]['id']);

                                //print_r($getData);die;
                                $this->session->set_userdata('userSession', $getData);

                                $openings11 = $this->Jobpost_Model->job_openings();
                                $userlist11 = $this->Common_Model->user_list();

                                $getDatamore = ["onedata" => $openings11[0]['openings'], "twodata" => count($userlist11)];
                                $this->session->set_userdata('userSession11', $getDatamore);

                                $recruiterId = $this->session->userdata('userSession');
                                //print_r($recruiterId);die;
                                $datecondition = "str_to_date(applied_jobs.created_at,'%Y-%m-%d')= str_to_date(applied_jobs.updated_at,'%Y-%m-%d')";
                                /*$checknotificationStatus= $this->Recruit_Model->countNotification($datecondition);
            
            if(!empty($checknotificationStatus && count($checknotificationStatus)>=1))
            {
                //print_r($recruiterId);die;
               $checkcountmessage = $this->Recruit_Model->checkCountmessage($recruiterId['id']);
                 // echo $this->db->last_query();die;
              $enddate =  date('Y-m-d', strtotime($checkcountmessage[0]['updated_at']. ' + 3 days'));
              $enddate = strtotime($enddate);
                $updated_date=  strtotime($checkcountmessage[0]['updated_at']);
              $datediff =  $enddate - $updated_date;
              $daydiff = floor($datediff / (60*60*24));

              if(!empty($checkcountmessage) && $datediff >=3)
              {
                   $Notificationupdate =      array(
                                                    'nitifycount'  =>'2',
                                                    );
                    
                   $notificationmessageupdate = array(
                                          'recruiter_id' =>$recruiterId['id'],
                                           'message'=>"Its been observed that the status of job application are not getting change kindly change the job application status. ",
                                           'job_status'        =>'1',
                                          );
                   $this->Recruit_Model->update_notificationcount($Notificationupdate,$recruiterId['id']);

                   $this->Recruit_Model->Notificationmessageinsert($notificationmessageupdate);
                   $this->session->set_flashdata('successAlert','successfully');
               }
               if(!empty($checkcountmessage) && count($checkcountmessage)>=5)
               {
                $blockrecruiter_id= array('recruiter_id' =>$recruiterId['id'],

                                         );
                 $this->Recruit_Model->blockRecruiter($blockrecruiter_id);
               }
               if(empty($checkcountmessage))
               {
                  $arrayNotification = array('recruiter_id' =>$recruiterId['id'],
                                         'nitifycount'  =>'1',
                                        );
                  $notificationmessage = array('recruiter_id' =>$recruiterId['id'],
                                           'message'=>"Your Application status is 'new Application'.",
                                           'job_status'        =>'1',
                                          );
                  $this->Recruit_Model->countNotificationinsert($arrayNotification);
                  $this->Recruit_Model->Notificationmessageinsert($notificationmessage);
               }  
               
            }
            else
            {
             $this->Recruit_Model->deleteRecruiter($recruiterId['id']); 
             $this->Recruit_Model->Notificationmessageinsert($notificationmessage);
            }*/
                                if (isset($userData['remember'])) {
                                    $this->input->set_cookie('uemail', $userData['email'], 86500);
                                    $this->input->set_cookie('upassword', $vPass, 86500);
                                } else {
                                    delete_cookie('uemail');
                                    delete_cookie('upassword');
                                }


                                $userSession = $this->session->userdata('userSession');
                                if (!empty($userSession['label']) && $userSession['label'] == '3') {
                                    $subrecruiter_id = $userSession['id'];
                                    $userSession['id'] = $userSession['parent_id'];
                                } else {
                                    $userSession['id'] = $userSession['id'];
                                    $subrecruiter_id = 0;
                                }

                                $getSubscription = $this->Subscription_Model->view_existing($userSession['id']);
                                if($getSubscription) {
                                    $this->session->set_userdata('userSubscriptionSession', $getSubscription[0]);
                                } else {
                                    $this->session->set_userdata('userSubscriptionSession', []);
                                }

                                redirect("recruiter/dashboard");
                            } else {
                                $this->session->set_tempdata('loginerror', 'Password is incorrect', 5);
                                redirect("recruiter/recruiter/index");
                            }
                        } else {
                            $this->session->set_tempdata('loginerror', 'Email id is incorrect', 5);
                            redirect("recruiter/recruiter/index");
                        }
                    } else {
                        $this->session->set_tempdata('loginerror', 'Your Account is blocked', 5);
                        redirect("recruiter/recruiter/index");
                    }
                } else {
                    $this->session->set_tempdata('loginerror', 'Please contact admin to get your account approved', 5);
                    redirect("recruiter/recruiter/index");
                }
            } else {
                $this->session->set_tempdata('loginerror', "Email id doesn't exist", 5);
                redirect("recruiter/recruiter/index");
            }
        }
    }

    public function signup()
    {
        $data["phonecodes"] = $data = $this->Common_Model->phonecode_lists();

        $data['meta_title'] = "Jobyoda Recruiter Sign up | Jobyoda Partner Recruiters Near You";
        $data['meta_description'] = "Partner with JobYoDa and find the best candidates that fit your vacant job position. Improve your roster of talents and sign up with JobYoDa today!";

        $this->load->view('recruiter/signup', $data);
    }

    public function signupinsert()
    {
        $userData = $this->input->post();
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|callback_alpha_dash_space|max_length[25]');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|callback_alpha_dash_space|max_length[25]');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('domain', 'Domain Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[recruiter.email]');
        $this->form_validation->set_message('is_unique', 'Email is already registered');
        $this->form_validation->set_message('valid_email', 'Email is not valid');
        $this->form_validation->set_rules('phonecode', 'Phone Code', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_valid_password');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data['userData'] = $userData;
            $this->load->view('recruiter/signup', $data);
        } else {

            $domain = $userData['domain'];
            //$domain1 = substr($domain, strpos($domain, ".") + 1);;
            $email1 = $userData['email'];
            $email2 = explode('@', $email1);

            if (strtolower($domain) == strtolower($email2[1])) {
                $hash = $this->encryption->encrypt($userData['password']);
                $data1 = ["fname" => $userData['fname'], "lname" => $userData['lname'], "cname" => $userData['cname'], "email" => $userData['email'], "password" => $hash, "status" => 0];

                $recruiterInserted = $this->Recruit_Model->recruiter_stepone_insert($data1);

                $addLatLong = $this->getLatLong($userData['address']);
                //print_r($addLatLong);die;
                $data2 = ["recruiter_id" => $recruiterInserted, "phonecode" => $userData['phonecode'], "phone" => $userData['phone'], "address" => $userData['address'], "city" => $addLatLong['city'], "latitude" => $addLatLong['latitude'], "longitude" => $addLatLong['longitude']];

                $this->Recruit_Model->recruiter_steptwo_insert($data2);

                if ($recruiterInserted) {
                    $email = $userData['email'];
                    $name = $userData['fname'];
                    $this->load->library('email');

                    $data['full_name'] = ucfirst($name) . ' ' . ucfirst($userData['lname']);
                    $data['phone'] = '+' . $userData['phonecode'] . $userData['phone'];
                    $data['email'] = $email;
                    $msg = $this->load->view('recruiter/email', $data, TRUE);
                    $msg1 = $this->load->view('recruiter/email_admin', $data, TRUE);
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtpout.asia.secureserver.net',
                        'smtp_user' => 'help@jobyoda.com',
                        'smtp_pass' => 'Usa@1234567',
                        'smtp_port' => 465,
                        'smtp_crypto' => 'ssl',
                        'charset' => 'utf-8',
                        'mailtype' => 'html',
                        'crlf' => "\r\n",
                        'newline' => "\r\n"
                    );

                    $this->email->initialize($config);
                    $this->email->from("help@jobyoda.com", "JobYoDA");
                    $this->email->to($email);
                    $this->email->subject('JobYoDA Recruiter - New Registration');
                    /*$msg = "Dear " . $name.' '.$userData['lname'];
                    $msg .= "You have Successfully Registered. You can login into your account once Admin approves your account within 24 hours!";*/

                    $this->email->message($msg);
                    if ($this->email->send()) { } else {
                        echo $this->email->print_debugger();
                    }

                    $this->email->initialize($config);
                    $this->email->from("help@jobyoda.com", "JobYoDA");
                    $this->email->to('priya@jobyoda.com');
                    $this->email->subject('JobYoDA Recruiter Approval - Pending');
                    /*$msg = "Dear " . $name.' '.$userData['lname'];
                    $msg .= "You have Successfully Registered. You can login into your account once Admin approves your account within 24 hours!";*/

                    $this->email->message($msg1);
                    if ($this->email->send()) { } else {
                        echo $this->email->print_debugger();
                    }
                    //exit();
                    /*$verifyCheck = $this->Recruit_Model->verification_check($recruiterInserted);
                    if($verifyCheck) {
                        $vCode = ["verifyCode"=>$token];
                        $this->Recruit_Model->verification_update($vCode, $recruiterInserted);
                    } else {
                        $vCode = ["user_id"=>$recruiterInserted, "verifyCode"=>$token];
                        $this->Recruit_Model->verification_insert($vCode);
                    }*/

                    /*$data1['forgotsuccess'] = "Check your Email for verification code";
                    $data1["userId"] = $recruiterInserted;
                    redirect("recruiter/recruiter/loginverify");*/
                    $data1['verifysuccess'] = "Thank you for your interest in JobYoDA. One of our account executives will reach out to you in 24-48 hours to set you up.";
                    $this->load->view('recruiter/login', $data1);
                    //$this->load->view('recruiter/newverification', $data1);
                } else {
                    redirect("recruiter/recruiter/signup");
                }
            } else {
                $data['errors'] = ["domain" => "Domain and Email domain did not match"];
                $data['userData'] = $userData;
                $data["phonecodes"] = $this->Common_Model->phonecode_lists();
                $this->load->view('recruiter/signup', $data);
            }
        }
    }

    public function verifysignupemail()
    {
        $userData = $this->input->post();
        $verifyCheck = $this->Recruit_Model->verification_check($userData['ids']);

        if ($verifyCheck) {
            if ($verifyCheck[0]['verifyCode'] == $userData['codeverify']) {
                redirect("recruiter/recruiter/loginverify");
            } else {
                $data1['verifyerror'] = "Verification code is incorrect";
                $data1["userId"] = $userData['ids'];
                $this->load->view('recruiter/newverification', $data1);
            }
        } else {
            $data1['verifyerror'] = "Check your Email for change password";
            $data1["userId"] = $userData['ids'];
            $this->load->view('recruiter/newverification', $data1);
        }
    }

    public function resendMail()
    {
        $userData = $this->input->post();
        $dataa = ["id" => $userData['ids']];
        $check = $this->Recruit_Model->recruiter_login($dataa);

        if ($check) {
            $email = $check[0]['email'];
            $name = $check[0]['fname'];
            $this->load->library('email');
            $this->email->from("help@jobyoda.com", "JobYoDA");
            $this->email->to($email);
            $this->email->subject('JobYoDA Recruiter - Resend Email Verification Code');
            $token = $this->getToken();
            $msg = "Dear " . $name;
            $msg .= ". Verify your Email Id by using this code: ";
            $msg .= ". Your verification code is " . $token;
            $this->email->message($msg);
            $this->email->send();

            $verifyCheck = $this->Recruit_Model->verification_check($userData['ids']);

            if ($verifyCheck) {
                $vCode = ["verifyCode" => $token];
                $this->Recruit_Model->verification_update($vCode, $userData['ids']);
            } else {
                $vCode = ["user_id" => $userData['ids'], "verifyCode" => $token];
                $this->Recruit_Model->verification_insert($vCode);
            }

            $data1['forgotsuccess'] = "Check your Email for change password";
            $data1["userId"] = $userData['ids'];
            $this->load->view('recruiter/newverification', $data1);
        } else {
            $data1['verifyerror'] = "Check your Email for change password";
            $data1["userId"] = $userData['ids'];
            $this->load->view('recruiter/newverification', $data1);
        }
    }

    public function loginverify()
    {
        $data1['verifysuccess'] = "Successfully Registered. Now Admin will approve your account within 24 hours!";
        $this->load->view('recruiter/login', $data1);
    }

    public function companyprofile()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        //print_r($userSession);die;
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
        $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
        $data['companySites'] = $this->Recruit_Model->company_sites($userSession['id']);
        $topPicks = $this->Recruit_Model->company_topicks_single($userSession['id']);
        if ($topPicks) {
            $x1 = 0;
            foreach ($topPicks as $topPick) {
                $topPicks1[$x1] = $topPick['picks_id'];
                $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else {
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($userSession['id']);
        if ($allowances) {
            $x1 = 0;
            foreach ($allowances as $allowance) {
                $allowance1[$x1] = $allowance['allowances_id'];
                $x1++;
            }
            $data['allowance2'] = $allowance1;
        } else {
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($userSession['id']);
        if ($medicals) {
            $x1 = 0;
            foreach ($medicals as $medical) {
                $medical1[$x1] = $medical['medical_id'];
                $x1++;
            }
            $data['medical2'] = $medical1;
        } else {
            $data['medical2'] = [];
        }

        $leaves = $this->Recruit_Model->company_leaves_single($userSession['id']);
        if ($leaves) {
            $x1 = 0;
            foreach ($leaves as $leave) {
                $leave1[$x1] = $leave['leaves_id'];
                $x1++;
            }
            $data['leave2'] = $leave1;
        } else {
            $data['leave2'] = [];
        }

        $workshifts = $this->Recruit_Model->company_workshifts_single($userSession['id']);
        if ($workshifts) {
            $x1 = 0;
            foreach ($workshifts as $workshift) {
                $workshift1[$x1] = $workshift['workshift_id'];
                $x1++;
            }
            $data['workshift2'] = $workshift1;
        } else {
            $data['workshift2'] = [];
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $this->load->view('recruiter/companyprofile', $data);
    }

    public function companySite()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data["regions"] = $this->Common_Model->region_lists();
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $topPicks = $this->Recruit_Model->company_topicks_single($userSession['id']);
        if ($topPicks) {
            $x1 = 0;
            foreach ($topPicks as $topPick) {
                $topPicks1[$x1] = $topPick['picks_id'];
                $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else {
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($userSession['id']);
        if ($allowances) {
            $x1 = 0;
            foreach ($allowances as $allowance) {
                $allowance1[$x1] = $allowance['allowances_id'];
                $x1++;
            }
            $data['allowance2'] = $allowance1;
        } else {
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($userSession['id']);
        if ($medicals) {
            $x1 = 0;
            foreach ($medicals as $medical) {
                $medical1[$x1] = $medical['medical_id'];
                $x1++;
            }
            $data['medical2'] = $medical1;
        } else {
            $data['medical2'] = [];
        }

        $leaves = $this->Recruit_Model->company_leaves_single($userSession['id']);
        if ($leaves) {
            $x1 = 0;
            foreach ($leaves as $leave) {
                $leave1[$x1] = $leave['leaves_id'];
                $x1++;
            }
            $data['leave2'] = $leave1;
        } else {
            $data['leave2'] = [];
        }

        $workshifts = $this->Recruit_Model->company_workshifts_single($userSession['id']);
        if ($workshifts) {
            $x1 = 0;
            foreach ($workshifts as $workshift) {
                $workshift1[$x1] = $workshift['workshift_id'];
                $x1++;
            }
            $data['workshift2'] = $workshift1;
        } else {
            $data['workshift2'] = [];
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $this->load->view('recruiter/addnewsite', $data);
    }

    public function companySiteInsert()
    {
        //print_r($_FILES['site_image']);die;
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        //print_r($userSession);die;
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
        }
        //echo $userSession['id'];die;
        $userData = $this->input->post();
        if (!empty($userData['comm_channel'])) {
            $channel = implode(',', $userData['comm_channel']);
        } else {
            $channel = "";
        }
        /*$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[recruiter.email]');*/
        $this->form_validation->set_rules('site_name', 'Site/Location', 'trim|required');
        //$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('dayfrom', 'Day From', 'trim|required');
        $this->form_validation->set_rules('dayto', 'Day To', 'trim|required');
        $this->form_validation->set_rules('from_time', 'From Time', 'trim|required');
        $this->form_validation->set_rules('to_time', 'To Time', 'trim|required');

        if (empty($_FILES['profilePic']['name'])) {
            $this->form_validation->set_rules('profilePic', 'Site logo', 'trim|required');
        }
        //$this->form_validation->set_rules('recruiter_contact', 'Recruiter Contact', 'trim|required');
        // $this->form_validation->set_rules('recruiter_email', 'Recruiter Email', 'trim|required|valid_email');

        /*$this->form_validation->set_rules('rating', 'Rating', 'trim');
        $this->form_validation->set_rules('gross_salary', 'Gross Salary', 'trim');
        $this->form_validation->set_rules('basic_salary', 'Basic Salary', 'trim');
        $this->form_validation->set_rules('work_off', 'Work Off', 'trim');
        $this->form_validation->set_rules('leave', 'Annual leaves', 'trim');
        $this->form_validation->set_rules('job_type', 'Job Type', 'trim');*/

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data['userdata'] = $userData;
            $data['industryLists'] = $this->Common_Model->industry_lists();
            $data["regions"] = $this->Common_Model->region_lists();
            $topPicks = $this->Recruit_Model->company_topicks_single($userSession['id']);
            // print_r($topPicks);die;
            if ($topPicks) {
                $x1 = 0;
                foreach ($topPicks as $topPick) {
                    $topPicks1[$x1] = $topPick['picks_id'];
                    $x1++;
                }
                $data['topPicks2'] = $topPicks1;
            } else {
                $data['topPicks2'] = [];
            }

            $allowances = $this->Recruit_Model->company_allowances_single($userSession['id']);
            if ($allowances) {
                $x1 = 0;
                foreach ($allowances as $allowance) {
                    $allowance1[$x1] = $allowance['allowances_id'];
                    $x1++;
                }
                $data['allowance2'] = $allowance1;
            } else {
                $data['allowance2'] = [];
            }

            $medicals = $this->Recruit_Model->company_medical_single($userSession['id']);
            if ($medicals) {
                $x1 = 0;
                foreach ($medicals as $medical) {
                    $medical1[$x1] = $medical['medical_id'];
                    $x1++;
                }
                $data['medical2'] = $medical1;
            } else {
                $data['medical2'] = [];
            }
            $leaves = $this->Recruit_Model->company_leaves_single($userSession['id']);
            if ($leaves) {
                $x1 = 0;
                foreach ($leaves as $leave) {
                    $leave1[$x1] = $leave['leaves_id'];
                    $x1++;
                }
                $data['leave2'] = $leave1;
            } else {
                $data['leave2'] = [];
            }
            $workshifts = $this->Recruit_Model->company_workshifts_single($userSession['id']);
            if ($workshifts) {
                $x1 = 0;
                foreach ($workshifts as $workshift) {
                    $workshift1[$x1] = $workshift['workshift_id'];
                    $x1++;
                }
                $data['workshift2'] = $workshift1;
            } else {
                $data['workshift2'] = [];
            }

            $this->load->view('recruiter/addnewsite', $data);
        } else {
            //$userSession = $this->session->userdata('userSession');

            $data1 = ["parent_id" => $userSession['id'], "fname" => '', "lname" => '', "cname" => $userData['site_name'], "email" => '', "status" => 1];
            $recruiterInserted = $this->Recruit_Model->recruiter_stepone_insert($data1);

            $config = array(
                'upload_path' => "./recruiterupload/",
                'allowed_types' => "jpg|png|jpeg",
            );


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('profilePic')) {
                $data = array('upload_data' => $this->upload->data());
                $this->resizeImage($data['upload_data']['file_name']);
                $picPath = base_url() . "recruiterupload/" . $data['upload_data']['file_name'];
            } else {
                $picPath = "";
            }

            $addLatLong = $this->getLatLong($userData['address']);

            $data = [
                "recruiter_id" => $recruiterInserted,
                "phone" => '',
                "address" => $userData['address'],
                "site_name" => '',
                "dayfrom" => $userData['dayfrom'],
                "dayto" => $userData['dayto'],
                "from_time" => $userData['from_time'],
                "to_time" => $userData['to_time'],
                "latitude" => $addLatLong['latitude'],
                "longitude" => $addLatLong['longitude'],
                "city" => $addLatLong['city'],
                "region" => $userData['region'],
                "rphonecode" => '',
                "recruiter_contact" => '',
                "comm_channel" => '',
                "recruiter_email" => '',
                "rating" => '',
                "gross_salary" => '',
                "basic_salary" => '',
                "work_off" => '',
                "annual_leave" => '',
                "job_type" => '',
                "companyDesc" => $userData['compDesc'],
                "companyPic" => $picPath,
                //"industry" => $userData['industry']
            ];
            $this->Recruit_Model->recruiter_steptwo_insert($data);
            $count = count($_FILES['site_image']['name']);
            //echo $count;die;
            if ($count > 0) {
                for ($i = 0; $i < $count; $i++) {

                    if (strlen($_FILES['site_image']['name'][$i]) > 0) {

                        $_FILES['file']['name'] = $_FILES['site_image']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['site_image']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['site_image']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['site_image']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['site_image']['size'][$i];

                        // Set preference
                        $config['upload_path'] = "./recruiterupload/";
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        $config['file_name'] = $_FILES['site_image']['name'][$i];
                        //Load upload library
                        $this->load->library('upload', $config);

                        // File upload
                        if ($this->upload->do_upload('file')) {
                            // Get data about the file
                            $uploadData1 = $this->upload->data();
                            $this->resizeImage($uploadData1['file_name']);
                            $filename = base_url() . "recruiterupload/" . $uploadData1['file_name'];

                            $data['filenames'][] = $filename;
                        } else {
                            $filename = "";
                        }
                        $data1 = ["recruiter_id" => $recruiterInserted, "pic" => $filename];
                        $this->Recruit_Model->site_image_insert($data1);
                    }
                }
            }
            if (isset($userData['toppicks'])) {
                $topPicks = $userData['toppicks'];
                $x = 0;
                foreach ($topPicks as $topPick) {
                    $dataPicks[$x] = ["recruiter_id" => $recruiterInserted, "picks_id" => $topPick];
                    $x++;
                }
                $this->Recruit_Model->recruiter_toppicks_insert($dataPicks);
            }
            if (isset($userData['allowances'])) {
                $topAllos = $userData['allowances'];
                $x = 0;
                foreach ($topAllos as $topAllo) {
                    $dataAllos[$x] = ["recruiter_id" => $recruiterInserted, "allowances_id" => $topAllo];
                    $x++;
                }
                $this->Recruit_Model->recruiter_allowances_insert($dataAllos);
            }
            if (isset($userData['medical'])) {
                $topMedis = $userData['medical'];
                $x = 0;
                foreach ($topMedis as $topMedi) {
                    $dataMedis[$x] = ["recruiter_id" => $recruiterInserted, "medical_id" => $topMedi];
                    $x++;
                }
                $this->Recruit_Model->recruiter_medical_insert($dataMedis);
            }
            if (isset($userData['shifts'])) {
                $topShifts = $userData['shifts'];
                $x = 0;
                foreach ($topShifts as $topShift) {
                    $dataShifts[$x] = ["recruiter_id" => $recruiterInserted, "workshift_id" => $topShift];
                    $x++;
                }
                $this->Recruit_Model->recruiter_shifts_insert($dataShifts);
            }
            if (isset($userData['leavs'])) {
                $topLeaves = $userData['leavs'];
                $x = 0;
                foreach ($topLeaves as $topLeave) {
                    $dataLeaves[$x] = ["recruiter_id" => $recruiterInserted, "leaves_id" => $topLeave];
                    $x++;
                }
                $this->Recruit_Model->recruiter_leaves_insert($dataLeaves);
            }

            $this->session->set_tempdata('inserted', 'Company site added Successfully', 5);
            redirect("recruiter/recruiter/companyprofile");
        }
    }

    public function manageCompany()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
        $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $topPicks = $this->Recruit_Model->company_topicks_single($userSession['id']);
        // print_r($topPicks);die;
        if ($topPicks) {
            $x1 = 0;
            foreach ($topPicks as $topPick) {
                $topPicks1[$x1] = $topPick['picks_id'];
                $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else {
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($userSession['id']);
        if ($allowances) {
            $x1 = 0;
            foreach ($allowances as $allowance) {
                $allowance1[$x1] = $allowance['allowances_id'];
                $x1++;
            }
            $data['allowance2'] = $allowance1;
        } else {
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($userSession['id']);
        if ($medicals) {
            $x1 = 0;
            foreach ($medicals as $medical) {
                $medical1[$x1] = $medical['medical_id'];
                $x1++;
            }
            $data['medical2'] = $medical1;
        } else {
            $data['medical2'] = [];
        }
        $leaves = $this->Recruit_Model->company_leaves_single($userSession['id']);
        if ($leaves) {
            $x1 = 0;
            foreach ($leaves as $leave) {
                $leave1[$x1] = $leave['leaves_id'];
                $x1++;
            }
            $data['leave2'] = $leave1;
        } else {
            $data['leave2'] = [];
        }
        $workshifts = $this->Recruit_Model->company_workshifts_single($userSession['id']);
        if ($workshifts) {
            $x1 = 0;
            foreach ($workshifts as $workshift) {
                $workshift1[$x1] = $workshift['workshift_id'];
                $x1++;
            }
            $data['workshift2'] = $workshift1;
        } else {
            $data['workshift2'] = [];
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $this->load->view('recruiter/managesites', $data);
    }

    public function companyupdate()
    {
        //print_r($_FILES['profilePic']);die;
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        $userData = $this->input->post();

        /*echo "<pre>";
        print_r($userData);die;*/
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');

        $checkrecruitImg = $this->Recruit_Model->company_details_single($userSession['id']);
        if (strlen($checkrecruitImg[0]['companyPic']) < 1) {

            if (empty($_FILES['profilePic']['name'])) {
                $this->form_validation->set_rules('profilePic', 'Company image', 'required');
            }
        }
        /*$this->form_validation->set_rules('rating', 'Rating', 'trim');
        $this->form_validation->set_rules('gross_salary', 'Gross Salary', 'trim');
        $this->form_validation->set_rules('basic_salary', 'Basic Salary', 'trim');
        $this->form_validation->set_rules('work_off', 'Work Off', 'trim');
        $this->form_validation->set_rules('leave', 'Annual leaves', 'trim');
        $this->form_validation->set_rules('job_type', 'Job Type', 'trim');*/

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
            $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
            $data['industryLists'] = $this->Common_Model->industry_lists();
            $topPicks = $this->Recruit_Model->company_topicks_single($userSession['id']);

            if ($topPicks) {
                $x1 = 0;
                foreach ($topPicks as $topPick) {
                    $topPicks1[$x1] = $topPick['picks_id'];
                    $x1++;
                }
                $data['topPicks2'] = $topPicks1;
            } else {
                $data['topPicks2'] = [];
            }

            $allowances = $this->Recruit_Model->company_allowances_single($userSession['id']);
            if ($allowances) {
                $x1 = 0;
                foreach ($allowances as $allowance) {
                    $allowance1[$x1] = $allowance['allowances_id'];
                    $x1++;
                }
                $data['allowance2'] = $allowance1;
            } else {
                $data['allowance2'] = [];
            }

            $medicals = $this->Recruit_Model->company_medical_single($userSession['id']);
            if ($medicals) {
                $x1 = 0;
                foreach ($medicals as $medical) {
                    $medical1[$x1] = $medical['medical_id'];
                    $x1++;
                }
                $data['medical2'] = $medical1;
            } else {
                $data['medical2'] = [];
            }
            $leaves = $this->Recruit_Model->company_leaves_single($userSession['id']);
            if ($leaves) {
                $x1 = 0;
                foreach ($leaves as $leave) {
                    $leave1[$x1] = $leave['leaves_id'];
                    $x1++;
                }
                $data['leave2'] = $leave1;
            } else {
                $data['leave2'] = [];
            }
            $workshifts = $this->Recruit_Model->company_workshifts_single($userSession['id']);
            if ($workshifts) {
                $x1 = 0;
                foreach ($workshifts as $workshift) {
                    $workshift1[$x1] = $workshift['workshift_id'];
                    $x1++;
                }
                $data['workshift2'] = $workshift1;
            } else {
                $data['workshift2'] = [];
            }
            $this->load->view('recruiter/managesites', $data);
        } else {
            $data1 = ["fname" => $userData['fname'], "lname" => $userData['lname'], "cname" => $userData['cname'], "email" => $userData['email']];

            $recruiterInserted = $this->Recruit_Model->recruiter_stepone_update($data1, $userSession['id']);

            if ($recruiterInserted) {

                if ($_FILES['profilePic']['tmp_name']) {

                    $config = array(
                        'upload_path' => "./recruiterupload/",
                        'allowed_types' => "jpg|png|jpeg",
                    );

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('profilePic')) {
                        $data = array('upload_data' => $this->upload->data());
                        $picPath = base_url() . "recruiterupload/" . $data['upload_data']['file_name'];
                    } else {

                        $picPath = " ";
                    }
                } else {
                    $picPath = " ";
                }

                $addLatLong = $this->getLatLong($userData['address']);

                if ($picPath == " ") {
                    $data2 = ["phone" => $userData['phone'], "address" => $userData['address'], "latitude" => $addLatLong['latitude'], "longitude" => $addLatLong['longitude'], "rating" => '', "gross_salary" => '', "basic_salary" => '', "work_off" => '', "annual_leave" => '', "job_type" => '', "companyDesc" => $userData['compDesc'], "industry" => $userData['industry']];
                } else {
                    $data2 = ["phone" => $userData['phone'], "address" => $userData['address'], "latitude" => $addLatLong['latitude'], "longitude" => $addLatLong['longitude'], "rating" => '', "gross_salary" => '', "basic_salary" => '', "work_off" => '', "annual_leave" => '', "job_type" => '', "companyDesc" => $userData['compDesc'],  "industry" => $userData['industry'], "companyPic" => $picPath];
                }
                $this->Recruit_Model->recruiter_steptwo_update($data2, $userSession['id']);
                //echo $this->db->last_query();die;            

                if (isset($userData['toppicks'])) {
                    $topPicks = $userData['toppicks'];
                    $x = 0;

                    foreach ($topPicks as $topPick) {
                        $dataPicks[$x] = ["recruiter_id" => $userSession['id'], "picks_id" => $topPick];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_toppicks_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_toppicks_insert($dataPicks);
                } else {
                    $this->Recruit_Model->recruiter_toppicks_delete($userSession['id']);
                }
                if ($userData['allowances']) {
                    $topAllos = $userData['allowances'];
                    $x = 0;
                    foreach ($topAllos as $topAllo) {
                        $dataAllos[$x] = ["recruiter_id" => $userSession['id'], "allowances_id" => $topAllo];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_allowances_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_allowances_insert($dataAllos);
                } else {
                    $this->Recruit_Model->recruiter_allowances_delete($userSession['id']);
                }
                if ($userData['medical']) {
                    $topMedis = $userData['medical'];
                    $x = 0;
                    foreach ($topMedis as $topMedi) {
                        $dataMedis[$x] = ["recruiter_id" => $userSession['id'], "medical_id" => $topMedi];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_medical_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_medical_insert($dataMedis);
                } else {
                    $this->Recruit_Model->recruiter_medical_delete($userSession['id']);
                }
                if ($userData['leavs']) {
                    $topLeaves = $userData['leavs'];
                    $x = 0;
                    foreach ($topLeaves as $topLeave) {
                        $dataLeaves[$x] = ["recruiter_id" => $userSession['id'], "leaves_id" => $topLeave];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_leaves_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_leaves_insert($dataLeaves);
                }
                if ($userData['shifts']) {
                    $topShifts = $userData['shifts'];
                    $x = 0;
                    foreach ($topShifts as $topShift) {
                        $dataShifts[$x] = ["recruiter_id" => $userSession['id'], "workshift_id" => $topShift];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_shifts_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_shifts_insert($dataShifts);
                }

                $this->session->set_tempdata('inserted', 'Company details updated successfully', 5);
                redirect("recruiter/recruiter/companyprofile");
            } else {
                redirect("recruiter/recruiter/manageCompany");
            }
        }
    }

    public function companySiteView()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if ($userSession['label'] == '3') {
            $userSession['id'] = $userSession['parent_id'];
            $subrecruiter_id = $userSession['id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $view = base64_decode($_GET['view']);
        $data['companyImg'] = $this->Recruit_Model->company_images($view);
        $data['companyDetails'] = $this->Recruit_Model->companysite_details_single1($userSession['id'], $view);
        //echo $this->db->last_query();die;
        $data['companySites'] = $this->Recruit_Model->company_sites1($userSession['id'], $view);
        $topPicks = $this->Recruit_Model->company_topicks_single($view);
        if ($topPicks) {
            $x1 = 0;
            foreach ($topPicks as $topPick) {
                $topPicks1[$x1] = $topPick['picks_id'];
                $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else {
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($view);
        if ($allowances) {
            $x1 = 0;
            foreach ($allowances as $allowance) {
                $allowance1[$x1] = $allowance['allowances_id'];
                $x1++;
            }
            $data['allowance2'] = $allowance1;
        } else {
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($view);
        if ($medicals) {
            $x1 = 0;
            foreach ($medicals as $medical) {
                $medical1[$x1] = $medical['medical_id'];
                $x1++;
            }
            $data['medical2'] = $medical1;
        } else {
            $data['medical2'] = [];
        }
        $leaves = $this->Recruit_Model->company_leaves_single($view);
        if ($leaves) {
            $x1 = 0;
            foreach ($leaves as $leave) {
                $leave1[$x1] = $leave['leaves_id'];
                $x1++;
            }
            $data['leave2'] = $leave1;
        } else {
            $data['leave2'] = [];
        }
        $workshifts = $this->Recruit_Model->company_workshifts_single($view);
        if ($workshifts) {
            $x1 = 0;
            foreach ($workshifts as $workshift) {
                $workshift1[$x1] = $workshift['workshift_id'];
                $x1++;
            }
            $data['workshift2'] = $workshift1;
        } else {
            $data['workshift2'] = [];
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        //echo $this->db->last_query();die;
        $this->load->view('recruiter/sitedetails', $data);
    }

    public function fetchRecruiterSingleData($id)
    {
        /*$recruiterData = $this->Recruit_Model->recruiter_single($id);
        $data = ["id" => $recruiterData[0]['id'], "fname" => $recruiterData[0]['fname'], "lname" => $recruiterData[0]['lname'], "email" => $recruiterData[0]['email']];
        return $data;*/
        $recruiterDatas = $this->Recruit_Model->recruiter_single($id);
        foreach ($recruiterDatas as $recruiterData) {
            $rid = $recruiterData['id'];

            $recruitpermission = $this->Recruit_Model->recruiter_permission($rid);
            if (!empty($recruitpermission[0]['site_details'])) {
                $recruitpermission[0]['site_details'] = $recruitpermission[0]['site_details'];
            } else {
                $recruitpermission[0]['site_details'] = '';
            }
            if (!empty($recruitpermission[0]['invoice_view'])) {
                $recruitpermission[0]['invoice_view'] = $recruitpermission[0]['invoice_view'];
            } else {
                $recruitpermission[0]['invoice_view'] = '';
            }
            if (!empty($recruitpermission[0]['edit_delete'])) {
                $recruitpermission[0]['edit_delete'] = $recruitpermission[0]['edit_delete'];
            } else {
                $recruitpermission[0]['edit_delete'] = '';
            }
            //print_r($recruitpermission);die;

            $recruiterDatas11 = $this->Recruit_Model->company_details_single($id);

            //var_dump($recruiterDatas11);die;

            $recruitData = ["id" => $recruiterData['id'], "parent_id" => $recruiterData['parent_id'], "label" => $recruiterData['label'], "fname" => $recruiterData['fname'], "lname" => $recruiterData['lname'], "email" => $recruiterData['email'], 'site_details' => $recruitpermission[0]['site_details'], "invoice_view" => $recruitpermission[0]['invoice_view'], "edit_delete" => $recruitpermission[0]['edit_delete'], "profilepic" => $recruiterDatas11[0]['companyPic'], "logincount" => $recruiterData['logincount']];
        }

        //$data = ["id" => $recruiterData[0]['id'], "fname" => $recruiterData[0]['fname'], "lname" => $recruiterData[0]['lname'], "email" => $recruiterData[0]['email']];
        return  $recruitData;
    }

    public function getLatLong($address)
    {
        if (!empty($address)) {
            //Formatted address
            $formattedAddr = str_replace(' ', '+', $address);
            //Send request and receive json data by address
            @$geocodeFromAddr = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
            //$geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=Noida&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk'); 
            if ($geocodeFromAddr) {
                $output = json_decode($geocodeFromAddr);
                //Get latitude and longitute from json data
                $data['latitude']  = $output->results[0]->geometry->location->lat;
                $data['longitude'] = $output->results[0]->geometry->location->lng;
                //var_dump($output->results[0]->address_components[0]->types);die;
                for ($j = 0; $j < count($output->results[0]->address_components); $j++) {
                    $city = $output->results[0]->address_components[$j]->types;
                    if ($city[0] == "locality") {
                        $data['city'] = $output->results[0]->address_components[$j]->long_name;
                    }
                }
                //Return latitude and longitude of the given address
                if (!empty($data)) {
                    return $data;
                } else {
                    return false;
                }
            } else {
                $data['latitude']  = '';
                $data['longitude'] = '';
                $data['city'] = '';
            }
        } else {
            return false;
        }
    }

    public function resizeImage($filename)
    {

        $source_path =  './recruiterupload/' . $filename;
        $target_path = './recruiterupload/thumbs/';
        $config_manip = array(
            'image_library' => 'gd2',
            'quality' => '100%',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => FALSE,
            'create_thumb' => FALSE,
            'width' => 150,
            'height' => 150
        );

        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
    }

    public function forgotPassword()
    {
        $userData = $this->input->post();

        if (!empty($userData['email'])) {
            $data = ["email" => $userData['email']];
            $userTokenCheck = $this->Recruit_Model->email_match($data);
            if ($userTokenCheck) {
                $email = $userTokenCheck[0]['email'];
                $name = $userTokenCheck[0]['fname'];
                /*$this->load->library('email');
                $this->email->from("ayush.jain@mobulous.com", "JobYoda");
                $this->email->to($email);
                $this->email->subject('JobYoda Recruiter- Forgot Password Link');
                $token = $this->getToken();
                $msg = "Dear " . $name;
                $msg .= ", Please click on the below link to change your account password: ";
                //$hash = $this->encryption->encrypt($userTokenCheck[0]['id']);
                $msg .= base_url() ."recruiter/recruiter/passwordChange/".$userTokenCheck[0]['id'];
                $msg .= ". Your verification code is ". $token;
                $this->email->message($msg);*/
                $this->load->library('email');
                $token = $this->getToken();
                $data['full_name'] = ucfirst($name) . ' ' . ucfirst($userTokenCheck[0]['lname']);
                $data['token'] = $token;
                $data['user_id'] = $userTokenCheck[0]['id'];
                $msg = $this->load->view('recruiter/passwordemail', $data, TRUE);
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtpout.asia.secureserver.net',
                    'smtp_user' => 'help@jobyoda.com',
                    'smtp_pass' => 'Usa@1234567',
                    'smtp_port' => 465,
                    'smtp_crypto' => 'ssl',
                    'charset' => 'utf-8',
                    'mailtype' => 'html',
                    'crlf' => "\r\n",
                    'newline' => "\r\n"
                );

                $this->email->initialize($config);
                $this->email->from("help@jobyoda.com", "JobYoDA");
                $this->email->to($email);
                $this->email->subject('JobYoDA Recruiter- Forgot Password Link');
                /*$msg = "Dear " . $name.' '.$userData['lname'];
                    $msg .= "You have Successfully Registered. You can login into your account once Admin approves your account within 24 hours!";*/

                $this->email->message($msg);


                if ($this->email->send()) {
                    $forgotPassCheck = $this->Recruit_Model->forgotPass_check($userTokenCheck[0]['id']);

                    if ($forgotPassCheck) {
                        $vCode = ["verifyCode" => $token];
                        $this->Recruit_Model->forgotPass_update($vCode, $userTokenCheck[0]['id']);
                    } else {
                        $vCode = ["user_id" => $userTokenCheck[0]['id'], "verifyCode" => $token];
                        $this->Recruit_Model->forgotPass_insert($vCode);
                    }
                    //$data1['forgotsuccess'] = "";
                    $this->session->set_tempdata('forgotsuccess', 'We have sent the verification link and code to your registered email Id. Please click on the link to change your password.', 1);
                    $this->load->view('recruiter/login');
                } else {
                    $data1['forgoterror'] = "Email not send";
                    $this->load->view('recruiter/login', $data1);
                }
            } else {
                $data1['forgoterror'] = "Please enter registered email";
                $this->load->view('recruiter/login', $data1);
            }
        } else {
            redirect('recruiter/recruiter/index');
        }
    }

    public function passwordChange()
    {
        $this->load->view('recruiter/forgot');
    }

    public function resetPassword()
    {
        $userData = $this->input->post();
        $encrypt_id = $userData['id'];
        //$dec_id = $this->encryption->decrypt($encrypt_id);
        //$data = ["id" => $encrypt_id];

        /*$userTokenCheck = $this->Recruit_Model->email_match($data);
        
        if($userTokenCheck) {*/


        $forgotPassCheck = $this->Recruit_Model->forgotPass_check($encrypt_id);
        //echo $this->db->last_query();die;
        //print_r($forgotPassCheck);die;
        $this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|callback_valid_password');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[newpassword]');
        $this->form_validation->set_rules('verifyCode', 'Verify Code', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data1['errors'] = $this->form_validation->error_array();
            $data1['id'] = $encrypt_id;
            $data1['userData'] = $userData;
            $this->load->view('recruiter/forgot', $data1);
        } else {
            $newPass = $userData['newpassword'];
            //echo $newPass;die;
            $verifyCode = $userData['verifyCode'];
            if ($forgotPassCheck[0]['verifyCode'] == $verifyCode) {
                $hash = $this->encryption->encrypt($newPass);
                $data = ["password" => $hash];
                $this->Recruit_Model->password_update($data, $encrypt_id);
                $vCode = ["verifyCode" => " "];
                $this->Recruit_Model->forgotPass_update($vCode, $encrypt_id);
                redirect("recruiter/recruiter/loginPage");
            } else {
                $data1['forgoterror'] = "Verification code incorrect";
                $data1['id'] = $encrypt_id;
                $data1['userData'] = $userData;
                $this->load->view("recruiter/forgot", $data1);
            }
        }
        /*} else{
            $data1['forgoterror'] = "Email id does not match";
            $data1['id'] = $encrypt_id;
            $this->load->view("recruiter/forgot", $data1);
        }*/
    }

    public function loginPage()
    {
        $this->session->set_tempdata('changesuccess', 'Password changed sucessfully', 1);
        $this->load->view('recruiter/login');
    }

    public function addExperience()
    {
        $userSession = $this->session->userdata('userSession');
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $data['expData'] = $this->Recruit_Model->recruiter_exp_fetch($userSession['id']);

        if (isset($_GET['id'])) {
            $idd = $_GET['id'];
            $data['expSingle'] = $this->Recruit_Model->recruiter_exp_singlefetch($idd, $userSession['id']);
        }
        $this->load->view('recruiter/addExp', $data);
    }

    public function experienceInsert()
    {
        $expData = $this->input->post();
        $userSession = $this->session->userdata('userSession');
        $data = ["recruiter_id" => $userSession['id'], "comp_id" => $expData['recruiter_id'], "exp" => $expData['exp']];
        $this->Recruit_Model->recruiter_exp_insert($data);
        redirect("recruiter/recruiter/addExperience");
    }

    public function experienceUpdate()
    {
        $expData = $this->input->post();
        $data = ["comp_id" => $expData['recruiter_id'], "exp" => $expData['exp']];
        $this->Recruit_Model->recruiter_exp_update($data, $expData['eid']);
        //echo $this->db->last_query(); die;
        redirect("recruiter/recruiter/addExperience");
    }

    public function experienceDelete()
    {
        $expData = $this->input->get();
        $this->Recruit_Model->recruiter_exp_delete($expData['id']);
        // echo $this->db->last_query();die;
        redirect("recruiter/recruiter/addExperience");
    }

    public function deleteJob()
    {
        $id = $this->input->get('id');
        $this->Jobpost_Model->deleteJob($id);
    }

    public function deleteimg()
    {
        $id = $this->input->post('id');
        $this->Recruit_Model->deletesiteimg($id);
    }

    public function deleteSite()
    {
        $id = $this->input->get('id');
        $this->Recruit_Model->site_delete($id);
        $this->Recruit_Model->deleteimg($id);
        $this->Recruit_Model->sitedetails_delete($id);
        $this->Recruit_Model->recruiter_toppicks_delete($id);
        $this->Recruit_Model->recruiter_allowances_delete($id);
        $this->Recruit_Model->recruiter_medical_delete($id);
        $this->Recruit_Model->job_delete($id);
        $this->Recruit_Model->ad_delete($id);
        $this->Recruit_Model->notification_delete($id);
        $this->Recruit_Model->review_delete($id);
        $this->Recruit_Model->special_profile_delete($id);
        $this->Recruit_Model->success_stories_delete($id);
        redirect('recruiter/recruiter/companyprofile');
    }

    public function transaction()
    {
        $userSession = $this->session->userdata('userSession');
        $data['invoices'] = $this->Recruit_Model->get_invoices_data($userSession['id']);
        $this->load->view('recruiter/transaction', $data);
    }

    public function glassdoor()
    {
        $this->load->view('recruiter/glassdoor');
    }

    public function rating()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['review_list'] = $this->Jobpost_Model->review_lists($userSession['id']);

        $companySites = $this->Recruit_Model->company_sites($userSession['id']);
        $compData = array();

        foreach ($companySites as $companySite) {
            $cid = $companySite['id'];

            $reviewlists = $this->Jobpost_Model->review_lists($cid);
            $average_rating = $this->Jobpost_Model->fetch_companyRating($cid);
            $company_rating = $this->Jobpost_Model->fetch_gcompanyRating($cid);
            if (!empty($company_rating[0]['rating'])) {
                $company_rating[0]['rating'] = $company_rating[0]['rating'];
            } else {
                $company_rating[0]['rating'] = '';
            }
            //print_r($company_rating);die;
            $compData[] = ["cname" => $companySite['cname'], "companyPic" => $companySite['companyPic'], "companyDesc" => $companySite['companyDesc'], "address" => $companySite['address'], "review" => $reviewlists, 'rating' => $average_rating[0]['average'], 'grating' => $company_rating[0]['rating']];
        }
        $data['comLists'] = $compData;
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $this->load->view('recruiter/rating', $data);
    }

    public function editSite()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $view = base64_decode($this->input->get('view'));
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        $data['companie'] = $this->Recruit_Model->company_single($view);
        $data['companyDetails'] = $this->Recruit_Model->company_details_single1($view);
        $data['companyImg'] = $this->Recruit_Model->company_images($view);
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["regions"] = $this->Common_Model->region_lists();
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $topPicks = $this->Recruit_Model->company_topicks_single($view);

        if ($topPicks) {
            $x1 = 0;
            foreach ($topPicks as $topPick) {
                $topPicks1[$x1] = $topPick['picks_id'];
                $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else {
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($view);
        if ($allowances) {
            $x1 = 0;
            foreach ($allowances as $allowance) {
                $allowance1[$x1] = $allowance['allowances_id'];
                $x1++;
            }
            $data['allowance2'] = $allowance1;
        } else {
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($view);
        if ($medicals) {
            $x1 = 0;
            foreach ($medicals as $medical) {
                $medical1[$x1] = $medical['medical_id'];
                $x1++;
            }
            $data['medical2'] = $medical1;
        } else {
            $data['medical2'] = [];
        }

        $leaves = $this->Recruit_Model->company_leaves_single($view);
        if ($leaves) {
            $x1 = 0;
            foreach ($leaves as $leave) {
                $leave1[$x1] = $leave['leaves_id'];
                $x1++;
            }
            $data['leave2'] = $leave1;
        } else {
            $data['leave2'] = [];
        }

        $workshifts = $this->Recruit_Model->company_workshifts_single($view);
        if ($workshifts) {
            $x1 = 0;
            foreach ($workshifts as $workshift) {
                $workshift1[$x1] = $workshift['workshift_id'];
                $x1++;
            }
            $data['workshift2'] = $workshift1;
        } else {
            $data['workshift2'] = [];
        }
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $this->load->view('recruiter/editSite', $data);
    }

    public function siteupdate()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        //print_r($_FILES);die;
        //      echo $count;die;
        $userSession = $this->session->userdata('userSession');
        $userData = $this->input->post();
        //print_r($userData);die;
        if (!empty($userData['comm_channel'])) {
            $channel = implode(',', $userData['comm_channel']);
        }
        //echo "<PRE>";

        $this->form_validation->set_rules('site_name', 'Site Name', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');

        $checkrecruitImg = $this->Recruit_Model->company_details_single1($userData['rid']);
        if (strlen($checkrecruitImg[0]['companyPic']) < 1) {

            if (empty($_FILES['profilePic']['name'])) {
                $this->form_validation->set_rules('profilePic', 'Site logo', 'trim|required');
            }
        }

        //$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        /*$this->form_validation->set_rules('recruiter_contact', 'Recruiter Contact', 'trim|required');
        $this->form_validation->set_rules('recruiter_email', 'Recruiter Email', 'trim|required');*/

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
            $data['companyDetails'] = $this->Recruit_Model->company_details_single1($userData['rid']);
            $data['companyImg'] = $this->Recruit_Model->company_images($userData['rid']);
            $topPicks = $this->Recruit_Model->company_topicks_single($userData['rid']);
            if ($topPicks) {
                $x1 = 0;
                foreach ($topPicks as $topPick) {
                    $topPicks1[$x1] = $topPick['picks_id'];
                    $x1++;
                }
                $data['topPicks2'] = $topPicks1;
            } else {
                $data['topPicks2'] = [];
            }

            $allowances = $this->Recruit_Model->company_allowances_single($userData['rid']);
            if ($allowances) {
                $x1 = 0;
                foreach ($allowances as $allowance) {
                    $allowance1[$x1] = $allowance['allowances_id'];
                    $x1++;
                }
                $data['allowance2'] = $allowance1;
            } else {
                $data['allowance2'] = [];
            }

            $medicals = $this->Recruit_Model->company_medical_single($userData['rid']);
            if ($medicals) {
                $x1 = 0;
                foreach ($medicals as $medical) {
                    $medical1[$x1] = $medical['medical_id'];
                    $x1++;
                }
                $data['medical2'] = $medical1;
            } else {
                $data['medical2'] = [];
            }

            $leaves = $this->Recruit_Model->company_leaves_single($userData['rid']);
            if ($leaves) {
                $x1 = 0;
                foreach ($leaves as $leave) {
                    $leave1[$x1] = $leave['leaves_id'];
                    $x1++;
                }
                $data['leave2'] = $leave1;
            } else {
                $data['leave2'] = [];
            }

            $workshifts = $this->Recruit_Model->company_workshifts_single($userData['rid']);
            if ($workshifts) {
                $x1 = 0;
                foreach ($workshifts as $workshift) {
                    $workshift1[$x1] = $workshift['workshift_id'];
                    $x1++;
                }
                $data['workshift2'] = $workshift1;
            } else {
                $data['workshift2'] = [];
            }
            $data['industryLists'] = $this->Common_Model->industry_lists();
            $comp_id = $userData['rid'];
            $this->load->view("recruiter/editSite", $data);
        } else {

            $data1 = ["cname" => $userData['site_name']];

            $recruiterInserted = $this->Recruit_Model->recruiter_stepone_update($data1, $userData['rid']);

            /*if($recruiterInserted) {*/

            if ($_FILES['profilePic']['tmp_name']) {
                $config = array(
                    'upload_path' => "./recruiterupload/",
                    'allowed_types' => "jpg|png|jpeg",
                );
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('profilePic')) {
                    $data = array('upload_data' => $this->upload->data());
                    $this->resizeImage($data['upload_data']['file_name']);
                    $picPath = base_url() . "recruiterupload/" . $data['upload_data']['file_name'];
                } else {
                    $picPath = " ";
                }
            } else {
                $picPath = " ";
            }
            $addLatLong = $this->getLatLong($userData['address']);

            if ($picPath == " ") {
                $data2 = [
                    "site_name" => $userData['site_name'],
                    "dayfrom" => $userData['dayfrom'],
                    "dayto" => $userData['dayto'],
                    "from_time" => $userData['from_time'],
                    "to_time" => $userData['to_time'],
                    "rphonecode" => '',
                    "recruiter_contact" => '',
                    "comm_channel" => '',
                    "recruiter_email" => '',
                    "phonecode" => '',
                    "phone" => '',
                    "address" => $userData['address'],
                    "city" => $addLatLong['city'],
                    "region" => $userData['region'],
                    "latitude" => $addLatLong['latitude'],
                    "longitude" => $addLatLong['longitude'],
                    "rating" => '',
                    "gross_salary" => '',
                    "basic_salary" => '',
                    "work_off" => '',
                    "annual_leave" => '',
                    "job_type" => '',
                    "companyDesc" => $userData['compDesc'],
                    //"industry" => $userData['industry']
                ];
            } else {
                $data2 = [
                    "site_name" => $userData['site_name'],
                    "dayfrom" => $userData['dayfrom'],
                    "dayto" => $userData['dayto'],
                    "from_time" => $userData['from_time'],
                    "to_time" => $userData['to_time'],
                    "rphonecode" => '',
                    "phonecode" => '',
                    "phone" => '',
                    "address" => $userData['address'],
                    "city" => $addLatLong['city'],
                    "region" => $userData['region'],
                    "latitude" => $addLatLong['latitude'],
                    "longitude" => $addLatLong['longitude'],
                    "rating" => '',
                    "gross_salary" => '',
                    "basic_salary" => '',
                    "work_off" => '',
                    "annual_leave" => '',
                    "job_type" => '',
                    "recruiter_contact" => '',
                    "recruiter_email" => '',
                    "companyDesc" => $userData['compDesc'],
                    "companyPic" => $picPath,
                    //"industry" => $userData['industry']
                ];
            }

            $this->Recruit_Model->recruiter_steptwo_update($data2, $userData['rid']);
            //echo $this->db->last_query();die;
            $count = count($_FILES['site_image']['name']);

            if ($count > 0) {

                for ($i = 0; $i < $count; $i++) {
                    $_FILES['file']['name'] = $_FILES['site_image']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['site_image']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['site_image']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['site_image']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['site_image']['size'][$i];

                    // Set preference
                    $config['upload_path'] = "./recruiterupload/";
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';

                    // $config['max_size'] = '50000'; // max_size in kb
                    $config['file_name'] = $_FILES['site_image']['name'][$i];

                    //Load upload library
                    $this->load->library('upload', $config);

                    // File upload
                    if ($this->upload->do_upload('file')) {
                        // Get data about the file
                        $uploadData = $this->upload->data();
                        //$this->resizeImage($uploadData['file_name']);
                        $filename = base_url() . "recruiterupload/" . $uploadData['file_name'];
                    }
                    if (!empty($filename)) {
                        $data1 = ["recruiter_id" => $userData['rid'], "pic" => $filename];
                        $this->Recruit_Model->site_image_insert($data1);
                    }
                }
            }
            if ($userData['toppicks']) {
                $topPicks = $userData['toppicks'];
                $x = 0;

                foreach ($topPicks as $topPick) {
                    $dataPicks[$x] = ["recruiter_id" => $userData['rid'], "picks_id" => $topPick];
                    $x++;
                }
                $this->Recruit_Model->recruiter_toppicks_delete($userData['rid']);
                $this->Recruit_Model->recruiter_toppicks_insert($dataPicks);
            } else {
                $this->Recruit_Model->recruiter_toppicks_delete($userData['rid']);
            }
            if ($userData['allowances']) {
                $topAllos = $userData['allowances'];
                $x = 0;
                foreach ($topAllos as $topAllo) {
                    $dataAllos[$x] = ["recruiter_id" => $userData['rid'], "allowances_id" => $topAllo];
                    $x++;
                }
                $this->Recruit_Model->recruiter_allowances_delete($userData['rid']);
                $this->Recruit_Model->recruiter_allowances_insert($dataAllos);
            } else {
                $this->Recruit_Model->recruiter_allowances_delete($userData['rid']);
            }
            if ($userData['medical']) {
                $topMedis = $userData['medical'];
                $x = 0;
                foreach ($topMedis as $topMedi) {
                    $dataMedis[$x] = ["recruiter_id" => $userData['rid'], "medical_id" => $topMedi];
                    $x++;
                }
                $this->Recruit_Model->recruiter_medical_delete($userData['rid']);
                $this->Recruit_Model->recruiter_medical_insert($dataMedis);
            } else {
                $this->Recruit_Model->recruiter_medical_delete($userData['rid']);
            }
            if ($userData['leavs']) {
                $topLeaves = $userData['leavs'];
                $x = 0;
                foreach ($topLeaves as $topLeave) {
                    $dataLeaves[$x] = ["recruiter_id" => $userData['rid'], "leaves_id" => $topLeave];
                    $x++;
                }
                $this->Recruit_Model->recruiter_leaves_delete($userData['rid']);
                $this->Recruit_Model->recruiter_leaves_insert($dataLeaves);
            }

            if ($userData['shifts']) {
                $topShifts = $userData['shifts'];
                $x = 0;
                foreach ($topShifts as $topShift) {
                    $dataShifts[$x] = ["recruiter_id" => $userData['rid'], "workshift_id" => $topShift];
                    $x++;
                }
                $this->Recruit_Model->recruiter_shifts_delete($userData['view']);
                $this->Recruit_Model->recruiter_shifts_insert($dataShifts);
            }

            $this->session->set_tempdata('inserted', 'Site details updated successfully', 5);
            redirect("recruiter/recruiter/companyprofile");
            /*} else {
                redirect("recruiter/recruiter/editSite");
            }*/
        }
    }

    public function recruiterprofile()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }
        if (!empty($subrecruiter_id)) {
            $data['companie'] = $this->Recruit_Model->company_single($subrecruiter_id);
        } else {
            $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
        }

        //echo $this->db->last_query();die;
        //print_r($data['companie']);die;
        if (!empty($subrecruiter_id)) {
            $data['companyDetails'] = $this->Recruit_Model->company_details_single($subrecruiter_id);
        } else {
            $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
        }

        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data['candidatesApplied'] = $this->fetchpending($userSession['id'], $subrecruiter_id);
        $this->load->view("recruiter/editprofile", $data);
    }

    public function profileupdate()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        $userData = $this->input->post();
        //print_r($userData);die;
        if (!empty($userData['comm_channel'])) {
            $channel = implode(',', $userData['comm_channel']);
        } else {
            $channel = "";
        }
        $this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[25]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phonecode', 'PhoneCode', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric|min_length[10]');
        $this->form_validation->set_rules('landline', 'Landline', 'min_length[6]');
        $this->form_validation->set_message('phone', 'Phone Number must contain at least 10 numbers');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
            $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $this->load->view('recruiter/editprofile', $data);
        } else {
            $name = $userData['name'];
            $name = explode(" ", $name);
            $data1 = ["fname" => $name[0], "lname" => $name[1], "email" => $userData['email']];
            $recruiterInserted = $this->Recruit_Model->recruiter_stepone_update($data1, $userSession['id']);
            // echo $this->db->last_query();die;
            $config = array(
                'upload_path' => "./recruiterupload/",
                'allowed_types' => "jpg|png|jpeg",
                //'max_size' => '1024',
                //'max_width' => '688',
                //'max_height' => '294',
            );
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('profilePic')) {

                $data = array('upload_data' => $this->upload->data());
                $this->resizeImage($data['upload_data']['file_name']);
                $picPath = base_url() . "recruiterupload/" . $data['upload_data']['file_name'];
            } else {
                $picPath = "";
            }
            if (!empty($picPath)) {
                $data2 = ["phonecode" => $userData['phonecode'], "phone" => $userData['phone'], "landline" => $userData['landline'], "companyPic" => $picPath, "comm_channel" => $channel];
            } else {
                $data2 = ["phonecode" => $userData['phonecode'], "phone" => $userData['phone'], "landline" => $userData['landline'], "comm_channel" => $channel];
            }
            $this->Recruit_Model->recruiter_steptwo_update($data2, $userSession['id']);
            $this->session->set_tempdata('inserted', 'Profile updated successfully', 5);
            redirect("recruiter/recruiter/recruiterprofile");
        }
    }

    public function changePassword()
    {
        if ($this->session->userdata('userSession')) { } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        $userData = $this->input->post();
        //$userData = $this->input->post();
        $this->form_validation->set_rules('password', 'New Password', 'trim|required|callback_valid_password');
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
            $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
            $this->load->view('recruiter/editprofile', $data);
        } else {
            $data = ["email" => $userSession['email']];
            $recruiterCheck = $this->Recruit_Model->recruiter_login($data);

            if ($recruiterCheck) {
                $hash = $this->encryption->encrypt($userData['password']);
                $data = ["password" => $hash];
                $this->Recruit_Model->password_update($data, $recruiterCheck[0]['id']);
                $this->session->set_tempdata('item', 'Password Changed Successfully', 2);
                redirect("recruiter/recruiter/recruiterprofile");
            } else {
                redirect("recruiter/recruiter/recruiterprofile");
            }
        }
    }

    public function recruiter_location()
    {
        $recruiter_id = $this->input->post('recruiter_id');
        //echo $recruiter_id;die;
        /*$data['companie'] = $this->Recruit_Model->company_single($recruiter_id);
        $data['companyDetails'] = $this->Recruit_Model->company_details_single($recruiter_id);
        $data['companySites'] = $this->Recruit_Model->company_sites($recruiter_id);*/
        $topPicks = $this->Recruit_Model->company_topicks_single($recruiter_id);
        //var_dump($topPicks);die;
        if ($topPicks) {
            $x1 = 0;
            foreach ($topPicks as $topPick) {
                $topPicks1[$x1] = $topPick['picks_id'];
                $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else {
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($recruiter_id);
        if ($allowances) {
            $x1 = 0;
            foreach ($allowances as $allowance) {
                $allowance1[$x1] = $allowance['allowances_id'];
                $x1++;
            }
            $data['allowance2'] = $allowance1;
        } else {
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($recruiter_id);
        if ($medicals) {
            $x1 = 0;
            foreach ($medicals as $medical) {
                $medical1[$x1] = $medical['medical_id'];
                $x1++;
            }
            $data['medical2'] = $medical1;
        } else {
            $data['medical2'] = [];
        }

        $leaves = $this->Recruit_Model->company_leaves_single($recruiter_id);
        if ($leaves) {
            $x1 = 0;
            foreach ($leaves as $leave) {
                $leave1[$x1] = $leave['leaves_id'];
                $x1++;
            }
            $data['leave2'] = $leave1;
        } else {
            $data['leave2'] = [];
        }

        $workshifts = $this->Recruit_Model->company_workshifts_single($recruiter_id);
        if ($workshifts) {
            $x1 = 0;
            foreach ($workshifts as $workshift) {
                $workshift1[$x1] = $workshift['workshift_id'];
                $x1++;
            }
            $data['workshift2'] = $workshift1;
        } else {
            $data['workshift2'] = [];
        }

        echo json_encode($data);
    }


    public function sendSms()
    {

        $this->load->library('twilio');
        $sms_sender = trim($this->input->post('sms_sender'));
        $sms_reciever = $this->input->post('sms_recipient');
        $sms_message = trim($this->input->post('sms_message'));
        $from = '+' . $sms_sender; //trial account twilio number
        $to = '+' . $sms_reciever; //sms recipient number
        $response = $this->twilio->sms("+12014823386", "+91 821 852 4174", "hello");
        print_r($response);
        if ($response->IsError) {

            echo 'Sms Has been Not sent';
        } else {

            echo 'Sms Has been sent';
        }
    }


    function getToken()
    {
        $string = "";
        $chars = "0123456789";
        for ($i = 0; $i < 4; $i++)
            $string .= substr($chars, (rand() % (strlen($chars))), 1);
        return $string;
    }


    public function upload()
    {

        //print_r($_FILES);die;
        if (isset($_POST["image"])) {
            $data = $_POST["image"];

            //echo getimagesize($data);die;
            $image_array_1 = explode(";", $data);


            $image_array_2 = explode(",", $image_array_1[1]);


            $data = base64_decode($image_array_2[1]);

            $imageName = time() . '.png';

            file_put_contents($imageName, $data);
            echo base_url() . $imageName;
        }
    }

    public function valid_password($password = '')
    {
        $password = trim($password);

        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

        if (empty($password)) {
            $this->form_validation->set_message('valid_password', 'Password is required.');

            return FALSE;
        }

        if (preg_match_all($regex_lowercase, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets, numeric and special characters.');

            return FALSE;
        }

        if (preg_match_all($regex_uppercase, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets, numeric and special characters.');

            return FALSE;
        }

        if (preg_match_all($regex_number, $password) < 1) {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets, numeric and special characters.');

            return FALSE;
        }

        /*if (preg_match_all($regex_special, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be a combination of alphanumeric and special characters.');

            return FALSE;
        }*/

        /*if (strlen($password) < 5)
        {
            $this->form_validation->set_message('valid_password', 'Password must be at least 5 characters in length.');

            return FALSE;
        }

        if (strlen($password) > 32)
        {
            $this->form_validation->set_message('valid_password', 'Password cannot exceed 32 characters in length.');

            return FALSE;
        }*/

        return TRUE;
    }

    function alpha_dash_space($str_in = '')
    {
        $str_in = trim($str_in);
        if (empty($str_in)) {
            $this->form_validation->set_message('alpha_dash_space', 'Name field is required.');

            return FALSE;
        }
        if (!preg_match("/^([a-z, ])+$/i", $str_in)) {
            $this->form_validation->set_message('alpha_dash_space', 'The name field may only contain characters and spaces.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function fetchpending($userSession, $subrecruiter_id)
    {
        $data["pendingData"] = [];
        $pending = $this->Candidate_Model->candidateduenotification_list($userSession, $subrecruiter_id);
        if (!empty($pending)) {
            foreach ($pending as $pendings) {
                //echo $pendings['updated_at'];die;
                $enddate =  date('Y-m-d', strtotime($pendings['updated_at'] . ' + 3 days'));
                //echo $enddate;die;
                $enddate = strtotime($enddate);
                $updated_date =  strtotime($pendings['updated_at']);
                $now = time();
                $datediff =  $now - $updated_date;

                $daydiff = floor($datediff / (60 * 60 * 24));
                //echo $daydiff;die;
                if ($daydiff >= 3) {
                    $data["pendingData"][] = ["profilePic" => $pendings['profilePic'], "user_id" => $pendings['user_id'], "name" => $pendings['name'], "email" => $pendings['email'], 'phone' => $pendings['phone'], "location" => $pendings['location'], "status" => $pendings['status'], "interviewdate" => $pendings['interviewdate'], 'interviewtime' => $pendings['interviewtime'], "date_day1" => $pendings['date_day1'], 'jobDesc' => $pendings['jobDesc'], "jobtitle" => $pendings['jobtitle'], "compid" => $pendings['compid'], "id" => $pendings['id'], 'user_id' => $pendings['user_id'], 'jobpost_id' => $pendings['jobpost_id'], 'updated_at' => $pendings['updated_at'], 'fallout_reason' => $pendings['fallout_reason']];
                }
            }
        }
        return $data["pendingData"];
    }


    public function transfer()
    {

        $userSession = $this->session->userdata('userSession');
        if (!empty($this->input->get('status'))) {
            $status = $this->input->get('status');
        } else {
            $status = 'Active';
        }

        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }

        $data['recruittype'] = 0;
        $data['recruitFetch'] = $this->Recruit_Model->subrecruiters($userSession['id']);

        $recruiterCheck = $this->Recruit_Model->recruiter_transferfetch($userSession['id']);


        if ($recruiterCheck) {
            $data['recruiterCheck'] = 1;
        } else {
            $data['recruiterCheck'] = 0;
        }
        $this->load->view('recruiter/transferrecruiter', $data);
    }

    public function transfersubmit()
    {

        $userSession = $this->session->userdata('userSession');
        if (!empty($this->input->get('status'))) {
            $status = $this->input->get('status');
        } else {
            $status = 'Active';
        }
        if (!empty($userSession['label']) && $userSession['label'] == '3') {
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        } else {
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id = 0;
        }

        $userData = $this->input->post();

        $this->form_validation->set_rules('transfertype', 'Transfer Type', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['recruitFetch'] = $this->Recruit_Model->subrecruiters($userSession['id']);
            $this->load->view('recruiter/transferrecruiter', $data);
        } else {

            if ($userData['transfertype'] == 1) {
                $this->form_validation->set_rules('jobassign', 'sub-recruiter', 'trim|required');
            } else {
                $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
                $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[recruiter.email]');
                $this->form_validation->set_message('is_unique', 'Email is already registered');
                $this->form_validation->set_rules('phone', 'Contact Number', 'trim|required');
            }

            if ($this->form_validation->run() == FALSE) {
                $data['errors'] = $this->form_validation->error_array();
                $data['recruitFetch'] = $this->Recruit_Model->subrecruiters($userSession['id']);
                $data['recruittype'] = $userData['transfertype'];
                $this->load->view('recruiter/transferrecruiter', $data);
            } else {

                $recru = $this->Recruit_Model->recruiter_single($userSession['id']);

                if ($userData['transfertype'] == 1) {
                    $data = [
                        "request_by" => $userSession['id'],
                        "nameby" => $recru[0]['fname'] . ' ' . $recru[0]['lname'],
                        "emailby" => $recru[0]['email'],
                        "cname" => $recru[0]['cname'],
                        "request_type" => $userData['transfertype'],
                        "request_to" => $userData['jobassign']
                    ];

                    $recru11 = $this->Recruit_Model->recruiter_single($userData['jobassign']);
                    $data1['super'] = $data;
                    $recruDetails = $this->Recruit_Model->company_details_single($userData['jobassign']);
                    $data1['subsuper'] = [
                        "name" => $recru11[0]['fname'] . '' . $recru11[0]['lname'],
                        "email" => $recru11[0]['email'],
                        "phone" => $recruDetails[0]['phone'],
                        "cname" => $recru[0]['cname']
                    ];
                } else {

                    $domain = $userData['domain'];
                    $domain1 = substr($domain, strpos($domain, ".") + 1);;
                    $email1 = $userData['email'];
                    $email2 = explode('@', $email1);

                    if (strtolower($domain1) == strtolower($email2[1])) {

                        $data = [
                            "request_by" => $userSession['id'],
                            "nameby" => $recru[0]['fname'] . ' ' . $recru[0]['lname'],
                            "emailby" => $recru[0]['email'],
                            "cname" => $recru[0]['cname'],
                            "request_type" => $userData['transfertype'],
                            "fname" => $userData['fname'],
                            "lname" => $userData['lname'],
                            "email" => $userData['email'],
                            "phone" => $userData['phone']
                        ];
                        $data1['super'] = $data;
                        $data1['subsuper'] = [
                            "name" => $userData['fname'] . ' ' . $userData['lname'],
                            "email" => $userData['email'],
                            "phone" => $userData['phone'],
                            "cname" => $recru[0]['cname']
                        ];
                    } else {

                        $data['errors'] = ["email" => "Domain and Email domain did not match"];
                        $data['recruitFetch'] = $this->Recruit_Model->subrecruiters($userSession['id']);
                        $data['recruittype'] = $userData['transfertype'];
                        $this->load->view('recruiter/transferrecruiter', $data);
                    }
                }
                $this->Recruit_Model->recruiter_transfer($data);  //insert

                $config = [
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtpout.asia.secureserver.net',
                    'smtp_user' => 'help@jobyoda.com',
                    'smtp_pass' => 'Usa@1234567',
                    'smtp_port' => 465,
                    'smtp_crypto' => 'ssl',
                    'charset' => 'utf-8',
                    'mailtype' => 'html',
                    'crlf' => "\r\n",
                    'newline' => "\r\n"
                ];
                $msg = $this->load->view('transferemail', $data1, TRUE);  //mailing -1

                $this->load->library('email');

                $subject = "Request for Super User Transfer";
                $this->email->initialize($config);
                $this->email->from("help@jobyoda.com", "JobYoDA");
                $this->email->to($data1['super']['emailby']);
                $this->email->subject($subject);
                $this->email->message($msg);

                if ($this->email->send()) {
                    $this->email->initialize($config);
                    $subject = "JobYoDA Transfer of Ownership Request";
                    $this->email->from("help@jobyoda.com", "JobYoDA");
                    $this->email->to($data1['subsuper']['email']);
                    $this->email->subject($subject);
                    $msg1 = $this->load->view('transfersubemail', $data1, TRUE);  //mailing -2
                    $this->email->message($msg1);

                    if ($this->email->send()) {
                        $this->session->set_tempdata('item', 'Request submitted Successfully', 5);
                        redirect("recruiter/recruiter/transfer");
                    } else {
                        $this->session->set_tempdata('postError', 'second Mail not send', 2);
                        redirect("recruiter/recruiter/transfer");
                    }
                } else {
                    $this->session->set_tempdata('postError', 'Mail not send', 2);
                    redirect("recruiter/recruiter/transfer");
                }
            }
        }
    }
}
