<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Bucket extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->library('Aws3');

    }

    public function show(){
        $this->load->view('bucket');
    }

    public function bucketUpload(){
       
        //$filesCount = count($_FILES['files']['name']);


        //echo "hello";die;
        $filename="";
        if($_FILES){
        for($i = 0; $i < $filesCount; $i++){
        $_FILES['file']['name'] = $_FILES['files']['name'][$i];
        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
        $_FILES['file']['size'] = $_FILES['files']['size'][$i];

        // File upload configuration
        $uploadPath = 'post_video/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'jpg|jpeg|png|gif|mp4|avi|3gp|avi|webm';
        $config['file_name'] = random_string('alnum',90);

        $temp = explode(".", $_FILES['files']['name'][$i]);
        $filename = round(microtime(true)) . '.' . end($temp);
        $fileUpload = [
        "name"=>$filename,
        "type"=>$_FILES['files']['type'][$i],
        "tmp_name"=>$_FILES['files']['tmp_name'][$i],
        "size"=>$_FILES['files']['size'][$i],
        "error"=>$_FILES['files']['error'][$i],
        ];

        $uploadData[$i]['file_name'] = $this->aws3->sendFile('jobyoda',$fileUpload);
        }
    }
}


    }
   