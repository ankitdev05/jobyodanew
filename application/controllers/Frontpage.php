<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittall
 * @license         Mobulous
 */
class FrontPage extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Newpoints_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->library('google');
        $this->load->library('Facebook');
       
     }

    public function aboutUs() {
        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data["states"] = $this->User_Model->getownstate();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);

        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data['content'] = $this->Common_Model->about_us();

        $data['meta_title'] = "BPO Industry Job Site | Find Jobs Philippines | Jobyoda About Us";
        $data['meta_description'] = "JobYoDa is a BPO Job Platform that helps jobseekers find the best BPO jobs in the Philippines, allowing people to search jobs based on their location. Apply now!";
        
        $this->load->view('aboutUs',$data);
    }

    public function terms() {
        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data["states"] = $this->User_Model->getownstate();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);

        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data['content'] = $this->Common_Model->about_us();

        $data['meta_title'] = "Jobyoda Terms and Conditions  | Jobyoda Dream Jobs Philippines";
        $data['meta_description'] = "This page states the terms of use under which users may use the JobYoDa website and application. Visit JobYoDa and find the best BPO jobs online. Apply now!";
        
        $this->load->view('terms',$data);
    }

    public function privacy() {
        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data["states"] = $this->User_Model->getownstate();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);

        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
    	$data['content'] = $this->Common_Model->about_us();

        $data['meta_title'] = "Jobyoda Privacy Policy | Jobyoda Website Philippines";
        $data['meta_description'] = "JobYoDa places cookies on users' computers and other devices to help jobseekers save their preferences in finding the best BPO jobs available online. Apply now!";

    	$this->load->view('privacy',$data);
    }

    public function faq() {

        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data["states"] = $this->User_Model->getownstate();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();

        $data['meta_title'] = "Frequently Asked Questions | BPO Jobs Informations | Jobyoda FAQ";
        $data['meta_description'] = "Find answers to your Frequently Asked Questions about any BPO job placement with JobYoDa, helping jobseekers find the best BPO jobs in the Philippines. Apply now!";

        $this->load->view('faqpage', $data);
    }

    public function howitworks() {

        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();
        $data["states"] = $this->User_Model->getownstate();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();        

        $company_list = $this->Common_Model->company_lists();
        if ($company_list) {
            $x = 0;
            foreach ($company_list as $company_lists) {
                $checkjob = $this->Common_Model->check_jobs($company_lists['id']);
                if ($checkjob) {
                    $company_lists1[$x] = ["id" => $company_lists['id'], "cname" => $company_lists['cname']];
                    $x++;
                }
            }
        } else {
            $company_lists1 = [];
        }
        
        $data['companynamelist'] = $company_lists1;
        $data['companylists'] = $company_list;
        $data['openings'] = $this->Jobpost_Model->job_openings();
        $data['userlist'] = $this->Common_Model->user_list();

        $data['meta_title'] = "How it works | Jobyoda Job Seeker | Jobyoda Job Recruiter";
        $data['meta_description'] = "Find best solutions in finding the best jobs based on any location in the Philippines with JobYoDa, helping jobseekers find the best BPO jobs available today. Apply now!";

        $this->load->view('howitworks', $data);
    }

}
?>