<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Dashboard extends CI_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Newpoints_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        if ($this->session->userdata('usersess')) {
        } else {
            redirect("user/index");
        }
    }
    public function index() {
        $userSess = $this->session->userdata('usersess');
        if (!empty($userSess)) {
            $dataToken = ["id" => $userSess['id']];
            $userInfo = $this->User_Model->user_match($dataToken);
            $jobApplyfetch = $this->Jobpost_Model->job_fetch_applied($userInfo[0]['id']);
            //print_r($jobApplyfetch);die;
            if($jobApplyfetch) {
                    
              foreach ($jobApplyfetch as $jobfetch) {

                  $jobApplyfetchcheck = $this->Jobpost_Model->job_fetch_appliedCheck11($jobfetch['jobpost_id'],$userInfo[0]['id']);
                  if($jobApplyfetchcheck) {
                      
                     $data["jobList"] = [];

                  } else{

                     $companyName = $this->Jobpost_Model->fetch_companyName($jobfetch['recruiter_id']);

                      $jobDate = strtotime($jobfetch['updated_at']);
                      //echo $jobDate;die;
                      $datediff = $now - $jobDate;
                      $daydiff = floor($datediff / (60 * 60 * 24));
                      //$hourdiff = round(($now - $jobDate)/3600, 1);
                      //echo $daydiff;die;
                      if($daydiff >= 2) {
                           $data["jobList"][] = [
                                "jobpost_id" => $jobfetch['jobpost_id'],
                                "job_title" => $jobfetch['jobtitle'],
                                "companyName" => $companyName[0]['cname'],
                                "companyId" => $jobfetch['recruiter_id'],
                                "msg" => "open popup",
                                "name" => $userInfo[0]['name']
                            ];
                            
                      } else {
                          $data["jobList"] = [];
                      }
                  }
              }
                
            } else{
                $data["jobList"] = [];
            }
            $data['profileDetail'] = $this->fetchUserSingleData($userInfo[0]['id']);
            $workData = $this->fetchWorkSingleData($userInfo[0]['id']);
            $eduData = $this->fetchEduSingleData($userInfo[0]['id']);
            $langData = $this->fetchLangSingleData($userInfo[0]['id']);
            $clientData = $this->fetchClientSingleData($userInfo[0]['id']);
            if (empty($clientData)) {
                $comProfile = ["topClient" => 0];
            }
            if (empty($workData)) {
                $comProfile = ["experience" => 0];
            }
            if (empty($eduData)) {
                $comProfile = ["education" => 0];
            }
            if (empty($langData)) {
                $comProfile = ["language" => 0];
            }
            if (!empty($comProfile)) {
                $this->User_Model->update_profileComplete($comProfile, $userInfo[0]['id']);
            }
        }
        $data['getCompletenes'] = $this->fetchUserCompleteData($userInfo[0]['id']);
        $data['category_list'] = $this->Common_Model->category_lists();
        $data['level_list'] = $this->Common_Model->level_lists();
        $data['jobtitle'] = $this->Common_Model->job_title_list();
        $data['industrylists'] = $this->Common_Model->industry_lists();
        $data['checkStatus'] = $this->User_Model->user_single($userInfo[0]['id']);
        /*$jobstatus = ['jobsearchstatus'=>$checkStatus[0]['jobsearch_status']];*/
        $this->load->view('dashboard', $data);
    }
    public function logout() {
        $this->session->unset_userdata('usersess');
        redirect("");
    }

    public function savedjob() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $condBYstatusjob = "saved_jobs.user_id='" . $userTokenCheck[0]['id'] . "' and saved_jobs.status = 1";
            $jobsavedData = $this->Jobpost_Model->JobSavedd_fetcH_byUseriD($condBYstatusjob);
            //echo "<pre>";
            // print_r($jobsavedData);die;
            if ($jobsavedData) {
                foreach ($jobsavedData as $jobsavedDatas) {
                    $jobImage = $this->Jobpost_Model->job_image($jobsavedDatas['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($jobsavedDatas['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $condtionssavedjob = array('jobpost_id' => $jobsavedDatas['id'], 'user_id' => $userTokenCheck[0]['id']);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobsavedDatas['company_id']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobsavedDatas['company_id']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobsavedDatas['id']);
                    if (!empty($jobTop)) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobsavedDatas['recruiter_id']);
	                    if(!empty($companyname[0]["cname"])){
	                        $cname = $companyname[0]["cname"];
	                    }else{
	                        $cname = '';
	                    }
	                    /*$ip = $_SERVER['REMOTE_ADDR'];
	                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
	                   $data_loc = json_decode($location);
	                   
	                   $Address = $data_loc->city;
	                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
	                   $output = json_decode($geocodeFromAddr);
	                      //print_r($output);die;       
	                   //Get latitude and longitute from json data
	                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
	                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
	                    $distance = $this->distance($current_lat,$current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
	                        $distance = number_format((float)$distance, 2, '.', '');
                    $data["savedjobss"][] = ["jobpost_id" => $jobsavedDatas['id'], "comapnyId" => $jobsavedDatas['company_id'], "recruiter_id" => $jobsavedDatas['recruiter_id'], "job_title" => $jobsavedDatas['jobtitle'], "jobDesc" => $jobsavedDatas['jobDesc'], "salary" => $basicsalary, "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData, "save_status" => $jobsstatus,"cname"=>$cname, "distance"=>$distance, "jobPitch"=>$jobsavedDatas['jobPitch'], "mode"=>$jobsavedDatas['mode'], "modeurl"=>$jobsavedDatas['modeurl']];
                }
            } else {
                $data["savedjobss"][] = ["jobpost_id" => "", "comapnyId" => "", "recruiter_id" => "", "job_title" => "", "jobDesc" => "", "salary" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "", "save_status" => "", "cname" => "", "distance" => "", "jobPitch" => "", "mode"=>"", "modeurl"=>""];
            }
            $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
            if($notification_list){
                $data['badge'] = count($notification_list);
             }
            $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
            $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
             $data['checkResume'] = $this->User_Model->resume_single($userTokenCheck[0]['id']);
            $this->load->view('savedjobs', $data);
        }
    }
    public function hotjob() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlong($userTokenCheck[0]['id'],$expfilter, $current_lat, $current_long);
            //print_r($hotjobData);die;
            //echo $this->db->last_query();die;
            if ($hotjobData) {
                foreach ($hotjobData as $hotjobsData) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                    if (!empty($jobTop) && count($jobTop) > 1) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                        $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $userTokenCheck[0]['id']);
                        $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobsavedStatus[0]['status'])) {
                            $jobsstatus = $jobsavedStatus[0]['status'];
                        } else {
                            $jobsstatus = '';
                        }
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                            
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
	                    if(!empty($companyname[0]["cname"])){
	                        $cname = $companyname[0]["cname"];
	                    }else{
	                        $cname = '';
	                    }
	                    /*$ip = $_SERVER['REMOTE_ADDR'];
	                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
	                   $data_loc = json_decode($location);
	                   
	                   $Address = $data_loc->city;
	                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
	                   $output = json_decode($geocodeFromAddr);
	                      //print_r($output);die;       
	                   //Get latitude and longitute from json data
	                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
	                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
	                    $distance = $this->distance($current_lat,$current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
	                        $distance = number_format((float)$distance, 2, '.', '');
                        $data["hotjobss"][] = ["jobpost_id" => $hotjobsData['id'],"jobPitch"=>$hotjobsData['jobPitch'], "comapnyId" => $hotjobsData['compId'], "recruiter_id" => $hotjobsData['recruiter_id'], "job_title" => $hotjobsData['jobtitle'], "jobDesc" => $hotjobsData['jobDesc'], "salary" => $hotjobsData['salary'], "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData, "save_status" => $jobsstatus,"cname"=>$cname,"distance"=>$distance, "mode"=>$hotjobsData['mode'], "modeurl"=>$hotjobsData['modeurl']];
                    }
                }
            } else {
                $data["hotjobss"][] = ["jobpost_id" => "", "comapnyId" => "", "recruiter_id" => "", "job_title" => "","jobPitch"=>"", "jobDesc" => "", "salary" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "", "save_status" => "","cname"=>"","distance"=>"", "mode"=>""];
            }
            $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
            if($notification_list){
                $data['badge'] = count($notification_list);
             }
            $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
            $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
            $data['checkResume'] = $this->User_Model->resume_single($userTokenCheck[0]['id']);
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data["nations"] = $this->Common_Model->nation_lists();
            $this->load->view('hotjobs', $data);
        }
    }
    public function notifications() {
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        //print_r($userTokenCheck);die;
        if ($userTokenCheck) {
            $notification_list = $this->Common_Model->getnotification($userTokenCheck[0]['id']);
            if (!empty($notification_list)) {
                foreach ($notification_list as $notification_lists) {
                    $companyName = $this->Jobpost_Model->company_detail_fetch($notification_lists['recruiter_id']);
                    $job_detail = $this->Jobpost_Model->job_fetch_appliedid($notification_lists['jobapp_id']);
                    //print_r($job_detail);
                    $reviews = $this->Jobpost_Model->review_listsbyuser($notification_lists['recruiter_id'],$userTokenCheck[0]['id']);
                    $story = $this->Jobpost_Model->story_listsbyuser($notification_lists['recruiter_id'],$userTokenCheck[0]['id']);
                    if($reviews){
                        $reviewstatus = '1';
                    }else{
                        $reviewstatus = '0';
                    }
                    if($story){
                        $storystatus = '1';
                    }else{
                        $storystatus = '0';
                    }
                    if (!empty($companyName[0]['cname'])) {
                        $companynamee = $companyName[0]['cname'];
                    } else {
                        $companynamee = "";
                    }
                    $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
                    if($notification_list){
                        $data['badge'] = count($notification_list);
                     }
                    $data['notification_listss'][] = ["id" => $notification_lists['id'], "notification" => $notification_lists['notification'], "status" => $notification_lists['status'], "recruiter_id" => $notification_lists['recruiter_id'], "job_status" => $notification_lists['job_status'], "companyname" => $companynamee, "date" => date("d-M-Y h:i", strtotime($notification_lists['createdon'])), "jobpost_id" =>  $job_detail[0]['jobpost_id'],"reviewstatus" => $reviewstatus, "storystatus" => $storystatus];
                }
            } else {
                $data['notification_listss'][] = ["id" => "", "notification" => "", "status" => "", "recruiter_id" => "", "job_status" => "", "companyname" => "", "date" => "", "jobpost_id" => "","reviewstatus" => "", "storystatus" => ""  ];
            }
            $promonotification_list = $this->Common_Model->getpromonotification($userTokenCheck[0]['id']);
            if (!empty($promonotification_list)) {
                foreach ($promonotification_list as $promonotification_lists) {
                    $data['promonotification_listss'][] = ["id" => $promonotification_lists['id'], "title" => $promonotification_lists['title'], "message" => $promonotification_lists['message']];
                }
            } else {
                $data['promonotification_listss'][] = ["id" => "", "title" => "", "message" => ""];
            }
            $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
            $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
            $this->load->view('notifications', $data);
        }
    }

    public function deleteNotification(){
        $id = $_GET['id'];
        //echo $id;die;
       $this->Jobpost_Model->notification_delete($id);
//echo $this->db->last_query();die;
        redirect('dashboard/notifications');
       
    }

    public function successstory() {
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $story_list = $this->Jobpost_Model->story_lists();
            //echo $this->db->last_query();die;
            if (!empty($story_list)) {
                $i = 0;
                foreach ($story_list as $story_lists) {
                    $like_story = $this->Jobpost_Model->liked_stories_count($story_lists['id']);
                    $story_list_count = $this->Jobpost_Model->comment_lists($story_lists['id']);
                    //  echo $this->db->last_query();die;
                    $like_story_status = $this->Jobpost_Model->liked_stories_status($story_lists['id'], $userTokenCheck[0]['id']);
                    if (!empty($like_story_status[0]['status'])) {
                        $like_status = $like_story_status[0]['status'];
                    } else {
                        $like_status = 0;
                    }
                    $data['story_listss'][] = ["story_id" => $story_lists['id'], "user_id" => $userTokenCheck[0]['id'], "user_name" => $story_lists['name'], "date" => date("y-m-d", strtotime($story_lists['createdon'])), "story" => $story_lists['story'], "recruiter_id" => $story_lists['recruiter_id'], "name" => $story_lists['name'], "profilePic" => $story_lists['profilePic'], "like_count" => count($like_story), "comment_count" => count($story_list_count), "like_status" => $like_status];
                    /*if(!empty($story_list_count))
                    {
                    foreach($story_list_count as $commentlist)
                    {
                    
                    $data['story_comments'][] = [
                          "comment_id" => $commentlist['id'],
                          "date" => date("y-m-d",strtotime($commentlist['createdon'])),
                          "comments" => $commentlist['comments'],
                          "name" => $commentlist['name'],
                          "profilePic" => $commentlist['profilePic']                    
                    
                      ];
                      
                    
                    }
                    }*/
                    /*  else
                    {
                      $data['story_comments'][] = [
                         "comment_id"   => "",
                          "date"   => "", 
                          "comments"      => "",
                          "name"     => "",
                          "profilePic" => "",         
                    
                      ];
                       //$data['story_comments'][]=$data['story_comments'][$i];
                    }*/
                }
            } else {
                $data['story_listss'][] = ["story_id" => "", "user_id" => "", "date" => "", "story" => "", "recruiter_id" => "", "name" => "", "profilePic" => "", "like_count" => "", "comment_count" => "", "like_status" => ""];
            }
            //echo "<pre>";
            // print_r($data['story_comments']);die;
            $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
            if($notification_list){
                $data['badge'] = count($notification_list);
             }
            $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
            $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
            $this->load->view('sucesstory', $data);
        }
    }
    public function likedtory() {
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $storyId = $this->input->post('storyId');
        $storyLikeStatus = $this->input->post('like_status');
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $data['liked_stories'] = $this->Jobpost_Model->liked_stories($storyId, $userTokenCheck[0]['id']);
            if (empty($data['liked_stories'])) {
                $data = ["user_id" => $userTokenCheck[0]['id'], "story_id" => $storyId, "status" => 1];
                $story_like = $this->Jobpost_Model->like_story($data);
                if ($story_like) {
                    echo $like_story = count($this->Jobpost_Model->liked_stories_count($storyId));
                    // $like_story = $this->Jobpost_Model->liked_stories_count($storyId);
                    //echo $this->db->last_query();die;
                    
                }
            } else {
                if ($storyLikeStatus == 1) {
                    $status = '0';
                }
                if ($storyLikeStatus == 0) {
                    $status = '1';
                }
                $data = ["status" => $status];
                $story_like = $this->Jobpost_Model->unlike_story($userTokenCheck[0]['id'], $storyId, $data);
                if ($story_like) {
                    echo $like_story = count($this->Jobpost_Model->liked_stories_count($storyId));
                    //$like_story = $this->Jobpost_Model->liked_stories_count($storyId);
                    //echo $this->db->last_query();die;
                    
                }
            }
        }
    }
    public function addComment() {
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $storyId = $this->input->post('storyId');
        $comment = $this->input->post('comment');
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $data = ["user_id" => $userTokenCheck[0]['id'], "story_id" => $storyId, "comments" => $comment];
            $story_commnet = $this->Jobpost_Model->add_storycomment($data);
            if ($story_commnet) {
                echo $story_list_count = count($this->Jobpost_Model->comment_lists($storyId));
                //echo "<div class ='alert alert-success'>comments are added successfully</div>";
                
            }
        }
    }
    public function commentlistingStory() {
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $storyId = $this->input->post('storyId');
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $single_story = $this->Jobpost_Model->fetch_singlestory($storyId);
            $liked_stories = $this->Jobpost_Model->liked_stories_count($storyId);
            $story_list = $this->Jobpost_Model->comment_lists($storyId);
            $like_story_status = $this->Jobpost_Model->liked_stories_status($storyId, $userTokenCheck[0]['id']);
            if (!empty($like_story_status[0]['status'])) {
                $like_status = $like_story_status[0]['status'];
            } else {
                $like_status = "";
            }
            //print_r($liked_stories);die;
            $data['story_list'] = ["story" => $single_story[0]['story'], "date" => date("y-m-d", strtotime($single_story[0]['createdon'])), "name" => $single_story[0]['name'], "profilePic" => $single_story[0]['profilePic'], "comment_count" => count($story_list), "like_count" => count($liked_stories), "like_status" => $like_status];
            foreach ($story_list as $story_lists) {
                $data['story_comments'] = ["comment_id" => $story_lists['id'], "date" => date("y-m-d", strtotime($story_lists['createdon'])), "comments" => $story_lists['comments'], "name" => $story_lists['name'], "profilePic" => $story_lists['profilePic']];
            }
            echo "1";
            // $this->load->view('commentlist');
            
        }
    }
    public function myprofile($dataErr = null) {
        $userSess = $this->session->userdata('usersess');
        $dataToken = ["id" => $userSess['id']];
        $userInfo = $this->User_Model->user_match($dataToken);
        $data['profileDetail'] = $this->fetchUserSingleData($userInfo[0]['id']);
        $data['strengthdata'] = $this->User_Model->user_more($userInfo[0]['id']);
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data['languageData'] = $this->User_Model->lang_single($userInfo[0]['id']);
        $data['clientData'] = $this->User_Model->client_single($userInfo[0]['id']);
        $data['skillData'] = $this->User_Model->skills_single($userInfo[0]['id']);
        $data['assessData'] = $this->User_Model->user_assessment($userInfo[0]['id']);
        $data['expertData'] = $this->User_Model->user_expert($userInfo[0]['id']);
        $data['industrylists'] = $this->Common_Model->industry_lists();
        $data['levellist'] = $this->Common_Model->level_lists();
        $data['categorylist'] = $this->Common_Model->category_lists();
        $data['subcategorylist'] = $this->Common_Model->subcategory_lists();
        $data['channellists'] = $this->Common_Model->channel_lists();
        $data['channellistss'] = $this->Common_Model->channel_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data['workData'] = $this->fetchWorkSingleData($userInfo[0]['id']);
        $notification_list = $this->Common_Model->unreadnotification($userInfo[0]['id']); 
            if($notification_list){
                $data['badge'] = count($notification_list);
             }
        $data['getCompletenes'] = $this->fetchUserCompleteData($userInfo[0]['id']);
        $data['languagelists'] = $this->Common_Model->language_lists();
        $data['eduData'] = $this->fetchEduSingleData($userInfo[0]['id']);
        $data['checkResume'] = $this->User_Model->resume_single($userInfo[0]['id']);
        //echo $this->db->last_query();die;
        if($this->uri->segment(3)) {
                $data['errors'] = $this->uri->segment(3);
        } else {
            $data['errors'] = $dataErr['errors'];
        }
        $data['checkStatus'] = $this->User_Model->user_single($userInfo[0]['id']);
        $data["intrestedin"] = $this->Jobpost_Model->filtersubcategory_listsbycatid(9);
        $data["states"] = $this->User_Model->getownstate();
        //var_dump($data['errors']);die;
        $this->load->view('myprofile', $data);
    }

    public function JobListings() {
        $filterData = $this->input->post();
        //print_r($filterData);die;
        if (empty($filterData['locationn']) &&  $filterData['locationn'] == "" && empty($filterData['cname']) && $filterData['cname'] == "" && $filterData['jobsubcategory'] == "" && empty($filterData['joblevell'])  &&  empty($filterData['jobcategoryy'])) {
            $this->session->set_tempdata('filtererr','At least one field is required to search.');
            redirect('dashboard/index');
        }else{
        if (!empty($filterData['joblevell'][0]) && $filterData['joblevell'][0] == 9) {
            $filterData['joblevell'] = array(6, 4, 7, 8);
        }
        
        if (!empty($filterData['jobcategoryy'][0]) && $filterData['jobcategoryy'][0] == 9) {
            $filterData['jobcategoryy'] = array(5, 3, 4, 6, 7, 8);
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        $data["jobList"] = [];
        
        if ($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $jobfetchs = $this->Jobpost_Model->job_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            // echo $this->db->last_query();die;
            
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userTokenCheck[0]['id']);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['company_id']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['compId']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);
                    $ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                      //print_r($output);die;       
                   //Get latitude and longitute from json data
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;
                    $distance = $this->distance($dataLatitude, $dataLongitude, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                    if ($jobApplyfetch) {
                    } else {
                        $data["jobList"][] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"jobPitch"=> $jobfetch['jobPitch'] ,  "salary" => $basicsalary, "companyName" => $jobfetch['cname'],"cname"=> $cname, "company_id" => $jobfetch['company_id'], "jobstatus" => $jobstatus, "company_address" => $jobfetch['address'], "latitude" => $jobfetch['latitude'], "longitude" => $jobfetch['longitude'], "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "save_status" => $jobsstatus, "job_image" => $jobImageData, "distance"=>$distance];
                    }
                }
                if (count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else {
                $data["jobList"] = [];
            }
            $jobcountfetchs = $this->Jobpost_Model->jobcount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            
            if ($jobcountfetchs) {
                $xx = 1;
                foreach ($jobcountfetchs as $jobcountfetch) {
                    $dataget[$xx] = ["info" => $jobcountfetch['jobcount'] . " Jobs", "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], 'jobid' => $jobcountfetch['id']];
                    $xx++;
                }
                $dataget1 = json_encode($dataget);
                $data['jobcount'] = $dataget1;
                $data['getjoblist'] = $dataget;
            } else {
                $data['jobcount'] = [];
            }
            $data['jobtitle'] = $this->Common_Model->job_title_list();
            // echo "<PRE>";
            // var_dump($data);die;
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['languagelist'] = $this->Common_Model->language_lists();
            $data['level_list'] = $this->Common_Model->level_lists();
            $data['category_list'] = $this->Common_Model->category_lists();
            $data['checkResume'] = $this->User_Model->resume_single($userTokenCheck[0]['id']);
            $this->load->view("searchjobs", $data);
        } else {
            $data['errors'] = "Bad Request";
            redirect("dashboard/myprofile", $data);
        }}
    }

    public function filters1() {
        $filterData = $this->input->post();
        if (!empty($filterData['joblevell'][0]) && $filterData['joblevell'][0] == 9) {
            $filterData['joblevell'] = array(6, 4, 7, 8);
        }
        if (!empty($filterData['jobcategoryy'][0]) && $filterData['jobcategoryy'][0] == 9) {
            $filterData['jobcategoryy'] = array(5, 3, 4, 6, 7, 8);
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);

        $data["jobList"] = [];
        if ($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($jobfetch['id']);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['company_id']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if (!empty($jobTop)) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $jobListing[] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'], "salary" => $basicsalary, "jobstatus" => $jobstatus, "company_address" => $companyaddress, "company_id" => $jobfetch['company_id'], "companyName" => $jobfetch['cname'], "latitude" => $jobfetch['latitude'], "longitude" => $jobfetch['longitude'], "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "save_status" => $jobsstatus, "job_image" => $jobImageData];
                }
                $data["jobList"] = $jobListing;
                if (count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else {
                $data["jobList"] = [];
            }
            if (!empty($filterData['salarySort']) && $filterData['salarySort'] == 1) {
                $price = array();
                foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
                }
                array_multisort($price, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            }
            $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            if ($jobcountfetchs) {
                $xx = 1;
                foreach ($jobcountfetchs as $jobcountfetch) {
                    $dataget[$xx] = ["info" => $jobcountfetch['jobcount'] . " Jobs", "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], "jobid" => $jobcountfetch['id']];
                    $xx++;
                }
                $dataget1 = json_encode($dataget);
                $data['jobcount'] = $dataget1;
            } else {
                $data['jobcount'] = [];
            }
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['languagelist'] = $this->Common_Model->language_lists();
            $data['level_list'] = $this->Common_Model->level_lists();
            $data['category_list'] = $this->Common_Model->category_lists();
            if (!empty($filterData['salarySort'])) {
                $data['salarysort'] = $filterData['salarySort'];
            }
            if (!empty($filterData['locationSort'])) {
                $data['locationsort'] = $filterData['locationSort'];
            }
            if (!empty($filterData['ratingSort'])) {
                $data['ratingsort'] = $filterData['ratingSort'];
            }
            $this->load->view("searchjobs", $data);
        } else {
            session_destroy();
            redirect("dashboard/JobListings");
        }
    }
    public function filters() {
        $filterData = $this->input->post();
        //print_r($filterData);
        if (!empty($filterData['joblevel']) && $filterData['joblevel'] == 9) {
            $joblevel = arrar(6, 4, 7, 8);
            $joblevell = implode(",", $joblevel);
            $filterData['joblevel'] = $joblevell;
        }
        if (!empty($filterData['toppicks'])) {
            $jobtopiks = implode(",", $filterData['toppicks']);
            $filterData['toppicks'] = $jobtopiks;
        }
        if (!empty($filterData['medical'])) {
            $jobmedical = implode(",", $filterData['medical']);
            $filterData['medical'] = $jobmedical;
        }
        if (!empty($filterData['allowances'])) {
            $joballowance = implode(",", $filterData['allowances']);
            $filterData['allowances'] = $joballowance;
        }
        if (!empty($filterData['workshift'])) {
            $jobworkshift = implode(",", $filterData['workshift']);
            $filterData['workshift'] = $jobworkshift;
        }
        if (!empty($filterData['leaves'])) {
            $jobleaves = implode(",", $filterData['leaves']);
            $filterData['leaves'] = $jobleaves;
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if(!empty($filterData['exp_year'])){
        	$expyear = $filterData['exp_year'];
        }else{
        	$expyear = $userTokenCheck[0]['exp_year'];
        }
        if(!empty($filterData['exp_month'])){
        	$expmonth = $filterData['exp_month'];
        }else{
        	$expmonth = $userTokenCheck[0]['exp_month'];
        }
        /*if(  $filterData['exp_year']==''  && $filterData['exp_month']==''){
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];
        }else{
            $expmonth = $filterData['exp_month'];
            $expyear = $filterData['exp_year'];
            
        }*/

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
        $data["jobList"] = [];
        if ($userTokenCheck) {
            $jobfetchs = $this->Jobpost_Model->jobfilter_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($jobfetch['id']);
                    if (isset($jobsavedStatus[0]['status'])) {
                        $jobsstatus = $jobsavedStatus[0]['status'];
                    } else {
                        $jobsstatus = '';
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['company_id']);
                    //print_r($companydetail1);die;
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if (!empty($jobTop)) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    $ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                      //print_r($output);die;       
                   //Get latitude and longitute from json data
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;
                    $distance = $this->distance($dataLatitude, $dataLongitude, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', '');
                    $jobListing[] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'], "salary" => $basicsalary, "jobstatus" => $jobstatus, "company_address" => $companyaddress, "company_id" => $jobfetch['company_id'], "companyName" => $jobfetch['cname'],"cname"=> $cname, "jobPitch"=>$jobfetch['jobPitch'], "latitude" => $jobfetch['latitude'], "longitude" => $jobfetch['longitude'], "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "save_status" => $jobsstatus, "job_image" => $jobImageData,"distance"=>$distance];
                }
                $data["jobList"] = $jobListing;
                if (count($data["jobList"]) <= 0) {
                    $data["jobList"][] = [];
                }
            } else {
                $data["jobList"] = [];
            }
            if (!empty($filterData['salarySort']) && $filterData['salarySort'] == 1) {
                $price = array();
                foreach ($jobListing as $key => $row) {
                    $price[$key] = $row['salary'];
                }
                array_multisort($price, SORT_DESC, $jobListing);
                $data["jobList"] = $jobListing;
            }
            $jobcountfetchs = $this->Jobpost_Model->jobfiltercount_fetch_all($filterData, $userTokenCheck[0]['id'],$expfilter);
            $returnnewarray = array();
                 $latlogcheck =array();
                 foreach ($jobcountfetchs as $value123) {
                      if(in_array($value123['latitude'],$latlogcheck)){
                          $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] = $returnnewarray[array_search($value123['latitude'], $latlogcheck)]['jobcount'] + 1;
                      } else {
                          $latlogcheck[] = $value123['latitude'];
                          $returnnewarray[] = $value123;
                      }
                 }

            $jobcountfetchs = $returnnewarray;
            if ($jobcountfetchs) {
                $xx = 1;
                foreach ($jobcountfetchs as $jobcountfetch) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobcountfetch['id']);
                       
                        if($jobTop) {
                            if(isset($jobTop[0]['picks_id'])){
                                $one = (string)$jobTop[0]['picks_id'];
                                $dataJobList[0] = "$one";
                            } else{
                                $dataJobList[0] = "";
                            }
                            if(isset($jobTop[1]['picks_id'])) {
                                $two = (string)$jobTop[1]['picks_id'];
                                $dataJobList[1] = "$two";
                            } else {
                                $dataJobList[1] = "";
                            }
                            if(isset($jobTop[2]['picks_id'])){
                                $three = (string)$jobTop[2]['picks_id'];
                                $dataJobList[2] = "$three";
                            } else{
                                $dataJobList[2] = "";
                            }
                        } else {
                            $dataJobList[0] = "";
                            $dataJobList[1] = "";
                            $dataJobList[2] = "";
                        }
                        if(!empty($dataJobList[0])){
                            if($dataJobList[0]=='1'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/bonus.png";
                            }else if($dataJobList[0]=='2'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_free_food.png";
                            }else if($dataJobList[0]=='3'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_dependent_hmo.png";
                            }else if($dataJobList[0]=='4'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_day_1_hmo.png";
                            }else if($dataJobList[0]=='5'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_dayshift.png";
                            }else if($dataJobList[0]=='6'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[1])){
                            if($dataJobList[1]=='1'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/bonus.png";
                            }else if($dataJobList[1]=='2'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_free_food.png";
                            }else if($dataJobList[1]=='3'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_dependent_hmo.png";
                            }else if($dataJobList[1]=='4'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_day_1_hmo.png";
                            }else if($dataJobList[1]=='5'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_dayshift.png";
                            }else if($dataJobList[1]=='6'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_14th_pay.png";
                            }
                        }

                        if(!empty($dataJobList[2])){
                            if($dataJobList[2]=='1'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/bonus.png";
                            }else if($dataJobList[2]=='2'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_free_food.png";
                            }else if($dataJobList[2]=='3'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_dependent_hmo.png";
                            }else if($dataJobList[2]=='4'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_day_1_hmo.png";
                            }else if($dataJobList[2]=='5'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_dayshift.png";
                            }else if($dataJobList[2]=='6'){
                                $url = "http://mobuloustech.com/jobyoda/markericon/j_14th_pay.png";
                            }
                        }
                        
                        if(count($jobTop)>1){
                            $url = "http://mobuloustech.com/jobyoda/markericon/j_hot_job.png";
                        }

                        if(empty($dataJobList[0]) && empty($dataJobList[1]) && empty($dataJobList[2])){
                            $url = "http://mobuloustech.com/jobyoda/markericon/maps.png";
                        }
                    $dataget[$xx] = ["info" => $jobcountfetch['jobcount'] . " Jobs"."<br>"."   ".substr($jobcountfetch['salary'], 0, 2)."K", "lat" => $jobcountfetch['latitude'], "lng" => $jobcountfetch['longitude'], "jobid" => $jobcountfetch['id'], "url" => $url];
                    $xx++;
                }
                //$dataget1 = json_encode($dataget);
                $data['jobcount'] = $dataget;
            } else {
                $data['jobcount'] = [];
            }
            $data['industrylists'] = $this->Common_Model->industry_lists();
            $data['channellists'] = $this->Common_Model->channel_lists();
            $data['languagelist'] = $this->Common_Model->language_lists();
            $data['level_list'] = $this->Common_Model->level_lists();
            $data['category_list'] = $this->Common_Model->category_lists();
            if (!empty($filterData['salarySort'])) {
                $data['salarysort'] = $filterData['salarySort'];
            }
            if (!empty($filterData['locationSort'])) {
                $data['locationsort'] = $filterData['locationSort'];
            }
            if (!empty($filterData['ratingSort'])) {
                $data['ratingsort'] = $filterData['ratingSort'];
            }
            $this->load->view("searchjobs", $data);
        } else {
            session_destroy();
            redirect("dashboard/JobListings");
        }
    }
    public function jobDescription() {
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
         }   
        $decrypt_id = $_GET['listing'];

        $jobpost_id= base64_decode($decrypt_id);
        //echo $jobpost_id;die;
        $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobpost_id);
        //print_r($jobDetails);die;
        //echo $this->db->last_query();die;
        $companydetail = $this->Jobpost_Model->company_detail_fetch($jobDetails[0]['company_id']);
        if($jobDetails[0]['subrecruiter_id']){
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['subrecruiter_id']);
            }else{
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobDetails[0]['recruiter_id']);
            }
          //  echo $this->db->last_query();die;
        $jobLocation = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
        $recruiteDetail = $this->Jobpost_Model->recruiter_detail_fetch($jobDetails[0]['recruiter_id']);

        if (isset($jobLocation[0]['address'])) {
            $companyaddress = $jobLocation[0]['address'];
        } else {
            $companyaddress = '';
        }
        
        if (isset($jobLocation[0]['phone'])) {
            $companyphone = $jobLocation[0]['phone'];
        } else {
            $companyphone = '';
        }
        $jobSaved = $this->Jobpost_Model->applied_status_fetch($jobpost_id, $userTokenCheck[0]['id']);
        
        if ($jobSaved) {
            $savestatus = 1;
        } else {
            $savestatus = 0;
        }
        
        $data['jobTop'] = $this->Jobpost_Model->job_toppicks($jobDetails[0]['id']);
        $data['jobAllowance'] = $this->Jobpost_Model->job_allowances($jobDetails[0]['id']);
        $data['jobMedical'] = $this->Jobpost_Model->job_medical($jobDetails[0]['id']);
        $data['jobShift'] = $this->Jobpost_Model->job_workshift($jobDetails[0]['id']);
        $data['jobLeaves'] = $this->Jobpost_Model->job_leaves($jobDetails[0]['id']);
        $data['jobskills'] = $this->Jobpost_Model->job_skills($jobpost_id);
        $appliedStatus = $this->Jobpost_Model->applied_status_fetch($jobDetails[0]['id'], $userTokenCheck[0]['id']);
        if ($appliedStatus) {
            $jobstatus = $appliedStatus[0]['status'];
            $jobapplieddate = date("Y-m-d",strtotime($appliedStatus[0]['interviewdate']));
            $jobappliedtiming = date("H:i",strtotime($appliedStatus[0]['interviewtime']));
            $jobappliedtime = $jobapplieddate.",".$jobappliedtiming;
            $date_day1 = date("Y-m-d",strtotime($appliedStatus[0]['date_day1']));
        } else {
            $jobstatus = 0;
            $jobappliedtime = '';
            $date_day1 = '';
        }
        for($itlku=$expfilter; $itlku >-1 ; $itlku--) { 
            $jobsal = $this->Jobpost_Model->getExpJob1($jobDetails[0]['id'],$itlku);
            //echo $this->db->last_query();
            if(!empty($jobsal)){
                break;
            }
        }
        //$jobsal = $this->Jobpost_Model->getExpJob($jobpost_id);
        
        if ($jobsal[0]['basicsalary']) {
            $basicsalary = $jobsal[0]['basicsalary'];
        } else {
            $basicsalary = "0";
        }

        $data['jobImg'] = $this->Jobpost_Model->job_images_single($jobpost_id);
        
        
        $data["jobdetail"] = ["jobpost_id" => $jobDetails[0]['id'], 
                            "job_title" => $jobDetails[0]['jobtitle'],
                            "phonecode"=>$companydetail1[0]['phonecode']??'',
                            "landline"=>$companydetail1[0]['landline']??'', 
                            "location" => $companyaddress, 
                            "opening" => $jobDetails[0]['opening'], 
                            "basicSalary"=>$basicsalary, 
                            "allowances"=>$jobDetails[0]['allowance'], 
                            "experience" => $jobDetails[0]['experience'], 
                            "jobDesc" => $jobDetails[0]['jobDesc'], 
                            "jobPitch" => $jobDetails[0]['jobPitch'],
                            "mode" => $jobDetails[0]['mode'], 
                            "modeurl" => $jobDetails[0]['modeurl'], 
                            "salary" => $basicsalary, 
                            "education" => $jobDetails[0]['education'], 
                            "jobSaved" => $savestatus, 
                            "status" => $jobstatus,
                            'jobappliedtime'=>$jobappliedtime, 
                            "companyphone" => $companydetail1[0]["phone"]??'', 
                            "email" => $companydetail1[0]["email"]??'' 
                    ];

        $data["comapnyDetail"] = ["comapnyId" => $companydetail[0]["id"],"name" => $companydetail[0]["cname"],"recruiterName" => $recruiteDetail[0]["cname"], "recruiter_id" => $jobDetails[0]['recruiter_id'], "phone" => $companyphone, "email" => $companydetail[0]["email"]];

        $data['checkResume'] = $this->User_Model->resume_single($userTokenCheck[0]['id']);
         $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
         $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
            if($notification_list){
                $data['badge'] = count($notification_list);
             }
         $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
        $this->load->view('jobdescription', $data);
    }

    public function resumeUpload() {
        $resumeData = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $checkResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);
            if ($checkResume) {
                if (!empty($_FILES['resumeFile']['name'])) {
                    $config = array('upload_path' => "./files/", 'allowed_types' => "pdf|doc",);
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('resumeFile')) {
                        $data = array('upload_data' => $this->upload->data());
                        $picPath = base_url() . "files/" . $data['upload_data']['file_name'];
                    } else {
                        $data1['errors'] = ["uploadresume" => "Upload Failed"];
                        redirect("dashboard/jobDescription", $data1);
                    }
                }
                $userResumeUpdate = $this->User_Model->user_resume_update($picPath, $userTokenCheck[0]['id']);
            } else {
                if (!empty($_FILES['resumeFile']['name'])) {
                    $config = array('upload_path' => "./files/", 'allowed_types' => "pdf|doc",);
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('resumeFile')) {
                        $data = array('upload_data' => $this->upload->data());
                        $picPath = base_url() . "files/" . $data['upload_data']['file_name'];
                    } else {
                        $data1['errors'] = ["uploadresume" => "Upload Failed"];
                        redirect("dashboard/jobDescription", $data1);
                    }
                }
                $userResumeInsert = $this->User_Model->user_resume_insert($picPath, $userTokenCheck[0]['id']);
            }
            redirect("dashboard/jobDescription?listing=" . $resumeData['listing'] . "&msg=uploadcomplete");
        } else {
            $data1['errors'] = "Bad Request";
            redirect("dashboard/jobDescription", $data1);
        }
    }

    public function checkdateschedule() {
    
        $datevalue = $this->input->post('dateValue');
        $getSelectDate = date('d-m-Y', strtotime($datevalue)); 
        $timestamp = strtotime($getSelectDate);
        $day = date('D', $timestamp);
        $currentDate = strtotime(date('d-m-Y'));

        $jobId = $this->input->post('jobId');
        $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobId);
        $expiryDate = strtotime($jobDetails[0]['jobexpire']); 

        $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
        
        $days1 = $recruiterdetail[0]['dayfrom'];
        $days2 = $recruiterdetail[0]['dayto'];

        if($days1 == 1) {  $dayName ='Monday'; }
        if($days1 == 2) {  $dayName ='Tuesday'; }
        if($days1 == 3) {  $dayName ='Wednesday'; }
        if($days1 == 4) {  $dayName ='Thursday'; }
        if($days1 == 5) {  $dayName ='Friday';  }
        if($days1 == 6) {  $dayName ='Saturday'; }
        if($days1 == 7) {  $dayName ='Sunday'; }

        if($days2 == 1) {  $dayName1 ='Monday'; }
        if($days2 == 2) {  $dayName1 ='Tuesday'; }
        if($days2 == 3) {  $dayName1 ='Wednesday'; }
        if($days2 == 4) {  $dayName1 ='Thursday'; }
        if($days2 == 5) {  $dayName1 ='Friday';  }
        if($days2 == 6) {  $dayName1 ='Saturday'; }
        if($days2 == 7) {  $dayName1 ='Sunday'; }

        $z = 1;
        $blarray = array();
        
        for ($i = $days1; $i <= $days2; $i++) {
            if ($i == 1) {
                $dayname = "Mon";
            } else if ($i == 2) {
                $dayname = "Tue";
            } else if ($i == 3) {
                $dayname = "Wed";
            } else if ($i == 4) {
                $dayname = "Thu";
            } else if ($i == 5) {
                $dayname = "Fri";
            } else if ($i == 6) {
                $dayname = "Sat";
            } else if ($i == 7) {
                $dayname = "Sun";
            }
            $blarray[] = $dayname;
            $z++;
        }
            
        if($timestamp >= $currentDate && $timestamp <= $expiryDate) {

            if (in_array($day, $blarray)) {

                echo json_encode(["status"=>"success"]);
                die;
            
            } else {

                $msg = "Please Select Schedule date between ".$dayName. ' to '.$dayName1;
                echo json_encode(["message"=>$msg, "status"=>"error"]);
                die;
            }
            
        } else {
            $msg = "Please select date between ". date('d-m-Y') ." to ". date('d-m-Y', strtotime($jobDetails[0]['jobexpire']));
            echo json_encode(["message"=>$msg, "status"=>"error"]);
            die;
        }
    }

    public function checktimeschedule() {
        $timevalue = $this->input->post('timeValue');
        $jobId = $this->input->post('jobId');
        $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobId);
        // $companydetail = $this->Jobpost_Model->company_detail_fetch($jobDetails[0]['company_id']);
        $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
        $timeFrom = $recruiterdetail[0]['from_time'];
        $timeFromm = date('H:i', strtotime($timeFrom));
        $timeTo = $recruiterdetail[0]['to_time'];
        $timeToo = date('H:i', strtotime($timeTo));
        
        $date1 = strtotime($timevalue);
        $date2 = strtotime($timeFromm);
        $date3 = strtotime($timeToo);
        if ($date1 >= $date2 && $date1 <= $date3) {
            echo '1';
        } else {

            if($date2 >= $date3 && $date1 < $date2) {
                if($date1 <= $date3) {
                    echo '1';
                } else {
                    echo '2';
                }
            } else {

                if($date2 >= $date3) {
                    echo '1';
                } else {
                    echo '2';
                }
            }
        }
    }
    public function scheduled() {

        $date = date('Y-m-d H:i:s');
        $job = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);

        if ($userTokenCheck) {

            $job_detail = $this->Jobpost_Model->job_detail_fetch($job['listing']);
            $companyname = $this->Candidate_Model->fetchcomp($job_detail[0]['recruiter_id']);

            $jobexpire = date("Y-m-d", strtotime($job_detail[0]['jobexpire']));
            $schedule = date("Y-m-d", strtotime($job['scheduledate']));
            
            $jobexpire1 = strtotime($jobexpire);
            $schedule1 = strtotime($schedule);
            $currentDate = strtotime(date('Y-m-d'));

            if($job_detail[0]['education_status']=='1') {        
                $user_education_match = $this->User_Model->user_single_educationbyuserid2($userTokenCheck[0]['id'], $job_detail[0]['education']);
                
                if($user_education_match) {
                    $userEdu = $user_education_match[0]['education'];

                    if($job_detail[0]['education'] == "No Requirement") {
                        $education_match = "True";
                    
                    } else if($job_detail[0]['education'] == "Vocational") {

                        if($job_detail[0]['education'] == $userEdu) {
                            $education_match = "True";

                        } else if($userEdu == "High School Graduate" || $userEdu == "Undergraduate" || $userEdu == "Associate Degree" || $userEdu == "College Graduate" || $userEdu == "Post Graduate") {

                            $education_match = "True";
                        } else {
                            $education_match = "False";                                
                        }
                    
                    } else if($job_detail[0]['education'] == "High School Graduate") {

                        if($job_detail[0]['education'] == $userEdu) {
                            $education_match = "True";

                        } else if($userEdu == "Undergraduate" || $userEdu == "Associate Degree" || $userEdu == "College Graduate" || $userEdu == "Post Graduate") {

                            $education_match = "True";
                        } else {
                            $education_match = "False";                                
                        }
                    
                    } else if($job_detail[0]['education'] == "Undergraduate") {

                        if($job_detail[0]['education'] == $userEdu) {
                            $education_match = "True";

                        } else if($userEdu == "Associate Degree" || $userEdu == "College Graduate" || $userEdu == "Post Graduate") {
                            $education_match = "True";
                        } else {
                            $education_match = "False";                                
                        }
                    
                    } else if($job_detail[0]['education'] == "Associate Degree") {

                        if($job_detail[0]['education'] == $userEdu) {
                            $education_match = "True";

                        } else if($userEdu == "College Graduate" || $userEdu == "Post Graduate") {
                            $education_match = "True";
                        } else {
                            $education_match = "False";                                
                        }
                    
                    } else if($job_detail[0]['education'] == "College Graduate") {

                        if($job_detail[0]['education'] == $userEdu) {
                            $education_match = "True";

                        } else if($userEdu == "Post Graduate") {
                            $education_match = "True";
                        } else {
                            $education_match = "False";                                
                        }
                    } else if($job_detail[0]['education'] == "College Graduate") {

                        if($job_detail[0]['education'] == $userEdu) {
                            $education_match = "True";
                        } else {
                            $education_match = "False";                                
                        }
                    } else {
                        $education_match = "False";
                    }

                } else {
                    $education_match = "False";
                }
            } else {
                $education_match = "True";
            }

            if($job_detail[0]['experience_status']=='1') {  
                $user_experience_match = $this->User_Model->user_single_exp($userTokenCheck[0]['id'], $job_detail[0]['experience']);
                if($user_experience_match){
                    $experience_match = "True";
                } else{
                    $experience_match = "False";
                }
            } else {
                $experience_match = "True";
            }

            if($jobexpire1 >= $schedule1) {
                    
                if($schedule1 >= $currentDate) {
                    
                    if($job_detail[0]['mode'] == "Walk-in") {
                        $messageee = "Thank You for applying through JobYoDA! This job has been tagged as a Walk-In by the Recruiter. Please be reminded that the Recruiter would be expecting you at their Recruitment Hub on your chosen schedule. Be on time and don't forget to look your best! Claim your Php500 from us if you get Hired, just send us an email at help@jobyoda.com! Good Luck!";
                    } else if($job_detail[0]['mode'] == "Call"){
                        $messageee = "Thank You for applying through JobYoDA!Please keep your lines open, <b>the Recruiter will call you if you meet the job requirements. Claim your Php500 from us if you get Hired, just send us an email at help@jobyoda.com! Good Luck!<b>";
                    }

                    $jobapply = [
                      "jobpost_id" => $job['listing'],
                      "user_id" => $userTokenCheck[0]['id'],
                      "interviewdate" => date("y-m-d",strtotime($job['scheduledate'])),
                      "interviewtime" => $job['scheduletime'],
                      "status" => 1,
                      "created_at" => $date,    
                    ];

                    if($education_match == "True" && $experience_match== "True") {

                        $this->Jobpost_Model->appliedjob_insert($jobapply);

                        $data2 = ['message'=>'You have got a new application from candidate'];
                        $this->Recruit_Model->Notificationmessageinsert($data2);

                        $msg = "New Interview Alert: ".$userTokenCheck[0]['name']." just scheduled for an interview today at ".$job['interviewtime']." for ".$job_detail[0]['jobtitle']." in ".$companyname[0]['cname'].". Get in touch via ".$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone']." or ".$userTokenCheck[0]['email']."";

                        $data1 = ["user_id"=>$userTokenCheck[0]['id'], "jobapp_id"=>$job['listing'], "recruiter_id" =>$job_detail[0]['company_id'], "notification"=>$msg, "status_changed"=>3, "job_status"=>'0', "badge_count"=>1, "createdon"=>$date];

                        $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                        
                        if($schedule1 == $currentDate){

                            $this->load->library('email');
                            $data['interviewtime'] = $job['scheduletime'];
                            $data['cname'] = $companyname[0]['cname'];
                            $data['rname'] = $companyname[0]['fname'].' '.$companyname[0]['lname'];
                            $data['jobtitle'] = $job_detail[0]['jobtitle'];
                            $data['name'] =$userTokenCheck[0]['name'];
                            $data['phone'] =$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone'];
                            $data['email'] =$userTokenCheck[0]['email'];
                            $data['uid'] =$userTokenCheck[0]['id'];
                            $msg = $this->load->view('recruiter/recruiteremail2',$data,TRUE);
                            $config=array(
                                'charset'=>'utf-8',
                                'wordwrap'=> TRUE,
                                'mailtype' => 'html'
                            );

                            $this->email->initialize([
                                          'protocol' => 'smtp',
                                          'smtp_host' => 'smtpout.asia.secureserver.net',
                                          'smtp_user' => 'help@jobyoda.com',
                                          'smtp_pass' => 'Usa@1234567',
                                          'smtp_port' => 465,
                                          'smtp_crypto' => 'ssl',
                                          'charset'=>'utf-8',
                                          'mailtype' => 'html',
                                          'crlf' => "\r\n",
                                          'newline' => "\r\n"
                            ]);
                            $this->email->from('help@jobyoda.com', "JobYoDA");
                            $this->email->to($companyname[0]['email']);
                            $this->email->subject('JobYoDA New Interview Alert - '.$companyname[0]['cname'].' - Date '.date('Y-m-d'));
                            $this->email->message($msg);
                            $this->email->send();

                            if(!empty($job_detail[0]['subrecruiter_id'])) {
                                $subrecruiter_data = $this->Candidate_Model->fetchcomp($job_detail[0]['subrecruiter_id']);
                                $subrecruiter_comp = $this->Candidate_Model->fetchcomp($subrecruiter_data[0]['parent_id']);
                                $this->load->library('email');
                                $data1['interviewtime'] = $job['scheduletime'];
                                $data1['cname'] = $subrecruiter_comp[0]['cname'];
                                $data1['rname'] = $subrecruiter_data[0]['fname'].' '.$subrecruiter_data[0]['lname'];
                                $data1['jobtitle'] = $job_detail[0]['jobtitle'];
                                $data1['name'] =$userTokenCheck[0]['name'];
                                $data1['phone'] =$userTokenCheck[0]['country_code'].$userTokenCheck[0]['phone'];
                                $data1['email'] =$userTokenCheck[0]['email'];
                                $data1['uid'] =$userTokenCheck[0]['id'];
                                $msg = $this->load->view('recruiter/recruiteremail2',$data1,TRUE);
                                $config=array(
                                    'charset'=>'utf-8',
                                    'wordwrap'=> TRUE,
                                    'mailtype' => 'html'
                                );

                                $this->email->initialize([
                                      'protocol' => 'smtp',
                                      'smtp_host' => 'smtpout.asia.secureserver.net',
                                      'smtp_user' => 'help@jobyoda.com',
                                      'smtp_pass' => 'Usa@1234567',
                                      'smtp_port' => 465,
                                      'smtp_crypto' => 'ssl',
                                      'charset'=>'utf-8',
                                      'mailtype' => 'html',
                                      'crlf' => "\r\n",
                                      'newline' => "\r\n"
                                ]);
                                $this->email->from('help@jobyoda.com', "JobYoDA");
                                $this->email->to($subrecruiter_data[0]['email']);
                                $this->email->subject('JobYoDA New Interview Alert - '.$subrecruiter_data[0]['cname'].' - Date '.date('Y-m-d'));
                                $this->email->message($msg);
                                $this->email->send(); 
                            }
                        }

                        $response = ['message' => $messageee, 'status' => "SUCCESS"];
                        echo json_encode($response);
                        exit;

                    } else {

                        // $jobapply1 = [
                        //     "jobpost_id" => $job['listing'],
                        //     "user_id" => $userTokenCheck[0]['id'],
                        //     "interviewdate" => date("y-m-d",strtotime($job['scheduledate'])),
                        //     "interviewtime" => $job['scheduletime'],
                        //     "status" => 3,
                        //     "fallout_status" => 2,
                        //     "fallout_reason" => "Did not meet requirement",
                        //     "created_at" => $date,
                        // ];
                        // $this->Jobpost_Model->appliedjob_insert($jobapply1);

                        // $msg="Your application for "." ". $job_detail[0]['jobtitle'] ." "." at "." ". $companyname[0]['cname'] ." was rejected for not meeting the hiring requirement. Make sure your profile is up-to-date for better chances!";
                        
                        // $data1 = ["user_id"=>$userTokenCheck[0]['id'], "jobapp_id"=>$job['listing'], "recruiter_id" =>$job_detail[0]['company_id'], "notification"=>$msg, "status_changed"=>3, "job_status"=>'0', "badge_count"=>1, "createdon"=>$date];

                        // $notificationInserted = $this->Candidate_Model->notification_insert($data1);
                        $msg = "You are applying for a job for which your profile doesn’t match the requirements. Please update your profile.";
                        $response = ['message' => $msg, 'status' => "FAILURE"];
                        echo json_encode($response);
                    }

                } else{
                    $errors = "Please select correct date";
                    $response = ['message' => $errors, 'status' => "FAILURE"];
                    echo json_encode($response);
                    exit;
                }

            } else {
                $errors = "Interview date expire on $jobexpire.";
                $response = ['message' => $errors, 'status' => "FAILURE"];
                echo json_encode($response);
                exit;
            }
            
        } else {
            $errors = "Bad Request";
            $response = ['message' => $errors, 'status' => "FAILURE"];
            echo json_encode($response);
            exit;
        }
    }

    public function resumeUploadListing() {
        $resumeData = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $checkResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);
            if ($checkResume) {
                if (!empty($_FILES['resumeFile']['name'])) {
                    $config = array('upload_path' => "./files/", 'allowed_types' => "pdf|doc",);
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('resumeFile')) {
                        $data = array('upload_data' => $this->upload->data());
                        $picPath = base_url() . "files/" . $data['upload_data']['file_name'];
                    } else {
                        $data1['errors'] = ["uploadresume" => "Upload Failed"];
                        redirect("dashboard/JobListings", $data1);
                    }
                }
                $userResumeUpdate = $this->User_Model->user_resume_update($picPath, $userTokenCheck[0]['id']);
            } else {
                if (!empty($_FILES['resumeFile']['name'])) {
                    $config = array('upload_path' => "./files/", 'allowed_types' => "pdf|doc",);
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('resumeFile')) {
                        $data = array('upload_data' => $this->upload->data());
                        $picPath = base_url() . "files/" . $data['upload_data']['file_name'];
                    } else {
                        $data1['errors'] = ["uploadresume" => "Upload Failed"];
                        redirect("dashboard/JobListings", $data1);
                    }
                }
                $userResumeInsert = $this->User_Model->user_resume_insert($picPath, $userTokenCheck[0]['id']);
            }
            redirect("dashboard/JobListings?type=" . $resumeData['type'] . "&msg=uploadcomplete");
        } else {
            $data1['errors'] = "Bad Request";
            redirect("dashboard/JobListings", $data1);
        }
    }
    public function imageUpload() {
        if (!empty($_FILES['file']['name'])) {
            $config = array('upload_path' => "./files/", 'allowed_types' => "pdf|doc|docx",);
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('file')) {
                $data = array('upload_data' => $this->upload->data());
                $picPath = base_url() . "files/" . $data['upload_data']['file_name'];
                echo $data['upload_data']['file_name'];
                die;
            } else {
                $data1['errors'] = ["uploadresume" => "Upload Failed"];
                //redirect("dashboard/JobListings", $data1);
                echo '';
            }
        }
    }
    public function fileInsert() {
        $filename = $this->input->post('imagedata');
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            if (!empty($filename)) {
                $checkResume = $this->User_Model->resume_single($userTokenCheck[0]['id']);
                if (!empty($checkResume)) {
                    $picPath = base_url() . "files/" . $filename;
                    $userResumeUpdate = $this->User_Model->user_resume_update($picPath, $userTokenCheck[0]['id']);
                    echo '1';
                } else {
                    $picPath = base_url() . "files/" . $filename;
                    $userResumeInsert = $this->User_Model->user_resume_insert($picPath, $userTokenCheck[0]['id']);
                    echo '2';
                }
            }
        }
    }
    public function scheduledListing() {
        $job = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        
        if ($userTokenCheck) {
            $jobapply = ["jobpost_id" => $job['listing'], "user_id" => $userTokenCheck[0]['id'], "interviewdate" => date("Y-m-d", strtotime($job['scheduledate'])), "interviewtime" => date("h:i:sa", strtotime($job['scheduletime'])), "status" => 1];
            $insertid = $this->Jobpost_Model->appliedjob_insert($jobapply);
            
            if($insertid){
                $this->session->set_tempdata('successmsg', "Job applied successfully.");
                
            	redirect("user/index?type=" . $job['type'] . "&msg=scheduled");
            }
           
        } else {
            $data1['errors'] = "Bad Request";
            redirect("user", $data1);
        }
    }

    public function jobsearchstatus(){
        $job = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $statusdata = $job['searchstatus'];
            $statusUpdate = $this->User_Model->jobsearchstatus_update($statusdata, $userTokenCheck[0]['id']);
            if($statusUpdate){
                $this->session->set_tempdata('statusmsg', "Status changed Successfully.");
                redirect("user/homeafterlogin");
            }
           
        } else {
            $data1['errors'] = "Bad Request";
            redirect("user", $data1);
        }
    }

    public function usercontact(){
        $userData = $this->input->post();
       // print_r($userData);die;
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $data = ["user_id" => $userTokenCheck[0]['id'],"email"=> $userData['email'], "message"=>$userData['message'] ];
            $contact_inserted =  $this->User_Model->insert_contact($data);
            if($contact_inserted){
                $this->session->set_tempdata('successmsg', "Submitted Successfully.");
                redirect("dashboard/contactUs");
            }
        }
    }

    public function jobLists() {

        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        //$userData = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        //echo $this->db->last_query();die;
        if ($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }

            $userLat = $_GET['lat'];
            $userLong = $_GET['long'];
            //$job_id = $_GET['job_id'];
            $jobfetchs = $this->Jobpost_Model->job_fetch_latlong($userLat, $userLong,'', $expfilter);
           // echo $this->db->last_query();die;
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    //print_r($jobfetch);
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    //print_r($jobsal);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    //print_r($jobstatus);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);
                    //print_r($jobApplyfetch);die;
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    /*$ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                      //print_r($output);die;       
                   //Get latitude and longitute from json data
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
                    $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                        $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userTokenCheck[0]['id']);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobsavedStatus[0]['status'])) {
                            $jobsstatus = $jobsavedStatus[0]['status'];
                        } else {
                            $jobsstatus = '';
                        }    
                    if ($jobApplyfetch) {
                    } else {
                        //print_r($jobfetch);
                        $data1["jobList"][] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => $jobfetch['salary'], "companyName" => $jobfetch['cname'],"cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "recruiter_id" => $jobfetch['recruiter_id'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image"=>$jobImageData,"savedjob" => $jobsstatus, "mode" => $jobfetch['mode'], "modeurl" => $jobfetch['modeurl']];
                    }
                }
                $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
                if($notification_list){
                    $data['badge'] = count($notification_list);
                 }
                 $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
                if($notification_list){
                    $data1['badge'] = count($notification_list);
                 }
                $data1['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
                 $data1['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
                 $data1['checkResume'] = $this->User_Model->resume_single($userTokenCheck[0]['id']);
                $this->load->view("joblists", $data1);
            } else {
                $data1['errors'] = "Bad Request";
                redirect("user/index", $data1);
            }
        } else {
            $data1['errors'] = "Bad Request";
            redirect("user/index", $data1);
        }
    }

    public function homejobLists() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        //$userData = $this->input->post();
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $userLat = $_POST['lat'];
            $userLong = $_POST['long'];
            //$job_id = $_GET['job_id'];
            $jobfetchs = $this->Jobpost_Model->homejob_fetch_latlong($userLat, $userLong,'', $expfilter);
            //echo $this->db->last_query();die;
            if ($jobfetchs) {
                foreach ($jobfetchs as $jobfetch) {
                    //print_r($jobfetch);
                    $jobsal = $this->Jobpost_Model->getExpJob($jobfetch['id']);
                    //print_r($jobsal);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $jobstatus = $this->Jobpost_Model->getstatusJob($jobfetch['id']);
                    //print_r($jobstatus);
                    if (isset($jobstatus[0]['status'])) {
                        $jobstatus = $jobstatus[0]['status'];
                    } else {
                        $jobstatus = '';
                    }
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobfetch['compId']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobApplyfetch = $this->Jobpost_Model->job_fetch_appliedbycompid($jobfetch['id'], $userTokenCheck[0]['id']);
                    //print_r($jobApplyfetch);die;
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobfetch['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobfetch['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }
                    /*$ip = $_SERVER['REMOTE_ADDR'];
                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
                   $data_loc = json_decode($location);
                   
                   $Address = $data_loc->city;
                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                   $output = json_decode($geocodeFromAddr);
                      //print_r($output);die;       
                   //Get latitude and longitute from json data
                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
                    $distance = $this->distance($current_lat, $current_long, $jobfetch['latitude'], $jobfetch['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', '');
                    $companyPic = $this->Jobpost_Model->fetch_companyPic($jobfetch['recruiter_id']);
                            
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($jobfetch['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                        $condtionssavedjob = array('jobpost_id' => $jobfetch['id'], 'user_id' => $userTokenCheck[0]['id']);
                    $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobsavedStatus[0]['status'])) {
                            $jobsstatus = $jobsavedStatus[0]['status'];
                        } else {
                            $jobsstatus = '';
                        }    
                    if ($jobApplyfetch) {
                    } else {
                        //print_r($jobfetch);
                        $data1["jobList"][] = ["jobpost_id" => $jobfetch['id'], "job_title" => $jobfetch['jobtitle'], "jobDesc" => $jobfetch['jobDesc'],"distance"=>$distance, "salary" => $basicsalary, "companyName" => $jobfetch['cname'],"cname"=> $cname,"jobPitch"=>$jobfetch['jobPitch'], "companyId" => $jobfetch['compId'], "jobstatus" => $jobstatus, "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "jobImg"=>$jobImageData,"savedjob" => $jobsstatus, ];
                    }
                }
                $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
                if($notification_list){
                    $data1['badge'] = count($notification_list);
                 }
                $data1['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
                $this->load->view("joblists", $data1);
            
            } else {
                $data1['errors'] = "Bad Request";
                redirect("user/index", $data1);
            }
        } else {
            $data1['errors'] = "Bad Request";
            redirect("user/index", $data1);
        }
    }


    public function job_listing() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        if(!empty($this->input->get('cat'))){
            $category = $this->input->get('cat');
        }else{
            $category = 0;
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
        $hotjobData = $this->Common_Model->hotjob_fetch_latlongg($category);
        //echo $this->db->last_query();die;
            
            //echo $this->db->last_query();die;
            if ($hotjobData) {
                foreach ($hotjobData as $hotjobsData) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                    
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                        
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        //echo $this->db->last_query();die;
                        if (!empty($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                       // echo $jobImageData;die;
                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if ($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }
                        $condtionssavedjob = array('jobpost_id' => $hotjobsData['id'], 'user_id' => $userTokenCheck[0]['id']);
                        $jobsavedStatus = $this->Jobpost_Model->getstatusSavedJob($condtionssavedjob);
                        if (isset($jobsavedStatus[0]['status'])) {
                            $jobsstatus = $jobsavedStatus[0]['status'];
                        } else {
                            $jobsstatus = '';
                        }  
                        /*$ip = $_SERVER['REMOTE_ADDR'];
                       $location = file_get_contents('http://ip-api.com/json/'.$ip);
                       $data_loc = json_decode($location);
                       
                       $Address = $data_loc->city;
                       $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($Address).'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                       $output = json_decode($geocodeFromAddr);*/
                    
                       //$dataLatitude  = $output->results[0]->geometry->location->lat; 
                       //$dataLongitude = $output->results[0]->geometry->location->lng;
                       //$distance = '';
                        $distance = $this->distance($current_lat, $current_long, $hotjobsData['latitude'], $hotjobsData['longitude'], "K");
                         $distance = number_format((float)$distance, 2, '.', '');
                        $data["jobList"][] = ["jobpost_id" => $hotjobsData['id'],"jobPitch"=>$hotjobsData['jobPitch'], "companyId" => $hotjobsData['compId'], "job_title" => $hotjobsData['jobtitle'], "jobDesc" => $hotjobsData['jobDesc'], "salary" => $basicsalary, "companyName" => $companydetail[0]["cname"], "company_address" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData, "cname"=>$cname,"distance"=>$distance, "savedjob"=> $jobsstatus];
                        $data['checkResume'] = $this->User_Model->resume_single($userTokenCheck[0]['id']);
                    
                }
            } 
        $this->load->view('job_listing_new',$data);
        }
    }


        public function find_jobs() {
        if($this->session->userdata('locationdata')) {
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }

        if ($this->session->userdata('usersess')) {
            $userSess = $this->session->userdata('usersess');  
            $tokendata = ['id'=>$userSess['id']];
            $userID = $userSess['id'];

            $userData = $this->User_Model->token_match($tokendata);
            $expmonth = $userData[0]['exp_month'];
            $expyear = $userData[0]['exp_year'];

            $expmonth = $exp_month;
            $expyear = $exp_year;

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";        
            } else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";  
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
            } else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";  
            } else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";  
            } else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";  
            } else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";  
            } else if($expyear >= 7) { 
                $expfilter = "10";
            } else {
                $expfilter = "";
            }

        } else {
            $expfilter = "";
            $userID = 0;
        }

        if(!empty($this->uri->segment(2))) {
            $getParams = $this->uri->segment(2);
            if(!empty($this->uri->segment(3))) {
                $getParamType = base64_decode($this->uri->segment(3));    
            } else {
                $getParamType = 1;
            }
        }else{
            $getParams = "nearby";
            $getParamType = 1;
        }

        $data["hotjobss"] = $this->viewallListings($getParams, $getParamType, $userID, $current_lat, $current_long, $expfilter);

        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data["nations"] = $this->Common_Model->nation_lists();
        $data["powers"] = $this->Common_Model->power_lists();
        $data['loginURL'] = $this->google->loginURL();
        $data['authUrl'] = $this->facebook->login_url();

        $this->load->view('joblists',$data);
    }

    public function viewallListings($getParams, $getParamType, $userID, $userLat, $userLong, $expfilter) {

        if($getParams == "hotjob") {

            $boostjobData = $this->Jobpost_Model->boostjob_fetch_latlong($userID, $expfilter, $userLat, $userLong);
            $data1 = array();
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                if(!empty($jobTop) && count($jobTop)>=1) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                            "modeurl"=> $boostjobsData['modeurl'], 
                    ];
                }
            }
            $hotjobData = $this->Jobpost_Model->hotjob_fetch_latlong($userID, $expfilter, $userLat, $userLong);

            foreach ($hotjobData as $hotjobsData) {

                if(in_array($hotjobsData['id'], $bostArr)) {
                } else {

                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                
                    if(!empty($jobTop) && count($jobTop)>1) {

                          //echo "</pre>";    
                        if(isset($jobTop[0]['picks_id'])){
                            $datatoppicks1 = $jobTop[0]['picks_id']; 
                        } else {
                            $datatoppicks1 = "";
                        }
                        if(isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id']; 
                        } else {
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id']; 
                        } else {
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);

                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                         $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address']))
                        {
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else
                        {
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        
                        
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                        if(isset($jobImage[0]['pic']))
                        {
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else
                        {
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1[] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "salary" => number_format($hotjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $hotjobsData['mode'], 
                        ];
                    }
                }
            }

            return $data1;
        
        } else if($getParams == "company") {

            $company_lists = $this->Newpoints_Model->companysite_lists($userLat, $userLong);
            if(!empty($company_lists)) {
                $x=0;

                foreach ($company_lists as $company_list) {
                    $jobcountfetch = $this->Newpoints_Model->companysite_jobcount($userID, $company_list['id']);
                    if($jobcountfetch > 0) {
                        if(strlen($company_list['companyPic']) > 0) {
                            $companylist[$x] = [
                                  "id" => $company_list['id'],
                                  "image" => $company_list['companyPic'],
                                  "cname" => $company_list['cname'],
                                  "jobcount" => $jobcountfetch
                            ];
                            $x++;
                        }
                    }
                }
            } else {
                $companylist = [];
            }

            $jobcount = array();
            foreach ($companylist as $key => $row)
            {
                $jobcount[$key] = $row['jobcount'];
            }
            array_multisort($jobcount, SORT_DESC, $companylist);
            $data1 = $companylist;

            return $data1;

        } else if($getParams == "expertise") {

            //$hotjobData = $this->Newpoints_Model->categoryjob_fetch_latlong($userID, $expfilter, $userData['typeid'], $userData['cur_lat'], $userData['cur_long']);
            //var_dump($getParamType);die;
            $getParamType = (int)$getParamType;
            $hotjobData = $this->Newpoints_Model->categoryjob_fetch_latlong($userID, $expfilter, $getParamType, $userLat, $userLong);

            $data1 = array();
            foreach ($hotjobData as $hotjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);

                if(isset($jobTop[0]['picks_id'])){
                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                } else {
                    $datatoppicks1 = "";
                }
                if(isset($jobTop[1]['picks_id'])) {
                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                } else {
                    $datatoppicks2 = "";
                }
                if(isset($jobTop[2]['picks_id'])) {
                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                } else {
                    $datatoppicks3 = "";
                }
                
                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);

                if(!empty($savedjob[0]['id'])) {
                    $savedjob = '1';
                } else{
                    $savedjob = "0";
                }

                $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                if($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else{
                    $basicsalary = 0;
                }

                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if(isset($companydetail1[0]['address']))
                {
                    $companyaddress=$companydetail1[0]['address'];
                }else{
                    $companyaddress='';
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                if(isset($jobImage[0]['pic'])){
                    $jobImageData=$jobImage[0]['pic'];
                }else{
                    $jobImageData=$comPic;
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', ''); 

                $data1[] = [
                    "jobpost_id" => $hotjobsData['id'],
                    "comapnyId" =>$hotjobsData['compId'],
                    "job_title" => $hotjobsData['jobtitle'],
                    "jobDesc" => $hotjobsData['jobDesc'],
                    "salary" => number_format($hotjobsData['salary']),
                    "companyName"=> $companydetail[0]["cname"],
                    "cname"=> $cname,
                    "companyAddress"=>$companyaddress,
                    "toppicks1" => $datatoppicks1,
                    "toppicks2" => $datatoppicks2,
                    "toppicks3" => $datatoppicks3,
                    "job_image" =>$jobImageData,
                    "latitude"=> $hotjobsData['latitude'], 
                    "longitude"=>$hotjobsData['longitude'],
                    "jobexpire"=> $hotjobsData['jobexpire'],
                    "companyPic"=> $comPic,
                    "savedjob" =>$savedjob,
                    "boostjob" =>$hotjobsData['boost_status'],
                    "distance" => $distance,
                    "mode"=> $hotjobsData['mode'],
                    "modeurl"=> $hotjobsData['modeurl'],
                ];
            }

            return $data1;
        
        } else if($getParams == "nearby") {
        
            $hotjobData = $this->Newpoints_Model->nearbyjob_fetch_latlong($userID, $userLat, $userLong, $expfilter);
            $data1 = array();
            foreach ($hotjobData as $hotjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);

                if(isset($jobTop[0]['picks_id'])){
                    $datatoppicks1 = $jobTop[0]['picks_id']; 
                } else {
                    $datatoppicks1 = "";
                }
                if(isset($jobTop[1]['picks_id'])) {
                    $datatoppicks2 = $jobTop[1]['picks_id']; 
                } else {
                    $datatoppicks2 = "";
                }
                if(isset($jobTop[2]['picks_id'])) {
                    $datatoppicks3 = $jobTop[2]['picks_id']; 
                } else {
                    $datatoppicks3 = "";
                }
                
                $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);

                if(!empty($savedjob[0]['id'])) {
                    $savedjob = '1';
                } else{
                    $savedjob = "0";
                }

                 $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                if($jobsal[0]['basicsalary']) {
                    $basicsalary = $jobsal[0]['basicsalary'];
                } else{
                    $basicsalary = 0;
                }

                $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                if(isset($companydetail1[0]['address'])) {
                    $companyaddress=$companydetail1[0]['address'];
                }
                else {
                    $companyaddress='';
                }

                $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                
                
                if(!empty($companyPic[0]['companyPic'])) {
                    $comPic = trim($companyPic[0]['companyPic']);
                } else{
                    $comPic = "";
                }
                $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                if(isset($jobImage[0]['pic'])) {
                    $jobImageData=$jobImage[0]['pic'];
                }
                else {
                    $jobImageData=$comPic;
                }

                $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                if(!empty($companyname[0]["cname"])){
                    $cname = $companyname[0]["cname"];
                }else{
                    $cname = '';
                }

                $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                $distance = number_format((float)$distance, 2, '.', ''); 

                $data1[] = [
                    "jobpost_id" => $hotjobsData['id'],
                    "comapnyId" =>$hotjobsData['compId'],
                    "job_title" => $hotjobsData['jobtitle'],
                    "jobDesc" => $hotjobsData['jobDesc'],
                    "salary" => number_format($hotjobsData['salary']),
                    "companyName"=> $companydetail[0]["cname"],
                    "cname"=> $cname,
                    "companyAddress"=>$companyaddress,
                    "toppicks1" => $datatoppicks1,
                    "toppicks2" => $datatoppicks2,
                    "toppicks3" => $datatoppicks3,
                    "job_image" =>$jobImageData,
                    "latitude"=> $hotjobsData['latitude'], 
                    "longitude"=>$hotjobsData['longitude'],
                    "jobexpire"=> $hotjobsData['jobexpire'],
                    "companyPic"=> $comPic,
                    "savedjob" =>$savedjob,
                    "boostjob" =>$hotjobsData['boost_status'],
                    "distance" => $distance,
                    "mode"=> $hotjobsData['mode'],
                    "modeurl"=> $hotjobsData['modeurl'],
                ];
            }

            return $data1;

        } else if($getParams == "toppicks") {
            $hotjobData = $this->Newpoints_Model->feturedtoppicks_fetch_latlong($userID, $expfilter, $userLat, $userLong);

            $data1 = array();

            foreach ($hotjobData as $hotjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks1($hotjobsData['id']);
                $pids = array(); 
                foreach($jobTop as $jobTopp) {
                    $pids[] = $jobTopp['picks_id'];
                }

                if(in_array($getParamType, $pids)) {

                    if(!empty($jobTop) && count($jobTop)>=1) {

                        $jobTopseleted = $this->Jobpost_Model->job_toppicksnotinclude($hotjobsData['id'], $getParamType);
                        $datatoppicks1 = $getParamType; 
                        
                        if(isset($jobTopseleted[0]['picks_id'])) {
                            $datatoppicks2 = $jobTopseleted[0]['picks_id']; 
                        } else{
                            $datatoppicks2 = "";
                        }
                        if(isset($jobTopseleted[1]['picks_id'])) {
                            $datatoppicks3 = $jobTopseleted[1]['picks_id']; 
                        } else{
                            $datatoppicks3 = "";
                        }
                        
                        $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);

                        if(!empty($savedjob[0]['id'])) {
                            $savedjob = '1';
                        } else{
                            $savedjob = "0";
                        }

                        $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);
                        if($jobsal[0]['basicsalary']) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else{
                            $basicsalary = 0;
                        }

                        $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                        if(isset($companydetail1[0]['address']))
                        {
                            $companyaddress=$companydetail1[0]['address'];
                        }
                        else
                        {
                            $companyaddress='';
                        }

                        $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);


                        if(isset($jobImage[0]['pic']))
                        {
                            $jobImageData=$jobImage[0]['pic'];
                        }
                        else
                        {
                            $jobImageData=$comPic;
                        }

                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }

                        $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                        $distance = number_format((float)$distance, 2, '.', ''); 

                        $data1[] = [
                            "jobpost_id" => $hotjobsData['id'],
                            "comapnyId" =>$hotjobsData['compId'],
                            "job_title" => $hotjobsData['jobtitle'],
                            "jobDesc" => $hotjobsData['jobDesc'],
                            "salary" => number_format($hotjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $hotjobsData['latitude'], 
                            "longitude"=>$hotjobsData['longitude'],
                            "jobexpire"=> $hotjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$hotjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $hotjobsData['mode'],
                            "modeurl"=> $hotjobsData['modeurl'],
                        ];
                    }
                }
            }

            return $data1;
        
        } else if($getParams == "Allowance") {
            $hotjobData = $this->Newpoints_Model->feturedtoppicks_fetch_latlong($userID, $expfilter, $userLat, $userLong);

            $data1 = array();
            foreach ($hotjobData as $hotjobsData) {

                $jobTop = $this->Jobpost_Model->job_allowancesbyid($hotjobsData['id'], $getParamType);
                
                if(!empty($jobTop) && count($jobTop)>=1) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else {
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else {
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else {
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($hotjobsData['id'], $userID);

                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                     $jobsal = $this->Jobpost_Model->getExpJob($hotjobsData['id']);

                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($hotjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }
                    else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }
                    $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);

                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }
                    else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $data1[] = [
                        "jobpost_id" => $hotjobsData['id'],
                        "comapnyId" =>$hotjobsData['compId'],
                        "job_title" => $hotjobsData['jobtitle'],
                        "jobDesc" => $hotjobsData['jobDesc'],
                        "salary" => number_format($hotjobsData['salary']),
                        "companyName"=> $companydetail[0]["cname"],
                        "cname"=> $cname,
                        "companyAddress"=>$companyaddress,
                        "toppicks1" => $datatoppicks1,
                        "toppicks2" => $datatoppicks2,
                        "toppicks3" => $datatoppicks3,
                        "job_image" =>$jobImageData,
                        "latitude"=> $hotjobsData['latitude'], 
                        "longitude"=>$hotjobsData['longitude'],
                        "jobexpire"=> $hotjobsData['jobexpire'],
                        "companyPic"=> $comPic,
                        "savedjob" =>$savedjob,
                        "boostjob" =>$hotjobsData['boost_status'],
                        "distance" => $distance,
                        "mode"=> $hotjobsData['mode'],
                        "modeurl"=> $hotjobsData['modeurl'],
                    ];
                }
            }

            return $data1;

        } else if($getParams == "leadership") {

            $boostjobData = $this->Jobpost_Model->catjob_fetch_leadership_all($userID, $expfilter, $userLat, $userLong);

            $data1 = array();
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                if(!empty($jobTop) && count($jobTop)>=0) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat, $userLong, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                            "modeurl"=> $boostjobsData['modeurl'], 
                    ];
                }
            }

            return $data1;

        }  else if($getParams == "information_technology") {

            $boostjobData = $this->Jobpost_Model->catjob_fetch_information_technology_all($userID,$expfilter, $userLat, $userLong);
            $data1 = array();
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                if(!empty($jobTop) && count($jobTop)>=0) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                            "modeurl"=> $boostjobsData['modeurl'], 
                    ];
                }
            }

            return $data1;
        
        }  else if($getParams == "city") {

            $boostjobData = $this->Jobpost_Model->cityjob_fetch($userID, $expfilter, $userLat, $userLong, $getParamType);
            $data1 = array();
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                //if(!empty($jobTop) && count($jobTop)>=0) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                            "modeurl"=> $boostjobsData['modeurl'], 
                    ];
                //}
            }

            return $data1;
        
        }  else if($getParams == "instant_screening") {

            $boostjobData = $this->Jobpost_Model->instantscreening_fetch_all($userID,$expfilter, $userLat, $userLong);
            $data1 = array();
            $bostArr = array();

            foreach ($boostjobData as $boostjobsData) {

                $jobTop = $this->Jobpost_Model->job_toppicks($boostjobsData['id']);
              
                if(!empty($jobTop) && count($jobTop)>=0) {

                    if(isset($jobTop[0]['picks_id'])) {
                        $datatoppicks1 = $jobTop[0]['picks_id']; 
                    } else{
                        $datatoppicks1 = "";
                    }
                    if(isset($jobTop[1]['picks_id'])) {
                        $datatoppicks2 = $jobTop[1]['picks_id']; 
                    } else{
                        $datatoppicks2 = "";
                    }
                    if(isset($jobTop[2]['picks_id'])) {
                        $datatoppicks3 = $jobTop[2]['picks_id']; 
                    } else{
                        $datatoppicks3 = "";
                    }
                    
                    $savedjob = $this->Jobpost_Model->job_saved($boostjobsData['id'], $userID);
                    if(!empty($savedjob[0]['id'])) {
                        $savedjob = '1';
                    } else{
                        $savedjob = "0";
                    }

                    $jobsal = $this->Jobpost_Model->getExpJob($boostjobsData['id']);
                    if($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else{
                        $basicsalary = 0;
                    }

                    $companydetail = $this->Jobpost_Model->company_detail_fetch($boostjobsData['compId']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($boostjobsData['compId']);
                    if(isset($companydetail1[0]['address'])){
                        $companyaddress=$companydetail1[0]['address'];
                    }else{
                        $companyaddress='';
                    }

                    $companyPic = $this->Jobpost_Model->fetch_companyPic($boostjobsData['compId']);
                    if(!empty($companyPic[0]['companyPic'])) {
                        $comPic = trim($companyPic[0]['companyPic']);
                    } else{
                        $comPic = "";
                    }

                    $jobImage = $this->Jobpost_Model->job_image($boostjobsData['id']);
                    if(isset($jobImage[0]['pic'])){
                        $jobImageData=$jobImage[0]['pic'];
                    }else{
                        $jobImageData=$comPic;
                    }

                    $companyname = $this->Jobpost_Model->company_name_fetch($boostjobsData['recruiter_id']);
                    if(!empty($companyname[0]["cname"])){
                        $cname = $companyname[0]["cname"];
                    }else{
                        $cname = '';
                    }

                    $distance = $this->distance($userLat,$userLong,$companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                    $distance = number_format((float)$distance, 2, '.', ''); 

                    $bostArr[] = $boostjobsData['id'];

                    $data1[] = [
                            "jobpost_id" => $boostjobsData['id'],
                            "comapnyId" =>$boostjobsData['compId'],
                            "job_title" => $boostjobsData['jobtitle'],
                            "jobDesc" => $boostjobsData['jobDesc'],
                            "jobPitch"=>$boostjobsData['jobPitch'],
                            "salary" => number_format($boostjobsData['salary']),
                            "companyName"=> $companydetail[0]["cname"],
                            "cname"=> $cname,
                            "companyAddress"=>$companyaddress,
                            "toppicks1" => $datatoppicks1,
                            "toppicks2" => $datatoppicks2,
                            "toppicks3" => $datatoppicks3,
                            "job_image" =>$jobImageData,
                            "latitude"=> $boostjobsData['latitude'], 
                            "longitude"=>$boostjobsData['longitude'],
                            "jobexpire"=> $boostjobsData['jobexpire'],
                            "companyPic"=> $comPic,
                            "savedjob" =>$savedjob,
                            "boostjob" =>$boostjobsData['boost_status'],
                            "distance" => $distance,
                            "mode"=> $boostjobsData['mode'], 
                            "modeurl"=> $boostjobsData['modeurl'], 
                    ];
                }
            }

            return $data1;
        
        }

    }



    public function fetchUserSingleData($id) {
        $userData = $this->User_Model->user_single($id);
        $data = ["token" => $userData[0]['token'], "name" => $userData[0]['name'], "email" => $userData[0]['email'], "type" => $userData[0]['type'], "profilePic" => $userData[0]['profilePic'], "phone" => $userData[0]['phone'], "country_code" => $userData[0]['country_code'], "dob" => $userData[0]['dob'], "location" => $userData[0]['location'], "designation" => $userData[0]['designation'], "current_salary" => $userData[0]['current_salary'], "exp_month" => $userData[0]['exp_month'], "exp_year" => $userData[0]['exp_year'],"education" => $userData[0]['education'],"nationality" => $userData[0]['nationality'],"superpower" => $userData[0]['superpower'],"jobsInterested" => $userData[0]['jobsInterested'],"state" => $userData[0]['state'],"city" => $userData[0]['city']];
        return $data;
    }
    public function fetchWorkSingleData($id) {
        $workDatas = $this->User_Model->work_single($id);
        if (!empty($workDatas)) {
            foreach ($workDatas as $workData) {
                $industryName = $this->User_Model->industryName_single($workData['industry']);
                $data[] = ["id" => $workData['id'], "title" => $workData['title'], "company" => $workData['company'], "desc" => $workData['jobDesc'], "from" => $workData['workFrom'], "to" => $workData['workTo'], "channel" => $workData['channel']];
            }
            return $data;
        } else {
            $data[] = "";
            return $data;
        }
    }
    public function fetchEduSingleData($id) {
        $eduDatas = $this->User_Model->edu_single($id);
        if ($eduDatas) {
            foreach ($eduDatas as $eduData) {
                $data[] = ["id" => $eduData['id'], "attainment" => $eduData['attainment'], "degree" => $eduData['degree'], "university" => $eduData['university'], "from" => $eduData['degreeFrom'], "to" => $eduData['degreeTo']];
            }
            return $data;
        } else {
            $data[] = "";
            return $data;
        }
    }
    public function appliedJobs() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $jobAppliedpasts = $this->Jobpost_Model->jobappliedpast_fetch_byuserid($userTokenCheck[0]['id']);
            if ($jobAppliedpasts) {
                foreach ($jobAppliedpasts as $jobAppliedpast) {
                    $jobImage = $this->Jobpost_Model->job_image($jobAppliedpast['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $jobsal = $this->Jobpost_Model->getExpJob($jobAppliedpast['id']);
                    if ($jobsal[0]['basicsalary']) {
                        $basicsalary = $jobsal[0]['basicsalary'];
                    } else {
                        $basicsalary = "0";
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedpast['company_id']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobAppliedpast['company_id']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedpast['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobAppliedpast['recruiter_id']);
	                    if(!empty($companyname[0]["cname"])){
	                        $cname = $companyname[0]["cname"];
	                    }else{
	                        $cname = '';
	                    }
	                    /*$ip = $_SERVER['REMOTE_ADDR'];
	                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
	                   $data_loc = json_decode($location);
	                   
	                   $Address = $data_loc->city;
	                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
	                   $output = json_decode($geocodeFromAddr);
	                      //print_r($output);die;       
	                   //Get latitude and longitute from json data
	                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
	                   $dataLongitude = $output->results[0]->geometry->location->lng;*/
	                    $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
	                        $distance = number_format((float)$distance, 2, '.', '');
                    $data["jobPast"][] = ["jobpost_id" => $jobAppliedpast['id'], "comapnyId" => $jobAppliedpast['company_id'], "recruiter_id" => $jobAppliedpast['recruiter_id'], "job_title" => $jobAppliedpast['jobtitle'], "jobDesc" => $jobAppliedpast['jobDesc'], "salary" => $basicsalary, "status" => $jobAppliedpast['status'], "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData,"cname"=>$cname, "distance"=>$distance, "jobPitch"=>$jobAppliedpast['jobPitch']];
                }
            } else {
                $data["jobPast"][] = ["jobpost_id" => "", "comapnyId" => "", "recruiter_id" => "", "job_title" => "", "jobDesc" => "", "salary" => "", "status" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "","cname"=>"", "distance"=>"","jobPitch"=>""];
            }
            $jobAppliedupcomings = $this->Jobpost_Model->jobappliedupcoming_fetch_byuserid($userTokenCheck[0]['id']);
            if ($jobAppliedupcomings) {
                foreach ($jobAppliedupcomings as $jobAppliedupcoming) {
                    $jobImage = $this->Jobpost_Model->job_image($jobAppliedupcoming['id']);
                    if (isset($jobImage[0]['pic'])) {
                        $jobImageData = $jobImage[0]['pic'];
                    } else {
                        $jobImageData = '';
                    }
                    $jobsal1 = $this->Jobpost_Model->getExpJob($jobAppliedupcoming['id']);
                    if ($jobsal1[0]['basicsalary']) {
                        $basicsalary1 = $jobsal1[0]['basicsalary'];
                    } else {
                        $basicsalary1 = "0";
                    }
                    $companydetail = $this->Jobpost_Model->company_detail_fetch($jobAppliedupcoming['company_id']);
                    $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($jobAppliedupcoming['company_id']);
                    if (isset($companydetail1[0]['address'])) {
                        $companyaddress = $companydetail1[0]['address'];
                    } else {
                        $companyaddress = '';
                    }
                    $jobTop = $this->Jobpost_Model->job_toppicks($jobAppliedupcoming['id']);
                    if ($jobTop) {
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                    } else {
                        $datatoppicks1 = "";
                        $datatoppicks2 = "";
                        $datatoppicks3 = "";
                    }
                    $companyname = $this->Jobpost_Model->company_name_fetch($jobAppliedupcoming['recruiter_id']);
	                    if(!empty($companyname[0]["cname"])){
	                        $cname = $companyname[0]["cname"];
	                    }else{
	                        $cname = '';
	                    }
	                    $ip = $_SERVER['REMOTE_ADDR'];
	                   $location = file_get_contents('http://ip-api.com/json/'.$ip);
	                   $data_loc = json_decode($location);
	                   
	                   $Address = $data_loc->city;
	                   $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
	                   $output = json_decode($geocodeFromAddr);
	                      //print_r($output);die;       
	                   //Get latitude and longitute from json data
	                   $dataLatitude  = $output->results[0]->geometry->location->lat; 
	                   $dataLongitude = $output->results[0]->geometry->location->lng;
	                    $distance = $this->distance($dataLatitude, $dataLongitude, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
	                        $distance = number_format((float)$distance, 2, '.', '');
                    $data["jobUpcoming"][] = ["jobpost_id" => $jobAppliedupcoming['id'], "comapnyId" => $jobAppliedupcoming['company_id'], "recruiter_id" => $jobAppliedupcoming['recruiter_id'], "job_title" => $jobAppliedupcoming['jobtitle'], "jobDesc" => $jobAppliedupcoming['jobDesc'], "salary" => $basicsalary1, "interviewdate" => $jobAppliedupcoming['interviewdate'], "interviewtime" => date("H:i", strtotime($jobAppliedupcoming['interviewtime'])), "status" => $jobAppliedupcoming['status'], "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData,"cname"=>$cname, "distance"=>$distance, "jobPitch"=>$jobAppliedupcoming['jobPitch']];
                }
            } else {
                $data["jobUpcoming"][] = ["jobpost_id" => "", "comapnyId" => "", "recruiter_id" => "", "job_title" => "", "jobDesc" => "", "salary" => "", "status" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "", "interviewdate" => "", "interviewtime" => "", "cname"=>"", "distance" => "", "jobPitch"=>""];
            }
            $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
             if($notification_list){
                $data['badge'] = count($notification_list);
             }
            $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
            $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
            $this->load->view('appliedjobs', $data);
        }
    }
    public function savedjovData() {
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        $job_id = $this->input->post('jobId');
        if ($userTokenCheck) {
            $condjobStaTUS = array('jobpost_id' => $job_id, 'user_id' => $userTokenCheck[0]['id']);
            $checkjobByuser = $this->User_Model->checkjobstatus($condjobStaTUS);
            if (!empty($checkjobByuser)) {
                $condjobStaTUS = array('jobpost_id' => $job_id, 'user_id' => $userTokenCheck[0]['id']);
                if ($checkjobByuser[0]['status'] == 0) {
                    $data = array('status' => 1);
                    $insertjobData = $this->Jobpost_Model->savejob_update($condjobStaTUS, $data);
                    echo 1;
                } else {
                    $data = array('status' => 0);
                    $insertjobData = $this->Jobpost_Model->savejob_update($condjobStaTUS, $data);
                    echo 2;
                }
            } else {
                $jobsave = ["jobpost_id" => $job_id, "user_id" => $userTokenCheck[0]['id'], "status" => 1];
                $insertjobData = $this->Jobpost_Model->savejob_insert($jobsave);
                echo 3;
            }
        }
    }

    public function savedinstantData() {
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        $job_id = $this->input->post('jobId');

        if ($userTokenCheck) {
        
            $condjobStaTUS = array('jobpost_id' => $job_id, 'user_id' => $userTokenCheck[0]['id']);
            $checkjobByuser = $this->Jobpost_Model->checkinstantstatus($condjobStaTUS);
            if ($checkjobByuser) {
                echo 2;
            } else {
                $jobsave = ["jobpost_id" => $job_id, "user_id" => $userTokenCheck[0]['id'], "status" => 1];
                $insertjobData = $this->Jobpost_Model->saveinstant_insert($jobsave);
                echo 1;
            }
        } else {
            echo 3;
        }
    }

    public function companyProfile() {
        if($this->session->userdata('locationdata')){
            $locationdata = $this->session->userdata('locationdata');
            $current_lat = $locationdata['current_lat'];
            $current_long = $locationdata['current_long'];
        }else{
            $current_lat='14.5676285';
            $current_long= '120.7397551';
        }
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $expmonth = $userTokenCheck[0]['exp_month'];
            $expyear = $userTokenCheck[0]['exp_year'];

            if($expyear == 0 && $expmonth == 0) {
                $expfilter = "1";
                
            }else if($expyear == 0 && $expmonth < 6) {
                $expfilter = "2";
                
            } else if($expyear < 1 && $expmonth >= 6) {
                $expfilter = "3";
                
            } else if($expyear < 2 && $expyear >= 1) {
                $expfilter = "4";
                
            } else if($expyear < 3 && $expyear >= 2) {
                $expfilter = "5";
                
            }else if($expyear < 4 && $expyear >= 3) {
                $expfilter = "6";
                
            }else if($expyear < 5 && $expyear >= 4) {
                $expfilter = "7";
                
            }else if($expyear < 6 && $expyear >= 5) {
                $expfilter = "8";
                
            }else if($expyear < 7 && $expyear >= 6) {
                $expfilter = "9";
                
            } else if($expyear >= 7) { 
                $expfilter = "10";
                
            }
            $company_id = base64_decode($this->uri->segment(3));
            //echo $company_id;die;
            $data['company_pic'] = $this->Jobpost_Model->fetch_companyPic($company_id);
            $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
            $data['cname'] = $this->Jobpost_Model->company_detail_fetch($company_id);
            $jobDetails = $this->Jobpost_Model->fetch_jobDetail($company_id,$expfilter);
            //print_r($jobDetails);die;
            if ($jobDetails) {
                foreach ($jobDetails as $hotjobsData) {
                    $jobTop = $this->Jobpost_Model->job_toppicks($hotjobsData['id']);
                        if (isset($jobTop[0]['picks_id'])) {
                            $datatoppicks1 = $jobTop[0]['picks_id'];
                        } else {
                            $datatoppicks1 = "";
                        }
                        if (isset($jobTop[1]['picks_id'])) {
                            $datatoppicks2 = $jobTop[1]['picks_id'];
                        } else {
                            $datatoppicks2 = "";
                        }
                        if (isset($jobTop[2]['picks_id'])) {
                            $datatoppicks3 = $jobTop[2]['picks_id'];
                        } else {
                            $datatoppicks3 = "";
                        }
                        $companyPic = $this->Jobpost_Model->fetch_companyPic($company_id);
                                
                        if(!empty($companyPic[0]['companyPic'])) {
                            $comPic = trim($companyPic[0]['companyPic']);
                        } else{
                            $comPic = "";
                        }
                        $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                        if (isset($jobImage[0]['pic'])) {
                            $jobImageData = $jobImage[0]['pic'];
                        } else {
                            $jobImageData = $comPic;
                        }
                        
                        $jobsal = $this->Jobpost_Model->getExpJob1($hotjobsData['id'],$expfilter);
                        if (!empty($jobsal[0]['basicsalary'])) {
                            $basicsalary = $jobsal[0]['basicsalary'];
                        } else {
                            $basicsalary = "0";
                        }
                        $companydetail = $this->Jobpost_Model->company_detail_fetch($company_id);
                        //print_r($companydetail);die;
                        $companydetail1 = $this->Jobpost_Model->company_detail1_fetch($company_id);
                        if (isset($companydetail1[0]['address'])) {
                            $companyaddress = $companydetail1[0]['address'];
                        } else {
                            $companyaddress = '';
                        }
                        $companyname = $this->Jobpost_Model->company_name_fetch($hotjobsData['recruiter_id']);
                        if(!empty($companyname[0]["cname"])){
                            $cname = $companyname[0]["cname"];
                        }else{
                            $cname = '';
                        }
                        /*$ip = $_SERVER['REMOTE_ADDR'];
                       $location = file_get_contents('http://ip-api.com/json/'.$ip);
                       $data_loc = json_decode($location);
                       
                       $Address = $data_loc->city;
                       $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
                       $output = json_decode($geocodeFromAddr);
                          //print_r($output);die;       
                       //Get latitude and longitute from json data
                       $dataLatitude  = $output->results[0]->geometry->location->lat; 
                       $dataLongitude = $output->results[0]->geometry->location->lng;*/
                        $distance = $this->distance($current_lat, $current_long, $companydetail1[0]['latitude'], $companydetail1[0]['longitude'], "K");
                            $distance = number_format((float)$distance, 2, '.', '');
                            //$distance = '';
                            $companyPic = $this->Jobpost_Model->fetch_companyPic($hotjobsData['recruiter_id']);
                            
                            if(!empty($companyPic[0]['companyPic'])) {
                                $comPic = trim($companyPic[0]['companyPic']);
                            } else{
                                $comPic = "";
                            }
                            $jobImage = $this->Jobpost_Model->job_image($hotjobsData['id']);
                            if (isset($jobImage[0]['pic'])) {
                                $jobImageData = $jobImage[0]['pic'];
                            } else {
                                $jobImageData = $comPic;
                            }
                        $data["jobDetails"][] = ["id" => $hotjobsData['id'], "company_id" => $company_id, "jobtitle" => $hotjobsData['jobtitle'], "jobDesc" => $hotjobsData['jobDesc'], "salary" => $hotjobsData['salary'], "companyName" => $companydetail[0]["cname"], "companyAddress" => $companyaddress, "toppicks1" => $datatoppicks1, "toppicks2" => $datatoppicks2, "toppicks3" => $datatoppicks3, "job_image" => $jobImageData,"jobPitch"=>$hotjobsData['jobPitch'], "cname"=>$cname, "distance" =>$distance, 'jobImage' => $jobImageData ,"mode"=>$hotjobsData['mode'],"modeurl"=>$hotjobsData['modeurl']];
                    
                }
            } else {
                $data["jobDetails"][] = ["id" => "", "company_id" => "", "jobtitle" => "", "jobDesc" => "", "salary" => "", "companyName" => "", "companyAddress" => "", "toppicks1" => "", "toppicks2" => "", "toppicks3" => "", "job_image" => "", "save_status" => "", 'jobImage'=> '', 'cname'=> '', 'distance'=>'', 'jobPitch' => '', "mode"=>"", "modeurl"=>""];
            }
            $notification_list = $this->Common_Model->unreadnotification($userTokenCheck[0]['id']); 
            if($notification_list){
                $data['badge'] = count($notification_list);
             }
            $data['review_list'] = $this->Jobpost_Model->review_lists($company_id);
            $data['glassdoor_review'] = $this->Jobpost_Model->fetch_gcompanyRating($company_id);
            $data['company_toppicks'] = $this->Jobpost_Model->recruiter_toppickss($company_id);
            $data['checkResume'] = $this->User_Model->resume_single($userTokenCheck[0]['id']);
            $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
            $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
            $data['companyDesc'] = $this->Jobpost_Model->company_detail1_fetch($hotjobsData['recruiter_id']);
           // print_r($data);die;
            $this->load->view('companyprofile', $data);
        }
    }

    public function contactUs(){
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        //print_r($userTokenCheck);die;
        if ($userTokenCheck) {
            $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
            $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
            $data['email'] = $userTokenCheck[0]['email'];
            $this->load->view('contactUs',$data);
        }
    }

    public function signinBonus(){
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        //print_r($userTokenCheck);die;
        if ($userTokenCheck) {
            $bonusamount =  $this->User_Model->fetch_bonusamount($userTokenCheck[0]['id']);
            $data['bonus_amount'] = $bonusamount[0]['sign_in_bonus'];
            $bonushistorys =  $this->User_Model->fetch_bonushistory($userTokenCheck[0]['id']);
            if (!empty($bonushistorys)) {
                foreach ($bonushistorys as $bonushistory) {
                    $data['bonus_history'][] = ["id"=>$bonushistory['id'], "bonus"=>$bonushistory['bonus'], "notification"=>$bonushistory['notification'], "added_at"=>date("d-M-Y h:i",strtotime($bonushistory['added_at']))];
                }
            } else {
                $data['bonus_history'][] = ["id" => "", "bonus" => "", "notification" => "", "added_at" => ""];
            }
        }
        $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
        $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
        $this->load->view('signinBonus',$data);
    } 

    public function aboutUs(){
        $userSess = $this->session->userdata('usersess');
        $data = ["id" => $userSess['id']];
        $userTokenCheck = $this->User_Model->user_match($data);
        if ($userTokenCheck) {
            $data['content'] = $this->Common_Model->about_us();
            $data['getCompletenes'] = $this->fetchUserCompleteData($userTokenCheck[0]['id']);
            $data['checkStatus'] = $this->User_Model->user_single($userTokenCheck[0]['id']);
            $this->load->view('aboutus',$data);
        }
    }

    public function fetchUserCompleteData($id) {
        $userData = $this->User_Model->userComplete_single($id);
        if ($userData) {
            $data['comp'] = $userData[0]['signup'] + $userData[0]['profile'] + $userData[0]['profilePic'] + $userData[0]['resume'] + $userData[0]['experience'] + $userData[0]['education'] + $userData[0]['assessment'] + $userData[0]['language'] + $userData[0]['expert'] + $userData[0]['topClient'];
        } else {
            $data['comp'] = 0;
        }
        return $data;
    }
    public function fetchLangSingleData($id) {
        $langDatas = $this->User_Model->lang_single($id);
        if ($langDatas) {
            foreach ($langDatas as $langData) {
                $data[] = ["id" => $langData['id'], "lang_name" => $langData['name']];
            }
            return $data;
        } else {
            $data = [];
            return $data;
        }
    }
    public function fetchClientSingleData($id) {
        $clientDatas = $this->User_Model->client_single($id);
        if ($clientDatas) {
            return $clientDatas;
        } else {
            $data = [];
            return $data;
        }
    }
    public function distance($lat1, $lon1, $lat2, $lon2, $unit) 
            {

            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) *sin(deg2rad($lat2)) + cos(deg2rad($lat1))* cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist* 60 *1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
            return ($miles * 1.609344);
            } else if ($unit == "N") {
            return ($miles * 0.8684);
            } else {
            return $miles;
            }
    }

    public function SendAwsSms($to,$sms_message){
            // require 'vendor/autoload.php';
            // //echo 'hi';die;
            // $params = array('credentials' => array('key' => 'AKIA2PEGBF6OC7RXCBWM', 'secret' => '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',), 'region' => 'us-west-2', 'version' => 'latest');
            // $sns = new \Aws\Sns\SnsClient($params);
            // $args = array(
            // "SenderID"=>"JobYoDA",
            // "SMSType"=>"Transactional",
            // "Message"=>$sms_message,
            // "PhoneNumber"=>$to,
            // );
            // //print_r($args);die;
            // $result = $sns->publish($args);
            //print_r($result);
            $result = true;
        }
}
?>