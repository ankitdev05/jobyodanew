<?php
ob_start();
class Homejob_Model extends CI_Model {

   function __construct() { 
    	parent::__construct(); 
   }

   public function job_fetch_homeforappnearby_groupby($id, $userLat, $userLong, $expfilter, $companyidsarr) {
		$userLat = (double)$userLat;
		$userLong = (double)$userLong;

		$cDate = date('Y-m-d');
		$uniquearraybyother = array();
		if(strlen($expfilter) > 0) {
   		for($ityu = $expfilter; $ityu >-1; $ityu--) {
   		    $uniquearraybyother[] = $ityu;
   		}
		}

		$this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

		$this->db->from('job_posting');
		$this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
		$this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
		$this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');

      $this->db->where("job_posting.jobexpire >=", $cDate);
		$this->db->where("job_posting.recruiter_id", $companyidsarr);
		if($id != 0) {
		    $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
		}
		if(strlen($expfilter) > 0) {
		    $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
		}

		$this->db->having("distance <","7000");
		$this->db->order_by("distance",'ASC');
		$this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(1);
		$query = $this->db->get();
		return $query->result_array();
   }


   public function hotjob_fetch_latlongbylimit_groupby($id,$expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.recruiter_id", $companyidsarr);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function catjob_fetch_information_technology($id,$expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 22);
      $this->db->where("job_posting.recruiter_id", $companyidsarr);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function catjob_fetch_leadership($id,$expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", 24);
      $this->db->where("job_posting.recruiter_id", $companyidsarr);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
    }

   public function feturedtoppicks_fetch_latlong_bonus_groupby($id, $expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",1);
      $this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_bonus($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",1);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function feturedtoppicks_fetch_latlong_freefood_groupby($id, $expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",2);
      $this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   	}

   	public function feturedtoppicks_fetch_latlong_bonus_freefood($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",2);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_dayihmo_groupby($id, $expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",3);
      $this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_bonus_dayihmo($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",3);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_shift_groupby($id, $expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",5);
      $this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_bonus_shift($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",5);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   	}

   public function feturedtoppicks_fetch_latlong_monthpay_groupby($id, $expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",6);
      $this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_bonus_monthpay($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",6);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_workfromhome_groupby($id, $expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",7);
      $this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_bonus_workfromhome($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",7);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_allowances_groupby($id, $expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_allowances.allowances_id",7);
      $this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_allowances.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong_bonus_allowances($id, $expfilter, $userLat, $userLong) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_allowances.allowances_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_allowances.allowances_id",7);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_allowances.jobpost_id");
      $this->db->limit(20);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function instantscreening_fetch_groupby($id, $expfilter, $userLat, $userLong, $companyidsarr) {
      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++){
            $uniquerarray[] = $ikl;
         }
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if($id != 0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }$this->db->where("job_posting.mode", 'Instant screening');
      
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_posting.recruiter_id", $companyidsarr);
      $this->db->having("distance<", '7000');
      $this->db->order_by('distance','asc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_all_groupby($id, $expfilter, $userLat, $userLong, $companyidsarr, $getParamType) {

      $cDate = date('Y-m-d');
      $uniquerarray = array();
      if(strlen($expfilter) > 0){
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch,job_posting.mode, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id ,job_toppicks.picks_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      if($id!=0) {
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
      if(strlen($expfilter) > 0){
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->where("job_toppicks.picks_id",$getParamType);
      $this->db->where("job_posting.recruiter_id",$companyidsarr);
      $this->db->having("distance<", '6000');
      $this->db->order_by('distance','ASC');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->group_by("job_toppicks.jobpost_id");
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

}