<?php
ob_start();
class Common_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   public function industry_lists() {
      $query = $this->db->get('industry_lists');
      return $query->result_array();
   }
   
   public function job_title_list() {
      $query = $this->db->get('job_titles');
      return $query->result_array();
   }

   public function user_list() {
    $this->db->where('phone!=','');
      $query = $this->db->get('user');
      return $query->result_array();
   }
   

   public function channel_lists() {
      $query = $this->db->get('channel_lists');
      return $query->result_array();
   }

   public function about_us() {
    $this->db->where('title','about us');
      $query = $this->db->get('static_content
');
      return $query->result_array();
   }

   public function language_lists() {
      $this->db->select("id, name");
      $this->db->order_by("name","ASC");
      $query = $this->db->get('languages');
      return $query->result_array();
   }

   public function phonecode_lists() {
      $this->db->select("name,phonecode");
      $query = $this->db->get('country');
      return $query->result_array();
   }

   public function nation_lists() {
      $this->db->select("*");
      $query = $this->db->get('nationality');
      return $query->result_array();
   }

   public function power_lists() {
      $this->db->select("*");
      $query = $this->db->get('super_power');
      return $query->result_array();
   }

   public function region_lists() {
      $this->db->select("id,region");
      $query = $this->db->get('region');
      return $query->result_array();
   }
   
   public function image_upload($image, $type) {
      $UPLOAD_DIR = "images/";
       //$image_parts = explode(";base64,", $image);
       //$image_type_aux = explode("image/", $image_parts[0]);
       //$image_type = $image_type_aux[1];
       $image_type = $type;
       //$image_base64 = base64_decode($image_parts[1]);
       $image_base64 = base64_decode($image);
       $file = $UPLOAD_DIR . uniqid() . '.'.$image_type;
       file_put_contents($file, $image_base64);
       $file = base_url() . $file;
       return $file;
   }
    public function level_lists() {
      $this->db->select("*");
      $this->db->from('job_levels');
      $query = $this->db->get();
      return $query->result_array();
   }

  public function category_lists() {
      $this->db->select("*");
      $this->db->from('job_category');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function company_lists() {
      $this->db->select("*");
      $this->db->from('recruiter');
      $this->db->where('parent_id!=',0);
      $this->db->where('status',1);
      $this->db->where_in('label', [0,2]);
      $this->db->order_by('cname','ASC');
      //$this->db->group_by('cname');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function companyname_lists($searchTerm) {
      $this->db->select("*");
      $this->db->from('recruiter');
      $this->db->where('parent_id!=',0);
      $this->db->where('status',1);
      $this->db->where_in('label', [0,2]);
      $this->db->like('cname',$searchTerm);
      //$this->db->group_by('cname');
      $query = $this->db->get();
      return $query->result_array();
   }
   public function check_jobs($cid) {
      $cDate = date('y-m-d');
      $this->db->select("*");
      $this->db->from('job_posting');
      $this->db->where("company_id", $cid);
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function subcategory_listsbycatid($catid){
      $this->db->select('*')
         ->from('job_subcategory')
         ->where("category_id", $catid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function subcategory_listsbysubcatid($catid){
      $this->db->select('*')
         ->from('job_subcategory')
         ->where("id", $catid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function filtersubcategory_listsbycatid($catid) {
       $cids = explode(',', $catid);
      $this->db->select('*')
         ->from('job_subcategory')
         ->where_in("category_id", $cids)
         ->group_by('subcategory');
      $query = $this->db->get();
      return $query->result_array();
   }
  public function subcategory_lists(){
      $this->db->select('*')
         ->from('job_subcategory')
         ->group_by('subcategory');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function file_upload($file, $type) {
       $UPLOAD_DIR = "files/";
       $image_base64 = base64_decode($file);
       $file = $UPLOAD_DIR . uniqid() . '.'.$type;
       file_put_contents($file, $image_base64);
       $file = base_url() . $file;
       return $file;
   }

   public function company_address_lists($id) {
      $this->db->select("address");
      $this->db->where("recruiter_id", $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }

   public function company_address_location($id) {
      $this->db->select("address, city, latitude, longitude");
      $this->db->where("recruiter_id", $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }

   public function fetch_site_name($id) {
      $this->db->select("cname,salary_status");
      $this->db->where("id", $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function companysite_address_lists($id) {
      $this->db->select("recruiter_details.site_name, recruiter.cname, recruiter_details.recruiter_id");
      $this->db->from('recruiter_details');
      $this->db->join('recruiter', "recruiter.id = recruiter_details.recruiter_id");
      $this->db->where("recruiter.parent_id", $id);
      $this->db->where("recruiter.label!=", 3);
      $this->db->order_by("recruiter.cname");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function companysite_lists() {
      $this->db->select("recruiter_details.site_name, recruiter.cname, recruiter_details.recruiter_id");
      $this->db->from('recruiter_details');
      $this->db->join('recruiter', "recruiter.id = recruiter_details.recruiter_id");
      $this->db->where("recruiter.parent_id!=",0 );
      $this->db->where("recruiter.label!=", 3);
      $this->db->order_by("recruiter.cname");
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function getCompJobTitle() {
      $query = $this->db->get('job_titles');
      return $query->result_array();
   }

   public function getJobs() {
      $this->db->select('job_posting.id, job_posting.company_id, job_posting.jobtitle, job_posting.experience, recruiter_details.address, recruiter.cname')
         ->from('job_posting')
         ->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function getnotification($userid) {
      $this->db->select("id,jobapp_id,notification,status_changed, status,recruiter_id,job_status,createdon");
      $this->db->where("user_id", $userid);
      $this->db->order_by("id", 'desc');
      $query = $this->db->get('notifications');
      return $query->result_array();
   }

   public function getnotificationjob($userid) {
      $this->db->select("id,job_id,title,notification,status,read_status,recruiter_id,created_at");
      $this->db->where("user_id", $userid);
      $this->db->where("status", 2);
      $this->db->order_by("id", 'desc');
      $query = $this->db->get('jobs_notify');
      return $query->result_array();
   }

   public function getpromonotification($userid) {
      $this->db->select("*");
      $this->db->where("user_id", $userid);
      $this->db->where("save_status", 1);
      $this->db->order_by("id", 'desc');
      $query = $this->db->get('promo_notifications');
      return $query->result_array();
   }
   
   public function unreadnotification($userid) {
      $this->db->select("id,jobapp_id,notification,status_changed, status,recruiter_id,job_status,createdon");
      $this->db->where("user_id", $userid);
      $this->db->where("status", '2');
      $this->db->order_by("id", 'desc');
      $query = $this->db->get('notifications');
      return $query->result_array();
   }

   public function popular_category() {
      $cDate = date('y-m-d');
      $this->db->select("job_category.category, job_category.id , COUNT(job_posting.id) as jobcount");
      $this->db->from('job_category');
      $this->db->join("job_posting", "job_posting.category = job_category.id");
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->group_by('job_posting.category');
      $this->db->order_by('rand()');
      $this->db->limit(4);
      $query = $this->db->get();
      return $query->result_array();
   }


    public function hotjob_fetch_latlongg($cat) {
      //echo $cat;die;
      $cDate = date('y-m-d');
      $this->db->select("job_posting.id,job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.salary, job_posting.jobexpire, job_posting.recruiter_id, job_posting.mode, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, COUNT(recruiter_details.latitude) as totaljobs");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);

      if($cat>0){
        $this->db->where("job_posting.category", $cat);
      }
      $this->db->group_by("recruiter_details.latitude");
      $this->db->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }

    public function recentjob_fetch_latlongg() {
      $cDate = date('y-m-d');
      $this->db->select("job_posting.id,job_posting.jobtitle, job_posting.jobDesc, job_posting.jobPitch , job_posting.boost_status ,job_posting.salary, job_posting.jobexpire, job_posting.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, recruiter.id as compId, COUNT(recruiter_details.latitude) as totaljobs");
      $this->db->from('recruiter_details');
      $this->db->join('job_posting', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->group_by("recruiter_details.latitude");
      $this->db->limit(4);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function skill_lists() {
      $this->db->select("*");
      $query = $this->db->get('skills');
      return $query->result_array();
   }

} 
?>
