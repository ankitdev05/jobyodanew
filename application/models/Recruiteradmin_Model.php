<?php
ob_start();
class Recruiteradmin_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   public function recruiter_lists() {
      $this->db->where('parent_id', 0);
      $this->db->where('status', 1);
      $this->db->where('label!=', 3);
      $this->db->order_by('id','desc');
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function recruitercost_lists() {
      $this->db->select("recruiter.cname, recruiter.id, hire_cost.cost, hire_cost.effective_date, hire_cost.billing_date ");
      $this->db->from("recruiter");
      $this->db->join("hire_cost","hire_cost.company_id = recruiter.id", 'left');
      $this->db->where('parent_id', 0);
      $this->db->where('status', 1);
      $this->db->where('label!=', 3);
      $this->db->order_by('hire_cost.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function pending_recruiters() {
      $this->db->where('parent_id', 0);
      $this->db->where('status', 0);
      $this->db->order_by('id','desc');
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function recruitercost_single($id){
      $this->db->where('company_id',$id);
      $query = $this->db->get('hire_cost');
      return $query->result_array();
   }

   public function specialcost_single($id){
      $this->db->where('company_id',$id);
      $query = $this->db->get('special_profile');
      return $query->result_array();
   }

   public function specialprofile_delete($id) {
      $this->db->where('company_id', $id);
      $delete = $this->db->delete("special_profile");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function special_profile_insert($data) {
      if($this->db->insert_batch("special_profile", $data)) {
         return true;
      } else{
         return false;
      }
   }

   public function all_stories() {
      $query = $this->db->get('success_stories');
      return $query->result_array();
   }
   
   public function companysite_lists($id) {
      $this->db->select("recruiter_details.companyPic, recruiter.email, recruiter_details.companyDesc, recruiter_details.address, recruiter_details.phone, recruiter.cname, recruiter.fname, recruiter.lname, recruiter.id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('parent_id', $id);
      $this->db->where('label!=', 3);
      $this->db->order_by('recruiter.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function subrecruiter_lists($id) {
      $this->db->select("recruiter_details.companyPic,recruiter.email, recruiter_details.companyDesc, recruiter_details.address, recruiter_details.phone, recruiter.cname,recruiter.fname, recruiter.lname, recruiter.id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('parent_id', $id);
      $this->db->where('label', 3);
      $this->db->order_by('recruiter.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }
   
    public function companysite_details_single($id) {
      $this->db->select("recruiter_details.companyPic, recruiter_details.dayfrom, recruiter_details.dayto, recruiter_details.from_time, recruiter_details.to_time, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id,recruiter.parent_id, recruiter_details.recruiter_id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('recruiter.id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function company_sites($id) {
      $this->db->select("recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('recruiter.id', $id);
      $this->db->order_by('recruiter.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }
   public function companyaddress($id) {
      $this->db->select("recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('recruiter_details.recruiter_id', $id);
      $this->db->order_by('recruiter.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }
   
   
   /*public function companysite_lists($id) {
      $this->db->where('parent_id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }*/
   
   public function companysite_detailsget($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }
   
   public function admin_listing() {
      $query = $this->db->get('admin');
      return $query->result_array();
   }

   public function boost_amount() {
      $query = $this->db->get('boost_amount');
      return $query->result_array();
   }
   
   public function candidatesLists() {
      $this->db->where('phone!=',0);
      $this->db->order_by('id','desc');
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function fetchspecial($compid,$catid) {
      $this->db->where('company_id',$compid);
      $this->db->where('special_category',$catid);
      $this->db->order_by('id','desc');
      $query = $this->db->get('special_profile');
      return $query->result_array();
   }

   public function hiredcandidates_lists($rid) {
      $this->db->select('job_posting.id, job_posting.jobtitle,job_posting.company_id,job_posting.recruiter_id, job_posting.allowance,job_posting.subcategory, applied_jobs.user_id, recruiter.cname,recruiter.fname, recruiter.lname, recruiter.email, recruiter_details.recruiter_email,user.id as uid ,user.name, user.current_salary, user.exp_month, user.exp_year, applied_jobs.status,applied_jobs.created_at, recruiter_details.address,applied_jobs.id as app_id, applied_jobs.updated_at, applied_jobs.special_price')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->join('recruiter_details', 'recruiter.id = recruiter_details.recruiter_id')
         ->where('applied_jobs.status', '7')
         ->where('job_posting.recruiter_id', $rid)
         ->where('payment', '0')
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function recruiter_lists_approved() {
      $this->db->where('parent_id', 0);
      $this->db->where('status', 1);
      $this->db->order_by('id','desc');
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function recruiter_stepone_insert($data) {
      if($this->db->insert("recruiter", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function advertise_insert($data) {
      if($this->db->insert("advertise_list", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function videoadvertise_insert($data) {
      if($this->db->insert("videoadvertise_list", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function newsadvertise_insert($data) {
      if($this->db->insert("news_list", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   public function recruiter_steptwo_insert($data) {
      if($this->db->insert("recruiter_details", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function recruiter_login($data) {
      $this->db->where($data);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }
   public function recruiter_single($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }
   public function password_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('recruiter', $data);
      return true;
   }

   public function advertise_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('advertise_list', $data);
      return true;
   }
   public function videoadvertise_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('videoadvertise_list', $data);
      return true;
   }
   public function newsadvertise_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('news_list', $data);
      return true;
   }

   public function payment_update($data, $uid, $jid) {
      $this->db->where('user_id', $uid);
      $this->db->where('jobpost_id', $jid);
      $this->db->update('applied_jobs', $data);
      return true;
   }

   public function price_update($data, $aid) {
      $this->db->where('id', $aid);
      $this->db->update('applied_jobs', $data);
      return true;
   }

    public function job_payment_update($data, $uid, $jid) {
      $this->db->where('company_id', $uid);
      $this->db->where('id', $jid);
      $this->db->update('job_posting', $data);
      return true;
   }


   // Company
   public function company_single($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }
   public function company_details_single($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }

    public function recruiter_stepone_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('recruiter', $data);
      return true;
   }

    public function recruiter_steptwo_update($data, $id) {
      $this->db->where('recruiter_id', $id);
      $this->db->update('recruiter_details', $data);
      return true;
   }

   public function delete_recruiter($id) {
      $this->db->where('id', $id);
      $this->db->or_where('parent_id', $id);
      $this->db->delete('recruiter');
      return true;
   }

   public function delete_comment($id) {
      $this->db->where('id', $id);
      $this->db->delete('story_comments');
      return true;
   }

   public function delete_jobs($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('job_posting');
      return true;
   }
   public function delete_advertise($id) {
      $this->db->where('company_id', $id);
      $this->db->delete('advertise_list');
      return true;
   }

   public function delete_notifications($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('notifications');
      return true;
   }

   public function delete_reviews($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('reviews');
      return true;
   }

   public function delete_comapny_rating($id) {
      $this->db->where('company_id', $id);
      $this->db->delete('comapny_rating');
      return true;
   }
   public function delete_recruiterDetails($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('recruiter_details');
      return true;
   }
   public function delete_recruiterAddExp($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('recruiter_addexp');
      return true;
   }
   public function delete_recruiterallowances($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('recruiter_allowances');
      return true;
   }
   public function delete_recruitermedical($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('recruiter_medical');
      return true;
   }
   public function delete_recruitertoppicks($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('recruiter_toppicks');
      return true;
   }
   public function delete_recruiterworkshift($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('recruiter_workshift');
      return true;
   }

   public function forgotPass_check($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_forgot');
      return $query->result_array();
   }
   
   public function forgotPass_insert($data) {
      if($this->db->insert("user_forgot", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function forgotPass_update($data, $id) {
      $this->db->where('user_id', $id);
      $this->db->update('user_forgot', $data);
      return true;
   }
   
   public function recruiterStatus($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('recruiter', $data);
      return true;
   }

   public function adStatus($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('advertise_list', $data);
      return true;
   }

   public function videoStatus($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('videoadvertise_list', $data);
      return true;
   }
   
   public function jobtitle_insert($data) {
      if($this->db->insert("job_titles", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function adprice_insert($data) {
      $this->db->where('id',1);
      $this->db->update("advertise_amount", $data);
      return true;
   }

   public function advideoprice_insert($data) {
      $this->db->where('id',2);
      $this->db->update("advertise_amount", $data);
      return true;
   }

   public function refercode_insert($data) {
      if($this->db->insert("referral_codes", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function hirecost_insert($data) {
      if($this->db->insert("hire_cost", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function hirecost_update($data,$id){
      $this->db->where('company_id',$id);
      $this->db->update('hire_cost',$data);
      return true;
   }
   public function staticontent_insert($data) {
      if($this->db->insert("static_content", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function faq_insert($data) {
      if($this->db->insert("faq", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function testimonial_insert($data) {
      if($this->db->insert("testimonials", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function invoice_insert($data) {
      if($this->db->insert("invoice", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function joblevel_insert($data) {
      if($this->db->insert("job_levels", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   public function company_lists() {
      $this->db->select("recruiter.parent_id, recruiter.cname,recruiter.fname,recruiter.lname, recruiter.email, recruiter.id, recruiter_details.address");
      $this->db->from('recruiter');
      $this->db->join("recruiter_details", "recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('parent_id !=', 0);
      $this->db->where('status =', 1);
      $this->db->where('label !=', 3);
      $this->db->order_by('recruiter.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function invoice_lists() {
      $this->db->select("recruiter.cname, invoice.id, invoice.company_id, invoice.invoice, invoice.type, invoice.date");
      $this->db->from('invoice');
      $this->db->join("recruiter", "invoice.company_id = recruiter.id");
      $this->db->order_by('invoice.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function referral_lists() {
      $this->db->select("user.name, user.email, user.country_code, user.phone, user.referral_used, user.created_at");
      $this->db->from('user');
      $this->db->where('referral_used!=','');
      $this->db->where('user.phone!=',0);
      //$this->db->join("recruiter", "invoice.company_id = recruiter.id");
      $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function recruiter_name($id) {
      $this->db->select("cname,fname,lname,email");
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function fetchcost($id) {
      $this->db->select("*");
      $this->db->where('company_id', $id);
      $query = $this->db->get('hire_cost');
      return $query->result_array();
   }

   public function staticcontent_single($id) {
      $this->db->select("*");
      $this->db->where('id', $id);
      $query = $this->db->get('static_content');
      return $query->result_array();
   }

   public function faq_single($id) {
      $this->db->select("*");
      $this->db->where('id', $id);
      $query = $this->db->get('faq');
      return $query->result_array();
   }

   public function advertise_single($id) {
      $this->db->select("*");
      $this->db->where('id', $id);
      $query = $this->db->get('advertise_list');
      return $query->result_array();
   }

   public function videoadvertise_single($id) {
      $this->db->select("*");
      $this->db->where('id', $id);
      $query = $this->db->get('videoadvertise_list');
      return $query->result_array();
   }
   public function newsadvertise_single($id) {
      $this->db->select("*");
      $this->db->where('id', $id);
      $query = $this->db->get('news_list');
      return $query->result_array();
   }
   
   public function jobtitle_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get('job_titles');
      return $query->result_array();
   }

   public function adprice_listing() {
      $this->db->where('id',1);
      $this->db->order_by('id','desc');
      $query = $this->db->get('advertise_amount');
      return $query->result_array();
   }

   public function advideoprice_listing() {
      $this->db->where('id',2);
      $this->db->order_by('id','desc');
      $query = $this->db->get('advertise_amount');
      return $query->result_array();
   }

   public function refercode_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get('referral_codes');
      return $query->result_array();
   }

   public function staticcontent_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get('static_content');
      return $query->result_array();
   }

   public function faq_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get('faq');
      return $query->result_array();
   }

   public function advertise_listing() {
      $this->db->select("advertise_list.*,recruiter.cname");
      $this->db->from("advertise_list");
      $this->db->join("recruiter","advertise_list.company_id = recruiter.id");
      $this->db->order_by('advertise_list.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function videoadvertise_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get("videoadvertise_list");
      return $query->result_array();
   }
   public function newsadvertise_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get("news_list");
      return $query->result_array();
   }

   public function pending_advertise_list() {
      $this->db->select("advertise_list.*,recruiter.cname");
      $this->db->from("advertise_list");
      $this->db->join("recruiter","advertise_list.company_id = recruiter.id");
      $this->db->where('advertise_list.status','2');
      $this->db->order_by('advertise_list.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function advertise_listingbyid($id) {
      $this->db->select("advertise_list.*,recruiter.cname,recruiter_details.address, recruiter.email");
      $this->db->from("advertise_list");
      $this->db->join("recruiter","advertise_list.company_id = recruiter.id");
      $this->db->join("recruiter_details","advertise_list.company_id = recruiter_details.recruiter_id");
      $this->db->where('advertise_list.id',$id);
      $this->db->order_by('advertise_list.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function advertise_price() {
      $this->db->where('id',1);
      $this->db->order_by('id','desc');
      $query = $this->db->get('advertise_amount');
      return $query->result_array();
   }
   public function videoadvertise_price() {
      $this->db->where('id',2);
      $this->db->order_by('id','desc');
      $query = $this->db->get('advertise_amount');
      return $query->result_array();
   }

   public function joblevel_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get('job_levels');
      return $query->result_array();
   }

   public function jobcategory_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get('job_category');
      return $query->result_array();
   }

   public function jobskills_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get('skills');
      return $query->result_array();
   }

   public function jobsubcategory_listing() {
      $this->db->select("job_subcategory.subcategory,job_subcategory.id, job_category.category");
      $this->db->from("job_subcategory");
      $this->db->join("job_category","job_subcategory.category_id = job_category.id");
      $this->db->order_by('job_subcategory.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function industry_insert($data) {
      if($this->db->insert("industry_lists", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function power_insert($data) {
      if($this->db->insert("super_power", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   public function industry_listing() {
      $query = $this->db->get('industry_lists');
      return $query->result_array();
   }

   public function power_listing() {
      $query = $this->db->get('super_power');
      return $query->result_array();
   }
   
   public function deletetitle($id) {
      $this->db->where('id', $id);
      $this->db->delete('job_titles');
      return true;
   }

   public function deleterefercode($id) {
      $this->db->where('id', $id);
      $this->db->delete('referral_codes');
      return true;
   }

   public function deletecontent($id) {
      $this->db->where('id', $id);
      $this->db->delete('static_content');
      return true;
   }

   public function deletefaq($id) {
      $this->db->where('id', $id);
      $this->db->delete('faq');
      return true;
   }

   public function deletead($id) {
      $this->db->where('id', $id);
      $this->db->delete('advertise_list');
      return true;
   }
   public function deletevideo($id) {
      $this->db->where('id', $id);
      $this->db->delete('videoadvertise_list');
      return true;
   }
   public function deletenews($id) {
      $this->db->where('id', $id);
      $this->db->delete('news_list');
      return true;
   }

   public function deletejoblevel($id) {
      $this->db->where('id', $id);
      $this->db->delete('job_levels');
      return true;
   }
   
   public function deleteindustry($id) {
      $this->db->where('id', $id);
      $this->db->delete('industry_lists');
      return true;
   }

   public function deletepower($id) {
      $this->db->where('id', $id);
      $this->db->delete('super_power');
      return true;
   }

   public function deletejobcategory($id) {
      $this->db->where('id', $id);
      $this->db->delete('job_category');
      return true;
   }

   public function deletejobskill($id) {
      $this->db->where('id', $id);
      $this->db->delete('skills');
      return true;
   }

   public function deletejobsubcategory($id) {
      $this->db->where('id', $id);
      $this->db->delete('job_subcategory');
      return true;
   }

   public function story_lists($companyid) {
      $this->db->select("recruiter.cname,recruiter.id as rid, recruiter.email, success_stories.*");
      $this->db->from('success_stories');
      $this->db->join("recruiter", "recruiter.id = success_stories.recruiter_id");
      $this->db->where('success_stories.recruiter_id',$companyid);
      $this->db->order_by('success_stories.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function comment_lists($storyid) {
      $this->db->select("*");
      $this->db->from('story_comments');
      $this->db->where('story_comments.story_id',$storyid);
      $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function storyStatus($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('success_stories', $data);
      return true;
   }

   public function staticontent_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('static_content', $data);
      return true;
   }

   public function faq_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('faq', $data);
      return true;
   }
   
   public function recruiter_listing() {
      $this->db->order_by('id','desc');
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function jobcategory_insert($data) {
      if($this->db->insert("job_category", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

    public function jobskill_insert($data) {
      if($this->db->insert("skills", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function jobsubcategory_insert($data) {
      if($this->db->insert("job_subcategory", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function rating_lists($id) {
      $this->db->select("*");
      $this->db->from('comapny_rating');
      $this->db->where('company_id',$id);
      $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function rating_insert($data) {
      $data = ["company_id"=> $data['cid'], "rating"=> $data['ratings']];
      
      $this->db->where('company_id', $data['company_id']);
      $query = $this->db->get('comapny_rating');
      $count = $query->num_rows();
      if($count >= 1) {
          $this->db->where('company_id', $data['company_id']);
          $this->db->update('comapny_rating', $data);
          return true;
      } else{
          if($this->db->insert("comapny_rating", $data)) { 
             $insert_id = $this->db->insert_id();
             return $insert_id;
          } else{
             return false;
          }
      }
   }

   public function recruiter_transferfetch() {
      $this->db->from('transfers');
      $this->db->order_by('id','DESC');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function recruiter_transferfetchsingle($id) {
      $this->db->from('transfers');
      $this->db->where("id",$id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function recruiter_singleupdate($id, $data) {
      $this->db->where('id', $id);
      $this->db->update('recruiter', $data);
      return true;
   }

   public function recruiter_singlemoreupdate($id, $data) {
      $this->db->where('recruiter_id', $id);
      $this->db->update('recruiter_details', $data);
      return true;
   }

   public function subrecruiter_delete($id) {
      $this->db->where('id', $id);
      $delete = $this->db->delete("recruiter");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function subrecruiter_advertiseupdate($id, $id2) {
      $this->db->where('company_id', $id);
      $this->db->update('advertise_list', ["company_id"=>$id2]);
      return true;
   }

   public function subrecruiter_permissiondelete($id) {
      $this->db->where('subrecruiter_id', $id);
      $delete = $this->db->delete("subrecruiter_permission");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function request_delete($id) {
      $this->db->where('id', $id);
      $delete = $this->db->delete("transfers");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

}
?>
