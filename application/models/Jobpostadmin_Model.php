<?php
ob_start();
class Jobpostadmin_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   }

   public function job_fetchAll() {
      $this->db->select('job_posting.id, job_posting.company_id,job_posting.opening, job_posting.jobtitle, job_posting.experience, job_posting.created_at, job_posting.jobexpire,job_posting.category,job_posting.subcategory,job_posting.mode,recruiter_details.address, recruiter.cname')
         ->from('job_posting')
         ->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function categoryname_fetch($id) {
      $this->db->where('id',$id);
      $query = $this->db->get('job_category');
      return $query->result_array();
   }

   public function subcategoryname_fetch($id) {
      $this->db->where('id',$id);
      $query = $this->db->get('job_subcategory');
      return $query->result_array();
   }

   public function jobexpire_fetchAll() {
      $this->db->select('job_posting.id, job_posting.company_id,job_posting.opening, job_posting.jobtitle, job_posting.experience, job_posting.created_at, job_posting.jobexpire,recruiter_details.address, recruiter.cname')
         ->where('job_posting.jobexpire <=', date('Y-m-d'))
         ->from('job_posting')
         ->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function notappliedjob_fetchAll() {
      $this->db->select('job_posting.id, job_posting.company_id, job_posting.jobtitle, job_posting.experience,job_posting.jobexpire ,job_posting.created_at,job_posting.mode, recruiter_details.address,recruiter.parent_id, recruiter.cname')
         ->from('job_posting')
         ->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id')
         ->join('recruiter', 'recruiter.id = job_posting.company_id')
         ->where("job_posting.id NOT IN (select jobpost_id from applied_jobs)")
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function boostjob_fetch() {
      $this->db->select('job_posting.id, job_posting.jobtitle, job_posting.company_id, job_posting.jobtitle, job_posting.experience, recruiter_details.address, recruiter.cname, recruiter_details.recruiter_email, recruiter.email')
         ->from('job_posting')
         ->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->where('boost_status','1')
         ->where('payment_status','0')
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function boost_amount() {
      $query = $this->db->get('boost_amount');
      return $query->result_array();
   }

   public function company() {
      $this->db->where('parent_id',0);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function company_site() {
      $this->db->where('parent_id!=',0);
      $this->db->where('label!=',3);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function company_sites($id) {
      $this->db->where('parent_id',$id);
      $this->db->where('label!=',3);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function site_name($id) {
      $this->db->where('id',$id);
      $this->db->where('label!=',3);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function getuserDatabyDay($status,$date) {
      if($status=='app'){
         $this->db->select('*')
         ->from('user')
         ->where('device_token!=','')
         ->where('phone!=',0)
         ->order_by('id','desc');
         $query = $this->db->get();
         return $query->result_array();
      }
      if($status=='web'){
         $this->db->select('*')
         ->from('user')
         ->where('device_token','')
         ->where('phone!=',0)
         ->order_by('id','desc');
         $query = $this->db->get();
         return $query->result_array();
      }
      if($status=='all'){
         $this->db->select('*')
         ->from('user')
         ->where('phone!=',0)
         ->order_by('id','desc');
         $query = $this->db->get();
         return $query->result_array();
      }
   }

   public function getnonuserDatabyDay($status,$date) {
      if($status=='app') {
         $this->db->select('*')
         ->from('user')
         ->where('email!=','')
         ->where('phone=',0)
         ->order_by('id','desc');
         $query = $this->db->get();
         return $query->result_array();
      }
      if($status=='web') {
         $this->db->select('*')
         ->from('user')
         ->where('email!=','')
         ->where('phone=',0)
         ->order_by('id','desc');
         $query = $this->db->get();
         return $query->result_array();
      }
      if($status=='all') {
         $this->db->select('*')
         ->from('user')
         ->where('email!=','')
         ->where('phone=',0)
         ->order_by('id','desc');
         $query = $this->db->get();
         return $query->result_array();
      }
   }

   public function getScreenDatabyStatus($status,$date) {
      $this->db->select('job_posting.id, job_posting.joining_bonus, job_posting.jobexpire, job_posting.level, job_posting.education, job_posting.allowance, job_posting.experience, job_posting.opening, job_posting.category, job_posting.subcategory, job_posting.language, job_posting.other_language,   job_posting.jobtitle,job_posting.mode, job_posting.company_id, recruiter.cname,recruiter.fname, recruiter.lname, user.name, user.email, user.id as uid, user.exp_month, user.exp_year, user.phone, user.superpower, job_screening.created_at, job_screening.updated_at, job_screening.status')
         ->from('job_screening')
         ->join('job_posting', 'job_screening.jobpost_id = job_posting.id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->join('user', 'user.id = job_screening.user_id');
         
         $this->db->order_by('job_screening.created_at','desc');
         $query = $this->db->get();
         return $query->result_array();
   }

    public function getDatabyStatus($status,$date) {
      $this->db->select('applied_jobs.status,applied_jobs.interviewdate, applied_jobs.interviewtime, job_posting.id, applied_jobs.updated_at, applied_jobs.created_at, applied_jobs.fallout_reason, job_posting.joining_bonus, job_posting.jobexpire, job_posting.level, job_posting.education, job_posting.allowance, job_posting.experience, job_posting.opening, job_posting.category, job_posting.subcategory, job_posting.language, job_posting.other_language,   job_posting.jobtitle,job_posting.mode, job_posting.company_id, recruiter.cname,recruiter.fname, recruiter.lname, user.name, user.email, user.id as uid, user.exp_month, user.exp_year, user.phone, user.superpower')
         ->from('applied_jobs')
         ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->join('user', 'user.id = applied_jobs.user_id');
         if($status=='10'){

         }else{
            $this->db->where('applied_jobs.status',$status);
         }
         
         $this->db->order_by('applied_jobs.created_at','desc');
         $query = $this->db->get();
         return $query->result_array();

      /*$this->db->select('applied_jobs.status,applied_jobs.interviewdate, applied_jobs.interviewtime, job_posting.id, job_posting.jobexpire, job_posting.jobtitle, job_posting.created_at, job_posting.company_id, recruiter.cname, user.name')
         ->from('applied_jobs')
         ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')
         ->join('recruiter', 'recruiter.id = job_posting.company_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->where('applied_jobs.status',$status)
         ->order_by('applied_jobs.id','desc');
      $query = $this->db->get();
      return $query->result_array();*/
   }

    public function appliedJobs() {
      $this->db->select('applied_jobs.status,applied_jobs.interviewdate, applied_jobs.interviewtime, job_posting.id, applied_jobs.updated_at, applied_jobs.fallout_reason, job_posting.joining_bonus, job_posting.jobexpire, job_posting.level, job_posting.education, job_posting.allowance, job_posting.experience, job_posting.opening, job_posting.category, job_posting.subcategory, job_posting.language, job_posting.other_language,   job_posting.jobtitle, job_posting.created_at, job_posting.company_id, recruiter.cname,recruiter.fname, recruiter.lname, user.name, user.id as uid, user.exp_month, user.exp_year')
         ->from('applied_jobs')
         ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->where('applied_jobs.status','1');
         $query = $this->db->get();
         return $query->result_array();
   }

   public function boostjob_fetchbyjobid($jid) {
      $this->db->select('job_posting.id,job_posting.created_at, job_posting.jobtitle, job_posting.company_id, job_posting.jobtitle, job_posting.experience, recruiter_details.address, recruiter.cname, recruiter.id as rid, recruiter_details.recruiter_email,recruiter.email')
         ->from('job_posting')
         ->join('recruiter_details', 'job_posting.recruiter_id = recruiter_details.recruiter_id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->where('boost_status','1')
         ->where('job_posting.id',$jid)
         ->where('job_posting.payment_status',0)
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function job_fetchsingle($id) {
      $this->db->select('job_posting.id, job_posting.jobtitle, applied_jobs.user_id, recruiter.cname, user.name, applied_jobs.status')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'recruiter.id = job_posting.company_id')
         ->where('job_posting.id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetchnotificationsingle($id) {
      $this->db->select('id,jobtitle,recruiter_id,subrecruiter_id,company_id,subcategory')
         ->from('job_posting')
         ->where('id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function companyname_fetch($id) {
      $this->db->select('recruiter.cname,recruiter_details.address')
         ->from('recruiter')
         ->join('recruiter_details','recruiter.id=recruiter_details.recruiter_id')
         ->where('recruiter.id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function delete_jobpost($id) {
      $this->db->where('id', $id);
      $this->db->delete('job_posting');
      return true;
   }

   public function delete_jobpostlocation($id) {
      $this->db->where('posting_id', $id);
      $this->db->delete('jobposting_location');
      return true;
   }

   public function jobupdate_detail_fetch($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('job_posting');
      return $query->result_array();
   }
   public function jobupdate_detailexp_fetch($id) {
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('exp_with_salary');
      return $query->result_array();
   }
   public function jobupdate_recruiterexp_fetch($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_addexp');
      return $query->result_array();
   }
   public function jobupdate_location_fetch($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }
   
   public function industry_lists($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('industry_lists');
      return $query->result_array();
   }
   
   public function channel_lists($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('channel_lists');
      return $query->result_array();
   }

   public function level_lists($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('job_levels');
      return $query->result_array();
   }
   
   public function language_lists($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('languages');
      return $query->result_array();
   }

   public function skill_lists() {
      $this->db->select("*");
      $this->db->from('skills');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobs_skills_single($id) {
      $this->db->select("skill_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_skills');
      return $query->result_array();
   }

   public function compId($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('job_posting');
      return $query->result_array();
   }

   public function get_invoices_data($id) {
      $this->db->where('company_id', $id);
      $this->db->order_by('id','desc');
      $query = $this->db->get('invoice');
      return $query->result_array();
   }


   // public function jobupdate_detail_fetch($id) {
   //    $this->db->select("job_posting.id, job_posting.company_id, job_posting.jobtitle, job_posting.opening, job_posting.experience, job_posting.industry, job_posting.language , job_posting.channel, job_posting.salary, job_posting.jobDesc, job_posting.skills, job_posting.qualification, job_posting.jobexpire, job_posting.companydetail, jobposting_location.jobLocation")
   //             ->where('job_posting.id', $id)
   //             ->from('job_posting')
   //             ->join('jobposting_location', 'jobposting_location.posting_id = job_posting.id')
   //             ->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
   //    $query = $this->db->get();
   //    return $query->result_array();
   // }

   public function job_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('job_posting', $data);
      return true;
   }
   
   public function jobLocation_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('jobposting_location', $data);
      return true;
   }

   public function subcategorybyid($id) {
      $this->db->select("*");
      $this->db->from('job_subcategory');
     $this->db->where('id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function getsalaryexp($jid,$expfilter){

      
      $this->db->select('basicsalary,exp');
      $this->db->where("jobpost_id", $jid);
      $this->db->where("grade_id", $expfilter);
     
      $query = $this->db->get('exp_with_salary');
      if($query->num_rows>0){
         return $query->result_array();
      }else{
         $this->db->select("basicsalary")
         ->from('exp_with_salary')
         ->where("jobpost_id", $jid);
         $query = $this->db->get();
         return $query->result_array();   
      }      
   }

   public function basic_salary_insert($data) {
      if($this->db->insert_batch("exp_with_salary", $data)) {
         return true;
      } else{
         return false;
      }
   }

    public function job_basicsalary_delete($id) {
      $this->db->where('jobpost_id', $id);
      $delete = $this->db->delete("exp_with_salary");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }


   public function getRowsUsers($params = array()) {
      $this->db->select('*');
      $this->db->from('user');
      
      if(array_key_exists("conditions", $params)){
          foreach($params['conditions'] as $key => $val){
              $this->db->where($key, $val);
          }
      }

      if(!empty($params['searchKeyword'])) {
          $search = $params['searchKeyword'];

          $this->db->group_start();

          $likeArr = array('name' => $search, 'email' => $search, 'phone' => $search, 'location' => $search, 'state' => $search, 'city' => $search, 'jobsInterested' => $search, 'education' => $search, 'nationality' => $search, 'device_type' => $search, 'app_version' => $search, 'last_used' => $search, 'type' => $search);

          $this->db->or_like($likeArr);

          $this->db->group_end();
      }

      if(!empty($params['searchStatus'])) {

          if($params['searchStatus'] == "app") {
            $this->db->where_in('platform', ['android','ios']);
          } elseif($params['searchStatus'] == "web")  {
            $this->db->where('platform', 'web');
          }
      }
      
      $this->db->where('phone!=',0);

      if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
          $result = $this->db->count_all_results();
      }else{
          if(array_key_exists("id", $params)){
              $this->db->where('id', $params['id']);
              $query = $this->db->get();
              $result = $query->row_array();
          }else{
              $this->db->order_by('id', 'DESC');
              if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                  $this->db->limit($params['limit'],$params['start']);
              }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                  $this->db->limit($params['limit']);
              }
              
              $query = $this->db->get();
              $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
          }
      }
        
        // Return fetched data
        return $result;
   }

   
}
?>