<?php
ob_start();
class User_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   }


   public function allusers_lists() {
      $this->db->select('id, location, state, latitude, longitude, jobsInterested');
        $this->db->from('user');
        $this->db->where('phone!=','');
        //$this->db->limit(50);
        $this->db->order_by('id','desc');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   //my
   public function update_latlong($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('user', $data);
      return true;
   }



   public function user_insert($data) { 
      if($this->db->insert("user", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function usertoken_insert($data) { 
      if($this->db->insert("user_token", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function update_usertoken($data,$device_token, $id) {
      $this->db->where('device_token', $device_token);
      $this->db->where('user_id', $id);
      $this->db->update('user_token', $data);
      return true;
   }

   public function usertoken_update($data, $id){
      $this->db->where('user_id',$id);
      $this->db->update('user_token',$data);
      return true;
   }

   public function user_login($data) {
      $this->db->where($data);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function fetch_version() {
      $query = $this->db->get('api_version');
      return $query->result_array();
   }

   public function user_token_check($data) {
      $this->db->where($data);
      $query = $this->db->get('user_token');
      return $query->result_array();
   }

   //my
   public function user_token_checksocialsignup($data) {
      $this->db->where($data);
      $this->db->order_by('id', 'DESC');
      $this->db->limit('1');
      $query = $this->db->get('user_token');
      return $query->result_array();
   }
   public function user_social_checksocialsignup($id) {
      $this->db->select('socialToken');
      $this->db->where("id",$id);
      $query = $this->db->get('user');
      return $query->result_array();
   }


   public function mail_exists($key)
   {
       $this->db->where('email',$key);
       $query = $this->db->get('user');
       if ($query->num_rows() > 0){
           return true;
       }
       else{
           return false;
       }
   }

   public function user_social($token, $type) {
      //$data = ["email" => $token, "type" => $type];
      $this->db->where("email",$token);
      $this->db->where("type", $type);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   //my
   public function user_socialagain($token, $type) {
      //$data = ["email" => $token, "type" => $type];
      $this->db->where("socialToken",$token);
      $this->db->where("type", $type);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function user_single($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('user');
      return $query->result_array();
   }
   
   public function user_single1($email) {
      $this->db->where('email', $email);
      $query = $this->db->get('user');
      return $query->result_array();
   }
  public function user_single_work($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('user_work_experience');
      return $query->result_array();
   }

   public function user_single_workbyuserid($id,$level) {
      $this->db->select('level');
      $this->db->where('user_id', $id);
      $this->db->where('level', $level);
      $query = $this->db->get('user_work_experience');
      return $query->result_array();
   }

   public function user_single_workbyuserid1($id) {
      $this->db->select('level');
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_work_experience');
      return $query->result_array();
   }

   public function user_single_educationbyuserid($id, $education) {
      $this->db->select('attainment');
      $this->db->where('user_id', $id);
      $this->db->where('attainment', $education);
      $query = $this->db->get('user_education');
      return $query->result_array();
   }

   public function user_single_educationbyuserid1($id) {
      $this->db->select('education');
      $this->db->where('id', $id);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function user_single_educationbyuserid2($id, $education) {
      $this->db->select('education');
      $this->db->where('id', $id);
      //$this->db->where('education', $education);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function user_single_exp($id,$exp) {
      if($exp=="All Tenure"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $query = $this->db->get('user');
      }
      if($exp=="No Experience"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_month >=', '0');
         $this->db->where('exp_year >=', '0');
         $query = $this->db->get('user');
      }
      if($exp=="< 6 months"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_month <', '6');
         $query = $this->db->get('user');
      }
      if($exp=="> 6 months"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_month >', '6');
         $this->db->or_where('exp_year>', '1');
         $query = $this->db->get('user');
      }

      if($exp=="> 1 yr"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_year >', '1');
         
         $query = $this->db->get('user');
      }if($exp=="> 2 yr"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_year >', '2');
         
         $query = $this->db->get('user');
      }if($exp=="> 3 yr"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_year >', '3');
         
         $query = $this->db->get('user');
      }if($exp=="> 4 yr"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_year >', '4');
         
         $query = $this->db->get('user');
      }if($exp=="> 5 yr"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_year >', '5');
         
         $query = $this->db->get('user');
      }if($exp=="> 6 yr"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_year >', '6');
         
         $query = $this->db->get('user');
      }if($exp=="> 7 yr"){
         $this->db->select('exp_month,exp_year');
         $this->db->where('id', $id);
         $this->db->where('exp_year >', '7');
         
         $query = $this->db->get('user');
      }
      
      
      return $query->result_array();
   }

   public function user_single_education($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('user_education');
      return $query->result_array();
   }

   public function token_match($data) {
      $this->db->where($data);
      $query = $this->db->get('user');
      return $query->result_array();
   }
   public function token_match1($data) {
      $this->db->where($data);
      $query = $this->db->get('user_token');
      return $query->result_array();
   }

   public function user_match($data) {
      $this->db->where($data);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function email_match($data) {
      $this->db->where($data);
      $query = $this->db->get('user');
      return $query->result_array();
   }
   
   public function checkUser1($email, $type) {
      $data = ["email" => $email, "type" => $type];
      $this->db->where($data);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function password_update($data, $token) {
      $this->db->where('token', $token);
      $this->db->update('user', $data);
      return true;
   }

 public function update_data($tbl,$connd,$data) {
      $this->db->where($connd);
      $this->db->update('user', $data);
      return true;
   }



   public function userComplete_single($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('profile_completeness');
      return $query->result_array();
   }

   public function check_profileComplete($id) {
      $this->db->where("user_id", $id);
      $query = $this->db->get('profile_completeness');
      return $query->result_array();
   }

   public function insert_profileComplete($data) { 
      if($this->db->insert("profile_completeness", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function update_profileComplete($data, $id) {
      $this->db->where('user_id', $id);
      $this->db->update('profile_completeness', $data);
      return true;
   }

   public function update_token($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('user', $data);
      return true;
   }

   //my
   public function update_tokenagain($data, $id, $token) {
      $this->db->where('user_id', $id);
      $this->db->where('device_token', $token);
      $this->db->update('user_token', $data);
      return true;
   }




   public function user_update($data, $id) { 
      $this->db->where('id', $id);
      $this->db->update('user', $data);
      return true;
   }
    public function user_updatepass($data, $cond) { 
      $this->db->where($cond);
      $this->db->update('user', $data);
      return true;
   }
   
   public function user_update1($data, $token) { 
      $this->db->where('token', $token);
      $this->db->update('user', $data);
      return true;
   }

   public function work_insert($data) { 
      if($this->db->insert("user_work_experience", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function work_update($data, $id) { 
      $this->db->where('id', $id);
      $this->db->update('user_work_experience', $data);
      return true;
   }

   public function work_single($id) {

      $this->db->select("user_work_experience.id, user_work_experience.title,user_work_experience.industry,user_work_experience.channel, user_work_experience.company, user_work_experience.jobDesc, user_work_experience.workFrom, user_work_experience.workTo , user_work_experience.currently_working, job_levels.level as level, job_levels.id as levelid, job_category.category as category ,job_category.id as categoryid, job_subcategory.subcategory as subcategory, job_subcategory.id as subcatid");
      $this->db->from("user_work_experience");
      $this->db->join("job_levels","job_levels.id = user_work_experience.level");
      $this->db->join("job_category","job_category.id = user_work_experience.category");
      $this->db->join("job_subcategory","job_subcategory.id = user_work_experience.subcategory");
      $this->db->where('user_work_experience.user_id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }

public function work_unique($id) {

      $this->db->select("user_work_experience.id, user_work_experience.title,user_work_experience.industry,user_work_experience.channel, user_work_experience.company, user_work_experience.jobDesc, user_work_experience.workFrom, user_work_experience.workTo , user_work_experience.currently_working, job_levels.level as level, job_levels.id as levelid, job_category.category as category ,job_category.id as categoryid, job_subcategory.subcategory as subcategory, job_subcategory.id as subcatid");
      $this->db->from("user_work_experience");
      $this->db->join("job_levels","job_levels.id = user_work_experience.level");
      $this->db->join("job_category","job_category.id = user_work_experience.category");
      $this->db->join("job_subcategory","job_subcategory.id = user_work_experience.subcategory");
      $this->db->where('user_work_experience.user_id', $id);
      $this->db->group_by('user_work_experience.company');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function industryName_single($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('industry_lists');
      return $query->result_array();
   }

   public function edu_insert($data) { 
      if($this->db->insert("user_education", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function edu_update($data, $id) { 
      $this->db->where('id', $id);
      $this->db->update('user_education', $data);
      return true;
   }

   public function edu_single($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_education');
      return $query->result_array();
   }

   public function lang_beforeinsertcheck($data) {
      $this->db->where($data);
      $query = $this->db->get('user_language');
      return $query->result_array();
   }

   public function lang_beforeInsertCount($id) {
      $this->db->where("user_id", $id);
      $query = $this->db->get('user_language');
      return $query->result_array();
   }

   public function lang_insert($data) { 
      if($this->db->insert("user_language", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function lang_update($data, $id) { 
      $this->db->where('id', $id);
      $this->db->update('user_language', $data);
      return true;
   }

   public function lang_single($id) {
      $this->db->select("user_language.id, languages.name,user_language.lang_id")
               ->where('user_language.user_id', $id)
               ->from('user_language')
               ->join('languages', 'languages.id = user_language.lang_id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function client_beforeInsertCount($id) {
      $this->db->where("user_id", $id);
      $query = $this->db->get('user_topclients');
      return $query->result_array();
   }

   public function client_beforeinsertcheck($data) {
      $this->db->where($data);
      $query = $this->db->get('user_topclients');
      return $query->result_array();
   }

   public function topClient_insert($data) { 
      if($this->db->insert("user_topclients", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function topClient_update($data, $id) { 
      $this->db->where('id', $id);
      $this->db->update('user_topclients', $data);
      return true;
   }

   public function client_single($id) {
      $this->db->select("id, clients");
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_topclients');
      return $query->result_array();
   }


   public function skills_beforeInsertCount($id) {
      $this->db->where("user_id", $id);
      $query = $this->db->get('user_skills');
      return $query->result_array();
   } 
   public function skills_beforeinsertcheck($data) {
      $this->db->where($data);
      $query = $this->db->get('user_skills');
      return $query->result_array();
   }
   public function topSkills_insert($data) { 
      if($this->db->insert("user_skills", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function topSkills_update($data, $id) { 
      $this->db->where('id', $id);
      $this->db->update('user_skills', $data);
      return true;
   }

   public function skills_single($id) {
      $this->db->select("id, skills");
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_skills');
      return $query->result_array();
   }

   public function user_more_update($data, $id) { 
      $this->db->where('user_id', $id);
      $this->db->update('user_more_details', $data);
      return true;
   }

   public function user_more_insert($data) { 
      if($this->db->insert("user_more_details", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function user_more($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_more_details');
      return $query->result_array();
   }

   public function user_assessment($id) {
      $this->db->where("user_id", $id);
      $query = $this->db->get('user_assessment');
      return $query->result_array();
   }

   public function user_assessment_update($data, $id) {
      
      $this->db->where('user_id', $id);
      $this->db->update('user_assessment', $data);
      return true;
   }

   public function user_assessment_insert($data) { 
      if($this->db->insert("user_assessment", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

  public function user_expert($id) {
      $this->db->where("user_id", $id);
      $query = $this->db->get('user_expert');
      return $query->result_array();
   }

   public function user_expert_update($data, $id) {
      $this->db->where('user_id', $id);
      $deleteExpert = $this->db->delete('user_expert');
      
      if($deleteExpert){
         if($this->db->insert("user_expert", $data)) { 
            $insert_id = $this->db->insert_id();
            return $insert_id;
         } else{
            return false;
         }
      } else{
         return false;
      }
   }

   public function user_expert_insert($data) { 
      if($this->db->insert("user_expert", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function user_pic_update($userPic, $id) {
      $data = ["profilePic" => $userPic];
      $this->db->where('id', $id);
      $this->db->update('user', $data);
      return true;
   }

   public function randomstring() {
        $len = 8;
        $string = "";
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for($i=0;$i<$len;$i++) {
            $string.=substr($chars,rand(0,strlen($chars)),1);
        }
        return $string;
    }

    public function resume_single($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_resume');
      return $query->result_array();
   }

   public function referral_code($code) {
      $this->db->where('refer_code', $code);
      $query = $this->db->get('referral_codes');
      return $query->result_array();
   }

   public function checkphone($phone) {
      $this->db->where('phone', $phone);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function checkemail($email) {
      $this->db->where('phone!=', 0);
      $this->db->where('email', $email);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function checkwebemail($email) {
      $this->db->where('email', $email);
      $this->db->where('type', "normal");
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function checkwebemailsocial($email) {
      $this->db->where('socialToken', $email);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function user_resume_update($userResume, $id) {
      $data = ["resume" => $userResume];
      $this->db->where('user_id', $id);
      $this->db->update('user_resume', $data);
      return true;
   }

   public function user_resume_insert($userResume, $id) {
      $data = ["user_id" =>$id, "resume" => $userResume];
      if($this->db->insert("user_resume", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function web_user_resume_insert($userResumeData) {
      if($this->db->insert("user_resume", $userResumeData)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   
   public function delete_user($id, $tableName){
      if($this->db->delete($tableName, 'id ='. $id)){
          return true;
      }        
   }

   public function delete_user1($id, $tableName){
      if($this->db->delete($tableName, 'user_id ='. $id)){
          return true;
      }        
   }
   
   public function user_logout($id) {
      $data = ["token" => " ","device_token" => " "];
      $this->db->where('id', $id);
      $this->db->update('user', $data);
      return true;
   }
   public function user_logout1($id,$token) {
     $this->db->where('token', $token);
     $this->db->where('user_id', $id);
      $delete = $this->db->delete("user_token");
      /*$data = ["token" => " ","device_token" => " "];
      $this->db->where('user_id', $id);
      $this->db->where('token', $token);
      $this->db->update('user_token', $data);*/
      return true;
   }

   public function usertoken_delete($id,$token) {
     $this->db->where('device_token', $token);
     $this->db->where('user_id', $id);
      $delete = $this->db->delete("user_token");
      return true;
   }
    
    public function forgotPass_check($id) { 
      $this->db->where("user_id", $id);
      $query = $this->db->get('jobseeker_forgot');
      return $query->result_array();
   }
   
   
   
   public function mobile_check($id) {
      $this->db->select("*");
      $this->db->where("phone", $id);
      $query = $this->db->get('user');
      return $query->result_array();
   }
   public function forgotPass_insert($data) { 
      if($this->db->insert("jobseeker_forgot", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function forgotPass_update($data, $id) { 
      $this->db->where('user_id', $id);
      $this->db->update('jobseeker_forgot', $data);
      return true;
   }
  public function checkjobstatus($condition) {
      $this->db->where($condition);
      $query = $this->db->get('saved_jobs');
      return $query->result_array();
   }

   public function jobsearchstatus_update($statusdata, $id) {
      $data = ["jobsearch_status" => $statusdata];
      $this->db->where('id', $id);
      $this->db->update('user', $data);
      return true;
   }

   public function notificationstatus_update($statusdata, $id) {
      $data = ["notification_status" => $statusdata];
      $this->db->where('id', $id);
      $this->db->update('user', $data);
      return true;
   }

   public function insert_contact($data) {
      if($this->db->insert("user_contact", $data)) {
         return true;
      } else{
         return false;
      }
   }

   public function quote_single() {
      $query = $this->db->get('motivational_quotes');
      return $query->result_array();
   }
  
   public function fetch_bonusamount($id) {
  $this->db->select("sign_in_bonus");      
  $this->db->where("id", $id);
      $query = $this->db->get('user');
      return $query->result_array();
   }
   public function fetch_bonushistory($id) {
  $this->db->select("id, bonus, notification, added_at");     
  $this->db->where("user_id", $id);
  $this->db->order_by("id", 'DESC');
      $query = $this->db->get('sign_in_bonus');
      return $query->result_array();
   }
   function getToken(){
        $string = "";
        $chars = "0123456789";
        for($i=0;$i<4;$i++)
        $string.=substr($chars,(rand()%(strlen($chars))), 1);
        return $string;
    }


   public function getownstate() {
      $this->db->select("id, state");
      $this->db->group_by('state');
      $query = $this->db->get('owncountrydata');
      return $query->result_array();
   }

   public function getowncity($token) {
      $this->db->select("id, city");
      $this->db->where("state",$token);
      $query = $this->db->get('owncountrydata');
      return $query->result_array();
   }
} 
?> 
