<?php
ob_start();
class Candidate_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   public function hired_list($id) {
      $this->db->select('COUNT(job_posting.id) as hiredCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.status", 7)
         ->where("applied_jobs.jobpost_id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function rejected_list($id) {
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.status", 3)
         ->where("applied_jobs.jobpost_id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }
   public function refered_list($id) {
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.status", 4)
         ->where("applied_jobs.jobpost_id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function checkjob($jid,$rid){
    $this->db->select('job_posting.id')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where('applied_jobs.status!=',4)
         ->where('job_posting.id',$jid)
         ->where('job_posting.recruiter_id',$rid);
    $query = $this->db->get();
    return $query->result_array();          
   }

   public function job_detail($id) {
      $this->db->select('job_posting.id, job_posting.jobtitle, job_posting.experience, job_posting.skills, recruiter_details.address')
         ->from('job_posting')
         ->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id')
         ->where("job_posting.id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function newApplication_list($id) {
      $this->db->select('applied_jobs.id, applied_jobs.jobpost_id, applied_jobs.user_id, applied_jobs.interviewdate, applied_jobs.interviewtime, user.name, user.email,user.designation, user.location, user.phone,user.country_code, user.profilePic, job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("applied_jobs.jobpost_id", $id)
         ->where("applied_jobs.status", 1)
         ->order_by("applied_jobs.id","DESC")
         ->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function goingApplication_list($id) {
      $this->db->select('applied_jobs.id,applied_jobs.jobpost_id, applied_jobs.user_id,applied_jobs.interviewdate, applied_jobs.interviewtime, user.name, user.email,user.designation, user.location, user.phone,user.country_code, user.profilePic, user_resume.resume,job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('user_resume', 'user_resume.user_id = applied_jobs.user_id', 'left')
         ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("applied_jobs.jobpost_id", $id)         
         ->where("applied_jobs.status", 5)
         ->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function acceptedApplication_list($id) {
      $this->db->select('applied_jobs.id,applied_jobs.jobpost_id, applied_jobs.user_id,applied_jobs.interviewdate, applied_jobs.interviewtime, applied_jobs.accepted_date,user.designation, applied_jobs.date_day1, user.name, user.email, user.location, user.phone,user.country_code, user.profilePic,job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("applied_jobs.jobpost_id", $id)
         ->where("applied_jobs.status", 6)
         ->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function hiredapplication_list($id) {
      $this->db->select('applied_jobs.id,applied_jobs.jobpost_id, applied_jobs.user_id, applied_jobs.date_day1 as interviewdate, applied_jobs.interviewtime, user.name, user.email,user.designation, user.location, user.phone, user.country_code, user.profilePic,job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("applied_jobs.jobpost_id", $id)
         ->where("applied_jobs.status", 7)
         ->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function referapplication_list($id) {
      $this->db->select('applied_jobs.id,applied_jobs.jobpost_id, applied_jobs.user_id, applied_jobs.interviewdate, applied_jobs.interviewtime, user.name, user.email,user.designation, user.location, user.phone, user.country_code, user.profilePic,job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("applied_jobs.jobpost_id", $id)
         ->where("applied_jobs.status", 4)
         ->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function falloutapplication_list($id) {
      $this->db->select('applied_jobs.id,applied_jobs.jobpost_id,applied_jobs.status, applied_jobs.user_id, applied_jobs.interviewdate, applied_jobs.interviewtime, user.name, user.email,user.designation, user.location, user.phone, user.country_code, user.profilePic,job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
          ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("applied_jobs.jobpost_id", $id)
         ->where("applied_jobs.status", 3)
         ->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function candidateApplied_list($id,$subid) {
      $this->db->select('applied_jobs.id, applied_jobs.jobpost_id,applied_jobs.interviewtime, applied_jobs.interviewdate,applied_jobs.accepted_date, applied_jobs.updated_at,applied_jobs.date_day1, applied_jobs.user_id, user.name, user.email, user.location, user.phone,user.country_code, user.profilePic, job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname, recruiter.id as compid')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("job_posting.recruiter_id", $id)
         ->where("applied_jobs.status!=", '7')
         ->where("recruiter.parent_id!=", '0');
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
         $this->db->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function candidateApplied_list1($id,$status,$subid,$site_id,$filters) {
    $cDate = date('y-m-d');
      $this->db->select('applied_jobs.id,applied_jobs.interviewtime, applied_jobs.interviewdate,applied_jobs.date_day1, applied_jobs.accepted_date , applied_jobs.updated_at, applied_jobs.jobpost_id, applied_jobs.user_id, user.name, user.email, user.location, user.phone,user.country_code, user.profilePic, job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("job_posting.recruiter_id", $id)
         ->where("recruiter.id", $site_id)
         
         ->where("recruiter.parent_id!=", '0');
         if($filters!='' && $filters=='Active'){
            $this->db->where("job_posting.jobexpire >=", $cDate);
         }else if($filters=='Expired'){
            $this->db->where("job_posting.jobexpire <", $cDate);
         }else{

         }
         if($status==6){
          $this->db->where("(applied_jobs.status=6)");
         }else if($status==10){
          
         }else{
         $this->db->where("applied_jobs.status", $status);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
         $this->db->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function candidateApplied_list2($id,$status,$subid) {
      $this->db->select('applied_jobs.id,applied_jobs.interviewtime, applied_jobs.interviewdate,applied_jobs.date_day1, applied_jobs.accepted_date , applied_jobs.updated_at, applied_jobs.jobpost_id, applied_jobs.user_id, user.name, user.email, user.location, user.phone,user.country_code, user.profilePic, job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("job_posting.recruiter_id", $id)         
         ->where("recruiter.parent_id!=", '0');
         if($status==6){
          $this->db->where("(applied_jobs.status=6 OR applied_jobs.status=7)");
         }else{
         $this->db->where("applied_jobs.status", $status);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
         $this->db->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function Applied_listfilter($id, $filter) {
        $this->db->select('applied_jobs.id,applied_jobs.jobpost_id, applied_jobs.user_id,applied_jobs.date_day1 ,applied_jobs.interviewdate, applied_jobs.interviewtime, user.name, user.email, user.location, user.phone, user.country_code, user.profilePic,job_posting.jobtitle, job_posting.jobDesc,job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname, applied_jobs.status')
          ->from('applied_jobs')
          ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
          ->join('user', 'user.id = applied_jobs.user_id')
          ->join('recruiter', 'job_posting.recruiter_id = recruiter.id')
          ->where("job_posting.recruiter_id", $id);
          /*if(!empty($filter['level'])){
            $this->db->where('job_posting.level',$filter['level']);
          }
          if(!empty($filter['category'])){
            $this->db->where('job_posting.category',$filter['category']);
          }
          if(!empty($filter['subcategory'])){
            $this->db->where('job_posting.subcategory',$filter['subcategory']);
          }*/
          if(!empty($filter['candidate_name'])){
            $this->db->like('user.name',$filter['candidate_name'],'both');
          }

          if(!empty($filter['job_title'])){
            $this->db->like('job_posting.jobtitle',$filter['job_title'],'both');
          }

          if(!empty($filter['job_posted'])){
            $this->db->like('recruiter.fname',$filter['job_posted'],'both');
          }

          if(!empty($filter['site_name'])){
            $this->db->where('job_posting.company_id',$filter['site_name']);
          }

          if(!empty($filter['app_status'])){
            $this->db->where('applied_jobs.status',$filter['app_status']);
          }

          if(isset($filter['education']) && !empty($filter['education'])) {
              $this->db->join('user_education', 'user_education.user_id= user.id','left' );
          }
          if(isset($filter['clients']) && !empty($filter['clients'])) {
            $this->db->join('user_topclients', 'user_topclients.user_id = user.id');
          }
          if(isset($filter['industry']) && !empty($filter['industry'])) {
            $this->db->join('user_expert', 'user_expert.user_id = user.id');
          }
          /*if(isset($filter['skills']) && !empty($filter['skills'])) {
            $this->db->join('user_skills', 'user_skills.user_id = user.id');
          }*/
          /*if(isset($filter['lang']) && !empty($filter['lang'])) {
            $this->db->join('user_language', 'user_language.user_id = user.id', 'left');
          }*/
         
          if(isset($filter['clients']) && !empty($filter['clients'])) {
              $this->db->like('user_topclients.clients', $filter['clients'], 'both');
          }
          if(isset($filter['exp']) && !empty($filter['exp'])) {
              //$lessExp = $filter['exp'] - 1;
              /*$this->db->where('user.exp_year >=', $lessExp);
              $this->db->where('user.exp_year <=', $filter['exp']);*/
              if($filter['exp']=="less than 6 months"){
                $this->db->where('user.exp_month <=', '6');
                $this->db->where('user.exp_year <', '1');
              }
              if($filter['exp']=="6mo to 1 yr"){
                $this->db->where('user.exp_year <=', '1');
                $this->db->where('user.exp_month >=', '6');
              }
              if($filter['exp']=="1 yr to 2 yr"){
                $this->db->where('user.exp_year <=', '2');
                $this->db->where('user.exp_year >=', '1');
              }
              if($filter['exp']=="2yr to 3 yr"){
                $this->db->where('user.exp_year <=', '3');
                $this->db->where('user.exp_year <=', '2');
              }
              if($filter['exp']=="3yr and up"){
                $this->db->where('user.exp_year >=', '3');
              }
              
          }
          if(isset($filter['industry']) && !empty($filter['industry'])) {
              $this->db->like('user_expert.expert', $filter['industry'], 'both');
          }
          if(isset($filter['education']) && !empty($filter['education'])) {
              $this->db->like('user_education.attainment', $filter['education'],'both' );
          }
          /*if(isset($filter['skills']) && !empty($filter['skills'])) {
            $this->db->like('user_skills.skills', $filter['skills'], 'both');
          }*/
          /*if(isset($filter['lang']) && !empty($filter['lang'])) {
            $this->db->where('user_language.lang_id', $filter['lang']);
          }
*/
          $this->db->order_by("applied_jobs.id","DESC");
          $query = $this->db->get();
          // echo $this->db->last_query();
          // die;
          return $query->result_array();
   }

   public function candidateApplied_statuschange($applied_id, $uid, $data) {
      $this->db->where("id", $applied_id);
      $this->db->where("user_id", $uid);
      $this->db->update('applied_jobs', $data);
      return true;
   }

   public function refer_status($data){
       if($this->db->insert('applied_jobs',$data)){
           $insert_id = $this->db->insert_id();
             return $insert_id;
        } else{
         return false;
        }
   }

   public function search_listfilter($id, $filter) {
        $this->db->select('user.name, user.id, user.email, user.location, user.phone, user.profilePic, user.created_at')
          ->from('user');
          if(!empty($filter['level']) || !empty($filter['category']) || !empty($filter['subcategory'])){
            $this->db->join('user_work_experience', 'user_work_experience.user_id = user.id', 'left');
          }
          if(!empty($filter['level'])){
            $this->db->where('user_work_experience.level',$filter['level']);
          }
          if(!empty($filter['category'])){
            $this->db->where('user_work_experience.category',$filter['category']);
          }
          if(!empty($filter['subcategory'])){
            $this->db->where('user_work_experience.subcategory',$filter['subcategory']);
          }
          if(isset($filter['clients']) && !empty($filter['clients'])) {
            $this->db->join('user_topclients', 'user_topclients.user_id = user.id');
          }
          if(isset($filter['industry']) && !empty($filter['industry'])) {
            $this->db->join('user_expert', 'user_expert.user_id = user.id');
          }
          /*if(isset($filter['skills']) && !empty($filter['skills'])) {
            $this->db->join('user_skills', 'user_skills.user_id = user.id');
          }*/
          if(isset($filter['lang']) && !empty($filter['lang'])) {
            $this->db->join('user_language', 'user_language.user_id = user.id', 'left');
          }
          $this->db->like("user.designation", $filter['keyword'], 'both');
         
          if(isset($filter['clients']) && !empty($filter['clients'])) {
              $this->db->like('user_topclients.clients', $filter['clients'], 'both');
          }
          if(isset($filter['exp']) && !empty($filter['exp'])) {
              /*$lessExp = $filter['exp'] - 1;
              $this->db->where('user.exp_year >=', $lessExp);
              $this->db->where('user.exp_year <=', $filter['exp']);*/
              if($filter['exp']=="less than 6 months"){
                $this->db->where('user.exp_month <=', '6');
                $this->db->where('user.exp_year <', '1');
              }
              if($filter['exp']=="6mo to 1 yr"){
                $this->db->where('user.exp_year <=', '1');
                $this->db->where('user.exp_month >=', '6');
              }
              if($filter['exp']=="1 yr to 2 yr"){
                $this->db->where('user.exp_year <=', '2');
                $this->db->where('user.exp_year >=', '1');
              }
              if($filter['exp']=="2yr to 3 yr"){
                $this->db->where('user.exp_year <=', '3');
                $this->db->where('user.exp_year <=', '2');
              }
              if($filter['exp']=="3yr and up"){
                $this->db->where('user.exp_year >=', '3');
              }
          }
          if(isset($filter['industry']) && !empty($filter['industry'])) {
              $this->db->like('user_expert.expert', $filter['industry'], 'both');
          }
          /*if(isset($filter['skills']) && !empty($filter['skills'])) {
            $this->db->like('user_skills.skills', $filter['skills'], 'both');
          }*/
          if(isset($filter['lang']) && !empty($filter['lang'])) {
            $this->db->where('user_language.lang_id', $filter['lang']);
          }
          $this->db->group_by("user.id");
          $this->db->order_by("user.id", "DESC");
          $query = $this->db->get();
          // echo $this->db->last_query();
          // die;
          return $query->result_array();
   }
   
   public function candidateApplied_listsearch($id, $key) {
      $this->db->select('user.name, user.id, user.email, user.location, user.phone,user.country_code, user.profilePic, user.created_at, user_resume.resume')
          ->from('user')
          ->join('user_resume', 'user.id = user_resume.user_id','left')
          ->join('user_work_experience', 'user.id = user_work_experience.user_id')
         ->where('user.jobsearch_status!=','3')
         ->like("user.designation", $key, 'both')
         ->or_like("user_work_experience.title", $key, 'both')
         ->group_by("user.id")
         ->order_by("user.id", "DESC");
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function suggestedcandidate($key) {
      $this->db->select('user_skills.*')
         ->from('user_skills')
         ->join('user','user_skills.user_id=user.id')
         ->like("skills", $key, 'both')
         ->where('jobsearch_status','1')
         ->order_by("user_id", "DESC");
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function fetchcandidate($userid, $jobid){
      $this->db->select('*')
         ->from('applied_jobs')
         ->where("user_id", $userid)
         ->where("jobpost_id", $jobid);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function fetchsuggested($userid) {
      $this->db->select('*')
      ->where('id',$userid);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function site_lists($rid) {
      $this->db->select('*')
      ->where('parent_id',$rid)
      ->where('label!=',3);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }
   
   public function candidateApplied_single($id) {
      $this->db->select('user.name,user.id,user.exp_month, user.exp_year, user.superpower,user.education, user.designation, user.email, user.location, user.phone,user.country_code, user.profilePic, user.created_at, user_resume.resume, user_work_experience.title, user_work_experience.company')
         ->from('user')
         ->join('user_resume', 'user.id = user_resume.user_id','left')
         ->join('user_work_experience', 'user.id = user_work_experience.user_id','left')
         ->where('user.id',$id)
         ->group_by("user_work_experience.user_id");
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function candidateEducation_single($id) {
      $this->db->select('*')
         ->from('user_education')
         ->where('user_id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function candidateExp_single($id) {
      $this->db->select('*')
         ->from('user_work_experience')
         ->where('user_id',$id);
         $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function candidateSkills_single($id) {
      $this->db->select('*')
         ->from('user_skills')
         ->where('user_id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function candidateClients_single($id) {
      $this->db->select('*')
         ->from('user_topclients')
         ->where('user_id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function candidateLanguages_single($id) {
      $this->db->select('user_language.id, languages.name')
         ->from('user_language')
         ->join('languages','user_language.lang_id=languages.id')
         ->where('user_language.user_id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function candidateAssessment_single($id) {
      $this->db->select('*')
         ->from('user_assessment')
         ->where('user_id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function candidateExpert_single($id) {
      $this->db->select('*')
         ->from('user_expert')
         ->where('user_id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function contact_insert($data){
       if($this->db->insert('contact_us',$data)){
           $insert_id = $this->db->insert_id();
             return $insert_id;
        } else{
         return false;
        }
   }

   public function skills_fetch() {
      $this->db->distinct();
      $this->db->select('skills');
      $query =  $this->db->get('user_skills');
      return $query->result_array();
   }
   
   public function user_list(){
       $this->db->select('designation, exp_year,id')
        ->from('user')
        ->where('jobsearch_status',1);
        $query = $this->db->get();
      return $query->result_array();
   }

   public function user_list1($id){
       $this->db->select('designation, exp_year,exp_month, id')
        ->from('user')
        ->where('id',$id)
        ->where('jobsearch_status',1);
        $query = $this->db->get();
      return $query->result_array();
   }

   public function fetchnotification($userid, $jobid, $changeid){
      $this->db->select('*')
         ->from('notifications')
         ->where("user_id", $userid)
         ->where("jobapp_id", $jobid)
         ->where("status_changed", $changeid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetchnotifications($userid){
      $this->db->select('*')
         ->from('notifications')
         ->where("user_id", $userid)
         ->where("badge_count", 1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function notification_insert($data){
       if($this->db->insert('notifications',$data)){
           $insert_id = $this->db->insert_id();
             return $insert_id;
        } else{
         return false;
        }
   }



   public function notificationbadge_update($uid,$data) {
      $this->db->where("user_id", $uid);
      $this->db->update('notifications', $data);
      //return $jobapp_id;
   }

   public function notification_update($uid, $changeid,$jobapp_id, $data) {
      $this->db->where("jobapp_id", $jobapp_id);
      $this->db->where("user_id", $uid);
      $this->db->where("status_changed", $changeid);
      $this->db->update('notifications', $data);
      return $jobapp_id;
   }

   public function fetchcomp($recruiter_id){
      $this->db->select('recruiter.cname,recruiter.parent_id, recruiter.email,recruiter.fname, recruiter.lname, recruiter_details.phonecode, recruiter_details.phone')
         ->from('recruiter')
         ->join('recruiter_details','recruiter.id=recruiter_details.recruiter_id')
         ->where("recruiter.id", $recruiter_id);
      $query = $this->db->get();
      return $query->result_array();
   }


   public function candidateduenotification_list($id,$subid) {
      $this->db->select('applied_jobs.id, applied_jobs.jobpost_id,applied_jobs.interviewtime, applied_jobs.interviewdate,applied_jobs.accepted_date, applied_jobs.updated_at,applied_jobs.date_day1, applied_jobs.user_id, user.name, user.email, user.location, user.phone, user.country_code, user.profilePic, job_posting.jobDesc, job_posting.recruiter_id, job_posting.jobtitle, applied_jobs.status, recruiter.cname, recruiter.id as compid, applied_jobs.fallout_reason')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.company_id = recruiter.id')
         ->where("job_posting.recruiter_id", $id)
         ->where("(applied_jobs.status=1 OR applied_jobs.status=5 OR applied_jobs.status=6)")
         ->where("recruiter.parent_id!=", '0');
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
         $this->db->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetchpromonotifications($userid){
      $this->db->select('*')
         ->from('promo_notifications')
         ->where("user_id", $userid)
         ->where("badge_count", 1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function promonotificationbadge_update($uid,$data) {
      $this->db->where("user_id", $uid);
      $this->db->update('promo_notifications', $data);
      
   }

   public function promobadge_update($uid,$data,$id) {
      $this->db->where("user_id", $uid);
      $this->db->where("id", $id);
      $this->db->update('promo_notifications', $data);
      
   }
}
?>
