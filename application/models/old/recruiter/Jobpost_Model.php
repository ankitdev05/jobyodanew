<?php
ob_start();
date_default_timezone_set('Asia/Kolkata');
class Jobpost_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
      $this->load->model('recruiter/Candidate_Model');
   } 

   public function job_insert($data) {
      if($this->db->insert("job_posting", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function job_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('job_posting', $data);
      return true;
   }
    
   public function jobLocation_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('jobposting_location', $data);
      return true;
   }

   public function jobStatus($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('job_posting', $data);
      return true;
   }

   public function basic_salary_insert($data) {
      if($this->db->insert_batch("exp_with_salary", $data)) {
         return true;
      } else{
         return false;
      }
   }

   public function basic_salary_update($data, $id) {
      $this->db->where('id', $id);
      $deleted = $this->db->delete('exp_with_salary');
      if($deleted) {
         if($this->db->insert_batch("exp_with_salary", $data)) {
            return true;
         } else{
            return false;
         }
      } else{
         return false;
      }
   }

   public function jobassignchange($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('job_posting', $data);
      return true;
   }
   
   public function fetch_recruiter_name($id) {
      $this->db->select("fname, lname");
      $this->db->where("id", $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function job_fetch($id,$subid,$status) {
      $cDate = date('y-m-d');
      $this->db->select('job_posting.id,job_posting.recruiter_id,job_posting.subrecruiter_id, job_posting.company_id, job_posting.location,job_posting.boost_status, job_posting.opening, job_posting.jobtitle, job_posting.jobDesc, job_posting.companydetail, job_posting.qualification,job_posting.education, job_posting.skills,job_posting.experience, job_posting.salary, recruiter_details.address, job_posting.jobexpire, job_posting.created_at')
         ->from('job_posting')
         ->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id')
         ->where("job_posting.recruiter_id", $id)
         ->where("job_posting.status", 1);
         
         if($status=='Active'){
            $this->db->where("job_posting.jobexpire >=", $cDate);
         }else if($status=='Expired'){
            $this->db->where("job_posting.jobexpire <", $cDate);
         }else{

         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
         $this->db->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch1($id,$subid,$status,$cid) {
      $cDate = date('y-m-d');
      $this->db->select('job_posting.id,job_posting.recruiter_id,job_posting.subrecruiter_id, job_posting.company_id, job_posting.location,job_posting.boost_status, job_posting.opening, job_posting.jobtitle, job_posting.jobDesc, job_posting.companydetail, job_posting.qualification,job_posting.education, job_posting.skills,job_posting.experience, job_posting.salary, recruiter_details.address, job_posting.jobexpire, job_posting.created_at')
         ->from('job_posting')
         ->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id')
         ->where("job_posting.recruiter_id", $id)
         ->where("job_posting.company_id", $cid);
         //->where("job_posting.status", 1);
         
         if($status!='' && $status=='Active'){
            $this->db->where("job_posting.jobexpire >=", $cDate);
         }else if($status=='Expired'){
            $this->db->where("job_posting.jobexpire <", $cDate);
         }else{

         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
         $this->db->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobappliedCount_fetch($id) {
      $this->db->select('COUNT(applied_jobs.id) as appliedCount')
         ->where("applied_jobs.jobpost_id", $id);
      $query = $this->db->get('applied_jobs');
      return $query->result_array();
   }
   
   public function jobnewCount_fetch($id) {
      $this->db->select('COUNT(applied_jobs.id) as appliedCount')
         ->where("applied_jobs.jobpost_id", $id)
		 ->where("applied_jobs.status", 1);;
      $query = $this->db->get('applied_jobs');
      return $query->result_array();
   }
   
    public function jobgoingCount_fetch($id) {
      $this->db->select('COUNT(applied_jobs.id) as appliedCount')
         ->where("applied_jobs.jobpost_id", $id)
		 ->where("applied_jobs.status", 5);;
      $query = $this->db->get('applied_jobs');
      return $query->result_array();
   }
   
    public function jobfallCount_fetch($id) {
      $this->db->select('COUNT(applied_jobs.id) as appliedCount')
         ->where("applied_jobs.jobpost_id", $id)
		 ->where("applied_jobs.status", 3);;
      $query = $this->db->get('applied_jobs');
      return $query->result_array();
   }

   public function jobApplication_fetch($id,$subid) {
      $this->db->select('job_posting.id')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("job_posting.recruiter_id", $id);
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

    public function newjobApplication_fetch($id,$subid) {
      $this->db->select('job_posting.id')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("job_posting.recruiter_id", $id)
         ->where("applied_jobs.status", 1)
         ->where("job_posting.status", 1);
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function testimonials_fetch() {
      $this->db->select('*')
         ->from('testimonials');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function activeJob_fetch($id,$subid) {
      $currentDate = date("y-m-d");
      $this->db->select('id')
               ->where("recruiter_id", $id)
               ->where("jobexpire >", $currentDate)
               ->where("job_posting.status", 1);
               if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get("job_posting");
      return $query->result_array();
   }

   public function activeJob_fetch1($id,$subid,$cid) {
      $currentDate = date("y-m-d");
      $this->db->select('id')
               ->where("recruiter_id", $id)
               ->where("jobexpire >", $currentDate)
               ->where("job_posting.status", 1);
               $this->db->where("job_posting.company_id", $cid);
               if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
            
         }
      $query = $this->db->get("job_posting");
      return $query->result_array();
   }

   public function expireJob_fetch($id,$subid) {
      $currentDate = date("y-m-d");
      $this->db->select('id')
               ->where("recruiter_id", $id)
               ->where("jobexpire <", $currentDate);
               /*->where("job_posting.status", 2);*/
               if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get("job_posting");
      return $query->result_array();
   }

   public function expireJob_fetch1($id,$subid,$cid) {
      $currentDate = date("y-m-d");
      $this->db->select('id')
               ->where("recruiter_id", $id)
               ->where("jobexpire <", $currentDate);
      $this->db->where("job_posting.company_id", $cid);         
               /*->where("job_posting.status", 2);*/
               if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get("job_posting");
      return $query->result_array();
   }

   public function jobLocation_insert($data) {
      if($this->db->insert("jobposting_location", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function group_locateHome($id,$expfilter) {
      $cDate = date('y-m-d');
     // $expfilter = array($expfilter,"All Tenure");
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++){
         $uniquerarray[] = $ikl;
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.jobtitle,job_posting.allowance, job_posting.jobDesc, job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, 1 as totaljobs");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
       $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $cDate);
       $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
     /* $this->db->where_in("job_posting.experience", $expfilter);*/
      //$this->db->or_where("job_posting.experience", "All Tenure");
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->order_by("exp_with_salary.basicsalary",'desc');
      
      $query = $this->db->get();
      return $query->result_array();
   }

   public function getSalary($jid,$exp){
      $this->db->select('exp,basicsalary');
      $this->db->where("jobpost_id", $jid);
	  $this->db->where("grade_id", $exp);
      $query = $this->db->get('exp_with_salary');
      return $query->result_array();
   }

	public function getExpJob($jid){
      $this->db->select_max('basicsalary');
      $this->db->where("jobpost_id", $jid);
      $query = $this->db->get('exp_with_salary');
      return $query->result_array();
   }
   
    public function getExpJob1($jid,$expfilter){

      
      $this->db->select('basicsalary,exp,grade_id');
      $this->db->where("jobpost_id", $jid);
      $this->db->where("grade_id", $expfilter);
     
      $query = $this->db->get('exp_with_salary');
      return $query->result_array();      
   }
   
   public function getsalaryexp($jid,$expfilter){

      
      $this->db->select('basicsalary,exp');
      $this->db->where("jobpost_id", $jid);
      $this->db->where("grade_id", $expfilter);
     
      $query = $this->db->get('exp_with_salary');
      //echo $query->num_rows();
      if($query->num_rows()>0){
         return $query->result_array();
      }else{
         $this->db->select("basicsalary")
         ->from('exp_with_salary')
         ->where("jobpost_id", $jid);
         $query = $this->db->get();
         return $query->result_array();   
      }      
   }

    public function getstatusJob($jid){
      $this->db->select('status');
      $this->db->where("jobpost_id", $jid);
      $query = $this->db->get('applied_jobs');
      return $query->result_array();
   }

   public function boostamount_fetch(){
      $query = $this->db->get('boost_amount');
      return $query->result_array();
   }


 public function getstatusSavedJob($conditions){
      $this->db->select('status');
      $this->db->where($conditions);
      $query = $this->db->get('saved_jobs');
      return $query->result_array();
   }
   public function job_toppicks($id) {
      $this->db->select("id, picks_id");
      $this->db->where("jobpost_id", $id);
      $this->db->where("picks_id!=", 7);
      $this->db->limit(3);
      $query = $this->db->get('job_toppicks');
      return $query->result_array();
   }

   public function job_toppicks1($id) {
      $this->db->select("id, picks_id");
      $this->db->where("jobpost_id", $id);
      /*$this->db->limit(3);*/
      $query = $this->db->get('job_toppicks');
      return $query->result_array();
   }
   public function job_allowances($id) {
      $this->db->select("id, allowances_id");
      $this->db->where("jobpost_id", $id);
      //$this->db->limit(3);
      $query = $this->db->get('job_allowances');
      return $query->result_array();
   }
   public function job_medical($id) {
      $this->db->select("id, medical_id");
      $this->db->where("jobpost_id", $id);
      //$this->db->limit(3);
      $query = $this->db->get('job_medical');
      return $query->result_array();
   }
   public function job_workshift($id) {
      $this->db->select("id, workshift_id");
      $this->db->where("jobpost_id", $id);
      //$this->db->limit(3);
      $query = $this->db->get('job_workshift');
      return $query->result_array();
   }
   public function job_leaves($id) {
      $this->db->select("id, leaves_id");
      $this->db->where("jobpost_id", $id);
      //$this->db->limit(3);
      $query = $this->db->get('job_leaves');
      return $query->result_array();
   }

   public function job_fetch_latlong($userLat, $userLong, $jobpost_id,$expfilter) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('y-m-d');
       //$expfilter = array($expfilter,"All Tenure");
      $uniquearraybyother = array();
      for($ityu = $expfilter;$ityu >-1;$ityu--){
         $uniquearraybyother[] = $ityu;
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch, recruiter.cname, recruiter.id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
       $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      
      if(!empty($jobpost_id)){
         $this->db->where("job_posting.id", $jobpost_id);
      }


       $this->db->where_in("exp_with_salary.grade_id",$uniquearraybyother);
      
      $this->db->where("recruiter_details.latitude", $userLat);
      $this->db->where("recruiter_details.longitude", $userLong);
      $this->db->order_by("salary",'desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->or_where("job_posting.experience","All Tenure");
      $query = $this->db->get();
      return $query->result_array();
   }


   public function homejob_fetch_latlong($userLat, $userLong, $jobpost_id,$expfilter) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('y-m-d');
       //$expfilter = array($expfilter,"All Tenure");
      $uniquearraybyother = array();
      for($ityu = $expfilter;$ityu >-1;$ityu--){
         $uniquearraybyother[] = $ityu;
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch, recruiter.cname, recruiter.id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
       $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->having("distance<", '500');
      
      if(!empty($jobpost_id)){
         $this->db->where("job_posting.id", $jobpost_id);
      }


       $this->db->where_in("exp_with_salary.grade_id",$uniquearraybyother);
      
      //$this->db->where("recruiter_details.latitude", $userLat);
      //$this->db->where("recruiter_details.longitude", $userLong);
      $this->db->order_by("salary",'desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->or_where("job_posting.experience","All Tenure");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch_latlongg($userLat, $userLong) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('y-m-d');
   
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch, recruiter.cname, recruiter.id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
       $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      
      $this->db->where("recruiter_details.latitude", $userLat);
      $this->db->where("recruiter_details.longitude", $userLong);
      $this->db->order_by("salary",'desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->or_where("job_posting.experience","All Tenure");
      $query = $this->db->get();
      return $query->result_array();
   }


   public function job_fetch_home($userLat, $userLong) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('y-m-d');
   
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch, recruiter.cname, recruiter.id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
       $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->having("distance <","500");
      
      //$this->db->where("recruiter_details.latitude", $userLat);
      //$this->db->where("recruiter_details.longitude", $userLong);
      $this->db->order_by("salary",'desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(10);
      //$this->db->or_where("job_posting.experience","All Tenure");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function homesearch($filterdata) {
      $userLat = (double)$filterdata['lat'];
      $userLong = (double)$filterdata['long'];
      $cDate = date('y-m-d');
      if(!empty($filterdata['lat']) && !empty($filterdata['long'])) {
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch, recruiter.cname,job_posting.company_id, recruiter.id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
         $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
         $this->db->where('recruiter.cname', $filterdata['cname']);
      }else{
         $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);

      $this->db->having("distance <","500");
      
      
      $this->db->order_by("salary",'desc');
      }else{
        $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.company_id, recruiter.cname, recruiter.id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
         $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
         $this->db->where('recruiter.cname', $filterdata['cname']);
      }else{
         $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      }
      $this->db->where("job_posting.jobexpire >=", $cDate);
 
      $this->db->order_by("salary",'desc'); 
      } 
       if(isset($filterdata['joblevell']) && !empty($filterdata['joblevell'])) {
         $jobl = $filterdata['joblevell'];
         //$jobl1 = explode(',', $jobl);
         $this->db->where_in('job_posting.level', $jobl);
      }
      if(isset($filterdata['jobcategoryy']) && !empty($filterdata['jobcategoryy'])) {
         $jobcat = $filterdata['jobcategoryy'];
         //$jobcat1 = explode(',', $jobcat);
         $this->db->where_in('job_posting.category', $jobcat);
      }

      if(isset($filterdata['jobsubcategory']) && !empty($filterdata['jobsubcategory'])) {
         $this->db->where('job_posting.subcategory', $filterdata['jobsubcategory']);
      }
      
      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->or_where("job_posting.experience","All Tenure");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch_home1($id,$userLat, $userLong,$expfilter) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('y-m-d');
      $uniquearraybyother = array();
      for($ityu = $expfilter;$ityu >-1;$ityu--){
         $uniquearraybyother[] = $ityu;
      }
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
       $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      $this->db->having("distance <","500");
      
      //$this->db->where("recruiter_details.latitude", $userLat);
      //$this->db->where("recruiter_details.longitude", $userLong);
      $this->db->order_by("salary",'desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(10);
      //$this->db->or_where("job_posting.experience","All Tenure");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetch_companydetails_bycompanyid($id) {
      $this->db->select("recruiter.cname, recruiter.id as compId,recruiter.parent_id, recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address,recruiter_details.latitude,recruiter_details.longitude");
      $this->db->from('recruiter');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = recruiter.id');
      $this->db->where("recruiter_details.recruiter_id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetch_companyaddress($id) {
      $this->db->select("recruiter.cname, recruiter.id as compId,recruiter.parent_id, recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address,recruiter_details.latitude,recruiter_details.longitude");
      $this->db->from('recruiter');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = recruiter.id');
      $this->db->where("recruiter.id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch_bycompId($id,$expfilter) {
      $cDate = date('y-m-d');
      $uniquearraybyother = array();
      for($ityu = $expfilter;$ityu >-1;$ityu--){
         $uniquearraybyother[] = $ityu;
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.jobtitle, job_posting.jobDesc, job_posting.jobexpire, recruiter.cname, recruiter.id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.company_id", $id);
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch_byrId($id) {
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc, job_posting.salary, job_posting.jobexpire, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.recruiter_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.recruiter_id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch_TopPicks($id) {
      $this->db->select("id, picks_id");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_toppicks');
      return $query->result_array();
   }

   public function company_detail_fetch($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function recruiter_detail_fetch($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function company_name_fetch($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   /*public function company_name_fetch($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }*/

   public function company_detail1_fetch($id) {
      $this->db->select("recruiter_details.phone,recruiter_details.phonecode,recruiter_details.companyDesc, recruiter_details.landline, recruiter.email, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address, recruiter_details.comm_channel");
      $this->db->from('recruiter_details');
      $this->db->join('recruiter','recruiter_details.recruiter_id=recruiter.id');
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_skills($id) {
      $this->db->select("skills.skill");
      $this->db->from('job_skills');
      $this->db->join('skills','job_skills.skill_id=skills.id');
      $this->db->where('job_skills.jobpost_id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }
   public function job_detail_fetch($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('job_posting');
      return $query->result_array();
   }

   public function jobupdate_detail_fetch($id) {
      $this->db->select("job_posting.id,job_posting.certification, job_posting.recruiter_id,job_posting.subrecruiter_id,job_posting.joining_bonus, job_posting.jobPitch,job_posting.allowance, job_posting.company_id, job_posting.jobtitle, job_posting.opening, job_posting.experience, job_posting.industry, job_posting.language , job_posting.qualification, job_posting.level_status, job_posting.education_status, job_posting.experience_status, job_posting.category, job_posting.subcategory, job_posting.level,job_posting.education, job_posting.other_language, job_posting.channel, job_posting.salary, job_posting.jobDesc, job_posting.skills, job_posting.qualification, job_posting.jobexpire, job_posting.created_at, recruiter_details.site_name, recruiter_details.address")
               ->where('job_posting.id', $id)
               ->from('job_posting')
               ->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id')
               ->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobupdate_detailexp_fetch($id) {
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('exp_with_salary');
      return $query->result_array();
   }

   public function job_detailLocation_fetch($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }
   public function appliedjob_insert($data) {
      if($this->db->insert("applied_jobs", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function job_fetch_appliedbycompid($jobid, $id) {
      $this->db->where('jobpost_id', $jobid);
      $this->db->where('user_id', $id);
      $query = $this->db->get('applied_jobs');
      return $query->result_array();
   }

   public function job_fetch_applied($uid) {
      $this->db->select('applied_jobs.jobpost_id,applied_jobs.updated_at, job_posting.recruiter_id,job_posting.jobtitle')
               ->where('(applied_jobs.status=1 OR applied_jobs.status=3)')
               ->where('user_id', $uid)
               ->from('applied_jobs')
               ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')
               ->order_by('applied_jobs.id','desc')
               ->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch_hired() {
      $this->db->select('applied_jobs.jobpost_id,applied_jobs.updated_at, job_posting.recruiter_id,job_posting.jobtitle')
               ->where('applied_jobs.status', '7')
               ->from('applied_jobs')
               ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_openings() {
      $cDate = date('y-m-d');
      $this->db->select('sum(opening) as openings')
               ->where('jobexpire >=', $cDate)
               ->from('job_posting');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function totalhired($id) {
      $this->db->select('count(applied_jobs.jobpost_id) as appliedCount')
               ->where('(applied_jobs.status=7)')
               ->where('job_posting.id',$id)
               ->from('applied_jobs')
               ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function totalaccpt($id) {
      $this->db->select('count(applied_jobs.jobpost_id) as appliedCount')
               ->where('(applied_jobs.status=6)')
               ->where('job_posting.id',$id)
               ->from('applied_jobs')
               ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function noshow($id) {
      $this->db->select('count(applied_jobs.jobpost_id) as appliedCount')
               ->where('applied_jobs.status',3)
               ->where('applied_jobs.fallout_status',1)
               ->where('job_posting.id',$id)
               ->from('applied_jobs')
               ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function noshow2($id) {
      $this->db->select('count(applied_jobs.jobpost_id) as appliedCount')
               ->where('applied_jobs.status',3)
               ->where('applied_jobs.fallout_status',2)
               ->where('job_posting.id',$id)
               ->from('applied_jobs')
               ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function noshow3($id) {
      $this->db->select('count(applied_jobs.jobpost_id) as appliedCount')
               ->where('applied_jobs.status',3)
               ->where('applied_jobs.fallout_status',3)
               ->where('job_posting.id',$id)
               ->from('applied_jobs')
               ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function noshow5($id) {
      $this->db->select('count(applied_jobs.jobpost_id) as appliedCount')
               ->where('applied_jobs.status',3)
               ->where('applied_jobs.fallout_status',5)
               ->where('job_posting.id',$id)
               ->from('applied_jobs')
               ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function refer($id) {
      $this->db->select('count(applied_jobs.jobpost_id) as appliedCount')
               ->where('applied_jobs.status',4)
               ->where('job_posting.id',$id)
               ->from('applied_jobs')
               ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_appliedbyyjobid($jid) {
      $this->db->select('count(id) as newcount')
               ->where('status', '1')
               ->from('applied_jobs')
               ->where('jobpost_id',$jid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function hiredjob_appliedbyyjobid($jid) {
      $this->db->select('count(id) as newcount')
               ->where('(applied_jobs.status=6 OR applied_jobs.status=7)')
               ->from('applied_jobs')
               ->where('jobpost_id',$jid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function goingjob_appliedbyyjobid($jid) {
      $this->db->select('count(id) as newcount')
               ->where('status', '5')
               ->from('applied_jobs')
               ->where('jobpost_id',$jid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function acceptedjob_appliedbyyjobid($jid) {
      $this->db->select('count(id) as newcount')
               ->where('status', '6')
               ->from('applied_jobs')
               ->where('jobpost_id',$jid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch_appliedid($aid) {
      $this->db->select('jobpost_id,status as jobstatus')
               ->where('id', $aid)
               ->from('applied_jobs');
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function job_fetch_appliedCheck11($id,$uid) {
      $this->db->select("*");
      $this->db->where('job_id', $id);
      $this->db->where('user_id', $uid);
      $query = $this->db->get('reconciliation');
      return $query->result_array();
   }

   public function fetch_companyPic($id) {
      $this->db->select("recruiter_id, companyPic,address,companyDesc,latitude,longitude");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }

   public function fetch_companyPic1($id) {
      $this->db->select("pic");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('site_images');
      return $query->result_array();
   }

   public function fetch_companyName($id) {
      $this->db->select("cname");
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function fetch_companydetail($id) {
      $this->db->select("companyPic,address,companyDesc");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }
   public function fetch_jobDetail1($id) {
      $cDate = date('y-m-d');
      $this->db->select("*");
      $this->db->where('company_id', $id);
      $this->db->where("jobexpire >=", $cDate);
      $query = $this->db->get('job_posting');
      return $query->result_array();
   }

   public function fetch_jobDetail($id,$expfilter) {
      $uniquearraybyother = array();
      for($ityu = $expfilter;$ityu >-1;$ityu--){
         $uniquearraybyother[] = $ityu;
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.jobtitle, job_posting.company_id, job_posting.jobPitch, job_posting.jobDesc,  job_posting.jobexpire, recruiter.cname, recruiter.id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.company_id", $id);
      $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function recruiter_toppickss($id) {
      $this->db->select("*");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get(' recruiter_toppicks');
      return $query->result_array();
   }

   public function jobappliedpast_fetch_byuserid($id) {
      $currentDate = date("y-m-d");
      $currentTime = date("H:i:s");
      //echo $currentTime;die;
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.salary, job_posting.company_id,job_posting.recruiter_id, applied_jobs.status, applied_jobs.date_day1")
               ->from('job_posting')
               ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
               ->where('applied_jobs.user_id', $id)
               ->where('applied_jobs.status!=', 1)
               //->where('applied_jobs.interviewdate <', $currentDate)
               //->where('applied_jobs.interviewtime <=', $currentTime)
               ->order_by('applied_jobs.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobappliedupcoming_fetch_byuserid($id) {
      $currentDate = date("y-m-d");
      $currentTime = date("H:i:s");
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.salary, job_posting.company_id,job_posting.recruiter_id, applied_jobs.status, applied_jobs.interviewdate, applied_jobs.interviewtime")
               ->from('job_posting')
               ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
               ->where('applied_jobs.user_id', $id)
               ->where('applied_jobs.interviewdate >=', $currentDate)
               ->where('applied_jobs.status', 1)
               //->where('applied_jobs.interviewtime >=', $currentTime)
               ->order_by('applied_jobs.id','desc');;
      $query = $this->db->get();
      return $query->result_array();
   }

   public function savejob_insert($data) {
      if($this->db->insert("saved_jobs", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function savejob_update($condstatus,$data) {
      $this->db->where($condstatus);
      $this->db->update('saved_jobs', $data);
      return true;
   }


   public function jobSaved_fetch_byuserid($id) {
      
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobexpire , job_posting.salary, job_posting.company_id, job_posting.recruiter_id")
               ->where('saved_jobs.user_id', $id)
               ->from('job_posting')
               ->join('saved_jobs', 'saved_jobs.jobpost_id = job_posting.id');
      $query = $this->db->get();
      return $query->result_array();
   }


   public function JobSavedd_fetcH_byUseriD($conditions) {
      
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc, job_posting.salary, job_posting.company_id, job_posting.recruiter_id, job_posting.jobPitch")
               ->where($conditions)
               ->from('job_posting')
               ->join('saved_jobs', 'saved_jobs.jobpost_id = job_posting.id');

      $query = $this->db->get();
      return $query->result_array();
   }


   public function applied_status_fetch($jobid, $id) {
      $this->db->where('jobpost_id', $jobid);
      $this->db->where('user_id', $id);
      $query = $this->db->get('applied_jobs');
      return $query->result_array();
   }

   public function job_fetch_all($filterdata, $id, $expfilter) {
      $cDate = date('y-m-d');
      if(!empty($filterdata['lat'])){
         $lat = $filterdata['lat'];
      }
      if(!empty($filterdata['long'])){
         $lang = $filterdata['long'];
      }

       $uniquearraybyother = array();
      for($ityu = $expfilter;$ityu >-1;$ityu--){
         $uniquearraybyother[] = $ityu;
      }

      if(!empty($filterdata['lat']) && !empty($filterdata['long'])) {
         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.jobexpire, job_posting.id, job_posting.jobtitle, job_posting.jobPitch,job_posting.jobDesc, job_posting.company_id, job_posting.allowance,job_posting.recruiter_id, recruiter.cname, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address, (6371 * acos (cos ( radians($lat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($lang) )+ sin ( radians($lat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      
      if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
         $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
         $this->db->where('recruiter.cname', $filterdata['cname']);
      }else{
         $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $cDate);
      
      $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);

      $this->db->having("distance<",'1000');
      $this->db->order_by("distance",'asc');
      }else{
         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.jobexpire, job_posting.id, job_posting.jobtitle, job_posting.jobPitch,job_posting.jobDesc, job_posting.company_id,job_posting.allowance,job_posting.recruiter_id, recruiter.cname, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         
         if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
         $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
         
         $this->db->where('recruiter.cname', $filterdata['cname']);
      }else{
         $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
      }
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      }
      
      if(isset($filterdata['industry']) && !empty($filterdata['industry'])) {
         $this->db->where('job_posting.industry', $filterdata['industry']);
      }
     
      if(isset($filterdata['location']) && !empty($filterdata['location'])) {
          $location = $filterdata['location'];
          $location1 = explode(",", $location);
          $this->db->like('recruiter_details.region', $location1[0]);
      }
      if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel']) && $filterdata['joblevel']!="9") {
         $jobl = $filterdata['joblevel'];
         $jobl1 = explode(',', $jobl);
         $this->db->where_in('job_posting.level', $jobl1);
      }
      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory']) && $filterdata['jobcategory']!="9") {
         $jobcat = $filterdata['jobcategory'];
         $jobcat1 = explode(',', $jobcat);
         $this->db->where_in('job_posting.category', $jobcat1);
      }

      if(isset($filterdata['joblevell']) && !empty($filterdata['joblevell'])) {
         $jobl = $filterdata['joblevell'];
        // $jobl1 = explode(',', $jobl);
         $this->db->where_in('job_posting.level', $jobl);
      }
      if(isset($filterdata['jobcategoryy']) && !empty($filterdata['jobcategoryy'])) {
         $jobcat = $filterdata['jobcategoryy'];
         
         $this->db->where_in('job_posting.category', $jobcat);
      }

      if(isset($filterdata['jobsubcategory']) && !empty($filterdata['jobsubcategory'])) {
         $this->db->where('job_posting.subcategory', $filterdata['jobsubcategory']);
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }

      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leave = $filterdata['leaves'];
         $categories5 = explode(",", $leave);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows > 0) {
         return $query->result_array();
      } else{
         if(!empty($filterdata['lat']) && !empty($filterdata['long'])){
            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.jobexpire, job_posting.id, job_posting.jobtitle, job_posting.jobPitch,job_posting.jobDesc, job_posting.company_id,job_posting.allowance,job_posting.recruiter_id, recruiter.cname, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address, (6371 * acos (cos ( radians($lat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($lang) )+ sin ( radians($lat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            
            if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
               $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
               $this->db->where('recruiter.cname', $filterdata['cname']);
            }else{
               $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
            }
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            $this->db->where("job_posting.jobexpire >=", $cDate);
           $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            
            $this->db->having("distance<",'1000');
            $this->db->order_by("distance",'asc');
         }else{
           $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.jobexpire, job_posting.id, job_posting.jobtitle, job_posting.jobPitch,job_posting.jobDesc, job_posting.company_id,job_posting.allowance,job_posting.recruiter_id, recruiter.cname, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
            $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
           
            if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
               $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
               
               $this->db->where('recruiter.cname', $filterdata['cname']);
            }else{
               $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
            }
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            $this->db->where("job_posting.jobexpire >=", $cDate); 
            $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
           
         }
         
      if(isset($filterdata['industry']) && !empty($filterdata['industry'])) {
         $this->db->where('job_posting.industry', $filterdata['industry']);
      }
      if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->where('recruiter.cname', $filterdata['cname']);
      }
      
      if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel']) && $filterdata['joblevel']!="9") {
         $jobl = $filterdata['joblevel'];
         $jobl1 = explode(',', $jobl);
         $this->db->where_in('job_posting.level', $jobl1);
      }
      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory']) && $filterdata['jobcategory']!="9") {
         $jobcat = $filterdata['jobcategory'];
         $jobcat1 = explode(',', $jobcat);
         $this->db->where_in('job_posting.category', $jobcat1);
      }

      
      if(isset($filterdata['joblevell']) && !empty($filterdata['joblevell'])) {
         $jobl = $filterdata['joblevell'];
        
         $this->db->where_in('job_posting.level', $jobl);
      }
      if(isset($filterdata['jobcategoryy']) && !empty($filterdata['jobcategoryy'])) {
         $jobcat = $filterdata['jobcategoryy'];
         //$jobcat1 = explode(',', $jobcat);
         $this->db->where_in('job_posting.category', $jobcat);
      }

      if(isset($filterdata['jobsubcategory']) && !empty($filterdata['jobsubcategory'])) {
         $this->db->where('job_posting.subcategory', $filterdata['jobsubcategory']);
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }

      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leave = $filterdata['leaves'];
         $categories5 = explode(",", $leave);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }
       $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
      
     }
  }
 


   public function jobcount_fetch_all($filterdata, $id, $expfilter) {
      $cDate = date('y-m-d');
       if(!empty($filterdata['lat'])){
         $lat = $filterdata['lat'];
      }
      if(!empty($filterdata['long'])){
         $lang = $filterdata['long'];
      }
      //$expfilter = array($expfilter,'All Tenure');
      $uniquearraybyother = array();
      for($ityu = $expfilter;$ityu >-1;$ityu--){
         $uniquearraybyother[] = $ityu;
      }
      if(!empty($filterdata['lat']) && !empty($filterdata['long'])){
         $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.company_id,job_posting.allowance,job_posting.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address, 1 as jobcount , (6371 * acos (cos ( radians($lat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($lang) )+ sin ( radians($lat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
            $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
            $this->db->where('recruiter.cname', $filterdata['cname']);
         }else{
            $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
         }
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         $this->db->where("job_posting.jobexpire >=", $cDate);
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         $this->db->having("distance<",'1000');
         $this->db->order_by("distance",'asc');
      }else{
         $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id, job_posting.company_id,job_posting.allowance,job_posting.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address, 1 as jobcount");
         $this->db->from('job_posting');
         $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
         //$this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
         if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
            $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
            $this->db->where('recruiter.cname', $filterdata['cname']);
         }else{
            $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
         }
         if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
           $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
           $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
           $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
           $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
         }
         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
         $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
         $this->db->where("job_posting.jobexpire >=", $cDate);
         //$this->db->where_in("exp_with_salary.exp", $expfilter);
         $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
        // $this->db->where_in("job_posting.experience", $expfilter);
         //$this->db->or_where("job_posting.experience", "All Tenure");
      }
      
      if(isset($filterdata['industry']) && !empty($filterdata['industry'])) {
         $this->db->where('job_posting.industry', $filterdata['industry']);
      }
      /*if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         $this->db->where('recruiter.cname', $filterdata['cname']);
      }*/
      if(isset($filterdata['location']) && !empty($filterdata['location'])) {
         $location = $filterdata['location'];
         $location1 = explode(",", $location);
         $this->db->like('recruiter_details.region', $location1[0]);
      }
      
      if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel']) && $filterdata['joblevel']!="9") {
         $jobl = $filterdata['joblevel'];
         $jobl1 = explode(',', $jobl);
         $this->db->where_in('job_posting.level', $jobl1);
      }
      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory']) && $filterdata['jobcategory']!="9") {
         $jobcat = $filterdata['jobcategory'];
         $jobcat1 = explode(',', $jobcat);
         $this->db->where_in('job_posting.category', $jobcat1);
      }
      /*if(isset($filterdata['locationn']) && !empty($filterdata['locationn'])) {
          $location = $filterdata['locationn'];
          $location1 = explode(",", $location);
         ///$location = $filterdata['locationn'];
       
         $this->db->like('recruiter_details.address', $location1[0]);
      }*/
      
      if(isset($filterdata['joblevell']) && !empty($filterdata['joblevell'])) {
         $jobl = $filterdata['joblevell'];
       
         $this->db->where_in('job_posting.level', $jobl);
      }
      if(isset($filterdata['jobcategoryy']) && !empty($filterdata['jobcategoryy'])) {
         $jobcat = $filterdata['jobcategoryy'];
         $this->db->where_in('job_posting.category', $jobcat);
      }
      if(isset($filterdata['jobsubcategory']) && !empty($filterdata['jobsubcategory'])) {
         $this->db->where('job_posting.subcategory', $filterdata['jobsubcategory']);
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }

      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leave = $filterdata['leaves'];
         $categories5 = explode(",", $leave);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      if($query->num_rows>0){
         return $query->result_array();
      }else{
         if(!empty($filterdata['lat']) && !empty($filterdata['long'])){
            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id, job_posting.company_id,job_posting.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address,job_posting.allowance, 1 as jobcount , (6371 * acos (cos ( radians($lat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($lang) )+ sin ( radians($lat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
             $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            //$this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
            if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
               $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
              
               $this->db->where('recruiter.cname', $filterdata['cname']);
            }else{
               $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
            }
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            $this->db->where("job_posting.jobexpire >=", $cDate);
           // $this->db->where_in("exp_with_salary.exp", $expfilter);
            $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            //$this->db->or_where("job_posting.experience", "All Tenure");
            $this->db->having("distance<",'1000');
            $this->db->order_by("distance",'asc');
         }
         else{
            $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id, job_posting.company_id,job_posting.allowance,job_posting.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address, 1 as jobcount");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
             $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            //$this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
            if(isset($filterdata['cname']) && !empty($filterdata['cname'])) {
         
               $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
               $this->db->where('recruiter.cname', $filterdata['cname']);
            }else{
               $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
            }
            if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
              $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
              $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
              $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
              $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
            }
            if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         }
         
      if(isset($filterdata['industry']) && !empty($filterdata['industry'])) {
         $this->db->where('job_posting.industry', $filterdata['industry']);
      }
      
      
      if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel']) && $filterdata['joblevel']!="9") {
         $jobl = $filterdata['joblevel'];
         $jobl1 = explode(',', $jobl);
         $this->db->where_in('job_posting.level', $jobl1);
      }
      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory']) && $filterdata['jobcategory']!="9") {
         $jobcat = $filterdata['jobcategory'];
         $jobcat1 = explode(',', $jobcat);
         $this->db->where_in('job_posting.category', $jobcat1);
      }
      
      if(isset($filterdata['joblevell']) && !empty($filterdata['joblevell'])) {
         $jobl = $filterdata['joblevell'];
       
         $this->db->where_in('job_posting.level', $jobl);
      }
      if(isset($filterdata['jobcategoryy']) && !empty($filterdata['jobcategoryy'])) {
         $jobcat = $filterdata['jobcategoryy'];
         $this->db->where_in('job_posting.category', $jobcat);
      }
      if(isset($filterdata['jobsubcategory']) && !empty($filterdata['jobsubcategory'])) {
         $this->db->where('job_posting.subcategory', $filterdata['jobsubcategory']);
      }
      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }

      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leave = $filterdata['leaves'];
         $categories5 = explode(",", $leave);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
      }
   }

   public function jobfilter_fetch_all($filterdata, $id,$expfilter) {
      $cDate = date('y-m-d');
      if(!empty($filterdata['lat'])){
         $lat = $filterdata['lat'];
      }
      if(!empty($filterdata['long'])){
         $lang = $filterdata['long'];
      }
      //$expfilter = array($expfilter,'All Tenure');
      $uniquearraybyother = array();
      for($ityu = $expfilter;$ityu >-1;$ityu--){
         $uniquearraybyother[] = $ityu;
      }
      if(!empty($filterdata['lat']) && !empty($filterdata['long'])){

        
          $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire, job_posting.id,job_posting.allowance, job_posting.jobtitle, job_posting.jobPitch, job_posting.company_id,job_posting.recruiter_id, job_posting.jobDesc, recruiter.cname, (6371 * acos (cos ( radians($lat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($lang) )+ sin ( radians($lat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance , recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address");
          $this->db->from('job_posting');
          $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
          $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
          $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
          $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
          $this->db->where("job_posting.jobexpire >=", $cDate);
          $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
          $this->db->having("distance<",'1000');
          $this->db->order_by("distance",'asc');
       
      } else{
          $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.jobexpire,job_posting.id,job_posting.allowance, job_posting.jobtitle, job_posting.jobPitch, job_posting.company_id,job_posting.recruiter_id, job_posting.jobDesc, recruiter.cname, recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address");
          $this->db->from('job_posting');
          $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
          $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
          $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
          $this->db->where("job_posting.jobexpire >=", $cDate);
          $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
          // $this->db->where_in("exp_with_salary.exp", $expfilter);
          // $this->db->or_where('job_posting.experience',"All Tenure");
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
        $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
        $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
        $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
        $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
      }
      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
        $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
      }

      /*if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      }*/

      if(isset($filterdata['industry']) && !empty($filterdata['industry'])) {
         $this->db->where('job_posting.industry', $filterdata['industry']);
      }
      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
         $this->db->where('exp_with_salary.basicsalary >=', $filterdata['basicSalary']);
      }

      /*if(isset($filterdata['location']) && !empty($filterdata['location'])) {
          $location = $filterdata['location'];
          $location1 = explode(",", $location);
          $this->db->like('recruiter_details.region', $location1[0]);
      }*/
      if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $jobl = $filterdata['joblevel'];
         $jobl1 = explode(',', $jobl);
         $this->db->where_in('job_posting.level', $jobl1);
      }
      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $jobcat = $filterdata['jobcategory'];
         $jobcat1 = explode(',', $jobcat);
         $this->db->where_in('job_posting.category', $jobcat1);
      }

      if(isset($filterdata['jobsubcategory']) && !empty($filterdata['jobsubcategory'])) {
         $this->db->where('job_posting.subcategory', $filterdata['jobsubcategory']);
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary']) && isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }else if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks']) && empty($filterdata['basicSalary'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->or_where_in('job_toppicks.picks_id', $categories1);
      }
      if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $allowances = $filterdata['allowances'];
         $categories2 = explode(",", $allowances);
         $this->db->or_where_in('job_allowances.allowances_id', $categories2);
      }
      if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $medical = $filterdata['medical'];
         $categories3 = explode(",", $medical);
         $this->db->or_where_in('job_medical.medical_id', $categories3);
      }
      if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $workshift = $filterdata['workshift'];
         $categories4 = explode(",", $workshift);
         $this->db->or_where_in('job_workshift.workshift_id', $categories4);
      }

      if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
         $leave = $filterdata['leaves'];
         $categories5 = explode(",", $leave);
         $this->db->or_where_in('job_leaves.leaves_id', $categories5);
      }

      
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      
      if($query->num_rows > 0) {
        return $query->result_array();
      
      } else{

          if(!empty($filterdata['lat']) && !empty($filterdata['long'])) {

              $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.jobexpire,job_posting.id,job_posting.allowance, job_posting.jobtitle, job_posting.jobDesc, job_posting.company_id, job_posting.recruiter_id, job_posting.jobPitch, recruiter.cname, (6371 * acos (cos ( radians($lat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($lang) )+ sin ( radians($lat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance , recruiter_details.latitude, recruiter_details.longitude, recruiter_details.address");
              $this->db->from('job_posting');
              $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
              $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
              $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
              $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
              $this->db->where("job_posting.jobexpire >=", $cDate);
               $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
               //$this->db->where_in("exp_with_salary.exp", $expfilter);
               //$this->db->or_where('job_posting.experience',"All Tenure");
              $this->db->having("distance<",'1000');
              $this->db->order_by("distance",'asc');

          } else {
              $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.jobexpire,job_posting.id,job_posting.allowance, job_posting.jobtitle, job_posting.jobDesc, job_posting.company_id,job_posting.recruiter_id, recruiter.cname, recruiter_details.latitude, job_posting.jobPitch,job_posting.jobPitch,recruiter_details.longitude, recruiter_details.address");
              $this->db->from('job_posting');
              $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
              $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
              $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
              $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
              $this->db->where("job_posting.jobexpire >=", $cDate);
              $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
               //$this->db->where_in("exp_with_salary.exp", $expfilter);
              // $this->db->or_where('job_posting.experience',"All Tenure");
          }

          if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
          }
          if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
          }
          if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
          }
          if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
          }
          if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }

          /*if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
             $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
          }*/

          if(isset($filterdata['industry']) && !empty($filterdata['industry'])) {
             $this->db->where('job_posting.industry', $filterdata['industry']);
          }
          if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
             $this->db->where('exp_with_salary.basicsalary>=', $filterdata['basicSalary']);
          }

          /*if(isset($filterdata['location']) && !empty($filterdata['location'])) {
              $location = $filterdata['location'];
              $location1 = explode(",", $location);
              $this->db->like('recruiter_details.address', $location1[0]);
          }*/

          if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
             $jobl = $filterdata['joblevel'];
             $jobl1 = explode(',', $jobl);
             $this->db->where_in('job_posting.level', $jobl1);
          }
          if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
             $jobcat = $filterdata['jobcategory'];
             $jobcat1 = explode(',', $jobcat);
             $this->db->where_in('job_posting.category', $jobcat1);
          }

          if(isset($filterdata['jobsubcategory']) && !empty($filterdata['jobsubcategory'])) {
             $this->db->where('job_posting.subcategory', $filterdata['jobsubcategory']);
          }

          if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary']) && isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $categories1 = explode(",", $toppicks);
               $this->db->where_in('job_toppicks.picks_id', $categories1);
            }else if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks']) && empty($filterdata['basicSalary'])) {
               $toppicks = $filterdata['toppicks'];
               $categories1 = explode(",", $toppicks);
               $this->db->or_where_in('job_toppicks.picks_id', $categories1);
            }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $categories2 = explode(",", $allowances);
            $this->db->or_where_in('job_allowances.allowances_id', $categories2);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $categories3 = explode(",", $medical);
            $this->db->or_where_in('job_medical.medical_id', $categories3);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $categories4 = explode(",", $workshift);
            $this->db->or_where_in('job_workshift.workshift_id', $categories4);
         }

         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leave = $filterdata['leaves'];
            $categories5 = explode(",", $leave);
            $this->db->or_where_in('job_leaves.leaves_id', $categories5);
         }

          $this->db->group_by("exp_with_salary.jobpost_id");
          $query = $this->db->get();
          return $query->result_array();
      }
      

   }

   public function jobfiltercount_fetch_all($filterdata, $id,$expfilter) {
      $cDate = date('y-m-d');
      if(!empty($filterdata['lat'])){
         $lat = $filterdata['lat'];
      }
      if(!empty($filterdata['long'])) {
         $lang = $filterdata['long'];
      }
      $uniquearraybyother = array();
      for($ityu = $expfilter;$ityu >-1;$ityu--){
         $uniquearraybyother[] = $ityu;
      }
      //$expfilter = array($expfilter,"All Tenure");
      if(!empty($filterdata['lat']) && !empty($filterdata['long'])){
      
          $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id,job_posting.allowance, job_posting.company_id,job_posting.recruiter_id,  recruiter_details.latitude, recruiter_details.longitude, 1 as jobcount, recruiter_details.address, (6371 * acos (cos ( radians($lat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($lang) )+ sin ( radians($lat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance ");
          $this->db->from('job_posting');
          $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
          $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
          $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
          $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
          $this->db->where("job_posting.jobexpire >=", $cDate);
           $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
           //$this->db->where_in("exp_with_salary.exp", $expfilter);
           //$this->db->or_where('job_posting.experience',"All Tenure");
          $this->db->having("distance<",'1000');
          $this->db->order_by("distance",'asc');
      }else{
        $this->db->select("job_posting.id,job_posting.allowance, job_posting.company_id,job_posting.recruiter_id, max(exp_with_salary.basicsalary) as salary, recruiter_details.latitude, recruiter_details.longitude, 1 as jobcount, recruiter_details.address");
        $this->db->from('job_posting');
        $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
        $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
        $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
        $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
        $this->db->where("job_posting.jobexpire >=", $cDate);
        $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
         //$this->db->where_in("exp_with_salary.exp", $expfilter);
         //$this->db->or_where('job_posting.experience',"All Tenure");
      }

      if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
       }
       if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
         $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
       }
       if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
         $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
       }
       if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
         $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
       }
       if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
           $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
         }
       if(isset($filterdata['industry']) && !empty($filterdata['industry'])) {
             $this->db->where('job_posting.industry', $filterdata['industry']);
          }  
      /*if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
             $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
          }*/
      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
             $this->db->where('exp_with_salary.basicsalary >=', $filterdata['basicSalary']);
          }

      if(isset($filterdata['location']) && !empty($filterdata['location'])) {
          $location = $filterdata['location'];
          $location1 = explode(",", $location);
          $this->db->like('recruiter_details.region', $location1[0]);
      }
      if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
         $jobl = $filterdata['joblevel'];
         $jobl1 = explode(',', $jobl);
         $this->db->where_in('job_posting.level', $jobl1);
      }
      if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
         $jobcat = $filterdata['jobcategory'];
         $jobcat1 = explode(',', $jobcat);
         $this->db->where_in('job_posting.category', $jobcat1);
      }

      if(isset($filterdata['jobsubcategory']) && !empty($filterdata['jobsubcategory'])) {
         $this->db->where('job_posting.subcategory', $filterdata['jobsubcategory']);
      }


      if(isset($filterdata['locationSort']) && !empty($filterdata['locationSort']) && $filterdata['locationSort'] == 1) {
         $this->db->order_by('recruiter_details.address', "DESC");
      }
      if(isset($filterdata['language']) && !empty($filterdata['language'])) {
         $this->db->where('job_posting.language', $filterdata['language']);
      }
      if(isset($filterdata['salarySort']) && !empty($filterdata['salarySort']) && $filterdata['salarySort'] == 1) {
         $this->db->order_by('job_posting.salary', "DESC");
      }

      if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary']) && isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->where_in('job_toppicks.picks_id', $categories1);
      }else if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks']) && empty($filterdata['basicSalary'])) {
         $toppicks = $filterdata['toppicks'];
         $categories1 = explode(",", $toppicks);
         $this->db->or_where_in('job_toppicks.picks_id', $categories1);
      }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $categories2 = explode(",", $allowances);
            $this->db->or_where_in('job_allowances.allowances_id', $categories2);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $categories3 = explode(",", $medical);
            $this->db->or_where_in('job_medical.medical_id', $categories3);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $categories4 = explode(",", $workshift);
            $this->db->or_where_in('job_workshift.workshift_id', $categories4);
         }

         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leave = $filterdata['leaves'];
            $categories5 = explode(",", $leave);
            $this->db->or_where_in('job_leaves.leaves_id', $categories5);
         }
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      
      if($query->num_rows > 0) {
         return $query->result_array();
      } else{
          
          if(!empty($filterdata['lat']) && !empty($filterdata['long'])){
      
              $this->db->select("job_posting.id,job_posting.allowance, job_posting.company_id,job_posting.recruiter_id,  max(exp_with_salary.basicsalary) as salary, recruiter_details.latitude, recruiter_details.longitude, 1 as jobcount, recruiter_details.address, (6371 * acos (cos ( radians($lat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($lang) )+ sin ( radians($lat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance ");
              $this->db->from('job_posting');
              $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
              $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
              $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
              $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
              $this->db->where("job_posting.jobexpire >=", $cDate);
              $this->db->having("distance<",'1000');
                $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
           //    $this->db->where_in("exp_with_salary.exp", $expfilter);
               //$this->db->or_where('job_posting.experience',"All Tenure");
              $this->db->order_by("distance",'asc');
          
          } else {
            $this->db->select("job_posting.id,job_posting.allowance, job_posting.company_id,job_posting.recruiter_id, max(exp_with_salary.basicsalary) as salary,recruiter_details.latitude, recruiter_details.longitude,1 as jobcount, recruiter_details.address");
            $this->db->from('job_posting');
            $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
            $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
             $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
            $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
            $this->db->where("job_posting.jobexpire >=", $cDate);
            $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
            // $this->db->where_in("exp_with_salary.exp", $expfilter);
            // $this->db->or_where('job_posting.experience',"All Tenure");
          }

          if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
            $this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
          }
          if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $this->db->join('job_allowances', 'job_allowances.jobpost_id = job_posting.id');
          }
          if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $this->db->join('job_medical', 'job_medical.jobpost_id = job_posting.id');
          }
          if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $this->db->join('job_workshift', 'job_workshift.jobpost_id = job_posting.id');
          }
          if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
              $this->db->join('job_leaves', 'job_leaves.jobpost_id = job_posting.id');
            }
          /*if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
             $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
          }*/
          if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary'])) {
             $this->db->where('exp_with_salary.basicsalary >=', $filterdata['basicSalary']);
          }

          if(isset($filterdata['industry']) && !empty($filterdata['industry'])) {
             $this->db->where('job_posting.industry', $filterdata['industry']);
          }

          /*if(isset($filterdata['location']) && !empty($filterdata['location'])) {
              $location = $filterdata['location'];
              $location1 = explode(",", $location);
              $this->db->like('recruiter_details.address', $location1[0]);
          }*/
          if(isset($filterdata['joblevel']) && !empty($filterdata['joblevel'])) {
             $jobl = $filterdata['joblevel'];
             $jobl1 = explode(',', $jobl);
             $this->db->where_in('job_posting.level', $jobl1);
          }
          if(isset($filterdata['jobcategory']) && !empty($filterdata['jobcategory'])) {
             $jobcat = $filterdata['jobcategory'];
             $jobcat1 = explode(',', $jobcat);
             $this->db->where_in('job_posting.category', $jobcat1);
          }

          if(isset($filterdata['jobsubcategory']) && !empty($filterdata['jobsubcategory'])) {
             $this->db->where('job_posting.subcategory', $filterdata['jobsubcategory']);
          }


          if(isset($filterdata['locationSort']) && !empty($filterdata['locationSort']) && $filterdata['locationSort'] == 1) {
             $this->db->order_by('recruiter_details.address', "DESC");
          }
          if(isset($filterdata['language']) && !empty($filterdata['language'])) {
             $this->db->where('job_posting.language', $filterdata['language']);
          }
          if(isset($filterdata['salarySort']) && !empty($filterdata['salarySort']) && $filterdata['salarySort'] == 1) {
             $this->db->order_by('job_posting.salary', "DESC");
          }

          if(isset($filterdata['basicSalary']) && !empty($filterdata['basicSalary']) && isset($filterdata['toppicks']) && !empty($filterdata['toppicks'])) {
               $toppicks = $filterdata['toppicks'];
               $categories1 = explode(",", $toppicks);
               $this->db->where_in('job_toppicks.picks_id', $categories1);
            }else if(isset($filterdata['toppicks']) && !empty($filterdata['toppicks']) && empty($filterdata['basicSalary'])) {
               $toppicks = $filterdata['toppicks'];
               $categories1 = explode(",", $toppicks);
               $this->db->or_where_in('job_toppicks.picks_id', $categories1);
            }
         if(isset($filterdata['allowances']) && !empty($filterdata['allowances'])) {
            $allowances = $filterdata['allowances'];
            $categories2 = explode(",", $allowances);
            $this->db->or_where_in('job_allowances.allowances_id', $categories2);
         }
         if(isset($filterdata['medical']) && !empty($filterdata['medical'])) {
            $medical = $filterdata['medical'];
            $categories3 = explode(",", $medical);
            $this->db->or_where_in('job_medical.medical_id', $categories3);
         }
         if(isset($filterdata['workshift']) && !empty($filterdata['workshift'])) {
            $workshift = $filterdata['workshift'];
            $categories4 = explode(",", $workshift);
            $this->db->or_where_in('job_workshift.workshift_id', $categories4);
         }

         if(isset($filterdata['leaves']) && !empty($filterdata['leaves'])) {
            $leave = $filterdata['leaves'];
            $categories5 = explode(",", $leave);
            $this->db->or_where_in('job_leaves.leaves_id', $categories5);
         }
          $this->db->group_by("exp_with_salary.jobpost_id");
          $query = $this->db->get();
          return $query->result_array();
      }
   }

   
   public function jobfetchhomeall($Address) {

      $cDate = date('y-m-d');

      $this->db->select("job_posting.id, job_posting.company_id, max(exp_with_salary.basicsalary) as salary, recruiter_details.latitude, recruiter_details.longitude, 1 as totaljobs, recruiter_details.address");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      //$this->db->like('recruiter_details.address', $Address, 'both');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->order_by("exp_with_salary.basicsalary",'desc');
      //$this->db->group_by('recruiter_details.latitude');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobfetchhomeall1($Address,$id,$expfilter) {

      $cDate = date('y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++){
         $uniquerarray[] = $ikl;
      }
      $this->db->select("job_posting.id, job_posting.company_id, max(exp_with_salary.basicsalary) as salary, recruiter_details.latitude, recruiter_details.longitude, 1 as jobcount, recruiter_details.address");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.company_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where_in('exp_with_salary.grade_id',$uniquerarray);
      $this->db->like('recruiter_details.address', $Address, 'both');
      $this->db->group_by('exp_with_salary.grade_id',$uniquerarray);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetchFeatured_Job() {
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc, job_posting.salary, job_posting.company_id, recruiter.cname, jobposting_location.jobLocation")
               ->from('job_posting')
               ->join('saved_jobs', 'saved_jobs.jobpost_id = job_posting.id')
               ->join('jobposting_location', 'jobposting_location.posting_id = job_posting.id')
               ->join('recruiter', 'recruiter.id = job_posting.company_id');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetchRecent_Job() {
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc, job_posting.salary, job_posting.company_id, recruiter.cname, jobposting_location.jobLocation, recruiter_details.companyPic")
               ->from('job_posting')
               ->join('saved_jobs', 'saved_jobs.jobpost_id = job_posting.id')
               ->join('jobposting_location', 'jobposting_location.posting_id = job_posting.id')
               ->join('recruiter', 'recruiter.id = job_posting.company_id')
               ->join('recruiter_details', 'recruiter_details.recruiter_id = recruiter.id')
               ->order_by("job_posting.id","desc")
               ->limit(2);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobtitlecache_check($title, $id) {
      $this->db->where('user_id', $id);
      $this->db->where('title', $title);
      $query = $this->db->get('jobtitlecache');
      return $query->result_array();
   }

   public function fetch_highsalary($id,$exp) {
      $this->db->select('MAX(basicsalary) as salary');
      $this->db->where('jobpost_id', $id);
      $this->db->where('exp', $exp);
      $query = $this->db->get('exp_with_salary');
      if($query->num_rows>0){
         return $query->result_array();
      }else{
         $this->db->select('MAX(basicsalary) as salary');
         $this->db->where('jobpost_id', $id);
         $query = $this->db->get('exp_with_salary');
         return $query->result_array();
      }
      
   }

    public function fetch_highsalary123($id,$exp) {
      $this->db->select('basicsalary as salary');
      $this->db->where('jobpost_id', $id);
      $this->db->where('exp', $exp);
      $query = $this->db->get('exp_with_salary');
      if($query->num_rows>0){
         return $query->result_array();
      }else{
         $this->db->select('basicsalary as salary');
         $this->db->where('jobpost_id', $id);
         $query = $this->db->get('exp_with_salary');
         return $query->result_array();
      }
      
   }

   public function fetch_highsalary12($id, $sal,$exp) {
      $this->db->select('basicsalary as salary');
      $this->db->where('jobpost_id', $id);
      $this->db->where('exp', $exp);
      $this->db->where('basicsalary >=', $sal);
      $query = $this->db->get('exp_with_salary');
      if($query->num_rows>0){
         return $query->result_array();   
      }else{
          $this->db->select('basicsalary as salary');
         $this->db->where('jobpost_id', $id);
         $this->db->where('basicsalary >=', $sal);
         $query = $this->db->get('exp_with_salary');
          return $query->result_array();    
      }
     
   }

   public function jobtitlecache_insert($data) {
      if($this->db->insert("jobtitlecache", $data)) {
         return true;
      } else{
         return false;
      }
   }

   public function fetch_keys($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('save_filter_keys');
      return $query->result_array();
   }

   public function update_keys($data, $id) {
      $this->db->where('user_id', $id);
      $this->db->update('save_filter_keys', $data);
      return true;
   }

   public function insert_keys($data) {
      if($this->db->insert("save_filter_keys", $data)) {
         return true;
      } else{
         return false;
      }
   }
   public function jobtitlecache_fetch($id) {
      $this->db->select("title");
      $this->db->where('user_id', $id);
      $query = $this->db->get('jobtitlecache');
      return $query->result_array();
   }

   public function keywordcache_check($title, $id) {
      $this->db->where('user_id', $id);
      $this->db->where('keyword', $title);
      $query = $this->db->get('keywordcache');
      return $query->result_array();
   }
   public function keywordcache_insert($data) {
      if($this->db->insert("keywordcache", $data)) {
         return true;
      } else{
         return false;
      }
   }
   public function keywordcachee_fetch($id) {
      $this->db->select("keyword");
      $this->db->where('user_id', $id);
      $query = $this->db->get('keywordcache');
      return $query->result_array();
   }

    public function getCompDetails($id) {
      $this->db->select("dayfrom, dayto, from_time, to_time");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }
   
    public function job_toppicks_insert($data) {
      if($this->db->insert_batch("job_toppicks", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function job_skills_insert($data) {
      if($this->db->insert_batch("job_skills", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   public function job_allowances_insert($data) {
      if($this->db->insert_batch("job_allowances", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
    public function job_medical_insert($data) {
      if($this->db->insert_batch("job_medical", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   public function job_shifts_insert($data) {
      if($this->db->insert_batch("job_workshift", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   public function job_leaves_insert($data) {
      if($this->db->insert_batch("job_leaves", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function job_basicsalary_delete($id) {
      $this->db->where('jobpost_id', $id);
      $delete = $this->db->delete("exp_with_salary");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function notification_delete($id) {
      $this->db->where('id', $id);
      $delete = $this->db->delete("notifications");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   public function job_toppicks_delete($id) {
      $this->db->where('jobpost_id', $id);
      $delete = $this->db->delete("job_toppicks");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function job_skills_delete($id) {
      $this->db->where('jobpost_id', $id);
      $delete = $this->db->delete("job_skills");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   public function job_allowances_delete($id) {
      $this->db->where('jobpost_id', $id);
      $delete = $this->db->delete("job_allowances");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   public function job_medical_delete($id) {
      $this->db->where('jobpost_id', $id);
      $delete = $this->db->delete("job_medical");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   public function job_shifts_delete($id) {
      $this->db->where('jobpost_id', $id);
      $delete = $this->db->delete("job_workshift");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   
   public function job_leaves_delete($id) {
      $this->db->where('jobpost_id', $id);
      $delete = $this->db->delete("job_leaves");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   
   public function jobs_skills_single($id) {
      $this->db->select("skill_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_skills');
      return $query->result_array();
   }

   public function jobs_topicks($id) {
      $this->db->select("picks_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_toppicks');
      $result = $query->result_array();
      $arr= array();
      foreach ($result as $toppicks) {
         $arr[] = $toppicks['picks_id'];
         //$final_arr[]=$arr;
      }
      return $arr;
   }

   public function jobs_allowances($id) {
      $this->db->select("allowances_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_allowances');
      $result = $query->result_array();
      $arr= array();
      foreach ($result as $allowances) {
         $arr[] = $allowances['allowances_id'];
      }
      return $arr;
   }

   public function jobs_medical($id) {
      $this->db->select("medical_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_medical');
      $result = $query->result_array();
      $arr= array();
      foreach ($result as $medicals) {
         $arr[] = $medicals['medical_id'];
      }
      return $arr;
   }

   public function jobs_leaves($id) {
      $this->db->select("leaves_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_leaves');
      $result = $query->result_array();
      $arr= array();
      foreach ($result as $leaves) {
         $arr[] = $leaves['leaves_id'];
      }
      return $arr;
   }

   public function jobs_topicks_single($id) {
      $this->db->select("picks_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_toppicks');
      return $query->result_array();
   }

   public function jobs_allowances_single($id) {
      $this->db->select("allowances_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_allowances');
      return $query->result_array();
   }

   public function jobs_medical_single($id) {
      $this->db->select("medical_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_medical');
      return $query->result_array();
   }

   public function jobs_workshifts_single($id) {
      $this->db->select("workshift_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_workshift');
      return $query->result_array();
   }
   
   public function jobs_leaves_single($id) {
      $this->db->select("leaves_id");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_leaves');
      return $query->result_array();
   }
   
   public function job_image_insert($data) {
      if($this->db->insert("job_images", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   public function job_images($id) {
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_images');
      return $query->result_array();
   }
   
   public function deleteimg($id) {
      $this->db->where('id', $id);
      $this->db->delete('job_images');
      return true;
   }

   public function deleteJob($id){
      $this->db->where('id',$id);
      $this->db->delete('job_posting');
      return true;
   }

   public function deleteappliedJob($id){
      $this->db->where('jobpost_id',$id);
      $this->db->delete('applied_jobs');
      return true;
   }

    public function deletejob_images($id){
      $this->db->where('jobpost_id',$id);
      $this->db->delete('job_images');
      return true;
   }
   
   public function job_images_single($id) {
      $this->db->select("job_images.pic");
      $this->db->where('jobpost_id', $id);
      $query = $this->db->get('job_images');
      return $query->result_array();
   }
   
   public function job_image($id) {
      $this->db->select("job_images.pic");
      $this->db->where('jobpost_id', $id);
      $this->db->limit('1');
      $query = $this->db->get('job_images');
      return $query->result_array();
   }
   
   public function savejob_delete($id,$userid) {
      $this->db->where('jobpost_id', $id);
      $this->db->where('user_id', $userid);
      $this->db->delete('saved_jobs');
      return true;
   }
   
   public function job_saved($id,$userid) {
      $this->db->select("saved_jobs.id");
      $this->db->where('jobpost_id', $id);
      $this->db->where('user_id', $userid);
      $query = $this->db->get('saved_jobs');
      return $query->result_array();
   }
   
   /*public function hotjob_fetch_latlong($userLat, $userLong) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;
      $cDate = date('y-m-d');
      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc, job_posting.salary, job_posting.jobexpire, recruiter.cname, recruiter.id as compId");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("recruiter_details.latitude", $userLat);
      $this->db->or_where("recruiter_details.longitude", $userLat);
      $query = $this->db->get();
      return $query->result_array();
   }*/


   public function hotjob_fetch_latlong($id,$expfilter) {
      $cDate = date('y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++){
         $uniquerarray[] = $ikl;
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      //$this->db->order_by('job_posting.id','desc');
      $this->db->order_by('salary','desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function recentjob_fetch_latlong($id,$expfilter) {
      $cDate = date('y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++){
         $uniquerarray[] = $ikl;
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, recruiter.id as compId, job_posting.recruiter_id");
      $this->db->from('recruiter_details');
      $this->db->join('job_posting', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = recruiter_details.recruiter_id');
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      //$this->db->order_by('job_posting.id','desc');
      $this->db->order_by('salary','desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $this->db->limit(5);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function insert_story($data) {
      if($this->db->insert("success_stories", $data)) {
         return true;
      } else{
         return false;
      }
   }

   public function story_lists() {
      $this->db->select("success_stories.story, success_stories.id, success_stories.user_id,success_stories.recruiter_id,success_stories.createdon, user.name, user.profilePic,recruiter.cname");
      $this->db->from('success_stories');
      $this->db->join('user', 'user.id = success_stories.user_id');
      $this->db->join('recruiter', 'success_stories.recruiter_id = recruiter.id');
      $this->db->where("success_stories.status", 1);
      $this->db->order_by('success_stories.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function read_notification($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('notifications', $data);
      return true;
   }

   public function read_notification_status($data, $uid, $rid) {
      $this->db->where('user_id', $uid);
      $this->db->where('recruiter_id', $rid);
      $this->db->update('notifications', $data);
      return true;
   }

   public function jobboostStatus($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('job_posting', $data);
      return true;
   }

   public function insert_review($data) {
      if($this->db->insert("reviews", $data)) {
         return true;
      } else{
         return false;
      }
   }

   public function review_lists($recruiter_id) {
      $this->db->select("reviews.rating, reviews.recommend, reviews.feedback , reviews.created_at, user.name, user.profilePic");
      $this->db->from('reviews');
      $this->db->join('user', 'user.id = reviews.user_id');
      $this->db->where("reviews.recruiter_id", $recruiter_id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function review_listsbyuser($recruiter_id,$userid) {
      $this->db->select("*");
      $this->db->from('reviews');
      $this->db->where("recruiter_id", $recruiter_id);
      $this->db->where("user_id", $userid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function story_listsbyuser($recruiter_id,$userid) {
      $this->db->select("*");
      $this->db->from('success_stories');
      $this->db->where("recruiter_id", $recruiter_id);
      $this->db->where("user_id", $userid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function liked_stories($storyid,$userid){
      $this->db->select('*')
         ->from('story_likes')
         ->where("user_id", $userid)
         ->where("story_id", $storyid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function like_story($data) {
      if($this->db->insert("story_likes", $data)) {
         return true;
      } else{
         return false;
      }
   }

   public function unlike_story($uid, $story_id, $data) {
      $this->db->where("story_id", $story_id);
      $this->db->where("user_id", $uid);
      $this->db->update('story_likes', $data);
      return true;
   }

   public function save_date($uid, $job_id, $data) {
      $this->db->where("jobpost_id", $job_id);
      $this->db->where("user_id", $uid);
      $this->db->update('applied_jobs', $data);
      return true;
   }

   public function story_comments($storyid,$userid){
      $this->db->select('*')
         ->from('story_comments')
         ->where("user_id", $userid)
         ->where("story_id", $storyid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function add_storycomment($data) {
      if($this->db->insert("story_comments", $data)) {
         return true;
      } else{
         return false;
      }
   }

   public function add_reconciliation($data) {
      if($this->db->insert("reconciliation", $data)) {
         return true;
      } else{
         return false;
      }
   }

   public function update_comment($uid, $story_id, $data) {
      $this->db->where("story_id", $story_id);
      $this->db->where("user_id", $uid);
      $this->db->update('story_comments', $data);
      return true;
   }

   public function comment_lists($story_id) {
      $this->db->select("story_comments.comments,story_comments.id , story_comments.createdon, user.name, user.profilePic");
      $this->db->from('story_comments');
      $this->db->join('user', 'user.id = story_comments.user_id');
      $this->db->where("story_comments.story_id", $story_id);
       $this->db->order_by("story_comments.id",'asc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetch_singlestory($story_id) {
      $this->db->select("success_stories.story, success_stories.createdon, user.name, user.profilePic");
      $this->db->from('success_stories');
      $this->db->join('user', 'user.id = success_stories.user_id');
      $this->db->where("success_stories.id", $story_id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetchstory($storyid){
      $this->db->select('*')
         ->from('success_stories')
         ->where("id", $storyid);
      $query = $this->db->get();
      return $query->result_array();
   }


   public function liked_stories_count($story_id) {
      $this->db->select("*");
      $this->db->from('story_likes');
      $this->db->where("story_id", $story_id);
      $this->db->where("status", 1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetch_companyRating($id) {
      $this->db->select('ROUND(AVG(rating),1) as average');
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('reviews');
      return $query->result_array();
   }

   public function fetch_gcompanyRating($id) {
      $this->db->select('rating');
      $this->db->where('company_id', $id);
      $query = $this->db->get('comapny_rating');
      return $query->result_array();
   }

   public function storycomment_delete($id,$userid) {
      $this->db->where('id', $id);
      $this->db->where('user_id', $userid);
      $this->db->delete('story_comments');
      return true;
   }

   public function liked_stories_status($story_id,$userid ) {
      $this->db->select("status");
      $this->db->from('story_likes');
      $this->db->where("story_id", $story_id);
      $this->db->where("user_id", $userid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function category_lists() {
      $this->db->select("*");
      $this->db->from('job_category');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function power_lists() {
      $this->db->select("*");
      $this->db->from('super_power');
      $query = $this->db->get();
      return $query->result_array();
   }

    public function category_lists1() {
      $this->db->select("*");
      $this->db->from('job_category');
      $this->db->where('id!=',9);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function skill_lists() {
      $this->db->select("*");
      $this->db->from('skills');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function level_lists() {
      $this->db->select("*");
      $this->db->from('job_levels');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function levelbyid($id) {
      $this->db->select("*");
      $this->db->from('job_levels');
      $this->db->where('id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function categorybyid($id) {
      $this->db->select("*");
      $this->db->from('job_category');
	  $this->db->where('id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function subcategorybyid($id) {
      $this->db->select("*");
      $this->db->from('job_subcategory');
	  $this->db->where('id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }
	
   public function languagebyid($id) {
      $this->db->select("*");
      $this->db->from('languages');
	  $this->db->where('id',$id);
      $query = $this->db->get();
      return $query->result_array();
   }	
   public function subcategory_lists(){
      $this->db->select('*')
         ->from('job_subcategory');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function subcategory_listsbycatid($catid){
      $this->db->select('*')
         ->from('job_subcategory')
         ->where("category_id", $catid)
         ->group_by('subcategory');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function filtersubcategory_listsbycatid($catid) {
       $cids = explode(',', $catid);
      $this->db->select('*')
         ->from('job_subcategory');
         if($catid!=9){
           $this->db ->where_in("category_id", $cids);
         }
         
         $this->db->group_by('subcategory');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobfetchbylatlong($lat, $long,$uid,$expfilter) {
     //  $this->db->select("job_posting.id");
     //  $this->db->from('recruiter_details');
     //  $this->db->join('job_posting', 'job_posting.company_id = recruiter_details.recruiter_id');
     // // $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
     //  $this->db->where("recruiter_details.latitude", $lat);
     //  $this->db->where("recruiter_details.longitude", $long);
     //  $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $uid)");
     //  $this->db->where("job_posting.experience", $expfilter);
     //  $this->db->or_where("job_posting.experience", "All Tenure");
     //  $this->db->group_by("job_posting.id");
      $query = $this->db->query("SELECT job_posting.id FROM `job_posting` inner join recruiter_details on (recruiter_details.recruiter_id=job_posting.company_id) where recruiter_details.latitude='".$lat."' and recruiter_details.longitude='".$long."' and job_posting.experience IN ('".$expfilter."','All Tenure' ) and job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = '".$uid."')");
      return $query->result_array();
      /*if($query->num_rows>0){
         return $query->result_array();
      }else{
         $query = $this->db->query("SELECT job_posting.id FROM `job_posting` inner join recruiter_details on (recruiter_details.recruiter_id=job_posting.company_id) where recruiter_details.latitude='".$lat."' and recruiter_details.longitude='".$long."' and job_posting.experience IN ('All Tenure','".$expfilter."') and job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = '".$uid."')");
         return $query->result_array();
      }*/
      
   }

   public function jobappliedcheck($jobid, $id) {
      $this->db->select("id")
         ->from('applied_jobs')
         ->where("jobpost_id", $jobid)
         ->where("user_id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobfetchbymaxsalary($id,$exp) {
      $this->db->select("MAX(basicsalary) as basesalary")
         ->from('exp_with_salary');
         $this->db->where("jobpost_id", $id);
         $this->db->where("exp", $exp);
      $query = $this->db->get();
      if($query->num_rows>0){
         return $query->result_array();
      }else{
         $this->db->select("MAX(basicsalary) as basesalary")
         ->from('exp_with_salary');
         $this->db->where("jobpost_id", $id);
         $query = $this->db->get();
         return $query->result_array();   
      }
      
   }

   public function jobfetchbymaxsalary123($id,$exp) {
      $this->db->select("basicsalary as basesalary")
         ->from('exp_with_salary');
         $this->db->where("jobpost_id", $id);
         $this->db->where("exp", $exp);
      $query = $this->db->get();
      if($query->num_rows>0){
         return $query->result_array();
      }else{
         $this->db->select("basicsalary as basesalary")
         ->from('exp_with_salary');
         $this->db->where("jobpost_id", $id);
         $query = $this->db->get();
         return $query->result_array();   
      }
      
   }

   public function jobfetchbymaxsalary1($id, $sal) {
      $this->db->select("MAX(basicsalary) as basesalary")
         ->from('exp_with_salary')
         ->where("jobpost_id", $id)
         ->where("basicsalary >=", $sal);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobfetchbymaxsalary12($id, $sal, $exp) {
      $this->db->select("basicsalary as basesalary")
         ->from('exp_with_salary')
         ->where("jobpost_id", $id)
         ->where("basicsalary >=", $sal);
         $this->db->where("exp", $exp);
      $query = $this->db->get();
      if($query->num_rows>0){
         return $query->result_array();
      }else{
         $this->db->select("basicsalary as basesalary")
         ->from('exp_with_salary')
         ->where("jobpost_id", $id)
         ->where("basicsalary >=", $sal);
         $query = $this->db->get();
         return $query->result_array();   
      }
   }

   public function getjobid($id) {
      $this->db->select("jobpost_id")
         ->from('applied_jobs')
         ->where("id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch_glass($cid){
      $this->db->select('*')
         ->from('comapny_rating')
         ->where("company_id", $cid);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_fetch_lastrate($cid){
      $this->db->select('*')
         ->from('reviews')
         ->where("recruiter_id", $cid)
         ->limit(1);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function advertise_lists($userLat,$userLong) {
      $this->db->select("advertise_list.*,recruiter.cname, recruiter_details.companyPic, (6371 * acos (cos ( radians($userLat) )* cos( radians( advertise_list.latitude ) ) * cos( radians( advertise_list.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( advertise_list.latitude ) ))) AS distance");
      $this->db->from('advertise_list');
      $this->db->join('recruiter', 'advertise_list.company_id = recruiter.id');
      $this->db->join('recruiter_details', 'advertise_list.company_id = recruiter_details.recruiter_id');
      $this->db->where("advertise_list.status", 1);
      $this->db->where("date_format(advertise_list.start_date,'%Y-%m-%d')<=",'CURDATE()',FALSE);
      $this->db->where("date_format(advertise_list.end_date,'%Y-%m-%d')>=",'CURDATE()',FALSE);
      $this->db->having("distance<", '1000');
      $this->db->order_by('advertise_list.id','desc');
      $this->db->limit(5);
      $query = $this->db->get();
      return $query->result_array();
   }


}
?>
