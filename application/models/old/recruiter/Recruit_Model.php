<?php
ob_start();
class Recruit_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   public function advertise_insert($data) {
      if($this->db->insert("advertise_list", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function advertisement_list($id) {
      $this->db->select("recruiter.cname,advertise_list.*");
      $this->db->from("advertise_list");
      $this->db->join("recruiter","advertise_list.site_id = recruiter.id");
      $this->db->where('advertise_list.company_id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function recruiter_stepone_insert($data) {
      if($this->db->insert("recruiter", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function recruiter_steptwo_insert($data) {
      if($this->db->insert("recruiter_details", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   public function site_image_insert($data) {
      if($this->db->insert("site_images", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function countNotificationinsert($data) {
      if($this->db->insert("counnotification", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function Notificationmessageinsert($data) {
      if($this->db->insert("notificationmessage", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function blockRecruiter($data) {
      if($this->db->insert("view_applicant_block", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function notification_listings($id) {
      $this->db->select("*");
      $this->db->where('recruiter_id', $id);
      $this->db->order_by('id','desc');
      $query = $this->db->get('notificationmessage');
      return $query->result_array();
   }
    public function deleteRecruiter($id) {
      $this->db->where('recruiter_id', $id);
      $delete = $this->db->delete("view_applicant_block");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function deleteSubRecruiter($id) {
      $this->db->where('id', $id);
      //$this->db->where('label', 3);
      $delete = $this->db->delete("recruiter");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function update_notificationcount($data, $id) {
      $this->db->where('recruiter_id', $id);
      $this->db->update('counnotification', $data);
      return true;
   }
   public function update_notificationmessage($data, $id) {
      $this->db->where('recruiter_id', $id);
      $this->db->update('notificationmessage', $data);
      return true;
   }
  public function deletecountnotification($id) {
      $this->db->where('recruiter_id', $id);
      $delete = $this->db->delete("counnotification");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   public function checkCountmessage($recruiterId)
   {
      $this->db->select("counnotification.nitifycount,counnotification.updated_at");
      $this->db->from("counnotification");
      $this->db->join("notificationmessage","notificationmessage.recruiter_id = counnotification.recruiter_id");
      $this->db->where('counnotification.recruiter_id', $recruiterId['id']);
      $query = $this->db->get();
      return $query->result_array();
   }


   public function recruiter_toppicks_delete($id) {
      $this->db->where('recruiter_id', $id);
      $delete = $this->db->delete("recruiter_toppicks");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   public function recruiter_toppicks_insert($data) {
      if($this->db->insert_batch("recruiter_toppicks", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function recruiter_allowances_delete($id) {
      $this->db->where('recruiter_id', $id);
      $delete = $this->db->delete("recruiter_allowances");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   public function recruiter_allowances_insert($data) {
      if($this->db->insert_batch("recruiter_allowances", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function recruiter_medical_delete($id) {
      $this->db->where('recruiter_id', $id);
      $delete = $this->db->delete("recruiter_medical");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   public function recruiter_medical_insert($data) {
      if($this->db->insert_batch("recruiter_medical", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function recruiter_shifts_delete($id) {
      $this->db->where('recruiter_id', $id);
      $delete = $this->db->delete("recruiter_workshift");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   
   public function recruiter_leaves_delete($id) {
      $this->db->where('recruiter_id', $id);
      $delete = $this->db->delete("recruiter_leaves");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
   public function recruiter_shifts_insert($data) {
      if($this->db->insert_batch("recruiter_workshift", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   
   public function recruiter_leaves_insert($data) {
      if($this->db->insert_batch("recruiter_leaves", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function company_topicks_single($id) {
      $this->db->select("picks_id");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_toppicks');
      return $query->result_array();
   }

   public function company_allowances_single($id) {
      $this->db->select("allowances_id");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_allowances');
      return $query->result_array();
   }

   public function company_medical_single($id) {
      $this->db->select("medical_id");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_medical');
      return $query->result_array();
   }

   public function company_workshifts_single($id) {
      $this->db->select("workshift_id");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_workshift');
      return $query->result_array();
   }
   
   public function company_leaves_single($id) {
      $this->db->select("leaves_id");
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_leaves');
      return $query->result_array();
   }

   public function recruiter_login($data) {
      $this->db->where($data);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }
   public function recruiter_single($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function recruiter_permission($id) {
      $this->db->where('subrecruiter_id', $id);
      $query = $this->db->get('subrecruiter_permission');
      return $query->result_array();
   }

   public function password_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('recruiter', $data);
      return true;
   }
   // Company
   public function company_single($id) {
      $this->db->where('id', $id);
      //$this->db->where('label !=', 3);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function faqlisting() {
      $query = $this->db->get('faq');
      return $query->result_array();
   }
   public function company_details_single($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }
   
   public function company_details_single1($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }

   public function companysite_details_single($id) {
      $this->db->select("recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id,recruiter_details.recruiter_id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('parent_id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function companysite_details_single1($id, $recruiter_id) {
      $this->db->select("recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id,recruiter_details.recruiter_id, recruiter_details.dayfrom, recruiter_details.dayto, recruiter_details.from_time, recruiter_details.to_time");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('parent_id', $id);
      $this->db->where('recruiter_id', $recruiter_id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function company_sites($id) {
      $this->db->select("recruiter.cname,recruiter.id,  recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('parent_id', $id);
      $this->db->where('label !=', 3);
      $this->db->order_by('recruiter.cname');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function subrecruiters($id) {
      $this->db->select("recruiter.fname,recruiter.password, recruiter.lname, recruiter.email, recruiter_details.phone,  recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id, subrecruiter_permission.site_details, subrecruiter_permission.invoice_view, subrecruiter_permission.edit_delete");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->join("subrecruiter_permission","subrecruiter_permission.subrecruiter_id = recruiter.id", 'left');
      $this->db->where('parent_id', $id);
      $this->db->where('label =', 3);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function subrecruitersfetch($id) {
      $this->db->select("recruiter.fname,recruiter.password, recruiter.lname, recruiter.email,recruiter_details.phonecode, recruiter_details.phone,  recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id, subrecruiter_permission.site_details, subrecruiter_permission.invoice_view, subrecruiter_permission.edit_delete");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->join("subrecruiter_permission","subrecruiter_permission.subrecruiter_id = recruiter.id", 'left');
      $this->db->where('recruiter.id', $id);
      $this->db->where('label =', 3);
      $query = $this->db->get();
      return $query->result_array();
   }
   
   public function company_sites1($id, $recruiter_id) {
      $this->db->select("recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('parent_id', $id);
      $this->db->where('recruiter_id', $recruiter_id);
      $query = $this->db->get();
      return $query->result_array();
   }
   public function company_site_insert($data) {
      if($this->db->insert("company_sites", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function recruiter_stepone_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('recruiter', $data);
      return true;
   }

    public function recruiter_steptwo_update($data, $id) {
      $this->db->where('recruiter_id', $id);
      $this->db->update('recruiter_details', $data);
      return true;
   }

   public function forgotPass_check($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_forgot');
      return $query->result_array();
   }
   
   public function forgotPass_insert($data) {
      if($this->db->insert("user_forgot", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function forgotPass_update($data, $id) {
      $this->db->where('user_id', $id);
      $this->db->update('user_forgot', $data);
      return true;
   }

   public function verification_check($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('verification_email');
      return $query->result_array();
   }
   
   public function verification_insert($data) {
      if($this->db->insert("verification_email", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function permission_delete($id) {
      $this->db->where('subrecruiter_id', $id);
      $delete = $this->db->delete("subrecruiter_permission");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function permission_insert($data) {
      if($this->db->insert("subrecruiter_permission", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function verification_update($data, $id) {
      $this->db->where('user_id', $id);
      $this->db->update('verification_email', $data);
      return true;
   }

   public function email_match($data) {
      $this->db->where($data);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }

   public function recruiter_exp_insert($data) {
      if($this->db->insert("recruiter_addexp", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function recruiter_exp_fetch($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_addexp');
      return $query->result_array();
   }
   public function recruiter_exp_singlefetch($idd, $id) {
      $this->db->where('recruiter_id', $id);
      $this->db->where('id', $idd);
      $query = $this->db->get('recruiter_addexp');
      return $query->result_array();
   }
   public function recruiter_exp_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('recruiter_addexp', $data);
      return true;
   }
   public function recruiter_exp_delete($id) {
      $this->db->where('id', $id);
      $this->db->delete('recruiter_addexp');
      return true;
   }

   public function user_clientSupported() {
      $this->db->distinct();
      $this->db->select("clients");
      $query = $this->db->get('user_topclients');
      return $query->result_array();
   }
   
   public function company_images($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('site_images');
      return $query->result_array();
   }
   
   public function deleteimg($id) {
      $this->db->where('recruiter_id', $id);
      $this->db->delete('site_images');
      return true;
   }

   public function deletesiteimg($id) {
      $this->db->where('id', $id);
      $this->db->delete('site_images');
      return true;
   }

   public function get_invoices_data($id) {
      $this->db->where('company_id', $id);
      $this->db->order_by('id','desc');
      $query = $this->db->get('invoice');
      return $query->result_array();
   }

   public function hired_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as hiredCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.status", 7)
         ->where("job_posting.company_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("(job_posting.status= 2 OR job_posting.jobexpire<'$currentDate')");
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function rejected_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("(applied_jobs.status=3 OR applied_jobs.status=4)")
         ->where("job_posting.company_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             $this->db ->where("job_posting.jobexpire <", $currentDate);
             //$this->db->where("job_posting.status", 2);
             //$this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function noshow_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.fallout_status", 1)
         ->where("job_posting.recruiter_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
            $this->db ->where("job_posting.jobexpire <", $currentDate);
             /*$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);*/
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function meetreq_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.fallout_status", 2)
         ->where("job_posting.recruiter_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             $this->db ->where("job_posting.jobexpire <", $currentDate);
             //$this->db->where("job_posting.status", 2);
             //$this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }
   public function noaccept_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.fallout_status", 3)
         ->where("job_posting.recruiter_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }
   public function refer_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.status", 4)
         ->where("job_posting.recruiter_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }
   public function another_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.fallout_status", 5)
         ->where("job_posting.recruiter_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id", $subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function acceptedApplication_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("(applied_jobs.status=6)")
         ->where("job_posting.company_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function newApplication_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.status", 1)
         ->where("job_posting.company_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function hiredApplication_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.status", 7)
         ->where("job_posting.company_id", $id);
         if($filters=='Active'){
           $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function allApplication_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("job_posting.company_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function allApplication_counted($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("job_posting.recruiter_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function allApplication_count1($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("job_posting.recruiter_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
            // $this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function hiredcounttotal($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rejectCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("job_posting.recruiter_id", $id)
         ->where("applied_jobs.status", 7);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
            // $this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function target_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('SUM(opening) as targetSum')
         ->from('job_posting')
         ->where("company_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function open_count($id,$subid,$filters) {
      $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.status", 5)
         ->where("job_posting.company_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function target_count1($id,$subid,$filters) {
       $currentDate = date("Y-m-d");
      $this->db->select('SUM(opening) as targetSum')
         ->from('job_posting')
         ->where("recruiter_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
            // $this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobposted($id,$subid,$filters) {
       $currentDate = date("Y-m-d");
      $this->db->select('count(job_posting.id) as targetSum')
         ->from('job_posting')
         ->where("company_id", $id);
         if($filters=='All'){
            
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
            // $this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }else{
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
         }
         if($subid!=0){
            $this->db->where("subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function open_count1($id,$subid,$filters) {
       $currentDate = date("Y-m-d");
      $this->db->select('COUNT(job_posting.id) as rCount')
         ->from('job_posting')
         ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
         ->where("applied_jobs.status", 5)
         ->where("applied_jobs.fallout_reason", "Open")
         ->where("job_posting.recruiter_id", $id);
         if($filters=='Active'){
            $this->db ->where("job_posting.jobexpire >=", $currentDate);
            //$this->db->where("job_posting.status", 1);
         }else if($filters=='Expired'){
             //$this->db->where("job_posting.status", 2);
             $this->db ->where("job_posting.jobexpire <", $currentDate);
         }
         if($subid!=0){
            $this->db->where("job_posting.subrecruiter_id",$subid);
         }
      $query = $this->db->get();
      return $query->result_array();
   }

   public function pendingdays_count($id) {
      $this->db->select('COUNT(id) as tSum')
         ->from('notificationmessage')
         ->where("recruiter_id", $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function countNotification($datecondition)
   {
    $this->db->select('*');
    $this->db->from('job_posting');
    $this->db->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id');
    $this->db->where("applied_jobs.status", '1');
    $this->db->where($datecondition);
    $query = $this->db->get();
    return $query->result_array();
   } 

public function countNotification1($datecondition, $rid,$subid)
   {
    $this->db->select('*');
    $this->db->from('job_posting');
    $this->db->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id');
    $this->db->where("applied_jobs.status", '1');
    $this->db->where("job_posting.recruiter_id", $rid);
    $this->db->where($datecondition);
    if($subid!=0){
      $this->db->where("job_posting.subrecruiter_id",$subid);
    }
    $query = $this->db->get();
    return $query->result_array();
   } 

   public function site_delete($id) {
      $this->db->where('id', $id);
      $delete = $this->db->delete("recruiter");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function sitedetails_delete($id) {
      $this->db->where('recruiter_id', $id);
      $delete = $this->db->delete("recruiter_details");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function job_delete($rid){
      $this->db->where('company_id', $rid);
      $delete = $this->db->delete("job_posting");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function ad_delete($rid){
      $this->db->where('site_id', $rid);
      $delete = $this->db->delete("advertise_list");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function notification_delete($rid){
      $this->db->where('recruiter_id', $rid);
      $delete = $this->db->delete("notifications");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function review_delete($rid){
      $this->db->where('recruiter_id', $rid);
      $delete = $this->db->delete("reviews");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function special_profile_delete($rid){
      $this->db->where('company_id', $rid);
      $delete = $this->db->delete("special_profile");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }

   public function success_stories_delete($rid){
      $this->db->where('recruiter_id', $rid);
      $delete = $this->db->delete("success_stories");
      if($delete) {
         return true;
      } else{
         return false;
      }
   }
}
?>
