<?php
ob_start();
date_default_timezone_set('Asia/Manila');
class Cron_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
      
   } 


   public function jobappliedupcoming_fetch() {
      $datetime = new DateTime('tomorrow');
      $currentDate = $datetime->format('Y-m-d');
      $currentHours = date("H");
      $currentMinute = date("i");

      $this->db->select("job_posting.id,recruiter.cname,recruiter.fname, recruiter.lname, user.phone, user.country_code, applied_jobs.user_id, job_posting.jobtitle, user.notification_status, job_posting.jobDesc,job_posting.jobPitch, job_posting.salary, job_posting.company_id,job_posting.recruiter_id, applied_jobs.status, applied_jobs.interviewdate, applied_jobs.interviewtime")
               ->from('job_posting')
               ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
               ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
               ->join('user', 'applied_jobs.user_id = user.id')
               ->where('date(applied_jobs.interviewdate)', $currentDate)
               ->where('hour(applied_jobs.interviewtime)', $currentHours)
               ->where('minute(applied_jobs.interviewtime)', $currentMinute)
               ->where('applied_jobs.status', 1)
               ->order_by('applied_jobs.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobappliedupcoming_fetch1() {
      $currentDate = date("y-m-d");
      $timestamp = time()+60*60*2;
      $currentHours = date('H', $timestamp);
      //$currentMinute = date('i', $timestamp);
      $this->db->select("count(applied_jobs.user_id) as candCount,applied_jobs.jobpost_id, recruiter.cname,recruiter.email, recruiter.fname,recruiter.lname, recruiter_details.phonecode, recruiter_details.phone, applied_jobs.user_id, job_posting.jobtitle, user.notification_status, job_posting.jobDesc,job_posting.jobPitch, job_posting.subrecruiter_id, job_posting.company_id,job_posting.recruiter_id, applied_jobs.status, applied_jobs.interviewdate, applied_jobs.interviewtime")
               ->from('job_posting')
               ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
               ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
               ->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id')
               ->join('user', 'applied_jobs.user_id = user.id')
               ->where('applied_jobs.interviewdate =', $currentDate)
               ->where('hour(recruiter_details.from_time) ', $currentHours)
               //->where('minute(recruiter_details.from_time) ', $currentMinute)
               ->where('applied_jobs.status', 1)
               ->group_by('job_posting.company_id')
               ->order_by('applied_jobs.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobappliedupcomingsub_fetch1() {
      $currentDate = date("y-m-d");
      $timestamp = time()+60*60*2;
      $currentHours = date('H', $timestamp);
      //$currentMinute = date('i', $timestamp);
      $this->db->select("count(applied_jobs.user_id) as candCount,applied_jobs.jobpost_id, recruiter.cname,recruiter.email, recruiter.fname,recruiter.lname, recruiter_details.phonecode, recruiter_details.phone, applied_jobs.user_id, job_posting.jobtitle, user.notification_status, job_posting.jobDesc,job_posting.jobPitch, job_posting.subrecruiter_id, job_posting.company_id,job_posting.recruiter_id, applied_jobs.status, applied_jobs.interviewdate, applied_jobs.interviewtime")
               ->from('job_posting')
               ->join('applied_jobs', 'applied_jobs.jobpost_id = job_posting.id')
               ->join('recruiter', 'recruiter.id = job_posting.subrecruiter_id')
               ->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id')
               ->join('user', 'applied_jobs.user_id = user.id')
               ->where('applied_jobs.interviewdate =', $currentDate)
               ->where('hour(recruiter_details.from_time) ', $currentHours)
               //->where('minute(recruiter_details.from_time) ', $currentMinute)
               ->where('applied_jobs.status', 1)
               ->group_by('job_posting.company_id')
               ->order_by('applied_jobs.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function candidateduenotification_list() {
      $Date = date('Y-m-d');
      $currentDate = date('Y-m-d', strtotime($Date. ' - 5 days'));
      $this->db->select('applied_jobs.id, count(applied_jobs.user_id) as candCount,applied_jobs.jobpost_id, applied_jobs.interviewtime, applied_jobs.interviewdate,applied_jobs.accepted_date, applied_jobs.updated_at,applied_jobs.date_day1, applied_jobs.user_id, user.name, user.location, user.phone, user.country_code, user.profilePic, job_posting.jobDesc, job_posting.recruiter_id,recruiter.fname, recruiter.lname, job_posting.jobtitle, applied_jobs.status, recruiter.cname,recruiter.email, job_posting.company_id as compid, applied_jobs.fallout_reason')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.recruiter_id = recruiter.id')
         ->where("(applied_jobs.status=1 OR applied_jobs.status=5 OR applied_jobs.status=6)")
         ->where("recruiter.parent_id=", '0')
         ->where("DATE(applied_jobs.updated_at)<=", $currentDate)
         ->group_by('recruiter.id');
         $this->db->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function candidatesubnotification_list() {
      $Date = date('Y-m-d');
      $currentDate = date('Y-m-d', strtotime($Date. ' - 5 days'));
      $this->db->select('applied_jobs.id, count(applied_jobs.user_id) as candCount,applied_jobs.jobpost_id, applied_jobs.interviewtime, applied_jobs.interviewdate,applied_jobs.accepted_date, applied_jobs.updated_at,applied_jobs.date_day1, applied_jobs.user_id, user.name, user.location, user.phone, user.country_code, user.profilePic, job_posting.jobDesc, job_posting.recruiter_id,recruiter.fname, recruiter.lname, job_posting.jobtitle, applied_jobs.status, recruiter.cname,recruiter.email, job_posting.company_id as compid, applied_jobs.fallout_reason')
         ->from('applied_jobs')
         ->join('job_posting', 'job_posting.id = applied_jobs.jobpost_id')
         ->join('user', 'user.id = applied_jobs.user_id')
         ->join('recruiter', 'job_posting.subrecruiter_id = recruiter.id')
         ->where("(applied_jobs.status=1 OR applied_jobs.status=5 OR applied_jobs.status=6)")
         ->where("recruiter.parent_id!=", '0')
         ->where("DATE(applied_jobs.updated_at)<=", $currentDate)
         ->group_by('recruiter.id');
         $this->db->order_by("applied_jobs.id","DESC");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function getPhone($rid)
   {
      $this->db->select('phonecode,phone')
         ->from('recruiter_details')
         ->where('recruiter_id',$rid);
      $query = $this->db->get();
      return $query->result_array();   
   }

   public function test_sms($data)
   {
      $this->db->insert('test_sms',$data);
   }
}
?>
