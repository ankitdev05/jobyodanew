<?php
ob_start();
class Jobpostrecruiter_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   }

   public function job_fetchAll() {
      $this->db->select('job_posting.id, job_posting.company_id, job_posting.jobtitle, job_posting.experience, recruiter_details.address, recruiter.cname')
         ->from('job_posting')
         ->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id')
         ->join('recruiter', 'recruiter.id = job_posting.company_id')
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function boostjob_fetch() {
      $this->db->select('job_posting.id, job_posting.jobtitle, job_posting.company_id, job_posting.jobtitle, job_posting.experience, recruiter_details.address, recruiter.cname, recruiter_details.recruiter_email')
         ->from('job_posting')
         ->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id')
         ->join('recruiter', 'recruiter.id = job_posting.company_id')
         ->where('boost_status','1')
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }
 public function getDatabyStatus($status,$date,$session_id) {
      $this->db->select('applied_jobs.status,applied_jobs.interviewdate,applied_jobs.interviewtime, job_posting.id, applied_jobs.updated_at, applied_jobs.fallout_reason, job_posting.joining_bonus, job_posting.jobexpire, job_posting.level, job_posting.education, job_posting.allowance, job_posting.experience, job_posting.opening, job_posting.category, job_posting.subcategory, job_posting.language, job_posting.other_language,   job_posting.jobtitle, job_posting.created_at, job_posting.company_id,job_posting.subrecruiter_id, recruiter.cname,recruiter.fname, recruiter.lname, user.name, user.phone ,user.country_code, user.id as uid, user.exp_month, user.exp_year')
         ->from('applied_jobs')
         ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->join('user', 'user.id = applied_jobs.user_id');
         if($status=='10'){

         }else{
            $this->db->where('applied_jobs.status',$status);
         }
         
        $this->db->where('recruiter.id',$session_id);
         /*->group_by('job_posting.id')*/
         $this->db->order_by('applied_jobs.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function getsubDatabyStatus($status,$date,$session_id) {
      $this->db->select('applied_jobs.status,applied_jobs.interviewdate,applied_jobs.interviewtime, job_posting.id, applied_jobs.updated_at, applied_jobs.fallout_reason, job_posting.joining_bonus, job_posting.jobexpire, job_posting.level, job_posting.education, job_posting.allowance, job_posting.experience, job_posting.opening, job_posting.category, job_posting.subcategory, job_posting.language, job_posting.other_language,   job_posting.jobtitle, job_posting.created_at, job_posting.company_id,job_posting.recruiter_id, recruiter.cname,recruiter.fname, recruiter.lname, user.name, user.phone ,user.country_code, user.id as uid, user.exp_month, user.exp_year')
         ->from('applied_jobs')
         ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')
         ->join('recruiter', 'recruiter.id = job_posting.subrecruiter_id')
         ->join('user', 'user.id = applied_jobs.user_id');
         if($status=='10'){

         }else{
            $this->db->where('applied_jobs.status',$status);
         }
         
        $this->db->where('recruiter.id',$session_id);
         /*->group_by('job_posting.id')*/
         $this->db->order_by('applied_jobs.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function getDatabyStatus1($date,$session_id) {
      $this->db->select('applied_jobs.status,applied_jobs.interviewdate,applied_jobs.interviewtime, job_posting.id, applied_jobs.updated_at, applied_jobs.fallout_reason, job_posting.joining_bonus, job_posting.jobexpire, job_posting.level, job_posting.education, job_posting.allowance, job_posting.experience, job_posting.opening, job_posting.category, job_posting.subcategory, job_posting.language, job_posting.other_language,   job_posting.jobtitle, job_posting.created_at, job_posting.company_id,job_posting.recruiter_id,job_posting.subrecruiter_id, recruiter.cname,recruiter.fname, recruiter.lname, user.name, user.id as uid, user.exp_month, user.exp_year')
         ->from('applied_jobs')
         ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         ->join('user', 'user.id = applied_jobs.user_id')
        /* ->where('applied_jobs.status',$status)*/
         ->where('recruiter.id',$session_id)
         ->group_by('job_posting.id')
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function getsubDatabyStatus1($date,$session_id) {
      $this->db->select('applied_jobs.status,applied_jobs.interviewdate,applied_jobs.interviewtime, job_posting.id, applied_jobs.updated_at, applied_jobs.fallout_reason, job_posting.joining_bonus, job_posting.jobexpire, job_posting.level, job_posting.education, job_posting.allowance, job_posting.experience, job_posting.opening, job_posting.category, job_posting.subcategory, job_posting.language, job_posting.other_language,   job_posting.jobtitle, job_posting.created_at, job_posting.company_id,job_posting.recruiter_id, recruiter.cname,recruiter.fname, recruiter.lname, user.name, user.id as uid, user.exp_month, user.exp_year')
         ->from('applied_jobs')
         ->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')
         ->join('recruiter', 'recruiter.id = job_posting.subrecruiter_id')
         ->join('user', 'user.id = applied_jobs.user_id')
        /* ->where('applied_jobs.status',$status)*/
         ->where('recruiter.id',$session_id)
         ->group_by('job_posting.id')
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function getDatabyStatus2($date,$session_id) {
      $this->db->select('job_posting.id, job_posting.joining_bonus, job_posting.jobexpire, job_posting.level, job_posting.education, job_posting.allowance, job_posting.experience, job_posting.opening, job_posting.category, job_posting.subcategory, job_posting.language, job_posting.other_language,   job_posting.jobtitle, job_posting.created_at, job_posting.company_id,job_posting.subrecruiter_id, recruiter.cname, recruiter.fname, recruiter.lname')
         ->from('job_posting')
         /*->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')*/
         ->join('recruiter', 'recruiter.id = job_posting.recruiter_id')
         // ->join('user', 'user.id = applied_jobs.user_id')
        /* ->where('applied_jobs.status',$status)*/
         ->where('recruiter.id',$session_id)
         /*->group_by('job_posting.id')*/
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function getsubDatabyStatus2($date,$session_id) {
      $this->db->select('job_posting.id, job_posting.joining_bonus, job_posting.jobexpire, job_posting.level, job_posting.education, job_posting.allowance, job_posting.experience, job_posting.opening, job_posting.category, job_posting.subcategory, job_posting.language, job_posting.other_language,   job_posting.jobtitle, job_posting.created_at,job_posting.recruiter_id, job_posting.company_id, recruiter.cname, recruiter.fname, recruiter.lname')
         ->from('job_posting')
         /*->join('job_posting', 'applied_jobs.jobpost_id = job_posting.id')*/
         ->join('recruiter', 'recruiter.id = job_posting.subrecruiter_id')
         // ->join('user', 'user.id = applied_jobs.user_id')
        /* ->where('applied_jobs.status',$status)*/
         ->where('recruiter.id',$session_id)
         /*->group_by('job_posting.id')*/
         ->order_by('job_posting.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

 
}
?>