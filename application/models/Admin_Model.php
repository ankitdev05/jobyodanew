<?php
ob_start();
class Admin_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   }

   public function admin_login($data) {
      $this->db->where($data);
      $query = $this->db->get('admin');
      return $query->result_array();
   }
   public function admin_single($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('admin');
      return $query->result_array();
   }
   public function admin_signup($data) {
      if($this->db->insert("admin", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function admin_all() {
      $query = $this->db->get('admin');
      return $query->result_array();
   }

   public function password_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('admin', $data);
      return true;
   }
   // Company
   public function company_single($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('recruiter');
      return $query->result_array();
   }
   public function company_details_single($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_details');
      return $query->result_array();
   }

   public function companysite_details_single($id) {
      $this->db->select("recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('parent_id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function company_sites($id) {
      $this->db->select("recruiter_details.companyPic, recruiter_details.companyDesc, recruiter_details.address, recruiter.cname, recruiter.id");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('parent_id', $id);
      $query = $this->db->get();
      return $query->result_array();
   }
   public function company_site_insert($data) {
      if($this->db->insert("company_sites", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function recruiter_stepone_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('recruiter', $data);
      return true;
   }

    public function recruiter_steptwo_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('recruiter_details', $data);
      return true;
   }

   public function forgotPass_check($id) {
      $this->db->where('admin_id', $id);
      $query = $this->db->get('admin_forgot');
      return $query->result_array();
   }
   
   public function forgotPass_insert($data) {
      if($this->db->insert("admin_forgot", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function forgotPass_update($data, $id) {
      $this->db->where('admin_id', $id);
      $this->db->update('admin_forgot', $data);
      return true;
   }

   public function verification_check($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('verification_email');
      return $query->result_array();
   }
   
   public function verification_insert($data) {
      if($this->db->insert("verification_email", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function verification_update($data, $id) {
      $this->db->where('user_id', $id);
      $this->db->update('verification_email', $data);
      return true;
   }

   public function email_match($data) {
      $this->db->where($data);
      $query = $this->db->get('admin');
      return $query->result_array();
   }

   public function recruiter_exp_insert($data) {
      if($this->db->insert("recruiter_addexp", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }
   public function recruiter_exp_fetch($id) {
      $this->db->where('recruiter_id', $id);
      $query = $this->db->get('recruiter_addexp');
      return $query->result_array();
   }
   public function recruiter_exp_singlefetch($idd, $id) {
      $this->db->where('recruiter_id', $id);
      $this->db->where('id', $idd);
      $query = $this->db->get('recruiter_addexp');
      return $query->result_array();
   }
   public function recruiter_exp_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('recruiter_addexp', $data);
      return true;
   }
   public function recruiter_exp_delete($id) {
      $this->db->where('id', $id);
      $this->db->delete('recruiter_addexp');
      return true;
   }

   public function user_clientSupported() {
      $this->db->distinct();
      $this->db->select("clients");
      $query = $this->db->get('user_topclients');
      return $query->result_array();
   }
   
   public function admin_detail_fetch($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('admin');
      return $query->result_array();
   }
   public function admin_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('admin', $data);
      return true;
   }
}
?>