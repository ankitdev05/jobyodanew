<?php
ob_start();
class Jobseekeradmin_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   public function jobseeker_lists() {
      //$this->db->where('parent_id', 0);
      $this->db->select('a.name,a.phone, a.app_version, a.last_used, a.platform, a.active, a.email, a.education, a.location, a.nationality, a.superpower, a.exp_month, a.exp_year, a.id, a.created_at, a.type,a.state,a.city,a.jobsInterested, a.device_type, COUNT(g.id) as countid');
        $this->db->from('user a');
        $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');
        $this->db->where('a.phone!=','');
        $this->db->order_by('a.id','desc');
        $this->db->group_by('a.id');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function jobseeker_bylocationlists($jobsubcat, $joblocation) {
      $this->db->select('id, name, phone, app_version, last_used, platform, active, email, education, location, nationality, superpower, exp_month, exp_year, created_at, type, device_type, jobsInterested, latitude, longitude');
        $this->db->from('user');
        $this->db->where('phone!=','');
        $this->db->like('jobsInterested', $jobsubcat);
        //$this->db->like('location', $joblocation);
        $this->db->where('jobsInterested is NOT NULL', NULL, FALSE);
        $this->db->order_by('id','desc');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function jobseekerpower_bylocationlists($power, $joblocation) {
      $this->db->select('id, name, phone, app_version, last_used, platform, active, email, education, location, nationality, superpower, exp_month, exp_year, created_at, type, device_type, jobsInterested, latitude, longitude');
        $this->db->from('user');
        $this->db->where('phone!=','');
        $this->db->like('superpower', $power);
        //$this->db->like('location', $joblocation);
        $this->db->where('superpower is NOT NULL', NULL, FALSE);
        $this->db->order_by('id','desc');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function jobseeker_byIntrestedInId($userIdArr) {
      $this->db->select('id, name, phone, app_version, last_used, platform, active, email, education, location, nationality, superpower, exp_month, exp_year, created_at, type, device_type, jobsInterested, latitude, longitude');
        $this->db->from('user');
        $this->db->where('phone!=','');
        $this->db->where_in('id', $userIdArr);
        //$this->db->like('location', $joblocation);
        $this->db->order_by('id','desc');
        $query = $this->db->get(); 
      return $query->result_array();
   }

   public function fetch_bonus($user_id){
      $this->db->select('sign_in_bonus.bonus,sign_in_bonus.added_at,user.name');
      $this->db->from('sign_in_bonus');
      $this->db->join('user','sign_in_bonus.user_id=user.id');
      $this->db->where('sign_in_bonus.user_id',$user_id);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function notification_lists(){
      $this->db->select('promo_notifications.title,promo_notifications.message, promo_notifications.added_at,user.name,user.superpower');
      $this->db->from('promo_notifications');
      $this->db->join('user','promo_notifications.user_id=user.id');
      $this->db->order_by('promo_notifications.id','desc');
      $this->db->limit(5000);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobrecordnotification_lists(){
      $this->db->select('id,title,notification,intrestedIn,superpower,created_at');
      $this->db->order_by('id','desc');
      $this->db->limit(5000);
      $query = $this->db->get('jobs_notify_records');
      return $query->result_array();
   }

   public function jobnotificationcount_lists($id){
      $this->db->select('count(id) as countid');
      $this->db->from('jobs_notify');
      $this->db->where("jobs_notify.notify_id",$id);
      $this->db->order_by('jobs_notify.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function jobnotification_lists($id){
      $this->db->select('job_posting.id,job_posting.jobtitle, jobs_notify.title,jobs_notify.notification,user.name,user.email');
      $this->db->from('jobs_notify');
      $this->db->where("jobs_notify.notify_id",$id);
      $this->db->join('user','jobs_notify.user_id=user.id');
      $this->db->join('job_posting','jobs_notify.job_id=job_posting.id');
      $this->db->order_by('jobs_notify.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

    public function promonotification_lists(){
      $this->db->select('promo_notifications.title,promo_notifications.id, promo_notifications.message,promo_notifications.user_id, promo_notifications.added_at, user.name, user.superpower,user.notification_status, user.device_type, user.device_token');
      $this->db->from('promo_notifications');
      $this->db->join('user','promo_notifications.user_id=user.id');
      $this->db->where('promo_notifications.cron_status',1);
      $this->db->order_by('user.id','asc');
      $query = $this->db->get();
      return $query->result_array();
   }  

   public function promo_count(){
    $this->db->select('count(id) as promo_count')
             ->from('promo_notifications')
             ->where('cron_status',1);
    $query = $this->db->get();
    return $query->result_array();          
   }

   public function fetchPromo() {
        $this->db->select('title,id,message,user_id,added_at');
        $this->db->where('cron_status', 1);
        $this->db->order_by('id','DESC');
        $this->db->limit(2000);
        $query = $this->db->get('promo_notifications');
        return $query->result_array();
   }

   public function fetchUser($id) {
      $this->db->where('user_id',$id);
      $query = $this->db->get('user_token');
      return $query->result_array();
   }

   public function fetchUserAgain($id) {
      $this->db->where('id',$id);
      $query = $this->db->get('user');
      return $query->result_array();
   }

   public function user_single($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('user');
      return $query->result_array();
   }
   
   public function batchUpdate($user){
      return  $this->db->where("id",$user)
            ->update('promo_notifications',["cron_status"=>2]);
      // return $this->db->update('promo_notifications', 'cron_status=2', $user_array);     
   }

   public function jobpushUpdate($user) {
      return  $this->db->where("id",$user)
            ->update('jobs_notify',["status"=>2]);    
   }

   public function reconciliation_lists() {
      $this->db->select('reconciliation.status,reconciliation.added_at,reconciliation.showupreason,reconciliation.schedule,recruiter.cname, user.email,user.name,user.phone,job_posting.jobtitle');
      $this->db->from('reconciliation');
      $this->db->join('user','reconciliation.user_id=user.id');
      $this->db->join('job_posting','job_posting.id=reconciliation.job_id');
      $this->db->join('recruiter','job_posting.recruiter_id=recruiter.id');
      $this->db->order_by('reconciliation.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function contact_lists() {
      //$this->db->where('parent_id', 0);
      $this->db->select('*');
      $this->db->from('user_contact');
      $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function fetchversion(){
      $this->db->select('*');
      $query = $this->db->get('api_version');
      return $query->result_array();
   }

   public function contact_lists1() {
      $this->db->where('status', 0);
      $this->db->select('*');
      $this->db->from('user_contact');
      $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function advertise_lists() {
      $this->db->where('status', 2);
      $this->db->select('*');
      $this->db->from('advertise_list');
      $this->db->order_by('id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   public function contactbyuserid($id) {
      $this->db->where('id', $id);
      $this->db->select('*');
      $this->db->from('user_contact');
      $query = $this->db->get();
      return $query->result_array();
   }
   public function quote_lists() {
      //$this->db->where('parent_id', 0);
      $query = $this->db->get('motivational_quotes');
      return $query->result_array();
   }

   public function singlequote($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('motivational_quotes');
      return $query->result_array();
   }
   
   public function quotebyScreen($id) {
      $this->db->where('title', $id);
      $query = $this->db->get('motivational_quotes');
      return $query->result_array();
   }

   
   
   public function delete_user($id) {
      $this->db->where('id', $id);
      $this->db->delete('user');
      return true;
   }

   public function delete_userquery($id) {
      $this->db->where('id', $id);
      $this->db->delete('user_contact');
      return true;
   }

   public function delete_quote($id) {
      $this->db->where('id', $id);
      $this->db->delete('motivational_quotes');
      return true;
   }
   
   public function delete_userAssessment($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_assessment');
      return true;
   }
   
   public function delete_userEducation($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_education');
      return true;
   }
   
   public function delete_userExpert($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_expert');
      return true;
   }
   
   public function delete_userForgot($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_forgot');
      return true;
   }
   
   public function delete_userLanguage($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_language');
      return true;
   }
   
   public function delete_userMoreDetails($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_more_details');
      return true;
   }
   
   public function delete_userResume($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_resume');
      return true;
   }
   
   public function delete_userSkills($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_skills');
      return true;
   }
   
   public function delete_userTopclients($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_topclients');
      return true;
   }
   
   public function delete_userWorkExperience($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_work_experience');
      return true;
   }
   
   public function delete_verificationEmail($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('verification_email');
      return true;
   }

   public function delete_token($id) {
      $this->db->where('user_id', $id);
      $this->db->delete('user_token');
      return true;
   }
   
   public function jobseekerStatus($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('user', $data);
      return true;
   }

   public function updatepromo($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('promo_notifications', $data);
      return true;
   }

   public function jobseekerQuery($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('user_contact', $data);
      return true;
   }

   public function updateVersion($data) {
      $this->db->update('api_version', $data);
      return true;
   }
   
   public function jobseeker_detail_fetch($id) {
        $this->db->select('*');
        $this->db->from('user a');
        $this->db->join('user_more_details g', 'g.user_id=a.id', 'left');
        $this->db->where('a.id',$id);
        $query = $this->db->get(); 
        if($query->num_rows() != 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        } 
   }
   
   public function jobseeker_exp_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_work_experience');
      return $query->result_array();
   }
   public function jobseeker_edu_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_education');
      return $query->result_array();
   }
   public function jobseeker_assisment_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_assessment');
      return $query->result_array();
   }
   public function jobseeker_expert_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_expert');
      return $query->result_array();
   }
    public function jobseeker_skills_fetch($id) {
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_skills');
      return $query->result_array();
   }
   
   public function hiredCandidates() {
      $this->db->where('status', 4);
      $query = $this->db->get('user');
      return $query->result_array();
   }
   public function getboostamount() {
      
      $query = $this->db->get('boost_amount');
      return $query->result_array();
   }

   public function quote_insert($data) {
      $this->db->where('title', $data['title']);
      $query = $this->db->get('motivational_quotes');
      $numrow = $query->num_rows();
      if($numrow >= 1) {
        return false;
      } else{
        if($this->db->insert("motivational_quotes", $data)) { 
           $insert_id = $this->db->insert_id();
           return $insert_id;
        } else{
           return false;
        }
      }
   }  

   public function promo_notification_insert($data) {
      if($this->db->insert("promo_notifications", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function bonusinert($data) {
      if($this->db->insert("sign_in_bonus", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function boostinert($data) {
      if($this->db->insert("boost_amount", $data)) { 
         $insert_id = $this->db->insert_id();
         return $insert_id;
      } else{
         return false;
      }
   }

   public function boostupdate($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('boost_amount', $data);
      return true;
   }

   public function quote_update($data, $id) {
      $this->db->where('id', $id);
      $this->db->update('motivational_quotes', $data);
      return true;
   }


      var $order_column = array(null, "name", "email", "phone", 'created_at',null,"education","location","nationality","superpower","device_type",'app_version',null,"type",null,null,null,null,null);
      
  function make_query() {  
           // $this->db->select('a.*,COUNT(g.id) as countid');
           // $this->db->from('user a');
           // $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');
           // $this->db->where('a.phone!=','0');
              
           // if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))  
           // {  
           //      $this->db->where('(a.name like "%'.$_POST["search"]["value"].'%" OR a.email like "%'.$_POST["search"]["value"].'%" OR a.phone like "%'.$_POST["search"]["value"].'%" OR a.education like "%'.$_POST["search"]["value"].'%" OR a.location like "%'.$_POST["search"]["value"].'%" OR a.nationality like "%'.$_POST["search"]["value"].'%" OR a.superpower like "%'.$_POST["search"]["value"].'%" OR a.device_type like "%'.$_POST["search"]["value"].'%" OR a.app_version like "%'.$_POST["search"]["value"].'%" OR a.type like "%'.$_POST["search"]["value"].'%" OR a.created_at like "%'.$_POST["search"]["value"].'%")');  
                 
           // }  
           
           // $this->db->order_by('a.id','desc');  
             
           // $this->db->group_by('a.id');


          $this->db->select('a.*,COUNT(g.id) as countid');
          $this->db->from('user a');
          $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');

          $column_search = array(null, "name", "email", "phone", 'created_at',null,"education","location","nationality","superpower","device_type",'app_version',null,"type",null,null,null,null,null);
          
          if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) {

              $column_search = array('superpower','location',"name", "email", "phone", 'created_at',"education","location","nationality","superpower","device_type",'app_version',"type");

                $this->db->where('(a.name like "%'.$_POST["search"]["value"].'%" OR a.email like "%'.$_POST["search"]["value"].'%" OR a.phone like "%'.$_POST["search"]["value"].'%" OR a.education like "%'.$_POST["search"]["value"].'%" OR a.location like "%'.$_POST["search"]["value"].'%" OR a.nationality like "%'.$_POST["search"]["value"].'%" OR a.superpower like "%'.$_POST["search"]["value"].'%" OR a.device_type like "%'.$_POST["search"]["value"].'%" OR a.app_version like "%'.$_POST["search"]["value"].'%" OR a.type like "%'.$_POST["search"]["value"].'%" OR a.created_at like "%'.$_POST["search"]["value"].'%")');  
                 
          } else {

            if($this->input->post('superpower') || $this->input->post('location')) {
                $column_search = array('superpower','location');

                if($this->input->post('superpower')) {
                  $this->db->like('superpower', $this->input->post('superpower'));
                }
                if($this->input->post('location')) {
                  $this->db->like('location', $this->input->post('location'));
                }
            } else {

                $column_search = array(null, "name", "email", "phone", 'created_at',null,"education","location","nationality","superpower","device_type",'app_version',null,"type",null,null,null,null,null);

            }
          }

          $this->db->where('a.phone!=','0');
          $this->db->order_by('a.id','desc');
          $this->db->group_by('a.id');

          $i = 0;
     
        foreach ($column_search as $item) { // loop column 
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
      }


      function make_datatables() {  
           $this->make_query();  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           }  
           $query = $this->db->get();  
           return $query->result();  
      }  

      function get_filtered_data(){  
           $this->make_query();
           $query = $this->db->get(); 
           //echo $this->db->last_query(); 
           return $query->num_rows();  
      }       
      function get_all_data()  
      {  
            $this->db->select('a.*,COUNT(g.id) as countid');
           $this->db->from('user a');
           $this->db->join('sign_in_bonus g', 'g.user_id=a.id', 'left');
           $this->db->where('a.phone!=','0');
           $this->db->group_by('a.id');
           return $this->db->count_all_results();  
      }  


      public function fetchJobNotify() {
            $this->db->where('status', 1);
            $this->db->order_by('id','DESC');
            $this->db->limit(2000);
            $query = $this->db->get('jobs_notify');
            return $query->result_array();
       }

       public function delete_jobnotification($date) {
          $this->db->where('created_at < ', $date);
          $this->db->delete('jobs_notify');
          return true;
       }

        public function delete_jobnotificationrecord($date) {
          $this->db->where('created_at < ', $date);
          $this->db->delete('jobs_notify_records');
          return true;
       }

       public function delete_jobnotificationpromo($date) {
          $this->db->where('created_at < ', $date);
          $this->db->delete('promo_notifications');
          return true;
       }

}
?>
