<?php
ob_start();
class Newpoints_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   }

   public function companysite_lists($userLat, $userLong) {
      $this->db->select("recruiter_details.companyPic, recruiter.email, recruiter_details.companyDesc, recruiter_details.address, recruiter_details.phone, recruiter.cname, recruiter.fname, recruiter.lname, recruiter.id,recruiter_details.site_name, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from("recruiter");
      $this->db->join("recruiter_details","recruiter_details.recruiter_id = recruiter.id");
      $this->db->where('recruiter.parent_id!=', 0);
      $this->db->where('recruiter_details.site_name!=', '');
      $this->db->having("distance<", '5000');
      $this->db->order_by('recruiter.id','desc');
      $query = $this->db->get();
      return $query->result_array();
   }

   // public function companysite_jobcount($id, $typeid , $expfilter, $userLat, $userLong) {
   //    $currentDate = date("y-m-d");
   //    $uniquerarray = array();
   //    if(strlen($expfilter) > 0){
   //       for($ikl = 0;$ikl<=$expfilter;$ikl++) {
   //          $uniquerarray[] = $ikl;
   //       }
   //    }
   //    $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
   //    $this->db->from('job_posting');
   //    $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
   //    $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
   //    $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
   //    $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
   //    $this->db->where("job_posting.jobexpire >=", $currentDate);
   //    $this->db->where("job_posting.company_id", $typeid);
   //    if(strlen($expfilter) > 0){
   //       $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
   //    }
   //    $this->db->having("distance<", '5000');
   //    $this->db->order_by('salary','desc');
   //    $this->db->group_by("exp_with_salary.jobpost_id");

   //    return $this->db->count_all_results();
   // }

   public function companysite_jobcount($id, $typeid) {
      $currentDate = date("y-m-d");
      
      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.recruiter_id");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $currentDate);
      $this->db->where("job_posting.company_id", $typeid);
      $this->db->order_by('salary','desc');
      return $this->db->count_all_results();
   }

   public function companyjob_fetch_latlong($id, $expfilter, $typeid) {
      $cDate = date('y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.company_id", $typeid);
      $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      //$this->db->order_by('job_posting.id','desc');
      $this->db->order_by('salary','desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function category_jobcount($id, $typeid , $expfilter) {

      $currentDate = date("y-m-d");
      $uniquerarray = array();
      if(strlen($expfilter) > 0) {
         for($ikl = 0;$ikl<=$expfilter;$ikl++) {
            $uniquerarray[] = $ikl;
         }
      }

      $this->db->select('max(exp_with_salary.basicsalary) as salary,job_posting.recruiter_id');
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $currentDate);
      $this->db->where("job_posting.category", $typeid);
      if(strlen($expfilter) > 0) {
         $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      }
      $this->db->order_by('salary','desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      return $this->db->count_all_results();
   }

   public function categoryjob_fetch_latlong($id, $expfilter, $typeid, $userLat, $userLong) {
      $cDate = date('y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }

      $this->db->select("max(exp_with_salary.basicsalary) as salary,job_posting.id,job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $cDate);
      $this->db->where("job_posting.category", $typeid);
      $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      //$this->db->order_by('job_posting.id','desc');
      $this->db->order_by('salary','desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      //$this->db->limit(10);
      $query = $this->db->get();
      return $query->result_array();
   }

   public function nearbyjob_fetch_latlong($id, $userLat, $userLong, $expfilter) {
      $userLat = (double)$userLat;
      $userLong = (double)$userLong;

      $cDate = date('y-m-d');
      $uniquearraybyother = array();
      
      for($ityu = $expfilter; $ityu >-1; $ityu--) {
         $uniquearraybyother[] = $ityu;
      }

      $this->db->select("job_posting.id, job_posting.jobtitle, job_posting.jobDesc,max(exp_with_salary.basicsalary) as salary, job_posting.jobexpire,job_posting.jobPitch,job_posting.mode, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id,recruiter_details.latitude,recruiter_details.longitude, (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) ) + sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");
      
      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'recruiter_details.recruiter_id = job_posting.company_id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      
      $this->db->where("job_posting.jobexpire >=", $cDate);
      
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      
      $this->db->where_in("exp_with_salary.grade_id", $uniquearraybyother);
      
      $this->db->having("distance <","5000");
      
      $this->db->order_by("salary",'desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }

   public function job_toppicks($id) {

      $this->db->select("id, picks_id");
      $this->db->where("jobpost_id", $id);
      $query = $this->db->get('job_toppicks');
      return $query->result_array();
   }

   public function feturedtoppicks_fetch_latlong($id, $expfilter, $userLat, $userLong, $typeid) {
      $cDate = date('y-m-d');
      $uniquerarray = array();

      for($ikl = 0;$ikl<=$expfilter;$ikl++) {
         $uniquerarray[] = $ikl;
      }
      $this->db->select("max(exp_with_salary.basicsalary) as salary, job_posting.id, job_posting.job_type, job_posting.walkin_date, job_posting.walkin_from, job_posting.walkin_to, job_posting.jobtitle, job_posting.jobDesc,job_posting.jobPitch, job_posting.boost_status ,job_posting.jobexpire,job_posting.mode, recruiter_details.recruiter_id, recruiter_details.latitude, recruiter_details.longitude, recruiter.cname, job_posting.company_id as compId, job_posting.recruiter_id , (6371 * acos (cos ( radians($userLat) )* cos( radians( recruiter_details.latitude ) ) * cos( radians( recruiter_details.longitude ) - radians($userLong) )+ sin ( radians($userLat) ) * sin( radians( recruiter_details.latitude ) ))) AS distance");

      $this->db->from('job_posting');
      $this->db->join('recruiter_details', 'job_posting.company_id = recruiter_details.recruiter_id');
      $this->db->join('exp_with_salary', 'exp_with_salary.jobpost_id = job_posting.id');
      $this->db->join('recruiter', 'recruiter.id = job_posting.recruiter_id');
      //$this->db->join('job_toppicks', 'job_toppicks.jobpost_id = job_posting.id');
      $this->db->where("job_posting.id NOT IN (select jobpost_id from applied_jobs where user_id = $id)");
      $this->db->where("job_posting.jobexpire >=", $cDate);
      //$this->db->where('job_toppicks.picks_id', $typeid);
      $this->db->where_in("exp_with_salary.grade_id", $uniquerarray);
      $this->db->having("distance<", '1000');
      $this->db->order_by('job_posting.id','desc');
      $this->db->group_by("exp_with_salary.jobpost_id");
      $query = $this->db->get();
      return $query->result_array();
   }


   public function fetchnews() {
      $this->db->order_by('id','desc');
      $query = $this->db->get("news_list");
      return $query->result_array();
   }

   public function fetchvideos() {
      $this->db->where('status',2);
      $this->db->order_by('id','desc');
      $query = $this->db->get("videoadvertise_list");
      return $query->result_array();
   }
}
?>