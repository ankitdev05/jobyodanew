<?php include_once('header.php'); ?>


<div class="container">
	<div class="mainheadings-yoda">
                 
              </div>
	<div class="contenttxtds pageformat">
	<h1>Terms of Use</h1>
	
            <p>This page states the terms of use (“Terms”) under which you (“You”) may use the JobYoDA Inc. Website and Application, and Your relationship with JobYoDA Inc. (“JobYoDA Inc.”, “we” or “us”). Please read them carefully as they affect Your rights and liabilities under the law. If You do not agree to these Terms, please do not register for or use the JobYoDA Inc. Website or JobYoDA Application. These Terms are effective as of 20th-August-2019.  Please review this page regularly as this may change at any time without prior notice. If You have any questions on the Terms, please contact us dataprivacyofficer@jobyoda.com.</p>

            <h4>1. Conditions of Use </h4>

                <p>This JobYoDA Inc. Website/Application is provided to you for your use subject to these terms. These terms form a binding agreement between you and JobYoDA Inc. By accessing or using the JobYoDA Inc. Website/Application you agree to accept and/or to be bound by these Terms. You agree to use the JobYoDA Inc. Website at your own risk.</p>

                <p>In the case of any conflict between these Terms and any contract you have with JobYoDA Inc., these Terms will take precedence.</p>
                
                <p>We may update these Terms from time to time for legal or regulatory reasons or to allow the proper operation of the JobYoDA Inc. Website. Any changes will be notified to you via a suitable announcement on the JobYoDA Inc. Website. The changes will apply to the use of the JobYoDA Inc. Website after we have given you notice. If you do not wish to accept the new Terms you should not continue to use the JobYoDA Inc. Website. If you continue to use the JobYoDA Inc. Website after the date on which the change comes into effect, your use of the JobYoDA Inc. Website indicates your agreement to be bound by the new Terms.</p>

            <h4>2. Definitions </h4>

                <p>“Recruiter” means a person or entity that is accessing a JobYoDA Inc. Website to post a job or utilizing the JobYoDA Inc. Services for any reason related to the purpose of seeking candidates for employment.</p>
                
                <p> “Company Materials” includes any brochures, emails, job postings, web site content, career fair material, audio, videos, photographs, logos, trademarks, service marks, domain names, documents or other materials provided by Employer, if any, for use in connection with the JobYoDA Inc. Services.</p>
                
                <p>“Jobseeker/Candidate” means a User who is accessing a JobYoDA Inc. Website/Application to search for a job or in any other capacity except as an Employer.</p>
                
                <p>“Company Profiles” means profile created by the recruiters.</p>
                
                <p>“JobYoDA Inc.” means JobYoDA Inc. or the company operating the JobYoDA Inc. Website for the country in which you live or in which business is headquartered.</p>
                
                <p>“JobYoDA Inc. Content” includes all Text, Graphics, Design, Programming, information, images, video, audio files, software and other contents used on the JobYoDA Inc. Website.</p>
                
                <p>“JobYoDA Inc. Database” or “JobYoDA Inc. Databases” includes all job advertisement posted on JobYoDA Inc. Websites and/or all candidates and/or employers’ information registered with JobYoDA Inc.</p>
                
                <p>“JobYoDA Inc. Profile” or “JobYoDA Inc. Profiles” means the profile created by Candidates that may include personal information, resume and photo.</p> 

                <p>“JobYoDA Inc. Resume Database” or “Resume Database” means the candidates JobYoDA Inc. Profiles and resume created and/or uploaded into JobYoDA Inc. databases.</p>
                
                <p>“JobYoDA Inc. Services” means any services provided by JobYoDA Inc. or its agents including: </p>
                
                <p>The provision of online job site, including posting and searching for employment opportunities, and creating JobYoDA Inc. Profiles and Company Profiles which may include personal information.</p>
                
                <p>Matching of job specifications with potential candidate JobYoDA Inc. Profiles and advising Users and/or Candidates of the positions available.</p>
                
                <p>Providing Advertisers and/or Employers with other ancillary talent sourcing and advertising services including banners, electronic direct mail system, design and posting support services.</p>
                
                <p>providing Advertisers and/or Employers services to advertise on JobYoDA Inc. Website.</p>
                
                <p>“JobYoDA Inc. Website” means any website under JobYoDA Inc.’s control, including:</p>
                
                <p>http://www.JobYoDA Inc.,</p>
                
                <p>JobYoDA Inc. Mobile Applications.</p>
                
                <p>“User” refers to any individual or entity that uses any aspect of the JobYoDA Inc. Website and/or JobYoDA Inc. Services.</p>
                
                <p>“User Content” means all information, data, text, software, music, sound, photographs, graphics, video, advertisements, messages or other materials submitted, posted or displayed by Users on or through JobYoDA Inc. Website.</p>

                <p>“You” or “you” means the person who (or the entity on behalf of whom you are acting) that is agreeing to these Terms.</p>






                <h4>3. Registration</h4>

                    <p>You must ensure that the details provided by you on registration or at any time are correct and complete.</p>
                    <p>You must inform us immediately of any changes to the information that you provided when registering by updating your personal details in order that we can communicate with you effectively.</p>

                    <h5>Password and security </h5>

                    <p>When you register to use the JobYoDA Inc. Website you will be asked to create a password. In order to prevent fraud, you must keep this password confidential and must not disclose it or share it with anyone. If you know or suspect that someone else knows your password you should notify us by contacting us here immediately.</p>

                    <p>If JobYoDA Inc. has reason to believe that there is likely to be a breach of security or misuse of the JobYoDA Inc. Website, we may require you to change your password or we may suspend your account.</p>

                    <p>As a result of your loss of your Password or the misuse of the JobYoDA Inc. Website: all losses or damage incurred thereby shall be borne by you and you shall fully indemnify JobYoDA Inc. should JobYoDA Inc. suffer any loss or damage.</p>

                <h4>4. Intellectual Property  </h4>

                    <p>JobYoDA Inc. Website, JobYoDA Inc. Content, and all rights, title and interest in and to JobYoDA Inc. Website and JobYoDA Inc. Content are the exclusive property of JobYoDA Inc. or its licensors.</p>

                    <p>JobYoDA Inc. Website and JobYoDA Inc. Content are protected by copyright, trade mark, database right and other intellectual property rights.</p>

                    <p>You may retrieve and display JobYoDA Inc.. Content of the JobYoDA Inc. Website on a computer screen, store such content in electronic form on disk (but not any server or other storage device connected to a network) or print one copy of such content for your own personal, non-commercial use, provided you keep intact all and any copyright and proprietary notices. You may not otherwise reproduce, modify, copy or distribute or use for commercial purposes any of the materials or JobYoDA Inc. Content on the JobYoDA Inc. Website without written permission from JobYoDA Inc..</p>

                <h4>5. Availability of the JobYoDA Inc. Website/Application </h4>

                    <p>Although we aim to offer you the best service possible, we make no promise that the services at the JobYoDA Inc. Website will meet your requirements.</p>

                    <p>However, we cannot guarantee that the JobYoDA Inc. Website will be fault-free, error-free, or that JobYoDA Inc. Website and servers are free of viruses or other harmful mechanisms. If a fault occurs with the JobYoDA Inc. website, you should report it here and we will attempt to correct the fault as soon as we can.</p>

                    <p>Your access to the JobYoDA Inc. Website may be occasionally restricted to allow for repairs, maintenance or the introduction of new content, facilities or services. We will attempt to restore access and/or service as soon as we reasonably can.</p>

                    <p>Your use of the JobYoDA Inc. Website </p>

                    <p>JobYoDA Inc. hereby grants you a limited, terminable, non-exclusive right to access and use JobYoDA Inc. Website only for your personal use and/or employment purposes.</p>

                    <p>You may not use the JobYoDA Inc. Website for any of the following purposes:</p>

                    <p>disseminating any unlawful, harassing, libelous, abusive, threatening, harmful, vulgar, obscene, or otherwise objectionable material or otherwise breaching any laws;</p>

                    <p>aggregating, copying or duplicating in any manner any of the JobYoDA Inc. Content or information available from any JobYoDA Inc. Website, including expired job postings;</p>

                    <p>reproducing any of the JobYoDA Inc. Content for general use;</p>

                    <p>You agree not to resell or assign your rights or obligations under these Terms. You also agree not to make any unauthorized commercial use of any JobYoDA Inc. Website.</p>

                    <p>link to any JobYoDA Inc. Content or information available from any JobYoDA Inc. Website;</p>

                    <p>transmitting material that encourages conduct that constitutes a criminal offence, or otherwise breaches any applicable laws, regulations or code of practice;</p>

                    <p>interfering with any other person’s use or enjoyment of the JobYoDA Inc. Website; or</p>

                    <p>making, transmitting or storing electronic copies of materials protected by copyright without the permission of the owner.</p>
  

                <h4>6. Use of the JobYoDA Inc. Services  </h4>

                    <p>The JobYoDA Inc. Services may be used only by: individuals seeking employment, and career and career-related information, such as education and voluntary services and recruiters or their agents seeking candidates for employment purposes</p>

                    <p>Your use of the JobYoDA Inc. Services is also subject to any other contracts you may have with JobYoDA Inc. In the case of any conflict between these Terms and any contract you have with JobYoDA Inc., these Terms will prevail.</p>



                <h4>7. Conditions: Users agree not to - </h4>

                    <p>transmit, post, distribute, store or destroy material, including without limitation JobYoDA Inc. Content, in violation of any applicable law or regulation, including but not limited to laws or regulations governing the collection, processing, or transfer of personal information, or in breach of JobYoDA Inc.’s Privacy Policy.</p>

                    <p>violate or attempt to violate the security of any JobYoDA Inc. Website.</p>

                    <p>reverse engineer or decompile any parts of any JobYoDA Inc. Website.</p>

                    <p>aggregate, copy or duplicate in any matter any of the JobYoDA Inc. Content or information available from any JobYoDA Inc. Website, including expired job postings, other than as permitted by these Terms.</p>

                    <p>post any content or material that facilitates, promotes or endorses scams, false or misleading information or illegal activities, OR endorses or provides instructional information about illegal activities or other activities prohibited by these Terms, such as making or buying illegal weapons, violating someone’s privacy, providing or creating computer viruses or pirating media, OR promotes or endorses any political views;</p>

                    <p>attempt to interfere with service to any User, host or network, including, without limitation, via means of submitting a virus to any JobYoDA Inc. Website, overloading, “flooding”, “spamming”, “mailbombing”, or “crashing”.</p>

                   <p>use the JobYoDA Inc. Services for any unlawful purpose or illegal activity, or post or submit any content, profile,  resume, or job posting that is defamatory, libelous, implicitly or explicitly offensive, vulgar, obscene, threatening, harassing, abusive, hateful, racist, discriminatory, of a menacing character or likely to cause annoyance, inconvenience, embarrassment, anxiety or could cause harassment to any person or include any links to pornographic, indecent or sexually explicit material of any kind, as determined by JobYoDA Inc.’s discretion.</p>
                    
                    <p>post any profile or resume which is not a genuine profile or resume and which attempts to advertise or promote products or services.</p>

                    <p>to utilize any of the information available from this website and/or any databases containing information by virtue of being granted access by JY for competition purposes.</p>

                    <p>Violations of network security may result in civil and/or criminal liability. JobYoDA Inc. will investigate occurrences which may involve such violations and may involve, and cooperate with, law enforcement authorities in prosecuting Users who are involved in such violations.</p>

                    <p>access data not intended for you or logging into a server or account which you are not authorized to access.</p>

                    <p>post or submit to any JobYoDA Inc. Website any inaccurate, incomplete, misleading, false, not up to date biographical information or information which is not your own.</p>

                    <p>post content that contains restricted or password-only access pages, or hidden pages or images.</p>

                    <p>solicit passwords or personally identifiable information from other Users.</p>

                    <p>delete or alter any material posted by any other person or entity.</p>

                    <p>harass, incite harassment or advocate harassment of any group, company or individual.</p>

                    <p>send unsolicited mail or email, make unsolicited phone calls or send unsolicited faxes promoting and/or advertising products or services to any User, or contact any users that have specifically requested not to be contacted by you.</p>





                <h4>8. Additional Terms applicable to Employers  </h4>
                
                    <p>JobYoDA Inc. hereby grants you a limited, terminable, non-exclusive right to access and use JobYoDA Inc. Website for your internal business use seeking candidates for employment. This authorizes you to view and download a single copy of JobYoDA Inc. Content or materials on JobYoDA Inc. Website solely for your use directly related to searching for and recruiting Candidates.</p>
                    
                    <p>JobYoDA Inc. also grants you a limited, terminable, non-exclusive right to use JobYoDA Inc. Services for your internal use only.</p>
                    
                    <p>You are responsible for maintaining the confidentiality of your Recruiter account, Company Profiles and passwords, as applicable.    </p>
                    
                    <p>You may not share your password or other account access information with any other party, temporarily or permanently, and you shall be strictly liable and/or responsible for all uses of your JobYoDA Inc. Website registrations and passwords, whether or not authorized by you.</p>

                    <p>You agree to immediately notify JobYoDA Inc. of any unauthorized use of your recruiter account, Company Profiles or passwords.</p>
                    
                    <p>Recruiters are solely responsible for the Company Materials on JobYoDA Inc. Website. JobYoDA Inc. is not to be considered to be an employer with respect to your use of any JobYoDA Inc. Website and JobYoDA Inc. shall not be responsible for any employment decisions, for whatever reasons, made by any entity posting jobs on any JobYoDA Inc. Website.</p>
                    
                    <p>Recruiter represents, warrants and covenants that any Company Materials provided by them for use in connection with JobYoDA Inc. Services will not violate any laws or regulations or third-party proprietary rights, including, without limitation, copyright, trade mark, obscenity, rights or publicity or privacy, and defamation laws.</p>
                    
                    <p>Recruiters acknowledge and agree that it shall disregard any personal data received from the Candidate which are irrelevant to obtaining and assessing the suitability of candidates.</p>

                    <p>Company Materials may not contain:</p>

                    <p>misleading, unreadable, or “hidden” keywords, repeated keywords or keywords that are irrelevant to the job opportunity being presented.</p>

                    <p>names, logos or trademarks of unaffiliated companies;</p>

                    <p>inaccurate, false, or misleading information; and</p>

                    <p>material or links to material that is discriminatory, sexually explicit, obscene, libelous, defamatory, threatening, harassing, abusive, or hateful, or solicits personal information from anyone under 18.</p>

                    <p>You may not use your Company Materials to: post jobs in a manner that does not comply with applicable local, national laws, including but not limited to laws relating to labor and employment, equal employment opportunity and employment eligibility requirements, data privacy, data access and use, and intellectual property, sell, promote or advertise products or services, post any franchise, pyramid scheme, distributorship, or multi-level marketing opportunity, advertise home-based and/or internet-based employment, promote any opportunity that does not represent bona fide employment.</p>

                    <p>JobYoDA Inc. does not encourage any Company Materials on JobYoDA Inc. Website that request Candidates to pay deposits, placement fees, processing fees or any other similar fees, directly or indirectly, and/or require Candidates to make purchases.</p>

                    <p>JobYoDA Inc. is under no obligation to monitor the Company Materials on JobYoDA Inc. Website, but JobYoDA Inc. may monitor the Employer Materials at random.</p>

                    <p>JobYoDA Inc. reserves the right to remove any Company Materials or content from any JobYoDA Inc. Website, which in the reasonable exercise of JobYoDA Inc.’s discretion, does not comply with this Section, or if any content is posted that JobYoDA Inc. believes is not in the best interest of JobYoDA Inc..</p>

                    <p>If any time during your use of the JobYoDA Inc. Services, you made a misrepresentation of fact to JobYoDA Inc. or otherwise misled JobYoDA Inc. in regards to the nature of your business activities or breach of the Terms herein, JobYoDA Inc. may terminate your use of the JobYoDA Inc. Services and in such event, any payment made by you shall be forfeited.</p>

                    <p>You understand and acknowledge that if you cancel your Recruiter account or your Employer account is terminated, all your account information from JobYoDA Inc., including saved JobYoDA Inc. Profiles and resumes, network contacts, and email mailing lists, will be marked as deleted in and may be deleted from JobYoDA Inc. Databases. Information may continue to be available for some period of time because of delays in propagating such deletion through JobYoDA Inc.’s web servers.</p>

                     

                <h4>9. Additional Terms applicable to Candidates  </h4>

                    <p>JobYoDA Inc. hereby grants you a limited, terminable, non-exclusive right to access and use JobYoDA Application only for your personal use seeking employment opportunities for yourself. This authorizes you to view and apply for JobYoDA Inc. Content or material on JobYoDA Inc. Website solely for your personal, non-commercial use.</p>
     
                    <p>When you register with any JobYoDA Inc. Application, you will be asked to create an account and provide JobYoDA Inc. with certain information including a valid email address.</p>

                    <p>Any JobYoDA Inc. Profile you submit must be accurate, complete, up to date and not misleading. The JobYoDA Inc. Profile requires standard fields to be completed by you. You may not impersonate another person, living or dead.</p>

                    <p>You acknowledge and agree that you are solely responsible for the form, content and accuracy of any JobYoDA Inc. Profile and resume or material contained therein placed by you on the JobYoDA Inc. Application.</p>

                    <p>You acknowledge and agree that you are solely responsible for any consequences arising from such posting.</p>
                    
                    <p>Please see JobYoDA Inc.’s Privacy Policy, for further details regarding your information. Please note, as set forth in the JobYoDA Inc.’s Privacy Policy, that JobYoDA Inc. may collect certain User information and may contact Users periodically in accordance with the terms of the Privacy Policy. In addition, JobYoDA Inc. reserves the right to comply, in its sole discretion, with legal requirements, requests from law enforcement agencies or requests from government entities, even to the extent that such compliance may require disclosure of certain User information. In addition, third parties may retain cached copies of User information.</p>

                    <p>You understand and acknowledge that all information provided by you, your JobYoDA Inc. Profile, resume, and/or account information shall be disclosed to prospective Employers.</p>

                    <p>You understand and acknowledge that you have no ownership rights in your account and that if you cancel your JobYoDA Inc. account or your JobYoDA Inc. Account is terminated, all your account information from JobYoDA Inc., including JobYoDA Inc. Profile, resumes, cover letters, saved jobs, will be marked as deleted in and may be deleted from JobYoDA Inc. Databases and will be removed from any public area of the JobYoDA Inc. Website. Information may continue to be available for some period of time because of delays in propagating such deletion through JobYoDA Inc.’s web servers. In addition, third parties may retain saved copies of your information. For your account deletion, you may reach out to help@jobyoda.com. </p>

                    <p>JobYoDA Inc. reserves the right to delete your account and all of your information after a significant duration of inactivity.</p>



                <h4>10. User Content and submissions </h4>

                    <p>You understand that all User Content is the sole responsibility of the person from which such User Content originated.</p> 

                    <p>You understand and acknowledge that all information provided by you, your JobYoDA Inc. Profile, resume, and/or account information shall be stored in JobYoDA Inc. Databases and/or JobYoDA Inc. Resume Databases.</p>

                    <p>By submitting, posting or displaying User Content on or through JobYoDA Inc. Website, you grant JobYoDA Inc., subject to your privacy setting, a worldwide, non-exclusive, royalty-free license to reproduce, adapt, distribute and publish such User Content through JobYoDA Inc. Website. JobYoDA Inc. will discontinue this licensed use within a commercially reasonable period after such User Content is removed from JobYoDA Inc. Website. JobYoDA Inc. reserves the right to refuse to accept, post, display or transmit any User Content in its sole discretion.</p>

                    <p>JobYoDA Inc. may review and remove any User Content that, in its sole judgment, violates these Terms, violates applicable laws, rules or regulations, is abusive, disruptive, offensive or illegal, or violates the rights of, or harms or threatens the safety of, Users of any JobYoDA Inc. Website. JobYoDA Inc. reserves the right to expel Users and prevent their further access to the JobYoDA Inc. Website and/or use of JobYoDA Inc. Services for violating the Terms or applicable laws, rules or regulations. JobYoDA Inc. may take any action with respect to User Content that it deems necessary or appropriate in its sole discretion if it believes that such User Content could create liability for JobYoDA Inc., damage JobYoDA Inc.’s brand or public image, or cause JobYoDA Inc. to lose Users.</p>

                    <p>JobYoDA Inc. does not represent or guarantee the truthfulness, accuracy, or reliability of User Content, derivative works from User Content, or any other communications posted by Users nor does JobYoDA Inc. endorse any opinions expressed by Users. You acknowledge that any reliance on material posted by other Users will be at your own risk.</p>

                   


                
                <h4>11. Terms for Termination  </h4>

                    <p>These Terms will remain in full force and effect while you are a user of any JobYoDA Inc. Website or Application.</p>

                    <p>JobYoDA Inc. reserves the right, at its sole discretion, to pursue all of its legal remedies, including but not limited to removal of your User Content from the JobYoDA Inc. Website and immediate termination of your registration with or ability to access the JobYoDA Inc. Website and/or any other services provided to you by JobYoDA Inc., upon any breach by you of these Terms or if JobYoDA Inc. is unable to verify or authenticate any information you submit to a JobYoDA Inc. Website registration.</p>

                    <p>JobYoDA Inc. does not screen or censor the listings, including JobYoDA Inc. or profiles created. JobYoDA Inc. is not involved in the actual transaction between recruiters and jobseekers. As a result, JobYoDA Inc. is not responsible for User Content, the quality, safety or legality of the jobs or JobYoDA Inc. or resumes posted, the truth or accuracy of the listings, the ability of employers to offer job opportunities to candidates or the ability of candidates to fill job openings and JobYoDA Inc. makes no representations about any jobs, JobYoDA Inc. Profiles, resumes or User Content on the JobYoDA Inc. Website/Application.</p>

                    <p>Note that there are risks, including but not limited to the risk of physical harm, of dealing with strangers, underage persons or people acting under false pretenses. You assume all risks associated with dealing with other users with whom you come in contact through the JobYoDA Inc. Website. By its very nature other people’s information may be offensive, harmful or inaccurate, and in some cases will be mislabeled or deceptively labeled. We expect that you will use caution and common sense when using the JobYoDA Inc. Website.</p>

                    <p>JobYoDA Inc. reserves the right in its sole discretion to remove any User Content including but not limited to JobYoDA Inc. Profiles, Company Profiles, Company Materials, or other material from the JobYoDA Inc. Website from time to time. When JobYoDA Inc. exercises this discretion and there is a breach of these Terms by the User, any payment made by the User will be forfeited.</p>

                    <p>JobYoDA Inc. Website and JobYoDA Inc. Content may contain inaccuracies or typographical errors. JobYoDA Inc. makes no representations about the accuracy, reliability, completeness, or timeliness of any JobYoDA Inc. Website or the JobYoDA Inc. Content. The use of all JobYoDA Inc. Website and the JobYoDA Inc. Content is at your own risk.</p>

                    <p>Nothing on JobYoDA Inc. Website shall be considered an endorsement, representation or warranty with respect to any User or third party, whether in regard to its web sites, content or services.</p>

                    <p>If we are in breach of these Terms, we will only be responsible for any losses that you suffer as a direct result and to the extent that they are a foreseeable consequence to both of us at the time you use the JobYoDA Inc. Website. Our liability shall not in any event include business losses such as lost data, lost profits or business interruption.</p>



                <h4>12. Disclaimers </h4>

                    <p>JobYoDA Inc. shall not be liable for any loss of information howsoever caused as a result of any interruption, suspension or termination of JobYoDA Inc. Services or for JobYoDA Inc. Content, accuracy or quality of information available or transmitted through JobYoDA Inc. Services.</p>

                    <p>You acknowledge and agree that it is not JobYoDA Inc.’s policy to exercise editorial control over and to edit or amend any data or contents of any emails or posting or any information that may be inserted or made available or transmitted to or from a third party in or through JobYoDA Inc. Website and/or JobYoDA Inc. Services.</p>

                    <p>obYoDA Inc. has taken every reasonable step to be complied and will continue to comply with its obligations as a data platform arising from the data protection and privacy laws in force from time to time to the extent that those obligations are relevant to this Agreement The term Confidential Information will not, however, include any information that identifies or directly relates to natural persons (“Personal Data”). Each party will exercise commercially reasonable efforts not to disclose any Personal Data to the other party and to restrict the other party’s access to its Personal Data, but if a party is given access to the other party’s Personal Data, the receiving party will protect such Personal Data using a reasonable standard of care.</p>

                <h4>13. Refund </h4>

                    <p>Any refund of payment made to JobYoDA Inc. shall be at the sole discretion of JobYoDA Inc..</p>

                <h4>14. Applicable Law </h4>

                    <p>These Terms will be subject to the laws of Philippines.</p>

                    <p>We will try to solve any disagreements quickly and efficiently. If you are not happy with the way we deal with any disagreement and you want to take court proceedings, you must do so within Philippines.</p>

                    <p>If you breach these Terms and JobYoDA Inc. chooses to ignore this, JobYoDA Inc. will still be entitled to use its rights and remedies at a later date or in any other situation where you breach the Terms.</p>

                    <p>JobYoDA Inc. shall not be responsible for any breach of these Terms caused by circumstances beyond its reasonable control.</p>

                    <p>If any of these Terms shall be found by any court or administrative body of competent jurisdiction to be invalid or unenforceable, the invalidity or unenforceability of such provision shall not affect the other provisions of these Terms and all provision not affected by such invalidity or unenforceability shall remain in full force and effect.</p>

                    <p>The JobYoDA Inc. Website is owned and operated by JobYoDA Inc. Inc. We have taken all the steps to implement and shall maintain an information security program including reasonable administrative, technical and physical measures designed to secure and protect the confidentiality, integrity and availability of all Confidential Information while in such party’s possession against unauthorized, unlawful or accidental access, disclosure, transfer, destruction, loss or alteration.</p>

                    <p>If you have any queries, please contact us at <a href="mailto:dataprivacyofficer@jobyoda.com">dataprivacyofficer@jobyoda.com</a>.</p>

                    <p><strong>JobYoDA Team</strong></p>

                    <BR>

<p>JobYoDA Team</p>
				
				
				
				
				

	</div> 
</div>

<?php include_once('footer.php'); ?>