<?php include_once('header.php'); ?>
<div class="managerpart">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-9 expanddiv">
		   <div class="innerbglay">
          <div class="mainheadings-yoda">
          </div>
          <div class="adminopnts">
           <?php 
            if(!empty($story_listss[0]['story']))
            {
              foreach($story_listss as $story_lists)                
              {
           ?>	
           <div class="successpanal">
            <div class="profiledata">
             <div class="imgthumb">
              <?php if(!empty($story_lists['profilePic']))
                {
              ?>
              <img src="<?php echo $story_lists['profilePic']?>" style="width:80px;border-radius: 50%;height:80px"> 
              <?php	
                }
              ?>
            </div>
          
            <div class="profilecontent">
              <h3><?php if(!empty($story_lists['user_name'])){echo $story_lists['user_name']; }?></h3>
              <p class="statusday"><?php if(!empty($story_lists['date'])){echo $story_lists['date']; }?></p>
            </div>
          
            <div class="description">
              <p><?php if(!empty($story_lists['story'])){echo $story_lists['story']; }?></p>
            </div>
          
            <div class="likesmanages">
              <button type="button" class="activelikes" onclick='likedstory("<?php echo $story_lists['story_id']; ?>","<?php echo $story_lists['like_status']; ?>")'><i class="far fa-thumbs-up" style="<?php if($story_lists['like_status']=='1'){?>color:#28a745;  <?php } ?>"></i><span id="likestories<?php echo $story_lists['story_id']; ?>"><?php if(!empty($story_lists['like_count'])){echo $story_lists['like_count']; } else{echo 0;}?></span>

              </button>
              <p class="activecomments" style="cursor: pointer;" data-toggle="modal" data-target="#mymodel<?php echo $story_lists['story_id']; ?>" id="comtest<?php echo $story_lists['story_id']; ?>" ><i class="far fa-comment">
                </i><?php if(!empty($story_lists['comment_count'])){echo $story_lists['comment_count']; }  else{echo 0;}?>           
              </p>

              <p class="activecomments" style="display:none;cursor:pointer;" data-toggle="modal" data-target="#mymodel<?php echo $story_lists['story_id']; ?>" id="comresp1<?php echo $story_lists['story_id'];?>" >

              </p>
        
            </div>
          
            <div class="areacomments">
              <span id="commentResponse<?php echo $story_lists['story_id'];?>"></span>
              <form>
                <input type="text" placeholder="Add Comment" id="comments<?php echo $story_lists['story_id'];?>">
               
                <input type="hidden" id="storyidd<?php echo $story_lists['story_id'];?>" value="<?php echo $story_lists['story_id']; ?>">
                <button type="button" class="greenbtn" onclick='addcomment("<?php echo $story_lists['story_id']; ?>")'> Add Comment</button>&emsp;&emsp;&emsp;&emsp;
                <span id="commentblank<?php echo $story_lists['story_id']; ?>"></span>
              </form>
            </div>
          
          </div>
        </div>

        <div class="modal fade" id="mymodel<?php echo $story_lists['story_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered addeducation" role="document">
            <div class="modal-content">
              <div class="modal-header" >
         
                <div class="" style="text-align:left; width:100%;">
                  <div class="imgthumb" style="">
           
                    <?php if(!empty($story_lists['profilePic']))
                    {
                    ?>
                      <img src="<?php echo $story_lists['profilePic']?>" style="width:80px;border-radius: 50%;height:80px"> 
                    <?php 
                    }
                    ?>
                  </div>
          
                  <div class="profilecontent">
                  <h3><?php if(!empty($story_lists['user_name'])){echo $story_lists['user_name']; }?></h3>
                  <p class="statusday"><?php if(!empty($story_lists['date'])){echo $story_lists['date']; }?></p>
                  <div class="description"><p style="margin-top: 10px;"><?php if(!empty($story_lists['story'])){echo $story_lists['story']; }?></p></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4"></div>
                  <div class="description">
                    <p></p>
                  </div>
          
                  <div class="likesmanages">
                    <button type="button" class="activelikes"><i class="far fa-thumbs-up"></i><span id="likestories<?php echo $story_lists['story_id']; ?>"><?php if(!empty($story_lists['like_count'])){echo $story_lists['like_count']; }?></span>

                    </button>
                    <p class="activecomments"><i class="far fa-comment">
                      </i><?php if(!empty($story_lists['comment_count'])){echo $story_lists['comment_count']; }?>
                    </p>
        
                  </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <?php 
              $story_list_count = $this->Jobpost_Model->comment_lists($story_lists['story_id']);
              if(!empty($story_list_count))
              {
                foreach($story_list_count as $story_list_counts)
                {
         
              ?>
              <div class="profiledata">
                <div class="imgthumb">
                  <img src="<?php echo $story_list_counts['profilePic'];?>" style="border-radius: 50%;"> 
                </div>
                <div class="profilecontent">
                  <h3><?php echo $story_list_counts['name'];?></h3>
        
                  <p class="statusday"><?php echo date("y-m-d",strtotime($story_list_counts['createdon']))?></p>
                </div>
          
                <div class="description">
                  <p><?php echo $story_list_counts['comments'];?></p>
                </div>
              </div>
              <?php 
    
               }
             } else {
                echo '<img class="bx_img" src="https://jobyoda.com/webfiles/img/emptybx.png"><p>No reviews yet.</p>';
              }
              ?>
            </div>
          </div>
        </div>
      </div>

      <?php }
      }else{ ?>
        <img class="bx_img" src="<?php echo base_url();?>webfiles/img/emptybx.png">
        <center>No Data Found in success story</center>
      <?php }
      ?>
    
   </div>
	</div>
  </div>  
 </div>
 </div>
 </div>
      
      
      <?php include_once('footer.php'); ?>
      
        
      <div class="modal fade" id="exampleModalCenterjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                   <h5 class="modal-title" id="exampleModalLongTitle">
                     <span id="savedjobmessage"></span>
                   </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                 <div class="filldetails">
                 
                 </div>
              </div>
               </div>
            </div>
         </div>
      </div>



    
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
   <!--   <script src="<?php echo base_url().'webfiles/';?>js/bootstrap-timepicker.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script>

 
  <script>
    function savedjob(id)
       {
         
          $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/savedjovData') ?>",
             'data' :'jobId='+id,
             'success':function(htmlres)
             {
                //alert(htmlres);return false;  
                if(htmlres !='')
                {
                if(htmlres == 1)
                {
                $('#test'+id).css('display', 'block'); 
                $('#test1'+id).css('display', 'none'); 
                $('#test3'+id).css('display', 'none');   
                $("#savedjobmessage").html('Job saved successfully');
                 $("#exampleModalCenterjob").modal('show');
                  
                }
                if(htmlres == 2)
                {
                   $('#test1'+id).css('display', 'block'); 
                   $('#test'+id).css('display', 'none'); 
                   $('#test3'+id).css('display', 'none'); 
                  $("#savedjobmessage").html('Job  unsaved successfully');
                  $("#exampleModalCenterjob").modal('show');
                
                }
                 if(htmlres == 3)
                {
                   $('#test'+id).css('display', 'none'); 
                   $('#test1'+id).css('display', 'none'); 
                   $("#savedjobmessage").html('Job saved successfully');
                   $("#exampleModalCenterjob").modal('show');
                }
             }
             }
               
           });
       }
 </script>
 <script>
 	function likedstory(story_id,like_status)
 	{
   //alert(like_status);
 		$.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/likedtory');?>",
             'data' :'storyId='+story_id+'&like_status='+like_status,
             'success':function(htmlres)
             {
              if(htmlres !='') 
              {
               // $("#likestories"+story_id).hide();
                //$("#likestory"+story_id).html(htmlres);
                location.reload();
           
              }
             }
               
           });
 	}
 </script>

 <script>
  function addcomment(id)
  {
    var storyid = $("#storyidd"+id).val();
    var comment = $("#comments"+id).val()
    if(comment=='')
    {

       $("#commentblank"+id).html('<p style="color:red;">please enter comment</p>');
       return false;
    }
    if(comment !="")
    {
       $("#commentblank"+id).html('');
    }
    $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/addComment');?>",
             'data' :'storyId='+storyid+'&comment='+comment,
             'success':function(htmlres)
             {
              if(htmlres !='')
              {
              $("#comments"+id).val('');
              $('#comtest'+id).css('display', 'none'); 
              $("#comresp1"+id).css('display', 'block'); 
              $("#comresp1"+id).html('<i class="far fa-comment"></i>'+htmlres); 
              $("#commentResponse"+id).html("<div class ='alert alert-success'>Comment added successfully</div>");
               setTimeout(function(){
                location.reload();
                }, 5000); 
              }
              else
              {
                $("#commentResponse"+id).html("<div class ='alert alert-danger'>Comment not added!!</div>");
              }
             }
               
           });
  }
 </script>

   
   </body>
</html>

