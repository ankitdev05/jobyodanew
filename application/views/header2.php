<!DOCTYPE html>
<html lang="en-us">
<head>
    <title>JobYoDA </title>
    <meta charset="UTF-8" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="google-site-verification" content="059_geDz93Z9O9EKdpV5BF3oQAB_Vu6keNmwG_l2kc8" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-R64BFR96SR"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-R64BFR96SR');
    </script>
    <!-- Global site tag (gtag.js) - Google Ads: 851948051 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-851948051"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-851948051');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178463617-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-178463617-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '197166028031236'); 
    fbq('track', 'PageView');
    </script>

    <noscript>
    <img height="1" width="1"
    src="imageproxy?token=932e1c629e9bf5d4381f631a825d98945b70e7d0e9200d8e3c300c34f4e6bb88&url=https://www.facebook.com/tr?id=197166028031236&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <meta name="title" content="<?php if(isset($meta_title)){ echo $meta_title; } ?>">
    <meta name="description" content="<?php if(isset($meta_description)){ echo $meta_description; } ?>">

    <link rel="icon" href="<?php echo base_url();?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'webfiles/';?>newone/css/style.css" />

    <link href="<?php echo base_url().'webfiles/';?>css/fontawesome.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'webfiles/';?>css/style.css" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url().'webfiles/';?>css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'webfiles/';?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css"/>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'webfiles/';?>newone/css/responsive.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'webfiles/';?>newone/css/aos.css" />
    <style type="text/css">
        .btn-group{height: 49px!important;}
        .pac-container {
            z-index: 10000 !important;
        }
        label.error{color:red!important;}
    </style>
</head>

<body class=""> 

    <header>
        <div class="Header">
            <nav class="navbar">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Menu" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="https://jobyoda.com" class="navbar-brand">
                            <img src="<?php echo base_url().'webfiles/';?>newone/images/Logo.png">
                            <p>#1 BPO Job Platform</p>
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="Menu">

                        <?php
                            if($this->session->userdata('usersess')) {
                                $usersess = $this->session->userdata('usersess');
                        ?>
                                <ul class="nav navbar-nav navbar-left">
                                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>about">About us</a></li>
                                    <li><a href="<?php echo base_url(); ?>jobs/nearby">Jobs </a></li>
                                    <li><a href="<?php echo base_url(); ?>blogs">Blogs </a></li>
                                    <li><a href="<?php echo base_url();?>faq">FAQs </a></li>
                                    <li><a href="<?php echo base_url();?>how_it_works">How it Works </a></li>
                                    <li><a href="<?php echo base_url();?>contact">Contact us</a></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="<?php echo base_url();?>user/homeafterlogin"><?php echo $usersess['name']; ?></a></li>
                                    <li><a href="<?php echo base_url();?>dashboard/logout">Logout</a></li> 
                                </ul>
                        <?php
                            } else {
                        ?>
                                <ul class="nav navbar-nav navbar-left">
                                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>about">About us</a></li>
                                    <li><a href="<?php echo base_url(); ?>jobs/nearby">Jobs </a></li>
                                    <li><a href="<?php echo base_url(); ?>blogs">Blogs </a></li>
                                    <li><a href="<?php echo base_url();?>faq">FAQs </a></li> 
                                    <li><a href="<?php echo base_url();?>how_it_works">How it Works </a></li>
                                    <li><a href="<?php echo base_url();?>contact">Contact us</a></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="<?php echo base_url();?>recruiter/" target="_blank">Recruiter's Portal</a></li> 
                                    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModalCenter4">Jobseekers Log In</a></li>
                                </ul>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </nav>
        </div>
    </header>

<input type="hidden" name="cur_lat" id="cur_lat" value="">
<input type="hidden" name="cur_long" id="cur_long" value="">
<?php
    $this->session->set_userdata('previous_url', current_url());
?>