<?php
	$userSess = $this->session->userdata('usersess');
	$listingTypeFun = "jobs";

	if($extra['keyword'] == 2) {
?>
	
	<div id="WorkHome" class="PopularTabs PopularTabs2 tab-pane fade in active">
        <h1>Work from Home Jobs  <?php if(count($popular) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/work_from_home">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
        
        <?php
            if(!empty($popular)) {
        ?>
        <div class="owl-carousel owl-theme JobSlider">
        <?php
                $f=1;
                foreach($popular as $workFromhomeJob) {
                    if($workFromhomeJob['mode'] == "call" || $workFromhomeJob['mode'] == "Call") {
                        $modeText = "OVER THE PHONE INTERVIEW";
                    } else if($workFromhomeJob['mode'] == "Walk-in") {
                        $modeText = "WALK IN INTERVIEW";
                    } else if($workFromhomeJob['mode'] == "Instant screening") {
                        $modeText = "INSTANT SCREENING";
                    }
                    if($f<=6) {
        ?>
                    <div class="item">
                        <div class="JobsBox">
                            <figure>
                                <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); }?>">
                                    <?php
                                        if(!empty($workFromhomeJob['job_image'])) {
                                    ?>
                                            <img src="<?php echo $workFromhomeJob['job_image']; ?>">
                                    <?php
                                        } else {
                                    ?>
                                            <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                    <?php
                                        }
                                    ?>
                                    <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                    
                                </a>
                            </figure>
                            <figcaption>
                                <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $workFromhomeJob['distance']; ?> KM</span>
                                <h3>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); }?>">
                                        <?php echo $workFromhomeJob['job_title']; ?> | <span class="salaryColor"><?php echo $workFromhomeJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                    </a>
                                </h3>
                                <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($workFromhomeJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($workFromhomeJob['comapnyId']); }?>"><?php echo $workFromhomeJob['companyName']; ?></a></h4>
                                
                                <h5>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($workFromhomeJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($workFromhomeJob['recruiter_id']); }?>">

                                        <?php echo $workFromhomeJob['cname']; ?>
                                    </a>
                                </h5>
                                
                                <p><?php echo substr($workFromhomeJob['jobPitch'], 0,45).'...'; ?></p> 
                                <ul>
                                    <?php
                                        if(!empty($workFromhomeJob['toppicks1'])) {
                                            echo $getToppickByFunction = getToppickFunction($workFromhomeJob['toppicks1']);
                                        }
                                        if(!empty($workFromhomeJob['toppicks2'])) {
                                            echo $getToppickByFunction = getToppickFunction($workFromhomeJob['toppicks2']);
                                        }
                                        if(!empty($workFromhomeJob['toppicks3'])) {
                                            echo $getToppickByFunction = getToppickFunction($workFromhomeJob['toppicks3']);
                                        }
                                    ?>
                                </ul>
                                <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                            </figcaption>
                        </div>
                    </div>
        <?php
                    } else {
                        break;
                    }
                    $f++;
                }
        ?>
        </div>
        <?php
            } else {
                echo "<p class='nofound'> No job found </p>";
            }
        ?>
    </div>

<?php
	} else if($extra['keyword'] == 3) {
?>
	
	<div id="Screening" class="PopularTabs PopularTabs3 tab-pane fade in active">
        <h1>Jobs with Instant Screening   <?php if(count($popular) > 0) { ?>  <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/instant_screening">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
        
        <?php
            if(!empty($popular)) {
        ?>
        <div class="owl-carousel owl-theme JobSlider">
        <?php
                $k=1;
                foreach($popular as $instantJob) {
                    if($instantJob['mode'] == "call" || $instantJob['mode'] == "Call") {
                        $modeText = "OVER THE PHONE INTERVIEW";
                    } else if($instantJob['mode'] == "Walk-in") {
                        $modeText = "WALK IN INTERVIEW";
                    } else if($instantJob['mode'] == "Instant screening") {
                        $modeText = "INSTANT SCREENING";
                    }
                    if($k<=6) {
        ?>
                    <div class="item">
                        <div class="JobsBox">
                            <figure>
                                <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); }?>">
                                    <?php
                                        if(!empty($instantJob['job_image'])) {
                                    ?>
                                            <img src="<?php echo $instantJob['job_image']; ?>">
                                    <?php
                                        } else {
                                    ?>
                                            <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                    <?php
                                        }
                                    ?>
                                    <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                    
                                </a>
                            </figure>
                            <figcaption>
                                <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $instantJob['distance']; ?> KM</span>
                                <h3>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); }?>">
                                        <?php echo $instantJob['job_title']; ?> | <span class="salaryColor"><?php echo $instantJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                    </a>
                                </h3>
                                <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($instantJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($instantJob['comapnyId']); }?>"><?php echo $instantJob['companyName']; ?></a></h4>
                                
                                <h5>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($instantJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($instantJob['recruiter_id']); }?>">
                                        <?php echo $instantJob['cname']; ?>
                                    </a>
                                </h5>
                                
                                <p><?php echo substr($instantJob['jobPitch'], 0,45).'...'; ?></p> 
                                <ul>
                                    <?php
                                        if(!empty($instantJob['toppicks1'])) {
                                            echo $getToppickByFunction = getToppickFunction($instantJob['toppicks1']);
                                        }
                                        if(!empty($instantJob['toppicks2'])) {
                                            echo $getToppickByFunction = getToppickFunction($instantJob['toppicks2']);
                                        }
                                        if(!empty($instantJob['toppicks3'])) {
                                            echo $getToppickByFunction = getToppickFunction($instantJob['toppicks3']);
                                        }
                                    ?>
                                </ul>
                                <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                            </figcaption>
                        </div>
                    </div>
        <?php
                    } else {
                        break;
                    }
                    $k++;
                }
        ?>
        </div>
        <?php
            } else {
                    echo "<p class='nofound'> No job found </p>";
                }
        ?>
    </div> 

<?php
	} else if($extra['keyword'] == 4) {
?>
		<div id="HMO" class="PopularTabs PopularTabs4 tab-pane fade in active">
            <h1>Jobs with Day 1 HMO  <?php if(count($popular) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hmo">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
            <?php
                if(!empty($popular)) {
            ?>
            <div class="owl-carousel owl-theme JobSlider">
            <?php
                    $g=1;
                    foreach($popular as $day1hmoJob) {
                        if($day1hmoJob['mode'] == "call" || $day1hmoJob['mode'] == "Call") {
                            $modeText = "OVER THE PHONE INTERVIEW";
                        } else if($day1hmoJob['mode'] == "Walk-in") {
                            $modeText = "WALK IN INTERVIEW";
                        } else if($day1hmoJob['mode'] == "Instant screening") {
                            $modeText = "INSTANT SCREENING";
                        }
                        if($g<=6) {
            ?>
                        <div class="item">
                            <div class="JobsBox">
                                <figure>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); }?>">
                                        <?php
                                            if(!empty($day1hmoJob['job_image'])) {
                                        ?>
                                                <img src="<?php echo $day1hmoJob['job_image']; ?>">
                                        <?php
                                            } else {
                                        ?>
                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                        <?php
                                            }
                                        ?>
                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                        
                                    </a>
                                </figure>
                                <figcaption>
                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $day1hmoJob['distance']; ?> KM</span>
                                    <h3>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); }?>">
                                            <?php echo $day1hmoJob['job_title']; ?> | <span class="salaryColor"><?php echo $day1hmoJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                        </a>
                                    </h3>
                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($day1hmoJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($day1hmoJob['comapnyId']); }?>"><?php echo $day1hmoJob['companyName']; ?></a></h4>
                                    
                                    <h5>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($day1hmoJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($day1hmoJob['recruiter_id']); }?>">

                                            <?php echo $day1hmoJob['cname']; ?>
                                        </a>
                                    </h5>
                                    
                                    <p><?php echo substr($day1hmoJob['jobPitch'], 0,45).'...'; ?></p> 
                                    <ul>
                                        <?php
                                            if(!empty($day1hmoJob['toppicks1'])) {
                                                echo $getToppickByFunction = getToppickFunction($day1hmoJob['toppicks1']);
                                            }
                                            if(!empty($day1hmoJob['toppicks2'])) {
                                                echo $getToppickByFunction = getToppickFunction($day1hmoJob['toppicks2']);
                                            }
                                            if(!empty($day1hmoJob['toppicks3'])) {
                                                echo $getToppickByFunction = getToppickFunction($day1hmoJob['toppicks3']);
                                            }
                                        ?>
                                    </ul>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                </figcaption>
                            </div>
                        </div>
            <?php
                        } else {
                            break;
                        }
                        $g++;
                    }
            ?>
            </div>
            <?php
                } else {
                    echo "<p class='nofound'> No job found </p>";
                }
            ?>
        </div>
<?php
	} else if($extra['keyword'] == 5) {
?>
		<div id="Food" class="PopularTabs PopularTabs5 tab-pane fade in active">
            <h1>Jobs with Free Food  <?php if(count($popular) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/free_food">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
            
            <?php
                if(!empty($popular)) {
            ?>
            <div class="owl-carousel owl-theme JobSlider">
            <?php
                    $h=1;
                    foreach($popular as $freefoodJob) {
                        if($freefoodJob['mode'] == "call" || $freefoodJob['mode'] == "Call") {
                            $modeText = "OVER THE PHONE INTERVIEW";
                        } else if($freefoodJob['mode'] == "Walk-in") {
                            $modeText = "WALK IN INTERVIEW";
                        } else if($freefoodJob['mode'] == "Instant screening") {
                            $modeText = "INSTANT SCREENING";
                        }
                        if($h<=6) {
            ?>
                        <div class="item">
                            <div class="JobsBox">
                                <figure>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); }?>">
                                        <?php
                                            if(!empty($freefoodJob['job_image'])) {
                                        ?>
                                                <img src="<?php echo $freefoodJob['job_image']; ?>">
                                        <?php
                                            } else {
                                        ?>
                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                        <?php
                                            }
                                        ?>
                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                        
                                    </a>
                                </figure>
                                <figcaption>
                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $freefoodJob['distance']; ?> KM</span>
                                    <h3>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); }?>">
                                            <?php echo $freefoodJob['job_title']; ?> | <span class="salaryColor"><?php echo $freefoodJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                        </a>
                                    </h3>
                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($freefoodJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($freefoodJob['comapnyId']); }?>"><?php echo $freefoodJob['companyName']; ?></a></h4>
                                    
                                    <h5>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($freefoodJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($freefoodJob['recruiter_id']); }?>">

                                            <?php echo $freefoodJob['cname']; ?>
                                        </a>
                                    </h5>
                                    
                                    <p><?php echo substr($freefoodJob['jobPitch'], 0,45).'...'; ?></p> 
                                    <ul>
                                        <?php
                                            if(!empty($freefoodJob['toppicks1'])) {
                                                echo $getToppickByFunction = getToppickFunction($freefoodJob['toppicks1']);
                                            }
                                            if(!empty($freefoodJob['toppicks2'])) {
                                                echo $getToppickByFunction = getToppickFunction($freefoodJob['toppicks2']);
                                            }
                                            if(!empty($freefoodJob['toppicks3'])) {
                                                echo $getToppickByFunction = getToppickFunction($freefoodJob['toppicks3']);
                                            }
                                        ?>
                                    </ul>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                </figcaption>
                            </div>
                        </div>
            <?php
                        } else {
                            break;
                        }
                        $h++;
                    }
            ?>
            </div>
            <?php
                } else {
                    echo "<p class='nofound'> No job found </p>";
                }
            ?>
        </div>

<?php
	} else if($extra['keyword'] == 6) {
?>
		<div id="IT" class="PopularTabs PopularTabs6 tab-pane fade in active">
            <h1>IT Jobs  <?php if(count($popular) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/information_technology">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
            
            <?php
                if(!empty($popular)) {
            ?>
            <div class="owl-carousel owl-theme JobSlider">
            <?php
                    $i=1;
                    foreach($popular as $itJob) {
                        if($itJob['mode'] == "call" || $itJob['mode'] == "Call") {
                            $modeText = "OVER THE PHONE INTERVIEW";
                        } else if($itJob['mode'] == "Walk-in") {
                            $modeText = "WALK IN INTERVIEW";
                        } else if($itJob['mode'] == "Instant screening") {
                            $modeText = "INSTANT SCREENING";
                        }
                        if($i<=6) {
            ?>
                        <div class="item">
                            <div class="JobsBox">
                                <figure>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); }?>">
                                        <?php
                                            if(!empty($itJob['job_image'])) {
                                        ?>
                                                <img src="<?php echo $itJob['job_image']; ?>">
                                        <?php
                                            } else {
                                        ?>
                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                        <?php
                                            }
                                        ?>
                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                        
                                    </a>
                                </figure>
                                <figcaption>
                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $itJob['distance']; ?> KM</span>
                                    <h3>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); }?>">
                                            <?php echo $itJob['job_title']; ?> | <span class="salaryColor"><?php echo $itJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?> 
                                        </a>
                                    </h3>
                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($itJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($itJob['comapnyId']); }?>"><?php echo $itJob['companyName']; ?></a></h4>
                                    
                                    <h5>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($itJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($itJob['recruiter_id']); }?>">

                                            <?php echo $itJob['cname']; ?>
                                        </a>
                                    </h5>
                                    
                                    <p><?php echo substr($itJob['jobPitch'], 0,45).'...'; ?></p> 
                                    <ul>
                                        <?php
                                            if(!empty($itJob['toppicks1'])) {
                                                echo $getToppickByFunction = getToppickFunction($itJob['toppicks1']);
                                            }
                                            if(!empty($itJob['toppicks2'])) {
                                                echo $getToppickByFunction = getToppickFunction($itJob['toppicks2']);
                                            }
                                            if(!empty($itJob['toppicks3'])) {
                                                echo $getToppickByFunction = getToppickFunction($itJob['toppicks3']);
                                            }
                                        ?>
                                    </ul>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                </figcaption>
                            </div>
                        </div>
            <?php
                        } else {
                            break;
                        }
                        $i++;
                    }
            ?>
            </div>
            <?php
                } else {
                    echo "<p class='nofound'> No job found </p>";
                }
            ?>
        </div>
<?php
	} else if($extra['keyword'] == 7) {
?>
		<div id="Leadership" class="PopularTabs PopularTabs7 tab-pane fade in active">
            <h1>Leadership Jobs  <?php if(count($popular) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/leadership">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
            
            <?php
                if(!empty($popular)) {
            ?>
            <div class="owl-carousel owl-theme JobSlider">
            <?php
                    $j=1;
                    foreach($popular as $leadershipJob) {
                        if($leadershipJob['mode'] == "call" || $leadershipJob['mode'] == "Call") {
                            $modeText = "OVER THE PHONE INTERVIEW";
                        } else if($leadershipJob['mode'] == "Walk-in") {
                            $modeText = "WALK IN INTERVIEW";
                        } else if($leadershipJob['mode'] == "Instant screening") {
                            $modeText = "INSTANT SCREENING";
                        }
                        if($j<=6) {
            ?>
                        <div class="item">
                            <div class="JobsBox">
                                <figure>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); }?>">
                                        <?php
                                            if(!empty($leadershipJob['job_image'])) {
                                        ?>
                                                <img src="<?php echo $leadershipJob['job_image']; ?>">
                                        <?php
                                            } else {
                                        ?>
                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                        <?php
                                            }
                                        ?>
                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                        
                                    </a>
                                </figure>
                                <figcaption>
                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $leadershipJob['distance']; ?> KM</span>
                                    <h3>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); }?>">
                                            <?php echo $leadershipJob['job_title']; ?> | <span class="salaryColor"><?php echo $leadershipJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                        </a>
                                    </h3>
                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($leadershipJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($leadershipJob['comapnyId']); }?>"><?php echo $leadershipJob['companyName']; ?></a></h4>
                                    <h5>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($leadershipJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($leadershipJob['recruiter_id']); }?>">

                                            <?php echo $leadershipJob['cname']; ?>
                                        </a>
                                    </h5>
                                    <p><?php echo substr($leadershipJob['jobPitch'], 0,45).'...'; ?></p> 
                                    <ul>
                                        <?php
                                            if(!empty($leadershipJob['toppicks1'])) {
                                                echo $getToppickByFunction = getToppickFunction($leadershipJob['toppicks1']);
                                            }
                                            if(!empty($leadershipJob['toppicks2'])) {
                                                echo $getToppickByFunction = getToppickFunction($leadershipJob['toppicks2']);
                                            }
                                            if(!empty($leadershipJob['toppicks3'])) {
                                                echo $getToppickByFunction = getToppickFunction($leadershipJob['toppicks3']);
                                            }
                                        ?>
                                    </ul>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                </figcaption>
                            </div>
                        </div>
            <?php
                        } else {
                            break;
                        }
                        $j++;
                    }
            ?>
            </div>
            <?php
                } else {
                    echo "<p class='nofound'> No job found </p>";
                }
            ?>
        </div>
<?php
	} else if($extra['keyword'] == 8) {
?>
		<div id="Jobs" class="PopularTabs PopularTabs8 tab-pane fade in active">
            <h1>Jobs with 14th Month Pay  <?php if(count($popular) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/14_month_pay">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
            
            <?php
                if(!empty($popular)) {
            ?>
            <div class="owl-carousel owl-theme JobSlider">
            <?php
                    $f=1;
                    foreach($popular as $monthpayJob) {
                        if($monthpayJob['mode'] == "call" || $monthpayJob['mode'] == "Call") {
                            $modeText = "OVER THE PHONE INTERVIEW";
                        } else if($monthpayJob['mode'] == "Walk-in") {
                            $modeText = "WALK IN INTERVIEW";
                        } else if($monthpayJob['mode'] == "Instant screening") {
                            $modeText = "INSTANT SCREENING";
                        }
                        if($f<=6) {
            ?>
                        <div class="item">
                            <div class="JobsBox">
                                <figure>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); }?>">
                                        <?php
                                            if(!empty($monthpayJob['job_image'])) {
                                        ?>
                                                <img src="<?php echo $monthpayJob['job_image']; ?>">
                                        <?php
                                            } else {
                                        ?>
                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                        <?php
                                            }
                                        ?>
                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                       
                                    </a>
                                </figure>
                                <figcaption>
                                     <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $monthpayJob['distance']; ?> KM</span>
                                    <h3>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); }?>">
                                            <?php echo $monthpayJob['job_title']; ?> | <span class="salaryColor"><?php echo $monthpayJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                        </a>
                                    </h3>
                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($monthpayJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($monthpayJob['comapnyId']); }?>"><?php echo $monthpayJob['companyName']; ?></a></h4>
                                    
                                    <h5>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($monthpayJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($monthpayJob['recruiter_id']); }?>">

                                            <?php echo $monthpayJob['cname']; ?>
                                        </a>
                                    </h5>
                                    
                                    <p><?php echo substr($monthpayJob['jobPitch'], 0,45).'...'; ?></p> 
                                    <ul>
                                        <?php
                                            if(!empty($monthpayJob['toppicks1'])) {
                                                echo $getToppickByFunction = getToppickFunction($monthpayJob['toppicks1']);
                                            }
                                            if(!empty($monthpayJob['toppicks2'])) {
                                                echo $getToppickByFunction = getToppickFunction($monthpayJob['toppicks2']);
                                            }
                                            if(!empty($monthpayJob['toppicks3'])) {
                                                echo $getToppickByFunction = getToppickFunction($monthpayJob['toppicks3']);
                                            }
                                        ?>
                                    </ul>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                </figcaption>
                            </div>
                        </div>
            <?php
                        } else {
                            break;
                        }
                        $f++;
                    }
            ?>
            </div>
            <?php
                } else {
                    echo "<p class='nofound'> No job found </p>";
                }
            ?>
        </div>
<?php
	} else if($extra['keyword'] == 1) {
?>
		<div id="Hot" class="PopularTabs PopularTabs1 tab-pane fade in active">
            <h1>Hot Jobs   <?php if(count($popular) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hotjob">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
            <?php
                if(!empty($popular)) {
            ?>
            <div class="owl-carousel owl-theme JobSlider">
            <?php
                    $e=1;
                    foreach($popular as $hotJob) {
                        if($hotJob['mode'] == "call" || $hotJob['mode'] == "Call") {
                            $modeText = "OVER THE PHONE INTERVIEW";
                        } else if($hotJob['mode'] == "Walk-in") {
                            $modeText = "WALK IN INTERVIEW";
                        } else if($hotJob['mode'] == "Instant screening") {
                            $modeText = "INSTANT SCREENING";
                        }
                        if($e<=6){
            ?>
                        <div class="item">
                            <div class="JobsBox">
                                <figure>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>">
                                        <?php
                                            if(!empty($hotJob['job_image'])) {
                                        ?>
                                                <img src="<?php echo $hotJob['job_image']; ?>">
                                        <?php
                                            } else {
                                        ?>
                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                        <?php
                                            }
                                        ?>
                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                        <!-- <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotJob['distance']; ?> KM</span> -->
                                    </a>
                                </figure>
                                <figcaption>
                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotJob['distance']; ?> KM</span>
                                    <h3>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>">
                                            <?php echo $hotJob['job_title']; ?> | <span class="salaryColor"><?php echo $hotJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                        </a>
                                    </h3>
                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($hotJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($hotJob['comapnyId']); }?>"><?php echo $hotJob['companyName']; ?></a></h4>
                                    
                                    <h5>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($hotJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($hotJob['recruiter_id']); }?>">

                                            <?php echo $hotJob['cname']; ?>
                                        </a>
                                    </h5>
                                    
                                    <p><?php echo substr($hotJob['jobPitch'], 0,45).'...'; ?></p> 
                                    <ul>
                                        <?php
                                            if(!empty($hotJob['toppicks1'])) {
                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks1']);
                                            }
                                            if(!empty($hotJob['toppicks2'])) {
                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks2']);
                                            }
                                            if(!empty($hotJob['toppicks3'])) {
                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks3']);
                                            }
                                        ?>
                                    </ul>
                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                </figcaption>
                            </div>
                        </div>
            <?php
                        } else {
                            break;
                        }
                        $e++;
                    }
            ?>    
            </div>
            <?php
                } else {
                    echo "<p class='nofound'> No job found </p>";
                }
            ?>
        </div>
<?php
	}
?>


<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }
?>