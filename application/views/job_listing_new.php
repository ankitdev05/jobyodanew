<?php
   include_once('header2.php');
   $this->session->set_userdata('previous_url_again', current_url());
   // if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

   //  } else {
   //      $link = "https";
   //      $link .= "://";
   //      $link .= $_SERVER['HTTP_HOST'];
   //      $link .= $_SERVER['REQUEST_URI'];
   //      redirect($link);
   //  }
   $userSess = $this->session->userdata('usersess');
   if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
        $listingTypeFun = "jobs";
    } else {
        $listingTypeFun = "jobs";
    }
?>
<style type="text/css">
span.Distance{float:right;background-color:#00a94f;color:#fff;padding:4px 8px;border-radius:5px;font-size:11px;font-family:Roboto;margin:0 -5px 0 0}
.InstantBox{padding:30px}
.InstantBox button{position:absolute;top:-15px;right:-15px;width:30px;height:30px;background-color:#00a94f;opacity:1;text-shadow:none;color:#fff;border-radius:50%;font-size:16px}
.InstantBox h5{font-family:Roboto;font-size:14px;font-weight:400;line-height:25px;text-align:center;color:#000;margin:0 0 10px 0}
.InstantBox h5 span{display:block;font-weight:500}
.InstantBox p{margin:0;text-align:center}
.InstantBox p a{background-color:#00a94f;color:#fff;padding:7px 25px;border-radius:5px;font-family:Roboto;font-size:14px;display:inline-block;box-shadow:none}
.InstantBox p a:hover{background-color:#fbaf31}
.JobListingBox aside{ margin: 0 0 30px 0 }

.datepicker td, 
.datepicker th{ font-size: 17px !important }

.schedulejobgs .forminputspswd p { margin-bottom: 16px; }

</style>
    <section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1> 
                <?php 
                  $getSegment = $this->uri->segment(2);
                  if($getSegment == "nearby") {
                      echo "Jobs with Nearby";
                  } else if($getSegment == "hotjob") {
                     echo "Hot Jobs";
                  
                  } else if($getSegment == "instant_screening") {
                     echo "Jobs with Instant screening";
                  
                  } else if($getSegment == "toppicks") {

                      $getSegmentID = base64_decode($this->uri->segment(3));
                      if($getSegmentID == 1) {
                        echo "Jobs with Joining Bonus";
                      } else if($getSegmentID == 2) {
                        echo "Jobs with Free Food";
                      } else if($getSegmentID == 3) {
                        echo "Jobs with Day 1 HMO";
                      } else if($getSegmentID == 4) {
                        echo "Jobs with Day 1 HMO for Dependent";
                      } else if($getSegmentID == 5) {
                        echo "Day Shift Jobs";
                      } else if($getSegmentID == 6) {
                        echo "Jobs with 14th Month Pay";
                      } else if($getSegmentID == 7) {
                        echo "Work From Home Jobs";
                      } 
                  } else if($getSegment == "Allowance") {
                     echo "Jobs with Allowance";
                  
                  } else if($getSegment == "leadership") {
                     echo "Jobs with Leadership";
                  
                  } else if($getSegment == "information_technology") {
                     echo "Jobs with Information technology";
                  
                  } else if($getSegment == "city") {
                     echo "Jobs with City";
                  
                  } else {
                      echo "Job Listings";
                  }

                ?>
            </h1>
            <!-- <h2>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.</h2> -->
        </div>
    </section>

    <section>
        <div class="JobListingArea">
            <div class="container">
                <div class="col-sm-9">
                    <div class="JobListingLeft">
                        <div class="JobListingBox">
                        <?php 
                            if(!empty($hotjobss)) {
                                $x=1;
                               foreach($hotjobss as $hotjobssData) {   
                        ?>
                            <aside>
                                <figure>
                                    <?php
                                        if($this->session->userdata('usersess')) {
                                            if($jobListing['savedjob']==1) {
                                                $title="Saved";
                                    ?>
                                                <span id="test3<?php echo $jobListing['jobpost_id'];?>" onclick="savedjob('<?php echo $jobListing['jobpost_id']?>')" class="Wishlist"> <i class="fa fa-heart" title="<?php echo $title;?>"></i> </span>
                                    <?php
                                            } if($jobListing['savedjob']==0) {
                                    ?>
                                                <span id="test4<?php echo $jobListing['jobpost_id'];?>" onclick="savedjob('<?php echo $jobListing['jobpost_id']?>')" class="Wishlist"> <i class="fa fa-heart" title="<?php echo $title;?>" style="color:#b5b5b5;"></i> </span>    
                                    <?php
                                            }
                                        }
                                    ?>
                                    <a href="<?php  if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); }?>">
                                    <?php
                                        if(!empty($hotjobssData['job_image'])) {
                                    ?>
                                            <img src="<?php echo $hotjobssData['job_image'];?>">
                                    <?php
                                        } else {
                                    ?>
                                            <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                    <?php
                                        }
                                    ?>
                                        
                                    </a>
                                    
                                </figure>
                                <figcaption>

                                  <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotjobssData['distance'];?> KM</span>
                                  
                                    <h1>
                                        <a href="<?php  if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotjobssData['jobpost_id']); }?>"><?php echo $hotjobssData['job_title'];?></a>
                                    </h1>
                                    
                                    <h2>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($hotjobssData['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($hotjobssData['comapnyId']); }?>"><?php echo $hotjobssData['companyName']; ?></a>
                                    </h2>
                                    
                                    <h3>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($hotjobssData['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($hotjobssData['recruiter_id']); }?>"> <?php echo $hotjobssData['cname'];?></a>
                                    </h3>
                                    
                                    <h4>
                                        <span><i class="fa fa-map" aria-hidden="true"></i></span>
                                        <?php echo $hotjobssData['companyAddress']; ?>
                                    </h4>
                                    
                                    <h5>
                                        <?php echo substr($hotjobssData['jobPitch'],0, 100); ?>
                                    </h5>

                                    <ul>
                                        <?php
                                            if(!empty($hotjobssData['toppicks1'])) {
                                                echo $getToppickByFunction = getToppickFunction($hotjobssData['toppicks1']);
                                            }
                                            if(!empty($hotjobssData['toppicks2'])) {
                                                echo $getToppickByFunction = getToppickFunction($hotjobssData['toppicks2']);
                                            }
                                            if(!empty($hotjobssData['toppicks3'])) {
                                                echo $getToppickByFunction = getToppickFunction($hotjobssData['toppicks3']);
                                            }
                                        ?>
                                    </ul>
                                
                                    <p>
                                    <?php
                                        if($this->session->userdata('usersess')) {

                                            if($hotjobssData['mode'] == "Instant screening") {
                                    ?>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenterInstant<?php echo $x;?>">Apply <?php if($hotjobssData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $hotjobssData['mode']; } ?>  </a>
                                    <?php
                                            } else {
                                    ?>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenterb<?php echo $x;?>">Apply <?php if($hotjobssData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $hotjobssData['mode']; } ?>  </a>
                                    <?php
                                            }

                                        } else {
                                    ?>
                                         <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter4">Apply <?php if($hotjobssData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'as '. $hotjobssData['mode']; } ?>  </a>
                                    <?php
                                        }
                                    ?>
                                    </p>
                                </figcaption>
                            </aside>
                        <?php
                            if($this->session->userdata('usersess')) {
                               $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($hotjobssData['comapnyId']);
                               $timeFrom = $recruiterdetail[0]['from_time'];
                               $timeFromm = date('H:i', strtotime($timeFrom));
                               $timeTo = $recruiterdetail[0]['to_time'];
                               $timeToo = date('H:i', strtotime($timeTo));
                        ?>
                            <div class="modal fade" id="exampleModalCenterInstant<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                 <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content" style="box-shadow: none;">

                                      <div class="InstantBox">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                          </button>

                                          <!-- <h5>You will now be redirected to the Instant Assessment page. Your time invested now will speed up the hiring process later. JobYoDA wishes you all the best! Remember, 
                                          <span>"Every pro was once an amature. Every expert was once a beginner. So dream big and start now".</span></h5> -->
                                          <h5>
                                            This is far easier than driving around the city applying right!? You will now begin the Instant Assessment of <?php echo $hotjobssData['companyName']; ?>. They are looking for people like you but they will only know about you once you finish the assessment. Those who finish the assessment quickly, are more likely to get hired!  
                                          </h5>
                                          <p> Best of luck from the JobYoDA team! </p>
                                          
                                          <p>
                                            <a class="greenbtn" onclick="instantfunction('<?php echo $hotjobssData['jobpost_id']; ?>', '<?php echo $hotjobssData['modeurl']; ?>')" href="javascript:void(0)"> Begin Screening </a>
                                          </p>
                                      </div>
                                    
                                    </div>
                                 </div>
                            </div>

                            <div class="modal fade" id="exampleModalCenterb<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                 <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">×</span>
                                          </button>
                                       </div>
                                       <div class="modal-body">
                                          <div class="formmidaress modpassfull">
                                             <div class="filldetails">
                                                <div class="addupdatecent">
                                                   <form method="post" action="<?php echo base_url();?>dashboard/resumeUploadListing" enctype="multipart/form-data">
                                                      <div class="profileupload">
                                                         <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png" style="width:50px; height:auto;">
                                                      </div>
                                                      <?php if (!empty($checkResume[0]['resume'])) { ?>
                                                      <p>Would you like to update Your Resume?</p>
                                                      <?php }else{?>
                                                      <p>Please add your Resume</p>
                                                      <?php }?>
                                                      <div class="statsusdd">
                                                         <p class="norm" class="close" data-toggle="modal" data-target="#exampleModalCenterdd<?php echo $x;?>" data-dismiss="modal">No</p>
                                                         <input type="hidden" name="type" value="<?php echo $x;?>">
                                                         <div class="shwnbts">
                                                            <input name="resumeFile" id="file<?php echo $x;?>" type="file" class="yesuplds" onchange="getFilename('<?php echo $x ?>')">
                                                            <?php if (!empty($checkResume[0]['resume'])) { ?>
                                                            <p class="btns-yes">Yes</p>
                                                            <?php }else{?>
                                                            <p class="btns-yes">Upload</p>
                                                            <?php }?>
                                                         </div>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                             <div class="clear"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>


                              <div class="modal fade" id="exampleModalCenterdd<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                               <div class="modal-dialog modal-dialog-centered" role="document">
                                  <div class="modal-content">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                        </button>
                                     </div>
                                     <div class="modal-body">
                                        <div class="formmidaress modpassfull">
                                           <div class="filldetails">
                                              <form id="formschedule<?php echo $hotjobssData['jobpost_id']?>">
                                                 <div class="schedulejobgs">
                                                    <h6>Schedule interview</h6>
                                                    <p>Select Date & Time</p>
                                                    <div class="forminputspswd">
                                                       <p>Date</p>
                                                       <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker12<?php echo $hotjobssData['jobpost_id']?>" placeholder="mm/dd/yy" onchange="getdaynamee('<?php echo $hotjobssData['jobpost_id']?>')"  required="required" autocomplete="off">
                                                       <input type="hidden" id="dayfrommm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                       <input type="hidden" id="daytooo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                                                       <input type="hidden" id="timefrommm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                       <input type="hidden" id="timetooo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                       <span id="setval12<?php echo $hotjobssData['jobpost_id']?>" style="color:red"></span>
                                                       <i class="fa fa-calendar-alt fieldicons"></i>
                                                    </div>
                                                    <div class="forminputspswd">
                                                       <p>Time</p>
                                                       <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee('<?php echo $hotjobssData['jobpost_id']?>')" id="datepicker13<?php echo $hotjobssData['jobpost_id']?>"  data-format="hh:mm:ss" required="required"> 
                                                       <span id="setvall12<?php echo $hotjobssData['jobpost_id']?>" style="color:red"></span>  
                                                       <i class="fa fa-clock"></i>
                                                    </div>
                                                    <div class="statsusdd">
                                                       <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                       <input type="hidden" name="listing" value="<?php echo $hotjobssData['jobpost_id'];?>">
                                                       <input type="hidden" name="type" value="<?php echo $x;?>">
                                                       <button type="button" class="updt" onclick='scheduleclick("<?php echo $hotjobssData['jobpost_id']?>", "<?php echo $x;?>")' id="schedulees<?php echo $hotjobssData['jobpost_id']?>" disabled>Schedule & Apply</button>
                                                    </div>
                                                 </div>
                                              </form>
                                           </div>
                                           <div class="clear"></div>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                              </div>

                              <div class="modal fade" id="exampleModalCenterc<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                 <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">×</span>
                                          </button>
                                       </div>
                                       <div class="modal-body">
                                          <div class="formmidaress modpassfull">
                                             <div class="filldetails">
                                                <form id="formschedulemore<?php echo $hotjobssData['jobpost_id']?>">
                                                   <div class="schedulejobgs">
                                                      <h6>Schedule interview</h6>
                                                      <p>Selected Date & Time</p>
                                                      <div class="forminputspswd">
                                                         <p>Date</p>
                                                         <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker1<?php echo $hotjobssData['jobpost_id']?>" autocomplete="off" placeholder="mm/dd/yy" onchange="getdayname('<?php echo $hotjobssData['jobpost_id']?>')"  required="required">
                                                         <span id="setval<?php echo $hotjobssData['jobpost_id']?>" style="color:red"></span>
                                                         <input type="hidden" id="dayfromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                         <input type="hidden" id="daytoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                                                         <input type="hidden" id="timefromm<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                         <input type="hidden" id="timetoo<?php echo $hotjobssData['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                         <i class="fa fa-calendar-alt fieldicons"></i>
                                                      </div>
                                                      <div class="forminputspswd">
                                                         <p>Time</p>   
                                                         <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTime('<?php echo $hotjobssData['jobpost_id']?>')" id="datepicker1<?php echo $hotjobssData['jobpost_id']?>"  data-format="hh:mm:ss" required="required"> 
                                                         <span id="setvall<?php echo $hotjobssData['jobpost_id']?>" style="color:red"></span>
                                                         <i class="fa fa-clock"></i>
                                                      </div>
                                                      <div class="statsusdd">
                                                         <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                         <input type="hidden" name="listing" value="<?php echo $hotjobssData['jobpost_id'];?>">
                                                         <input type="hidden" name="type" value="<?php echo $x;?>">
                                                         <button type="button" class="updt" onclick='scheduleclickmore("<?php echo $hotjobssData['jobpost_id']?>", "<?php echo $x;?>")' id="schedule<?php echo $hotjobssData['jobpost_id']?>" disabled>Schedule & Apply</button>
                                                      </div>
                                                </form>
                                                </div> 
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                             </div>

                            <div class="modal fade" id="exampleModalCenter100<?php echo $hotjobssData['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header"> 
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form>
                                          <div class="addupdatecent">
                                             <img src="<?php echo base_url('webfiles/img/savedbighover.png')?>">
                                             <p class="jobsuccess<?php echo $hotjobssData['jobpost_id']?>">Job Applied Successfully</p>
                                             <div class="statsusdd">
                                                <p class="updt" data-dismiss="modal" aria-label="Close">Ok</p>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal fade" id="exampleModalCenter900<?php echo $hotjobssData['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header"> 
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form>
                                          <div class="addupdatecent">
                                             <img src="<?php echo base_url('webfiles/img/savedbighover.png')?>">
                                             <p class="jobsuccess1<?php echo $hotjobssData['jobpost_id']?>"></p>
                                             <div class="statsusdd">
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                        <?php
                            }
                        ?>
                        <?php
                                $x++;
                                }
                            }
                        ?>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3" style="position: sticky; top: 15px">
                    <div class="JobListingRight">
                        <figure>
                        <?php
                            if(!empty($ad_lists)) {
                        ?>
                            <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($ad_lists[0]['site_id']);} else { echo base_url();?>site_details/<?php echo base64_encode($ad_lists[0]['site_id']); }?>">
                                
                                <img src="<?php echo $ad_lists[0]['banner']; ?>">

                            </a>
                        <?php
                            }
                        ?>
                        </figure>
                        <aside>

                            <h3>Hiring for over <span style="color:#fbaf31"><?php echo $openings[0]['openings']; ?></span> BPO positions! </h3>
                            <h4>Best BPO jobs in the Philippines in one single site sorted by benefits and distance</h4>
                            <p><i class="fa fa-tags"></i> Trending Keywords : <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hotjob">Hot Jobs,</a> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/work_from_home">Work From Home Jobs,</a> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/14_month_pay">Jobs with 14th Month Pay,</a> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/day_shift">Day Shift Jobs</a></p>
                            <div class="DownloadApp">
                                <a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" class="download-btn" target="_blank">
                                    <span class="fa fa-apple"></span>
                                    <p>
                                        <small>Download On</small>
                                        <br>
                                        App Store
                                    </p>
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.jobyodamo" class="download-btn" target="_blank">
                                    <span class="fa fa-android"></span>
                                    <p>
                                        <small>Get It On</small>
                                        <br>
                                        Google Play
                                    </p>
                                </a>
                                <div class="clear"></div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
    include_once('footer1.php');
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script> 
<script type="text/javascript">
   $(function () {
      $(".datetimepicker1").datepicker({ 
       dateFormat: "dd/mm/yy", 
        yearRange: '1900:2020', 
        defaultDate: '',
        autoclose: true,
     })
   });
   $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
    
</script>

<script>
   function savedjob(id)
      {
        //alert(id);
         $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/savedjovData') ?>",
            'data' :'jobId='+id,
            'success':function(htmlres)
            {
               //alert(htmlres);return false;  
               if(htmlres !='')
               {
               if(htmlres == 1)
               {
                  $("#test4"+id).hide();
                  $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Saved" style="color:red;font-size: 24px;cursor:pointer;"></i>');
                 var msg="Job saved successfully";
     //alert(msg);
                 myFunction(msg);
                
                 
               }
               if(htmlres == 2)
               {
                 $("#test3"+id).hide();
                 $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Unsaved" style="color:#b5b5b5;font-size: 24px;cursor:pointer;"></i>');
                 //var msg="Job unsaved successfully";
    //alert(msg);
                 //myFunction(msg);
                  
               }
                if(htmlres == 3)
               {
                 $("#test4"+id).hide();
                 $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Saved" style="color:red;font-size: 24px;cursor:pointer;"></i>');
                 var msg="Job saved successfully";
     //alert(msg);
                 myFunction(msg);
               
   
               }
            }
            }
              
          });
      }
   
      function instantfunction(id, url) {
          $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/savedinstantData') ?>",
             'data' :'jobId='+id,
             'success':function(htmlres) {
                  $(".modal").modal('hide');
                 window.open(url, '_blank');
             }
         });
   }
   
   function myFunction(msg) {
    var x = document.getElementById("snackbar");
    x.className = "show";
    $("#snackbar").html('<div class="alert alert-success">'+msg+ '</div>');
    setTimeout(function(){ x.className = x.className.replace("show", "top"); }, 1000);
     }
</script>
<script type="text/javascript">
   function PdfImagesend(id)
   {
     //alert('hi');
     var image= $("#imagedata"+id).val();
       $.ajax({
              'type' :'POST',
              'url' :"<?php echo base_url('dashboard/fileInsert') ?>",
              'data' :'imagedata='+image,
              'success':function(htmlres)
              {
   
                if(htmlres==1)
                {
                 $("#exampleModalCenterdd"+id).modal('show');
                }
                if(htmlres==2)
                {
                  $("#exampleModalCenterdd"+id).modal('show');
                }
              }
        });
    }
</script>     
<script type="text/javascript">
       
      function getFilename(id)
      {
         var name = document.getElementById("file"+id).files[0].name;
         var filetype= $("#file"+id).val();
         var ext = filetype.split('.').pop();
         if(ext =="jpg" || ext =="jpeg" || ext =="png"){
           $("#setimgres"+id).html("<span style='color:red'>plese select file format(pdf/doc/docx)</span>");
           return false;
        } 
        else
        {
         $("#setimgres"+id).html("");
         var form_data = new FormData();
         form_data.append("file", document.getElementById('file'+id).files[0]);
         $.ajax({
           url:'<?php echo base_url('dashboard/imageUpload')?>',
           method:"POST",
           data: form_data,
           contentType: false,
           cache: false,
           processData: false,
           success:function(imagedata)
           { 
             $("#imagedata"+id).val(imagedata);
             $("#file"+id).val('');
             $("#setimgres"+id).html(imagedata);
             PdfImagesend(id);
           }
         });
        }
      }   
</script>
<?php
   if(isset($_GET["type"]) && $_GET["msg"] == "uploadcomplete") {
   ?>
<script type="text/javascript">
   $(window).on('load',function(){
        var idmodal = "#exampleModalCenterc<?php echo $_GET["type"]; ?>";
        $(idmodal).modal('show');
   });
</script>
<?php
   }
?>
</body>

<script>
  
  function scheduleclick(id, id1) {
      var getform = "#formschedule" + id;
      var successmsg = ".jobsuccess"+id;
      var successmsg1 = ".jobsuccess1"+id;
      var successmodal = "#exampleModalCenter100"+id;
      var errormodal = "#exampleModalCenter900"+id;
      var closeModal = "#exampleModalCenterc"+id1;

      $.ajax({
          'type' :'POST',
          'url' :"<?php echo base_url('dashboard/scheduled') ?>",
          'data' :$(getform).serialize(),
          success:function(response) {

            var jsonData = JSON.parse(response);
              
            if(jsonData.status == "SUCCESS") {
              $(successmsg).html(jsonData.message);
              $(successmodal).modal('show');
              $(closeModal).modal('hide');

            } else {
                $(successmsg1).html(jsonData.message);
                $(errormodal).modal('show');
                $(closeModal).modal('hide');
            }
          }
      });
  }

  function scheduleclickmore(id, id1) {
      var getform = "#formschedulemore" + id;
      var successmsg = ".jobsuccess"+id;
      var successmsg1 = ".jobsuccess1"+id;
      var successmodal = "#exampleModalCenter100"+id;
      var errormodal = "#exampleModalCenter900"+id;
      var closeModal = "#exampleModalCenterc"+id1;

      $.ajax({
          'type' :'POST',
          'url' :"<?php echo base_url('dashboard/scheduled') ?>",
          'data' :$(getform).serialize(),
          success:function(response) {

            var jsonData = JSON.parse(response);
              
            if(jsonData.status == "SUCCESS") {
              $(successmsg).html(jsonData.message);
              $(successmodal).modal('show');
              $(closeModal).modal('hide');

            } else {
                $(successmsg1).html(jsonData.message);
                $(errormodal).modal('show');
                $(closeModal).modal('hide');
            }
          }
      });
  }

</script>
<script type="text/javascript">
   $(".updt").on("click",function(){
         window.location = "<?php echo base_url(); ?>appliedjobs";
   });
</script>
<script>
   function getdayname(id)
   {
       var getdate = $("#datetimepicker1"+id).val();
       var dayfrom = $("#dayfromm"+id).val();
       var dayto   = $("#daytoo"+id).val();

       $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
         'data' :'dateValue='+getdate+'&jobId='+id,
         'success':function(htmlres) {

            htmlres = jQuery.parseJSON(htmlres);

            if(htmlres.status == "error") {

                $("#setval"+id).html(htmlres.message);
                $("#datetimepicker1"+id).val(''); 
                $('#schedule'+id).attr('disabled',true);

            } else {
                 $('#schedule'+id).attr('disabled',false);
                 $("#setval"+id).html("");
                 $("#datetimepicker1"+id).val(getdate);
            }
          }            
        });
    }
</script>
<script>
   function getTime(id)
   {
      var gettime= $("#datepicker1"+id).val();
      var timefrom= $("#timefromm"+id).val();
      var timetoo= $("#timetoo"+id).val();
      //return false;
      $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
         'data' :'timeValue='+gettime+'&jobId='+id,
         'success':function(htmlres)
         {
            if(htmlres !='')
            {
            if(htmlres == 1)
            {
              $('#schedule'+id).attr('disabled',false);
              $("#setvall"+id).html("");
              $("#datepicker1"+id).val(gettime);
            }
            else
            {
                //alert('Please Select Schedule time');
               $("#setvall"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
               $("#datepicker1"+id).val(''); 
                $('#schedule'+id).attr('disabled',true);
            }
         }
         }
           
       });
   }
</script>
<script>
   function getdaynamee(id) {
       var getdate= $("#datetimepicker12"+id).val();
       var dayfrom = $("#dayfrommm"+id).val();
       var dayto   = $("#daytooo"+id).val();

       $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
         'data' :'dateValue='+getdate+'&jobId='+id,
         'success':function(htmlres) {

            htmlres = jQuery.parseJSON(htmlres);

            if(htmlres.status == "error") {

                $("#setval12"+id).html(htmlres.message);
                $("#datetimepicker12"+id).val(''); 
                $('#schedulees'+id).attr('disabled',true);

            } else {
                 $('#schedulees'+id).attr('disabled',false);
                 $("#setval12"+id).html("");
                 $("#datetimepicker12"+id).val(getdate);
            }
          }    
        });
    }   
</script>
<script>
   function getTimee(id)
      {
         var gettime= $("#datepicker13"+id).val();
         var timefrom= $("#timefrommm"+id).val();
         var timetoo= $("#timetooo"+id).val();
         $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
            'data' :'timeValue='+gettime+'&jobId='+id,
            'success':function(htmlres)
            {
               if(htmlres !='')
               {
               if(htmlres == 1)
               {
                 $('#schedulees'+id).attr('disabled',false);
                 $("#setvall12"+id).html("");
                   $("#datepicker13"+id).val(gettime);
               }
               else
               {
                   //alert('Please Select Schedule time');
                  $("#setvall12"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                  $("#datepicker13"+id).val(''); 
                   $('#schedulees'+id).attr('disabled',true);
               }
            }
            }
              
          });
      }
</script>

<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"></figure> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"></figure> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"></figure> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"></figure> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"></figure> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><figure><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"></figure> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><figure><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"></figure> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }
?>

