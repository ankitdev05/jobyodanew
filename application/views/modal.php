<?php
   include_once('mapscript.php');
?>
<div class="modal fade" id="gpsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <p>JobYoDA requires access to location. To enjoy all that JobYoDA has to offer, turn on your GPS and give JobYoDA access to your location.</p>
                        <div class="statsusdd">
                           <button type="button" class="btn btn-success" id="allowGps">OK</button>
                        </div>
                     </div>
                  </form>
               </div>

               <div class="clear"></div>
               
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">User Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <p id="errorfield" class="text-danger text-center text-center1">
                  </p>
                  <p id="errorfield1" class="text-success text-center errorfield1"></p>
                  <form method="post">
                     <?php if(isset($signupsuccess['success'])){echo "<p class='text-center text-success'>".$signupsuccess['success']."</p>"; } ?>
                     <div class="forminputspswd">
                        <input type="text" name="email" id="loginemail" class="form-control" value="<?php if(!empty($_COOKIE['emaill'])){echo $_COOKIE['emaill'];}?>" placeholder="Email" required="required">
                        <img src="<?php echo base_url().'webfiles/';?>img/msgping.png">
                     </div>
                     <div class="forminputspswd">
                        <input type="password" name="password" id="loginpass" value="<?php if(!empty($_COOKIE['passwordd'])){echo $_COOKIE['passwordd'];}?>" class="form-control" placeholder="Password" required="required">
                        <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                     </div>
                     <div class="forminputspswd">
                        <label class="form-check-label" for="check1">
                           <input type="checkbox" class="form-check-input" id="check1" name="option1" value="1" <?php if(!empty($_COOKIE['checkval'])){echo 'checked';}?>> 
                           <p class="shiftup">Remember Me</p>
                        </label>
                        <a href="#" class="forgetxt" data-toggle="modal" data-target="#exampleModalCenter18" data-dismiss="modal">Forgot Password ?</a>
                     </div>
                     <div class="forminputspswd loginspace">
                        <!--<button type="submit" class="srchbtns">Login</button>-->
                        <button type="button" class="srchbtns" id="loginjob" onclick="login()">Login</button>
                     </div>
                     <div class="forminputspswd socialsignar">
                        <p><a href="javascript:void(0)" onclick="showmodel()">Please click here for Sign Up!</a></p>
                        <p class="nxtopnt">or</p>
                        <?php  //var_dump($authUrl); ?>
                        <ul>
                           <li><a class="facebookurlget" href="<?php if(!empty($authUrl)) echo $authUrl; ?>"><img src="<?php echo base_url().'webfiles/';?>img/facetag.png"></a></li>
                           <li><a class="googleurlget" href="<?php if(!empty($loginURL)) echo $loginURL;?>"><img src="<?php echo base_url().'webfiles/';?>img/googtag.png"></a></li>
                        </ul>
                     </div>
                  </form>
               </div>
            </div>

            <div class="clear"></div>

         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="exampleModalCenter5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered signopopd" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Sign Up</h5>
            <button type="button" id="exampleModalCenter5Close" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form method="post" action="<?php echo base_url();?>user/signup" id="signupform">
                     <?php if(isset($signuperrors['datanot'])){echo "<p class='text-center text-danger'>".$signuperrors['datanot']."</p>"; } ?>
                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" maxlength="20" value="<?php if(!empty($userData['fname'])){ echo $userData['fname']; }?>">
                        <img src="<?php echo base_url().'webfiles/';?>img/usertag.png">
                     </div>
                     <?php if(isset($signuperrors['fname'])){echo "<p class='text-center text-danger'>".$signuperrors['fname']."</p>"; } ?>
                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" maxlength="20" value="<?php if(!empty($userData['lname'])){ echo $userData['lname']; }?>">
                        <img src="<?php echo base_url().'webfiles/';?>img/usertag.png">
                     </div>
                     <?php if(isset($signuperrors['lname'])){echo "<p class='text-center text-danger'>".$signuperrors['lname']."</p>"; } ?>
                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php if(!empty($userData['email'])){ echo $userData['email']; }?>" maxlength="150" >
                        <img src="<?php echo base_url().'webfiles/';?>img/msgping.png">
                        <?php if(isset($signuperrors['email'])){echo "<p class='text-center text-danger'>".$signuperrors['email']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="refer_code" id="refer_code" placeholder="Promocode" value="<?php if(!empty($userData['refer_code'])){ echo $userData['refer_code']; }?>" maxlength="60" onkeyup="referralCode()" >
                        <img src="<?php echo base_url().'webfiles/';?>img/referral.png">
                        <p class='text-center text-danger' id='refer_err'></p>
                     </div>
                     <div class="forminputspswd">
                        <div class="form-group">
                           <select name="country_code" placeholder="Code" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                              <option value="">Select Country Code</option>
                              <?php
                                 foreach($phonecodes as $phonecode) {
                                 ?>
                              <option <?php if(!empty($userData['country_code'])){ if($userData['country_code']=='+'.$phonecode['phonecode']) { echo "selected"; } } ?>  value="+<?php echo $phonecode['phonecode'];?>" <?php if($phonecode["phonecode"]=='63'){ echo "selected";} ?>> <?php echo $phonecode['name'];?> - <?php echo $phonecode['phonecode'];?> </option>
                              <?php
                                 }
                                 ?>
                           </select>
                        </div>
                        <?php if(isset($signuperrors['country_code'])){echo "<p class='text-center text-danger'>".$signuperrors['country_code']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <input type="number" class="form-control" name="phone" placeholder="Phone Number" value="<?php if(!empty($userData['phone'])){ echo $userData['phone']; }?>" id="phone" maxlength="12" minlength="4">
                        <img src="<?php echo base_url().'webfiles/';?>img/phonetag.png">
                        <?php if(isset($signuperrors['phone'])){echo "<p class='text-center text-danger'>".$signuperrors['phone']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <select name="exp_year" placeholder="Years of Experience" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                           <option value="">Years of Exp.</option>
                           <?php for($i=0;$i<=50;$i++) {?>
                           <option value="<?php echo $i;?>"
                              ><?php echo $i.' '.'years';?>
                           </option>
                           <?php }?>
                        </select>
                        <?php if(isset($signuperrors['exp_year'])){echo "<p class='text-center text-danger'>".$signuperrors['exp_year']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <select name="exp_month" placeholder="Months of Experience" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                           <option value="">Months of Exp.</option>
                           <?php for($i=0;$i<=12;$i++) {?>
                           <option value="<?php echo $i;?>"
                              ><?php echo $i.' '.'months';?></option>
                           <?php }?>
                        </select>
                        <?php if(isset($signuperrors['exp_month'])){echo "<p class='text-center text-danger'>".$signuperrors['exp_month']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <select name="education" id="education" class="form-control" placeholder="Attainment" style="width:100%">
                           <option value="">Select Highest Level of Education</option>
                           <option value="High School Graduate" >High School Graduate</option>
                           <option value="Vocational" >Vocational</option>
                           <option value="Undergraduate" >Undergraduate</option>
                           <option value="Associate Degree" >Associate Degree</option>
                           <option value="College Graduate" >College Graduate</option>
                           <option value="Post Graduate" >Post Graduate</option>
                        </select>
                        <?php if(isset($signuperrors['education'])){echo "<p class='text-center text-danger'>".$signuperrors['education']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="location" id="location" placeholder="Location" value="<?php if(!empty($userData['location'])){ echo $userData['location']; }?>">
                        
                        <img src="<?php echo base_url().'webfiles/';?>img/location.png" style="width: 23px;">
                        <?php if(isset($signuperrors['location'])){echo "<p class='text-center text-danger'>".$signuperrors['location']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <select name="state" placeholder="State" class="changestate" style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                           <option value="">Select State</option>
                           <?php
                              foreach($states as $getstates) {
                              ?>
                           <option <?php if(!empty($userData['state'])){ if($userData['state']==$getstates['state']) { echo "selected"; } } ?>  value="<?php echo $getstates['state'];?>" > <?php echo $getstates['state'];?> </option>
                           <?php
                              }
                              ?>
                        </select>
                        <?php if(isset($signuperrors['state'])){echo "<p class='text-center text-danger'>".$signuperrors['state']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <select name="city" placeholder="City/Town" class="changecity" style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                           <option value="">Select City</option>
                        </select>
                        <?php if(isset($signuperrors['city'])){echo "<p class='text-center text-danger'>".$signuperrors['city']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd" style="width:100%">
                        <select class="multiselect-ui" id="framework" name="intrested[]" multiple style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                           <!-- <option value="">Select Interested In</option> -->
                           <?php
                              foreach($intrestedin as $getintrested) {
                                 if($getintrested['subcategory'] == "ALL") {} else {
                              ?>
                           <option <?php if(!empty($userData['intrested'])){ if($userData['intrested']==$getintrested['subcategory']) { echo "selected"; } } ?>  value="<?php echo $getintrested['subcategory'];?>" > <?php echo $getintrested['subcategory'];?> </option>
                           <?php
                                 }
                              }
                              ?>
                        </select>
                        <span class="interestmsg text-center text-warning"> </span>
                        <?php if(isset($signuperrors['intrested'])){echo "<p class='text-center text-danger'>".$signuperrors['intrested']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <select name="nationality" placeholder="Nationality" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                           <option value="">Select Nationality</option>
                           <?php
                              foreach($nations as $nation) {
                              ?>
                           <option <?php if(!empty($userData['nationality'])){ if($userData['nationality']==$nation['nationality']) { echo "selected"; } } ?>  value="<?php echo $nation['nationality'];?>" > <?php echo $nation['nationality'];?> </option>
                           <?php
                              }
                              ?>
                        </select>
                        <?php if(isset($signuperrors['nationality'])){echo "<p class='text-center text-danger'>".$signuperrors['nationality']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <select name="superpower" placeholder="Nationality" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                           <option value="">Select Superpower</option>
                           <?php
                              foreach($powers as $power) {
                              ?>
                           <option <?php if(!empty($userData['superpower'])){ if($userData['superpower']==$power['power']) { echo "selected"; } } ?>  value="<?php echo $power['power'];?>" > <?php echo $power['power'];?> </option>
                           <?php
                              }
                              ?>
                        </select>
                        <?php if(isset($signuperrors['superpower'])){echo "<p class='text-center text-danger'>".$signuperrors['superpower']."</p>"; } ?>
                     </div>
                     <?php
                     if(empty($type)) {
                     ?>
                     <div class="forminputspswd">
                        <input type="password" class="form-control" name="pass" id="pass" placeholder="Password" value="<?php if(!empty($userData['pass'])){ echo $userData['pass']; }?>">
                        <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                     </div>
                     <div class="forminputspswd">
                        <input type="password" class="form-control" name="cpass" placeholder="Confirm Password" value="<?php if(!empty($userData['cpass'])){ echo $userData['cpass']; }?>">
                        <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                     </div>
                     <?php if(isset($signuperrors['pass'])){echo "<p class='text-center text-danger'>".$signuperrors['pass']."</p>"; } ?>
                     <?php if(isset($signuperrors['cpass'])){echo "<p class='text-center text-danger'>".$signuperrors['cpass']."</p>"; } ?>
                     
                     <?php
                        }
                     ?>

                     <div class="forminputspswd fullma" >
                        <label class="form-check-label accptance" for="check2">
                        <input type="checkbox" class="form-check-input" id="check2" name="option1" value="something"> I accept all <a href="<?php echo base_url(); ?>terms" target="_blank">Terms & Conditions</a> and <a href="<?php echo base_url(); ?>privacy_policy" target="_blank">Privacy Policy.</a>
                        </label>
                     </div>
                     <?php if(isset($signuperrors['option1'])){echo "<p class='text-center text-danger'>".$signuperrors['option1']."</p>"; } ?>
                     <div class="" style="float:left; width:100%;">

                        <div class="forminputspswd loginspace" style="margin: 0 auto; float: none;">
                           <input type="hidden" id="signupType" name="signupType" value="<?php if(empty($type)){ echo "normal"; }else{ echo $type; } ?>">

                           <button type="submit" class="srchbtns1 srchbtns" name="signupjob" id="signupjob">Sign Up</button>
                        </div>
                     </div>
                  </form>
               </div>

               <div class="clear"></div>

            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="exampleModalCenter18" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                 
                  <div class="inform">
                     <p class="greentxt">Forgot Password</p>
                     <p>Enter your registered Email Id</p>
                     <div>
                        <div class="forminputspswd">
                           <script>        </script>
                           <input type="text" class="form-control" placeholder="Email Address" name="user_email" id="user_email">
                           <img src="<?php echo base_url().'webfiles/';?>img/msgping.png">
                        </div>
                     <div>
                     <p id="errorfieldd" class="text-danger text-center"></p>
                  </div>
          
                        <div class="forminputspswd loginspace">
                           <!--<button type="submit" class="srchbtns">Login</button>-->
                           <button type="button" class="srchbtns" id="forgetpass">Reset Password</button>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="clear"></div>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="confrmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div class="inform">
                     <p class="">Forgot Password</p>
                     <p id="confrmMsg" class="text-success text-center"></p>
                     
                  </div>
               </div>

               <div class="clear"></div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter4444" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div>
                     <p id="errorfielddddd" class="text-success text-center">you are successfully verified please login!</p>
                  </div>
               </div>

               <div class="clear"></div>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="exampleModalCenter44" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Verification Code</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div>
                     <p id="errorfielddd" class="text-danger text-center"></p>
                  </div>
                  <form method="post">
                     <?php if(isset($signupsuccesss['success'])){echo "<p class='text-center text-success'>".$signupsuccesss['success']."</p>"; } ?>
                     <div class="forminputspswd">
                        <input type="text" name="mobile_number" id="mobile_number" class="form-control" value="<?php echo $this->session->userdata('phone') ?>" placeholder="mobile number" required="required" readonly>
                        <img src="<?php echo base_url().'webfiles/';?>img/phonetag.png">
                     </div>
                     <input type="hidden" id="hiddenotp" value="<?php  echo $this->session->userdata('verify_code') ?>">
                     <div class="forminputspswd">
                        <input type="text" name="otp" id="otp" value="" class="form-control" placeholder="Verification code" required="required">
                        <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                     </div>
                     <div class="forminputspswd loginspace">
                        <button type="button" class="srchbtns" id="checkotp" onclick="verifyotp()">OK</button>
                     </div>
                  </form>
               </div>

               <div class="clear"></div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- <div class="ModalBox">
  <div id="VideoModal" class="modal fade" role="dialog">
      <div class="modal-dialog"> 
          <div class="modal-content"> 
              <div class="modal-body">
                  <a href="javascript:void(0);" class="Close" data-dismiss="modal">×</a>
                  <iframe id="videoframe" src="https://www.youtube.com/embed/G3YeTQEdZmw"></iframe>
              </div> 
          </div>
      </div>
  </div> 
</div> -->

<div class="ModalBox">
   <div class="modal fade" id="VideoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-body">
               <a href="javascript:void(0);" class="Close" id="modalCloseClass" data-dismiss="modal">×</a>
               <iframe id="videoframe" src="" autoplay></iframe>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   function videofunction(video) {
      $('#videoframe').attr('src', video);
      $('#VideoModal').addClass('in');
      $('body').addClass('modalbackground');
      $('#VideoModal').show();

   }
   $(document).ready(function() {
      $('#modalCloseClass').on('click', function() {
         $('body').removeClass('modalbackground');
         $('#VideoModal').removeClass('in');
         $('#VideoModal').css('display','none');
         $('#videoframe').attr('src', '');
      });
   });
</script>



<script>
    function showmodel() {
        $("#exampleModalCenter4").modal('hide');
        $("#exampleModalCenter5").modal('show');
         $('body').addClass('modal-opennn');
    }
    $(document).ready(function() {
         $('#exampleModalCenter5Close').on('click', function() {
            $('body').removeClass('modal-opennn');       
         });
    });
</script> 

<script type = "text/javascript">
    function referralCode() {
        var refer_code = $('#refer_code').val();
        if (refer_code != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "user/checkreferral",
                data: {
                    refer_code: refer_code
                },
                success: function(data) {
                    if (data == 1) {
                        $('#refer_err').html("This Referral code does not exist");
                    } else {
                        $('#refer_err').html('');
                    }
                }
            })
        }
    } 

</script>

<script>
    function verifyotp() {
        var mobile_number = $("#mobile_number").val();
        var otp = $("#otp").val();
        var hiddenotp = $("#hiddenotp").val();
        if (otp != hiddenotp) {
            $("#errorfielddd").html('please enter correct otp');
            return false;
        }

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "user/verify_User",
            data: {
                mobile_number: mobile_number
            },
            cache: false,
            success: function(htmldata) {
               //console.log(htmldata);

               var htmldata = JSON.parse(htmldata);
                if (htmldata.success == 1) {

                  window.location.href = "https://jobyoda.com/thank_you";
                    // $("#exampleModalCenter44").modal('hide');
                    // $("#exampleModalCenter4").modal('show');
                    // $('#errorfield1').html('Successfully Registered. Please login');
                }
            },
        });
    } 
</script>

<script>
    function login() {
        var email = $("#loginemail").val();
        var pass = $("#loginpass").val();
        var checkval = $('#check1:checked').val();
        var cur_lat = $('#cur_lat').val();
        var cur_long = $('#cur_long').val();
        var currentURL = window.location.href;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "user/login",
            data: {
                email: email,
                password: pass,
                rememberval: checkval,
                cur_lat: cur_lat,
                cur_long: cur_long
            },
            success: function(data) {
               //alert('success');
                var logdone = "logindone";
                var n = data.includes(logdone);
                //console.log(n);
                if (n) {
                  //console.log('logged in');
                     //alert(currentURL);
                    window.location = currentURL;
                    //location.reload();
                    exit();
                } else {
                    $('.modpassfull .filldetails p.text-center1').text(data);
                }
            },
            error: function() {
                console.log('error');
            }
        });
    } 
</script>

<script type = "text/javascript" >
    $(document).ready(function() {
        $('#signupform').validate({
            rules: {
                fname: {
                    required: true,
                    minlength: 1,
                    maxlength: 30
                },
                lname: {
                    required: true,
                    minlength: 1,
                    maxlength: 30
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 100
                },
                phone: {
                    required: true,
                    number: true,
                },
                exp_year: {
                    required: true,
                },
                exp_month: {
                    required: true,
                },
                education: {
                    required: true,
                },
                location: {
                    required: true,
                },
                nationality: {
                    required: true,
                },
                superpower: {
                    required: true,
                },
                pass: {
                    required: true,
                    minlength: 5,
                },
                cpass: {
                    required: true,
                    equalTo: "#pass"
                },
                option1: {
                    required: true,
                },
            },
            messages: {
                fname: {
                    required: 'The first name field is required'
                },
                lname: {
                    required: 'The last name field is required'
                },
                email: {
                    required: 'The email filed is required',
                    remote: "This Email id is already registered.",
                    maxlength: "The email must not contain more than 100 characters"
                },
                phone: {
                    required: "The phone field is required",
                },
                exp_year: {
                    required: "Years of Exp is required",
                },
                exp_month: {
                    required: "Months of Exp is required",
                },
                education: {
                    required: "The education field is required",
                },
                location: {
                    required: "The location field is required",
                },
                nationality: {
                    required: "The nationality field is required",
                },
                superpower: {
                    required: "The superpower field is required",
                },
                pass: {
                    required: 'The password field is required',
                    minlength: 'Password must be at least 5 characters long'
                },
                cpass: {
                    required: "The confirm password field is required",
                    equalTo: "Please enter the same password as above"
                },
                option1: {
                    required: "The terms and condition field is required",
                },
            }
        });
    }); 
</script>

<script>
    $(document).ready(function() {
        $("#forgetpass").click(function() {
            var user_email = $("#user_email").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "user/forgetpassword",
                data: {
                    email: user_email
                },
                cache: false,
                success: function(htmldata) {
                    htmldata = htmldata.trim();
                    if (htmldata == 'We have sent you a link and verification code to reset your password.') {
                        $('#exampleModalCenter18').modal('hide');
                        $('#confrmModal').modal('show');
                        $('#confrmMsg').html(htmldata);
                        setTimeout(function() {
                            $('#confrmModal').modal('hide');
                            $('#exampleModalCenter4').modal('show');
                        }, 5000);

                    } else {
                        $('#errorfieldd').html(htmldata);
                    }
                },
                error: function() {
                    console.log('error');
                }
            });
        });
    });

$(document).ready(function() {
    $(".changestate").on('change', function() {
        var userstate = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "user/getownstate",
            data: {
                state: userstate
            },
            cache: false,
            success: function(htmldata) {
                $('.changecity').html(htmldata);
            },
            error: function() {
                console.log('error');
            }
        });
    });
});

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#framework').multiselect({
            nonSelectedText: "Select Interested In",
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('.multiselect-ui option:selected');
                
                if (selectedOptions.length >= 3) {
                     $('.interestmsg').html("You can select 3 Job categories only");
                }
                if (selectedOptions.length >= 3) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('.multiselect-ui option').filter(function() {
                        return !$(this).is(':selected');
                    });
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('.multiselect-ui option').each(function() {
                        var input = $('input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });
    });
</script>