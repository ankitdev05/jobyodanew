<script type = "text/javascript" >
    $('document').ready(function() {
        
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;
                var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                var geocoder = geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'latLng': latlng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            var fill_address = results[1].formatted_address;
                        }
                    }
                });

                $('#cur_lat').val(currentLatitude);
                $('#cur_long').val(currentLongitude);
                $('#lat').val(currentLatitude);
                $('#long').val(currentLongitude);
                if (localStorage) {
                    localStorage.setItem('currentLatitude', currentLatitude);
                    localStorage.setItem('currentLongitude', currentLongitude);
                }
            });
        }
    })

$('.rgtpos').click(function() {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var currentLatitudes = position.coords.latitude;
            var currentLongitudes = position.coords.longitude;
            var latlng = new google.maps.LatLng(currentLatitudes, currentLongitudes);
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'latLng': latlng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        var fill_address = results[1].formatted_address;
                        $('#txtPlacess').val(fill_address);
                    }
                }
            });

            $('#lat').val(currentLatitudes);
            $('#long').val(currentLongitudes);
        });
    }
});
</script> 

<script type = "text/javascript">
    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlacess'));
        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.A;
            var longitude = place.geometry.location.F;
            var mesg = "Address: " + address;
            $('#lat').val(place.geometry.location.lat());
            $('#long').val(place.geometry.location.lng());
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
        });
    }); 

</script>

<script type = "text/javascript" >
    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));

        google.maps.event.addListener(places, 'place_changed', function() {

            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.A;
            var longitude = place.geometry.location.F;
            $('#lati').val(place.geometry.location.lat());
            $('#longi').val(place.geometry.location.lng());
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
        });
    }); 

</script> 

<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5d243e1d5dcabb0012a03d41&product=inline-share-buttons' async='async'></script>

<script >
    $(".showsrchd").click(function(e) {
        e.stopPropagation();
        $(".homeheaders").toggleClass('opensidemenu');
    }); 
</script> 