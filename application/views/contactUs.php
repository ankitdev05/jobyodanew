<?php include_once('header.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>

<div class="managerpart">
     <div class="container-fluid">
        <div class="row">
           <div class="col-md-3 sideshift">
		   <div class="dashboardleftmenu">
                     <div class="menuinside">
                        <?php include_once('sidebar.php'); ?>
                     </div>
                     <div class="skillgraph">
                        <h6>Skills Percentage</h6>
                        <p>Put value for "Cover Image" field to <br>
                           increase your skill up to "15%"
                        </p>
                      <!--   <img src="<?php //echo base_url().'webfiles/';?>img/skillpercent.png"> -->
                       <link rel="stylesheet" href="<?php echo base_url().'webfiles/';?>css/progresscircle.css">
    <style>
      #demo {font-family: 'Roboto'; background-color:#fefefe; padding:2em;}
      .circlechart {
         float: left;
         padding: 20px;
      }

   </style> 

   <div id="demo">
     <div class="circlechart" data-percentage="<?php if(!empty($getCompletenes)){
      echo $getCompletenes['comp'];
     }?>"></div>
 
   </div>
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   
   <script src="<?php echo base_url().'webfiles/';?>js/progresscircle.js"></script>
   <script>
      $('.circlechart').circlechart(); // Initialization
   </script>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
                     </div>
                  </div>
				  
		   </div>
		   <div class="col-md-9 expanddiv">
		   <div class="innerbglay">
			 <div class="mainheadings-yoda">
         <h5>Contact Us</h5>
       </div>
				 <div class="contentare">
  				 <div class="formsrches">
            <form method="post" action="<?php echo base_url(); ?>Dashboard/usercontact">
             <div class="formmidaress">
                <div class="filldetails">
                   <input type="text" class="form-control" name="email" placeholder="Email ID" autocomplete="off" value="<?php echo $email; ?>" readonly>
  	 
  	               <textarea class="form-control" placeholder="Enter Details" name="message" required=""></textarea>
                
                </div>
             </div>
             <button type="submit" id="changepassbtn" class="srchbtns">Submit</button>
             </form>
           </div>
	       </div>
	       </div>
		   </div>
		   
		</div>
	 </div>
</div>

    <?php include_once('footer.php'); ?>