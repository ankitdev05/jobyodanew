<?php
    include_once('header2.php');
    // if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    //     $requestURI = $_SERVER['REQUEST_URI'];
    //     if($requestURI == "/user/index") {
    //         redirect("https://jobyoda.com/");    
    //     }
    // } else {
    //     $link = "https";
    //     $link .= "://";
    //     $link .= $_SERVER['HTTP_HOST'];
    //     $link .= $_SERVER['REQUEST_URI'];
    //     redirect($link);
    // }

    $userSess = $this->session->userdata('usersess'); 
    if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
        $listingTypeFun = "jobs";
    } else {
        $listingTypeFun = "jobs";
    }
?>
<style>
    .salaryColor{color:#fbaf3d;}
    .JobArea .tab-content .tab-pane{min-height: 150px!important;}
    .JobArea .tab-content .tab-pane .nofound{font-size: 20px!important;} 

    .JobArea .tab-content .tab-pane .nofound{
        border: 1px solid #ddd;
    text-align: center;
    padding: 40px 0;
    margin: 40px 0 30px 0;
    border-radius: 7px;
    text-transform: capitalize;
    font-family: Roboto;
    font-weight: 600;
    font-size: 30px !important;
    color: #000;
}



</style>

<!-- <div class='Loader'>
    <div class="Circle"></div>
</div> -->

    <section>
        <div class="SliderArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="SliderText">
                            <h3>Hiring for over <span><?php echo $openings[0]['openings'];?></span> BPO positions! </h3>

                            <h4>Best BPO jobs in the Philippines in one single site sorted by benefits and distance</h4>

                            <ul>
                                <li><i class="fa fa-tags"></i> Trending Keywords :</li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hotjob">Hot Jobs,</a></li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/work_from_home">Work From Home Jobs,</a></li> 
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/14_month_pay">Jobs with 14th Month Pay,</a></li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/day_shift">Day Shift Jobs</a></li> 
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="DownloadApp">
                            <a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" class="download-btn" target="_blank">
                                <span class="fa fa-apple"></span>
                                <p>
                                    <small>Download On</small>
                                    <br>
                                    App Store
                                </p>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.jobyodamo" class="download-btn" target="_blank">
                                <span class="fa fa-android"></span>
                                <p>
                                    <small>Get It On</small>
                                    <br>
                                    Google Play
                                </p>
                            </a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section>
        <div class="AdvertisementArea" style="padding: 10px 0 0px!important;">
            <div class="container">
                <div class="row">
                    <div class="col-md-4" data-aos="fade-right"> 
                        <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Rocket.png" style="width: 15px;filter: inherit;margin-bottom:5px;"></span> <b style="font-size: 15px;">Here to help.</b> <a href="<?php echo base_url(); ?>jobs/instant_screening" style="color: #084d87;"><b>Find Instant Screening jobs here</b> </a>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-right" data-aos="fade-left"> <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-2.png" style="width: 15px;filter: inherit;margin-bottom:5px;"></span> <b style="font-size: 15px;">Here to help.</b> <a href="<?php echo base_url(); ?>jobs/work_from_home" style="color: #084d87;"><b>Find Work From Home jobs here</b></a> </div>
                </div>
            </div>
        </div>    
    </section>
    
    <section>
        <div class="AdvertisementArea" style="padding: 10px 0 20px!important;">
            <div class="container">
            <?php 
                if(!empty($ad_list)) {
            ?>
                <div class="owl-carousel owl-theme" id="Advertisement">
                <?php
                    foreach($ad_list as $ad_lists) {
                ?>
                    <div class="item">
                        <div class="AdvertisementBox">
                            <figure>
                                <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($ad_lists['recruiter_id']);} else { echo base_url();?>site_details/<?php echo base64_encode($ad_lists['recruiter_id']); }?>">
                                    <img src="<?php echo $ad_lists['banner']; ?>">
                                </a>
                            </figure>
                            <?php
                                if(strlen($ad_lists['description']) > 1) {
                            ?>
                                <p> <?php echo $ad_lists['description']; ?> </p>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                <?php
                    }
                ?>
                </div>
            <?php
                } else {
            ?>
                    <div class="owl-carousel owl-theme" id="Advertisement">
                        <div class="item">
                            <div class="AdvertisementBox">
                                <figure>
                                        <img src="<?php echo base_url(); ?>images/new_banner_Man.png">
                                </figure>
                            </div>
                        </div>
                    </div>
            <?php
                }
            ?>
            </div>
        </div>
    </section>


    <section>
        <div class="JobArea" id="popularsection" style="display:none">
            <div class="container">
                <h1 class="Title"> Popular Searches</h1>
                <div class="JobHead">
                    <h2>BPO jobs with best benefits in the Philippines that everyone is searching for</h2>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" class="SearchTab" data-id="1" href="#Hot" onclick="changehtmlfunction(1)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-1.png"></span>
                                Hot Jobs
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="2" href="#WorkHome" onclick="changehtmlfunction(2)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-2.png"></span>
                                Work From Home Jobs
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="3" href="#Screening" onclick="changehtmlfunction(3)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Rocket.png" style="filter: inherit;"></span>
                                Jobs with Instant Screening 
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="4" href="#HMO" onclick="changehtmlfunction(4)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-6.png"></span>
                                Jobs with Day 1 HMO 
                            </a>
                        </li>
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="5" href="#Food" onclick="changehtmlfunction(5)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-7.png"></span>
                                Jobs with Free Food 
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="6" href="#IT" onclick="changehtmlfunction(6)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-9.png"></span>
                                IT Jobs  
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="7" href="#Leadership" onclick="changehtmlfunction(7)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-10.png"></span>
                                Leadership Jobs 
                            </a>
                        </li>
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="8" href="#Jobs" onclick="changehtmlfunction(8)">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-4.png"></span>
                                Jobs with 14th Month Pay 
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="JobBody">
                    <div class="tab-content">
                        <div id="Hot" class="PopularTabs PopularTabs1 tab-pane fade in active">
                            <h1>Hot Jobs   <?php if(count($hotJobs) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hotjob">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            <?php
                                if(!empty($hotJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $e=1;
                                    foreach($hotJobs as $hotJob) {
                                        if($hotJob['mode'] == "call" || $hotJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($hotJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($hotJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($e<=6){
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($hotJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $hotJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                        <!-- <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotJob['distance']; ?> KM</span> -->
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>">
                                                            <?php echo $hotJob['job_title']; ?> | <span class="salaryColor"><?php echo $hotJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($hotJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($hotJob['comapnyId']); }?>"><?php echo $hotJob['companyName']; ?></a></h4>
                                                    
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($hotJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($hotJob['recruiter_id']); }?>">

                                                            <?php echo $hotJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    
                                                    <p><?php echo substr($hotJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($hotJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks1']);
                                                            }
                                                            if(!empty($hotJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks2']);
                                                            }
                                                            if(!empty($hotJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $e++;
                                    }
                            ?>    
                            </div>
                            <?php
                                } else {
                                    echo "<p class='nofound'> No job found </p>";
                                }
                            ?>
                        </div>  
                       
                        

                        

                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="SearchArea">
            <div class="container">
                <h1>Search Jobs <span> ( <i class="fa fa-info-circle"></i> No need to fill up everything, Choose at least one) </span> </h1>
                <div class="row">

                    <div class="col-sm-7"> 

                        <div class="SearchLeft">
                            <?php if (!empty($userSess)) {?>
                            <form method="post" action="<?php  echo base_url(); ?>user/search1" class="has-validation-callback">
                                <?php }else{?>
                            <form method="post" action="<?php echo base_url(); ?>search" class="has-validation-callback">
                                  <?php }?>
                                <span class="text-center text-danger"><?php if($this->session->tempdata('homeerr')){ echo $this->session->tempdata('homeerr'); } ?></span>
                                    <div id="bpoits" class="tagscontent">
                                        <div class="form-group col-sm-6"> 
                                            <label>Search by Location <span class="text-danger" style="font-size: 19px;"> *</span></label>
                                            <span class="Icon loctrack"><img src="<?php echo base_url(); ?>webfiles/img/home/targetloc.png"></span>
                                            <input type="text" class="form-control" name="locationn" placeholder="Location" id="txtPlaces" autocomplete="off">
                                            <input type="hidden" name="lat" id="lati">
                                            <input type="hidden" name="long" id="longi">
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <label>Search by Company</label>
                                            <input id="myInput" type="text" name="cname" placeholder="Search by Company Name" class="form-control" autocomplete="off">
                                            <input type="hidden" value="28.5355161" name="cur_lat">
                                            <input type="hidden" value="77.3910265" name="cur_long">
                                            <div id="suggesstion-box"></div>
                                        </div>

                                        <div class="form-group col-sm-12"> 
                                            <div class="LevelTags">
                                               <h6>Job Level</h6>
                                               <ul>
                                                    <?php
                                                        if(!empty($level_list)) {
                                                            $x=1;
                                                        foreach($level_list as $level_lists) {
                                                    ?>
                                                    <li>
                                                        <div class="Checkbox">
                                                            <input type="checkbox" name="joblevell[]" value="<?php echo $level_lists['id'];?>" class="custom-control-input" id="customCheck<?php echo $x;?>">
                                                            
                                                            <label for="customCheck<?php echo $x;?>">
                                                                <?php echo $level_lists['level'];?> 
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <?php $x++;}}?>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-12">
                                            <div class="LevelTags">
                                               <h6>Job Categories</h6>
                                               <ul>
                                                    <?php if(!empty($category_list)) {
                                                        $y=1;   
                                                        foreach($category_list as $category_lists) {      
                                                    ?>
                                                    <li>
                                                        <div class="Checkbox">
                                                            <input type="checkbox" name="jobcategoryy[]" onchange='getcatsubcat("<?php echo $category_lists['id'];?>")' value="<?php echo $category_lists['id'];?>" class="custom-control-input" id="customCheck<?php echo $y.$y;?>">

                                                            <label for="customCheck<?php echo $y.$y;?>">
                                                                <?php echo $category_lists['category'];?> 
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <?php $y++;}}?>
                                                </ul>
                                            </div> 
                                        </div>

                                        <div class="form-group col-sm-12">
                                            <label>Sub Categories</label>
                                            <select name="jobsubcategory" id="subcatresss" class="form-control" placeholder="Job title">
                                                <option value=""> Select Subcategory </option>
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <button type="submit" id="btnsearch">Click here to see your next Job!</button>
                                        </div>
                                        <div class="clear"></div>  
                                    </div>

                                    <div id="hotelsd" class="tagscontent" style="display:none;">
                                        <p>to be added in hotel</p>
                                    </div>

                                    <div id="restrosd" class="tagscontent" style="display:none;">
                                        <p>to be added in Restro</p>
                                    </div> 
                                </form> 
                            </div>
                        </div>

                    <div class="col-sm-5 padding_none">
                        <div class="SearchRight">
                            <div id="map" style="width: 100%; height: 480px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="CompanyArea">
            <div class="container">
                <h1>Our Partners
                    <a href="<?php echo base_url();?>our_partners">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <h2>Get hired in top BPO Companies through JobYoDA</h2>
                <?php
                    if(!empty($ourPartners)) {
                ?>
                <div class="owl-carousel owl-theme" id="Company">
                    <?php
                        foreach($ourPartners as $ourPartner) {
                    ?>
                            <div class="item">
                                <div class="CompanyBox"> 
                                    <figure>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']);}else{ echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']); }?>">
                                            <img src="<?php echo $ourPartner['image']; ?>">
                                        </a>
                                    </figure>
                                    <figcaption>
                                        <h3>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']);}else{ echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']); }?>">
                                                <?php if(strlen($ourPartner['cname']) > 45) { echo substr($ourPartner['cname'], 0,45).'...'; } else {echo $ourPartner['cname']; } ?>
                                            </a>
                                        </h3>
                                        <p>
                                            <i class="fa fa-briefcase"></i> 
                                            Numbers Of Jobs : <span><?php echo $ourPartner['jobcount']; ?></span>
                                        </p>
                                    </figcaption>
                                    <!-- <a href="">03 Openings</a> -->
                                </div>
                            </div>
                    <?php
                        }
                    ?>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>

    <section>
        <div class="CityArea">
            <div class="container">
                <h1>City Search
                    <a href="<?php echo base_url();?>city_search">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <h2>Find your dream BPO Job in all Major Cities across the</h2>

                <?php
                    if(!empty($citySearchs)) {
                ?>
                <div class="owl-carousel owl-theme" id="City">
                    <?php
                        foreach($citySearchs as $citySearch) {
                    ?>
                        <div class="item">
                            <div class="CityBox">
                                <figure>
                                    <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/city/<?php echo $citySearch['cityslug']; ?>">
                                        <img src="<?php echo $citySearch['image']; ?>">
                                    </a>
                                </figure>
                                <figcaption>
                                    <h5> 
                                        <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/city/<?php echo $citySearch['cityslug']; ?>">
                                            <?php echo $citySearch['cityname']; ?>
                                        </a>
                                    </h5>
                                    <p>Number Of Jobs : <?php echo $citySearch['jobcount']; ?></p>
                                </figcaption>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>

    <section>
        <div class="ExpertiseArea">
            <div class="container">
                <h1 class="Title">
                    Job Categories
                    <a href="<?php echo base_url();?>your_expertise">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <h2>Apply for the best BPO jobs that match your skills</h2>

                <?php
                    if(!empty($expertises)) {
                ?>
                <div class="owl-carousel owl-theme" id="Expertise">
                    <?php
                        foreach($expertises as $expertise) {

                            if($expertise['jobcount'] > 0) {
                    ?>
                            <div class="item">
                                <div class="ExpertiseBox">
                                    <?php
                                        if($expertise['catname'] == "Sales") {
                                            $image = "images/Expertise-1.png";
                                        } elseif($expertise['catname'] == "Customer Care") {
                                            $image = "images/Expertise-2.png";
                                        } elseif($expertise['catname'] == "Technical Support") {
                                            $image = "images/Expertise-3.png";
                                        } elseif($expertise['catname'] == "HealthCare") {
                                            $image = "images/Expertise-4.png";
                                        } elseif($expertise['catname'] == "Shared Services Support") {
                                            $image = "images/Expertise-5.png";
                                        } elseif($expertise['catname'] == "Specialized Jobs") {
                                            $image = "images/Expertise-6.png";
                                        } elseif($expertise['catname'] == "Information Technology") {
                                            $image = "images/Expertise-7.png";
                                        } elseif($expertise['catname'] == "Banking") {
                                            $image = "images/Expertise-8.png";
                                        } elseif($expertise['catname'] == "Leadership Roles") {
                                            $image = "images/ex_leader.png";
                                        } elseif($expertise['catname'] == "Finance") {
                                            $image = "images/ex_finance.png";
                                        } elseif($expertise['catname'] == "Quality") {
                                            $image = "images/ex_quality.png";
                                        } elseif($expertise['catname'] == "Training") {
                                            $image = "images/ex_training.png";
                                        } elseif($expertise['catname'] == "Workforce") {
                                            $image = "images/ex_work.png";
                                        } elseif($expertise['catname'] == "Human Resources") {
                                            $image = "images/ex_hr.png";
                                        } 
                                    ?>
                                    <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/expertise/<?php echo base64_encode($expertise['id']);?>">
                                    <figure> 
                                        
                                            <img src="<?php echo base_url().'webfiles/newone/'.$image; ?>"> 
                                        
                                    </figure>
                                    <h3><?php echo $expertise['catname']; ?></h3>
                                    <p><span><?php echo $expertise['jobcount']; ?></span> Open Positions </p>
                                    <span class="Icon"><img src="<?php echo base_url().'webfiles/newone/'.$image; ?>"></span>
                                    </a>
                                </div>
                            </div>
                    <?php
                            }
                        }
                    ?>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>

    <section>
        <div class="JobArea">
            <div class="container">
                <h1 class="Title"> 
                    <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-3.png"></span>
                    Nearby Jobs 
                    <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/nearby">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <div class="JobHead">
                    <h2>Find the best BPO jobs near you</h2> 
                </div>

                <div class="JobBody">
                <?php
                    if(!empty($nearByJobs)) {
                ?>
                    <div class="owl-carousel owl-theme JobSlider">
                        <?php
                            $a=1;
                            foreach($nearByJobs as $nearByJob) {
                                if($nearByJob['mode'] == "call" || $nearByJob['mode'] == "Call") {
                                    $modeText = "OVER THE PHONE INTERVIEW";
                                } else if($nearByJob['mode'] == "Walk-in") {
                                    $modeText = "WALK IN INTERVIEW";
                                } else if($nearByJob['mode'] == "Instant screening") {
                                    $modeText = "INSTANT SCREENING";
                                }
                                if($a <= 15) {
                        ?>
                                <div class="item">
                                    <div class="JobsBox">
                                        <figure>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($nearByJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($nearByJob['jobpost_id']); }?>">
                                            <?php
                                                if(!empty($nearByJob['job_image'])) {
                                            ?>
                                                    <img src="<?php echo $nearByJob['job_image']; ?>">
                                            <?php
                                                } else {
                                            ?>
                                                    <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                            <?php
                                                }
                                            ?>
                                                <!-- <span class="Logo"><img src="images/Company-8.png"></span> -->
                                                
                                            </a>
                                        </figure>
                                        <figcaption>
                                            <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $nearByJob['distance']; ?> KM</span>
                                            <h3>
                                                <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($nearByJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($nearByJob['jobpost_id']); }?>">
                                                    <?php echo $nearByJob['job_title']; ?> | <span class="salaryColor"><?php echo $nearByJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?> 
                                                </a>
                                            </h3>
                                            
                                            <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($nearByJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($nearByJob['comapnyId']); }?>"><?php echo $nearByJob['companyName']; ?></a></h4>
                                            
                                            <h5>
                                                <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($nearByJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($nearByJob['recruiter_id']); }?>">
                                                    
                                                    <?php echo $nearByJob['cname']; ?>
                                                </a>
                                            </h5>
                                            
                                            <p><?php echo substr($nearByJob['jobPitch'], 0,45).'...'; ?></p> 
                                            <ul>
                                                <?php
                                                    if(!empty($nearByJob['toppicks1'])) {
                                                        echo $getToppickByFunction = getToppickFunction($nearByJob['toppicks1']);
                                                    }
                                                    if(!empty($nearByJob['toppicks2'])) {
                                                        echo $getToppickByFunction = getToppickFunction($nearByJob['toppicks2']);
                                                    }
                                                    if(!empty($nearByJob['toppicks3'])) {
                                                        echo $getToppickByFunction = getToppickFunction($nearByJob['toppicks3']);
                                                    }
                                                ?>
                                            </ul>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($nearByJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($nearByJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                        </figcaption>
                                    </div>
                                </div>
                        <?php
                                } else {
                                    break;
                                }
                                $a++;
                            }
                        ?>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>

    <section>
        <div class="JobArea">
            <div class="container">
            <?php
                if(!empty($workFromhomeJobs)) {
            ?>
                <h1 class="Title"> 
                    <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-2.png"></span>
                    Work from Home jobs  
                    <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/work_from_home">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <div class="JobHead">
                    <h2>Full-time Work from Home BPO jobs</h2> 
                </div>

                <div class="JobBody">
                    <div class="owl-carousel owl-theme JobSlider">
                    <?php
                        $b = 1;
                        foreach($workFromhomeJobs as $workFromhomeJob) {
                            if($workFromhomeJob['mode'] == "call" || $workFromhomeJob['mode'] == "Call") {
                                $modeText = "OVER THE PHONE INTERVIEW";
                            } else if($workFromhomeJob['mode'] == "Walk-in") {
                                $modeText = "WALK IN INTERVIEW";
                            } else if($workFromhomeJob['mode'] == "Instant screening") {
                                $modeText = "INSTANT SCREENING";
                            }
                            if($b <= 15){
                    ?>
                            <div class="item">
                                <div class="JobsBox">
                                        <figure>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); }?>">
                                            <?php
                                                if(!empty($workFromhomeJob['job_image'])) {
                                            ?>
                                                    <img src="<?php echo $workFromhomeJob['job_image']; ?>">
                                            <?php
                                                } else {
                                            ?>
                                                    <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                            <?php
                                                }
                                            ?>
                                                <!-- <span class="Logo"><img src="images/Company-8.png"></span> -->
                                                
                                            </a>
                                        </figure>
                                        <figcaption>
                                            <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $workFromhomeJob['distance']; ?> KM</span>
                                            <h3>
                                                <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); }?>">
                                                    <?php echo $workFromhomeJob['job_title']; ?> | <span class="salaryColor"><?php echo $workFromhomeJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                </a>
                                            </h3>
                                            <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($workFromhomeJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($workFromhomeJob['comapnyId']); }?>"><?php echo $workFromhomeJob['companyName']; ?></a></h4>
                                            
                                            <h5>
                                                <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($workFromhomeJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($workFromhomeJob['recruiter_id']); }?>">
                                                    
                                                    <?php echo $workFromhomeJob['cname']; ?>
                                                </a>
                                            </h5>
                                            
                                            <p><?php echo substr($workFromhomeJob['jobPitch'], 0,45).'...'; ?></p> 
                                            <ul>
                                                <?php
                                                    if(!empty($workFromhomeJob['toppicks1'])) {
                                                        echo $getToppickByFunction = getToppickFunction($workFromhomeJob['toppicks1']);
                                                    }
                                                    if(!empty($workFromhomeJob['toppicks2'])) {
                                                        echo $getToppickByFunction = getToppickFunction($workFromhomeJob['toppicks2']);
                                                    }
                                                    if(!empty($workFromhomeJob['toppicks3'])) {
                                                        echo $getToppickByFunction = getToppickFunction($workFromhomeJob['toppicks3']);
                                                    }
                                                ?>
                                            </ul>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                        </figcaption>
                                    </div>
                            </div>
                    <?php
                            } else {
                                break;
                            }
                            $b++;
                        }
                    ?>
                    </div>
                </div>
            </div>
        <?php
            }
        ?>
        </div>
    </section>

    <section>
        <div class="JobArea">
            <div class="container">
            <?php
                if(!empty($bonusJobs)) {
            ?>
                <h1 class="Title"> 
                    <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-8.png"></span>
                    Jobs with Signing Bonus 
                    <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/signing_bonus">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <div class="JobHead">
                    <h2>Apply for BPO jobs with Joining Bonus</h2> 
                </div>

                <div class="JobBody">
                    <div class="owl-carousel owl-theme JobSlider">
                    <?php
                        $c=1;
                        foreach($bonusJobs as $bonusJob) {
                            if($bonusJob['mode'] == "call" || $bonusJob['mode'] == "Call") {
                                $modeText = "OVER THE PHONE INTERVIEW";
                            } else if($bonusJob['mode'] == "Walk-in") {
                                $modeText = "WALK IN INTERVIEW";
                            } else if($bonusJob['mode'] == "Instant screening") {
                                $modeText = "INSTANT SCREENING";
                            }
                            if($c <= 15) {
                    ?>
                            <div class="item">
                                <div class="JobsBox">
                                        <figure>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($bonusJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($bonusJob['jobpost_id']); }?>">
                                            <?php
                                                if(!empty($bonusJob['job_image'])) {
                                            ?>
                                                    <img src="<?php echo $bonusJob['job_image']; ?>">
                                            <?php
                                                } else {
                                            ?>
                                                    <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                            <?php
                                                }
                                            ?>
                                                <!-- <span class="Logo"><img src="images/Company-8.png"></span> -->
                                                
                                            </a>
                                        </figure>
                                        <figcaption>
                                            <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $bonusJob['distance']; ?> KM</span>
                                            <h3>
                                                <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($bonusJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($bonusJob['jobpost_id']); }?>">
                                                    <?php echo $bonusJob['job_title']; ?> | <span class="salaryColor"><?php echo $bonusJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?> 
                                                </a>
                                            </h3>
                                            <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($bonusJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($bonusJob['comapnyId']); }?>"><?php echo $bonusJob['companyName']; ?></a></h4>
                                            
                                            <h5>
                                                <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($bonusJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($bonusJob['recruiter_id']); }?>">

                                                    <?php echo $bonusJob['cname']; ?>
                                                </a>
                                            </h5>
                                            
                                            <p><?php echo substr($bonusJob['jobPitch'], 0,45).'...'; ?></p> 
                                            <ul>
                                                <?php
                                                    if(!empty($bonusJob['toppicks1'])) {
                                                        echo $getToppickByFunction = getToppickFunction($bonusJob['toppicks1']);
                                                    }
                                                    if(!empty($bonusJob['toppicks2'])) {
                                                        echo $getToppickByFunction = getToppickFunction($bonusJob['toppicks2']);
                                                    }
                                                    if(!empty($bonusJob['toppicks3'])) {
                                                        echo $getToppickByFunction = getToppickFunction($bonusJob['toppicks3']);
                                                    }
                                                ?>
                                            </ul>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($bonusJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($bonusJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                        </figcaption>
                                    </div>
                            </div>
                    <?php
                            } else {
                                break;
                            }
                            $c++;
                        }
                    ?>
                    </div>
                </div>
            <?php
                }
            ?>
            </div>
        </div>
    </section>

    <section>
        <div class="JobArea">
            <div class="container">
            <?php
                if(!empty($shiftJobs)) {
            ?>
                <h1 class="Title"> 
                    <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-5.png"></span>
                    Day shift jobs
                    <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/day_shift">view all <i class="fa fa-angle-double-right"></i></a>
                </h1>
                <div class="JobHead">
                    <h2>No more Night Shift, Find Dayshift BPO jobs here</h2> 
                </div>

                <div class="JobBody">
                    <div class="owl-carousel owl-theme JobSlider">
                    <?php
                        $d=1;
                        foreach($shiftJobs as $shiftJob) {
                            if($shiftJob['mode'] == "call" || $shiftJob['mode'] == "Call") {
                                $modeText = "OVER THE PHONE INTERVIEW";
                            } else if($shiftJob['mode'] == "Walk-in") {
                                $modeText = "WALK IN INTERVIEW";
                            } else if($shiftJob['mode'] == "Instant screening") {
                                $modeText = "INSTANT SCREENING";
                            }
                            if($d<=6) {
                    ?>
                            <div class="item">
                                <div class="JobsBox">
                                        <figure>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($shiftJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($shiftJob['jobpost_id']); }?>">
                                            <?php
                                                if(!empty($shiftJob['job_image'])) {
                                            ?>
                                                    <img src="<?php echo $shiftJob['job_image']; ?>">
                                            <?php
                                                } else {
                                            ?>
                                                    <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                            <?php
                                                }
                                            ?>
                                                <!-- <span class="Logo"><img src="images/Company-8.png"></span> -->
                                                
                                            </a>
                                        </figure>
                                        <figcaption>
                                            <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $shiftJob['distance']; ?> KM</span>
                                            <h3>
                                                <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($shiftJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($shiftJob['jobpost_id']); }?>">
                                                    <?php echo $shiftJob['job_title']; ?> | <span class="salaryColor"><?php echo $shiftJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                </a>
                                            </h3>
                                            <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($shiftJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($shiftJob['comapnyId']); }?>"><?php echo $shiftJob['companyName']; ?></a></h4>
                                            
                                            <h5>
                                                <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($shiftJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($shiftJob['recruiter_id']); }?>">

                                                    <?php echo $shiftJob['cname']; ?>
                                                </a>
                                            </h5>
                                            
                                            <p><?php echo substr($shiftJob['jobPitch'], 0,45).'...'; ?></p> 
                                            <ul>
                                                <?php
                                                    if(!empty($shiftJob['toppicks1'])) {
                                                        echo $getToppickByFunction = getToppickFunction($shiftJob['toppicks1']);
                                                    }
                                                    if(!empty($shiftJob['toppicks2'])) {
                                                        echo $getToppickByFunction = getToppickFunction($shiftJob['toppicks2']);
                                                    }
                                                    if(!empty($shiftJob['toppicks3'])) {
                                                        echo $getToppickByFunction = getToppickFunction($shiftJob['toppicks3']);
                                                    }
                                                ?>
                                            </ul>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($shiftJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($shiftJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                        </figcaption>
                                    </div>
                            </div>
                    <?php
                            } else {
                                break;
                            }
                            $d++;
                        }
                    ?>
                    </div>
                </div>
            <?php
                }
            ?>
            </div>
        </div>
    </section>

    <section>
        <div class="VideoArea">
            <div class="container">
                <h1>Check It Out </h1>
                <div class="owl-carousel owl-theme" id="Check">
                <?php
                    if($videoListings) {

                        foreach($videoListings as $videoListing) {
                ?>
                            <div class="item">
                                <div class="VideoBox">
                                    <a href="javascript:void(0);" onclick="videofunction('<?php echo $videoListing['video']; ?>')">
                                        <figure>
                                        <?php
                                            if(strlen($videoListing['banner']) > 0) {
                                        ?>
                                            <img src="<?php echo $videoListing['banner']; ?>">
                                        <?php
                                            } else {
                                                echo '<img src="'.base_url().'webfiles/img/user_man.png">';
                                            }
                                        ?>
                                        </figure>
                                        <figcaption>
                                            <h5><?php echo $videoListing['title']; ?></h5> 
                                        </figcaption>
                                        <span><i class="fa fa-play" aria-hidden="true"></i></span>
                                    </a>
                                </div>
                            </div>
                <?php
                        }
                    }
                ?>
                </div>
            </div>
        </div>
    </section>

<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }
?>

<script>
    function changehtmlfunction(id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>homepage/fetchpopularsearch",
            data:'keyword='+id,
            beforeSend: function() {
              $(".tab-content").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
              //$(".popularloader").css("background","#FFF url('http://jobyoda.com/webfiles/newone/loadericon.gif') no-repeat 165px");
            },
            success: function(data) {
                console.log(data);
                $(".tab-content").html(data);
                var owl = $(".JobSlider");
                owl.owlCarousel({
                    margin: 0,
                    smartSpeed: 1000,
                    autoplay: 5000, 
                    nav: true,
                    dots:false,
                    autoplayHoverPause: true,
                    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                    loop: true,
                    responsive: {
                        0: {
                            items: 1,
                        },
                        600: {
                            items: 2,
                        },
                        1000: {
                            items: 3,
                        },
                    },
                });
            },
            error: function (error_) {
                console.log(error_);
            }
        });
    }
</script>
<?php
    include_once('footer1.php');
?>