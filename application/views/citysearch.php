<?php
    include_once('header2.php');
// if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

//     } else {
//         $link = "https";
//         $link .= "://";
//         $link .= $_SERVER['HTTP_HOST'];
//         $link .= $_SERVER['REQUEST_URI'];
//         redirect($link);
//     }
    $userSess = $this->session->userdata('usersess'); 
    if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
    }
?>


    <section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1>City Search </h1>
            <h2>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.</h2>
        </div>
    </section>
 
    <section>
        <div class="CityArea BG-Gray">
            <div class="container"> 

                <div class="row">
                <?php
                    if($citySearchs) {

                        foreach($citySearchs as $citySearch) {
                ?>
                    <div class="col-sm-6 col-md-3">
                        <div class="CityBox">
                            <figure>
                                <a href="<?php echo base_url(); ?>jobs/city/<?php echo $citySearch['cityslug']; ?>">
                                    <img src="<?php echo $citySearch['image']; ?>">
                                </a>
                            </figure>
                            <figcaption>
                                <h5>
                                    <a href="<?php echo base_url(); ?>jobs/city/<?php echo $citySearch['cityslug']; ?>">
                                        <?php echo $citySearch['cityname']; ?>
                                    </a>
                                </h5>
                                <p>Number Of Jobs : <?php echo $citySearch['jobcount']; ?></p>
                            </figcaption>
                        </div>
                    </div>

                <?php
                        }
                    }
                ?>
                    <!-- <div class="col-sm-12">
                        <a href="javascript:void(0);" class="Loadmore">Load More</a>
                    </div> -->

                </div> 
            </div>
        </div>
    </section>

 
<?php
    include_once('footer1.php');
?>