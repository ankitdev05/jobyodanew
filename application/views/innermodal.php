
<div class="modal fade" id="statusModalCenter36" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered howshireds jbsrchstatus" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <div class="salryedits" style="width:100%;">
               <h4>Job Search Status</h4>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <span id="errorstatus"></span>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form method="post" action="<?php echo base_url(); ?>Dashboard/jobsearchstatus">
                     <div class="ratesuld">
                        <ul>
                           <li>
                              <div class="custom-control custom-radio">
                                 <input type="radio" name="searchstatus" value="1" <?php if($checkStatus[0]['jobsearch_status']=='1'){ echo "checked"; } ?> class="custom-control-input" id="customCheck45">
                                 <label class="custom-control-label" for="customCheck45">Actively Seeking</label>
                              </div>
                           </li>
                           <li>
                              <div class="custom-control custom-radio">
                                 <input type="radio" name="searchstatus" value="2" <?php if($checkStatus[0]['jobsearch_status']=='2'){ echo "checked"; } ?> class="custom-control-input" id="customCheck46">
                                 <label class="custom-control-label" for="customCheck46">Open to Offers</label>
                              </div>
                           </li>
                           <li>
                              <div class="custom-control custom-radio">
                                 <input type="radio" name="searchstatus" value="3" <?php if($checkStatus[0]['jobsearch_status']=='3'){ echo "checked"; } ?> class="custom-control-input" id="customCheck47">
                                 <label class="custom-control-label" for="customCheck47">Not open</label>
                              </div>
                           </li>
                        </ul>
                     </div>
                     <button type="submit" id="changestatusbtn" class="srchbtns">Done</button>  
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form method="post" action="<?php echo base_url();?>user/resumeUpload" enctype="multipart/form-data">
                     <div class="addupdatecent">
                        <div class="profileupload">
                           <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                           <input type="file"  id="file" onchange="getFilename()" name="resumeFile" data-validation="required" >
                           <?php if(!empty($checkResume[0]['resume'])) {?>
                           <p>Would you like to update your Resume?</p>
                           <?php }else{?>
                           <p>Please Add Your Resume</p>
                           <?php }?>
                           <div class="statsusdd">
                              <p class="norm" class="close" data-dismiss="modal" aria-label="Close">No</p>
                              <button type="button" class="updt uploadboxclasss">Yes</button>
                              <button type="submit" class="updt" id="resumebtn" style="display: none;">Update</button>
                           </div>
                           <p id="setimgres"></p>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                        <p>Resume Updated Successfully</p>
                        <div class="statsusdd">
                           <p class="updt" data-dismiss="modal" aria-label="Close">Ok</p>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <span id="errorpassword" class="text-danger text-center errorfield1"></span>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Enter Old Password" name="oldpass" id="oldpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Enter New Password" name="newpass" id="newpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Confirm New Password" name="confpass" id="confpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <button type="button" id="changepassbtn" class="srchbtns">Change</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="socialModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <?php $userSess = $this->session->userdata('usersess');
               $type = $usersess['type'];
               ?>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="baseyoda">
               <div class="thmb"> <img src="<?php echo base_url(); ?>webfiles/img/yellos.png"></div>
               <p>You are not allowed to change password because you logged in via <?php echo $type; ?></p>
            </div>
         </div>
      </div>
   </div>
</div>



<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                        <p><?php if(!empty($this->session->tempdata('updatemsg'))){ echo $this->session->tempdata('updatemsg'); }?></p>
                        <div class="statsusdd">
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<script type="text/javascript">
  $('.uploadboxclasss').click(function() { 
      $('#file').trigger('click');
      $('.uploadboxclasss').css('display','none');
      $('#resumebtn').css('display','block');
  });
</script>

<?php if($this->session->tempdata('updatemsg') != null) {
    
    if($this->session->tempdata('updatemsg')){$this->session->unset_tempdata('updatemsg');}?>
	<script type="text/javascript">
	    $(window).on('load',function(){
	        $('#profileModal').modal('show');
	    });
	    if ( window.history.replaceState ) {
	        window.history.replaceState( null, null, window.location.href );
	    }
	</script>
<?php } ?>

<script>
    $(document).ready(function(){   
    $("#changepassbtn").click(function() {
      var oldpass = $("#oldpass").val();
      var newpass = $("#newpass").val();
      var confpass = $("#confpass").val();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/changepass",
          data: {oldpass:oldpass, newpass:newpass,confpass:confpass},
          cache:false,
          success:function(htmldata) {
               $('#errorpassword').html(htmldata);
          },
          error:function(){
            console.log('error');
          }
      });
  });
});
</script>


<?php
   include_once('innermapscript.php');
?>