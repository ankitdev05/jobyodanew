<?php
    include_once('header2.php');
// if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

//     } else {
//         $link = "https";
//         $link .= "://";
//         $link .= $_SERVER['HTTP_HOST'];
//         $link .= $_SERVER['REQUEST_URI'];
//         redirect($link);
//     }
    $userSess = $this->session->userdata('usersess'); 
    if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
    }
?>

    <section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1>Partners  </h1>
            <h2>Get hired in top BPO Companies through JobYoDA</h2>
        </div>
    </section>

 
 
    <section>
        <div class="CompanyArea">
            <div class="container"> 
                <div class="row">
                <?php
                    if($ourPartners) {
                        $x=0;
                        foreach($ourPartners as $ourPartner) {
                ?>
                            <div class="col-sm-6 col-md-3 blogBox moreBox" <?php if($x >=8 ){ echo "style='display:none;'"; } ?>>
                                <div class="CompanyBox"> 
                                    <figure>
                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']);}else{ echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']); }?>">
                                            
                                            <img src="<?php echo $ourPartner['image']; ?>">
                                        </a>
                                    </figure>
                                    <figcaption>
                                        <h3>
                                            <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']);}else{ echo base_url();?>site_details/<?php echo base64_encode($ourPartner['id']); }?>">
                                                
                                                <?php if(strlen($ourPartner['cname']) > 45) { echo substr($ourPartner['cname'], 0,45).'...'; } else {echo $ourPartner['cname']; } ?>
                                            </a>
                                        </h3>
                                        <p>
                                            <i class="fa fa-briefcase"></i> 
                                            Numbers Of Jobs : <span><?php echo $ourPartner['jobcount']; ?></span>
                                        </p>
                                    </figcaption>
                                    <!-- <a href="">03 Openings</a> -->
                                </div>
                            </div>
                <?php
                        $x++;
                        }
                    }
                ?>

                    <div class="col-sm-12" id="loadMore" style="">
                        <a href="javascript:void(0);" class="Loadmore">Load More</a>
                    </div>

                </div> 
            </div>
        </div>
    </section>

<?php
    include_once('footer1.php');
?>