<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>


	<section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1>FAQ  </h1> 
        </div>
    </section>

 
 
    <section>
        <div class="FaqArea">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="FaqLeft">
                            <div class="panel-group" id="accordion">
                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse1">
                                        <h4>How does setting up my complete profile help me in finding the right job?</h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <p>Your complete profile will help us push the right jobs matching your profile to you and also when you apply for a job,recruiters would be able to see what you want to showcase about your skillsets.</p>
                                        </div>
                                    </div>
                                </div> 

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse2">
                                        <h4>What to do when you get "App not available in your country" error when downloading the App </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Please check the country in Play store settings(Account, Preference). It should be set to Philippines. If that doesnt work, please check the gmail account with which you have set up your Playstore account.For example if it was set outside Philippines, at times it wouldnt allow the app to be downloaded, so you may use an alternate email ID to download the app.</p>
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse3">
                                        <h4>What happens when you book a Date and Time slot for an interview? Does it hurt my profile if I am a No Show?</h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>"When you book a date and time slot for interview, recruiters know that you'll be there and would be waiting for you.It also allows you to beat the queue as you have booked your timeslot. If you dont show up, it does reflect on your profile, so please show up!"</p> 

                                        </div>
                                    </div>
                                </div>

                                
                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse4">
                                        <h4>What all platforms is JobYoDA available on?</h4>
                                    </div>
                                    <div id="collapse4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Our App can be downloaded on Andriod and ioS platforms. You may also create your profile through our website https://JobYoDA.com</p>
                                        </div>
                                    </div>
                                </div> 
                                
                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse5">
                                        <h4>Is it possible to reschedule an interview?</h4>
                                    </div>
                                    <div id="collapse5" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Interviews cannot be rescheduled at the moment. We suggest you call the recruiter if needed and agree upon the revised schedule</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse6">
                                        <h4>Can I apply for more than 1 job at the same time?</h4>
                                    </div>
                                    <div id="collapse6" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Yes, you may apply for multiple jobs within the same company or with other companies.Please make sure though that you meet the requirments of the job</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse7">
                                        <h4>How will I get notified about my interview status?</h4>
                                    </div>
                                    <div id="collapse7" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>You will recieve a reminder via In App notification and on your registered phone number 24hours prior to the scheduled interview time</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse8">
                                        <h4>Can I contact the recruiter directly? If yes, how? </h4>
                                    </div>
                                    <div id="collapse8" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>You may contact the recruiter via their listed Email Id or Phone Number. You may also connect with the recruite via Whatsapp or Viber if they have chosen that as a preferred mode to communicate</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse9">
                                        <h4>Where can I find the address of the company I have applied for?</h4>
                                    </div>
                                    <div id="collapse9" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Company Address can be found in the notifications section of the App. The reminder text and InApp notification will also have the company address</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse10">
                                        <h4>Will I be prioritzed over other walk-ins since I applied through the JobYoDA platform?</h4>
                                    </div>
                                    <div id="collapse10" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Yes. You will be prioiritized over walk-ins since recruiters would have details of the interview schedule you have chosen</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse11">
                                        <h4>What should I do if I am not able to appear for the interview at the scheduled time?</h4>
                                    </div>
                                    <div id="collapse11" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>It is very important to appear on the schedule you have chosen thorough our App.We recommend you give prior notification to the recruiter incase you are not able to make it. You may contact the recruiter via the listed email id, phone number or through Viber / Whatapp (incase the recruiter has chosen as a preferred mode of communication)</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse12">
                                        <h4>How can I delete my old resume and upload a new one?</h4>
                                    </div>
                                    <div id="collapse12" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>There is no option to delete the resume. You have the option to attach another copy and the last attached version would be visible to the recuiter</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse13">
                                        <h4>Can I search for jobs in a particular area?</h4>
                                    </div>
                                    <div id="collapse13" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Yes! You may type the location you are interested to find you Dream Job in</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapse14">
                                        <h4>How will I create my profile/look for jobs if I do not have a phone?</h4>
                                    </div>
                                    <div id="collapse14" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>You may create your profile by logging in to our website https://JobYoDA.com and start applying for jobs</p>
                                        </div>
                                    </div>
                                </div> 





                            </div> 
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </section>

<?php include_once('footer1.php'); ?>