<?php 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
if($this->session->userdata('usersess')){
	$usersess =  $this->session->userdata('usersess'); 
}

?>
<?php include('header.php'); ?>
<style type="text/css">

#contactform .error {
    color: #f00;
    margin: 0 0 10px 0;
    font-size: 12px;
    width: 100%;
}

label.error{
    left: 0;
    bottom: -4px;
    font-size: 11px;
}
</style>

<div class="managerpart">
     <div class="container-fluid">
        <div class="row">
         
		   <div class="col-md-9 expanddiv">
		   <div class="innerbglay" style="padding-top: 20px;">
			
				 <div class="contentare outerlgos">

				 <div class="row">
				 <div class="col-md-6">
				 <div class="addrsarea">

				 <div class="contmapsd">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30897.18638746072!2d121.02182253059688!3d14.533508596891739!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb54e623026f4033!2sFort%20Legend%20Tower%20Corporation!5e0!3m2!1sen!2sin!4v1571055883364!5m2!1sen!2sin"  frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>

				 	<div class="adrctnara">
                    <p><i class="fas fa-map-marker-alt"></i> <span>Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City 31st Street Fort Bonifacio TAGUIG CITY, FOURTH DISTRICT, NCR, Philippines, 1634</span></p>
                    <!-- <a href="tel:+1 246-345-0695"><i class="fas fa-phone"></i> +63 9178721630</a> -->
                    <a href="mailto:info@jobhunt.com"> <i class="fas fa-envelope"></i> Help@JobYoDA.com</a>

                    <div class="socialicons">
                           <ul>
                             <li>
                                <a href="https://www.facebook.com/jobyodapage/" target="_blank">
                                  <img src="<?php echo base_url().'webfiles/';?>img/facebook.png"> 
                                </a>
                              </li>
                             
                              <li>
                                <a href="https://instagram.com/jobyoda_ig?igshid=1u7xaxp20etlb" target="_blank">
                                  <img src="<?php echo base_url().'webfiles/';?>img/instagram.png"> 
                                </a>
                              </li>

                              <!-- <li>
                                <a href="#">
                                  <img src="<?php echo base_url().'webfiles/';?>img/google-plus.png"> 
                                </a>
                              </li> -->
							  
                           <!--  <li><a href="#"></a><i class="fab fa-twitter"></i></li>
                              <li><a href="#"></a><i class="fab fa-pinterest"></i></li>
                              <li><a href="#"></a><i class="fab fa-facebook-f"></i></li>
                              <li><a href="#"></a><i class="fab fa-google"></i></li> -->
                           </ul>
                        </div>

                      </div>
                        

                </div>

				 </div>
				 <div class="col-md-6">
  				 <div class="formsrches">
		            <form method="post" id="contactform" action="<?php echo base_url(); ?>user/usercontact">
		             <div class="formmidaress">
		             <h5>Get In Touch With Us:</h5>
                  <?php
                    if($this->session->tempdata('successmsg')) {
                  ?>
                    <p class="text-success text-center"> <?php echo $this->session->tempdata('successmsg'); ?> </p>
		              <?php
                    }
                  ?>
                    <div class="filldetails">
		                 <input type="text" class="form-control" name="fname" placeholder="Name" autocomplete="off" value="<?php if(!empty($usersess['name'])){ echo $usersess['name']; }?>" >
		                   <input type="text" class="form-control" name="email" placeholder="Email ID" autocomplete="off" value="<?php if(!empty($usersess['email'])){ echo $usersess['email']; }?>" >
		  	 				 <input type="text" class="form-control" name="phone" placeholder="Phone Number" autocomplete="off" value="<?php if(!empty($usersess['phone'])){ echo $usersess['phone']; }?>" >
		  	               <textarea class="form-control" placeholder="Enter Message" name="message" required=""></textarea>
		                
		                </div>
		             </div>
		             <button type="submit" id="changepassbtn" class="srchbtns">Submit</button>
		             </form>
           		</div>
           		</div>
           		 </div>
	       		</div>
	       </div>
		   </div>
		   
		</div>
	 </div>
</div>

<?php include('footer.php');?>
<script type="text/javascript">
  $(document).ready(function(){
         $('#contactform').validate({
              rules: {
                fname: {required:true,minlength:1,maxlength:30},
                email: {
                  required: true,
                  email: true,
                  maxlength:30
                },
                phone:{
                  required: true,
                  number: true,
                  //minlength:8,
                  //maxlength:10
                },
                message:{
                	required:true,
                }
              },
              messages: {
                fname: {required:'The name field is required'},
                email: {required:'The email field is required',remote:"This Email id is already registered.", maxlength:"The email must not contain more than 30 characters"},
                phone: {
                     required:"The phone field is required",
                    // maxlength:"Please enter no more than {0} number.",
                     //minlength:"Please enter at least {0} number."
                },
                message: {
                	required:"The message field is required",
                }
              }
         });
   });
</script>