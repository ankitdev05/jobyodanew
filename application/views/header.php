<html>
   <head>
      <title>JobYoDA</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <meta name="google-site-verification" content="059_geDz93Z9O9EKdpV5BF3oQAB_Vu6keNmwG_l2kc8" />
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-R64BFR96SR"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-R64BFR96SR');
      </script>
      <!-- Global site tag (gtag.js) - Google Ads: 851948051 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-851948051"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-851948051');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178463617-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-178463617-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '197166028031236'); 
    fbq('track', 'PageView');
    </script>

    <noscript>
    <img height="1" width="1"
    src="imageproxy?token=932e1c629e9bf5d4381f631a825d98945b70e7d0e9200d8e3c300c34f4e6bb88&url=https://www.facebook.com/tr?id=197166028031236&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <meta name="title" content="<?php if(isset($meta_title)){ echo $meta_title; } ?>">
    <meta name="description" content="<?php if(isset($meta_description)){ echo $meta_description; } ?>">
    
      <link href="<?php echo base_url().'webfiles/';?>css/all.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/fontawesome.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/style.css" rel="stylesheet" type="text/css">
	    <link href="<?php echo base_url().'webfiles/';?>css/owl.carousel.min.css" rel="stylesheet" type="text/css">
	    <link href="<?php echo base_url().'webfiles/';?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
      
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
      <link rel="icon" href="<?php echo base_url();?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link href="<?php echo base_url().'webfiles/';?>css/timepicker.less" rel="stylesheet" type="text/css" /> 
    
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'webfiles/';?>newone/css/responsive.css" />
      
      <style type="text/css">
         #signupform .error{color:#f00;}
         .pac-container {
            z-index: 10000 !important;
        }
      </style>
	  
	 	 <script src="<?php echo base_url().'webfiles/';?>js/jquery.min.js"></script>

       <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5d243e1d5dcabb0012a03d41&product=inline-share-buttons' async='async'></script>   
   </head>


   <body class="">
   <?php
   $this->session->set_userdata('previous_url', current_url());
      if($this->session->userdata('usersess')) {
         $usersess = $this->session->userdata('usersess');
   ?>
    
<header class="mainhead insidepad">

  <div class="InnerHeader">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a href="<?php echo base_url(); ?>" class="navbar-brand">
              <img src="<?php echo base_url().'webfiles/';?>newone/images/Logo.png">
              <p>#1 BPO Job Platform</p>
          </a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
              <li><a href="<?php echo base_url(); ?>">Home</a></li>
              <li><a href="<?php echo base_url(); ?>about">About us</a></li>
              <li><a href="<?php echo base_url(); ?>jobs/nearby/<?php echo base64_encode(0);?>">Jobs </a></li>
              <li><a href="<?php echo base_url(); ?>blogs">Blogs </a></li>
              <li><a href="<?php echo base_url();?>faq">FAQs </a></li>
              <li><a href="<?php echo base_url();?>how_it_works">How it Works </a></li>
              <li><a href="<?php echo base_url();?>contact">Contact us</a></li>
            </ul>
            <ul class="navbar-nav ml-auto navbar-right">
              <li><a href="<?php echo base_url();?>user/homeafterlogin"><?php echo $usersess['name']; ?></a></li>
              <li><a href="<?php echo base_url();?>dashboard/logout">Logout</a></li> 
            </ul>
          </div>
        </div>
    </nav> 
</div>
 
  <div class="col-md-12 sideshift">
    <div class="dashboardleftmenu">
      <div class="menuinside">
        <?php include_once('sidebar.php'); ?>
      </div>
    </div>
  </div>
         
</header>

      <!-- <div class="welcomebga">
         <h4>Welcome <span class="username"><?php echo $usersess['name'];?></span></h4>
      </div> -->
   <?php
      } else{
   ?>

      <header class="mainhead insidepad">
         
         <div class="InnerHeader">
            <nav class="navbar navbar-expand-lg">
              <div class="container">
                <a href="<?php echo base_url(); ?>" class="navbar-brand">
                    <img src="<?php echo base_url().'webfiles/';?>newone/images/Logo.png">
                    <p>#1 BPO Job Platform</p>
                </a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>about">About us</a></li>
                    <li><a href="<?php echo base_url(); ?>jobs/nearby/<?php echo base64_encode(0);?>">Jobs </a></li>
                    <li><a href="<?php echo base_url(); ?>blogs">Blogs </a></li>
                    <li><a href="<?php echo base_url();?>faq">FAQs </a></li> 
                    <li><a href="<?php echo base_url();?>how_it_works">How it Works </a></li>
                    <li><a href="<?php echo base_url();?>contact">Contact us</a></li>
                  </ul>
                  <ul class="navbar-nav ml-auto navbar-right">
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>recruiter/" target="_blank">Recruiter's Portal</a></li>
                    <li class="nav-item"><a class="nav-link" href="javascript:void(0);" data-toggle="modal" data-target="#exampleModalCenter4">Jobseekers Log In</a></li> 
                  </ul>
                </div>  
              </div>
          </nav>
         
      </header>
   <?php
      }
   ?>
    <input type="hidden" name="cur_lat" id="cur_lat" value="">
    <input type="hidden" name="cur_long" id="cur_long" value="">
<script type="text/javascript">
           $('document').ready(function(){
             if ("geolocation" in navigator){
                 navigator.geolocation.getCurrentPosition(function(position){ 
                   var currentLatitude = position.coords.latitude;
                   var currentLongitude = position.coords.longitude;
                   var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                     var geocoder = geocoder = new google.maps.Geocoder();
                     geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                         if (status == google.maps.GeocoderStatus.OK) {
                             if (results[1]) {
                                 var fill_address = results[1].formatted_address;
                                 //$('#txtPlacess').val(fill_address);
                             }
                         }
                     });
                   
                   $('#cur_lat').val(currentLatitude);
                   $('#cur_long').val(currentLongitude);
                   $('#lat').val(currentLatitude);
                   $('#long').val(currentLongitude);
                   if (localStorage) {
                    localStorage.setItem('currentLatitude', currentLatitude);
                    localStorage.setItem('currentLongitude', currentLongitude);
                   }

                 });
                
              }
           })
           $('.rgtpos').click(function(){
                  if ("geolocation" in navigator){
                  navigator.geolocation.getCurrentPosition(function(position){ 
                    var currentLatitudes = position.coords.latitude;
                    var currentLongitudes = position.coords.longitude;
                    var latlng = new google.maps.LatLng(currentLatitudes, currentLongitudes);
                      var geocoder = geocoder = new google.maps.Geocoder();
                      geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                          if (status == google.maps.GeocoderStatus.OK) {
                              if (results[1]) {
                                  var fill_address = results[1].formatted_address;
                                  $('#txtPlacess').val(fill_address);
                              }
                          }
                      });
                    
                    $('#lat').val(currentLatitudes);
                    $('#long').val(currentLongitudes);
                  });
                }
                });
 
         </script>   

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script> -->

         
         <script type="text/javascript">
           google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('txtPlacess'));
             google.maps.event.addListener(places, 'place_changed', function () {
                 var place = places.getPlace();
                 var address = place.formatted_address;
                 var latitude = place.geometry.location.A;
                 var longitude = place.geometry.location.F;
                 var mesg = "Address: " + address;
                 $('#lat').val(place.geometry.location.lat());
                 $('#long').val(place.geometry.location.lng());
                 mesg += "\nLatitude: " + latitude;
                 mesg += "\nLongitude: " + longitude;
             });
         });
         </script>

         <script type="text/javascript">
           google.maps.event.addDomListener(window, 'load', function () {
              var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
            
               google.maps.event.addListener(places, 'place_changed', function () {
                 
                   var place = places.getPlace();
                   var address = place.formatted_address;
                   var latitude = place.geometry.location.A;
                   var longitude = place.geometry.location.F;
                   $('#lati').val(place.geometry.location.lat());
                   $('#longi').val(place.geometry.location.lng());
                   var mesg = "Address: " + address;
                   mesg += "\nLatitude: " + latitude;
                   mesg += "\nLongitude: " + longitude;
               });
           });
        </script>

        <script type="text/javascript">
   google.maps.event.addDomListener(window, 'load', function () {
      var places1 = new google.maps.places.Autocomplete(document.getElementById('location'));
    
       google.maps.event.addListener(places1, 'place_changed', function () {
         
           var place = places1.getPlace();
           var address = place1.formatted_address;
           var latitude = place1.geometry.location.A;
           var longitude = place1.geometry.location.F;
           var mesg = "Address: " + address;
           mesg += "\nLatitude: " + latitude;
           mesg += "\nLongitude: " + longitude;
       });
   });
</script>

    <script>
$(".showsrchd").click(function (e) {
    e.stopPropagation();
    $(".homeheaders").toggleClass('opensidemenu');
});
</script> 
<!-- </body> -->