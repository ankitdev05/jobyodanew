<?php include_once('header2.php'); 
// if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

//     } else {
//         $link = "https";
//         $link .= "://";
//         $link .= $_SERVER['HTTP_HOST'];
//         $link .= $_SERVER['REQUEST_URI'];
//         redirect($link);
//     }
?>


	<section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1>How It Works  </h1> 
        </div>
    </section> 
 
    <section>
        <div class="WorkArea">
            <div class="container"> 
                <!-- <h1>How it works</h1> -->
                <div class="WorkLeft">
                    <figure><img src="https://jobyoda.com/webfiles/img/arrlfhow.png"></figure>
                    <h1>JOBSEEKERS</h1>
                    <figcaption>
                        <ul>
                            <li>
                                <span class="Number">1</span>
                                <span class="Line"></span>
                                <span class="Text">Schedule an interview</span>
                            </li>
                            <li>
                                <span class="Number">2</span>
                                <span class="Line"></span>
                                <span class="Text">Filter jobs based on the benefits you look for</span>
                            </li>
                            <li>
                                <span class="Number">3</span>
                                <span class="Line"></span>
                                <span class="Text">Search jobs near you</span>
                            </li>
                            <li>
                                <span class="Number">4</span>
                                <span class="Line"></span>
                                <span class="Text">Create your profile</span>
                            </li>
                            <li>
                                <span class="Number">5</span>
                                <span class="Line"></span>
                                <span class="Text">Download the app</span>
                            </li>
                        </ul>
                    </figcaption>
                </div>

                <div class="WorkImages">
                    <img src="https://jobyoda.com/webfiles/img/jobhowcen.png">
                </div>

                <div class="WorkRight">
                    <figure><img src="https://jobyoda.com/webfiles/img/arrorgthow.png"></figure>
                    <h1>RECRUITERS</h1>
                    <figcaption>
                        <ul>
                            <li>
                                <span class="Number">5</span>
                                <span class="Line"></span>
                                <span class="Text">Manage applicants</span>
                            </li>
                            <li>
                                <span class="Number">4</span>
                                <span class="Line"></span>
                                <span class="Text">Specify your offer</span>
                            </li>
                            <li>
                                <span class="Number">3</span>
                                <span class="Line"></span>
                                <span class="Text">Post jobs for FREE!</span>
                            </li>
                            <li>
                                <span class="Number">2</span>
                                <span class="Line"></span>
                                <span class="Text">Build your company profile</span>
                            </li>
                            <li>
                                <span class="Number">1</span>
                                <span class="Line"></span>
                                <span class="Text">Sign up</span>
                            </li>
                        </ul>
                    </figcaption>
                </div>

                <div class="clear"></div>

            </div>
        </div>
    </section>
 
    <section>
        <div class="JobSeekerArea">
            <div class="JobSeekerLeft">
                <span><i class="fa fa-laptop" aria-hidden="true"></i></span>
                <h3>I am a recruiter !</h3>
                <p>Posting jobs has never been this easy and it’s free! With JobYoDA, your posts are as good as making a pitch to jobseekers. Showcase your offer, company profile, pictures of your office and more! Sign up and post jobs now!</p>

                <a href="<?php echo base_url('recruiter')?>" target="_blank">Register Company</a>

            </div>
            <div class="JobSeekerRight">
                <span><i class="fa fa-user-circle-o" aria-hidden="true"></i></span>
                <h3>i am a jobseeker !</h3>
                <p>We can help you find that dream job NOW. JobYoDA is the only portal that allows you to search jobs based on location and benefits that matter to you. Do you want a dayshift account? Jobs with free meals? Jobs with 14th month pay or signing bonus? What about having HMO on Day 1? All these filters are available in JobYoDA!</p>

                <?php if(empty($this->session->userdata('usersess'))) { ?>
                    
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter4" data-dismiss="modal">Register Candidate</a>

                <?php } if(!empty($this->session->userdata('usersess'))) {?>

                    <a href="<?php echo base_url(); ?>jobs/nearby/<?php echo base64_encode(0);?>">Register Candidate</a>

                <?php }?>

            </div>
        </div>
    </section>


    <section>
        <div class="StatsArea">
            <div class="container">
                <h1>JobYoDA Site Stats</h1>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="StatsBox">
                            <span class="Icon"> <i class="fa fa-building" aria-hidden="true"></i> </span>
                            <h3 data-number="50"> 
                                <span id="Number3"><?php echo count($companylists); ?></span> 
                            </h3>
                            <p>Companies</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="StatsBox">
                            <span class="Icon"><i class="fa fa-handshake-o" aria-hidden="true"></i> </span>
                            <h3 data-number="27062"> 
                                <span id="Number4"><?php echo $openings[0]['openings'];?></span> 
                            </h3>
                            <p>Employment Opportunities</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="StatsBox">
                            <span class="Icon"> <i class="fa fa-suitcase" aria-hidden="true"></i> </span>
                            <h3 data-number="54426"> 
                                <span id="Number5"><?php echo count($userlist) ?></span> 
                            </h3>
                            <p>Jobseekers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>






<?php include_once('footer1.php'); ?>

<script type="text/javascript">
    $.fn.jQuerySimpleCounter = function( options ) {
        var settings = $.extend({
            start:  0,
            end:    100,
            easing: 'swing',
            duration: 400,
            complete: ''
        }, options );

        var thisElement = $(this);

        $({count: settings.start}).animate({count: settings.end}, {
            duration: settings.duration,
            easing: settings.easing,
            step: function() {
                var mathCount = Math.ceil(this.count);
                thisElement.text(mathCount);
            },
            complete: settings.complete
        });
    };

    $('#Number').jQuerySimpleCounter({end: 50,duration: 3000});
    $('#Number1').jQuerySimpleCounter({end: 27062,duration: 3000});
    $('#Number1').jQuerySimpleCounter({end: 54426,duration: 3000});
</script>