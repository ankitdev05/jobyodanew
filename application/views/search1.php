<?php 
//print_r($completeness);die;
include_once('header.php');

   if($this->session->userdata('userfsess')) {
       $userfsess = $this->session->userdata('userfsess');
       $type = $userfsess['type'];
     }
   
?>
<style>
  @import url('https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap');
.darkbgafootdiv {
display: none;
}
.insidenoftrs footer.homefooter {
opacity: 0;
height: 0;
padding: 0;
}
   .filterchekers input[type="checkbox"] {
   opacity: 0;
   z-index: auto;
   margin: 0;
   width: 90px;
   height: 55px;
   margin-top: -59px;
   }
   input[type="checkbox"], input[type="radio"] {
   z-index: 0!important;
   }
   .filterchekers ul li i {
   font-size: 25px;
   color: #27aa60;
   padding: 13px 12px;
   box-shadow: #adadad 1px 1px 6px 0px;
   border-radius: 25px;
   margin-bottom: 6px;
   text-align: center;
   width: 100px;
   height: 50px;
   }
   .managerpart input[type="reset"] {
   float: none;
   width: 93%;
   border: none;
   border-bottom: 1px solid #ddd;
   padding: 8px 13px;
   margin: 10px 0px;
   }
   #snackbar {
   visibility: hidden;
   min-width: 250px;
   margin-left: -125px;
   background-color: #dff0d8
   color: #3c763d;
   text-align: center;
   border-radius: 2px;
   padding: 1px;
   position: fixed;
   z-index: 10000;
   left: 74%;
   top: 39%;
   font-size: 17px;
   }
   .alert, .thumbnail {
   margin-bottom: 0px;
   }
   #snackbar.show {
   visibility: visible;
   -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
   animation: fadein 0.5s, fadeout 0.5s 2.5s;
   }
   @media screen and (max-width: 767px) and (min-width: 320px){
   .managerpart input[type="reset"] {
   float: none;
   width: 93%;
   border: none;
   border-bottom: 1px solid #ddd;
   padding: 8px 13px;
   margin: 10px 0px;
   }
   }

   .autocomplete input#myInput {
    background: rgb(56, 55, 55) !important;
}



.FeaturejobBox{}
.JobListingBox #slidelists{ width: 100% }


.FeaturejobBox figure{width:40%;position:relative}
.FeaturejobBox figure a{ display: block; }
.FeaturejobBox figure a img{ width: 100% }
.FeaturejobBox figure span.Wishlist{position:absolute;top:10px;right:10px;height:30px;width:30px;border-radius:20px;background-color:#fff;text-align:center;line-height:31px;color:#00a94f}
.FeaturejobBox .thumbimpr span.Distance{position:absolute;left: 10px;top: 10px;background-color:#00a94f;padding:5px 15px;color:#fff;border-radius:5px; font-size: 14px}


.FeaturejobBox .imagethumbs .thumbimpr{
border: none;
    padding: 0;
    border-radius: 0;
    position: relative;
}


.FeaturejobBox .imagethumbs .thumbimpr img{
  border-radius: 0px
}

.FeaturejobBox .thumbimpr .Wishlist{
    position: absolute;
    top: 10px;
    right: 10px;
    height: 30px;
    width: 30px;
    border-radius: 20px;
    background-color: #fff;
    text-align: center;
    line-height: 31px;
    color: #00a94f;
}



.FeaturejobBox .thumbimpr .Wishlist{
  position: absolute;
  top: 10px;
  right: 10px;
  height: 30px;
  width: 30px;
  border-radius: 20px;
  background-color: #fff;
  text-align: center;
  line-height: 31px;
  color: #00a94f;
  font-size: 14px;
  padding: 9px 0;
}

.FeaturejobBox .thumbimpr .Wishlist i{}

.positionarea .Distance{
    float: right;
    background-color: #00a94f;
    color: #fff;
    padding: 4px 8px;
    border-radius: 5px;
    font-size: 11px;
    font-family: Roboto;
    margin: 0 -5px 0 0;
}

.positionarea .Distance i{
    font-size: 9px;
    margin: 0 2px 0 0;
}

.positionarea .posttypes{
    color: #5d5c5c;
    margin: 0 0 4px;
    font-size: 14px;
    line-height: 18px;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
}

.positionarea h3{
    color: #00a94f;
    margin: 0 0 5px 0;
    font-size: 14px;
}

.positionarea .posttypes a {margin: 0 0 10px 0;font-size: 14px;color: #000;}

.positionarea .locationtype{
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
    margin: 0 0 1px;
    font-size: 13px;
    font-weight: 400;
}

.positionarea .salryedits{
    margin: 0 0 4px 0;
    font-family: Roboto;
}

.positionarea .salryedits .salicoftr .salry span{
    font-weight: 500;
    font-size: 13px;
}

.positionarea .Description{
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    font-family: Roboto;
    margin: 0 0 7px 0;
    font-size: 13px;
    line-height: 20px;
    color: #000;
}

.positionarea .SlaryAmount{
    float: left;
    width: 100%;
    font-size: 14px;
} 

.positionarea .ApplyJob{
    background-color: #00a94f;
    color: #fff;
    padding: 7px 25px;
    border-radius: 5px;
    font-family: Roboto;
    font-size: 14px;
    display: inline-block;
    box-shadow: none;
}

.positionarea .ApplyJob:hover{
      background-color: #fbaf31;
}

.InstantBox{
    padding: 30px;
}

.InstantBox button{
    position: absolute;
    top: -15px;
    right: -15px;
    width: 30px;
    height: 30px;
    background-color: #00a94f;
    opacity: 1;
    text-shadow: none;
    color: #fff;
    border-radius: 50%;
    font-size: 16px;
}

.InstantBox h5{
    font-family: Roboto;
    font-size: 14px;
    font-weight: 400;
    line-height: 25px;
    text-align: center;
    color: #000;
    margin: 0 0 10px 0;
}

.InstantBox h5 span{
    display: block;
    font-weight: 500;
}

.InstantBox p{
    margin: 0;
    text-align: center;
}

.InstantBox p a{
  background-color: #00a94f;
    color: #fff;
    padding: 7px 25px;
    border-radius: 5px;
    font-family: Roboto;
    font-size: 14px;
    display: inline-block;
    box-shadow: none;
}

.InstantBox p a:hover{
    background-color: #fbaf31;
}

.yodserchfilts.homefilters.openyodsrch{ width: 100%; right: 0 } 

@media screen and (max-width: 767px) and (min-width: 320px){

  .yodserchfilts.homefilters .tagsaresdplc ul li{ display: inline-block; margin: 0 10px 10px 0; position: relative;width: auto;
    height: auto; }
  .yodserchfilts.homefilters .tagsaresdplc ul li label {
    padding: 5px 8px;
    background: #fff;
    color: #747474;
    border-radius: 3px;
    font-size: 13px;
    margin: 0;
    min-width: 50px;
    text-align: center;
    font-weight: 400;
    border: 1px solid #ccc;
    cursor: pointer;
    width: auto !important;
    height: auto !important;
  }
  .yodserchfilts.homefilters .tagsaresdplc ul li input[type="checkbox"]:checked+label, 
  .yodserchfilts.homefilters .tagsaresdplc ul li .Checked+label {
    background: #084d87;
    border-color: #084d87;
    color: #fff;
}

.yodserchfilts.homefilters.openyodsrch{ width: 100%; right: 0 }
.yodserchfilts.homefilters .adminopnts.childpray {
    height: inherit;     border: none;
}



.girdh .yodserchfilts.homefilters.openyodsrch {
    top: 0;
    background: #f3f3f3;
}








}


.datepicker td, 
.datepicker th{ font-size: 17px !important }




</style>
<div class="managerpart showarbnd">
  <div class="container-fluid">
    <div class="expanddiv srcflows mapavigas">
			<div class="col-md-5 col-lg-12 girdh">
        <div class="" style="position: relative; float: left; width: 100%">
          <div class="searchareas">
            <img class="srchbtbs" src="https://jobyoda.com/webfiles/img/icosrch.png">
            <img class="srchbtbs show" src="https://jobyoda.com/webfiles/img/cross-symbol.png">
            <button type="submit" class="searchfiels onmapsrchs" style="float:left;">Search Jobs</button>
          </div>
				  
          <div class="yodserchfilts homefilters">
            <div class="adminopnts childpray">
              <div class="formmidaress modpassfull">
                <div class="filldetails">
                  <form method="post" action="https://jobyoda.com/user/search2" class="has-validation-callback">
                    <div class="tab">
                      <ul>
                        <li class="tagslinks active" onclick="openTagsjd(event, 'bpoits')">
                          <div class="icosprts">
                            <img class="shwnacts" src="https://jobyoda.com/webfiles/img/talkgreen.png">
                          </div>
                          <p>BPO-IT</p>
			                    <p class="csoon">Coming Soon!!</p>
                        </li>
                      </ul>
                    </div>
                    
                    <span class="text-center text-danger">
                      <?php if($this->session->tempdata('searcherr')){ echo $this->session->tempdata('searcherr'); } ?>
                    </span>
                    
                    <div id="bpoits" class="tagscontent">
                      
                      <div class="srchkeywidgets  advncspo">
                        <div class="loctrack">
                          <img src="<?php echo base_url(); ?>webfiles/img/home/targetloc.png">
                        </div>
                        <input type="text" class="form-control" name="locationn" placeholder="Location" id="txtPlaces" autocomplete="off">
                        <input type="hidden" name="lat" id="lati">
                        <input type="hidden" name="long" id="longi">
                      </div>
                      
                      <div class="autocomplete">
                        <input id="myInput" type="text" name="cname" placeholder="Search by Company Name" class="form-control" autocomplete="off">
                        <input type="hidden" value="28.5355161" name="cur_lat">
                        <input type="hidden" value="77.3910265" name="cur_long">
                        <div id="suggesstion-box"></div>
                      </div>          
             
                      <div class="joblevelstagsareas"> 
                        <div class="tagsaresdplc">
  			                  <h6>Job Level</h6>
                          <ul>
                            
                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="joblevell[]" value="4" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Frontline</label>
                              </div>
                            </li>

                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="joblevell[]" value="6" class="custom-control-input" id="customCheck2">
                                <label class="custom-control-label" for="customCheck2">Supervisory</label>
                              </div>
                            </li>
                            
                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="joblevell[]" value="8" class="custom-control-input" id="customCheck3">
                                <label class="custom-control-label" for="customCheck3">Senior Manager and Up</label>
                              </div>
                            </li>
                            
                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="joblevell[]" value="7" class="custom-control-input" id="customCheck4">
                                <label class="custom-control-label" for="customCheck4">Managerial</label>
                              </div>
                            </li>
                            
                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="joblevell[]" value="9" class="custom-control-input" id="customCheck5">
                                <label class="custom-control-label" for="customCheck5">All Levels</label>
                              </div>
                            </li>
                          </ul>
                        </div> 
                
                        <div class="tagsaresdplc">
                          <h6>Job Categories</h6>
                          <ul>
                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="jobcategoryy[]" onchange="getcatsubcat(&quot;5&quot;)" value="5" class="custom-control-input" id="customCheck11">
                                <label class="custom-control-label" for="customCheck11">Sales</label>
                              </div>
                            </li>

                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="jobcategoryy[]" onchange="getcatsubcat(&quot;3&quot;)" value="3" class="custom-control-input" id="customCheck22">
                                <label class="custom-control-label" for="customCheck22">Customer Care</label>
                              </div>
                            </li>
                            
                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="jobcategoryy[]" onchange="getcatsubcat(&quot;4&quot;)" value="4" class="custom-control-input" id="customCheck33">
                                <label class="custom-control-label" for="customCheck33">Technical Support</label>
                              </div>
                            </li>

                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="jobcategoryy[]" onchange="getcatsubcat(&quot;6&quot;)" value="6" class="custom-control-input" id="customCheck44">
                                <label class="custom-control-label" for="customCheck44">HealthCare</label>
                              </div>
                            </li>

                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="jobcategoryy[]" onchange="getcatsubcat(&quot;7&quot;)" value="7" class="custom-control-input" id="customCheck55">
                                <label class="custom-control-label" for="customCheck55">Shared Services Support</label>
                              </div>
                            </li>
                            
                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="jobcategoryy[]" onchange="getcatsubcat(&quot;8&quot;)" value="8" class="custom-control-input" id="customCheck66">
                                <label class="custom-control-label" for="customCheck66">Specialized Jobs</label>
                              </div>
                            </li>
                            
                            <li>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="jobcategoryy[]" onchange="getcatsubcat(&quot;9&quot;)" value="9" class="custom-control-input" id="customCheck77">
                                <label class="custom-control-label" for="customCheck77">All Categories</label>
                              </div>
                            </li>
                          </ul>
                        </div>
                
                        <h6>Sub Categories</h6>
                
                        <div class="form-group">
                          <select name="jobsubcategory" id="subcatresss" class="form-control" style="margin-left:0px" placeholder="Job title">
                            <option value=""> Select Subcategory </option> 
                          </select>
                        </div> 
                            
                        <button type="submit" class="srchbtns" id="btnsearch" style="float:left;">
                          Click here to see your next Job!
                        </button>
                      </div>
                    </div>
               
                    <div id="hotelsd" class="tagscontent" style="display:none;">
                      <p>to be added in hotel</p>
                    </div>
               
                    <div id="restrosd" class="tagscontent" style="display:none;">
                      <p>to be added in Restro</p>
                    </div>  
                  </form>
                </div> 
              </div>
            </div> 
          </div>

          <div class="mymapsght">
            <div id="map" style="width: 100%; height: 400px;"></div>
          </div> 
        </div>
      </div>
    </div>
				  
    <div class="expanddiv srcflows jobflowsd">
			   <div class="innerbglay">

                  <div class="">
                    <?php
                        if(array_filter($jobList)) {
                           $x=30;
                           foreach($jobList as $jobListing) {
                                 $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobListing['jobpost_id']);
                                 $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
                                 $timeFrom = $recruiterdetail[0]['from_time'];
                                 $timeFromm = date('H:i', strtotime($timeFrom));
                                 $timeTo   = $recruiterdetail[0]['to_time'];
                                 $timeToo = date('H:i', strtotime($timeTo));
                      ?>
                      
                  <div class="tabledivsdsn Min-height100">
				            <div class="featurerowjob FeaturejobBox">
                      <div class="row" id="slidelists">
                        <div class="col-md-3 col-lg-4 tumb">
                          <div class="imagethumbs">
							             <div class="thumbimpr"> 
                              <?php
                                if(!empty($jobListing['job_image']))
                                {
                              ?>
                              <a href="<?php echo base_url();?>job/description/<?php echo base64_encode($jobListing['jobpost_id']);?>">
                                <img src="<?php echo $jobListing['job_image'];?>">  
                              </a>  
                              <?php
                                }
                                else {
                              ?>
                              <a href="<?php echo base_url();?>job/description/<?php echo base64_encode($jobListing['jobpost_id']);?>">
                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                              </a>
                              <?php
                                }
                              ?>
                              


                              <!--   wishlist tags   -->

                              <div class="Wishlist">
                              <?php 
                                  if($jobListing['save_status']==1)
                                  {
                                   $title="Saved";
                              ?>
                               <span id="test3<?php echo $jobListing['jobpost_id'];?>"  onclick="savedjob('<?php echo $jobListing['jobpost_id']?>')">
                               <i class="fa fa-heart" title="<?php echo $title;?>" style="color:red;"></i>
                               </span>
                               <?php 
                                  }
                                  
                                  if($jobListing['save_status']==0)
                                  {
                                    $title="Unsaved";
                                  ?>
                              <span id="test4<?php echo $jobListing['jobpost_id'];?>" title="<?php echo $title;?>" onclick="savedjob('<?php echo $jobListing['jobpost_id']?>')">
                               <i class="fa fa-heart" title="<?php echo $title;?>" style="color:#b5b5b5; "></i>
                               </span>
                               <?php }?>

                               <span id="checkedstattus<?php echo $jobListing['jobpost_id'];?>" onclick="savedjob('<?php echo $jobListing['jobpost_id']?>')"></span>

                                </div>

                               <!--   wishlist tags End  -->
								            </div>
                          </div>
                         </div>
                         <div class="col-md-9 col-lg-8">
                            <div class="positionarea">
                              <span class="Distance">
                                <i class="fas fa-map-marker-alt"></i> 
                                <?php echo $jobListing['distance'];?>KM
                              </span>
                               <!--    <h6>Position</h6>-->
                               <a href="<?php echo base_url();?>job/description/<?php echo base64_encode($jobListing['jobpost_id']);?>">
                                  <p class="posttypes"><?php echo $jobListing['job_title']; ?></p>
                               </a>
							   
							                 <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($jobListing['company_id']);}else{ echo base_url();?>site_details/<?php echo base64_encode($jobListing['company_id']); }?>">

                                  <h3>
                                        <?php echo $jobListing['companyName']; ?>
                                  </h3>
                               </a>
							   
                               <p class="posttypes dire"> <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($jobListing['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($jobListing['recruiter_id']); }?>"> <?php echo $jobListing['cname']; ?> </a> </p>
                               
                               
                               <p class="locationtype"><img src="<?php echo base_url().'webfiles/';?>img/locmap.png"><?php echo $jobListing['company_address']; ?></p>
                                <div class="salryedits">
                            <div class="salicoftr"> 
                               <?php
                                  if(!empty($jobListing['toppicks1']))
                                    {
                                     if($jobListing['toppicks1']=='1')
                                     {
                                     ?>
                               <div class="salry">
                                  <img title="Joining Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                  <span>Joining Bonus</span>
                               </div>
                               <?php 
                                  }
                                  
                                  if($jobListing['toppicks1']=='2')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                  <span>Free Food</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks1']=='3')
                                   {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                  <span>Day 1 HMO</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks1']=='4')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                  <span>Day 1 HMO for Dependent</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks1']=='5')
                                  {
                                    ?>
                               <div class="salry">
                                  <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                  <span>Day Shift</span>
                               </div>
                               <?php
                                  }
                                  if($jobListing['toppicks1']=='6')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                  <span>14th Month Pay</span>
                               </div>
                               <?php
                                  }}
                                  
                                  
                                  if(!empty($jobListing['toppicks2']))
                                  {
                                  if($jobListing['toppicks2']=='1')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Joining Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                  <span>Joining Bonus</span>
                               </div>
                               <?php 
                                  }
                                  
                                  if($jobListing['toppicks2']=='2')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                  <span>Free Food</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks2']=='3')
                                   {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                  <span>Day 1 HMO</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks2']=='4')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                  <span>Day 1 HMO for Dependent</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks2']=='5')
                                  {
                                    ?>
                               <div class="salry">
                                  <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                  <span>Day Shift</span>
                               </div>
                               <?php
                                  }
                                  if($jobListing['toppicks2']=='6')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                  <span>14th Month Pay</span>
                               </div>
                               <?php
                                  }}
                                  
                                  
                                  if(!empty($jobListing['toppicks3']))
                                  {
                                  if($jobListing['toppicks3']=='1')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Joining Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                  <span>Joining Bonus</span>
                               </div>
                               <?php 
                                  }
                                  
                                  if($jobListing['toppicks3']=='2')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                  <span>Free Food</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks3']=='3')
                                   {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                  <span>Day 1 HMO</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks3']=='4')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                  <span>Day 1 HMO for Dependent</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks3']=='5')
                                  {
                                    ?>
                               <div class="salry">
                                  <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                  <span>Day 1 HMO for Dependent</span>
                               </div>
                               <?php
                                  }
                                  if($jobListing['toppicks3']=='6')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                  <span>14th Month Pay</span>
                               </div>
                               <?php
                                  }}
                                  ?>
								          </div>
                            </div>
							
							         <p class="Description"><?= $jobListing['jobPitch'];?></p>
											 			  
                              
                          <h4 class="SlaryAmount">
                                  <?php 
                                     if(isset($jobListing['salary']) && $jobListing['salary'] >0)
                                       {
                                       echo $jobListing['salary']."/Month";
                                       } else{
                                        echo "Salary confidential";
                                       } 
                                     
                                     ?>
                               </h4> 
                          <?php 
                            if($jobListing['mode'] == "Instant screening") {
                          ?>
                              <button class="greenbtn ApplyJob" data-toggle="modal" data-target="#exampleModalCenterInstant<?php echo $x;?>">Apply <?php if($jobListing['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $jobListing['mode']; } ?> </button>
                          <?php
                            } else {
                          ?>
                               <button class="greenbtn ApplyJob" data-toggle="modal" data-target="#exampleModalCenterb<?php echo $x;?>">Apply <?php if($jobListing['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $jobListing['mode']; } ?> </button>
                          <?php } ?>                              
                            </div>
                         </div>
            </div>
					</div>

                    <div class="modal fade" id="exampleModalCenterInstant<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="width: 500px">
                           <div class="modal-content" style="box-shadow: none;">

                              <div class="InstantBox">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                  </button>

                                  <!-- <h5>You will now be redirected to the Instant Assessment page. Your time invested now will speed up the hiring process later. JobYoDA wishes you all the best! Remember, 
                                  <span>"Every pro was once an amature. Every expert was once a beginner. So dream big and start now".</span></h5> -->

                                  <h5>
                                    This is far easier than driving around the city applying right!? You will now begin the Instant Assessment of <?php echo $jobListing['companyName']; ?>. They are looking for people like you but they will only know about you once you finish the assessment. Those who finish the assessment quickly, are more likely to get hired!  
                                  </h5>
                                  <p> Best of luck from the JobYoDA team! </p>

                                  <p>
                                    <a class="greenbtn" onclick="instantfunction('<?php echo $jobListing['jobpost_id']; ?>', '<?php echo $jobListing['modeurl']; ?>')" href="javascript:void(0)"> Begin Screening </a>
                                  </p>
                              </div>

                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="exampleModalCenterb<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form method="post" action="<?php echo base_url();?>dashboard/resumeUploadListing" enctype="multipart/form-data">
                                          <div class="addupdatecent">
                                             <div class="profileupload">
                                                <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png" style="width:50px; height:auto;">
                                                <input type="file" class="resumeupdtf" id="file<?php echo $x;?>" title="" name="resumeFile" onchange='getFilename("<?php echo $x ?>")'>
                                             </div>
                                             <p id="setimgres<?php echo $x ?>"></p>
                                             <input type="hidden" id="imagedata<?php echo $x ?>">
                                             <p>Would you like to update Your Resume?</p>
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-toggle="modal" data-target="#exampleModalCenterc<?php echo $x;?>" data-dismiss="modal">No</p>
                                                &nbsp;
                                                <input  type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="button" onclick="PdfImagesend('<?php echo $x ?>')" data-toggle="modal" data-target="#exampleModalCenterdd<?php echo $x;?>" data-dismiss="modal"  class="updty updt">Yes</button>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="exampleModalCenterc<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form id="formschedule<?php echo $jobListing['jobpost_id']?>">
                                          <div class="schedulejobgs">
                                             <h6>Schedule interview</h6>
                                             <p>Select Date & Time</p>
                                             <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker1<?php echo $jobListing['jobpost_id']?>" autocomplete="off" placeholder="mm/dd/yy" onchange="getdayname('<?php echo $jobListing['jobpost_id']?>')"  required="required">
                                                
                                                <span id="setval<?php echo $jobListing['jobpost_id']?>" style="color:red"></span>
                                                
                                                <input type="hidden" id="dayfromm<?php echo $jobListing['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                
                                                <input type="hidden" id="daytoo<?php echo $jobListing['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                                                
                                                <input type="hidden" id="timefromm<?php echo $jobListing['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                
                                                <input type="hidden" id="timetoo<?php echo $jobListing['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                             </div>
                                             <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTime('<?php echo $jobListing['jobpost_id']?>')" id="datepicker1<?php echo $jobListing['jobpost_id']?>"  data-format="hh:mm:ss" required="required"> 
                                                <span id="setvall<?php echo $jobListing['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                             </div>
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $jobListing['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                
                                                <button type="button" class="updt" onclick='scheduleclick("<?php echo $jobListing['jobpost_id']?>", "<?php echo $x;?>")' id="schedule<?php echo $jobListing['jobpost_id']?>" disabled>Schedule & Apply</button>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="exampleModalCenterdd<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form id="formschedulemore<?php echo $jobListing['jobpost_id']?>">
                                          <div class="schedulejobgs">
                                             <h6>Schedule interview</h6>
                                             <p>Select Date & Time</p>
                                             <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker12<?php echo $jobListing['jobpost_id']?>" placeholder="mm/dd/yy" onchange="getdaynamee('<?php echo $jobListing['jobpost_id']?>')"  required="required" autocomplete="off">
                                                <input type="hidden" id="dayfrommm<?php echo $jobListing['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytooo<?php echo $jobListing['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                                                <input type="hidden" id="timefrommm<?php echo $jobListing['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetooo<?php echo $jobListing['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <span id="setval12<?php echo $jobListing['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                             </div>
                                             <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee('<?php echo $jobListing['jobpost_id']?>')" id="datepicker13<?php echo $jobListing['jobpost_id']?>"  data-format="hh:mm:ss" required="required"> 
                                                <span id="setvall12<?php echo $jobListing['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                             </div>
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $jobListing['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="button" onclick='scheduleclickmore("<?php echo $jobListing['jobpost_id']?>", "<?php echo $x;?>")' class="updt" id="schedulees<?php echo $jobListing['jobpost_id']?>" disabled>Schedule & Apply</button>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="exampleModalCenter100<?php echo $jobListing['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header"> 
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form>
                                          <div class="addupdatecent">
                                             <img src="<?php echo base_url('webfiles/img/savedbighover.png')?>">
                                             <p class="jobsuccess<?php echo $jobListing['jobpost_id']?>">Job Applied Successfully</p>
                                             <div class="statsusdd">
                                                <p class="updt" data-dismiss="modal" aria-label="Close">Ok</p>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal fade" id="exampleModalCenter900<?php echo $jobListing['jobpost_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header"> 
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form>
                                          <div class="addupdatecent">
                                             <img src="<?php echo base_url('webfiles/img/savedbighover.png')?>">
                                             <p class="jobsuccess1<?php echo $jobListing['jobpost_id']?>"></p>
                                             <div class="statsusdd">
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     </div>

                  <?php
                        $x++;
                      }
                    }else{
                  ?>
                     <div>
                        <img class="bx_img" src="<?php echo base_url(); ?>webfiles/img/emptybx.png">
                        <center><h6 style="margin-top: 20px;">No Jobs available. Please update your experience to get more relevant Jobs.</h6>
                        
                     </center>
                     </div>
                  <?php }?>

                    
                     </div>
					 </div>
                  </div>
				  
				  
               
            </div>
         </div>

<div class="insidenoftrs">
<?php include_once('footer.php'); ?>
</div>

<div class="modal fade" id="exampleModalCenterjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">
               <span id="savedjobmessage"></span>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered howshireds" role="document">
        <div class="modal-content">
           <div class="modal-header">
       
       <div class="salryedits" style="width:100%;">
            <h4 style="text-align: center;">Hey <?php if(!empty($resultstatus[0]['name'])){ echo $resultstatus[0]['name']; } ?></h4>            
          </div>
      
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
           </div>
            <span id="errorfield"></span>
           <div class="modal-body">
              <div class="formmidaress modpassfull">
                 <div class="filldetails">
                   
             <div class="forminputspswd basemsg">
            <p>How was your application for <?php if(!empty($resultstatus[0]['job_title'])){ echo $resultstatus[0]['job_title']; } ?><br> at Jobs Posted by <?php if(!empty($resultstatus[0]['companyName'])){ echo $resultstatus[0]['companyName']; } ?> ? Tell us!</p>
             </div>
             
             <div class="ratesuld">
              <ul>
                <input type="hidden" name="jid" value="<?php if(!empty($resultstatus[0]['jobpost_id'])){ echo $resultstatus[0]['jobpost_id']; } ?> ">
                <li>
                <img src="<?php echo base_url();?>webfiles/img/wasnt.png">
                <p>Wasn't <br>Successful</p>
                </li>
                <li><img src="<?php echo base_url();?>webfiles/img/feedbck.png">
                <p>Waiting for <br>Feedback</p>
                </li>
                <li><img src="<?php echo base_url();?>webfiles/img/hired.png">
                <p>Got <br>Hired</p>
                </li>
              </ul>
             </div>
                       
                        
             
             <div class="quotesorgs">
            <p>Get hired through the app and get amazing perks</p>
             </div>
                   <!-- </form>-->
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>

<div class="modal fade" id="updateprofileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Update Profile</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <span id="errorfield"></span>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
               <div class="baseyoda">
                  <p>Please update your profile to get relevant jobs.</p>
                  <button type="button" id="" data-dismiss="modal" class="">SKIP</button>
                  <a href="<?php echo base_url(); ?>dashboard/myprofile"> UPDATE</a>
                </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="snackbar"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script>

<script src="<?php echo base_url(); ?>recruiterfiles/js/maps2.js" type="text/javascript"></script>


<script type="text/javascript">
   $(function () {
      $(".datetimepicker1").datepicker({ 
       dateFormat: "dd/mm/yy", 
        yearRange: '1900:2020', 
        defaultDate: '',
        autoclose: true,
     })
   });
   $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
    
</script>

<?php
   if(isset($_GET["msg"]) && $_GET["msg"] == "uploadcomplete" ) {
?>
      <script type="text/javascript">
         $(window).on('load',function(){
         $('#exampleModalCenter9').modal('show');
         });
      </script>
<?php
   }
?>

<script type="text/javascript">
   function initMap() {
        var currentLati = parseFloat(localStorage.getItem('currentLatitude'));
         var currentLongi = parseFloat(localStorage.getItem('currentLongitude'));
         <?php if(isset($jobcount[1]["lat"]) && $jobcount[1]["lat"]!=''){?>
          var centerlat=<?php echo $jobcount[1]["lat"] ?>;
        <?php }else{?>
          var centerlat=currentLati;
         <?php } if(isset($jobcount[1]["lng"]) && $jobcount[1]["lng"]!=''){?>
          var centerlong=<?php echo $jobcount[1]["lng"] ?>;
         <?php }else{?> 
          var centerlong=currentLongi;
          <?php }?>
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom:10,
          disableDefaultUI: true,
          center: {lat: centerlat, lng: centerlong}
        });

         var icon = { 
                url: 'https://jobyoda.com/markericon/maps.png'
            };

         //var infoWin = new google.maps.InfoWindow();   

        var markers = locations.map(function(location, i) {
                var marker = new google.maps.Marker({
                    position: {lat: location.lat, lng: location.lng},
                    icon:{url:location.url}
                });
                var infowindow = new google.maps.InfoWindow({
                content: location.info
                });
                infowindow.open(map,marker); 
                return marker;
            });

        var mcOptions = {
              //imagePath: 'https://googlemaps.github.io/js-marker-clusterer/images/m',
              styles:[{
                    url: "https://googlemaps.github.io/js-marker-clusterer/images/m1.png",
                    width: 53,
                    height:53,
                    fontFamily:"comic sans ms",
                    textSize:15,
                    textColor:"white"
              }]
              
            };

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers, mcOptions);
      }

      var locations = [
            <?php
              
              foreach ($jobcount as $js) {
                    //var_dump($js);
                    //print_r($jobcount);
                     echo "{lat: ".$js['lat'].", lng: ".$js['lng'].", info: '".$js['info']."', url:'".$js['url']."'},";
               }
            ?>     
      ];
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script>
<script type="text/javascript">
   google.maps.event.addDomListener(window, 'load', function () {
      var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
    
       google.maps.event.addListener(places, 'place_changed', function () {
         
           var place = places.getPlace();
           var address = place.formatted_address;
           var latitude = place.geometry.location.A;
           var longitude = place.geometry.location.F;
           $('#lati').val(place.geometry.location.lat());
           $('#longi').val(place.geometry.location.lng());
           var mesg = "Address: " + address;
           mesg += "\nLatitude: " + latitude;
           mesg += "\nLongitude: " + longitude;
       });
   });
</script>

    <!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&callback=initMap">
    </script> -->
<script>
   $('input.custom-control').on('change', function() {
      $('input.custom-control').not(this).prop('checked', false);  
   });
</script>

<script>
   $(document).ready(function(){    
     $(".listtoggle").on("click", function(){
         $(".jobllisting").css("display", "block");
         $(".mapsectionapp").css("display", "none");
         $("#mapdata").html('');
         $(".listtoggle1").css("display", "block");
         $(".listtoggle").css("display", "none");
     });
     $(".listtoggle1").on("click", function(){
         $(".jobllisting").css("display", "none");
         $(".mapsectionapp").css("display", "block");
         ("#mapdata").text("No Data Found");
         $(".listtoggle1").css("display", "none");
         $(".listtoggle").css("display", "block");
     });
   
   });
   
   $(".filterchekers li").click(function(){
   $(this).toggleClass("selectedgreen");  
   });
   
</script>


<script>
  
  function scheduleclick(id, id1) {
      var getform = "#formschedule" + id;
      var successmsg = ".jobsuccess"+id;
      var successmsg1 = ".jobsuccess1"+id;
      var successmodal = "#exampleModalCenter100"+id;
      var errormodal = "#exampleModalCenter900"+id;
      var closeModal = "#exampleModalCenterc"+id1;

      $.ajax({
          'type' :'POST',
          'url' :"<?php echo base_url('dashboard/scheduled') ?>",
          'data' :$(getform).serialize(),
          success:function(response) {

            var jsonData = JSON.parse(response);
              
            if(jsonData.status == "SUCCESS") {
              $(successmsg).html(jsonData.message);
              $(successmodal).modal('show');
              $(closeModal).modal('hide');

            } else {
                $(successmsg1).html(jsonData.message);
                $(errormodal).modal('show');
                $(closeModal).modal('hide');
            }
          }
      });
  }

  function scheduleclickmore(id, id1) {
      var getform = "#formschedulemore" + id;
      var successmsg = ".jobsuccess"+id;
      var successmsg1 = ".jobsuccess1"+id;
      var successmodal = "#exampleModalCenter100"+id;
      var errormodal = "#exampleModalCenter900"+id;
      var closeModal = "#exampleModalCenterc"+id1;

      $.ajax({
          'type' :'POST',
          'url' :"<?php echo base_url('dashboard/scheduled') ?>",
          'data' :$(getform).serialize(),
          success:function(response) {

            var jsonData = JSON.parse(response);
              
            if(jsonData.status == "SUCCESS") {
              $(successmsg).html(jsonData.message);
              $(successmodal).modal('show');
              $(closeModal).modal('hide');

            } else {
                $(successmsg1).html(jsonData.message);
                $(errormodal).modal('show');
                $(closeModal).modal('hide');
            }
          }
      });
  }

</script>
<script type="text/javascript">
   $(".updt").on("click",function(){
         window.location = "<?php echo base_url(); ?>appliedjobs";
   });
</script>

<script>
   function getdayname(id)
   {
       var getdate = $("#datetimepicker1"+id).val();
       var dayfrom = $("#dayfromm"+id).val();
       var dayto   = $("#daytoo"+id).val();

       $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
         'data' :'dateValue='+getdate+'&jobId='+id,
         'success':function(htmlres) {

            htmlres = jQuery.parseJSON(htmlres);

            if(htmlres.status == "error") {

                $("#setval"+id).html(htmlres.message);
                $("#datetimepicker1"+id).val(''); 
                $('#schedule'+id).attr('disabled',true);

            } else {
                 $('#schedule'+id).attr('disabled',false);
                 $("#setval"+id).html("");
                 $("#datetimepicker1"+id).val(getdate);
            }
          }            
        });
    }
</script>
<script>
   function getTime(id) {
         var gettime= $("#datepicker1"+id).val();
         var timefrom= $("#timefromm"+id).val();
         var timetoo= $("#timetoo"+id).val();
         //return false;
         $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
            'data' :'timeValue='+gettime+'&jobId='+id,
            'success':function(htmlres)
            {
              if(htmlres !='') {
                if(htmlres == 1) {
                  $('#schedule'+id).attr('disabled',false);
                  $("#setvall"+id).html("");
                  $("#datepicker1"+id).val(gettime);
                
                } else {
                   $("#setvall"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                   $("#datepicker1"+id).val(''); 
                   $('#schedule'+id).attr('disabled',true);
                }
            }
          }
              
        });
    }
</script>
<script>
   function getdaynamee(id) {
       var getdate= $("#datetimepicker12"+id).val();
       var dayfrom = $("#dayfrommm"+id).val();
       var dayto   = $("#daytooo"+id).val();

       $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
         'data' :'dateValue='+getdate+'&jobId='+id,
         'success':function(htmlres) {

            htmlres = jQuery.parseJSON(htmlres);

            if(htmlres.status == "error") {

                $("#setval12"+id).html(htmlres.message);
                $("#datetimepicker12"+id).val(''); 
                $('#schedulees'+id).attr('disabled',true);

            } else {
                 $('#schedulees'+id).attr('disabled',false);
                 $("#setval12"+id).html("");
                 $("#datetimepicker12"+id).val(getdate);
            }
          }    
        });
    }
</script>

<script>
   function getTimee(id)
      {
         var gettime= $("#datepicker13"+id).val();
         var timefrom= $("#timefrommm"+id).val();
         var timetoo= $("#timetooo"+id).val();
         $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
            'data' :'timeValue='+gettime+'&jobId='+id,
            'success':function(htmlres)
            {
                if(htmlres !='') {
                  if(htmlres == 1) {
                    $('#schedulees'+id).attr('disabled',false);
                    $("#setvall12"+id).html("");
                    $("#datepicker13"+id).val(gettime);
                  
                  } else {
                      $("#setvall12"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                      $("#datepicker13"+id).val(''); 
                      $('#schedulees'+id).attr('disabled',true);
                  }
                }
            }
              
          });
      }
</script>
<script>
   function savedjob(id)
         {
           $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/savedjovData') ?>",
            'data' :'jobId='+id,
            'success':function(htmlres)
            {
               //alert(htmlres);return false;  
               if(htmlres !='')
               {
               if(htmlres == 1)
               {
                  $("#test4"+id).hide();
                  $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Saved" style="color:red;cursor:pointer;"></i>');
                 var msg="Job saved successfully";
                 myFunction(msg);
                }
               if(htmlres == 2)
               {
                 $("#test3"+id).hide();
                 $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Unsaved" style="color:#b5b5b5;cursor:pointer;"></i>');
                 /*var msg="jobs are unsaved successfully";
                 myFunction(msg);*/
                }
                if(htmlres == 3)
               {
                 $("#test4"+id).hide();
                 $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Saved" style="color:red;cursor:pointer;"></i>');
                 var msg="Job saved successfully";
                 myFunction(msg);
               }
            }
            }
              
          });
      }
      

    function instantfunction(id, url) {
      
           $.ajax({
              'type' :'POST',
              'url' :"<?php echo base_url('dashboard/savedinstantData') ?>",
              'data' :'jobId='+id,
              'success':function(htmlres) {
                  window.open(url, '_blank');
              }
          });
    }



   function myFunction(msg) {
    var x = document.getElementById("snackbar");
    x.className = "show";
    $("#snackbar").html('<div class="alert alert-success">'+msg+ '</div>');
    setTimeout(function(){ x.className = x.className.replace("show", "top"); }, 3000);
     }
</script>

<script>
   $(document).ready(function(){   
   $("#changepassbtn").click(function() {
     //var oldpass = $("#oldpass").val();
     var newpass = $("#newpass").val();
     var confpass = $("#confpass").val();
     $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>" + "user/changepass",
         data: {newpass:newpass,confpass:confpass},
         cache:false,
         success:function(htmldata) {
            $('#errorfield').html(htmldata);
         },
         error:function(){
           console.log('error');
         }
     });
   });
   $("input[name='locationSort']").click(function(){
     var location = $('#txtPlaces').val();
     if(location!=''){
       alert('You have already entered location');
       return false;
     }
   });
   
   });
</script>
<script>
   function getFilename(id)
   {
      var name = document.getElementById("file"+id).files[0].name;
      var filetype= $("#file"+id).val();
      var ext = filetype.split('.').pop();
      if(ext =="jpg" || ext =="jpeg" || ext =="png"){
        $("#setimgres"+id).html("<span style='color:red'>please select file format(pdf/doc/docx)</span>");
        return false;
     } 
     else
     {
      $("#setimgres"+id).html("");
      var form_data = new FormData();
      form_data.append("file", document.getElementById('file'+id).files[0]);
      $.ajax({
        url:'<?php echo base_url('dashboard/imageUpload')?>',
        method:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success:function(imagedata)
        { 
          $("#imagedata"+id).val(imagedata);
          $("#file"+id).val('');
          $("#setimgres"+id).html(imagedata);
        }
      });
     }
   }
</script>
<script type="text/javascript">
   function PdfImagesend(id)
   {
     //alert('hi');
     var image= $("#imagedata"+id).val();
       $.ajax({
              'type' :'POST',
              'url' :"<?php echo base_url('dashboard/fileInsert') ?>",
              'data' :'imagedata='+image,
              'success':function(htmlres)
              {
   
                if(htmlres=='1')
                {
                 $("#exampleModalCenterdd"+id).modal('show');
                }
                if(htmlres=='2')
                {
                  $("#exampleModalCenterdd"+id).modal('show');
                }
              }
        });
    }
</script>



<script>
$(".bcnormals").click(function (e) {
    $(".yodserchfilts").removeClass('openyodsrch');
}); 
</script>
<script>
  $(function() {
      $("#myInput").keyup(function(){
        $.ajax({
        type: "POST",
        url: "<?php echo base_url() ?>user/fetchcompany",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
          $("#myInput").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
          $("#suggesstion-box").show();
          $("#suggesstion-box").html(data);
          $("#myInput").css("background","#FFF");
        }
        });
      });
  });
  function selectCountry(val) {
    $("#myInput").val(val);
    $("#suggesstion-box").hide();
  }
</script>

<script>
   function getcatsubcat(id)
   {
     var catid = $.map($('input[name="jobcategoryy[]"]:checked'), function(c){return c.value; })
     $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/getsubcategoryInner",
          data: {catid:catid},
          cache:false,
          success:function(htmldata){
   
            $("#subcatresss").html(htmldata);
              
          },
      });
   }

   $('.loctrack').click(function(){
        if ("geolocation" in navigator){
          navigator.geolocation.getCurrentPosition(function(position){ 
            var currentLatitude = position.coords.latitude;
            var currentLongitude = position.coords.longitude;
            var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
              var geocoder = geocoder = new google.maps.Geocoder();
              geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                      if (results[1]) {
                          var fill_address = results[1].formatted_address;
                          $('#txtPlaces').val(fill_address);
                      }
                  }
              });
            
            $('#lati').val(currentLatitude);
            $('#longi').val(currentLongitude);
            if (localStorage) {
              localStorage.setItem('currentLatitude', currentLatitude);
              localStorage.setItem('currentLongitude', currentLongitude);
           }
          });
        }
      });
</script>
<?php

  if($this->session->tempdata('filtererr')!= null) {
    
    if($this->session->tempdata('filtererr')){$this->session->unset_tempdata('filtererr');}
?>
      <script type="text/javascript">
        $(window).on('load',function(){
           $(".yodserchfilts").addClass('openyodsrch');
        });
        if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
        }
      </script>
<?php
  } if(!empty($resultstatus)){
?>
  <script type="text/javascript">
        $(window).on('load',function(){
           $("#exampleModalCenter6").modal('show');
        });
        if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
        }
      </script>
      <?php }if(!empty($completeness) && $completeness=='Yes'){
?>
  <script type="text/javascript">
        $(window).on('load',function(){
           $("#updateprofileModal").modal('show');
        });
        if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
        }
      </script>
      <?php }?>

      <script type="text/javascript">
  $(".tagsaresdplc .custom-control-label").click(function(){
             $(this).toggleClass("selectedgreen");
         });
</script>
<script type="text/javascript">
if ( window.history.replaceState ) {
window.history.replaceState( null, null, window.location.href );
}
</script>
</body>
</html>

