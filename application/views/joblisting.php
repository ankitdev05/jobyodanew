<?php include_once('header.php'); ?>
      <div class="managerpart">
         <div class="container-fluid">
            <div class="row">
            
               <div class="col-md-9 expanddiv">
                  <div class="mainheadings-yoda">
                     <h5>Jobs</h5>
                     <div class="prosave filink">
                        <div class="combinefilters filrgt">
                           <a href="searchjobs.html"><i class="fas fa-map-marker-alt"></i> Map</a>
                        </div>
                     </div>
                  </div>
                  <div class="adminopnts">
                     <div class="tabledivsdsn">
                        <!-- <div class="row tableshead">
                           <div class="col-md-3 col-lg-3 ">
                              <h6>Applied Jobs</h6>
                           </div>
                           <div class="col-md-6 col-lg-5">
                              <h6>Position</h6>
                           </div>
                           <div class="col-md-3 col-lg-2">
                              <h6>Job Status</h6>
                           </div>
                           <div class="col-md-3 col-lg-2">
                              <h6>Salary</h6>
                           </div>
                        </div> -->
                     <?php
                     //print_r($jobList);die;
                        if($jobList) {
                           $x=30;
                           foreach($jobList as $jobListing) {
                              
                     ?>
                        <div class="row tablesrow">
                           <div class="col-md-3 col-lg-3 ">
                              <h6>Applied Jobs</h6>
                              <div class="imagethumbs">
                              <?php if(!empty($jobListing['jobImg'])){?>
                                 <a href="<?php echo base_url();?>dashboard/jobDescription?listing=<?php echo base64_encode($jobListing['jobpost_id']);?>"><img src="<?php echo $jobListing['jobImg'];?>"></a>
                              <?php }else{?>   
                                 <a href="<?php echo base_url();?>dashboard/jobDescription?listing=<?php echo base64_encode($jobListing['jobpost_id']);?>"><img src="<?php echo base_url().'webfiles/';?>img/thumpost.jpg"></a>
                              <?php }?>   
                              </div>
                           </div>
                           <div class="col-md-6 col-lg-5">
                              <div class="positionarea">
                                 <h6>Position</h6>
                                 <h3><?php echo $jobListing['cname']; ?></h3>
                                 <a href="<?php echo base_url('dashboard/companyProfile/'.base64_encode($jobListing['companyId']))?>">
                                    <h3><?php echo $jobListing['companyName']; ?></h3>
                                   
                                 </a>
                                 <a href="<?php echo base_url();?>dashboard/jobDescription?listing=<?php echo base64_encode($jobListing['jobpost_id']);?>">
                                    <p class="posttypes"><?php echo $jobListing['job_title']; ?></p>
                                 </a>
                                 <p class="locationtype"><img src="<?php echo base_url().'webfiles/';?>img/locmap.png"><?php echo $jobListing['address']; ?></p>
                                 <p><?php echo $jobListing['distance'];?>KM</p>

                                 <p>
                                    <?php
                                        if($this->session->userdata('usersess')) {

                                            if($jobListing['mode'] == "Instant screening") {
                                    ?>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenterInstant<?php echo $x;?>">Apply <?php if($jobListing['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $jobListing['mode']; } ?>  </a>
                                    <?php
                                            } else {
                                    ?>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenterb<?php echo $x;?>">Apply <?php if($jobListing['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $jobListing['mode']; } ?>  </a>
                                    <?php
                                            }

                                        } else {
                                    ?>
                                         <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter4">Apply <?php if($jobListing['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'as '. $jobListing['mode']; } ?>  </a>
                                    <?php
                                        }
                                    ?>
                                    </p>

                              </div>
                           </div>
                           <div class="col-md-3 col-lg-2 lefthalf">
                              <h6>Job Status</h6>
                              <p class="jobstatus">Selected</p>
                           </div>
                           <div class="col-md-9 col-lg-2 lefthalf">
                              <div class="salryedits">
                                 <h6>Salary</h6>
                                 <h4><?php if(!empty($jobListing['salary'])){ echo $jobListing['salary']; ?>/Month <?php } else{ echo "Salary Confidential"; } ?> </h4>
                                 <img src="<?php echo base_url().'webfiles/';?>img/usertick.png" class="usertick">
                                 <img src="<?php echo base_url().'webfiles/';?>img/edits.png" class="usereditr">
                                 <img src="<?php echo base_url().'webfiles/';?>img/userpf.png" class="userpf">
                              </div>
                           </div>
                        </div>

                        <div class="modal fade" id="exampleModalCenterInstant<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                 <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content" style="box-shadow: none;">

                                      <div class="InstantBox">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                          </button>

                                          <!-- <h5>You will now be redirected to the Instant Assessment page. Your time invested now will speed up the hiring process later. JobYoDA wishes you all the best! Remember, 
                                          <span>"Every pro was once an amature. Every expert was once a beginner. So dream big and start now".</span></h5> -->

                                          <h5>
                                            This is far easier than driving around the city applying right!? You will now begin the Instant Assessment of <?php echo $jobListing['companyName']; ?>. They are looking for people like you but they will only know about you once you finish the assessment. Those who finish the assessment quickly, are more likely to get hired!  
                                          </h5>
                                          <p> Best of luck from the JobYoDA team! </p>

                                          <p>
                                            <a class="greenbtn" onclick="instantfunction('<?php echo $jobListing['jobpost_id']; ?>', '<?php echo $jobListing['modeurl']; ?>')" href="javascript:void(0)"> Begin Screening </a>
                                          </p>
                                      </div>
                                    
                                    </div>
                                 </div>
                            </div>

                        <div class="modal fade" id="exampleModalCenterb<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                    </button>
                                 </div>
                                 <div class="modal-body">
                                    <div class="formmidaress modpassfull">
                                       <div class="filldetails">
                                             <div class="addupdatecent">
                                             <form method="post" action="<?php echo base_url();?>dashboard/resumeUploadListing" enctype="multipart/form-data">
                                                <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                                                <input type="file" name="resumeFile">
                                                <?php if (!empty($checkResume[0]['resume'])) { ?>
                                                 <p>Would you like to update Your Resume?</p>
                                                 <?php }else{?>
                                                 <p>Please Add your Resume</p>
                                                 <?php }?>
                                                <div class="statsusdd">
                                                   <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Close</p>
                                                   <p class="norm" class="close" data-toggle="modal" data-target="#exampleModalCenterc<?php echo $x;?>" data-dismiss="modal">Cancel</p>
                                                   <input type="hidden" name="type" value="<?php echo $x;?>">
                                                   <button type="submit" class="updt">Upload</button>
                                                </div>
                                             </form>
                                             </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="modal fade" id="exampleModalCenterc<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                           <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                    </button>
                                 </div>
                                 <div class="modal-body">
                                    <div class="formmidaress modpassfull">
                                       <div class="filldetails">
                                          <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing>
                                             <div class="schedulejobgs">
                                                <h6>Schedule interview</h6>
                                                <p>Selected Date & Time</p>
                                                <div class="forminputspswd">
                                                   <p>Date</p>
                                                   <input type="text" name="scheduledate" class="form-control datetimepicker1" placeholder="mm/dd/yy" required="required">
                                 <i class="far fa-calendar-alt fieldicons"></i>
                                                </div>
                                                <div class="forminputspswd">
                                                   <p>Time</p>
                                                   <input type="text" name="scheduletime" class="form-control bootstrap-timepicker timepicker timepicker1" data-format="hh:mm:ss" required="required"> 
                                                   <i class="far fa-clock"></i>
                                                </div>
                                                <div class="statsusdd">
                                                   <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                   <input type="hidden" name="listing" value="<?php echo $jobListing['jobpost_id'];?>">
                                                   <input type="hidden" name="type" value="<?php echo $x;?>">
                                                   <button type="submit" class="updt">Schedule & Apply</button>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php
                           $x++;
                           }
                        }
                     ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include_once('footer.php'); ?>
      
      <div class="modal fade" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                     <div class="filldetails">
                        <form>
                           <div class="addupdatecent">
                              <img src="img/savedbighover.png">
                              <p>Job Applied Successfully</p>
                              <div class="statsusdd">
                                 <p class="updt" data-dismiss="modal" aria-label="Close">Ok</p>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                     <div class="filldetails">
                        <form>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="Change Password">
                              <img src="img/keypass.png">
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="New Password">
                              <img src="img/keypass.png">
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="Confirm New Password">
                              <img src="img/keypass.png">
                           </div>
                           <button type="submit" class="srchbtns">Change</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="<?php echo base_url().'webfiles/';?>js/jquery.min.js"></script>
      <script src="<?php echo base_url().'webfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'webfiles/';?>js/bootstrap.js"></script>
    
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
      <script src="<?php echo base_url().'webfiles/';?>js/bootstrap-timepicker.js"></script>
      <script type="text/javascript">
         $(function () {
           $(".datetimepicker1").datepicker({ 
                 autoclose: true, 
                 todayHighlight: true
           }).datepicker('update', new Date());
         });
         $('#timepicker1').timepicker();
      </script>
      <?php
         if(isset($_GET["type"]) && $_GET["msg"] == "uploadcomplete") {
      ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                     var idmodal = "#exampleModalCenterc<?php echo $_GET["type"]; ?>";
                     $(idmodal).modal('show');
                });
            </script>
      <?php
         }
      ?>
      <?php
         if(isset($_GET["type"]) && $_GET["msg"] == "scheduled") {
      ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                    $('#exampleModalCenter10').modal('show');
                });
            </script>
      <?php
         }
      ?>
   </body>
   <script type="text/javascript">
      $(".updt").on("click",function(){
            window.location = "<?php echo base_url();?>dashboard/JobListings";
      });
   </script>
   
    <script type="text/javascript">
  
$(".hammenu").click(function (e) {
    e.stopPropagation();
    $(".sideshift").toggleClass('opensidemenu');
});

$(".hammenu").click(function (e) {
    e.stopPropagation();
    $(".hdebarsd").toggleClass('hidebarsd');
});

$(".crossmob").click(function (e) {
    e.stopPropagation();
    $(".sideshift").removeClass('opensidemenu');
});


$('.sideshift').on('click', function () {        
 $(this).toggleClass('opensidemenu');});

$(document).click(function (e) {    
 if (!$(e.target).hasClass('sideshift')) { 
  $('.sideshift.opensidemenu').removeClass('opensidemenu')    
 }})
 
  </script>
</html>

