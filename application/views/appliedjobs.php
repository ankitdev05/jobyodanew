<?php include_once('header.php'); ?>
      <div class="managerpart">
         <div class="container-fluid">
            <div class="row">
               
               <div class="col-md-9 expanddiv">
			   <div class="innerbglay noshad">
                 
                  <div class="adminopnts">
                     <nav class="jobyodatabs">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                           <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home">Past</a>
                           <a class="nav-item nav-link"  id="nav-profile-tab" data-toggle="tab" href="#nav-profile">Upcoming</a>
                        </div>
                     </nav>
                     <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home">
                           <div class="tabledivsdsn">
                              <!-- <div class="row tableshead">
                                 <div class="col-md-3 col-lg-3 ">
                                    <h6>Applied Jobs</h6>
                                 </div>
                                 <div class="col-md-6 col-lg-5">
                                    <h6>Position</h6>
                                 </div>
                                 <div class="col-md-3 col-lg-2">
                                    <h6>Job Status</h6>
                                 </div>
                                 <div class="col-md-3 col-lg-2">
                                    <h6>Salary</h6>
                                 </div>
                              </div> -->
                              <?php 
                            
                              if(!empty($jobPast))
                              {
                                  foreach($jobPast as $jobpastData)
                                  {
                                     
                              ?>
							   <div class="featurerowjob">
                              <div class="row">
                                  <?php if(!empty($jobpastData['jobpost_id'])) {?>
                                 <div class="col-md-3 col-lg-4 tumb">
                                   <!-- <h6>Applied Jobs</h6>-->
                                    <div class="imagethumbs">
                                      <?php 
                                         if(!empty($jobpastData['job_image']))
                                         {
                                      ?>
                                       <div class="thumbimpr"> 
									   <img src="<?php echo $jobpastData['job_image'];?>">
									   </div>
                                     <?php }
                                       else
                                       {
                                        ?>
                                       
                                        <div class="thumbimpr"> <img src="<?php echo base_url().'webfiles/';?>img/thumpost.jpg">
										</div>
                                        <?php 
                                       }
                                     ?>
                                    </div>
                                 </div>
                                 <?php }?>
                                 <div class="col-md-6 col-lg-5">
                                    <div class="positionarea">
                                       <!--<h6>Position</h6>-->
                                       <a href="<?php echo base_url();?>dashboard/jobDescription?type=applied&listing=<?php echo base64_encode($jobpastData['jobpost_id']);?>">
                                     <!--  <a href="jobdescription.html">-->
                                          <p class="posttypes"><?php echo $jobpastData['job_title'];?></p>
                                       </a>
									                     
                                       <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($jobpastData['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($jobpastData['comapnyId']); }?>">

                                          <h3><?php echo $jobpastData['companyName']; ?></h3>
                                       </a>
									   
                                       <p class="posttypes dire"><a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($jobpastData['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($jobpastData['recruiter_id']); }?>"> <?php echo $jobpastData['cname'];?>  </a> </p>
                                       
                                         
                                       <?php if(!empty($jobpastData['companyAddress'])) {?>
                                       <p class="locationtype"><img src="<?php echo base_url().'webfiles/';?>img/locmap.png"><?php echo $jobpastData['companyAddress']; ?></p>
                                       
                                       <p class="" style="font-size:12px; display:none;"><?php echo $jobpastData['jobPitch'];?></p>
                                       
									    <div class="salicoftr"> 
                                      <?php
                                    if(!empty($jobpastData['toppicks1']))
                                      {
                                       if($jobpastData['toppicks1']=='1')
                                       {
                                       ?>
                                       <div class="salry">
                                      <img title="Joining Bonus" src="<?php echo base_url();?>recruiterfiles/images/m_bonus.png">
                                      <span>Joining Bonus</span>
                                      </div>
                                       <?php 
                                           
                                       }
                                       
                                       if($jobpastData['toppicks1']=='2')
                                       {
                                       ?>
                                     <div class="salry">
                                      <img title="Free Food" src="<?php echo base_url();?>recruiterfiles/images/m_freefood.png">
                                      <span>Free Food</span>
                                      </div>
                                       <?php }
                                        if($jobpastData['toppicks1']=='3')
                                         {
                                       ?>
                                      <div class="salry">
                                      <img title="Day 1 HMO" src="<?php echo base_url();?>recruiterfiles/images/m_day_1_hmo.png">
                                       <span>Day 1 HMO</span>
                                       </div>
                                        <?php }
                                         if($jobpastData['toppicks1']=='4')
                                         {
                                        ?>
                                        <div class="salry">
                                           <img title="Day 1 HMO for Dependent" src="<?php echo base_url();?>recruiterfiles/images/m_dependent_hmo.png">
                                           <span>Day 1 HMO for Dependent</span>
                                           </div>
                                      <?php }
                                        if($jobpastData['toppicks1']=='5')
                                        {
                                          ?>
                                          <div class="salry">
                                           <img title="Day Shift" src="<?php echo base_url();?>recruiterfiles/images/m_dayshift.png">
                                           <span>Day Shift</span>
                                           </div>
                                        <?php
                                        }
                                        if($jobpastData['toppicks1']=='6')
                                        {
                                        ?>
                                        <div class="salry">
                                         <img title="14th Month Pay" src="<?php echo base_url();?>recruiterfiles/images/m_14th_pay.png">
                                         <span>14th Month Pay</span>
                                         </div>
                                        <?php
                                        }}


                                    if(!empty($jobpastData['toppicks2']))
                                      {
                                       if($jobpastData['toppicks2']=='1')
                                       {
                                       ?>
                                       <div class="salry">
                                     <img title="Joining Bonus" src="<?php echo base_url();?>recruiterfiles/images/m_bonus.png">
                                     <span>Joining Bonus</span>
                                     </div>
                                       <?php 
                                           
                                       }
                                       
                                       if($jobpastData['toppicks2']=='2')
                                       {
                                       ?>
                                     <div class="salry">
                                       <img title="Free Food" src="<?php echo base_url();?>recruiterfiles/images/m_freefood.png">
                                       <span>Free Food</span>
                                       </div>
                                       <?php }
                                        if($jobpastData['toppicks2']=='3')
                                         {
                                       ?>
                                      <div class="salry">                                      
                                      <img title="Day 1 HMO" src="<?php echo base_url();?>recruiterfiles/images/m_day_1_hmo.png">
                                      <span>Day 1 HMO</span>
                                      </div>
                                        <?php }
                                         if($jobpastData['toppicks2']=='4')
                                         {
                                        ?>
                                        <div class="salry">
                                      <img title="Day 1 HMO for Dependent" src="<?php echo base_url();?>recruiterfiles/images/m_dependent_hmo.png">
                                      <span>Day 1 HMO for Dependent</span>
                                      </div>
                                      <?php }
                                        if($jobpastData['toppicks2']=='5')
                                        {
                                          ?>
                                          <div class="salry">
                                          <img title="Day Shift" src="<?php echo base_url();?>recruiterfiles/images/m_dayshift.png">
                                          <span>Day Shift</span>
                                          </div>
                                        <?php
                                        }
                                        if($jobpastData['toppicks2']=='6')
                                        {
                                        ?>
                                        <div class="salry">
                                        <img title="14th Month Pay" src="<?php echo base_url();?>recruiterfiles/images/m_14th_pay.png">
                                        <span>14th Month Pay</span>
                                        </div>
                                        <?php
                                        }}


                                    if(!empty($jobpastData['toppicks3']))
                                      {
                                       if($jobpastData['toppicks3']=='1')
                                       {
                                       ?>
                                       <div class="salry">
                                      <img title="Joining Bonus" src="<?php echo base_url();?>recruiterfiles/images/m_bonus.png">
                                      <span>Joining Bonus</span>
                                      </div>
                                       <?php 
                                           
                                       }
                                       
                                       if($jobpastData['toppicks3']=='2')
                                       {
                                       ?>
                                     <div class="salry">
                                      <img title="Free Food" src="<?php echo base_url();?>recruiterfiles/images/m_freefood.png">
                                       <span>Free Food</span>
                                       </div>
                                       <?php }
                                        if($jobpastData['toppicks3']=='3')
                                         {
                                       ?>
                                      
                                      <div class="salry">
                                       <img title="Day 1 HMO" src="<?php echo base_url();?>recruiterfiles/images/m_day_1_hmo.png">
                                       <span>Day 1 HMO</span>
                                       </div>
                                        <?php }
                                         if($jobpastData['toppicks3']=='4')
                                         {
                                        ?>
                                        <div class="salry">
                                       <img title="Day 1 HMO for Dependent" src="<?php echo base_url();?>recruiterfiles/images/m_dependent_hmo.png">

                                       <span>Day 1 HMO for Dependent</span>
                                       </div>
                                      <?php }
                                        if($jobpastData['toppicks3']=='5')
                                        {
                                          ?>
                                          <div class="salry">
                                         <img title="Day Shift" src="<?php echo base_url();?>recruiterfiles/images/m_dayshift.png">
                                         <span>Day Shift</span>
                                         </div>
                                        <?php
                                        }
                                        if($jobpastData['toppicks3']=='6')
                                        {
                                        ?>
                                        <div class="salry">
                                        <img title="14th Month Pay" src="<?php echo base_url();?>recruiterfiles/images/m_14th_pay.png">
                                        <span>14th Month Pay</span>
                                        </div>
                                        <?php
                                        }}
                                      ?> 
                                    </div>
									
									<!-- <p class="posttypes drdmn"><i class="fas fa-map-marked-alt"></i><?php echo $jobpastData['distance'];?>KM</p> -->
									
                                       <?php }?>
                                    </div>
                                    <?php if(!empty($jobpastData['distance'])){?>
                                       <p class="posttypes drdmn"><i class="fas fa-map-marked-alt"></i><?php echo $jobpastData['distance'];?>KM</p>
                                       <?php }else{?>
                                       <img class="bx_img" src="<?php echo base_url();?>webfiles/img/emptybx.png">
                                       <p class="posttypes">No Data Found in Past Jobs</p>
                                       <?php } ?>
                                 </div>
                                 <div class="col-md-3 col-lg-3 lefthalf">
								 
								                  <div class="salryedits">
                                      <!-- <h6>Salary</h6>-->
                                     <?php  
                                     $mynumber=substr($jobpastData['salary'], 0, 2);
                                     if(!empty($jobpastData['salary'])){
                                      ?>
                                       <h4><?php echo $jobpastData['salary']; ?>/Month</h4>
                                       <?php 
                                      }else{?>
                                       <!-- <h4>Salary confidential</h4> -->
                                       <?php }?>
									  
									   <!--<h6>Job Status</h6>-->
                                    <p class="jobstatus">
                                    <?php if($jobpastData['status']=='2')
                                         {
                                             echo 'Fall Out';
                                         }
                                         else if($jobpastData['status']=='3')
                                         {
                                             echo 'Fall Out';
                                         }
                                         else if($jobpastData['status']=='4')
                                         {
                                           echo 'Refer';
                                         }else if($jobpastData['status']=='5')
                                         {
                                           echo 'On Going Application';
                                         }else if($jobpastData['status']=='6')
                                         {
                                           echo 'Accepted JO';
                                         }else if($jobpastData['status']=='7')
                                         {
                                           echo 'Hired';
                                         }
                                    ?>    
                                    </p>
                                 </div>
								 
                                  
                                 </div>
                                 
                              </div>
							  </div>
                         <?php }}?>
                              
                           </div>
                        </div>
                        <div class="tab-pane fade show active" id="nav-profile" style="display:none">
                           <div class="tabledivsdsn">
                              <!-- <div class="row tableshead">
                                 <div class="col-md-3 col-lg-3 ">
                                    <h6>Applied Jobs</h6>
                                 </div>
                                 <div class="col-md-6 col-lg-5">
                                    <h6>Position</h6>
                                 </div>
                                 <div class="col-md-3 col-lg-2">
                                    <h6>Job Status</h6>
                                 </div>
                                 <div class="col-md-3 col-lg-2">
                                    <h6>Salary</h6>
                                 </div>
                              </div> -->
                               <?php 
                            
                              if(!empty($jobUpcoming))
                              {
                                
                                  foreach($jobUpcoming as $jobUpcomingData)
                                  {
                                
                              ?>
							  <div class="featurerowjob">
                              <div class="row">
                                <?php  if($jobUpcomingData['jobpost_id'] !=''){ ?>
                                  
                                 <div class="col-md-3 col-lg-4 tumb">
                                    <!--<h6>Applied Jobs</h6>-->
                                    
                                    <div class="imagethumbs">
                                     <?php 
                                         if(!empty($jobUpcomingData['job_image']))
                                         {
                                      ?>
                                       <div class="thumbimpr"> <img src="<?php echo $jobUpcomingData['job_image'];?>">
									   </div>
                                     <?php }
                                       else
                                       {
                                        ?>
                                       
                                        <div class="thumbimpr"> <img src="<?php echo base_url().'webfiles/';?>img/thumpost.jpg">
										</div>
                                        <?php 
                                       }
                                     ?>
                                    </div>
                                 </div>
                                 <?php }?>
                                 <div class="col-md-6 col-lg-5">
                                    <div class="positionarea">
                                    <!--   <h6>Position</h6>-->
                                    <a href="<?php echo base_url();?>dashboard/jobDescription?type=applied&listing=<?php echo base64_encode($jobUpcomingData['jobpost_id']);?>">
                                      <!-- <a href="jobdescription.html">-->
                                          <p class="posttypes"><?php echo $jobUpcomingData['job_title'];?></p>
                                       </a>
									                     
                                       <a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($jobUpcomingData['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($jobUpcomingData['comapnyId']); }?>">

                                          <h3><?php echo $jobUpcomingData['companyName']; ?></h3>
                                       </a>
									   
                                       <p class="posttypes"> <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($jobUpcomingData['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($jobUpcomingData['recruiter_id']); }?>"> <?php echo $jobUpcomingData['cname'];?> </a> </p>
                                       
                                      
                                          <p class="" style="font-size:12px; display:none;"><?php echo $jobUpcomingData['jobPitch'];?></p>
                        <?php if(!empty($jobUpcomingData['companyAddress'])){?>
                                       <p class="locationtype"><img src="<?php echo base_url().'webfiles/';?>img/locmap.png">
                                       
                                       <?php echo $jobUpcomingData['companyAddress']; ?></p>
									   
									    <div class="salicoftr"> 
                                      <?php
                                    if(!empty($jobUpcomingData['toppicks1']))
                                      {
                                       if($jobUpcomingData['toppicks1']=='1')
                                       {
                                       ?>
                                       <div class="salry">
                                      <img title="Joining Bonus" src="<?php echo base_url();?>recruiterfiles/images/m_bonus.png">
                                      <span>Joining Bonus</span>
                                      </div>
                                       <?php 
                                           
                                       }
                                       
                                       if($jobUpcomingData['toppicks1']=='2')
                                       {
                                       ?>
                                     <div class="salry">
                                      <img title="Free Food" src="<?php echo base_url();?>recruiterfiles/images/m_freefood.png">
                                      <span>Free Food</span>
                                      </div>
                                       <?php }
                                        if($jobUpcomingData['toppicks1']=='3')
                                         {
                                       ?>
                                      <div class="salry">
                                      <img title="Day 1 HMO" src="<?php echo base_url();?>recruiterfiles/images/m_day_1_hmo.png">
                                       <span>Day 1 HMO</span>
                                       </div>
                                        <?php }
                                         if($jobUpcomingData['toppicks1']=='4')
                                         {
                                        ?>
                                        <div class="salry">
                                           <img title="Day 1 HMO for Dependent" src="<?php echo base_url();?>recruiterfiles/images/m_dependent_hmo.png">
                                           <span>Day 1 HMO for Dependent</span>
                                           </div>
                                      <?php }
                                        if($jobUpcomingData['toppicks1']=='5')
                                        {
                                          ?>
                                          <div class="salry">
                                           <img title="Day Shift" src="<?php echo base_url();?>recruiterfiles/images/m_dayshift.png">
                                           <span>Day Shift</span>
                                           </div>
                                        <?php
                                        }
                                        if($jobUpcomingData['toppicks1']=='6')
                                        {
                                        ?>
                                        <div class="salry">
                                         <img title="14th Month Pay" src="<?php echo base_url();?>recruiterfiles/images/m_14th_pay.png">
                                         <span>14th Month Pay</span>
                                         </div>
                                        <?php
                                        }}


                                    if(!empty($jobUpcomingData['toppicks2']))
                                      {
                                       if($jobUpcomingData['toppicks2']=='1')
                                       {
                                       ?>
                                       <div class="salry">
                                     <img title="Joining Bonus" src="<?php echo base_url();?>recruiterfiles/images/m_bonus.png">
                                     <span>Joining Bonus</span>
                                     </div>
                                       <?php 
                                           
                                       }
                                       
                                       if($jobUpcomingData['toppicks2']=='2')
                                       {
                                       ?>
                                     <div class="salry">
                                       <img title="Free Food" src="<?php echo base_url();?>recruiterfiles/images/m_freefood.png">
                                       <span>Free Food</span>
                                       </div>
                                       <?php }
                                        if($jobUpcomingData['toppicks2']=='3')
                                         {
                                       ?>
                                      <div class="salry">
                                      <img title="Day 1 HMO" src="<?php echo base_url();?>recruiterfiles/images/m_day_1_hmo.png">
                                      <span>Day 1 HMO</span>
                                      </div>
                                        <?php }
                                         if($jobUpcomingData['toppicks2']=='4')
                                         {
                                        ?>
                                        <div class="salry">
                                      <img title="Day 1 HMO for Dependent" src="<?php echo base_url();?>recruiterfiles/images/m_dependent_hmo.png">
                                      <span>Day 1 HMO for Dependent</span>
                                      </div>
                                      <?php }
                                        if($jobUpcomingData['toppicks2']=='5')
                                        {
                                          ?>
                                          <div class="salry">
                                          <img title="Day Shift" src="<?php echo base_url();?>recruiterfiles/images/m_dayshift.png">
                                          <span>Day Shift</span>
                                          </div>
                                        <?php
                                        }
                                        if($jobUpcomingData['toppicks2']=='6')
                                        {
                                        ?>
                                        <div class="salry">
                                        <img title="14th Month Pay" src="<?php echo base_url();?>recruiterfiles/images/m_14th_pay.png">
                                        <span>14th Month Pay</span>
                                        </div>
                                        <?php
                                        }}


                                    if(!empty($jobUpcomingData['toppicks3']))
                                      {
                                       if($jobUpcomingData['toppicks3']=='1')
                                       {
                                       ?>
                                       <div class="salry">
                                      <img title="Joining Bonus" src="<?php echo base_url();?>recruiterfiles/images/m_bonus.png">
                                      <span>Joining Bonus</span>
                                      </div>
                                       <?php 
                                           
                                       }
                                       
                                       if($jobUpcomingData['toppicks3']=='2')
                                       {
                                       ?>
                                     <div class="salry">
                                      <img title="Free Food" src="<?php echo base_url();?>recruiterfiles/images/m_freefood.png">
                                       <span>Free Food</span>
                                       </div>
                                       <?php }
                                        if($jobUpcomingData['toppicks3']=='3')
                                         {
                                       ?>
                                      
                                      <div class="salry">
                                       <img title="Day 1 HMO" src="<?php echo base_url();?>recruiterfiles/images/m_day_1_hmo.png">
                                       <span>Day 1 HMO</span>
                                       </div>
                                        <?php }
                                         if($jobUpcomingData['toppicks3']=='4')
                                         {
                                        ?>
                                        <div class="salry">
                                       <img title="Day 1 HMO for Dependent" src="<?php echo base_url();?>recruiterfiles/images/m_dependent_hmo.png">
                                       <span>Day 1 HMO for Dependent</span>
                                       </div>
                                      <?php }
                                        if($jobUpcomingData['toppicks3']=='5')
                                        {
                                          ?>
                                          <div class="salry">
                                         <img title="Day Shift" src="<?php echo base_url();?>recruiterfiles/images/m_dayshift.png">
                                         <span>Day Shift</span>
                                         </div>
                                        <?php
                                        }
                                        if($jobUpcomingData['toppicks3']=='6')
                                        {
                                        ?>
                                        <div class="salry">
                                        <img title="14th Month Pay" src="<?php echo base_url();?>recruiterfiles/images/m_14th_pay.png">
                                        <span>14th Month Pay</span>
                                        </div>
                                        <?php
                                        }}
                                      ?>
									   <div class="salry">
                                     
									   </div>
                                    </div>
                                       <?php }?>
                                    </div>
									
									  <span style="font-size:12px;"><?php echo $jobUpcomingData['interviewdate'].' '.$jobUpcomingData['interviewtime'];  ?></span>
									  
									  <?php if(!empty($jobUpcomingData['distance'])){?>
                                         <p class="posttypes drdmn"><i class="fas fa-map-marked-alt"></i> <?php echo $jobUpcomingData['distance'];?>KM</p>
                                         <?php }else{?>
                                         <img class="bx_img" src="<?php echo base_url();?>webfiles/img/emptybx.png">
                                         <p class="posttypes">No Data Found in Upcoming Jobs</p>
                                         <?php }?>
                                 </div>
                                 <div class="col-md-3 col-lg-3 lefthalf appset">
								  <div class="salryedits">
                                   <!-- <h6>Job Status</h6>-->
                                    <!-- <p class="jobstatus">
									
									 <div class="salryedits">
                                     <!--  <h6>Salary</h6>-->
                                     <?php  
                                     //print_r($jobUpcomingData);
									 
                                     $mynumber=substr($jobUpcomingData['salary'], 0, 2);
                                     if(!empty($jobUpcomingData['salary'])){
                                      ?>
                                       <h4><?php echo $jobUpcomingData['salary']; ?>/Month</h4>
                                       <?php 
                                      }else{?>
                                       <!-- <h4>Salary confidential</h4> -->
                                      <?php }?> 
									 
									
									<p class="jobstatus">
                                    <?php if($jobUpcomingData['status']=='0')
                                         {
                                             echo 'Not Applied';
                                         }
                                         else if($jobUpcomingData['status']=='1')
                                         {
                                             echo 'Schedule';
                                         }
                                         else if($jobUpcomingData['status']=='2')
                                         {
                                             echo 'On-hold';
                                         }
                                         else if($jobUpcomingData['status']=='3')
                                         {
                                             echo 'Reject';
                                         }
                                         else if($jobUpcomingData['status']=='4')
                                         {
                                           echo 'Selected';
                                         }
                                    ?>    
                                    </p> 
									</div>
                                 </div>
                                
                              </div>
							  </div>
                              <?php }}?>
                             
                              
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
              </div>
            </div>
         </div>
      </div>
      
      
      <?php include_once('footer.php'); ?>
      
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <span id="errorfield"></span>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                 <div class="filldetails">
                   <!-- <form method="post" action="<?php echo base_url('user/changepass') ?>">-->
                       <!--<div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Enter Old Password" name="oldpass" id="oldpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>-->
                       <div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Enter New Password" name="newpass" id="newpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>
                       <div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Confirm New Password" name="confpass" id="confpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>
                       <button type="button" id="changepassbtn" class="srchbtns">Change</button>
                   <!-- </form>-->
                 </div>
              </div>
               </div>
            </div>
         </div>
      </div>
    
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
   <!--   <script src="<?php echo base_url().'webfiles/';?>js/bootstrap-timepicker.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url();?>recruiterfiles/js/jquery.timepicker.min.js"></script>
     <script type="text/javascript">
         $(document).ready(function(){
            $("#nav-home-tab").on("click", function(){
                $("#nav-home-tab").addClass("active");
                $("#nav-profile-tab").removeClass("active");
                $("#nav-home").css("display","block");
                $("#nav-profile").css("display","none");
            });

            $("#nav-profile-tab").on("click", function(){
                  $("#nav-home-tab").removeClass("active");
                  $("#nav-profile-tab").addClass("active");
                  $("#nav-profile").css("display","block");
                  $("#nav-home").css("display","none");
            
            });
         });
      </script>
         <script>
    $(document).ready(function(){   
    $("#changepassbtn").click(function() {
      //var oldpass = $("#oldpass").val();
      var newpass = $("#newpass").val();
      var confpass = $("#confpass").val();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/changepass",
          data: {newpass:newpass,confpass:confpass},
          cache:false,
          success:function(htmldata){
          
             // console.log(data);
             // var logdone = "logindone";
             // var n = data.includes(logdone);
              //if(n) {
             //   window.location = "<?php echo base_url(); ?>dashboard";
             // } else {
               $('#errorfield').html(htmldata);
              //}
          },
          error:function(){
            console.log('error');
          }
      });
  });
});
</script>
    
   </body>
</html>

