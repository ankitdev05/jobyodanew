<?php //print_r($jobImg);die;?>

<?php include_once ('header.php'); $this->session->set_userdata('previous_url_again', current_url());?>

 <style type="text/css">
   
   .JobDescriptionScroll{float: left; width: 100%; height: 400px; overflow: auto; margin: 0 0 50px 0; padding: 0 20px 0 0;}
   .managerpart .jobdeskyod .jobtxtsdesc.rfed.fullph .greenbtn{background-color:#fbaf31;color:#fff;padding:9px 25px;border-radius:5px;font-family:Roboto;font-size:14px;display:inline-block; box-shadow: none;}
.managerpart .jobdeskyod .jobtxtsdesc.rfed.fullph .greenbtn:hover{background-color:#00a94f}
.jobtxtsdesc.picsd .provd.usericonds p{
    font-family: Roboto;
    font-size: 12px;
    font-weight: 500;
    letter-spacing: 0.4px;
    margin: 15px 0 0 0 !important;
    line-height: 20px;
}

.jobtxtsdesc p {
    font-size: 14px;
    color: #787878;
    font-family: Roboto;
    line-height: 26px;
}

</style>


<div class="managerpart">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 expanddiv jobdeskyod" style="padding-bottom: 40px;">
         <div class="innerbglay" style="padding:0px;">
            <!-- <div class="mainheadings-yoda" style="opacity:0;">
               <h5>Job Description</h5>
            </div> -->

               <div class="adminopnts">
			   <div class="row">
			   <div class="col-md-6">
                <section id="demos" class="agentssliders propertcomps">
               <div class="owl-carousel owl-theme">
                  <?php if(!empty($jobImg)){ foreach ($jobImg as $images) {
                     # code...
                  ?>
                    <div class="item">
                       <div class="innersliders" style="margin-top:0;">
                          
                          <img src="<?php echo $images['pic']; ?>">
                         
                          
                       </div>
                    </div>
                    <?php }}else{ ?>
                   <div class="item">
                       <div class="innersliders" style="margin-top:0;">
                          
                          <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                         
                          
                       </div>
                    </div>

                     
                    <?php }?>
       
               </div>
            </section> 
			</div>
			
			<div class="col-md-6">
			 <div class="adminopnts jbdtprt">
			<div class="jobsetstype">
                  <div class="jobinfodtls">
                     <p class="positionlok"><?php echo $jobdetail['job_title']; ?></p>
                     
                     <p class="organisationm"><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($comapnyDetail['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($comapnyDetail['comapnyId']); }?>"><?php echo $comapnyDetail['name']; ?></a></p>

                     <p class="organisationm recnmes"> <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($comapnyDetail['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($comapnyDetail['recruiter_id']); }?>"> <?php echo $comapnyDetail['recruiterName']; ?> </a> </p>
                  </div>
					<div class="sharethis-inline-share-buttons" style="float:right;"></div>
               </div>
			   
			     <div class="customesets">
                  <div class="provd locnwdth">
                     <img src="<?php echo base_url() . 'webfiles/'; ?>img/mapsm.png"> 
                     <p><?php echo $jobdetail['location']; ?></p>
                  </div>
                  <div class="provd">
                     <img src="<?php echo base_url() . 'webfiles/'; ?>img/openings.png"> 
                     <p><?php echo $jobdetail['opening']; ?><?php if($jobdetail['opening']>1){ echo "Openings"; }else{ echo "Opening"; } ?></p>
                  </div>
                  <div class="provd">
                     <img src="<?php echo base_url() . 'webfiles/'; ?>img/bagsm.png"> 
                     <p><?php echo $jobdetail['experience']; ?> </p>
                  </div>
                  <div class="provd">
                     <img src="<?php echo base_url() . 'webfiles/'; ?>img/salsm.png">
                     <?php
                     $total_sal = $jobdetail['salary']??0 + $jobdetail['allowances']??0;
                        $allow = $total_sal/1000;
                        if (!empty($allow)) {
                        ?>
                     <p><?php echo $allow; ?>k/Month</p>
                     <?php
                        }else{ ?>
                     <p>Confidential</p>
                     <?php }?>
                  </div>
                  
               </div>
			    <div class="jobtxtsdesc basictextsal">
                  <p>Basic Salary : <?php 
                        if (!empty($jobdetail['salary'])) {echo $jobdetail['salary']; 
                        ?>/Month <?php }else{ echo "Confidential"; }?></p>
                  <p>Total Guaranteed Allowance : <?php if(!empty($jobdetail['allowances'])){ echo $jobdetail['allowances']; ?>/Month <?php }else{ echo "Confidential"; }?>  </p>
               </div>
			   
			            <div class="jobtxtsdesc rfed fullph"> 
                        <div class="contopsd">
                           <h4>Contact</h4>
                           <div class="pincts"> 
                              <p>
                                 <a href='tel:" <?php echo '+'. $jobdetail['phonecode'].' '. $jobdetail['companyphone']; ?>"'>
                                    <img src="<?php echo base_url(); ?>webfiles/img/phonetag.png" class="acthover" style="width: 20px;"><?php echo '+'. $jobdetail['phonecode'].' '. $jobdetail['companyphone']; ?>
                                 </a>
                              </p>
                              <?php if(!empty($jobdetail['landline'])){?>
                              
                              <p>
                                 <a href='tel:" <?php echo $jobdetail['landline']; ?>"'>
                                    <img src="<?php echo base_url(); ?>webfiles/img/conta.png" class="acthover" style="width: 20px;"><?php if(!empty($jobdetail['landline'])){ echo $jobdetail['landline']; }?>
                                 </a>
                              </p>
                              <?php }?>
                              
                              <p>
                                 <a href='mailto:" <?php echo $jobdetail['email']; ?>"'>
                                    <img src="<?php echo base_url(); ?>webfiles/img/mail-2.png" class="acthover" style="width: 20px;"><?php echo $jobdetail['email']; ?>
                                 </a>
                              </p>
                           </div> 
                        </div> 
                        <button class="greenbtn" data-toggle="modal" data-target="#exampleModalCenter4">Apply <?php if($jobdetail['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $jobdetail['mode']; } ?></button>
                     </div>
			         </div>
			      </div>
			   </div>
         </div>

            <div class="adminopnts jobdtlfs">
               <div class="row">

                  <div class="col-sm-6">
                     <div class="JobDescriptionScroll">
                        <div class="jobtxtsdesc"> 
                           <h4>Job Pitch</h4>
                           <p><?php echo $jobdetail['jobPitch']; ?></p>
                        </div>

                        <div class="jobtxtsdesc">
                           <h4>Job Description</h4>
                           <p><?php echo $jobdetail['jobDesc']; ?></p>
                        </div>

                        <?php if(!empty($jobskills)){?>

                        <div class="jobtxtsdesc">
                           <h4>Skill Required</h4>
                           <ul>
                              <?php
                                 foreach ($jobskills as $jobskill) {
                                 ?>
                              <li><?php echo $jobskill['skill']; ?></li>
                              <?php
                                 }
                                 ?>
                           </ul>
                        </div>

                        <?php }?>
                        <?php if(!empty($jobdetail['education'])){?>

                        <div class="jobtxtsdesc">
                           <h4>Qualification Required</h4>
                           <ul>
                              <li><?php echo $jobdetail['education']; ?></li>
                           </ul>
                        </div>
                        <?php if (!empty($jobTop)) {?>
                     </div>
                  </div> 

                  <div class="col-sm-6">
                     <div class="JobDescriptionScroll">
                        <div class="jobtxtsdesc picsd">
                           <h4>Top Picks</h4>
                           
                           <div class="provd usericonds">
                              <?php
                                 if (!empty($jobTop)) {
                                 foreach($jobTop as $jobTops) {
                                    if ($jobTops['picks_id'] == '1') {
                              ?>
                                      <li> 
         							 <div class="icodtds">
         							 <img title="Joining Bonus" src="<?php echo base_url(); ?>webfiles/img/toppics/joining-bonus.png">
         							 </div>
         							 
                                       <p>Joining <br>Bonus</p>
         							  </li>
                              <?php
                                    }
                                    if ($jobTops['picks_id'] == '2') {
                              ?>      <li> 
         							<div class="icodtds">
                                       <img title="Free Food" src="<?php echo base_url(); ?>webfiles/img/toppics/free-food.png">
         							 </div>
                                       <p>Free <br>Food</p>
         							 </li>
                              <?php
                                    }
                                    if ($jobTops['picks_id'] == '3') {
                              ?>         <li>
         							<div class="icodtds">
                                       <img title="Day 1 HMO" src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo.png">
         							 </div>
         							 
                                       <p>Day 1 <br>HMO</p>
         							  </li>
                              <?php
                                    }
                                    if ($jobTops['picks_id'] == '4') {
                              ?>  
         							<li>
         							<div class="icodtds">
                                       <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo-for-depended.png">
         							</div>
                                       <p>Day 1 HMO <br>for Dependent</p>
         							</li>
                              <?php
                                    }
                                    if ($jobTops['picks_id'] == '5') {
                              ?>        <li>
         							<div class="icodtds">
                                       <img title="Day Shift" src="<?php echo base_url(); ?>webfiles/img/toppics/day-shift.png">
         							 </div>
         							 
                                       <p>Day <br>Shift</p>
         							  </li>
                              <?php
                                    }
                                    if ($jobTops['picks_id'] == '6') {
                              ?>
                                       <li>
         							<div class="icodtds">
         							<img title="14th Month Pay" src="<?php echo base_url(); ?>webfiles/img/toppics/14-month-pay.png">
         							</div>
                                       <p>14th Month<br> Pay</p>
         							 </li>
                              <?php
                                    }
                                 }
                                 }
                              ?>
                              
                           </div>
                        </div>

                        <?php } if (!empty($jobAllowance)) {?>
                     
                        <div class="jobtxtsdesc picsd">
                           <h4>Allowances and Incentives</h4>
                           
                           <div class="provd usericonds">
                              <?php
                                 if (!empty($jobAllowance)) {
                                 foreach($jobAllowance as $jobAllowances) {
                                    if ($jobAllowances['allowances_id'] == '1') {
                              ?>
         							 <li>
         							<div class="icodtds">
                                       <img title="Cell Phone Allowances" src="<?php echo base_url(); ?>webfiles/img/toppics/cell-phone-allowance.png">
         							 </div>
                                       <p>Cell Phone<br> Allowance</p>
         							 </li>
                              <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '2') {
                              ?>       <li>
         							<div class="icodtds">
                                       <img title="Annual Performance Bonus" src="<?php echo base_url(); ?>webfiles/img/toppics/annual--performance-bonus.png">
         							</div>
                                       <p>Annual <br>Performance Bonus</p>
         							  </li>
                              <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '3') {
                              ?>        <li>
         							<div class="icodtds">
                                       <img title="Free Parking" src="<?php echo base_url(); ?>webfiles/img/toppics/free-parking.png">
         							</div>
                                       <p>Free <br>Parking</p>
         							 </li>
                              <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '4') {
                              ?>  
         							 <li>
         							<div class="icodtds">
                                       <img title="Free Shuttle" src="<?php echo base_url(); ?>webfiles/img/toppics/free-shuttle.png">
         							</div>
                                       <p>Free <br>Shuttle</p>
         							 </li>
                              <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '5') {
                              ?>
         							 <li>
         							<div class="icodtds">
                                       <img title="Retirement Benefits" src="<?php echo base_url(); ?>webfiles/img/toppics/reteirment-benifits.png">
         							</div>
                                       <p>Retirement <br>Benefits</p>
         							 </li>
                              <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '6') {
                              ?>
         							<li>
         							<div class="icodtds">
                                       <img title="Transport Allowance" src="<?php echo base_url(); ?>webfiles/img/toppics/transportation.png">
         							</div>
                                       <p>Transport<br> Allowance</p>
         							 </li>
                              <?php
                                    }
                                    if ($jobAllowances['allowances_id'] == '7') {
                              ?>
         								<li>
         							<div class="icodtds">
                                       <img title="Monthly Performance Incentive" src="<?php echo base_url(); ?>webfiles/img/toppics/monthly-performance-incentive.png">
         							</div>
                                       <p>Monthly <br>Performance Incentive</p>
         							  </li>
                              <?php
                                    }
                                 }
                                 }
                              ?>
                              
                           </div>
                        </div>

                        <?php } if (!empty($jobMedical)) {?>

                        <div class="jobtxtsdesc picsd">
                           <h4>Medical Benefits</h4>
                           
                           <div class="provd usericonds">
                              <?php
                                 if (!empty($jobMedical)) {
                                 foreach($jobMedical as $jobMedicals) {
                                    if ($jobMedicals['medical_id'] == '1') {
                              ?>
         							<li>
         							<div class="icodtds">
                                       <img title="Medicine Reimbursement" src="<?php echo base_url(); ?>webfiles/img/toppics/medicine-reimbursemer.png">
         							 </div>
                                       <p>Medicine <br>Reimbursement</p>
         							 </li>
                              <?php
                                    }
                                    if ($jobMedicals['medical_id'] == '2') {
                              ?>
         							<li>
         							<div class="icodtds">
                                       <img title="Free HMO for Dependents Allowances" src="<?php echo base_url(); ?>webfiles/img/toppics/free-hmo-for-dependents.png">
         							  </div>
                                       <p>Free HMO for <br>Dependents</p>
         							  </li>
                              <?php
                                    }
                                    if ($jobMedicals['medical_id'] == '3') {
                              ?>
         							<li>
         							<div class="icodtds">
                                       <img title="Critical illness Benefits" src="<?php echo base_url(); ?>webfiles/img/toppics/critical-illness-benefits.png">
         							</div>
                                       <p>Critical <br>illness Benefits</p>
         							 </li>
                              <?php
                                    }
                                    if ($jobMedicals['medical_id'] == '4') {
                              ?>		<li>
         							<div class="icodtds">
                                       <img title="Life Insurance" src="<?php echo base_url(); ?>webfiles/img/toppics/life-insurence.png">
         							</div>
                                       <p>Life <br>Insurance</p>
         							</li>
                              <?php
                                    }
                                    if ($jobMedicals['medical_id'] == '5') {
                              ?>		<li>
         							<div class="icodtds">
                                       <img title="Maternity Assistance" src="<?php echo base_url(); ?>webfiles/img/toppics/maternity-assistance.png">
         							</div>
                                       <p>Maternity <br>Assistance</p>
         							 </li>
                              <?php
                                    }
                                 }
                                 }
                              ?>
                              
                           </div>
                        </div>

                        <?php } if (!empty($jobShift) || !empty($jobLeaves)) {?>

                        <div class="jobtxtsdesc picsd">
                           <h4>Work Shift/Schedule</h4>
                           
                           <div class="provd usericonds">
                              <?php
                                 if (!empty($jobShift)) {
                                 foreach($jobShift as $jobShifts) {
                                    if ($jobShifts['workshift_id'] == '1') {
                              ?>     <li>
         							<div class="icodtds">
                                       <img title="Mid Shift" src="<?php echo base_url(); ?>webfiles/img/toppics/mid-shift.png">
         							</div>
                                       <p>Mid <br>Shift</p>
         							</li>
                              <?php
                                    }
                                    if ($jobShifts['workshift_id'] == '2') {
                              ?>		<li>
         							<div class="icodtds">
                                       <img title="Night Shift" src="<?php echo base_url(); ?>webfiles/img/toppics/night-shift.png">
         							</div>
                                       <p>Night <br>Shift</p>
         							</li>
                              <?php
                                    }
                                    if ($jobShifts['workshift_id'] == '3') {
                              ?>			<li>
         							<div class="icodtds">
                                       <img title="24/7" src="<?php echo base_url(); ?>webfiles/img/toppics/24.png">
         							 </div>
                                       <p>24/7</p>
         							 </li>
         							  
                              <?php
                                    }
                                 }
                                 }
                              ?>
                              <?php
                                 if (!empty($jobLeaves)) {
                                 foreach($jobLeaves as $jobLeave) {
                                    if ($jobLeave['leaves_id'] == '41') {
                              ?>		<li>
         							<div class="icodtds">
                                       <img title="Weekends Off" src="<?php echo base_url(); ?>webfiles/img/toppics/weekend-off.png">
         							</div>
                                       <p>Weekends <br>Off</p>
         							 </li>
                              <?php
                                    }
                                    if ($jobLeave['leaves_id'] == '42') {
                              ?>		<li>
         							<div class="icodtds">
                                       <img title="Holidays Off" src="<?php echo base_url(); ?>webfiles/img/toppics/holiday-off.png">
         							</div>
                                       <p>Holidays <br>Off</p>
         							</li>
                              <?php
                                    }
                                 }
                                 }
                              ?>
                              
                           </div>
                        </div>
                        <?php }?>
                       
                        <?php }?>
                     </div>
                  </div>
               </div>
            </div>


         </div>
      </div>
   </div>
</div>
<?php include_once ('footer.php'); ?>



<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!--   <script src="<?php echo base_url() . 'webfiles/'; ?>js/bootstrap-timepicker.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script>
<script>
   // function login() { 
   //      var email = $("#loginemail").val();
   //       var pass = $("#loginpass").val();
   //       var checkval=  $('#check1:checked').val();
   //       $.ajax({
   //           type: "POST",
   //           url: "<?php echo base_url(); ?>" + "user/login",
   //           data: {email:email,password:pass,rememberval:checkval},
   //           cache:false,
   //           success:function(data){
   //              // alert(data);
   //               console.log(data);
   //               var logdone = "logindone";
   //               var n = data.includes(logdone);
   //               if(n) {
   //                 window.location = "<?php echo base_url(); ?>user/search1";
   //               } else {
   //                //alert(data);
   //                 $('.modpassfull .filldetails p.text-center1').text(data);
   //               }
   //           },
   //           error:function(){
   //             console.log('error');
   //           }
   //       });
   // }  
</script>
<script type="text/javascript">
   $(function () {
      $(".datetimepicker11").datepicker({ 
       dateFormat: "yy-mm-dd", 
        defaultDate: '',
        autoclose: true,
     })
   });
   $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
    
</script>
<?php
   if (isset($_GET["msg"]) && $_GET["msg"] == "uploadcomplete") {
   ?>
<script type="text/javascript">
   $(window).on('load',function(){
       $('#exampleModalCenter9').modal('show');
   });
</script>
<?php
   }
   ?>
<?php
   if (isset($_GET["msg"]) && $_GET["msg"] == "scheduled") {
   ?>
<script type="text/javascript">
   $(window).on('load',function(){
       $('#exampleModalCenter10').modal('show');
   });
</script>
<?php
   }
   ?>
</body>
<script type="text/javascript">
   $("#okup").on("click",function(){
         window.location = "<?php echo base_url(); ?>dashboard/JobListings";
   });
</script>
<script>
   function getdayname()
     {
       //  alert(id);return false;
       var jobId= $("#jobId").val();
       var dayfrom = $("#dayfromm").val();
       var dayto   = $("#daytoo").val();
       var getdate= $("#datetimepicker1").val();
      
       if(dayfrom ==1)
       {
         var day ='Monday';
       }
       if(dayfrom ==2)
       {
         var day ='Tuesday';
       }
       if(dayfrom ==3)
       {
         var day ='Wednesday';
       }
       if(dayfrom ==4)
       {
         var day ='Thursday';
       }
       if(dayfrom ==5)
       {
         var day ='Friday';
       }
       if(dayfrom ==6)
       {
         var day ='Saturday';
       }
       if(dayfrom ==7)
       {
         var day ='Sunday';
       }
       
      if(dayto ==1)
       {
         var dayt ='Monday';
       }
       if(dayto ==2)
       {
         var dayt ='Tuesday';
       }
       if(dayto ==3)
       {
         var dayt ='Wednesday';
       }
       if(dayto ==4)
       {
         var dayt ='Thursday';
       }
       if(dayto ==5)
       {
         var dayt ='Friday';
       }
       if(dayto ==6)
       {
         var dayt ='Saturday';
       }
       if(dayto ==7)
       {
         var dayt ='Sunday';
       }
       
            $.ajax({
              'type' :'POST',
              'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
              'data' :'dateValue='+getdate+'&jobId='+jobId,
              'success':function(htmlres)
              {
                
                 if(htmlres == 1)
                 {
                     $('#schedule').attr('disabled',false);
                     $("#setval").html("");
                     $("#datetimepicker1").val(getdate);
                 }
                 else
                 {
                    // alert('Please Select Schedule date');
                   $("#setval").html("Please Select Schedule date between ("+day+" to "+dayt+")");
                   $("#datetimepicker1").val(''); 
                   $('#schedule').attr('disabled',true);
                 }
              }
                
            });
        }
    
</script>
<script>
   function getTime()
   {
      var jobId= $("#jobId").val();
      var timefrom= $("#timefromm").val();
      var timetoo= $("#timetoo").val();
      var gettime= $("#datepicker1").val();
       $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
         'data' :'timeValue='+gettime+'&jobId='+jobId,
         'success':function(htmlres)
         {
           
            if(htmlres == 1)
            {
              
              $('#schedule').attr('disabled',false);
              $("#setvall").html("");
              $("#datepicker1").val(gettime);
            }
            else
            {
                $("#setvall").html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                $("#datepicker1").val(''); 
                $('#schedule').attr('disabled',true);
   
            }
         }
           
       });
   }
</script>
<script>
   function getFilename()
    {
      var name = document.getElementById("file").files[0].name;
      var filetype= $("#file").val();
      var ext = filetype.split('.').pop();
      if(ext =="jpg" || ext =="jpeg" || ext =="png"){
        $("#setimgres").html("<span style='color:red'>plese select file format(pdf/doc/docx)</span>");
        return false;
     } 
     else
     {
      $("#setimgres").html("");
      var form_data = new FormData();
      form_data.append("file", document.getElementById('file').files[0]);
      $.ajax({
        url:'<?php echo base_url('dashboard/imageUpload') ?>',
        method:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success:function(imagedata)
        { 
           $("#imagedata").val(imagedata);
          $("#file").val('');
          $("#setimgres").html(imagedata);
        }
      });
     }
   }
</script>
<script type="text/javascript">
   function PdfImagesend()
   {
     var image= $("#imagedata").val();
    // alert(image);return false;
       $.ajax({
              'type' :'POST',
              'url' :"<?php echo base_url('dashboard/fileInsert') ?>",
              'data' :'imagedata='+image,
              'success':function(htmlres)
              {
   
                if(htmlres=='1')
                {
                 $("#exampleModalCenter9").modal('show');
                }
                if(htmlres=='2')
                {
                  $("#exampleModalCenter9").modal('show');
                }
              }
        });
    }
</script>
</html>

