<?php
   include_once('header.php');
   $userSess = $this->session->userdata('usersess');
?>

<style type="text/css">
  .clear{ clear: both; }
  .AdminJobViewArea .innerbglay{
      padding: 0;
      width: 100%
  }
  .AdminJobViewArea .innerbglay .adminopnts{    margin: 0 0 30px;}
  .AdminJobViewArea .innerbglay .adminopnts .featurerowjob a{ width: 100% }
</style>


<section>
  <div class="managerpart AdminJobViewArea">
     <div class="container-fluid">
        <div class="row">
           <div class="col-md-12 expanddiv">
              <div class="innerbglay noshad">
                 <div class="" style="padding-top:0px;">
                    <div class="adminopnts">
                       <div class="tab-content" id="nav-tabContent">
                          <div class="tab-pane fade show active" id="nav-home">
                             <div class="tabledivsdsn Min-height100">
                                <?php 
                                   if(!empty($hotjobss))
                                   {
                                       foreach($hotjobss as $hotjobssData)
                                       {
                                   
                                   ?>
                                <div class="featurerowjob">
                                   <div class="row">
                                      <?php if(!empty($hotjobssData['jobpost_id'])) {?>
                                      <div class="col-md-3 col-lg-4 tumb">
                                         <!-- <h6>Applied Jobs</h6>-->
                                         <div class="imagethumbs">
                                            <?php 
                                               if(!empty($hotjobssData['job_image']))
                                               {
                                               ?>
                                            <div class="thumbimpr"><a href="<?php  if (!empty($userSess)) { echo base_url();?>dashboard/jobDescription?listing=<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>user/jobDescription?listing=<?php echo base64_encode($hotjobssData['jobpost_id']); }?>"><img src="<?php echo $hotjobssData['job_image'];?>" style=""></a></div>
                                            <?php }
                                               else
                                               {
                                               ?>
                                            <div class="thumbimpr"><a href="<?php  if (!empty($userSess)) { echo base_url();?>dashboard/jobDescription?listing=<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>user/jobDescription?listing=<?php echo base64_encode($hotjobssData['jobpost_id']); }?>"><img src="<?php echo base_url().'webfiles/';?>img/user_man.png"></a></div>
                                            <?php 
                                               }
                                               ?>
                                         </div>
                                      </div>
                                      <?php }?> 
                                      <div class="col-md-6 col-lg-5">
                                         <div class="positionarea">
                                            <!--<h6>Position</h6>-->
                                            <a href="<?php  if (!empty($userSess)) { echo base_url();?>dashboard/jobDescription?listing=<?php echo base64_encode($hotjobssData['jobpost_id']); } else{ echo base_url();?>user/jobDescription?listing=<?php echo base64_encode($hotjobssData['jobpost_id']); }?>">
                                               <!--  <a href="jobdescription.html">-->
                                               <p class="posttypes"><?php echo $hotjobssData['job_title'];?></p>
                                            </a>
                                            <a href="<?php  if (!empty($userSess)) { echo base_url('dashboard/companyProfile/'.base64_encode($hotjobssData['comapnyId'])); } else{ echo base_url('user/companyProfile/'.base64_encode($hotjobssData['comapnyId'])); }?> ">
                                               <h3><?php echo $hotjobssData['companyName']; ?></h3>
                                            </a>
                                            <p class="posttypes dire"><?php echo $hotjobssData['cname'];?></p>
                                            <?php if(!empty($hotjobssData['companyAddress'])) {?>
                                            <p class="locationtype"><img src="<?php echo base_url().'webfiles/';?>img/locmap.png"><?php echo $hotjobssData['companyAddress']; ?></p>
                                            <?php }?>
                                            <p style="font-size:12px; color: #21252; margin:0 0 6px 0; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"><?php echo substr($hotjobssData['jobPitch'],0, 100); ?></p>
                                            <div class="salicoftr">
                                               <?php
                                                  if(!empty($hotjobssData['toppicks1']))
                                                    {
                                                     if($hotjobssData['toppicks1']=='1')
                                                     {
                                                     ?>
                                               <div class="salry">
                                                  <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                                  <span>Joining Bonus</span>
                                               </div>
                                               <?php 
                                                  }
                                                  
                                                  if($hotjobssData['toppicks1']=='2')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                                  <span>Free Food</span>
                                               </div>
                                               <?php }
                                                  if($hotjobssData['toppicks1']=='3')
                                                   {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                                  <span>Day 1 HMO</span>
                                               </div>
                                               <?php }
                                                  if($hotjobssData['toppicks1']=='4')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                                  <span>Day 1 HMO for Dependent</span>
                                               </div>
                                               <?php }
                                                  if($hotjobssData['toppicks1']=='5')
                                                  {
                                                    ?>
                                               <div class="salry">
                                                  <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                                  <span>Day Shift</span>
                                               </div>
                                               <?php
                                                  }
                                                  if($hotjobssData['toppicks1']=='6')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                                  <span>14th Month Pay</span>
                                               </div>
                                               <?php
                                                  }}
                                                  
                                                  
                                                  if(!empty($hotjobssData['toppicks2']))
                                                  {
                                                  if($hotjobssData['toppicks2']=='1')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                                  <span>Joining Bonus</span>
                                               </div>
                                               <?php 
                                                  }
                                                  
                                                  if($hotjobssData['toppicks2']=='2')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                                  <span>Free Food</span>
                                               </div>
                                               <?php }
                                                  if($hotjobssData['toppicks2']=='3')
                                                   {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                                  <span>Day 1 HMO</span>
                                               </div>
                                               <?php }
                                                  if($hotjobssData['toppicks2']=='4')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                                  <span>Day 1 HMO for Dependent</span>
                                               </div>
                                               <?php }
                                                  if($hotjobssData['toppicks2']=='5')
                                                  {
                                                    ?>
                                               <div class="salry">
                                                  <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                                  <span>Day Shift</span>
                                               </div>
                                               <?php
                                                  }
                                                  if($hotjobssData['toppicks2']=='6')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                                  <span>14th Month Pay</span>
                                               </div>
                                               <?php
                                                  }}
                                                  
                                                  
                                                  if(!empty($hotjobssData['toppicks3']))
                                                  {
                                                  if($hotjobssData['toppicks3']=='1')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                                  <span>Joining Bonus</span>
                                               </div>
                                               <?php 
                                                  }
                                                  
                                                  if($hotjobssData['toppicks3']=='2')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                                  <span>Free Food</span>
                                               </div>
                                               <?php }
                                                  if($hotjobssData['toppicks3']=='3')
                                                   {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                                  <span>Day 1 HMO</span>
                                               </div>
                                               <?php }
                                                  if($hotjobssData['toppicks3']=='4')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                                  <span>Day 1 HMO for Dependent</span>
                                               </div>
                                               <?php }
                                                  if($hotjobssData['toppicks3']=='5')
                                                  {
                                                    ?>
                                               <div class="salry">
                                                  <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                                  <span>Day Shift</span>
                                               </div>
                                               <?php
                                                  }
                                                  if($hotjobssData['toppicks3']=='6')
                                                  {
                                                  ?>
                                               <div class="salry">
                                                  <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                                  <span>14th Month Pay</span>
                                               </div>
                                               <?php
                                                  }}
                                                  ?>
                                            </div>
                                            <p class="posttypes drdmn"><i class="fas fa-map-marked-alt"></i><?php echo $hotjobssData['distance'];?>KM</p>
                                            <p class="" style="font-size:12px; display:none;"><?php echo $hotjobssData['jobPitch'];?></p>
                                         </div>
                                      </div>
                                      <div class="col-md-9 col-lg-3 lefthalf appset">
                                         <div class="salryedits">
                                            <!-- <h6>Salary</h6>-->
                                            <p>
                                               <span id="" title="" data-target="#exampleModalCenter4" data-toggle="modal">
                                               <i class="fa fa-heart" title="" style="color:#b5b5b5;font-size: 24px;cursor:pointer;"></i>
                                               </span>
                                            </p>
                                            <?php  
                                               $mynumber=substr($hotjobssData['salary'], 0, 2);
                                               if($hotjobssData['salary']>0){
                                                ?>
                                            <h4><?php echo $hotjobssData['salary']; ?>/Month</h4>
                                            <?php 
                                               }else{?>
                                            <h4>Salary confidential</h4>
                                            <?php }?>                     
                                         </div>
                                         <button class="greenbtn" data-toggle="modal" data-target="#exampleModalCenter4">Apply <?php if($hotjobssData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'for '. $hotjobssData['mode']; } ?> </button>
                                      </div>
                                   </div>
                                </div>
                                <?php }}?>
                             </div>
                          </div>
                          <div class="tab-pane fade show active" id="nav-profile" style="display:none">
                             <div class="tabledivsdsn">
                             </div>
                             <div class="clear"></div>
                          </div>
                          <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                 </div>
                 <div class="clear"></div>
              </div>
           </div>
        </div>
     </div>
  </div>
</section>
<script type="text/javascript">
   $(".hammenu").click(function (e) {
       e.stopPropagation();
       $(".sideshift").toggleClass('opensidemenu');
   });
   
   $(".hammenu").click(function (e) {
       e.stopPropagation();
       $(".hdebarsd").toggleClass('hidebarsd');
   });
   
   $(".crossmob").click(function (e) {
       e.stopPropagation();
       $(".sideshift").removeClass('opensidemenu');
   });
   
   
   $('.sideshift').on('click', function () {        
    $(this).toggleClass('opensidemenu');});
   
   $(document).click(function (e) {    
    if (!$(e.target).hasClass('sideshift')) { 
     $('.sideshift.opensidemenu').removeClass('opensidemenu')    
    }})
    
</script>
<script type="text/javascript">
   if ( window.history.replaceState ) {
     window.history.replaceState( null, null, window.location.href );
   }
</script>


<?php include_once('footer.php'); ?>

