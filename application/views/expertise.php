<?php
    include_once('header2.php');
// if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

//     } else {
//         $link = "https";
//         $link .= "://";
//         $link .= $_SERVER['HTTP_HOST'];
//         $link .= $_SERVER['REQUEST_URI'];
//         redirect($link);
//     }
    $userSess = $this->session->userdata('usersess'); 
    if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
        $listingTypeFun = "jobs";
    } else {
        $listingTypeFun = "jobs";
    }
?>

    <section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1>Expertise  </h1>
            <h2>Apply for the best BPO jobs that match your skills</h2>
        </div>
    </section>

    <section>
        <div class="ExpertiseArea" style="padding: 50px 0 30px">
            <div class="container"> 
                <div class="row">
                <?php
                    if(!empty($expertises)) {
                        foreach($expertises as $expertise) {
                ?>
                            <div class="col-sm-6 col-md-3">
                                <div class="ExpertiseBox">
                                    <?php
                                        if($expertise['catname'] == "Sales") {
                                            $image = "images/Expertise-1.png";
                                        } elseif($expertise['catname'] == "Customer Care") {
                                            $image = "images/Expertise-2.png";
                                        } elseif($expertise['catname'] == "Technical Support") {
                                            $image = "images/Expertise-3.png";
                                        } elseif($expertise['catname'] == "HealthCare") {
                                            $image = "images/Expertise-4.png";
                                        } elseif($expertise['catname'] == "Shared Services Support") {
                                            $image = "images/Expertise-5.png";
                                        } elseif($expertise['catname'] == "Specialized Jobs") {
                                            $image = "images/Expertise-6.png";
                                        } elseif($expertise['catname'] == "Information Technology") {
                                            $image = "images/Expertise-7.png";
                                        } elseif($expertise['catname'] == "Banking") {
                                            $image = "images/Expertise-8.png";
                                        }
                                    ?>
                                    <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/expertise/<?php echo base64_encode($expertise['id']);?>">
                                        <figure> <img src="<?php echo base_url().'webfiles/newone/'.$image; ?>"> </figure>
                                        <h3><?php echo $expertise['catname']; ?></h3>
                                        <p><span><?php echo $expertise['jobcount']; ?></span> Open Positions</p>
                                        <span class="Icon"><img src="<?php echo base_url().'webfiles/newone/'.$image; ?>"></span>
                                    </a>
                                </div>
                            </div>
                <?php
                        }
                    }
                ?>
                </div>  
            </div>
        </div>
    </section>

 
 <?php
    include_once('footer1.php');
?>