<?php include_once('header.php'); ?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<style type="text/css">
.rating-loading{
    width:25px;
    height:25px;
    font-size:0;
    color:#fff;
    background:url(../img/loading.gif) top left no-repeat;border:none
    }
.rating-container .rating-stars
    {position:relative;
    cursor:pointer;
    vertical-align:middle;
    display:inline-block;
    overflow:hidden;
    white-space:nowrap
         }
         .rating-container .rating-input
         {position:absolute;
         cursor:pointer;
         width:100%;
         height:1px;
         bottom:0;left:0;
         font-size:1px;
         border:none;
         background:0 0;
         padding:0;
         margin:0
         }
         .rating-disabled .rating-input,.rating-disabled .rating-stars
         {
             cursor:not-allowed
             
         }
         .rating-container .star{
             display:inline-block;
             margin:0 3px;
             text-align:center
         }
         .rating-container .empty-stars{
             color:#aaa
         }
         .rating-container .filled-stars
         {
             position:absolute;
             left:0;
             top:0;
             margin:auto;
             color:#fde16d;
             white-space:nowrap;
             overflow:hidden;
             -webkit-text-stroke:1px #777;
             text-shadow:1px 1px #999
         }
             .rating-rtl
             {
                 float:right
                 
             }
             .rating-animate .filled-stars
             {
                 transition:width .25s ease;
                 -o-transition:width .25s ease;
                 -moz-transition:width .25s ease;-webkit-transition:width .25s ease
             }
             .rating-rtl .filled-stars
             {left:auto;right:0;-moz-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);-webkit-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);-o-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);
             transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0)}.rating-rtl.is-star 
             .filled-stars{
                 right:.06em
                 
             }
             .rating-rtl.is-heart .empty-stars{
                 margin-right:.07em
                 
             }
             .rating-lg{
                 font-size:3.91em
                 
             }
             .rating-md{
                 font-size:3.13em
                 
             }
             .rating-sm{
                 font-size:2.5em
                 
             }
             .rating-xs{
                 font-size:2em
                 
             }
             .rating-xl{
             font-size:4.89em
                 
             }
             .rating-container .clear-rating{
                 color:#aaa;
                 cursor:not-allowed;
                 display:inline-block;
                 vertical-align:middle;
                 font-size:60%;
                 padding-right:5px
                 
             }
             .clear-rating-active{
                 cursor:pointer!important
                 
             }
             .clear-rating-active:hover{
                 color:#843534
                 
             }
             .rating-container .caption{
                 color:#999;
                 display:inline-block;
                 vertical-align:middle;
                 font-size:60%;
                 margin-top:-.6em;
                 margin-left:5px;
                 margin-right:0
                 
             }
             .rating-rtl .caption{
                 margin-right:5px;
                 margin-left:0
                 
             }
             @media print{.rating-container .clear-rating{
                 display:none
                 
             }
                 
             }
			 
			  .ratingpoput .rating-container {
    margin: 0 auto;
    width:51%;
    text-align: center;
}

.ratingpoput .clear-rating {
    float: left;
}

.ratingpoput .rating-stars {
    float: left;
}

.ratingpoput .caption {
    float: left;
}

.rating-stars span{
    float:none;
    width:auto;
}

.belowrtf {
    float: left;
    width: 100%;
    margin-top: 20px;
}

.ratingpoput .caption {
    margin-top: 2px;
}

</style>
<div class="managerpart">
   <div class="container-fluid">
      <div class="row">
        
         <div class="col-md-9 expanddiv">
		 <div class="innerbglay">
            <div class="mainheadings-yoda">
              <?php if($this->session->flashdata('message')): ?>
    <p><div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong><?php echo $this->session->flashdata('message'); ?>
  </div></p>
<?php endif; ?>
              
            </div>
            <div class="adminopnts">
              <nav class="jobyodatabs">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                   <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home">Activity</a>
                   <a class="nav-item nav-link"  id="nav-profile-tab" data-toggle="tab" href="#nav-profile">Promo</a>
                </div>
             </nav>
             <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home">
                           <div class="tabledivsdsn">
               <div class="jobtxtsdesc">
               <?php 
               if(!empty($notification_listss)){
                ?>
                <img class="bx_img" src="<?php echo base_url();?>webfiles/img/emptybx.png">
                <center>No Data Found in Notifications</center>
                <?php
               }

                  else {
                     $x = 1;
                     foreach ($notification_listss as $notification_lists) {
               ?>  
                  <div class="notifypanel">
                     <!-- <a href="<?php echo base_url();?>dashboard/jobDescription?type=applied&listing=<?php echo base64_encode($notification_lists['jobpost_id']); ?>"  class="ctnpanels" > -->
                     <a href="#"  class="ctnpanels" >
                     <?php 
                        if(!empty($notification_lists['notification'])) {
                         
                     ?>
                            <div class="nothumbd">
                           <img src="<?php echo base_url('images')?>/tie.png">
                            </div>
                               
                           <?php 
                              if($notification_lists['status'] == '1') {  
                           ?>    <?php if($notification_lists['job_status'] == 1 && $notification_lists['storystatus']==0) { ?>
                                    <span id="changecolor<?php echo $notification_lists['id'];?>" <?php if($notification_lists['job_status'] == 1) { ?> data-toggle="modal" data-target="#storyModal<?php echo $x;?>" <?php } ?> style="color:black;" title="Read"><?php echo $notification_lists['notification']; ?></span>
                              
                              <?php } else if($notification_lists['job_status'] == 2 && $notification_lists['reviewstatus']==0) {?>
                                       <span id="changecolor<?php echo $notification_lists['id'];?>" <?php if($notification_lists['job_status'] == 2) { ?> data-toggle="modal" data-target="#reviewModal<?php echo $x;?>" <?php } ?> style="color:black;" title="Read"><?php echo $notification_lists['notification']; ?></span>
                              <?php } else {?>
                                      <span id="changecolor<?php echo $notification_lists['id'];?>" style="color:black;" title="Read"><?php echo $notification_lists['notification']; ?></span>
                              <?php } ?> 
                                <div class="modal fade" id="storyModal<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                 <div class="modal-dialog modal-dialog-centegreen addeducation" role="document">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                          </button>
                                       </div>
                                       <div class="modal-body">
                                          <div class="workexperience addresumes editssections">
                                             <p style="text-align:center">Share your success story</p>
                                             <p style="text-align:center"></p>
                                             <div class="formmidaress modpassfull">
                                                <div class="filldetails">
                                                   <form method="post" action="<?php echo base_url();?>user/addStory">
                                                      <div class="forminputspswd">
                                                      <textarea name="story" class="form-control" data-validation="requigreen" data-validation-error-msg-requigreen="Please add your story" placeholder="Write here.."></textarea>
                                                      </div>
                                                      <div class="forminputspswd">
                                                         <input type="hidden" name="recruiter_id" value="<?php echo $notification_lists['recruiter_id']?>">
                                                         <button type="submit" class="srchbtns">Share</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div> 

                              <div class="modal fade" id="reviewModal<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-dialog-centegreen addeducation" role="document">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                             </button>
                                          </div>
                                          <div class="modal-body ratingpoput">
                                              <form method="post" action="<?php echo base_url();?>user/addReview">
                                             <div class="workexperience addresumes editssections">
                                                <p style="text-align:center"> How was your overall experience for <strong><?php echo $notification_lists['companyname'];?></strong></p>
                        
                        <div class="ratetextsd">
                                                <p style="text-align:center">Rating 1-Low Rating 5-High</p>
                        </div>
 
                                               <input id="input-1" name="rating" class="rating rating-loading" data-min="0" data-max="5" data-step="0.5" value="2" data-validation="requigreen">
                                                 <?php if(isset($reviewerrors['rating'])){echo "<p class='text-center text-danger'>".$reviewerrors['rating']."</p>"; } ?>
                                                <div class="belowrtf">  <p style="text-align:center">Would you recommend the <strong><?php echo $notification_lists['companyname'];?></strong> to your friends.</p>
                        </div>
                        
                                                   <div class="forminputspswd">
                                                   <p style="text-align:center;">
                                                   <input type="radio" name="recommend" value="1" data-validation="requigreen">Yes
                                                    <input type="radio" name="recommend" value="0" data-validation="requigreen">No
                                                    </p>
                                                    <?php if(isset($reviewerrors['recommend'])){echo "<p class='text-center text-danger'>".$reviewerrors['recommend']."</p>"; } ?>
                                                      </div>
                                                      <div class="forminputspswd">
                                                      <textarea name="feedback" class="form-control" data-validation="requigreen" placeholder="Write here.." data-validation-error-msg-requigreen="Please add your feedback"></textarea>
                                                      <?php if(isset($reviewerrors['feedback'])){echo "<p class='text-center text-danger'>".$reviewerrors['feedback']."</p>"; } ?>
                                                      </div>
                                                <div class="formmidaress modpassfull">
                                                   <div class="filldetails">
                                                  
                                                         <div class="forminputspswd">
                                                         </div>
                                                         <div class="forminputspswd">
                                                    <input type="hidden" name="recruiter_id" value="<?php echo $notification_lists['recruiter_id'];?>">
                                                            <button type="submit" class="srchbtns">Share</button>
                                                         </div>
                                                      
                                                   </div>
                                                </div>
                                             </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div> 

                           <?php }?>

                           <?php if($notification_lists['status'] == '2') { 

                           ?>
                              <?php if($notification_lists['job_status'] == 1) { ?>
                                    <span id="changecolor<?php echo $notification_lists['id'];?>" <?php if($notification_lists['job_status'] == 1) { ?> data-toggle="modal" data-target="#storyModal<?php echo $x;?>" <?php } ?> style="color:green;" title="Unread"><?php echo $notification_lists['notification']; ?></span>
                              
                              <?php } else if($notification_lists['job_status'] == 2) {?>
                                       <span id="changecolor<?php echo $notification_lists['id'];?>" <?php if($notification_lists['job_status'] == 2) { ?> data-toggle="modal" data-target="#reviewModal<?php echo $x;?>" <?php } ?> style="color:green;" title="Unread"><?php echo $notification_lists['notification']; ?></span>
                              <?php } else {?>
                                       <span id="changecolor<?php echo $notification_lists['id'];?>" style="color:green;" title="Unread"><?php echo $notification_lists['notification']; ?></span>
                              <?php } ?>
                              <?php if($notification_lists['job_status'] == 1) { ?>
                              <div class="modal fade" id="storyModal<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                 <div class="modal-dialog modal-dialog-centegreen addeducation" role="document">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                          </button>
                                       </div>
                                       <div class="modal-body">
                                          <div class="workexperience addresumes editssections">
                                             <p style="text-align:center">Share your success story</p>
                                             <p style="text-align:center"></p>
                                             <div class="formmidaress modpassfull">
                                                <div class="filldetails">
                                                   <form method="post" action="<?php echo base_url();?>user/addStory">
                                                      <div class="forminputspswd">
                                                      <textarea name="story" class="form-control" data-validation="requigreen" data-validation-error-msg-requigreen="Please add your story" placeholder="Write here.."></textarea>
                                                      </div>
                                                      <div class="forminputspswd">
                                                         <input type="hidden" name="recruiter_id" value="<?php echo $notification_lists['recruiter_id']?>">
                                                         <button type="submit" class="srchbtns">Share</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <?php } ?>
                              <?php if($notification_lists['job_status'] == 2) { ?>
                                 <div class="modal fade" id="reviewModal<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-dialog-centegreen addeducation" role="document">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                             </button>
                                          </div>
                                          <div class="modal-body ratingpoput">
                                              <form method="post" action="<?php echo base_url();?>user/addReview">
                                             <div class="workexperience addresumes editssections">
                                                <p style="text-align:center"> How was your overall experience for <strong><?php echo $notification_lists['companyname'];?></strong></p>
												
												<div class="ratetextsd">
                                                <p style="text-align:center">Rating 1-Low Rating 5-High</p>
												</div>
 
                                               <input id="input-1" name="rating" class="rating rating-loading" data-min="0" data-max="5" data-step="0.5" value="2" data-validation="requigreen">
                                                 <?php if(isset($reviewerrors['rating'])){echo "<p class='text-center text-danger'>".$reviewerrors['rating']."</p>"; } ?>
                                                <div class="belowrtf">  <p style="text-align:center">Would you recommend the <strong><?php echo $notification_lists['companyname'];?></strong> to your friends.</p>
												</div>
												
                                                   <div class="forminputspswd">
                                                   <p style="text-align:center;">
                                                   <input type="radio" name="recommend" value="1" data-validation="requigreen">Yes
                                                    <input type="radio" name="recommend" value="0" data-validation="requigreen">No
                                                    </p>
                                                    <?php if(isset($reviewerrors['recommend'])){echo "<p class='text-center text-danger'>".$reviewerrors['recommend']."</p>"; } ?>
                                                      </div>
                                                      <div class="forminputspswd">
                                                      <textarea name="feedback" class="form-control" data-validation="requigreen" placeholder="Write here.." data-validation-error-msg-requigreen="Please add your feedback"></textarea>
                                                      <?php if(isset($reviewerrors['feedback'])){echo "<p class='text-center text-danger'>".$reviewerrors['feedback']."</p>"; } ?>
                                                      </div>
                                                <div class="formmidaress modpassfull">
                                                   <div class="filldetails">
                                                  
                                                         <div class="forminputspswd">
                                                         </div>
                                                         <div class="forminputspswd">
                                                    <input type="hidden" name="recruiter_id" value="<?php echo $notification_lists['recruiter_id'];?>">
                                                            <button type="submit" class="srchbtns">Share</button>
                                                         </div>
                                                      
                                                   </div>
                                                </div>
                                             </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              <?php } ?>
                           <?php } ?>
                        <?php } ?>
                     </a>
					 <p style="padding-top:10px;padding-left:10px;"><?php echo $notification_lists['date'];?></p> 
					 <div class="managebtsd">
					 <button type="button" class="deletenotify" id="<?php echo $notification_lists['id'];?>" onclick="getrid(this.id)" data-toggle="modal" data-target="#deleteModal<?php echo $x; ?>">Delete</button>
            <?php 
              if($notification_lists['status'] == '2') {  
           ?>
					 <a href="#" onclick="readNotification('<?php echo $notification_lists['status'];?>','<?php echo $notification_lists['id'];?>','<?php echo $notification_lists['job_status'];?>','<?php echo base64_encode($notification_lists['jobpost_id']);?>','<?php echo $x;?>')" class="deletenotify">Mark it read</a>
           <?php }else{}?>
					 </div>
                      
                  </div>  
            <div class="modal" id="deleteModal<?php echo $x;?>" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Delete Notification</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure to delete this notification?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a href="" class="btn btn-primary" id="createLink">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
               <?php
                     $x++;
                  }
               }
               ?>
               </div>
               </div>
               </div>
               <div class="tab-pane fade show active" id="nav-profile" style="display:none">
                 <div class="tabledivsdsn">
                   <?php   
                    if(!empty($promonotification_listss[0]['message'])){
                      $x = 1;
                      foreach ($promonotification_listss as $promonotification_lists) { ?>
                        <div class="notifypanel">
                          <div class="nothumbd">
                            <img src="<?php echo base_url()?>/webfiles/img/active_notification.png" style="width:22px;">
                          </div>
                          <span id="changecolor" style="color:black;"><?php echo urldecode($promonotification_lists['message']); ?></span>
                        </div>
                    <?php }                   
                                         
                    }else{?>
                      <img class="bx_img" src="<?php echo base_url();?>webfiles/img/emptybx.png">
                      <center>No Data Found in Notifications</center>
                    <?php }?>
                 </div>
               </div>
               </div>
            </div>
			</div>
         </div>
      </div>
   </div>
</div>
<?php include_once('footer.php'); ?>

<div class="modal fade" id="exampleModalCenterjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centegreen" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">
               <span id="savedjobmessage"></span>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<script src="<?php echo base_url().'webfiles/';?>js/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>
</head>
<script type="text/javascript">
         $(document).ready(function(){
            $("#nav-home-tab").on("click", function(){
                $("#nav-home-tab").addClass("active");
                $("#nav-profile-tab").removeClass("active");
                $("#nav-home").css("display","block");
                $("#nav-profile").css("display","none");
            });

            $("#nav-profile-tab").on("click", function(){
                  $("#nav-home-tab").removeClass("active");
                  $("#nav-profile-tab").addClass("active");
                  $("#nav-profile").css("display","block");
                  $("#nav-home").css("display","none");
            
            });
         });
      </script>
<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>dashboard/deleteNotification?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }
</script>    
<script>
   $(document).ready(function(){   
   $("#changepassbtn").click(function() {
     //var oldpass = $("#oldpass").val();
     var newpass = $("#newpass").val();
     var confpass = $("#confpass").val();
     $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>" + "user/changepass",
         data: {newpass:newpass,confpass:confpass},
         cache:false,
         success:function(htmldata){
         
              $('#errorfield').html(htmldata);
             
         },
         error:function(){
           console.log('error');
         }
     });
   });
   });
</script>
<script>
   function savedjob(id)
      {
        
         $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/savedjovData') ?>",
            'data' :'jobId='+id,
            'success':function(htmlres)
            {
               //alert(htmlres);return false;  
               if(htmlres !='')
               {
               if(htmlres == 1)
               {
               $('#test'+id).css('display', 'block'); 
               $('#test1'+id).css('display', 'none'); 
               $('#test3'+id).css('display', 'none');   
               $("#savedjobmessage").html('jobs are saved successfully');
                $("#exampleModalCenterjob").modal('show');
                 
               }
               if(htmlres == 2)
               {
                  $('#test1'+id).css('display', 'block'); 
                  $('#test'+id).css('display', 'none'); 
                  $('#test3'+id).css('display', 'none'); 
                 $("#savedjobmessage").html('jobs are unsaved successfully');
                 $("#exampleModalCenterjob").modal('show');
               
               }
                if(htmlres == 3)
               {
                  $('#test'+id).css('display', 'none'); 
                  $('#test1'+id).css('display', 'none'); 
                  $("#savedjobmessage").html('jobs are saved successfully');
                  $("#exampleModalCenterjob").modal('show');
               }
            }
            }
              
          });
      }
</script>
<script>
   function readNotification(Status,id,job_statsus,jobid,popid)
   {
    
      $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('user/changeUnReadstatus') ?>",
         'data' :'Status='+Status+'&id='+id,
         'success':function(htmlres)
         {

             if(htmlres==1)
             {
               //  $("#reviewModal"+popid).show();
                // $("#storyModal"+popid).show();
                 $("#changecolor"+id).css({ 'color': 'black'});
                 $("#changecolor"+id).attr("title", "Read");
                 if(job_statsus=='0'){
                    window.setTimeout(function() {
                        window.location.href = '<?php echo base_url();?>dashboard/jobDescription?type=applied&listing='+jobid;
                    }, 1000);
                 }else{
                        window.location.href = '<?php echo base_url();?>dashboard/notifications';
                      }
             } 
         }
       });
   }
</script>
<script type="text/javascript">
   
! function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof module && module.exports ? module.exports = e(require("jquery")) : e(window.jQuery)
}(function(e) {
    "use strict";
    e.fn.ratingLocales = {}, e.fn.ratingThemes = {};
    var t, a;
    t = {
        NAMESPACE: ".rating",
        DEFAULT_MIN: 0,
        DEFAULT_MAX: 5,
        DEFAULT_STEP: .5,
        isEmpty: function(t, a) {
            return null === t || void 0 === t || 0 === t.length || a && "" === e.trim(t)
        },
        getCss: function(e, t) {
            return e ? " " + t : ""
        },
        addCss: function(e, t) {
            e.removeClass(t).addClass(t)
        },
        getDecimalPlaces: function(e) {
            var t = ("" + e).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            return t ? Math.max(0, (t[1] ? t[1].length : 0) - (t[2] ? +t[2] : 0)) : 0
        },
        applyPrecision: function(e, t) {
            return parseFloat(e.toFixed(t))
        },
        handler: function(e, a, n, r, i) {
            var l = i ? a : a.split(" ").join(t.NAMESPACE + " ") + t.NAMESPACE;
            r || e.off(l), e.on(l, n)
        }
    }, a = function(t, a) {
        var n = this;
        n.$element = e(t), n._init(a)
    }, a.prototype = {
        constructor: a,
        _parseAttr: function(e, a) {
            var n, r, i, l, s = this,
                o = s.$element,
                c = o.attr("type");
            if ("range" === c || "number" === c) {
                switch (r = a[e] || o.data(e) || o.attr(e), e) {
                    case "min":
                        i = t.DEFAULT_MIN;
                        break;
                    case "max":
                        i = t.DEFAULT_MAX;
                        break;
                    default:
                        i = t.DEFAULT_STEP
                }
                n = t.isEmpty(r) ? i : r, l = parseFloat(n)
            } else l = parseFloat(a[e]);
            return isNaN(l) ? i : l
        },
        _parseValue: function(e) {
            var t = this,
                a = parseFloat(e);
            return isNaN(a) && (a = t.clearValue), !t.zeroAsNull || 0 !== a && "0" !== a ? a : null
        },
        _setDefault: function(e, a) {
            var n = this;
            t.isEmpty(n[e]) && (n[e] = a)
        },
        _initSlider: function(e) {
            var a = this,
                n = a.$element.val();
            a.initialValue = t.isEmpty(n) ? 0 : n, a._setDefault("min", a._parseAttr("min", e)), a._setDefault("max", a._parseAttr("max", e)), a._setDefault("step", a._parseAttr("step", e)), (isNaN(a.min) || t.isEmpty(a.min)) && (a.min = t.DEFAULT_MIN), (isNaN(a.max) || t.isEmpty(a.max)) && (a.max = t.DEFAULT_MAX), (isNaN(a.step) || t.isEmpty(a.step) || 0 === a.step) && (a.step = t.DEFAULT_STEP), a.diff = a.max - a.min
        },
        _initHighlight: function(e) {
            var t, a = this,
                n = a._getCaption();
            e || (e = a.$element.val()), t = a.getWidthFromValue(e) + "%", a.$filledStars.width(t), a.cache = {
                caption: n,
                width: t,
                val: e
            }
        },
        _getContainerCss: function() {
            var e = this;
            return "rating-container" + t.getCss(e.theme, "theme-" + e.theme) + t.getCss(e.rtl, "rating-rtl") + t.getCss(e.size, "rating-" + e.size) + t.getCss(e.animate, "rating-animate") + t.getCss(e.disabled || e.readonly, "rating-disabled") + t.getCss(e.containerClass, e.containerClass)
        },
        _checkDisabled: function() {
            var e = this,
                t = e.$element,
                a = e.options;
            e.disabled = void 0 === a.disabled ? t.attr("disabled") || !1 : a.disabled, e.readonly = void 0 === a.readonly ? t.attr("readonly") || !1 : a.readonly, e.inactive = e.disabled || e.readonly, t.attr({
                disabled: e.disabled,
                readonly: e.readonly
            })
        },
        _addContent: function(e, t) {
            var a = this,
                n = a.$container,
                r = "clear" === e;
            return a.rtl ? r ? n.append(t) : n.prepend(t) : r ? n.prepend(t) : n.append(t)
        },
        _generateRating: function() {
            var a, n, r, i = this,
                l = i.$element;
            n = i.$container = e(document.createElement("div")).insertBefore(l), t.addCss(n, i._getContainerCss()), i.$rating = a = e(document.createElement("div")).attr("class", "rating-stars").appendTo(n).append(i._getStars("empty")).append(i._getStars("filled")), i.$emptyStars = a.find(".empty-stars"), i.$filledStars = a.find(".filled-stars"), i._renderCaption(), i._renderClear(), i._initHighlight(), n.append(l), i.rtl && (r = Math.max(i.$emptyStars.outerWidth(), i.$filledStars.outerWidth()), i.$emptyStars.width(r)), l.appendTo(a)
        },
        _getCaption: function() {
            var e = this;
            return e.$caption && e.$caption.length ? e.$caption.html() : e.defaultCaption
        },
        _setCaption: function(e) {
            var t = this;
            t.$caption && t.$caption.length && t.$caption.html(e)
        },
        _renderCaption: function() {
            var a, n = this,
                r = n.$element.val(),
                i = n.captionElement ? e(n.captionElement) : "";
            if (n.showCaption) {
                if (a = n.fetchCaption(r), i && i.length) return t.addCss(i, "caption"), i.html(a), void(n.$caption = i);
                n._addContent("caption", '<div class="caption">' + a + "</div>"), n.$caption = n.$container.find(".caption")
            }
        },
        _renderClear: function() {
            var a, n = this,
                r = n.clearElement ? e(n.clearElement) : "";
            if (n.showClear) {
                if (a = n._getClearClass(), r.length) return t.addCss(r, a), r.attr({
                    title: n.clearButtonTitle
                }).html(n.clearButton), void(n.$clear = r);
                n._addContent("clear", '<div class="' + a + '" title="' + n.clearButtonTitle + '">' + n.clearButton + "</div>"), n.$clear = n.$container.find("." + n.clearButtonBaseClass)
            }
        },
        _getClearClass: function() {
            var e = this;
            return e.clearButtonBaseClass + " " + (e.inactive ? "" : e.clearButtonActiveClass)
        },
        _toggleHover: function(e) {
            var t, a, n, r = this;
            e && (r.hoverChangeStars && (t = r.getWidthFromValue(r.clearValue), a = e.val <= r.clearValue ? t + "%" : e.width, r.$filledStars.css("width", a)), r.hoverChangeCaption && (n = e.val <= r.clearValue ? r.fetchCaption(r.clearValue) : e.caption, n && r._setCaption(n + "")))
        },
        _init: function(t) {
            var a, n = this,
                r = n.$element.addClass("rating-input");
            return n.options = t, e.each(t, function(e, t) {
                n[e] = t
            }), (n.rtl || "rtl" === r.attr("dir")) && (n.rtl = !0, r.attr("dir", "rtl")), n.starClicked = !1, n.clearClicked = !1, n._initSlider(t), n._checkDisabled(), n.displayOnly && (n.inactive = !0, n.showClear = !1, n.showCaption = !1), n._generateRating(), n._initEvents(), n._listen(), a = n._parseValue(r.val()), r.val(a), r.removeClass("rating-loading")
        },
        _initEvents: function() {
            var e = this;
            e.events = {
                _getTouchPosition: function(a) {
                    var n = t.isEmpty(a.pageX) ? a.originalEvent.touches[0].pageX : a.pageX;
                    return n - e.$rating.offset().left
                },
                _listenClick: function(e, t) {
                    return e.stopPropagation(), e.preventDefault(), e.handled === !0 ? !1 : (t(e), void(e.handled = !0))
                },
                _noMouseAction: function(t) {
                    return !e.hoverEnabled || e.inactive || t && t.isDefaultPrevented()
                },
                initTouch: function(a) {
                    var n, r, i, l, s, o, c, u, d = e.clearValue || 0,
                        p = "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch;
                    p && !e.inactive && (n = a.originalEvent, r = t.isEmpty(n.touches) ? n.changedTouches : n.touches, i = e.events._getTouchPosition(r[0]), "touchend" === a.type ? (e._setStars(i), u = [e.$element.val(), e._getCaption()], e.$element.trigger("change").trigger("rating.change", u), e.starClicked = !0) : (l = e.calculate(i), s = l.val <= d ? e.fetchCaption(d) : l.caption, o = e.getWidthFromValue(d), c = l.val <= d ? o + "%" : l.width, e._setCaption(s), e.$filledStars.css("width", c)))
                },
                starClick: function(t) {
                    var a, n;
                    e.events._listenClick(t, function(t) {
                        return e.inactive ? !1 : (a = e.events._getTouchPosition(t), e._setStars(a), n = [e.$element.val(), e._getCaption()], e.$element.trigger("change").trigger("rating.change", n), void(e.starClicked = !0))
                    })
                },
                clearClick: function(t) {
                    e.events._listenClick(t, function() {
                        e.inactive || (e.clear(), e.clearClicked = !0)
                    })
                },
                starMouseMove: function(t) {
                    var a, n;
                    e.events._noMouseAction(t) || (e.starClicked = !1, a = e.events._getTouchPosition(t), n = e.calculate(a), e._toggleHover(n), e.$element.trigger("rating.hover", [n.val, n.caption, "stars"]))
                },
                starMouseLeave: function(t) {
                    var a;
                    e.events._noMouseAction(t) || e.starClicked || (a = e.cache, e._toggleHover(a), e.$element.trigger("rating.hoverleave", ["stars"]))
                },
                clearMouseMove: function(t) {
                    var a, n, r, i;
                    !e.events._noMouseAction(t) && e.hoverOnClear && (e.clearClicked = !1, a = '<span class="' + e.clearCaptionClass + '">' + e.clearCaption + "</span>", n = e.clearValue, r = e.getWidthFromValue(n) || 0, i = {
                        caption: a,
                        width: r,
                        val: n
                    }, e._toggleHover(i), e.$element.trigger("rating.hover", [n, a, "clear"]))
                },
                clearMouseLeave: function(t) {
                    var a;
                    e.events._noMouseAction(t) || e.clearClicked || !e.hoverOnClear || (a = e.cache, e._toggleHover(a), e.$element.trigger("rating.hoverleave", ["clear"]))
                },
                resetForm: function(t) {
                    t && t.isDefaultPrevented() || e.inactive || e.reset()
                }
            }
        },
        _listen: function() {
            var a = this,
                n = a.$element,
                r = n.closest("form"),
                i = a.$rating,
                l = a.$clear,
                s = a.events;
            return t.handler(i, "touchstart touchmove touchend", e.proxy(s.initTouch, a)), t.handler(i, "click touchstart", e.proxy(s.starClick, a)), t.handler(i, "mousemove", e.proxy(s.starMouseMove, a)), t.handler(i, "mouseleave", e.proxy(s.starMouseLeave, a)), a.showClear && l.length && (t.handler(l, "click touchstart", e.proxy(s.clearClick, a)), t.handler(l, "mousemove", e.proxy(s.clearMouseMove, a)), t.handler(l, "mouseleave", e.proxy(s.clearMouseLeave, a))), r.length && t.handler(r, "reset", e.proxy(s.resetForm, a), !0), n
        },
        _getStars: function(e) {
            var t, a = this,
                n = '<span class="' + e + '-stars">';
            for (t = 1; t <= a.stars; t++) n += '<span class="star">' + a[e + "Star"] + "</span>";
            return n + "</span>"
        },
        _setStars: function(e) {
            var t = this,
                a = arguments.length ? t.calculate(e) : t.calculate(),
                n = t.$element,
                r = t._parseValue(a.val);
            return n.val(r), t.$filledStars.css("width", a.width), t._setCaption(a.caption), t.cache = a, n
        },
        showStars: function(e) {
            var t = this,
                a = t._parseValue(e);
            return t.$element.val(a), t._setStars()
        },
        calculate: function(e) {
            var a = this,
                n = t.isEmpty(a.$element.val()) ? 0 : a.$element.val(),
                r = arguments.length ? a.getValueFromPosition(e) : n,
                i = a.fetchCaption(r),
                l = a.getWidthFromValue(r);
            return l += "%", {
                caption: i,
                width: l,
                val: r
            }
        },
        getValueFromPosition: function(e) {
            var a, n, r = this,
                i = t.getDecimalPlaces(r.step),
                l = r.$rating.width();
            return n = r.diff * e / (l * r.step), n = r.rtl ? Math.floor(n) : Math.ceil(n), a = t.applyPrecision(parseFloat(r.min + n * r.step), i), a = Math.max(Math.min(a, r.max), r.min), r.rtl ? r.max - a : a
        },
        getWidthFromValue: function(e) {
            var t, a, n = this,
                r = n.min,
                i = n.max,
                l = n.$emptyStars;
            return !e || r >= e || r === i ? 0 : (a = l.outerWidth(), t = a ? l.width() / a : 1, e >= i ? 100 : (e - r) * t * 100 / (i - r))
        },
        fetchCaption: function(e) {
            var a, n, r, i, l, s = this,
                o = parseFloat(e) || s.clearValue,
                c = s.starCaptions,
                u = s.starCaptionClasses;
            return o && o !== s.clearValue && (o = t.applyPrecision(o, t.getDecimalPlaces(s.step))), i = "function" == typeof u ? u(o) : u[o], r = "function" == typeof c ? c(o) : c[o], n = t.isEmpty(r) ? s.defaultCaption.replace(/\{rating}/g, o) : r, a = t.isEmpty(i) ? s.clearCaptionClass : i, l = o === s.clearValue ? s.clearCaption : n, '<span class="' + a + '">' + l + "</span>"
        },
        destroy: function() {
            var a = this,
                n = a.$element;
            return t.isEmpty(a.$container) || a.$container.before(n).remove(), e.removeData(n.get(0)), n.off("rating").removeClass("rating rating-input")
        },
        create: function(e) {
            var t = this,
                a = e || t.options || {};
            return t.destroy().rating(a)
        },
        clear: function() {
            var e = this,
                t = '<span class="' + e.clearCaptionClass + '">' + e.clearCaption + "</span>";
            return e.inactive || e._setCaption(t), e.showStars(e.clearValue).trigger("change").trigger("rating.clear")
        },
        reset: function() {
            var e = this;
            return e.showStars(e.initialValue).trigger("rating.reset")
        },
        update: function(e) {
            var t = this;
            return arguments.length ? t.showStars(e) : t.$element
        },
        refresh: function(t) {
            var a = this,
                n = a.$element;
            return t ? a.destroy().rating(e.extend(!0, a.options, t)).trigger("rating.refresh") : n
        }
    }, e.fn.rating = function(n) {
        var r = Array.apply(null, arguments),
            i = [];
        switch (r.shift(), this.each(function() {
            var l, s = e(this),
                o = s.data("rating"),
                c = "object" == typeof n && n,
                u = c.theme || s.data("theme"),
                d = c.language || s.data("language") || "en",
                p = {},
                h = {};
            o || (u && (p = e.fn.ratingThemes[u] || {}), "en" === d || t.isEmpty(e.fn.ratingLocales[d]) || (h = e.fn.ratingLocales[d]), l = e.extend(!0, {}, e.fn.rating.defaults, p, e.fn.ratingLocales.en, h, c, s.data()), o = new a(this, l), s.data("rating", o)), "string" == typeof n && i.push(o[n].apply(o, r))
        }), i.length) {
            case 0:
                return this;
            case 1:
                return void 0 === i[0] ? this : i[0];
            default:
                return i
        }
    }, e.fn.rating.defaults = {
        theme: "",
        language: "en",
        stars: 5,
        filledStar: '<i class="glyphicon glyphicon-star"></i>',
        emptyStar: '<i class="glyphicon glyphicon-star-empty"></i>',
        containerClass: "",
        size: "md",
        animate: !0,
        displayOnly: !1,
        rtl: !1,
        showClear: !0,
        showCaption: !0,
        starCaptionClasses: {
            .5: "label label-danger",
            1: "label label-danger",
            1.5: "label label-warning",
            2: "label label-warning",
            2.5: "label label-info",
            3: "label label-info",
            3.5: "label label-primary",
            4: "label label-primary",
            4.5: "label label-success",
            5: "label label-success"
        },
        clearButton: '<i class="glyphicon glyphicon-minus-sign"></i>',
        clearButtonBaseClass: "clear-rating",
        clearButtonActiveClass: "clear-rating-active",
        clearCaptionClass: "label label-default",
        clearValue: null,
        captionElement: null,
        clearElement: null,
        hoverEnabled: !0,
        hoverChangeCaption: !0,
        hoverChangeStars: !0,
        hoverOnClear: !0,
        zeroAsNull: !0
    }, e.fn.ratingLocales.en = {
        defaultCaption: "{rating} Stars",
        starCaptions: {
            .5: ".5",
            1: "1",
            1.5: "1.5",
            2: "2",
            2.5: "2.5",
            3: "3",
            3.5: "3.5",
            4: "4",
            4.5: "4.5",
            5: "5"
        },
        clearButtonTitle: "Clear",
        clearCaption: ""
    }, e.fn.rating.Constructor = a, e(document).ready(function() {
        var t = e("input.rating");
        t.length && t.removeClass("rating-loading").addClass("rating-loading").rating()
    })
});
</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>

  $.validate({
    modules : 'location, date, security, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );

</script>
</body>
</html>


