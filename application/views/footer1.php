    
    <div class="clear"></div>
    
    <footer>
        <div class="Footer">
            <div class="FooterTop">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="Foot">
                                <figure><img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoDa.png"></figure>

                                <p>
                                    <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                    Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City 31st Street Fort Bonifacio TAGUIG CITY, FOURTH DISTRICT, NCR, Philippines, 1634
                                </p>

                                <p>
                                    <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                    <a href="mailto:info@jobhunt.com">help@jobyoda.com</a>
                                </p>

                                <!-- <h6><a href="tel:+63 9178721630">+63 9178721630</a></h6> -->

                            </div>
                        </div>
                        <div class="col-sm-3 col-sm-offset-1">
                            <div class="Foot">
                                <h3>Useful Links</h3>
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>privacy_policy">Privacy Policy </a></li>
                                    <li><a href="<?php echo base_url(); ?>terms">Terms of Service </a></li>
                                    <li><a href="<?php echo base_url(); ?>contact">Contact Us </a></li>
                                    <li><a href="<?php echo base_url(); ?>about">About Us </a></li> 
                                    <li><a href="<?php echo base_url(); ?>how_it_works">How it Works </a></li>
                               </ul>
                            </div>
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="Foot">
                                <h3>Get It On</h3>
                                <div class="DownloadApp">
                                    <a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" class="download-btn" target="_blank">
                                        <span><img src="https://jobyoda.com/webfiles/img/home/apple.png"></span>
                                        <p>
                                            <small>Download On</small>
                                            <br>
                                            App Store
                                        </p>
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.jobyodamo" class="download-btn" target="_blank">
                                        <span><img src="https://jobyoda.com/webfiles/img/home/appstore.png"></span>
                                        <p>
                                            <small>Get It On</small>
                                            <br>
                                            Google Play
                                        </p>
                                    </a>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="Copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>Copyright JobYoDA © 2020. All Rights Reserved</p>
                        </div>
                        <div class="col-sm-6">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/jobyodapage/" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                 
                                <li>
                                    <a href="https://instagram.com/jobyoda_ig?igshid=1u7xaxp20etlb" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </footer>

<!--     <div class="ModalBox">
        <div id="VideoModal" class="modal fade" role="dialog">
            <div class="modal-dialog"> 
                <div class="modal-content"> 
                    <div class="modal-body">
                        <a href="javascript:void(0);" class="Close" data-dismiss="modal">×</a>
                        <iframe src="https://www.youtube.com/embed/G3YeTQEdZmw"></iframe>
                    </div> 
                </div>
            </div>
        </div> 
    </div> -->

    <script src="<?php echo base_url().'webfiles/';?>js/popper.min.js"></script>
    <script src="<?php echo base_url().'webfiles/';?>newone/js/jquery.min.js"></script>
    <script src="<?php echo base_url().'webfiles/';?>newone/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url().'webfiles/';?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url().'webfiles/';?>newone/js/index.js"></script>

    <script src="<?php echo base_url().'webfiles/';?>newone/js/aos.js"></script>
    
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
	
	

    <script src="<?php echo base_url().'webfiles/';?>js/adminyoda.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <script src='https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js'></script>
    

    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script> -->
    <script type="text/javascript">
        // Animation
AOS.init({
duration: 1200,
easing: 'ease-in-out-back'
});
// animation end
    </script>
    <script type="text/javascript"> 
        if(screen.width<=768){
            $(".SearchTab").click(function() {
                var id=$(this).data('id')
                    $(".PopularTabs").hide();
                    $(".PopularTabs"+id).show();
                    $('html, body').animate({
                    scrollTop: $(".PopularTabs"+id).offset().top-90
                }, 1000);
            });
            $('.SearchTab').bind('click', function() {
                $('.active').removeClass('active')
                $(this).addClass('active');
            });
        }
        else{
              $(".SearchTab").click(function() {
                var id=$(this).data('id')
                $(".PopularTabs").hide();
                $(".PopularTabs"+id).show();
            });
        }
    </script> 

   

    <!-- <script type="text/javascript"> 
        if(screen.width<=768){
            $(".SearchTab").click(function() {
                var id=$(this).data('id')
                    $(".PopularTabs").hide();
                    $(".PopularTabs"+id).show();
                    $('html, body').animate({
                    scrollTop: $(".PopularTabs"+id).offset().top
                }, 1000);
            });
        } 
    </script> -->

    <!-- <script type="text/javascript">
        var carousel = $('.JobSlider');
        carousel.on({ 
            'initialized.owl.carousel': function () {
                 carousel.find('.item').show();
                 carousel.find('.loading-placeholder').hide();
            } 
        }).owlCarousel(options);
    </script> -->

<style type="text/css">
    #loadMore {
    padding-bottom: 30px;
    padding-top: 30px;
    text-align: center;
    width: 100%;
}
#loadMore a {
    background: #faa635;
    border-radius: 3px;
    color: white;
    display: inline-block;
    padding: 10px 30px;
    transition: all 0.25s ease-out;
    -webkit-font-smoothing: antialiased;
}
#loadMore a:hover {
    background-color: #042a63;
}
</style>


<script>
    $( document ).ready(function () {
  $(".moreBox").slice(0, 3).show();
    if ($(".blogBox:hidden").length != 0) {
      $("#loadMore").show();
    }   
    $("#loadMore").on('click', function (e) {
      e.preventDefault();
      $(".moreBox:hidden").slice(0, 6).slideDown();
      if ($(".moreBox:hidden").length == 0) {
        $("#loadMore").fadeOut('slow');
      }
    });
  });
</script>

<script>
$(function() {
  $("#popularsection").delay(3000).fadeIn(500);
});
</script>

<?php
    if($this->session->userdata('usersess')) {
        $homeurl = '/';
        $homepage = '/';
        if ($currentpage == $homeurl || $homepage) {
            include_once('modal.php');
        } else {
            include_once('innermodal.php');
        }
    } else {
        $homeurl = '/';
        $homepage = '/';
        if ($currentpage == $homeurl || $homepage) {
            include_once('modal.php');
        } else {
            include_once('innermodal.php');
        }
    }
    include_once('ratingscript.php');
?>

</body>

</html>