<?php
    include_once('header2.php');
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
    $userSess = $this->session->userdata('usersess'); 
    if ($this->session->userdata('userfsess')) {
        $userfsess = $this->session->userdata('userfsess');
        $type      = $userfsess['type'];
    }
?>

    <section>
        <div class="SliderArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="SliderText">
                            <h3>Hiring for over <span><?php echo $openings[0]['openings'];?></span> BPO positions! </h3>

                            <h4>Best BPO jobs in the Philippines in one single site sorted by benefits and distance</h4>

                            <ul>
                                <li><i class="fa fa-tags"></i> Trending Keywords :</li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hotjob">Hot Jobs,</a></li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/work_from_home">Work From Home Jobs,</a></li> 
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/14_month_pay">Jobs with 14th Month Pay,</a></li>
                                <li><a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/day_shift">Day Shift Jobs</a></li> 
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="DownloadApp">
                            <a href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" class="download-btn" target="_blank">
                                <span class="fa fa-apple"></span>
                                <p>
                                    <small>Download On</small>
                                    <br>
                                    App Store
                                </p>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.jobyodamo" class="download-btn" target="_blank">
                                <span class="fa fa-android"></span>
                                <p>
                                    <small>Get It On</small>
                                    <br>
                                    Google Play
                                </p>
                            </a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    <section>
        <div class="JobArea">
            <div class="container">
                <h1 class="Title"> Popular Searches</h1>
                <div class="JobHead">
                    <h2>BPO jobs with best benefits in the Philippines that everyone is searching for</h2>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" class="SearchTab" data-id="1" href="#Hot">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-1.png"></span>
                                Hot Jobs
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="2" href="#WorkHome">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-2.png"></span>
                                Work From Home Jobs
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="3" href="#Screening">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Rocket.png" style="filter: inherit;"></span>
                                Jobs with Instant Screening 
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="4" href="#HMO">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-6.png"></span>
                                Jobs with Day 1 HMO 
                            </a>
                        </li>
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="5" href="#Food">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-7.png"></span>
                                Jobs with Free Food 
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="6" href="#IT">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-9.png"></span>
                                IT Jobs  
                            </a>
                        </li> 
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="7" href="#Leadership">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-10.png"></span>
                                Leadership Jobs 
                            </a>
                        </li> 
                        
                        
                        <li>
                            <a data-toggle="tab" class="SearchTab" data-id="8" href="#Jobs">
                                <span><img src="<?php echo base_url().'webfiles/';?>newone/images/Searches-4.png"></span>
                                Jobs with 14th Month Pay 
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="JobBody">
                    <div class="tab-content">
                        <div id="Hot" class="PopularTabs PopularTabs1 tab-pane fade in active">
                            <h1>Hot Jobs   <?php if(count($hotJobs) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hotjob">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            <?php
                                if(!empty($hotJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $e=1;
                                    foreach($hotJobs as $hotJob) {
                                        if($hotJob['mode'] == "call" || $hotJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($hotJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($hotJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($e<=6){
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($hotJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $hotJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                        <!-- <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotJob['distance']; ?> KM</span> -->
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $hotJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>">
                                                            <?php echo $hotJob['job_title']; ?> | <span class="salaryColor"><?php echo $hotJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($hotJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($hotJob['comapnyId']); }?>"><?php echo $hotJob['companyName']; ?></a></h4>
                                                    
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($hotJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($hotJob['recruiter_id']); }?>">

                                                            <?php echo $hotJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    
                                                    <p><?php echo substr($hotJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($hotJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks1']);
                                                            }
                                                            if(!empty($hotJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks2']);
                                                            }
                                                            if(!empty($hotJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($hotJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($hotJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $e++;
                                    }
                            ?>    
                            </div>
                            <?php
                                } else {
                                    echo "<p class='nofound'> No job found </p>";
                                }
                            ?>
                        </div>  
                       
                        <div id="WorkHome" class="PopularTabs PopularTabs2 tab-pane fade">
                            <h1>Work from Home Jobs  <?php if(count($workFromhomeJobs) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/work_from_home">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            
                            <?php
                                if(!empty($workFromhomeJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $f=1;
                                    foreach($workFromhomeJobs as $workFromhomeJob) {
                                        if($workFromhomeJob['mode'] == "call" || $workFromhomeJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($workFromhomeJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($workFromhomeJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($f<=6) {
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($workFromhomeJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $workFromhomeJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                        
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $workFromhomeJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); }?>">
                                                            <?php echo $workFromhomeJob['job_title']; ?> | <span class="salaryColor"><?php echo $workFromhomeJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($workFromhomeJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($workFromhomeJob['comapnyId']); }?>"><?php echo $workFromhomeJob['companyName']; ?></a></h4>
                                                    
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($workFromhomeJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($workFromhomeJob['recruiter_id']); }?>">

                                                            <?php echo $workFromhomeJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    
                                                    <p><?php echo substr($workFromhomeJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($workFromhomeJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($workFromhomeJob['toppicks1']);
                                                            }
                                                            if(!empty($workFromhomeJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($workFromhomeJob['toppicks2']);
                                                            }
                                                            if(!empty($workFromhomeJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($workFromhomeJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($workFromhomeJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $f++;
                                    }
                            ?>
                            </div>
                            <?php
                                } else {
                                    echo "<p class='nofound'> No job found </p>";
                                }
                            ?>
                        </div>

                        <div id="Screening" class="PopularTabs PopularTabs3 tab-pane fade">
                            <h1>Jobs with Instant Screening   <?php if(count($instantJobs) > 0) { ?>  <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/instant_screening">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            
                            <?php
                                if(!empty($instantJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $k=1;
                                    foreach($instantJobs as $instantJob) {
                                        if($instantJob['mode'] == "call" || $instantJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($instantJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($instantJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($k<=6) {
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($instantJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $instantJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                        
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $instantJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); }?>">
                                                            <?php echo $instantJob['job_title']; ?> | <span class="salaryColor"><?php echo $instantJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($instantJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($instantJob['comapnyId']); }?>"><?php echo $instantJob['companyName']; ?></a></h4>
                                                    
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($instantJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($instantJob['recruiter_id']); }?>">
                                                            <?php echo $instantJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    
                                                    <p><?php echo substr($instantJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($instantJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($instantJob['toppicks1']);
                                                            }
                                                            if(!empty($instantJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($instantJob['toppicks2']);
                                                            }
                                                            if(!empty($instantJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($instantJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($instantJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $k++;
                                    }
                            ?>
                            </div>
                            <?php
                                } else {
                                        echo "<p class='nofound'> No job found </p>";
                                    }
                            ?>
                        </div> 

                        <div id="HMO" class="PopularTabs PopularTabs4 tab-pane fade">
                            <h1>Jobs with Day 1 HMO  <?php if(count($day1hmoJobs) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/hmo">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            <?php
                                if(!empty($day1hmoJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $g=1;
                                    foreach($day1hmoJobs as $day1hmoJob) {
                                        if($day1hmoJob['mode'] == "call" || $day1hmoJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($day1hmoJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($day1hmoJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($g<=6) {
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($day1hmoJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $day1hmoJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                        
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $day1hmoJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); }?>">
                                                            <?php echo $day1hmoJob['job_title']; ?> | <span class="salaryColor"><?php echo $day1hmoJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($day1hmoJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($day1hmoJob['comapnyId']); }?>"><?php echo $day1hmoJob['companyName']; ?></a></h4>
                                                    
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($day1hmoJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($day1hmoJob['recruiter_id']); }?>">

                                                            <?php echo $day1hmoJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    
                                                    <p><?php echo substr($day1hmoJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($day1hmoJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($day1hmoJob['toppicks1']);
                                                            }
                                                            if(!empty($day1hmoJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($day1hmoJob['toppicks2']);
                                                            }
                                                            if(!empty($day1hmoJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($day1hmoJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($day1hmoJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $g++;
                                    }
                            ?>
                            </div>
                            <?php
                                } else {
                                    echo "<p class='nofound'> No job found </p>";
                                }
                            ?>
                        </div> 

                        <div id="Food" class="PopularTabs PopularTabs5 tab-pane fade">
                            <h1>Jobs with Free Food  <?php if(count($freefoodJobs) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/free_food">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            
                            <?php
                                if(!empty($freefoodJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $h=1;
                                    foreach($freefoodJobs as $freefoodJob) {
                                        if($freefoodJob['mode'] == "call" || $freefoodJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($freefoodJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($freefoodJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($h<=6) {
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($freefoodJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $freefoodJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                        
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $freefoodJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); }?>">
                                                            <?php echo $freefoodJob['job_title']; ?> | <span class="salaryColor"><?php echo $freefoodJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($freefoodJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($freefoodJob['comapnyId']); }?>"><?php echo $freefoodJob['companyName']; ?></a></h4>
                                                    
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($freefoodJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($freefoodJob['recruiter_id']); }?>">

                                                            <?php echo $freefoodJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    
                                                    <p><?php echo substr($freefoodJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($freefoodJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($freefoodJob['toppicks1']);
                                                            }
                                                            if(!empty($freefoodJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($freefoodJob['toppicks2']);
                                                            }
                                                            if(!empty($freefoodJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($freefoodJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($freefoodJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $h++;
                                    }
                            ?>
                            </div>
                            <?php
                                } else {
                                    echo "<p class='nofound'> No job found </p>";
                                }
                            ?>
                        </div>

                        <div id="IT" class="PopularTabs PopularTabs6 tab-pane fade">
                            <h1>IT Jobs  <?php if(count($itJobs) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/information_technology">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            
                            <?php
                                if(!empty($itJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $i=1;
                                    foreach($itJobs as $itJob) {
                                        if($itJob['mode'] == "call" || $itJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($itJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($itJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($i<=6) {
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($itJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $itJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                        
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $itJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); }?>">
                                                            <?php echo $itJob['job_title']; ?> | <span class="salaryColor"><?php echo $itJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?> 
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($itJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($itJob['comapnyId']); }?>"><?php echo $itJob['companyName']; ?></a></h4>
                                                    
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($itJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($itJob['recruiter_id']); }?>">

                                                            <?php echo $itJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    
                                                    <p><?php echo substr($itJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($itJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($itJob['toppicks1']);
                                                            }
                                                            if(!empty($itJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($itJob['toppicks2']);
                                                            }
                                                            if(!empty($itJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($itJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($itJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $i++;
                                    }
                            ?>
                            </div>
                            <?php
                                } else {
                                    echo "<p class='nofound'> No job found </p>";
                                }
                            ?>
                        </div>

                        <div id="Leadership" class="PopularTabs PopularTabs7 tab-pane fade">
                            <h1>Leadership Jobs  <?php if(count($leadershipJobs) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/leadership">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            
                            <?php
                                if(!empty($leadershipJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $j=1;
                                    foreach($leadershipJobs as $leadershipJob) {
                                        if($leadershipJob['mode'] == "call" || $leadershipJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($leadershipJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($leadershipJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($j<=6) {
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($leadershipJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $leadershipJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                        
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                    <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $leadershipJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); }?>">
                                                            <?php echo $leadershipJob['job_title']; ?> | <span class="salaryColor"><?php echo $leadershipJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($leadershipJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($leadershipJob['comapnyId']); }?>"><?php echo $leadershipJob['companyName']; ?></a></h4>
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($leadershipJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($leadershipJob['recruiter_id']); }?>">

                                                            <?php echo $leadershipJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    <p><?php echo substr($leadershipJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($leadershipJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($leadershipJob['toppicks1']);
                                                            }
                                                            if(!empty($leadershipJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($leadershipJob['toppicks2']);
                                                            }
                                                            if(!empty($leadershipJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($leadershipJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($leadershipJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $j++;
                                    }
                            ?>
                            </div>
                            <?php
                                } else {
                                    echo "<p class='nofound'> No job found </p>";
                                }
                            ?>
                        </div>

                        <div id="Jobs" class="PopularTabs PopularTabs8 tab-pane fade">
                            <h1>Jobs with 14th Month Pay  <?php if(count($monthpayJobs) > 0) { ?> <a href="<?php echo base_url(); ?><?php echo $listingTypeFun; ?>/14_month_pay">view all <i class="fa fa-angle-double-right"></i></a> <?php } ?> </h1>
                            
                            <?php
                                if(!empty($monthpayJobs)) {
                            ?>
                            <div class="owl-carousel owl-theme JobSlider">
                            <?php
                                    $f=1;
                                    foreach($monthpayJobs as $monthpayJob) {
                                        if($monthpayJob['mode'] == "call" || $monthpayJob['mode'] == "Call") {
                                            $modeText = "OVER THE PHONE INTERVIEW";
                                        } else if($monthpayJob['mode'] == "Walk-in") {
                                            $modeText = "WALK IN INTERVIEW";
                                        } else if($monthpayJob['mode'] == "Instant screening") {
                                            $modeText = "INSTANT SCREENING";
                                        }
                                        if($f<=6) {
                            ?>
                                        <div class="item">
                                            <div class="JobsBox">
                                                <figure>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); }?>">
                                                        <?php
                                                            if(!empty($monthpayJob['job_image'])) {
                                                        ?>
                                                                <img src="<?php echo $monthpayJob['job_image']; ?>">
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <img src="<?php echo base_url().'webfiles/';?>img/user_man.png">
                                                        <?php
                                                            }
                                                        ?>
                                                        <!-- <span class="Logo"><img src="images/Company-2.png"></span> -->
                                                       
                                                    </a>
                                                </figure>
                                                <figcaption>
                                                     <span class="Distance"><i class="fa fa-map-marker"></i> <?php echo $monthpayJob['distance']; ?> KM</span>
                                                    <h3>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); }?>">
                                                            <?php echo $monthpayJob['job_title']; ?> | <span class="salaryColor"><?php echo $monthpayJob['salary']; ?> Monthly</span> | <?php echo $modeText; ?>
                                                        </a>
                                                    </h3>
                                                    <h4><a href="<?php if (!empty($userSess)) { echo base_url();?>site_details/<?php echo base64_encode($monthpayJob['comapnyId']);}else{ echo base_url();?>site_details/<?php echo base64_encode($monthpayJob['comapnyId']); }?>"><?php echo $monthpayJob['companyName']; ?></a></h4>
                                                    
                                                    <h5>
                                                        <a href="<?php if (!empty($userSess)) { echo base_url();?>company_details/<?php echo base64_encode($monthpayJob['recruiter_id']);}else{ echo base_url();?>company_details/<?php echo base64_encode($monthpayJob['recruiter_id']); }?>">

                                                            <?php echo $monthpayJob['cname']; ?>
                                                        </a>
                                                    </h5>
                                                    
                                                    <p><?php echo substr($monthpayJob['jobPitch'], 0,45).'...'; ?></p> 
                                                    <ul>
                                                        <?php
                                                            if(!empty($monthpayJob['toppicks1'])) {
                                                                echo $getToppickByFunction = getToppickFunction($monthpayJob['toppicks1']);
                                                            }
                                                            if(!empty($monthpayJob['toppicks2'])) {
                                                                echo $getToppickByFunction = getToppickFunction($monthpayJob['toppicks2']);
                                                            }
                                                            if(!empty($monthpayJob['toppicks3'])) {
                                                                echo $getToppickByFunction = getToppickFunction($monthpayJob['toppicks3']);
                                                            }
                                                        ?>
                                                    </ul>
                                                    <a href="<?php if (!empty($userSess)) { echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); } else{ echo base_url();?>job/description/<?php echo base64_encode($monthpayJob['jobpost_id']); }?>" class="GetJob">Get this job</a> 
                                                </figcaption>
                                            </div>
                                        </div>
                            <?php
                                        } else {
                                            break;
                                        }
                                        $f++;
                                    }
                            ?>
                            </div>
                            <?php
                                } else {
                                    echo "<p class='nofound'> No job found </p>";
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="StatsArea" style="padding: 0px 0 0;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <center style="margin-bottom: 0px;"><img src="<?php echo base_url().'webfiles/';?>newone/images/THANK_YOU_PAGE_main_photo_SEPT.png" style="width:80%"></center>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="StatsArea" style="padding: 0px 0 0;">
            <div class="container">
                <h1>JobYoDA Site Stats</h1>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="StatsBox">
                            <span class="Icon"> <i class="fa fa-building" aria-hidden="true"></i> </span>
                            <h3 data-number="50"> 
                                <span id="Number3"><?php echo count($companylists); ?></span> 
                            </h3>
                            <p>Companies</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="StatsBox">
                            <span class="Icon"><i class="fa fa-handshake-o" aria-hidden="true"></i> </span>
                            <h3 data-number="27062"> 
                                <span id="Number4"><?php echo $openings[0]['openings'];?></span> 
                            </h3>
                            <p>Employment Opportunities</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="StatsBox">
                            <span class="Icon"> <i class="fa fa-suitcase" aria-hidden="true"></i> </span>
                            <h3 data-number="54426"> 
                                <span id="Number5"><?php echo count($userlist) ?></span> 
                            </h3>
                            <p>Jobseekers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
    function getToppickFunction($toppickID) {

        if($toppickID == 1) {
                                                            
            return '<li><img src="'.base_url() .'recruiterfiles/images/m_bonus.png"> Joining Bonus</li>';

        } else if($toppickID == 2) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_freefood.png"> Free Food</li>';

        } else if($toppickID == 3) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_day_1_hmo.png"> Day 1 HMO</li>';

        } else if($toppickID == 4) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dependent_hmo.png"> Day 1 HMO for Dependent</li>';

        } else if($toppickID == 5) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_dayshift.png"> Day Shift</li>';

        } else if($toppickID == 6) {

            return '<li><img src="'.base_url() .'recruiterfiles/images/m_14th_pay.png"> 14th Month Pay </li>';
        
        } else if($toppickID == 7) {

            return '<li><img src="'. base_url(). 'webfiles/newone/images/Searches-2.png"> Work From Home </li>';
        
        } else {
            return "";
        }                                                 
    }
?>
<?php
    include_once('footer1.php');
?>