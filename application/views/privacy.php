<?php include_once('header.php'); ?>


<div class="container">
	
	<div class="contenttxtds pageformat">
	<div class="mainheadings-yoda">
                
              </div>
			  
              <div class="PrivacyArea">
            
                <div class="PrivacyBox">
                    <p>Effective Date: <strong>Aug 20, 2019</strong></p>
                    <p>Hello, At JobYoDA.com website and JobYoDA mobile application, your data privacy is really important. Kindly read this document on Privacy Notice carefully as it forms part of the terms of use which govern the use of JobYoDA IT APP Services and the JobYoDA.com Websites Services (hereafter either or combined of JobYoDA IT platform services referred to JobYoDA.com Services).</p>

                    <h4>Scope of the Privacy policy notice -</h4>                   

                    <p>This privacy policy applies to the users of JobYoDA.com website and the JobYoDA mobile application “JobYoDA.com Services”.  which may include recruiters, jobseekers and any other person using the mentioned for purposes as defined in the terms.  This further explains the type of personal information the purposes for which JobYoDA collects and processes such personal information and how it would be used. </p>

                    <p>Any changes to the privacy policy would be notified to you via a suitable announcement on the JobYoDA.com website and/or JobYoDA mobile application. The changes will apply to the use of the JobYoDA.com Services after JobYoDA have given you notice. If you do not wish to accept the new Terms you should not continue to use the JobYoDA.com Services. If you continue to use the JobYoDA IT APP Services and/or the JobYoDA.com Websites (“JobYoDA.com Services”) after the date on which the changes come into effect, your use of the JobYoDA IT APP Services and/or the JobYoDA.com Websites indicates your agreement to be bound by the new Terms.</p>

                </div>

                <h2>COLLECTION OF INFORMATION</h2>
                    <div class="PrivacyBox">                         
                        <article>
                            <p>JobSeekers, location, nationality, academic & work credentials, salary, resume, photo etc.</p>

                            <p>Whenever the personal information requested by JobYoDA.com Services is marked as “Required Field”, you must provide and agree to the processing of this personal information </p>

                            <p>Every time a user downloads and registers on the JobYoDA.com Services you would be asked to provide personal information through the various forms and would be proceeded with only after a valid consent provided by you. By giving such valid consent the user understands, agrees and consent to the collection, use and storage of personal information including name, email address, phone number, date of birth on by JobYoDA.com Services. If you do not agree to provide this personal information and/or not agree to us processing it in the manner set out in this Privacy Notice, then JobYoDA will be unable to provide the relevant services.</p>
                        </article>
                    </div>

                    <div class="PrivacyBox">
                        <h3>Recruiter Companies</h3>
                        <p>JobYoDA.com Services collects the company name, business address and billing address of Recruiter Companies. It also collects the name, email address and phone number of a primary contact (also referred to on the Site as the ‘administrator’). Administrator can choose to add additional sub recruiters to their account, in which case JobYoDA.com will collect the name and email address of those other users. Administrator can change the company name, billing address, or any other details by contacting the designated </p> 
                    </div>

                    <div class="PrivacyBox">
                        <h3>Jobseekers and Recruiters</h3>
                        <p>If you choose to use JobYoDA.com Services to refer to  a friend about its services, JobYoDA will ask for your friend’s name, mobile number and/or email address. An email, message, WhatsApp, Viber, messenger, LinkedIn or other electronic communication modes inviting your friend to use the JobYoDA.com Services, will be sent to him/her automatically. The giving of this information represents that you have obtained your friend’s consent for his/her name, phone number and email address to be given to JobYoDA Inc.</p> 

                        <p>In addition, JobYoDA.com Services may ask your permission to post your success story or testimonial. If you agree to post your material on JobYoDA IT APP or Websites (JobYoDA.com Services), you should be aware that any personal information you submit can be read, collected, or used by other users of the JobYoDA.com Services and could be used to send you unsolicited messages. If you wish to update or delete your success stories or testimonial, you can contact us at help@jobyoda.com. </p>

                        <p>You agree to indemnify, hold harmless and defend the JobYoDA.com Services, and its employees, officers, directors, agents, successors and assigns from and against damages, penalties, losses, liabilities, judgments, settlements, awards, and costs and expenses of any kind or nature, including reasonable attorneys’ fees, suffered or incurred in connection with any claims, demands, causes of action, suits, proceedings or other actions, arising from a willful breach of the representations.</p>
                    </div>
 
                    <div class="PrivacyBox">
                        <h3>When you visit the JobYoDA.com websites and or mobile applications.</h3>

                        <ul>
                            <li>When you visit any JobYoDA.com Websites our web servers automatically collect information about your visit to these websites, including your Internet Protocol (IP) address, the time, date and duration of your visit. your IP address is a unique identifier for your computer or other access device.</li>

                            <li>JobYoDA.com may track your visits to any JobYoDA.com Websites by placing a “cookie” on your computer or other access device when you enter. Cookies are small text files that are placed on your computer or other access device by websites that you visit. They are widely used in order to make websites work, or work more effectively, as well as to provide information to the owners of the websites.</li>

                            <li>Cookies allow JobYoDA.com to save preferences for you so that you will not have to re-enter them the next time you visit. Cookies also help JobYoDA.com collect anonymous click stream data for tracking user trends and patterns. JobYoDA.com may use the anonymous click stream data to help the Advertiser to deliver better-targeted advertisements.</li>

                            <li>You can remove cookies by following directions provided in your internet browser’s “help” file. You should understand that areas of certain websites will not function properly if you set your internet browser to not accept cookies.</li>

                            <li>JobYoDA.com also uses clear gifs in its HTML-based emails in order to find out which emails have been opened by the recipients. This allows JobYoDA.com to gauge the effectiveness of certain communications and the effectiveness of its marketing campaigns.</li>

                            <li>Our partners and service providers use cookies to collect information about the usage of JobYoDA.com Websites. The use of cookies by our partners and service providers are not covered by our privacy policy. We do not have access or control over these cookies</li>

                        </ul>

                    </div>

                    <div class="PrivacyBox">
                        <h2>PURPOSES OF COLLECTION</h2>

                        <p>The purposes for which JobYoDA.com Services collects and processes your personal information includes tasks such as authenticate your identity, to administer and manage the JobYoDA.com Services provided to you, to provide information on the right job matching with your qualifications, experience and other benefits chosen by you. We would use the same also to contact you about any changes to the product or services as also for any marketing research and statistical purposes. To monitor and improve the performance of JobYoDA.com Services, to investigate and resolve any enquiries, complaints or any other concerns raised through help@jobyoda.com, </p>

                        <p>The collection and processing, including the use, sharing and retention of information shall only be for valid purposes and to the extent necessary to achieve those purposes. Where necessary for any of the above purposes, JobYoDA.com or its service providers shall use automated processes such as artificial intelligence and data analytics in relation to your personal information.</p>

                    </div>

                    <div class="PrivacyBox">
                        <h2>PROCESSING AND SHARING OF INFORMATION</h2>

                        <p>JobYoDA.com Services aims to provide best in class and highly disruptive application to jobseekers that enables them to choose jobs on the basis of user preferences such as location proximity, shift timings, medical  benefits, joining bonus etc. We aim to bring the right kind of job to the right kind of candidate by letting recruiters and job seekers see eye to eye. Through the various filters available on the recruiter portal as well as the JobYoDA application the platform seeks to achieve this aim. The information provided by the jobseeker would be used for the said purposes and as such shared with the recruiters matching the requirements. The JobYoDA platform also allows the jobseeker to find a suitable job and apply for the same.  The user can also directly communicate with the recruiter through text, Whatsapp, Viber or other telecommunication channels. JobYoDA shall not be responsible under this policy for the availability, accessibility and storage of information outside the JobYoDA platform. Also JobYoDA is not a party to such arrangements including employee onboarding, administration of employee benefits, finalization of hired candidates KPI’s or any other tasks that construes JobYoDA as a recruitment company.   Also note that the information independently disclosed to recruiters outside the JobYoDA platform shall not be covered by this privacy policy.</p>
 

                    </div>

                    <div class="PrivacyBox">
                        <h2>STORAGE, RETENTION AND DELETION OF INFORMATION</h2>

                        <p>JobYoDA.com Services will retain your personal information for the period necessary or deemed fit to fulfil the purposes as defined in this privacy policy or any legal or business purposes. </p>

                        <p>You may request for deletion of your JobYoDA account at any time in which event JobYoDA.com Services will remove all access to your account, resume and JobYoDA Profile in the database. The deletion of your JobYoDA account does not affect JobYoDA Profile that you had already sent to Recruiters or were previously downloaded by recruiters</p>

                        <p>JobYoDA.com is committed to keeping personal information secure. JobYoDA.com has appropriate technical, administrative and physical procedures in place to protect personal information from loss, theft and misuse, as well as against unauthorized access, disclosure, alteration and destruction.  No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, we cannot guarantee its absolute security. If you have any questions about security on our Website, you can contact us at help@jobyoda.com. JobYoDA shall duly investigate to resolve data breaches in adherence to breach management procedures described by the local laws and regulations as applicable. </p>

                    </div>


                    <div class="PrivacyBox">
                        <h2>USER OBLIGATIONS REGARDING PERSONAL INFORMATION</h2>

                        <p>You are responsible for providing accurate, not misleading, complete and up-to-date information to JobYoDA.com Services about yourself and any other person whose personal information you provide us and for updating this personal information as and when it becomes inaccurate, misleading, incomplete and out-of-date by contacting JobYoDA.com Services.</p>

                    </div>

                    <div class="PrivacyBox">
                        <h2>USER CONSENT</h2>

                        <p>In using the JobYoDA.com Services, you consent to the collection and use of the personal information by JobYoDA.com in the ways described hereinabove (which may change from time to time) unless and until you inform JobYoDA.com to the contrary at help@jobyoda.com</p>

                    </div>

                    <div class="PrivacyBox">
                        <h2>CONTACT INFORMATION</h2>

                        <p>If you still have questions about this Privacy Notice, please visit JobYoDA.com or reach out to help@jobyoda.com or you may also contact Data Privacy Officer through the following email address:</p>

                        <p><strong>Email Address:</strong> <a href="mailto:dataprivacyofficer@JobYoDA.com">dataprivacyofficer@JobYoDA.com</a></p>
                        <p><strong>JobYoDA.Inc. </strong><br>
                            Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City, 31st street Fort Bonifacio Taguig City, Fourth District, NCR Philippines 1634.</p>


                    </div>



                    <!-- <div class="PrivacyBox">

                        <h3>3. When you visit the JobYoDA.com Websites..</h3>
                        <article>
                            <p>1. When you visit any JobYoDA.com Websites our web servers automatically collect information about your visit to these websites, including your Internet Protocol (IP) address, the time, date and duration of your visit. your IP address is a unique identifier for your computer or other access device.</p>

                            <p>2. JobYoDA.com may track your visits to any JobYoDA.com Websites by placing a “cookie” on your computer or other access device when you enter. Cookies are small text files that are placed on your computer or other access device by websites that you visit. They are widely used in order to make websites work, or work more effectively, as well as to provide information to the owners of the websites.</p>

                            <p>3. Cookies allow JobYoDA.com to save preferences for you so that you will not have to re-enter them the next time you visit. Cookies also help JobYoDA.com collect anonymous click stream data for tracking user trends and patterns. JobYoDA.com may use the anonymous click stream data to help the Advertiser to deliver better-targeted advertisements.</p>

                            <p>4. You can remove cookies by following directions provided in your internet browser’s “help” file. You should understand that areas of certain websites will not function properly if you set your internet browser to not accept cookies.</p>
                            
                            <p>5. JobYoDA.com also uses clear gifs in its HTML-based emails in order to find out which emails have been opened by the recipients. This allows JobYoDA.com to gauge the effectiveness of certain communications and the effectiveness of its marketing campaigns.</p>
                            
                            <p>6. Our partners and service providers use cookies to collect information about the usage of JobYoDA.com Websites. The use of cookies by our partners and service providers is not covered by our privacy policy. We do not have access or control over these cookies.</p>
                        </article>
                    </div>

                <h2>2. PURPOSES OF COLLECTING AND USE OF PERSONAL INFORMATION</h2>

                    <div class="PrivacyBox">

                        <h4>1. The purposes for which JobYoDA.com processes your personal information are as follows:</h4>
                        <article>
                            <ul>
                                <li>To verify your identity</li>

                                <li>To assess and/or verify your employability and credit worthiness</li>

                                <li>To provide the JobYoDA.com Services which you have requested</li>

                                <li>To administer and manage the JobYoDA.com Services provided to you</li>

                                <li>To contact you in respect of JobYoDA.com Services</li>

                                <li>To facilitate the provision to you of tailored educational offerings and improve your career prospects by supplying your information to our strategic partners</li>

                                <li>To improve placement opportunities for you or for tailoring specific services to you</li>

                                <li>To verify your academic and professional qualification by contacting the school/college/university/institute/ professional qualifying bodies</li>

                                <li>To contact the referee and/or the guarantor whom details have been provided by you</li>

                                <li>To process your order for the JobYoDA.com Services which you have requested</li>

                                <li>To investigate and resolve any of the JobYoDA.com Services, billing queries, complaints or other enquiries that you submit to JobYoDA.com regarding JobYoDA.com Services</li>

                                <li>To manage staff training and quality assurance</li>

                                <li>To monitor and improve the performance of the JobYoDA.com Websites and the JobYoDA.com Services</li>

                                <li>To maintain and develop the JobYoDA.com Websites and JobYoDA.com Services</li>

                                <li>To gain an understanding of your information and communication needs in order for JobYoDA.com to enhance and customize the JobYoDA.com Services</li>

                                <li>To conduct research and development and statistical analysis in connection with the JobYoDA.com Services to identify trends and develop new services that reflect your interests</li>

                                <li>To assist JobYoDA.com in understanding your browsing preferences on the JobYoDA.com Websites so that JobYoDA.com can tailor content accordingly</li>

                                <li>To detect and prevent fraudulent activity.</li>

                                <p>Where necessary for any of the above purposes, JobYoDA.com or its service providers shall use automated processes such as artificial intelligence and data analytics in relation to your personal information.</p>
                            </article>
                        </ul>

                        <p>2. You are not able to limit the processing of your personal information for the purposes set out in Clause 2.1 above. If you do not consent to JobYoDA.com processing your personal information for the said purposes, you must terminate your relevant agreement with JobYoDA.com for the JobYoDA.com Services and stop using the JobYoDA.com Websites.</p>

                        <h4>3. In addition, JobYoDA.com may use your personal information for the following purposes:-</h4>
                            <article>
                                <p>1. To promote and market to you:</p>
                                    <ul>
                                        <li>Other JobYoDA.com Services; or</li>
                                        <li>Services of third parties which JobYoDA.com thinks may be of interest of you;</li>
                                    </ul> 
                                <p>2. To send you seasonal greeting messages and/or message informing you of a critical performance error in the JobYoDA.com Websites and/or the JobYoDA.com Services.</p>

                                <p>3. To send you tips, advice and survey information to maximize your career development including but not limited to using the JobYoDA.com Services and/or the JobYoDA.com Websites.</p>

                                <p>4. JobYoDA.com will ask for your consent before processing your personal information other than those set out in Clauses 2.1 and 2.3.</p>
                            </article>  
                    </div>

                <h2>3. JOBYODA.COM PROFILE</h2>

                    <div class="PrivacyBox">

                        <h3></h3>

                        <p>1. JobYoDA.com gives you the option of putting your JobYoDA.com Profile in the JobYoDA.com Resume Database provided that you use JobYoDA.com’s preset format for uploading your information. There are two ways of doing so:-</p>

                        <article>
                            <p>1. You can store your JobYoDA.com Profile in the JobYoDA.com Resume Database, but not allow it to be searchable by potential Employers or Advertisers. Not allowing your JobYoDA.com Profile to be searchable means that you can use it to apply for a job online, but Employers or Advertisers will not have access to search it through the JobYoDA.com Resume Database.</p>

                            <p>2. You can allow your JobYoDA.com Profile to be searchable by Employers or Advertisers. When you opt to make your JobYoDA.com Profile searchable, your full profile, resume and personal information will be stored in the site’s databank and will be visible to Employers or Advertisers who download your JobYoDA.com Profile from JobYoDA.com Resume Database.</p>
                        </article>

                        <p>2. JobYoDA.com uses its best effort to restrict access to the JobYoDA.com Resume Database only to those who have subscribed to the JobYoDA.com Services, these parties may retain a copy of your JobYoDA.com Profile in their own files or databases.</p>

                        <p>3. JobYoDA.com will take reasonable steps to ensure that parties other than those mentioned above will not, without JobYoDA.com’s consent, gain access to the JobYoDA.com Resume Database. However, JobYoDA.com is not responsible for the retention, use or privacy of JobYoDA.com Profile by any third parties.</p>

                        <p>4. Notwithstanding Clause 3.1, JobYoDA.com reserves the right to have full access to your JobYoDA.com Profile for the purpose set out in Clause 2.1 to perform JobYoDA.com Services.</p> 
                    </div>

                <h2>4. CHOICE AND ACCESS OF PERSONAL INFORMATION</h2>
                    <div class="PrivacyBox"> 

                        <p>1. You may have different privacy concerns. JobYoDA.com’s goal is to be clear of what information it collects, so that you can make meaningful choices about how it is used. For example:-</p>
                        <article>
                            <ul>
                                <li>You can control who you share your personal information with;</li>
                                <li>You can review and control your subscription to various marketing preferences, JobYoDA.com Services;</li>
                                <li>You may view, edit or delete your personal information and preferences at any time.</li>
                                <li>You may choose not to receive any marketing materials from JobYoDA.com;</li>
                                <li>You may also subscribe to additional JobYoDA.com Services by logging into a MyJobYoDA.com account;</li>
                                <li>You may obtain a copy of your personal information by contacting JobYoDA.com here.</li>
                            </ul>
                        </article>

                        <p>2. You can delete your MyJobYoDA.com account at any time in which event JobYoDA.com will remove all access to your account, resume and JobYoDA.com Profile in the database. The deletion of your MyJobYoDA.com account does not affect JobYoDA.com Profile that you had already sent to Employers or were previously downloaded by Employers, Advertisers.</p> 
                    </div>

                <h2>5. RETENTION OF PERSONAL INFORMATION</h2>
                    <div class="PrivacyBox"> 

                        <p>1. JobYoDA.com will retain your personal information for the period necessary to fulfill the purposes set out in Clause 2 above and any legal or business purposes.</p> 

                    </div>

                <h2>6. SECURITY OF PERSONAL INFORMATION</h2>
                    <div class="PrivacyBox"> 

                        <p>1. JobYoDA.com is committed to keeping personal information secure. JobYoDA.com has appropriate technical, administrative and physical procedures in place to protect personal information from loss, theft and misuse, as well as against unauthorized access, disclosure, alteration and destruction. Sensitive information (such as a credit card number) entered on our payment gateway services are encrypted during the transmission of that information using secure socket layer technology (SSL).</p>
        
                        <p>2. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, we cannot guarantee its absolute security. If you have any questions about security on our Web site, you can contact us https://jobyoda.com/contact</p> 

                    </div>

                 <h2>7. TO WHOM PERSONAL INFORMATION IS DISCLOSED</h2>

                    <div class="PrivacyBox"> 

                        <p>1. Personal information mentioned in Clause 1 above may be disclosed to the classes of third parties as follows in order for JobYoDA.com to effectively manage its business including to render the JobYoDA.com Services to you to achieve the purposes described in Clause 2:-</p>

                        <ul>
                            <li>The Advertisers;</li>
                            <li>The Employers;</li>
                            <li>Third parties contracted by JobYoDA.com to assist JobYoDA.com in delivering all or part of the JobYoDA.com Services to you, including without limitation, to the following:-
                                <ul>
                                    <li>Payment gateway services for optional services;</li>
                                    <li>Profiling/assessment services;</li>
                                    <li>Online advertising services;</li>
                                    <li>Mapping services;</li>
                                    <li>Maintenance and repair services; and</li>
                                    <li>Market research and website usage analysis services.</li>
                                </ul>
                            </li>


                            <li>Strategic partners that work with JobYoDA.com to provide the JobYoDA.com Services or that help market and promote JobYoDA.com Services and/or JobYoDA.com Websites;</li>

                            
                            <li>Strategic partners that provide ancillary services such as education offerings for the purposes of improving your career prospects and/or professional qualifications.</li>

                            
                            <li>The school/college/university/institute you studied at and your referees so as to verify your academic qualification;</li>

                            
                            <li>Professional qualifying bodies where you obtained your accreditation;</li>

                            
                            <li>Where you log in to a JobYoDA.com Website using Facebook Connect – your Facebook friends who have also logged in in to our site using Facebook Connect;</li>

                            
                            <li>Regulatory bodies, governmental bodies or other authorities if required or authorized to do so to discharge any regulatory function, under any law or in relation to any order or judgment of a court;</li>

                            
                            <li>Regulatory bodies, governmental bodies or other authorities for the purpose of detection or prevention of crime, illegal/unlawful activities or fraud or for the apprehension or prosecution of offenders, or for an investigation relating to any of these;</li>
                            
                            <li>Any party involved in or related to a legal proceeding (or prospective legal proceeding), for purposes of the legal proceedings;</li>
                            
                            <li>JobYoDA.com’s professional advisors on a need to know basis for the purpose of those advisors providing advice to JobYoDA.com;</li>
                            
                            <li>Any third party which acquires all or part of the assets or business of JobYoDA.com (including accounts and trade receivables) for the purpose of that third party continuing to provide all or that part of the JobYoDA.com Services which it acquired; and</li>
                            
                            <li>Otherwise permitted under any data protection legislation.</li>

                        </ul>

                        <p>2. Other than set out above, you will receive notice when personal information about you might go to third parties, and you will have an opportunity to choose not to share the information.</p>

                        <p>3. JobYoDA.com does not sell or rent any personal information collected to any other parties.</p> 

                    </div>
                    

                 <h2>8. YOUR OBLIGATIONS REGARDING YOUR PERSONAL INFORMATION</h2>
                    <div class="PrivacyBox">

                        <p>1. You are responsible for providing accurate, not misleading, complete and up-to-date information to JobYoDA.com about yourself and any other person whose personal information you provide us and for updating this personal information as and when it becomes inaccurate, misleading, incomplete and out-of-date by contacting JobYoDA.com (need to provide link here) . Upon your request, JobYoDA.com may inform third parties that previously received your personal information will be informed of the changes made. Such requests may be directed to JobYoDA.com ( need to provide link here.).</p>

                        <p>2. In the circumstances you may need to provide to JobYoDA.com personal information about someone other than yourself (for example, your referee or guarantor). If so, we rely on you to inform these individual that you are providing their personal information to JobYoDA.com, to make sure they consent to you giving us their information and to advise them about where they can find a copy of this Privacy Notice (on our website at Privacy Policy).</p>

                    </div>

                 <h2>9. TRANSFER OF YOUR PERSONAL INFORMATION OUTSIDE OF YOUR LOCAL JURISDICTION</h2>

                    <div class="PrivacyBox"> 
                        <p>1. It may be necessary for JobYoDA.com to transfer your personal information outside of your local jurisdiction if any of JobYoDA.com’s service providers or strategic partners (“overseas entities”) is involved in providing part of the JobYoDA.com Services.</p> 
                    </div>

                <h2>10. LINKED SITES</h2>

                    <div class="PrivacyBox"> 
                        
                        <p>1. JobYoDA.com Websites that may contain links to third party sites.</p>
                        
                        <p>2. JobYoDA.com is not responsible for such third party sites. Any personal information made available by you on such sites will not have the benefit of this Privacy Notice and will be subject to the relevant third party’s privacy policy (if any).</p>
                        
                        <p>3. You can login to our site using sign-in services such as Facebook Connect. This service will authenticate your identity and provide you the option to share certain personal information with us such as your name and email address to pre-populate our sign-up form. Services like Facebook Connect give you the option to post information about your activities on this website to your profile page to share with others within your network.</p>
                        
                        <p>4. Where you use Facebook Connect to log in to our site you consent to JobYoDA.com displaying your name, employer, job title and Facebook profile picture to your Facebook friends who have also logged in to JobYoDA.com using Facebook Connect. You can unlink your Facebook and JobYoDA.com accounts at any time.</p>
                        
                        <p>5. Our website includes Social Media Features, such as the Facebook button and widgets, or interactive mini-programs that run on our site. These features may collect your IP address, which page you are visiting on our site, and may set cookies to enable the Feature to function properly. Social Media Features and widgets are either hosted by third party or hosted directly on our site. Your interactions with these Features are governed by the privacy policy of the company providing it.</p>
                    </div>

                <h2>11. YOUR CONSENT</h2>

                    <div class="PrivacyBox"> 
                        <p>1. In using the JobYoDA.com Services and/or the JobYoDA.com Websites, you consent to the collections and use of the personal information by JobYoDA.com in the ways described hereinabove (which may change from time to time) unless and until you inform JobYoDA.com to the contrary (need to provide a link here).</p>
                        
                        <p>2. Further, you consent to your referees, the school/college/university/institute where you have studied, the professional qualifying bodies where you received your accreditation and Employers to disclose your personal information to JobYoDA.com.</p>
                    </div>

                <h2>12. CONTACT INFORMATION</h2>

                    <div class="PrivacyBox"> 
                        <p>1. If you still have questions about this Privacy Notice, please visit (link for Help/ Support) or (link for FAQ’s) on the JobYoDA.com Websites. JobYoDA.com requests your name, phone number, email address and comments so that JobYoDA.com may respond to your concerns effectively and works to improve the JobYoDA.com Services and/or JobYoDA.com Websites.</p>
        
                        <p>2. You may also contact JobYoDA.com’s Data Privacy Officer through the following:</p>

                        <p><b>Email Address:</b> dataprivacyofficer@JobYoDA.com</p>
                        <p><b>Telephone Number:</b> +639178721630
                        <p><b>JobYoDA.Inc. </b>
                        Level 10-1 Fort Legend Tower, <br>
                        3rd Avenue Bonifacio Global City, <br>
                        31st street Fort Bonifacio Taguig City, <br>
                        Fourth District, <br>
                        NCR Philippines 1634.</p>
                    </div> -->

                



          
        </div>
                    
                </div>

               
	</div> 


<?php include_once('footer.php'); ?>