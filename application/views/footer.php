 <style>
    .pac-container {
            z-index: 10000 !important;
        }
 </style>
  <div class="clear"></div>    
  
      <footer class="homefooter" id="fooapp">
         <div class="darkbgafootdiv">
            <div class="container">
               <div class="row">
                  <div class="col-md-4 col-lg-4">
                     <div class="widgetsfoot">
                        <img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoDa.png" style="width:179px;">
                        <p> <span><i class="fas fa-map-marker-alt"></i></span> Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City 31st Street Fort Bonifacio TAGUIG CITY, FOURTH DISTRICT, NCR, Philippines, 1634</p>
                        <p><span><i class="fas fa-envelope"></i></span><a href="mailto:info@jobhunt.com">help@jobyoda.com</a></p>
                        <!-- <h6><a href="tel:+63 9178721630">+63 9178721630</a></h6> -->
                     </div>
                  </div>
                  <div class="col-md-4 col-lg-4">
                     <div class="widgetsfoot informar" style="width: 100%">
                        <h5>Useful Links</h5>
                        <div class="halfdividedft" style="width: 100%; margin: 0">
                           <ul>
                              <li><a href="<?php echo base_url(); ?>privacy_policy">Privacy Policy </a></li>
                              <li><a href="<?php echo base_url(); ?>terms">Terms of Service </a></li>
                              <li><a href="<?php echo base_url(); ?>contact">Contact Us </a></li>
                              <li><a href="<?php echo base_url(); ?>about">About Us </a></li> 
                              <li><a href="<?php echo base_url(); ?>how_it_works">How it Works </a></li>
                           </ul>
                        </div>
                        
                     </div>
                  </div>
                  <div class="col-md-4 col-lg-4">
                     <div class="widgetsfoot informar">
                        <h5>Get It On</h5>
                        
                        <div class="halfdividedft morearea">
                           <a target="_blank" href="https://apps.apple.com/us/app/jobyoda/id1471619860?ls=1" class="footerapp-btn">
                              <img src="<?php echo base_url().'webfiles/';?>img/home/apple.png"> 
                              <p>
                                <small>Download On</small>
                                <br>
                                App Store
                            </p>
                           </a>
                           <a target="_blank" href="https://play.google.com/store/apps/details?id=com.jobyodamo" class="footerapp-btn">
                              <img src="<?php echo base_url().'webfiles/';?>img/home/appstore.png"> 
                              <p>
                                <small>Get It On</small>
                                <br>
                                Google Play
                            </p>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
          
          <div class="Copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>Copyright JobYoDA © 2020. All Rights Reserved</p>
                        </div>
                        <div class="col-sm-6">
                            <ul>
                                <li>
                                  <a href="https://www.facebook.com/jobyodapage/" target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                  </a>
                                </li>
                             
                              <li>
                                <a href="https://instagram.com/jobyoda_ig?igshid=1u7xaxp20etlb" target="_blank">
                                  <i class="fab fa-instagram"></i>
                                </a>
                              </li> 
                              <!-- <li>
                                <a href="#">
                                  <i class="fab fa-google"></i>
                                </a>
                              </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

      </footer>

 
      <!-- <div class="copyrightfoot">
         <div class="container">
            <p>© 2019 Jobyoda All rights reserved. Design by Creative Layers</p>
            <img src="<?php echo base_url().'webfiles/';?>img/home/arrtp.png">
         </div>
      </div> -->
	   
	

 <script src="<?php echo base_url().'webfiles/';?>js/popper.min.js"></script>
 <script src="<?php echo base_url().'webfiles/';?>js/bootstrap.js"></script> 
 <script src="<?php echo base_url().'webfiles/';?>js/owl.carousel.js"></script> 


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> 
   <!--   <script src="<?php echo base_url().'webfiles/';?>js/bootstrap-timepicker.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script> <script src='https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js'></script>

<script src="<?php echo base_url().'webfiles/';?>js/adminyoda.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script> -->
 <div class="modal fade" id="statusModalCenter36" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered howshireds jbsrchstatus" role="document">
        <div class="modal-content">
           <div class="modal-header">
		   
		   <div class="salryedits" style="width:100%;">
            <h4>Job Search Status</h4>						 
          </div>

              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
           </div>
            <span id="errorstatus"></span>
           <div class="modal-body">
              <div class="formmidaress modpassfull">
                 <div class="filldetails">
                 <form method="post" action="<?php echo base_url(); ?>Dashboard/jobsearchstatus">
      					   <div class="ratesuld">
      							<ul>
      								<li>
      								<div class="custom-control custom-radio">
                        <input type="radio" name="searchstatus" value="1" <?php if($checkStatus[0]['jobsearch_status']=='1'){ echo "checked"; } ?> class="custom-control-input" id="customCheck45">
                        <label class="custom-control-label" for="customCheck45">Actively Seeking</label>
                      </div>
      								</li>
      								<li>
      									<div class="custom-control custom-radio">
                        <input type="radio" name="searchstatus" value="2" <?php if($checkStatus[0]['jobsearch_status']=='2'){ echo "checked"; } ?> class="custom-control-input" id="customCheck46">
                        <label class="custom-control-label" for="customCheck46">Open to Offers</label>
                      </div>
      								</li>
      								<li>
      								<div class="custom-control custom-radio">
                        <input type="radio" name="searchstatus" value="3" <?php if($checkStatus[0]['jobsearch_status']=='3'){ echo "checked"; } ?> class="custom-control-input" id="customCheck47">
                        <label class="custom-control-label" for="customCheck47">Not open</label>
                      </div>
      				
      								</li>
      							</ul>
      					   </div>
                       
                   <button type="submit" id="changestatusbtn" class="srchbtns">Done</button>  
					       </form>
					  
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>

  <div class="modal fade" id="exampleModalCenter8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form method="post" action="<?php echo base_url();?>user/resumeUpload" enctype="multipart/form-data">
                   <div class="addupdatecent">
                     <div class="profileupload">
                        <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                         <input type="file"  id="file" onchange="getFilename()" name="resumeFile" data-validation="required" >
                        <?php if(!empty($checkResume[0]['resume'])) {?>
                        <p>Would you like to update your Resume?</p>
                        <?php } else { ?>
                        <p>Please Add Your Resume</p>
                        <?php } ?>
                        <div class="statsusdd">
                           <p class="norm" class="close" data-dismiss="modal" aria-label="Close">No</p>
                          <button type="button" class="updt uploadboxclasss">Yes</button>
                          <button type="submit" class="updt" id="resumebtn" style="display: none;">Update</button>
                        </div>
                        <p id="setimgres"></p>
                     </div>
                    </div>
                      <input type="hidden" value="<?php echo current_url(); ?>" name="currentURL"> 
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form>
                     <div class="addupdatecent">
                        <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                        <p>Resume Updated Successfully</p>
                        <div class="statsusdd">
                           <p class="updt" data-dismiss="modal" aria-label="Close">Ok</p>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="exampleModalCenter4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">User Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">

                     <p id="errorfield" class="text-danger text-center text-center1">
                     </p> 
                     <p id="errorfield1" class="text-success text-center errorfield1"></p>
                  
                  <form method="post">
                     <?php if(isset($signupsuccess['success'])){echo "<p class='text-center text-success'>".$signupsuccess['success']."</p>"; } ?>
                  
                     <div class="forminputspswd">
                        <input type="text" name="email" id="loginemail" class="form-control" value="<?php if(!empty($_COOKIE['emaill'])){echo $_COOKIE['emaill'];}?>" placeholder="Email" required="required">
                        <img src="<?php echo base_url().'webfiles/';?>img/msgping.png">
                     </div>
                  
                     <div class="forminputspswd">
                        <input type="password" name="password" id="loginpass" value="<?php if(!empty($_COOKIE['passwordd'])){echo $_COOKIE['passwordd'];}?>" class="form-control" placeholder="Password" required="required">
                        <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                     </div>
                  
                     <div class="forminputspswd">
                        <label class="form-check-label" for="check1">
                           <input type="checkbox" class="form-check-input" id="check1" name="option1" value="1" <?php if(!empty($_COOKIE['checkval'])){echo 'checked';}?>> 
                           <p class="shiftup">Remember Me</p>
                        </label>
                        <a href="#" class="forgetxt" data-toggle="modal" data-target="#exampleModalCenter18" data-dismiss="modal">Forgot Password ?</a>
                     </div>
                  
                     <div class="forminputspswd loginspace">
                        <!--<button type="submit" class="srchbtns">Login</button>-->
                        <button type="button" class="srchbtns" id="loginjob" onclick="login()">Login</button>
                     </div>
                  
                     <div class="forminputspswd socialsignar">
                        <p><a href="javascript:void(0)" onclick="showmodel()">Please click here for Sign Up!</a></p>
                        <p class="nxtopnt">or</p>
                        <ul>
                           <li><a href="<?php if(!empty($authUrl)) echo $authUrl; ?>"><img src="<?php echo base_url().'webfiles/';?>img/facetag.png"></a></li>
                           <li><a href="<?php if(!empty($loginURL)) echo $loginURL;?>"><img src="<?php echo base_url().'webfiles/';?>img/googtag.png"></a></li>
                        </ul>
                  
                     </div>
                  
                  </form>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php

  //if($this->session->userdata('usersess')) { } else {
?>

<div class="modal fade" id="exampleModalCenter5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered signopopd" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Sign Up</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <form method="post" action="<?php echo base_url();?>user/signup" id="signupform">
                     <?php if(isset($signuperrors['datanot'])){echo "<p class='text-center text-danger'>".$signuperrors['datanot']."</p>"; } ?>
                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" maxlength="20" value="<?php if(!empty($userData['fname'])){ echo $userData['fname']; }?>">
                        <img src="<?php echo base_url().'webfiles/';?>img/usertag.png">
                     </div>
                     <?php if(isset($signuperrors['fname'])){echo "<p class='text-center text-danger'>".$signuperrors['fname']."</p>"; } ?>
                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" maxlength="20" value="<?php if(!empty($userData['lname'])){ echo $userData['lname']; }?>">
                        <img src="<?php echo base_url().'webfiles/';?>img/usertag.png">
                     </div>
                     <?php if(isset($signuperrors['lname'])){echo "<p class='text-center text-danger'>".$signuperrors['lname']."</p>"; } ?>
                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php if(!empty($userData['email'])){ echo $userData['email']; }?>" maxlength="160" >
                        <img src="<?php echo base_url().'webfiles/';?>img/msgping.png">
                        <?php if(isset($signuperrors['email'])){echo "<p class='text-center text-danger'>".$signuperrors['email']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="refer_code" id="refer_code" placeholder="Promocode" value="<?php if(!empty($userData['refer_code'])){ echo $userData['refer_code']; }?>" maxlength="60" onkeyup="referralCode()" >
                        <img src="<?php echo base_url().'webfiles/';?>img/referral.png">
                        <p class='text-center text-danger' id='refer_err'></p>
                     </div>
                     <div class="forminputspswd">
                     <div class="form-group">
                        <select name="country_code" placeholder="Code" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                            <option value="">Select Country Code</option>
                            <?php
                              foreach($phonecodes as $phonecode) {
                            ?>
                                <option <?php if(!empty($userData['country_code'])){ if($userData['country_code']=='+'.$phonecode['phonecode']) { echo "selected"; } } ?>  value="+<?php echo $phonecode['phonecode'];?>" <?php if($phonecode["phonecode"]=='63'){ echo "selected";} ?>> <?php echo $phonecode['name'];?> - <?php echo $phonecode['phonecode'];?> </option>
                            <?php
                              }
                            ?>
                        </select>
                     </div>
                     <?php if(isset($signuperrors['country_code'])){echo "<p class='text-center text-danger'>".$signuperrors['country_code']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <input type="number" class="form-control" name="phone" placeholder="Phone Number" value="<?php if(!empty($userData['phone'])){ echo $userData['phone']; }?>" id="phone" maxlength="12" minlength="4">
                        <img src="<?php echo base_url().'webfiles/';?>img/phonetag.png">
                        <?php if(isset($signuperrors['phone'])){echo "<p class='text-center text-danger'>".$signuperrors['phone']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <select name="exp_year" placeholder="Years of Experience" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                            <option value="">Years of Exp.</option>
                            <?php for($i=0;$i<=50;$i++) {?>
                              <option value="<?php echo $i;?>"
                              
                            ><?php echo $i.' '.'years';?>
                           </option>
                            <?php }?>
                        </select>
                        <?php if(isset($signuperrors['exp_year'])){echo "<p class='text-center text-danger'>".$signuperrors['exp_year']."</p>"; } ?>
                     </div>

                     <div class="forminputspswd">
                        <select name="exp_month" placeholder="Months of Experience" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                            <option value="">Months of Exp.</option>
                           <?php for($i=0;$i<=12;$i++) {?>
                              <option value="<?php echo $i;?>"
                               
                              ><?php echo $i.' '.'months';?></option>
                           <?php }?>
                        </select>
                        <?php if(isset($signuperrors['exp_month'])){echo "<p class='text-center text-danger'>".$signuperrors['exp_month']."</p>"; } ?>
                     </div>
                     <div class="forminputspswd">
                        <select name="education" id="education" class="form-control" placeholder="Attainment" style="width:100%">
                           <option value="">Select Highest Level of Education</option>   
                            <option value="High School Graduate" >High School Graduate</option>
                            <option value="Vocational" >Vocational</option>
                            <option value="Undergraduate" >Undergraduate</option>
                            <option value="Associate Degree" >Associate Degree</option>
                            <option value="College Graduate" >College Graduate</option>
                            <option value="Post Graduate" >Post Graduate</option>
                        </select>

                        <?php if(isset($signuperrors['education'])){echo "<p class='text-center text-danger'>".$signuperrors['education']."</p>"; } ?>
                     </div>
                     

                     <div class="forminputspswd">
                        <input type="text" class="form-control" name="location" id="location" placeholder="Location" value="<?php if(!empty($userData['location'])){ echo $userData['location']; }?>">
                        <img src="<?php echo base_url().'webfiles/';?>img/location.png" style="width: 23px;">
                        <?php if(isset($signuperrors['location'])){echo "<p class='text-center text-danger'>".$signuperrors['location']."</p>"; } ?>
                     </div>

                     
                     <div class="forminputspswd">
                        <select name="state" placeholder="State" class="changestate" style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                            <option value="">Select State</option>
                            <?php
                              foreach($states as $getstates) {
                            ?>
                                <option <?php if(!empty($userData['state'])){ if($userData['state']==$getstates['state']) { echo "selected"; } } ?>  value="<?php echo $getstates['state'];?>" > <?php echo $getstates['state'];?> </option>
                            <?php
                              }
                            ?>
                        </select>
                        <?php if(isset($signuperrors['state'])){echo "<p class='text-center text-danger'>".$signuperrors['state']."</p>"; } ?>
                     </div>


                     <div class="forminputspswd">
                        <select name="city" placeholder="City/Town" class="changecity" style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                            <option value="">Select City</option>
                        </select>
                        <?php if(isset($signuperrors['city'])){echo "<p class='text-center text-danger'>".$signuperrors['city']."</p>"; } ?>
                     </div>

                     
                     <div class="forminputspswd" style="width:100%">
                        <select class="multiselect-ui" id="framework" name="intrested[]" multiple style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                            <?php
                              foreach($intrestedin as $getintrested) {
                                if($getintrested['subcategory'] == "ALL") {} else {
                            ?>
                                <option <?php if(!empty($userData['intrested'])){ if($userData['intrested']==$getintrested['subcategory']) { echo "selected"; } } ?>  value="<?php echo $getintrested['subcategory'];?>" > <?php echo $getintrested['subcategory'];?> </option>
                            <?php
                                }
                              }
                            ?>
                        </select>
                        <span class="interestmsg text-center text-warning"> </span>
                        <?php if(isset($signuperrors['intrested'])){echo "<p class='text-center text-danger'>".$signuperrors['intrested']."</p>"; } ?>
                     </div>


                     <div class="forminputspswd">
                        <select name="nationality" placeholder="Nationality" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                            <option value="">Select Nationality</option>
                            <?php
                              foreach($nations as $nation) {
                            ?>
                                <option <?php if(!empty($userData['nationality'])){ if($userData['nationality']==$nation['nationality']) { echo "selected"; } } ?>  value="<?php echo $nation['nationality'];?>" > <?php echo $nation['nationality'];?> </option>
                            <?php
                              }
                            ?>
                        </select>
                        <?php if(isset($signuperrors['nationality'])){echo "<p class='text-center text-danger'>".$signuperrors['nationality']."</p>"; } ?>
                     </div>

                     <div class="forminputspswd">
                        <select name="superpower" placeholder="Nationality" class="" style="margin-left:0px;     width: 100%; margin-bottom: 10px;">
                            <option value="">Select Superpower</option>
                            <?php
                              foreach($powers as $power) {
                            ?>
                                <option <?php if(!empty($userData['superpower'])){ if($userData['superpower']==$power['power']) { echo "selected"; } } ?>  value="<?php echo $power['power'];?>" > <?php echo $power['power'];?> </option>
                            <?php
                              }
                            ?>
                        </select>
                        <?php if(isset($signuperrors['superpower'])){echo "<p class='text-center text-danger'>".$signuperrors['superpower']."</p>"; } ?>
                     </div>

                     <?php
                     if(empty($type)) {
                     ?>

                     <div class="forminputspswd">
                        <input type="password" class="form-control" name="pass" id="pass" placeholder="Password" value="<?php if(!empty($userData['pass'])){ echo $userData['pass']; }?>">
                        <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                     </div>
                     
                     <div class="forminputspswd">
                        <input type="password" class="form-control" name="cpass" placeholder="Confirm Password" value="<?php if(!empty($userData['cpass'])){ echo $userData['cpass']; }?>">
                        <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                     </div>
                     <?php if(isset($signuperrors['pass'])){echo "<p class='text-center text-danger'>".$signuperrors['pass']."</p>"; } ?>
                     <?php if(isset($signuperrors['cpass'])){echo "<p class='text-center text-danger'>".$signuperrors['cpass']."</p>"; } ?>
                     
                     <?php
                        }
                     ?>

                     <div class="forminputspswd fullma" >
                        <label class="form-check-label accptance" for="check2">
                        <input type="checkbox" class="form-check-input" id="check2" name="option1" value="something"> I accept all <a href="<?php echo base_url(); ?>terms" target="_blank">Terms & Conditions</a> and <a href="<?php echo base_url(); ?>privacy_policy" target="_blank">Privacy Policy.</a>
                        </label>
                     </div>

                     <?php if(isset($signuperrors['option1'])){echo "<p class='text-center text-danger'>".$signuperrors['option1']."</p>"; } ?>

                      <div class="" style="float:left; width:100%;">
                         <div class="forminputspswd loginspace" style="margin: 0 auto; float: none;">
                            <input type="hidden" id="signupType" name="signupType" value="<?php if(empty($type)){ echo "normal"; }else{ echo $type; } ?>">
                            <button type="submit" class="srchbtns1 srchbtns" name="signupjob" id="signupjob">Sign Up</button>
                         </div>
                      </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<?php
  //}
?>

<div class="modal fade" id="exampleModalCenter18" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                 
                  <div class="inform">
                     <p class="greentxt">Forgot Password</p>
                     <p>Enter your registered Email Id</p>
                     <div>
                        <div class="forminputspswd">
                           <script>        </script>
                           <input type="text" class="form-control" placeholder="Email Address" name="user_email" id="user_email">
                           <img src="<?php echo base_url().'webfiles/';?>img/msgping.png">
                        </div>
             <div>
                     <p id="errorfieldd" class="text-danger text-center"></p>
                  </div>
          
                        <div class="forminputspswd loginspace">
                           <!--<button type="submit" class="srchbtns">Login</button>-->
                           <button type="button" class="srchbtns" id="forgetpass">Reset Password</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="confrmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div class="inform">
                     <p class="">Forgot Password</p>
                     <p id="confrmMsg" class="text-success text-center"></p>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter4444" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div>
                     <p id="errorfielddddd" class="text-success text-center">you are successfully verified please login!</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="exampleModalCenter44" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Verification Code</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div>
                     <p id="errorfielddd" class="text-danger text-center"></p>
                  </div>
                  <form method="post" action="<?php echo base_url();?>user/login">
                     <?php if(isset($signupsuccesss['success'])){echo "<p class='text-center text-success'>".$signupsuccesss['success']."</p>"; } ?>
                     <div class="forminputspswd">
                        <input type="text" name="mobile_number" id="mobile_number" class="form-control" value="<?php echo $this->session->userdata('phone') ?>" placeholder="mobile number" required="required" readonly>
                        <img src="<?php echo base_url().'webfiles/';?>img/phonetag.png">
                     </div>
                     <input type="hidden" id="hiddenotp" value="<?php  echo $this->session->userdata('verify_code') ?>">
                     <div class="forminputspswd">
                        <input type="text" name="otp" id="otp" value="" class="form-control" placeholder="Verification code" required="required">
                        <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                     </div>
                     <div class="forminputspswd loginspace">
                        <button type="button" class="srchbtns" id="checkotp" onclick="verifyotp()">OK</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                  
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <span id="errorpassword" class="text-danger text-center errorfield1"></span>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                 <div class="filldetails">
                   <!-- <form method="post" action="<?php echo base_url('user/changepass') ?>">-->
                       <!--<div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Enter Old Password" name="oldpass" id="oldpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>-->
                       <div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Enter Old Password" name="oldpass" id="oldpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>
                       <div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Enter New Password" name="newpass" id="newpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>
                       <div class="forminputspswd">
                          <input type="password" class="form-control" placeholder="Confirm New Password" name="confpass" id="confpass">
                          <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                       </div>
                       <button type="button" id="changepassbtn" class="srchbtns">Change</button>
                   <!-- </form>-->
                 </div>
              </div>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="socialModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <!-- <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5> -->
                  <?php $userSess = $this->session->userdata('usersess');
                  $type = $usersess['type'];
                  ?>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <!-- <span id="errorpassword" class="text-danger text-center errorfield1"></span> -->
               <div class="modal-body">
                <div class="baseyoda">
                 <div class="thmb"> <img src="<?php echo base_url(); ?>webfiles/img/yellos.png"></div>
                  <p>You are not allowed to change password because you logged in via <?php echo $type; ?></p>
                </div>
               </div>
            </div>
         </div>
      </div>

<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
  <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
     </div>
     <div class="modal-body">
        <div class="formmidaress modpassfull">
           <div class="filldetails">
              <form>
                 <div class="addupdatecent">
                    <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png">
                    <p><?php if(!empty($this->session->tempdata('updatemsg'))){ echo $this->session->tempdata('updatemsg'); }?></p>
                    <div class="statsusdd">
                       <!-- <p class="updt" data-dismiss="modal" aria-label="Close">Ok</p> -->
                    </div>
                 </div>
              </form>
           </div>
        </div>
     </div>
  </div>
</div>
</div>

<style type="text/css">
  .btn-group{width:100%!important;}
  .multiselect.dropdown-toggle{background-color: #d4cfcf!important;height: 40px!important;width:100%!important;}
  .filter-option{font-size: 13px!important;font-weight: 100!important;}
</style>

<script type="text/javascript">
  $('.uploadboxclasss').click(function() { 
      $('#file').trigger('click');
      $('.uploadboxclasss').css('display','none');
      $('#resumebtn').css('display','block');

  });
</script>

<?php if($this->session->tempdata('updatemsg') != null) {
             if($this->session->tempdata('updatemsg')){$this->session->unset_tempdata('updatemsg');}?>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#profileModal').modal('show');
    });
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
        //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
    }
</script>
<?php } ?>


<script>
    $(document).ready(function(){   
    $("#changepassbtn").click(function() {
      var oldpass = $("#oldpass").val();
      var newpass = $("#newpass").val();
      var confpass = $("#confpass").val();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/changepass",
          data: {oldpass:oldpass, newpass:newpass,confpass:confpass},
          cache:false,
          success:function(htmldata){
          //alert(htmldata);
             // console.log(data);
             // var logdone = "logindone";
             // var n = data.includes(logdone);
              //if(n) {
             //   window.location = "<?php echo base_url(); ?>dashboard";
             // } else {
               $('#errorpassword').html(htmldata);
              //}
          },
          error:function(){
            console.log('error');
          }
      });
  });
});
</script>

<script>
  function showmodel() {
      $("#exampleModalCenter4").modal('hide');
      $("#exampleModalCenter5").modal('show');
      $('body').addClass('modal-opennn');
  }
</script>


<script type="text/javascript">
  function referralCode(){
    var refer_code = $('#refer_code').val();
    if(refer_code!=''){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>"+ "user/checkreferral",
        data: {refer_code:refer_code},
        success:function(data){
          if(data==1){
            $('#refer_err').html("This Referral code does not exist");
          }else{
            $('#refer_err').html('');
          }
        }
      })
    }
  }
</script>

<script>
  function verifyotp() {
        var mobile_number = $("#mobile_number").val();
        var otp = $("#otp").val();
        var hiddenotp = $("#hiddenotp").val();
        if (otp != hiddenotp) {
            $("#errorfielddd").html('please enter correct otp');
            return false;
        }

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "user/verify_User",
            data: {
                mobile_number: mobile_number
            },
            cache: false,
            success: function(htmldata) {
               //console.log(htmldata);

               var htmldata = JSON.parse(htmldata);
                if (htmldata.success == 1) {
                  window.location.href = "https://jobyoda.com/thank_you";
                    // $("#exampleModalCenter44").modal('hide');
                    // $("#exampleModalCenter4").modal('show');
                    // $('#errorfield1').html('Successfully Registered. Please login');
                }
            },
        });
    } 
</script>

<script>
   function login() { 
        var email = $("#loginemail").val();
         var pass = $("#loginpass").val();
         var checkval=  $('#check1:checked').val();
         var cur_lat = $('#cur_lat').val();
         var cur_long = $('#cur_long').val();

         var currentURL = window.location.href;

         $.ajax({
             type: "POST",
             url: "<?php echo base_url(); ?>" + "user/login",
             data: {email:email,password:pass,rememberval:checkval,cur_lat:cur_lat,cur_long:cur_long },
             cache:false,
             success:function(data) {
                 var logdone = "logindone";
                 var n = data.includes(logdone);
                 if(n) {
                   window.location = currentURL;
                 } else {
                   $('.modpassfull .filldetails p.text-center1').text(data);
                 }
             },
             error:function(){
               console.log('error');
             }
         });
   }  
</script>

<script type="text/javascript">
  $(document).ready(function(){
         $('#signupform').validate({
              rules: {
                fname: {required:true,minlength:1,maxlength:30},
                lname: {required:true,minlength:1,maxlength:30},
                email: {
                  required: true,
                  email: true,
                  maxlength:100
                },
                phone:{
                  required: true,
                  number: true,
                  //minlength:8,
                  //maxlength:10
                },
                exp_year:{
                  required:true,
                },
                exp_month:{
                  required:true,
                },
                education:{
                  required:true,
                },
                location:{
                  required:true,
                },
                nationality:{
                  required:true,
                },
                superpower:{
                  required:true,
                },
                pass: {
                  required: true,
                  minlength: 5,
                },
                cpass: {
                  required: true,
                  //minlength: 5,
                  equalTo: "#pass"
                },
                option1:{
                  required:true,
                },
              },
              messages: {
                fname: {required:'The first name field is required'},
                lname: {required:'The last name field is required'},
                email: {required:'The email filed is required',remote:"This Email id is already registered.", maxlength:"The email must not contain more than 100 characters"},
                phone: {
                     required:"The phone field is required",
                    // maxlength:"Please enter no more than {0} number.",
                     //minlength:"Please enter at least {0} number."
                },
                exp_year: {
                  required:"Years of Exp is required",
                },
                exp_month: {
                  required:"Months of Exp is required",
                },
                education: {
                  required:"The education field is required",
                },
                location: {
                  required:"The location field is required",
                },
                nationality: {
                  required:"The nationality field is required",
                },
                superpower: {
                  required:"The superpower field is required",
                },
                pass: {
                  required: 'The password field is required',
                  minlength: 'Password must be at least 5 characters long'
                },
                cpass: {
                  required: "The confirm password field is required",
                 // minlength: "Your password must be at least 5 characters long",
                  equalTo: "Please enter the same password as above"
                },
                option1: {
                  required:"The terms and condition field is required",
                },
              }
         });
   });
</script>

<script>
   $(document).ready(function(){   
    $("#forgetpass").click(function() {
      var user_email = $("#user_email").val();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/forgetpassword",
          data: {email:user_email},
          cache:false,
          success:function(htmldata){
            //alert(htmldata);
            htmldata = htmldata.trim();
            if(htmldata=='We have sent you a link and verification code to reset your password.'){
              $('#exampleModalCenter18').modal('hide');
              $('#confrmModal').modal('show');
              $('#confrmMsg').html(htmldata);
              setTimeout(function(){
                  $('#confrmModal').modal('hide');
                  $('#exampleModalCenter4').modal('show');
              }, 5000);

            }else{
              $('#errorfieldd').html(htmldata);
              
            }
              
          },
          error:function(){
            console.log('error');
          }
            
      });      
   });
   });

   $(document).ready(function(){   
    $(".changestate").on('change', function() {
      var userstate = $(this).val();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/getownstate",
          data: {state:userstate},
          cache:false,
          success:function(htmldata) {
              $('.changecity').html(htmldata);  
          },
          error:function(){
            console.log('error');
          }
      });      
   });
   });

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />

<script> 
$("#fooapptops").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#fooapp").offset().top
    }, 2000);
});

// $(document).ready(function(){
//    $('#framework').multiselect({
      
//    });
// });
</script> 
<script type="text/javascript">

    $(document).ready(function() {

        $('#framework').multiselect({
            nonSelectedText: "Select Interested In",
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('.multiselect-ui option:selected');
                if (selectedOptions.length >= 3) {
                     $('.interestmsg').html("You can select 3 Job categories only");
                }
                if (selectedOptions.length >= 3) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('.multiselect-ui option').filter(function() {
                        return !$(this).is(':selected');
                    });
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('.multiselect-ui option').each(function() {
                        var input = $('input[type="checkbox"][value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });

        var selectedOptions = $('.multiselect-ui option:selected');

                if (selectedOptions.length >= 3) {
                    // // Disable all other checkboxes.
                    var nonSelectedOptions = $('.multiselect-ui option').filter(function() {
                        return !$(this).is(':selected');
                    });

                    nonSelectedOptions.each(function() {
                        var inputValue = $(this).val();
                        var input = $('.checkbox input[value="'+ trim(inputValue) +'"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                function trim(str) {
                  return str.replace(/^\s+|\s+$/g,"");
                }
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script>
 <script type="text/javascript">
   google.maps.event.addDomListener(window, 'load', function () {
      var places1 = new google.maps.places.Autocomplete(document.getElementById('location'));
    
       google.maps.event.addListener(places1, 'place_changed', function () {
         
           var place = places1.getPlace();
           var address = place1.formatted_address;
           var latitude = place1.geometry.location.A;
           var longitude = place1.geometry.location.F;
           var mesg = "Address: " + address;
           mesg += "\nLatitude: " + latitude;
           mesg += "\nLongitude: " + longitude;
       });
   });
</script>

<?php 
if($this->session->userdata('userfsess') != null) {
       $userfsess = $this->session->userdata('userfsess'); 
       //print_r($userfsess);
       $name = $userfsess['name'];
       $name = explode(" ",$name);
       $fname = $name[0];
       $lname = $name[1];
?>
    <script type="text/javascript">
        var fname = '<?php echo $fname ?>';
        var lname = '<?php echo $lname ?>';
        var email = '<?php echo $userfsess['email']; ?>';
        
        $(window).on('load',function(){
           $('#exampleModalCenter5').modal('show');
           $('#fname').val(fname);
           $('#lname').val(lname);
           $('#email').val(email);
        });
      /*}*/
    </script>
<?php
  }
?>