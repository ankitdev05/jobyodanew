<?php include_once('header2.php'); 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {

    } else {
        $link = "https";
        $link .= "://";
        $link .= $_SERVER['HTTP_HOST'];
        $link .= $_SERVER['REQUEST_URI'];
        redirect($link);
    }
?>


	<section>
        <div class="BannerArea" style="background-image: url('https://jobyoda.com/webfiles/img/newmap.jpg');">
            <h1>About Us  </h1> 
        </div>
    </section>

 
 
    <section>
        <div class="AboutArea">
            <div class="container"> 
                <div class="AboutBox">
                    <p><strong style="color:#00a94f;">OUR MISSION :</strong> To help you find the best BPO jobs near you</p>
                    <p><strong style="color:#00a94f;">Our Promise :</strong> At JobYoDA, we believe there is a better way to find the best BPO jobs in the Philippines.</p>

                    <p>We want to empower the BPO jobseeker by letting them search jobs by benefits that matter the most to them.</p>

                    <p>Search for jobs with Day1 HMO, jobs with Retirement benefits, with Joining Bonus. You get the drift?</p>

                    <p>JobYoDA is the first ever app built specifically to support hiring in the BPO industry which hires close to a million people in the Philippines each year.</p>

                    <p>We're obsessively passionate about helping people find their dream BPO jobs.</p>

                    <p><strong style="color:#00a94f;">Our Motto : </strong> "Nil satis nisi optimum" Nothing but the best is good enough. And we'll always strive to help you find your dream BPO job. </p>

                </div> 
            </div>
        </div>
    </section>

<?php include_once('footer1.php'); ?>