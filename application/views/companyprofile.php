<?php 
  include_once('header.php'); if($this->session->userdata('userfsess')) {
    $userfsess = $this->session->userdata('userfsess');    
  }
?>

<html>
<head>
<title>Admin Dashboard - Applied Jobs </title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
<script src="<?php echo base_url().'webfiles/';?>js/jquery.min.js"></script>
<script src="<?php echo base_url().'webfiles/';?>js/popper.min.js"></script>
<script src="<?php echo base_url().'webfiles/';?>js/bootstrap.js"></script>
<!--   <script src="<?php echo base_url().'webfiles/';?>js/bootstrap-timepicker.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url();?>recruiterfiles/js/jquery.timepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script> -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

<style type="text/css">
.rating-loading{
    width:25px;
    height:25px;
    font-size:0;
    color:#fff;
    background:url(../img/loading.gif) top left no-repeat;border:none
    }

  .rating-loadingg{
    width:25px;
    height:25px;
    font-size:0;
    color:#fff;
    background:url(../img/loading.gif) top left no-repeat;border:none
    }
.rating-container .rating-stars
    {position:relative;
    cursor:pointer;
    vertical-align:middle;
    display:inline-block;
    overflow:hidden;
    white-space:nowrap
         }
         .rating-container .rating-input
         {position:absolute;
         cursor:pointer;
         width:100%;
         height:1px;
         bottom:0;left:0;
         font-size:1px;
         border:none;
         background:0 0;
         padding:0;
         margin:0
         }
         .rating-disabled .rating-input,.rating-disabled .rating-stars
         {
             cursor:not-allowed
             
         }
         .rating-container .star{
             display:inline-block;
             margin:0 3px;
             text-align:center
         }
         .rating-container .empty-stars{
             color:#aaa
         }
         .rating-container .filled-stars
         {
             position:absolute;
             left:0;
             top:0;
             margin:auto;
             color:#fde16d;
             white-space:nowrap;
             overflow:hidden;
             -webkit-text-stroke:1px #777;
             text-shadow:1px 1px #999
         }
             .rating-rtl
             {
                 float:right
                 
             }
             .rating-animate .filled-stars
             {
                 transition:width .25s ease;
                 -o-transition:width .25s ease;
                 -moz-transition:width .25s ease;-webkit-transition:width .25s ease
             }
             .rating-rtl .filled-stars
             {left:auto;right:0;-moz-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);-webkit-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);-o-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);
             transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0)}.rating-rtl.is-star 
             .filled-stars{
                 right:.06em
                 
             }
             .rating-rtl.is-heart .empty-stars{
                 margin-right:.07em
                 
             }
             .rating-lg{
                 font-size:3.91em
                 
             }
             .rating-md{
                 font-size:3.13em
                 
             }
             .rating-sm{
                 font-size:2.5em
                 
             }
             .rating-xs{
                 font-size:2em
                 
             }
             .rating-xl{
             font-size:4.89em
                 
             }
             .rating-container .clear-rating{
                 color:#aaa;
                 cursor:not-allowed;
                 display:inline-block;
                 vertical-align:middle;
                 font-size:60%;
                 padding-right:5px
                 
             }
             .clear-rating-active{
                 cursor:pointer!important
                 
             }
             .clear-rating-active:hover{
                 color:#843534
                 
             }
             .rating-container .caption{
                 color:#999;
                 display:inline-block;
                 vertical-align:middle;
                 font-size:60%;
                 margin-top:-.6em;
                 margin-left:5px;
                 margin-right:0
                 
             }
             .rating-rtl .caption{
                 margin-right:5px;
                 margin-left:0
                 
             }
             @media print{.rating-container .clear-rating{
                 display:none
                 
             }
                 
             }
</style>



<style type="text/css">
.rating-loading{
    width:25px;
    height:25px;
    font-size:0;
    color:#fff;
    background:url(../img/loading.gif) top left no-repeat;border:none
    }
.rating-container .rating-stars
    {position:relative;
    cursor:pointer;
    vertical-align:middle;
    display:inline-block;
    overflow:hidden;
    white-space:nowrap
         }
         .rating-container .rating-input
         {position:absolute;
         cursor:pointer;
         width:100%;
         height:1px;
         bottom:0;left:0;
         font-size:1px;
         border:none;
         background:0 0;
         padding:0;
         margin:0
         }
         .rating-disabled .rating-input,.rating-disabled .rating-stars
         {
             cursor:not-allowed
             
         }
         .rating-container .star{
             display:inline-block;
             margin:0 3px;
             text-align:center
         }
         .rating-container .empty-stars{
             color:#aaa
         }
         .rating-container .filled-stars
         {
             position:absolute;
             left:0;
             top:0;
             margin:auto;
             color:#fde16d;
             white-space:nowrap;
             overflow:hidden;
             -webkit-text-stroke:1px #777;
             text-shadow:1px 1px #999
         }
             .rating-rtl
             {
                 float:right
                 
             }
             .rating-animate .filled-stars
             {
                 transition:width .25s ease;
                 -o-transition:width .25s ease;
                 -moz-transition:width .25s ease;-webkit-transition:width .25s ease
             }
             .rating-rtl .filled-stars
             {left:auto;right:0;-moz-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);-webkit-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);-o-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);
             transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0)}.rating-rtl.is-star 
             .filled-stars{
                 right:.06em
                 
             }
             .rating-rtl.is-heart .empty-stars{
                 margin-right:.07em
                 
             }
             .rating-lg{
                 font-size:3.91em
                 
             }
             .rating-md{
                 font-size:3.13em
                 
             }
             .rating-sm{
                 font-size:2.5em
                 
             }
             .rating-xs{
                 font-size:2em
                 
             }
             .rating-xl{
             font-size:4.89em
                 
             }
             .rating-container .clear-rating{
                 color:#aaa;
                 cursor:not-allowed;
                 display:inline-block;
                 vertical-align:middle;
                 font-size:60%;
                 padding-right:5px
                 
             }
             .clear-rating-active{
                 cursor:pointer!important
                 
             }
             .clear-rating-active:hover{
                 color:#843534
                 
             }
             .rating-container .caption{
                 color:#999;
                 display:inline-block;
                 vertical-align:middle;
                 font-size:60%;
                 margin-top:-.6em;
                 margin-left:5px;
                 margin-right:0
                 
             }
             .rating-rtl .caption{
                 margin-right:5px;
                 margin-left:0
                 
             }
             @media print{.rating-container .clear-rating{
                 display:none
                 
             }
                 
             }
</style> 
</head>
<body>
<!--<header class="mainhead">
	<div class="container-fluid">
		<div class="row">
			<div class="logomain col-md-3">
				<a href="index.html"><img src="img/jobyoda.png"></a>
			</div>
			<div class="menurightopn col-md-9">
			    <div class="rightpan">
					<img src="img/ali.png">
					<h5>Ali Tufan</h5>
					<div class="hammenu">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
-->

<div class="managerpart">
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-md-12 expanddiv jobdeskyod">
			<div class="innerbglay">
			  <div class="mainheadings-yoda">
				<h5>Company Profile</h5>
				
			  </div>
			  
			  <div class="adminopnts companyprofile">

              <div class="row">
              <div class="col-md-6">
                <div class="jobtxtsdesc comprofilepicent" style="border: none;">

                <section id="demos" class="agentssliders propertcomps">
               <div class="owl-carousel owl-theme">
                   <?php if(!empty($companyPic)){
                foreach ($companyPic as $companyPics) {
                
                ?>

                  <div class="item">
                       <div class="innersliders">
                          
                          <img src="<?php echo $companyPics['pic']; ?>">
                         
                          
                       </div>
                    </div>
                    <?php  }}else{?>

                        <div class="item">
                       <div class="innersliders">
                          
                          <img src="<?php echo base_url();?>webfiles/img/user_man.png">
                         
                          
                       </div>
                    </div>
                    <?php }?>
               </div>
            </section>

                  <!-- <?php 
                   if(!empty($company_pic))
                   {
                ?>
                <img src="<?php  echo $company_pic[0]['companyPic']?>">
                <?php
                   }
                 
                  ?> -->
      
                </div>
              </div>

                 <div class="col-md-6">
				 <div class="adminopnts jbdtprt">
                             <p style="font-family: 'Quicksand', sans-serif; margin: 11px 0;"> <?php echo $cname[0]['cname'];?></p>

                        <p class="locationtype"><img src="<?php echo base_url().'webfiles/';?>img/locmap.png"><?php if(!empty($company_pic[0]['address'])) {echo $company_pic[0]['address'];}?></p>

                         <div class="formsrches">
                           <div class="jobtxtsdesc picsd" style="margin-top:30px; border:none;">
                              <h4>Top Picks</h4>
                              
                              <div class="provd usericonds filterchekers">
                                 <ul>
                                  <?php if(!empty($company_toppicks))
                                  {  
                                    foreach($company_toppicks as $company_toppick) {
                                   if($company_toppick['picks_id']=='1')
                                   {
                                  ?>
                                   <li> <div class="icodtds"> <img title="Joining Bonus" src="<?php echo base_url();?>webfiles/img/toppics/annual--performance-bonus.png">
                                   </div>
                                   <p>Joining Bonus</p>
                                   </li>
                                   <?php }
                                     if($company_toppick['picks_id']=='2')
                                    {
                                   ?>
                                   <li>
                                   <div class="icodtds"><img title="Free Food" src="<?php echo base_url();?>webfiles/img/toppics/free-food.png">
                                   </div>
                                   <p>Free Food</p>
                                   </li>
                                 <?php }
                                    if($company_toppick['picks_id']=='3')
                                   {
                                 ?>
                                 <li>
                                    <div class="icodtds"><img title="Day 1 HMO" src="<?php echo base_url();?>webfiles/img/toppics/day-1-hmo.png">
                                    </div>
                                    <p>Day 1 HMO</p>
                                  </li>     
                                    <?php }
                                       if($company_toppick['picks_id']=='4')
                                      {
                                    ?>
                                    <li>
                                     <div class="icodtds"><img title="Day 1 HMO for Dependent" src="<?php echo base_url();?>webfiles/img/toppics/day-1-hmo-for-depended.png">
                                     </div>
                                     <p>Day 1 HMO <br>for Dependent</p>
                                     </li>
                                     
                                    <?php }
                                       if($company_toppick['picks_id']=='5')
                                      {
                                    ?>
                                    <li>
                                     <div class="icodtds"><img title="Day Shift" src="<?php echo base_url();?>webfiles/img/toppics/day-shift.png">
                                     </div>
                                     <p>Day Shift</p>
                                     </li>
                                    <?php }
                                       if($company_toppick['picks_id']=='6')
                                      {
                                    ?>
                                   <li>
                                     <div class="icodtds"><img title="14th Month Pay" src="<?php echo base_url();?>webfiles/img/toppics/14-month-pay.png">
                                     </div>
                                     <p>14th Month Pay</p>
                                    </li>
                                    <?php }}}?>
                                 </ul>
                              </div>
                        
                           </div>
                         
                             
                           </div>
						   </div>
                 </div>
              </div>
			
		
			
			        
                  <?php if(!empty($glassdoor_review)) {
            
            ?>
				 <div class="jobtxtsdesc halfrew">
					<div class="basetih">
						<h4>Glassdoor Rating</h4>
						<!-- <p class="seeall">25 More Reviews</p> -->
						
							<div class="ratingsserevi belowopt" style="margin:0;">
          
          <ul>
            <!-- <li><img src="img/usertick.png" class="usertick"></li>
            <li><img src="img/edits.png" class="usereditr"></li>
            <li><img src="img/userpf.png" class="userpf"></li> -->
          <?php if(!empty($glassdoor_review)) {
            
            ?>
       <span style=""> <input id="input-1" name="rating" class="ratingg rating-loadingg" data-min="0" data-max="5" data-step="0.5" value="2.5" data-validation="required"></span> 
        <?php } else {?>  
           <span style=""> <input id="input-1" name="rating" class="ratingg rating-loadingg" data-min="0" data-max="5" data-step="0.5" value="" data-validation="required"></span>       -
         <?php }?>
          </ul>
          
          </div>
		  
					</div>
				

					<!-- <p class="greentxt">Good Team and Work Culture.</p>
					
					<p>You will get good projects.<br>Salary on time.
					You will get appreciations for your good work.<br>
					Office party & team bonding activities.</p> -->

				</div>
				<?php }?>
				
				<div class="jobtxtsdesc halfrew">
				<div class="basetih">
					<h4>Jobseeker’s Experience with company</h4>
					<p style="text-align:right"><a class="seeall" href="javascript:void(0)" data-toggle="modal" data-target="#mymodel">Show Reviews</a></p>
				</div>


    <div class="modal fade" id="mymodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
         <?php if(!empty($review_list)) {?>
        <h3>Review List</h3>
        <?php }?>
         
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
		 <?php if(!empty($review_list)) {
            $reviewRating= array();
             foreach($review_list as $review_lists){
         ?>
		 <div class="profiledata">
          <div class="imgthumb">
               <img src="<?php echo $review_lists['profilePic']?>" style="width:80px;border-radius: 50%;height:80px"> 
           </div>
          
          <div class="profilecontent">
          <h3><?php echo $review_lists['name'];?></h3>
		    
          <p class="statusday"><?php echo $review_lists['created_at'];?></p>
          <p class="statusday"><?php echo $review_lists['rating'];?></p>
          </div>
          
          <div class="description">
            <p><?php echo $review_lists['feedback'];?></p>
          </div>
          </div>
		  <?php 
    $reviewRating[]=$review_lists['rating'];
       }}
       else {
        echo '<img class="bx_img" src="https://jobyoda.com/webfiles/img/emptybx.png"><p style="text-align:center;">No reviews yet.</p>';
       }
       ?>
            
         </div>
      </div>
   </div>
</div>
<?php 
if(!empty($reviewRating))
{
 $reviewRatingcount=count($reviewRating);
 $reviewRatingsum= array_sum($reviewRating)/$reviewRatingcount;
}

?>


				<div class="ratingsserevi">
					
				
					<div class="ratingsserevi belowopt">
					
					<ul>
						<!-- <li><img src="img/usertick.png" class="usertick"></li>
						<li><img src="img/edits.png" class="usereditr"></li>
						<li><img src="img/userpf.png" class="userpf"></li> -->
						<?php if(!empty($reviewRatingsum)) {?>
						  <span style=""> <input id="input-1" name="rating" class="rating rating-loading" data-min="0" data-max="5" data-step="0.5" value="<?php echo $reviewRatingsum;?>" data-validation="required"></span>
						  <?php } else {?>
						    <span style=""> <input id="input-1" name="rating" class="rating rating-loading" data-min="0" data-max="5" data-step="0.5" value="" data-validation="required"></span>
						  <?php }?>
					</ul>
					
					</div>
				</div>
				</div>


				
				
				<div class="jobtxtsdesc">
					<h4>Company Details</h4>
					<p><?php if(!empty($companyDesc[0]['companyDesc'])) {echo $companyDesc[0]['companyDesc'];}?></p>
                </div>
				
				<div class="tabledivsdsn">
				<div class="row tableshead">
					<div class="col-md-3 col-lg-12 ">
                    <?php if(!empty($jobDetails[0]['id'])) {?>
					<h6>Other jobs</h6>
                    <?php }?>
					</div>
					
				</div>
				
				 
                 
				 <?php if(!empty($jobDetails[0]['id'])) {
			      $x=30;
				  foreach($jobDetails as $jobDetailsData){
                    $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobDetailsData['id']);
                    $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetailsData['company_id']);
                    $timeFrom = $recruiterdetail[0]['from_time'];
                    $timeFromm = date('H:i', strtotime($timeFrom));
                    $timeTo   = $recruiterdetail[0]['to_time'];
                    $timeToo = date('H:i', strtotime($timeTo));
				 ?>
                <div class="featurerowjob">
				<div class="row">
				 
					<div class="col-md-3 col-lg-3 ">
				   
					<div class="imagethumbs">
						<?php 
						 $jobImage = $this->Jobpost_Model->job_image($jobDetailsData['id']);
						 if(!empty($jobImage[0]['pic']))
						 {
						 ?>
					     <a href="<?php echo base_url();?>dashboard/jobDescription?listing=<?php echo base64_encode($jobDetailsData['id']); ?>"">
						 <div class="thumbimpr"><img src="<?php echo $jobImage[0]['pic'];?>"></div>
                          </a>
				         <?php } else {?>
				         	 <a href="<?php echo base_url();?>dashboard/jobDescription?listing=<?php echo base64_encode($jobDetailsData['id']); ?>">
                                <div class="thumbimpr"><img src="<?php echo base_url();?>webfiles/img/user_man.png">
								</div>
                             </a>   
				         <?php }?>
					</div>
					</div>
					<div class="col-md-6 col-lg-7">
					
					<div class="positionarea">
				
					<a href="<?php echo base_url();?>dashboard/jobDescription?listing=<?php echo base64_encode($jobDetailsData['id']); ?>"">
                        <p class="posttypes"><?php echo $jobDetailsData['jobtitle']?></p>
                    </a>    
                    <a href="<?php echo base_url();?>dashboard/companyProfile/<?php echo base64_encode($jobDetailsData['company_id']);?>"><h3><?php echo $jobDetailsData['companyName']; ?></h3></a>
					<p class="locationtype"><?php echo $jobDetailsData['cname'] ?></p>
					<p class="locationtype"><?php echo $jobDetailsData['jobDesc'];?></p>

                        <div class="salicoftr">
               <?php
                        if(!empty($jobDetailsData['toppicks1'])) {
                          if($jobDetailsData['toppicks1']=='1') {
                      ?>
                <div class="salry">
                            <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                            <span>Joining Bonus</span>
               </div>
             
                      <?php       
                          }
                         
                          if($jobDetailsData['toppicks1']=='2') {
                      ?>  <div class="salry">
                            <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                            <span>Free Food</span>
              </div>
                      <?php 
                          } 
                          
                          if($jobDetailsData['toppicks1']=='3') {
                      ?>     <div class="salry">
                            <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                            <span>Day 1 HMO</span>
              </div>
                      <?php 
                          }
                          
                          if($jobDetailsData['toppicks1']=='4') {
                      ?>
               <div class="salry">
                             <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                             <span>Day 1 HMO for Dependent</span>
               </div>
                      <?php 
                          }

                          if($jobDetailsData['toppicks1']=='5') {
                      ?>
             <div class="salry">
                             <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                             <span>Day Shift</span>
             </div>
                      <?php
                          }
                          
                          if($jobDetailsData['toppicks1']=='6') {
                      ?>
                 <div class="salry">
                           <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                           <span>14th Month Pay</span>
               </div>
                      <?php
                          }
                        }

                        if(!empty($jobDetailsData['toppicks2'])) {
                          
                          if($jobDetailsData['toppicks2']=='1') {
                      ?> <div class="salry">
                            <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                            <span>Joining Bonus</span>
             </div>
                      <?php       
                          }
                         
                          if($jobDetailsData['toppicks2']=='2') {
                      ?>
                            <div class="salry">
                              <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                              <span>Free Food</span>
                            </div>
                      <?php 
                          }
                      
                          if($jobDetailsData['toppicks2']=='3') {
                      ?>
                            <div class="salry">
                              <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                              <span>Day 1 HMO</span>
                            </div>
                      <?php 
                          }
                          
                          if($jobDetailsData['toppicks2']=='4') {
                      ?>
                              <div class="salry">
                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                  <span>Day 1 HMO for Dependent</span>
                              </div>
                      <?php 
                          }
                          
                          if($jobDetailsData['toppicks2']=='5') {
                      ?>
                            <div class="salry">
                              <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                              <span>Day Shift</span>
                            </div>
                      <?php
                          }
                          
                          if($jobDetailsData['toppicks2']=='6') {
                      ?>
                          <div class="salry">
                            <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                            <span>14th Month Pay</span>
                          </div>
                      <?php
                          }
                        }

                        if(!empty($jobDetailsData['toppicks3'])) {
                          
                          if($jobDetailsData['toppicks3']=='1') {
                      ?>
                            <div class="salry">
                              <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                              <span>Joining Bonus</span>
                            </div>
                      <?php 
                          }
                         
                          if($jobDetailsData['toppicks3']=='2') {
                      ?>
                            <div class="salry">
                              <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                              <span>Free Food</span>
                            </div>
                      <?php 
                          }
                          
                          if($jobDetailsData['toppicks3']=='3') {
                      ?>
                        
                            <div class="salry">
                              <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                              <span>Day 1 HMO</span>
                            </div>
                      <?php 
                          }
                          
                          if($jobDetailsData['toppicks3']=='4') {
                      ?>
                            <div class="salry">
                              <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                              <span>Day 1 HMO for Dependent</span>
                            </div>
                      <?php 
                          }
                          
                          if($jobDetailsData['toppicks3']=='5') {
                      ?>
                            <div class="salry">
                              <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                              <span>Day Shift</span>
                            </div>
                      <?php
                          }
                          
                          if($jobDetailsData['toppicks3']=='6') {
                      ?>
                            <div class="salry">
                              <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                              <span>14th Month Pay</span>
                            </div>
                      <?php
                          }
                        }
                      ?>                 
                        </div>
					
                       <p class="posttypes drdmn"><i class="fas fa-map-marked-alt"></i> <?php echo $jobDetailsData['distance']; ?>KM</p>
               
					</div>

					</div>
					
					
					<div class="col-md-3 col-lg-2 lefthalf appset">
					<div class="salryedits">
					  <h4> <?php if(!empty($jobDetailsData['salary'])){ echo $jobDetailsData['salary'];?>/Month <?php }else{ echo "Salary confidential"; } ?></h4>  
					    
					</div>

                    <?php 
                    if($jobDetailsData['mode'] == "Instant screening") {
                  ?>
                      <button class="greenbtn" data-toggle="modal" data-target="#exampleModalCenterInstant<?php echo $x;?>">Apply <?php if($jobDetailsData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'as '. $jobDetailsData['mode']; } ?> </button>
                  <?php
                    } else {
                  ?>
                       <button class="greenbtn" data-toggle="modal" data-target="#companyapplymodal<?php echo $x;?>">Apply <?php if($jobDetailsData['mode'] == "Call") { echo "for Phone Screening"; } else { echo 'as '. $jobDetailsData['mode']; } ?> </button>
                  <?php } ?>


					</div>
					
				</div>
                </div>
			
                                    
			<?php 
            $x++;
        }}?>
			
			    </div>
			  </div>
		    </div>
			</div>
		   </div>
	     </div>
       </div>

   <div class="modal fade" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
          <div class="modal-header"> 
        
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        </div>
	  
      <div class="modal-body">
	  <div class="formmidaress modpassfull">
			<div class="filldetails">
				<form>
				<div class="addupdatecent">
					<img src="img/savedbighover.png">
					
					<p>Job Applied Successfully</p>
					<div class="statsusdd">
						
						<p class="updt" data-dismiss="modal" aria-label="Close">Ok</p>
					</div>
				</div>
				</form>
			</div>
		</div>
      </div>
	  
      
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
   <!--   <script src="<?php echo base_url().'webfiles/';?>js/bootstrap-timepicker.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url();?>recruiterfiles/js/jquery.timepicker.min.js"></script>
     
<script type="text/javascript">
    $(function () {
         $(".datetimepicker1").datepicker({ 
             dateFormat: "dd/mm/yy", 
              yearRange: '1900:2020', 
              defaultDate: '',
              autoclose: true,
           })
         });
        $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
          
</script>
   
      
      <?php
         if(isset($_GET["msg"]) && $_GET["msg"] == "scheduled") {
      ?>
<script type="text/javascript">
    $(window).on('load',function(){
      $('#exampleModalCenter10').modal('show');
     });
</script>
      <?php
         }
      ?>
   </body>
<!-- <script type="text/javascript">
 $(".updt").on("click",function(){
  window.location = "<?php echo base_url();?>dashboard/JobListings";
 });
</script> -->
<script>
  function getdayname(id)
     {
      var getdate= $("#datetimepicker1"+id).val();
           // alert(getdate);return false;
      $.ajax({
        'type' :'POST',
        'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
        'data' :'dateValue='+getdate+'&jobId='+id,
        'success':function(htmlres)
         {
          if(htmlres !='')
             {
              if(htmlres == 1)
                {
                    $('#schedull').attr('disabled',false);
                	$("#setval"+id).html("");
                    $("#datetimepicker1"+id).val(getdate);
                }
                else
                {
                   // alert('Please Select Schedule date');
                  
                    $("#setval"+id).html("Please Select Schedule date");
                   $("#datetimepicker1"+id).val(''); 
                    $('#schedull').attr('disabled',true);
                }
             }
          }
               
       });
      }
   
   </script>
 <script>
    function getTime(id)
       {
          var gettime= $("#datepicker1"+id).val();
          $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
             'data' :'timeValue='+gettime+'&jobId='+id,
             'success':function(htmlres)
             {
                if(htmlres !='')
                {
                if(htmlres == 1)
                {
                	$('#schedull').attr('disabled',false);
                	$("#setvall"+id).html("");
                    $("#datepicker1"+id).val(gettime);
                }
                else
                {
                    //alert('Please Select Schedule time');
                   $("#setvall"+id).html("Please Select Schedule time");
                   $("#datepicker1"+id).val(''); 
                    $('#schedull').attr('disabled',true);
                }
             }
             }
               
           });
       }
 </script>

<script type="text/javascript">
   
! function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof module && module.exports ? module.exports = e(require("jquery")) : e(window.jQuery)
}(function(e) {
    "use strict";
    e.fn.ratingLocales = {}, e.fn.ratingThemes = {};
    var t, a;
    t = {
        NAMESPACE: ".rating",
        DEFAULT_MIN: 0,
        DEFAULT_MAX: 5,
        DEFAULT_STEP: .5,
        isEmpty: function(t, a) {
            return null === t || void 0 === t || 0 === t.length || a && "" === e.trim(t)
        },
        getCss: function(e, t) {
            return e ? " " + t : ""
        },
        addCss: function(e, t) {
            e.removeClass(t).addClass(t)
        },
        getDecimalPlaces: function(e) {
            var t = ("" + e).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            return t ? Math.max(0, (t[1] ? t[1].length : 0) - (t[2] ? +t[2] : 0)) : 0
        },
        applyPrecision: function(e, t) {
            return parseFloat(e.toFixed(t))
        },
        handler: function(e, a, n, r, i) {
            var l = i ? a : a.split(" ").join(t.NAMESPACE + " ") + t.NAMESPACE;
            r || e.off(l), e.on(l, n)
        }
    }, a = function(t, a) {
        var n = this;
        n.$element = e(t), n._init(a)
    }, a.prototype = {
        constructor: a,
        _parseAttr: function(e, a) {
            var n, r, i, l, s = this,
                o = s.$element,
                c = o.attr("type");
            if ("range" === c || "number" === c) {
                switch (r = a[e] || o.data(e) || o.attr(e), e) {
                    case "min":
                        i = t.DEFAULT_MIN;
                        break;
                    case "max":
                        i = t.DEFAULT_MAX;
                        break;
                    default:
                        i = t.DEFAULT_STEP
                }
                n = t.isEmpty(r) ? i : r, l = parseFloat(n)
            } else l = parseFloat(a[e]);
            return isNaN(l) ? i : l
        },
        _parseValue: function(e) {
            var t = this,
                a = parseFloat(e);
            return isNaN(a) && (a = t.clearValue), !t.zeroAsNull || 0 !== a && "0" !== a ? a : null
        },
        _setDefault: function(e, a) {
            var n = this;
            t.isEmpty(n[e]) && (n[e] = a)
        },
        _initSlider: function(e) {
            var a = this,
                n = a.$element.val();
            a.initialValue = t.isEmpty(n) ? 0 : n, a._setDefault("min", a._parseAttr("min", e)), a._setDefault("max", a._parseAttr("max", e)), a._setDefault("step", a._parseAttr("step", e)), (isNaN(a.min) || t.isEmpty(a.min)) && (a.min = t.DEFAULT_MIN), (isNaN(a.max) || t.isEmpty(a.max)) && (a.max = t.DEFAULT_MAX), (isNaN(a.step) || t.isEmpty(a.step) || 0 === a.step) && (a.step = t.DEFAULT_STEP), a.diff = a.max - a.min
        },
        _initHighlight: function(e) {
            var t, a = this,
                n = a._getCaption();
            e || (e = a.$element.val()), t = a.getWidthFromValue(e) + "%", a.$filledStars.width(t), a.cache = {
                caption: n,
                width: t,
                val: e
            }
        },
        _getContainerCss: function() {
            var e = this;
            return "rating-container" + t.getCss(e.theme, "theme-" + e.theme) + t.getCss(e.rtl, "rating-rtl") + t.getCss(e.size, "rating-" + e.size) + t.getCss(e.animate, "rating-animate") + t.getCss(e.disabled || e.readonly, "rating-disabled") + t.getCss(e.containerClass, e.containerClass)
        },
        _checkDisabled: function() {
            var e = this,
                t = e.$element,
                a = e.options;
            e.disabled = void 0 === a.disabled ? t.attr("disabled") || !1 : a.disabled, e.readonly = void 0 === a.readonly ? t.attr("readonly") || !1 : a.readonly, e.inactive = e.disabled || e.readonly, t.attr({
                disabled: e.disabled,
                readonly: e.readonly
            })
        },
        _addContent: function(e, t) {
            var a = this,
                n = a.$container,
                r = "clear" === e;
            return a.rtl ? r ? n.append(t) : n.prepend(t) : r ? n.prepend(t) : n.append(t)
        },
        _generateRating: function() {
            var a, n, r, i = this,
                l = i.$element;
            n = i.$container = e(document.createElement("div")).insertBefore(l), t.addCss(n, i._getContainerCss()), i.$rating = a = e(document.createElement("div")).attr("class", "rating-stars").appendTo(n).append(i._getStars("empty")).append(i._getStars("filled")), i.$emptyStars = a.find(".empty-stars"), i.$filledStars = a.find(".filled-stars"), i._renderCaption(), i._renderClear(), i._initHighlight(), n.append(l), i.rtl && (r = Math.max(i.$emptyStars.outerWidth(), i.$filledStars.outerWidth()), i.$emptyStars.width(r)), l.appendTo(a)
        },
        _getCaption: function() {
            var e = this;
            return e.$caption && e.$caption.length ? e.$caption.html() : e.defaultCaption
        },
        _setCaption: function(e) {
            var t = this;
            t.$caption && t.$caption.length && t.$caption.html(e)
        },
        _renderCaption: function() {
            var a, n = this,
                r = n.$element.val(),
                i = n.captionElement ? e(n.captionElement) : "";
            if (n.showCaption) {
                if (a = n.fetchCaption(r), i && i.length) return t.addCss(i, "caption"), i.html(a), void(n.$caption = i);
                n._addContent("caption", '<div class="caption">' + a + "</div>"), n.$caption = n.$container.find(".caption")
            }
        },
        _renderClear: function() {
            var a, n = this,
                r = n.clearElement ? e(n.clearElement) : "";
            if (n.showClear) {
                if (a = n._getClearClass(), r.length) return t.addCss(r, a), r.attr({
                    title: n.clearButtonTitle
                }).html(n.clearButton), void(n.$clear = r);
                n._addContent("clear", '<div class="' + a + '" title="' + n.clearButtonTitle + '">' + n.clearButton + "</div>"), n.$clear = n.$container.find("." + n.clearButtonBaseClass)
            }
        },
        _getClearClass: function() {
            var e = this;
            return e.clearButtonBaseClass + " " + (e.inactive ? "" : e.clearButtonActiveClass)
        },
        _toggleHover: function(e) {
            var t, a, n, r = this;
            e && (r.hoverChangeStars && (t = r.getWidthFromValue(r.clearValue), a = e.val <= r.clearValue ? t + "%" : e.width, r.$filledStars.css("width", a)), r.hoverChangeCaption && (n = e.val <= r.clearValue ? r.fetchCaption(r.clearValue) : e.caption, n && r._setCaption(n + "")))
        },
        _init: function(t) {
            var a, n = this,
                r = n.$element.addClass("rating-input");
            return n.options = t, e.each(t, function(e, t) {
                n[e] = t
            }), (n.rtl || "rtl" === r.attr("dir")) && (n.rtl = !0, r.attr("dir", "rtl")), n.starClicked = !1, n.clearClicked = !1, n._initSlider(t), n._checkDisabled(), n.displayOnly && (n.inactive = !0, n.showClear = !1, n.showCaption = !1), n._generateRating(), n._initEvents(), n._listen(), a = n._parseValue(r.val()), r.val(a), r.removeClass("rating-loading")
        },
        _initEvents: function() {
            var e = this;
            e.events = {
                _getTouchPosition: function(a) {
                    var n = t.isEmpty(a.pageX) ? a.originalEvent.touches[0].pageX : a.pageX;
                    return n - e.$rating.offset().left
                },
                _listenClick: function(e, t) {
                    return e.stopPropagation(), e.preventDefault(), e.handled === !0 ? !1 : (t(e), void(e.handled = !0))
                },
                _noMouseAction: function(t) {
                    return !e.hoverEnabled || e.inactive || t && t.isDefaultPrevented()
                },
                initTouch: function(a) {
                    var n, r, i, l, s, o, c, u, d = e.clearValue || 0,
                        p = "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch;
                    p && !e.inactive && (n = a.originalEvent, r = t.isEmpty(n.touches) ? n.changedTouches : n.touches, i = e.events._getTouchPosition(r[0]), "touchend" === a.type ? (e._setStars(i), u = [e.$element.val(), e._getCaption()], e.$element.trigger("change").trigger("rating.change", u), e.starClicked = !0) : (l = e.calculate(i), s = l.val <= d ? e.fetchCaption(d) : l.caption, o = e.getWidthFromValue(d), c = l.val <= d ? o + "%" : l.width, e._setCaption(s), e.$filledStars.css("width", c)))
                },
                starClick: function(t) {
                    var a, n;
                    e.events._listenClick(t, function(t) {
                        return e.inactive ? !1 : (a = e.events._getTouchPosition(t), e._setStars(a), n = [e.$element.val(), e._getCaption()], e.$element.trigger("change").trigger("rating.change", n), void(e.starClicked = !0))
                    })
                },
                clearClick: function(t) {
                    e.events._listenClick(t, function() {
                        e.inactive || (e.clear(), e.clearClicked = !0)
                    })
                },
                starMouseMove: function(t) {
                    var a, n;
                    e.events._noMouseAction(t) || (e.starClicked = !1, a = e.events._getTouchPosition(t), n = e.calculate(a), e._toggleHover(n), e.$element.trigger("rating.hover", [n.val, n.caption, "stars"]))
                },
                starMouseLeave: function(t) {
                    var a;
                    e.events._noMouseAction(t) || e.starClicked || (a = e.cache, e._toggleHover(a), e.$element.trigger("rating.hoverleave", ["stars"]))
                },
                clearMouseMove: function(t) {
                    var a, n, r, i;
                    !e.events._noMouseAction(t) && e.hoverOnClear && (e.clearClicked = !1, a = '<span class="' + e.clearCaptionClass + '">' + e.clearCaption + "</span>", n = e.clearValue, r = e.getWidthFromValue(n) || 0, i = {
                        caption: a,
                        width: r,
                        val: n
                    }, e._toggleHover(i), e.$element.trigger("rating.hover", [n, a, "clear"]))
                },
                clearMouseLeave: function(t) {
                    var a;
                    e.events._noMouseAction(t) || e.clearClicked || !e.hoverOnClear || (a = e.cache, e._toggleHover(a), e.$element.trigger("rating.hoverleave", ["clear"]))
                },
                resetForm: function(t) {
                    t && t.isDefaultPrevented() || e.inactive || e.reset()
                }
            }
        },
        /*_listen: function() {
            var a = this,
                n = a.$element,
                r = n.closest("form"),
                i = a.$rating,
                l = a.$clear,
                s = a.events;
            return t.handler(i, "touchstart touchmove touchend", e.proxy(s.initTouch, a)), t.handler(i, "click touchstart", e.proxy(s.starClick, a)), t.handler(i, "mousemove", e.proxy(s.starMouseMove, a)), t.handler(i, "mouseleave", e.proxy(s.starMouseLeave, a)), a.showClear && l.length && (t.handler(l, "click touchstart", e.proxy(s.clearClick, a)), t.handler(l, "mousemove", e.proxy(s.clearMouseMove, a)), t.handler(l, "mouseleave", e.proxy(s.clearMouseLeave, a))), r.length && t.handler(r, "reset", e.proxy(s.resetForm, a), !0), n
        },*/
        _getStars: function(e) {
            var t, a = this,
                n = '<span class="' + e + '-stars">';
            for (t = 1; t <= a.stars; t++) n += '<span class="star">' + a[e + "Star"] + "</span>";
            return n + "</span>"
        },
        _setStars: function(e) {
            var t = this,
                a = arguments.length ? t.calculate(e) : t.calculate(),
                n = t.$element,
                r = t._parseValue(a.val);
            return n.val(r), t.$filledStars.css("width", a.width), t._setCaption(a.caption), t.cache = a, n
        },
        showStars: function(e) {
            var t = this,
                a = t._parseValue(e);
            return t.$element.val(a), t._setStars()
        },
        calculate: function(e) {
            var a = this,
                n = t.isEmpty(a.$element.val()) ? 0 : a.$element.val(),
                r = arguments.length ? a.getValueFromPosition(e) : n,
                i = a.fetchCaption(r),
                l = a.getWidthFromValue(r);
            return l += "%", {
                caption: i,
                width: l,
                val: r
            }
        },
        getValueFromPosition: function(e) {
            var a, n, r = this,
                i = t.getDecimalPlaces(r.step),
                l = r.$rating.width();
            return n = r.diff * e / (l * r.step), n = r.rtl ? Math.floor(n) : Math.ceil(n), a = t.applyPrecision(parseFloat(r.min + n * r.step), i), a = Math.max(Math.min(a, r.max), r.min), r.rtl ? r.max - a : a
        },
        getWidthFromValue: function(e) {
            var t, a, n = this,
                r = n.min,
                i = n.max,
                l = n.$emptyStars;
            return !e || r >= e || r === i ? 0 : (a = l.outerWidth(), t = a ? l.width() / a : 1, e >= i ? 100 : (e - r) * t * 100 / (i - r))
        },
        fetchCaption: function(e) {
            var a, n, r, i, l, s = this,
                o = parseFloat(e) || s.clearValue,
                c = s.starCaptions,
                u = s.starCaptionClasses;
            return o && o !== s.clearValue && (o = t.applyPrecision(o, t.getDecimalPlaces(s.step))), i = "function" == typeof u ? u(o) : u[o], r = "function" == typeof c ? c(o) : c[o], n = t.isEmpty(r) ? s.defaultCaption.replace(/\{rating}/g, o) : r, a = t.isEmpty(i) ? s.clearCaptionClass : i, l = o === s.clearValue ? s.clearCaption : n, '<span class="' + a + '">' + l + "</span>"
        },
        destroy: function() {
            var a = this,
                n = a.$element;
            return t.isEmpty(a.$container) || a.$container.before(n).remove(), e.removeData(n.get(0)), n.off("rating").removeClass("rating rating-input")
        },
        create: function(e) {
            var t = this,
                a = e || t.options || {};
            return t.destroy().rating(a)
        },
        clear: function() {
            var e = this,
                t = '<span class="' + e.clearCaptionClass + '">' + e.clearCaption + "</span>";
            return e.inactive || e._setCaption(t), e.showStars(e.clearValue).trigger("change").trigger("rating.clear")
        },
        reset: function() {
            var e = this;
            return e.showStars(e.initialValue).trigger("rating.reset")
        },
        update: function(e) {
            var t = this;
            return arguments.length ? t.showStars(e) : t.$element
        },
        refresh: function(t) {
            var a = this,
                n = a.$element;
            return t ? a.destroy().rating(e.extend(!0, a.options, t)).trigger("rating.refresh") : n
        }
    }, e.fn.rating = function(n) {
        var r = Array.apply(null, arguments),
            i = [];
        switch (r.shift(), this.each(function() {
            var l, s = e(this),
                o = s.data("rating"),
                c = "object" == typeof n && n,
                u = c.theme || s.data("theme"),
                d = c.language || s.data("language") || "en",
                p = {},
                h = {};
            o || (u && (p = e.fn.ratingThemes[u] || {}), "en" === d || t.isEmpty(e.fn.ratingLocales[d]) || (h = e.fn.ratingLocales[d]), l = e.extend(!0, {}, e.fn.rating.defaults, p, e.fn.ratingLocales.en, h, c, s.data()), o = new a(this, l), s.data("rating", o)), "string" == typeof n && i.push(o[n].apply(o, r))
        }), i.length) {
            case 0:
                return this;
            case 1:
                return void 0 === i[0] ? this : i[0];
            default:
                return i
        }
    }, e.fn.rating.defaults = {
        theme: "",
        language: "en",
        stars: 5,
        filledStar: '<i class="glyphicon glyphicon-star"></i>',
        emptyStar: '<i class="glyphicon glyphicon-star-empty"></i>',
        containerClass: "",
        size: "md",
        animate: !0,
        displayOnly: !1,
        rtl: !1,
        showClear: !0,
        showCaption: !0,
        starCaptionClasses: {
            .5: "",
            
        },
        clearButton: '',
        clearButtonBaseClass: "clear-rating",
        clearButtonActiveClass: "clear-rating-active",
        clearCaptionClass: "label label-default",
        clearValue: null,
        captionElement: null,
        clearElement: null,
        hoverEnabled: !0,
        hoverChangeCaption: !0,
        hoverChangeStars: !0,
        hoverOnClear: !0,
        zeroAsNull: !0
    }, e.fn.ratingLocales.en = {
        defaultCaption: "{rating}",
        starCaptions: {
            
        },
       clearButtonTitle: "Clear",
        clearCaption: ""
    }, e.fn.rating.Constructor = a, e(document).ready(function() {
        var t = e("input.rating");
        t.length && t.removeClass("rating-loading").addClass("rating-loading").rating()
    })
});
</script>






<script type="text/javascript">
   
! function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof module && module.exports ? module.exports = e(require("jquery")) : e(window.jQuery)
}(function(e) {
    "use strict";
    e.fn.ratingLocales = {}, e.fn.ratingThemes = {};
    var t, a;
    t = {
        NAMESPACE: ".ratingg",
        DEFAULT_MIN: 0,
        DEFAULT_MAX: 5,
        DEFAULT_STEP: .5,
        isEmpty: function(t, a) {
            return null === t || void 0 === t || 0 === t.length || a && "" === e.trim(t)
        },
        getCss: function(e, t) {
            return e ? " " + t : ""
        },
        addCss: function(e, t) {
            e.removeClass(t).addClass(t)
        },
        getDecimalPlaces: function(e) {
            var t = ("" + e).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            return t ? Math.max(0, (t[1] ? t[1].length : 0) - (t[2] ? +t[2] : 0)) : 0
        },
        applyPrecision: function(e, t) {
            return parseFloat(e.toFixed(t))
        },
        handler: function(e, a, n, r, i) {
            var l = i ? a : a.split(" ").join(t.NAMESPACE + " ") + t.NAMESPACE;
            r || e.off(l), e.on(l, n)
        }
    }, a = function(t, a) {
        var n = this;
        n.$element = e(t), n._init(a)
    }, a.prototype = {
        constructor: a,
        _parseAttr: function(e, a) {
            var n, r, i, l, s = this,
                o = s.$element,
                c = o.attr("type");
            if ("range" === c || "number" === c) {
                switch (r = a[e] || o.data(e) || o.attr(e), e) {
                    case "min":
                        i = t.DEFAULT_MIN;
                        break;
                    case "max":
                        i = t.DEFAULT_MAX;
                        break;
                    default:
                        i = t.DEFAULT_STEP
                }
                n = t.isEmpty(r) ? i : r, l = parseFloat(n)
            } else l = parseFloat(a[e]);
            return isNaN(l) ? i : l
        },
        _parseValue: function(e) {
            var t = this,
                a = parseFloat(e);
            return isNaN(a) && (a = t.clearValue), !t.zeroAsNull || 0 !== a && "0" !== a ? a : null
        },
        _setDefault: function(e, a) {
            var n = this;
            t.isEmpty(n[e]) && (n[e] = a)
        },
        _initSlider: function(e) {
            var a = this,
                n = a.$element.val();
            a.initialValue = t.isEmpty(n) ? 0 : n, a._setDefault("min", a._parseAttr("min", e)), a._setDefault("max", a._parseAttr("max", e)), a._setDefault("step", a._parseAttr("step", e)), (isNaN(a.min) || t.isEmpty(a.min)) && (a.min = t.DEFAULT_MIN), (isNaN(a.max) || t.isEmpty(a.max)) && (a.max = t.DEFAULT_MAX), (isNaN(a.step) || t.isEmpty(a.step) || 0 === a.step) && (a.step = t.DEFAULT_STEP), a.diff = a.max - a.min
        },
        _initHighlight: function(e) {
            var t, a = this,
                n = a._getCaption();
            e || (e = a.$element.val()), t = a.getWidthFromValue(e) + "%", a.$filledStars.width(t), a.cache = {
                caption: n,
                width: t,
                val: e
            }
        },
        _getContainerCss: function() {
            var e = this;
            return "rating-container" + t.getCss(e.theme, "theme-" + e.theme) + t.getCss(e.rtl, "rating-rtl") + t.getCss(e.size, "rating-" + e.size) + t.getCss(e.animate, "rating-animate") + t.getCss(e.disabled || e.readonly, "rating-disabled") + t.getCss(e.containerClass, e.containerClass)
        },
        _checkDisabled: function() {
            var e = this,
                t = e.$element,
                a = e.options;
            e.disabled = void 0 === a.disabled ? t.attr("disabled") || !1 : a.disabled, e.readonly = void 0 === a.readonly ? t.attr("readonly") || !1 : a.readonly, e.inactive = e.disabled || e.readonly, t.attr({
                disabled: e.disabled,
                readonly: e.readonly
            })
        },
        _addContent: function(e, t) {
            var a = this,
                n = a.$container,
                r = "clear" === e;
            return a.rtl ? r ? n.append(t) : n.prepend(t) : r ? n.prepend(t) : n.append(t)
        },
        _generateRating: function() {
            var a, n, r, i = this,
                l = i.$element;
            n = i.$container = e(document.createElement("div")).insertBefore(l), t.addCss(n, i._getContainerCss()), i.$rating = a = e(document.createElement("div")).attr("class", "rating-stars").appendTo(n).append(i._getStars("empty")).append(i._getStars("filled")), i.$emptyStars = a.find(".empty-stars"), i.$filledStars = a.find(".filled-stars"), i._renderCaption(), i._renderClear(), i._initHighlight(), n.append(l), i.rtl && (r = Math.max(i.$emptyStars.outerWidth(), i.$filledStars.outerWidth()), i.$emptyStars.width(r)), l.appendTo(a)
        },
        _getCaption: function() {
            var e = this;
            return e.$caption && e.$caption.length ? e.$caption.html() : e.defaultCaption
        },
        _setCaption: function(e) {
            var t = this;
            t.$caption && t.$caption.length && t.$caption.html(e)
        },
        _renderCaption: function() {
            var a, n = this,
                r = n.$element.val(),
                i = n.captionElement ? e(n.captionElement) : "";
            if (n.showCaption) {
                if (a = n.fetchCaption(r), i && i.length) return t.addCss(i, "caption"), i.html(a), void(n.$caption = i);
                n._addContent("caption", '<div class="caption">' + a + "</div>"), n.$caption = n.$container.find(".caption")
            }
        },
        _renderClear: function() {
            var a, n = this,
                r = n.clearElement ? e(n.clearElement) : "";
            if (n.showClear) {
                if (a = n._getClearClass(), r.length) return t.addCss(r, a), r.attr({
                    title: n.clearButtonTitle
                }).html(n.clearButton), void(n.$clear = r);
                n._addContent("clear", '<div class="' + a + '" title="' + n.clearButtonTitle + '">' + n.clearButton + "</div>"), n.$clear = n.$container.find("." + n.clearButtonBaseClass)
            }
        },
        _getClearClass: function() {
            var e = this;
            return e.clearButtonBaseClass + " " + (e.inactive ? "" : e.clearButtonActiveClass)
        },
        _toggleHover: function(e) {
            var t, a, n, r = this;
            e && (r.hoverChangeStars && (t = r.getWidthFromValue(r.clearValue), a = e.val <= r.clearValue ? t + "%" : e.width, r.$filledStars.css("width", a)), r.hoverChangeCaption && (n = e.val <= r.clearValue ? r.fetchCaption(r.clearValue) : e.caption, n && r._setCaption(n + "")))
        },
        _init: function(t) {
            var a, n = this,
                r = n.$element.addClass("rating-input");
            return n.options = t, e.each(t, function(e, t) {
                n[e] = t
            }), (n.rtl || "rtl" === r.attr("dir")) && (n.rtl = !0, r.attr("dir", "rtl")), n.starClicked = !1, n.clearClicked = !1, n._initSlider(t), n._checkDisabled(), n.displayOnly && (n.inactive = !0, n.showClear = !1, n.showCaption = !1), n._generateRating(), n._initEvents(), n._listen(), a = n._parseValue(r.val()), r.val(a), r.removeClass("rating-loadingg")
        },
        _initEvents: function() {
            var e = this;
            e.events = {
                _getTouchPosition: function(a) {
                    var n = t.isEmpty(a.pageX) ? a.originalEvent.touches[0].pageX : a.pageX;
                    return n - e.$rating.offset().left
                },
                _listenClick: function(e, t) {
                    return e.stopPropagation(), e.preventDefault(), e.handled === !0 ? !1 : (t(e), void(e.handled = !0))
                },
                _noMouseAction: function(t) {
                    return !e.hoverEnabled || e.inactive || t && t.isDefaultPrevented()
                },
                initTouch: function(a) {
                    var n, r, i, l, s, o, c, u, d = e.clearValue || 0,
                        p = "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch;
                    p && !e.inactive && (n = a.originalEvent, r = t.isEmpty(n.touches) ? n.changedTouches : n.touches, i = e.events._getTouchPosition(r[0]), "touchend" === a.type ? (e._setStars(i), u = [e.$element.val(), e._getCaption()], e.$element.trigger("change").trigger("rating.change", u), e.starClicked = !0) : (l = e.calculate(i), s = l.val <= d ? e.fetchCaption(d) : l.caption, o = e.getWidthFromValue(d), c = l.val <= d ? o + "%" : l.width, e._setCaption(s), e.$filledStars.css("width", c)))
                },
                starClick: function(t) {
                    var a, n;
                    e.events._listenClick(t, function(t) {
                        return e.inactive ? !1 : (a = e.events._getTouchPosition(t), e._setStars(a), n = [e.$element.val(), e._getCaption()], e.$element.trigger("change").trigger("rating.change", n), void(e.starClicked = !0))
                    })
                },
                clearClick: function(t) {
                    e.events._listenClick(t, function() {
                        e.inactive || (e.clear(), e.clearClicked = !0)
                    })
                },
                starMouseMove: function(t) {
                    var a, n;
                    e.events._noMouseAction(t) || (e.starClicked = !1, a = e.events._getTouchPosition(t), n = e.calculate(a), e._toggleHover(n), e.$element.trigger("rating.hover", [n.val, n.caption, "stars"]))
                },
                starMouseLeave: function(t) {
                    var a;
                    e.events._noMouseAction(t) || e.starClicked || (a = e.cache, e._toggleHover(a), e.$element.trigger("rating.hoverleave", ["stars"]))
                },
                clearMouseMove: function(t) {
                    var a, n, r, i;
                    !e.events._noMouseAction(t) && e.hoverOnClear && (e.clearClicked = !1, a = '<span class="' + e.clearCaptionClass + '">' + e.clearCaption + "</span>", n = e.clearValue, r = e.getWidthFromValue(n) || 0, i = {
                        caption: a,
                        width: r,
                        val: n
                    }, e._toggleHover(i), e.$element.trigger("rating.hover", [n, a, "clear"]))
                },
                clearMouseLeave: function(t) {
                    var a;
                    e.events._noMouseAction(t) || e.clearClicked || !e.hoverOnClear || (a = e.cache, e._toggleHover(a), e.$element.trigger("rating.hoverleave", ["clear"]))
                },
                resetForm: function(t) {
                    t && t.isDefaultPrevented() || e.inactive || e.reset()
                }
            }
        },
        /*_listen: function() {
            var a = this,
                n = a.$element,
                r = n.closest("form"),
                i = a.$rating,
                l = a.$clear,
                s = a.events;
            return t.handler(i, "touchstart touchmove touchend", e.proxy(s.initTouch, a)), t.handler(i, "click touchstart", e.proxy(s.starClick, a)), t.handler(i, "mousemove", e.proxy(s.starMouseMove, a)), t.handler(i, "mouseleave", e.proxy(s.starMouseLeave, a)), a.showClear && l.length && (t.handler(l, "click touchstart", e.proxy(s.clearClick, a)), t.handler(l, "mousemove", e.proxy(s.clearMouseMove, a)), t.handler(l, "mouseleave", e.proxy(s.clearMouseLeave, a))), r.length && t.handler(r, "reset", e.proxy(s.resetForm, a), !0), n
        },*/
        _getStars: function(e) {
            var t, a = this,
                n = '<span class="' + e + '-stars">';
            for (t = 1; t <= a.stars; t++) n += '<span class="star">' + a[e + "Star"] + "</span>";
            return n + "</span>"
        },
        _setStars: function(e) {
            var t = this,
                a = arguments.length ? t.calculate(e) : t.calculate(),
                n = t.$element,
                r = t._parseValue(a.val);
            return n.val(r), t.$filledStars.css("width", a.width), t._setCaption(a.caption), t.cache = a, n
        },
        showStars: function(e) {
            var t = this,
                a = t._parseValue(e);
            return t.$element.val(a), t._setStars()
        },
        calculate: function(e) {
            var a = this,
                n = t.isEmpty(a.$element.val()) ? 0 : a.$element.val(),
                r = arguments.length ? a.getValueFromPosition(e) : n,
                i = a.fetchCaption(r),
                l = a.getWidthFromValue(r);
            return l += "%", {
                caption: i,
                width: l,
                val: r
            }
        },
        getValueFromPosition: function(e) {
            var a, n, r = this,
                i = t.getDecimalPlaces(r.step),
                l = r.$rating.width();
            return n = r.diff * e / (l * r.step), n = r.rtl ? Math.floor(n) : Math.ceil(n), a = t.applyPrecision(parseFloat(r.min + n * r.step), i), a = Math.max(Math.min(a, r.max), r.min), r.rtl ? r.max - a : a
        },
        getWidthFromValue: function(e) {
            var t, a, n = this,
                r = n.min,
                i = n.max,
                l = n.$emptyStars;
            return !e || r >= e || r === i ? 0 : (a = l.outerWidth(), t = a ? l.width() / a : 1, e >= i ? 100 : (e - r) * t * 100 / (i - r))
        },
        fetchCaption: function(e) {
            var a, n, r, i, l, s = this,
                o = parseFloat(e) || s.clearValue,
                c = s.starCaptions,
                u = s.starCaptionClasses;
            return o && o !== s.clearValue && (o = t.applyPrecision(o, t.getDecimalPlaces(s.step))), i = "function" == typeof u ? u(o) : u[o], r = "function" == typeof c ? c(o) : c[o], n = t.isEmpty(r) ? s.defaultCaption.replace(/\{rating}/g, o) : r, a = t.isEmpty(i) ? s.clearCaptionClass : i, l = o === s.clearValue ? s.clearCaption : n, '<span class="' + a + '">' + l + "</span>"
        },
        destroy: function() {
            var a = this,
                n = a.$element;
            return t.isEmpty(a.$container) || a.$container.before(n).remove(), e.removeData(n.get(0)), n.off("rating").removeClass("rating rating-input")
        },
        create: function(e) {
            var t = this,
                a = e || t.options || {};
            return t.destroy().rating(a)
        },
        clear: function() {
            var e = this,
                t = '<span class="' + e.clearCaptionClass + '">' + e.clearCaption + "</span>";
            return e.inactive || e._setCaption(t), e.showStars(e.clearValue).trigger("change").trigger("rating.clear")
        },
        reset: function() {
            var e = this;
            return e.showStars(e.initialValue).trigger("rating.reset")
        },
        update: function(e) {
            var t = this;
            return arguments.length ? t.showStars(e) : t.$element
        },
        refresh: function(t) {
            var a = this,
                n = a.$element;
            return t ? a.destroy().rating(e.extend(!0, a.options, t)).trigger("rating.refresh") : n
        }
    }, e.fn.rating = function(n) {
        var r = Array.apply(null, arguments),
            i = [];
        switch (r.shift(), this.each(function() {
            var l, s = e(this),
                o = s.data("rating"),
                c = "object" == typeof n && n,
                u = c.theme || s.data("theme"),
                d = c.language || s.data("language") || "en",
                p = {},
                h = {};
            o || (u && (p = e.fn.ratingThemes[u] || {}), "en" === d || t.isEmpty(e.fn.ratingLocales[d]) || (h = e.fn.ratingLocales[d]), l = e.extend(!0, {}, e.fn.rating.defaults, p, e.fn.ratingLocales.en, h, c, s.data()), o = new a(this, l), s.data("rating", o)), "string" == typeof n && i.push(o[n].apply(o, r))
        }), i.length) {
            case 0:
                return this;
            case 1:
                return void 0 === i[0] ? this : i[0];
            default:
                return i
        }
    }, e.fn.rating.defaults = {
        theme: "",
        language: "en",
        stars: 5,
        filledStar: '<i class="glyphicon glyphicon-star"></i>',
        emptyStar: '<i class="glyphicon glyphicon-star-empty"></i>',
        containerClass: "",
        size: "md",
        animate: !0,
        displayOnly: !1,
        rtl: !1,
        showClear: !0,
        showCaption: !0,
        starCaptionClasses: {
            .5: "",
            
        },
        clearButton: '',
        clearButtonBaseClass: "clear-rating",
        clearButtonActiveClass: "clear-rating-active",
        clearCaptionClass: "",
        clearValue: null,
        captionElement: null,
        clearElement: null,
        hoverEnabled: !0,
        hoverChangeCaption: !0,
        hoverChangeStars: !0,
        hoverOnClear: !0,
        zeroAsNull: !0
    }, e.fn.ratingLocales.en = {
        defaultCaption: "",
        starCaptions: {
            
        },
       clearButtonTitle: "Clear",
        clearCaption: ""
    }, e.fn.rating.Constructor = a, e(document).ready(function() {
        var t = e("input.ratingg");
        t.length && t.removeClass("rating-loadingg").addClass("rating-loadingg").rating()
    })
});
</script>


 <?php include_once('footer.php'); ?>
<!-- <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <span id="errorfield"></span>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Enter New Password" name="newpass" id="newpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Confirm New Password" name="confpass" id="confpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <button type="button" id="changepassbtn" class="srchbtns">Change</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> -->
<!-- <script src="<?php echo base_url().'webfiles/';?>js/jquery.min.js"></script>
 --><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
   <script>
  function getdayname(id)
     {
      var getdate = $("#datetimepicker1"+id).val();
      var dayfrom = $("#dayfromm"+id).val();
      var dayto   = $("#daytoo"+id).val();
      if(dayfrom ==1)
      {
        var day ='Monday';
      }
      if(dayfrom ==2)
      {
        var day ='Tuesday';
      }
      if(dayfrom ==3)
      {
        var day ='Wednesday';
      }
      if(dayfrom ==4)
      {
        var day ='Thursday';
      }
      if(dayfrom ==5)
      {
        var day ='Friday';
      }
      if(dayfrom ==6)
      {
        var day ='Saturday';
      }
      if(dayfrom ==7)
      {
        var day ='Sunday';
      }
      
     if(dayto ==1)
      {
        var dayt ='Monday';
      }
      if(dayto ==2)
      {
        var dayt ='Tuesday';
      }
      if(dayto ==3)
      {
        var dayt ='Wednesday';
      }
      if(dayto ==4)
      {
        var dayt ='Thursday';
      }
      if(dayto ==5)
      {
        var dayt ='Friday';
      }
      if(dayto ==6)
      {
        var dayt ='Saturday';
      }
      if(dayto ==7)
      {
        var dayt ='Sunday';
      }
      //  alert(getdate);return false;
      $.ajax({
        'type' :'POST',
        'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
        'data' :'dateValue='+getdate+'&jobId='+id,
        'success':function(htmlres)
         {
          if(htmlres !='')
             {
              if(htmlres == 1)
                {
                    $('#schedule'+id).attr('disabled',false);
                    $("#setval"+id).html("");
                    $("#datetimepicker1"+id).val(getdate);
                }
                else
                {
                  $("#setval"+id).html("Please Select Schedule date between ("+day+" to "+dayt+")");
                  $("#datetimepicker1"+id).val(''); 
                  $('#schedule'+id).attr('disabled',true);
                }
             }
          }
               
       });
      }
   
   </script>


 <script>
    function getTime(id)
       {
          var gettime= $("#datepicker1"+id).val();
          var timefrom= $("#timefromm"+id).val();
          var timetoo= $("#timetoo"+id).val();
          //return false;
          $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
             'data' :'timeValue='+gettime+'&jobId='+id,
             'success':function(htmlres)
             {
                if(htmlres !='')
                {
                if(htmlres == 1)
                {
                  $('#schedule'+id).attr('disabled',false);
                  $("#setvall"+id).html("");
                  $("#datepicker1"+id).val(gettime);
                }
                else
                {
                    //alert('Please Select Schedule time');
                   $("#setvall"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                   $("#datepicker1"+id).val(''); 
                    $('#schedule'+id).attr('disabled',true);
                }
             }
             }
               
           });
       }
 </script>

<script>
  function getdaynamee(id)
     {

      var getdate= $("#datetimepicker12"+id).val();
      var dayfrom = $("#dayfrommm"+id).val();
      var dayto   = $("#daytooo"+id).val();
      if(dayfrom ==1)
      {
        var day ='Monday';
      }
      if(dayfrom ==2)
      {
        var day ='Tuesday';
      }
      if(dayfrom ==3)
      {
        var day ='Wednesday';
      }
      if(dayfrom ==4)
      {
        var day ='Thursday';
      }
      if(dayfrom ==5)
      {
        var day ='Friday';
      }
      if(dayfrom ==6)
      {
        var day ='Saturday';
      }
      if(dayfrom ==7)
      {
        var day ='Sunday';
      }
      
     if(dayto ==1)
      {
        var dayt ='Monday';
      }
      if(dayto ==2)
      {
        var dayt ='Tuesday';
      }
      if(dayto ==3)
      {
        var dayt ='Wednesday';
      }
      if(dayto ==4)
      {
        var dayt ='Thursday';
      }
      if(dayto ==5)
      {
        var dayt ='Friday';
      }
      if(dayto ==6)
      {
        var dayt ='Saturday';
      }
      if(dayto ==7)
      {
        var dayt ='Sunday';
      }
         // alert(getdate);return false;
      $.ajax({
        'type' :'POST',
        'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
        'data' :'dateValue='+getdate+'&jobId='+id,
        'success':function(htmlres)
         {
          if(htmlres !='')
             {
              if(htmlres == 1)
                {
                    $('#schedulees'+id).attr('disabled',false);
                    $("#setval12"+id).html("");
                    $("#datetimepicker12"+id).val(getdate);
                }
                else
                {
                  $("#setval12"+id).html("Please Select Schedule date between ("+day+" to "+dayt+")");
                  $("#datetimepicker12"+id).val(''); 
                  $('#schedulees'+id).attr('disabled',true);
                }
             }
          }
               
       });
      }
   
   </script>

   
 <script>
    function getTimee(id)
       {
          var gettime= $("#datepicker13"+id).val();
          var timefrom= $("#timefrommm"+id).val();
          var timetoo= $("#timetooo"+id).val();
          $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
             'data' :'timeValue='+gettime+'&jobId='+id,
             'success':function(htmlres)
             {
                if(htmlres !='')
                {
                if(htmlres == 1)
                {
                  $('#schedulees'+id).attr('disabled',false);
                  $("#setvall12"+id).html("");
                    $("#datepicker13"+id).val(gettime);
                }
                else
                {
                    //alert('Please Select Schedule time');
                   $("#setvall12"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                   $("#datepicker13"+id).val(''); 
                    $('#schedulees'+id).attr('disabled',true);
                }
             }
             }
               
           });
       }
 </script>

<script>
   $(document).ready(function(){   
   $("#changepassbtn").click(function() {
     //var oldpass = $("#oldpass").val();
     var newpass = $("#newpass").val();
     var confpass = $("#confpass").val();
     $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>" + "user/changepass",
         data: {newpass:newpass,confpass:confpass},
         cache:false,
         success:function(htmldata){
         
              $('#errorfield').html(htmldata);
             
         },
         error:function(){
           console.log('error');
         }
     });
   });
   });
</script>

<script>

 function getFilename(id)
 {
    var name = document.getElementById("file"+id).files[0].name;
    var filetype= $("#file"+id).val();
    var ext = filetype.split('.').pop();
    if(ext =="jpg" || ext =="jpeg" || ext =="png"){
      $("#setimgres"+id).html("<span style='color:red'>plese select file format(pdf/doc/docx)</span>");
      return false;
   } 
   else
   {
    $("#setimgres"+id).html("");
    var form_data = new FormData();
    form_data.append("file", document.getElementById('file'+id).files[0]);
    $.ajax({
      url:'<?php echo base_url('dashboard/imageUpload')?>',
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(imagedata)
      { 
        if(imagedata !='')
        {
        $("#imagedata"+id).val(imagedata);
        $("#file"+id).val('');
        $("#setimgres"+id).html(imagedata);
      }
      }
    });
   }
 }
</script>
<script type="text/javascript">
  function PdfImagesend(id)
  {
    var image= $("#imagedata"+id).val();
      $.ajax({
             'type' :'POST',
             'url' :"<?php echo base_url('dashboard/fileInsert') ?>",
             'data' :'imagedata='+image,
             'success':function(htmlres)
             {

               if(htmlres=='1')
               {
                $("#exampleModalCenterdd"+id).modal('show');
               }
               if(htmlres=='2')
               {
                 $("#exampleModalCenterdd"+id).modal('show');
               }
             }
       });
   }
</script>
<?php if(!empty($jobDetails)) {
                  $x=30;
                  foreach($jobDetails as $jobDetailsData){
                    $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobDetailsData['id']);
                    $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetailsData['company_id']);
                    $timeFrom = $recruiterdetail[0]['from_time'];
                    $timeFromm = date('H:i', strtotime($timeFrom));
                    $timeTo   = $recruiterdetail[0]['to_time'];
                    $timeToo = date('H:i', strtotime($timeTo));
                    ?>

                    <div class="modal fade" id="exampleModalCenterInstant<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">×</span>
             </button>
          </div>
          <div class="modal-body">
             <div class="formmidaress modpassfull">
                <div class="filldetails">
                    <div class="addupdatecent">
                       <p>You will now be redirected to the Instant Assessment page. Your time invested now will speed up the hiring process later. Jobyoda wishes you all the best! Remember, "Every pro was once an amature. Every expert was once a beginner. So dream big and start now".</p>
                       <div class="statsusdd">
                          <a class="greenbtn" onclick="instantfunction('<?php echo $jobDetailsData['jobpost_id']; ?>', '<?php echo $jobDetailsData['modeurl']; ?>')" href="javascript:void(0)"> Begin Screening </a>
                       </div>
                    </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>

 
<div class="modal fade" id="exampleModalCenter9<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                       <div class="modal-dialog modal-dialog-centered" role="document">
                                          <div class="modal-content">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                                </button>
                                             </div>
                                             <div class="modal-body">
                                                <div class="formmidaress modpassfull">
                                                   <div class="filldetails">
                                                      <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                                         <div class="schedulejobgs">
                                                            <h6>Schedule interview</h6>
                                                            <p>Select Date & Time</p>
                                                            <div class="forminputspswd">
                                                               <p>Date</p>
                                              <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker1<?php echo $jobDetailsData['id']?>" autocomplete="off" placeholder="mm/dd/yy" onchange="getdayname('<?php echo $jobDetailsData['id']?>')"  required="required">
                                              <span id="setval<?php echo $jobDetailsData['id']?>" style="color:red"></span>
                                              <input type="hidden" id="dayfromm<?php echo $jobDetailsData['id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytoo<?php echo $jobDetailsData['id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefromm<?php echo $jobDetailsData['id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetoo<?php echo $jobDetailsData['id']?>" value="<?php echo $timeToo;?>">
                                             <i class="far fa-calendar-alt fieldicons"></i>
                                              </div>
                                            <div class="forminputspswd">
                                              <p>Time</p>
                                            <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTime('<?php echo $jobDetailsData['id']?>')" id="datepicker1<?php echo $jobDetailsData['id']?>"  data-format="hh:mm:ss" required="required"> 
                                             <span id="setvall<?php echo $jobDetailsData['id']?>" style="color:red"></span>
                                             <i class="far fa-clock"></i>
                                                            </div>
                                              <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $jobDetailsData['id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                 <button type="submit" class="updt" id="schedule<?php echo $jobDetailsData['id']?>" disabled>Schedule & Apply</button>
                                              </div>
                                            </div>
                                          </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                <div class="modal fade" id="companyapplymodal<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                   <div class="modal-dialog modal-dialog-centered" role="document">
                   <div class="modal-content">
                   <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                    </button>
                   </div>
                   <div class="modal-body">
                   <div class="formmidaress modpassfull">
                        <div class="filldetails">
                            <form method="post" action="<?php echo base_url();?>dashboard/resumeUpload" enctype="multipart/form-data">
                            <div class="addupdatecent">
                                <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png" style="width:50px">
                                <input type="file" class="resumeupdtf" id="file<?php echo $x;?>" title="" name="resumeFile" onchange='getFilename("<?php echo $x ?>")'>
                                <p id="setimgres<?php echo $x ?>"></p>
                                <input type="hidden" id="imagedata<?php echo $x ?>">
                                <p>Please Add Your Resume</p>
                            <div class="statsusdd">
                                <!-- <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Close</p> -->
                                <p class="norm" class="close" data-toggle="modal" data-target="#exampleModalCenter9<?php echo $x;?>" data-dismiss="modal">Cancel</p>
                                            
                                <?php if(!empty($checkResume[0]['resume'])) {?>
                                    <button type="button" onclick="PdfImagesend('<?php echo $x ?>')"  class="updt" data-toggle="modal" data-target="#exampleModalCenterdd<?php echo $x;?>" data-dismiss="modal">Update</button>
                                <?php } else {?>
                                    <button type="button" onclick="PdfImagesend('<?php echo $x ?>')" data-toggle="modal" data-target="#exampleModalCenterdd<?php echo $x;?>" data-dismiss="modal"  class="updt">Add</button>
                                <?php }?>
                             </div>
                             </div>
                           </form>
                        </div>
                    </div>
                   </div>
                </div>
              </div>
            </div>      
             


             <div class="modal fade" id="exampleModalCenterdd<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                       <div class="modal-dialog modal-dialog-centered" role="document">
                                          <div class="modal-content">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                                </button>
                                             </div>
                                             <div class="modal-body">
                                                <div class="formmidaress modpassfull">
                                                   <div class="filldetails">
                                                      <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                                         <div class="schedulejobgs">
                                                            <h6>Schedule interview</h6>
                                                            <p>Select Date & Time</p>
                                                            <div class="forminputspswd">
                                                               <p>Date</p>
                                                               <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker12<?php echo $jobDetailsData['id']?>" placeholder="mm/dd/yy" onchange="getdaynamee('<?php echo $jobDetailsData['id']?>')"  required="required" autocomplete="off">
                                                <input type="hidden" id="dayfrommm<?php echo $jobDetailsData['id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytooo<?php echo $jobDetailsData['id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">

                                                <input type="hidden" id="timefrommm<?php echo $jobDetailsData['id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetooo<?php echo $jobDetailsData['id']?>" value="<?php echo $timeToo;?>">
                                                           <span id="setval12<?php echo $jobDetailsData['id']?>" style="color:red"></span>
                                             <i class="far fa-calendar-alt fieldicons"></i>
                                                            </div>
                                                            <div class="forminputspswd">
                                                               <p>Time</p>
                                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee('<?php echo $jobDetailsData['id']?>')" id="datepicker13<?php echo $jobDetailsData['id']?>"  data-format="hh:mm:ss" required="required"> 
                                             <span id="setvall12<?php echo $jobDetailsData['id']?>" style="color:red"></span>
                                             <i class="far fa-clock"></i>
                                                            </div>
                                                            <div class="statsusdd">
                                                               <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                               <input type="hidden" name="listing" value="<?php echo $jobDetailsData['id'];?>">
                                                               <input type="hidden" name="type" value="<?php echo $x;?>">
                                                               <button type="submit" class="updt" id="schedulees<?php echo $jobDetailsData['id']?>" disabled>Schedule & Apply</button>
                                                            </div>
                                                         </div>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div> 
                                    <?php 
                                    $x++;
                                    }}?>
</body>
</html>