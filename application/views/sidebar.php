<style type="text/css">
   .menuinside ul{}
   .menuinside ul li{ border: none; box-shadow: none; border-radius: 0px; background: inherit !important; }
   .menuinside ul li a{
    padding: 0px 10px 0 5px;
    border: 1px solid #ddd;
    border-radius: 5px;
        border-radius: 5px;
   }
   .menuinside ul li a img.acthover{
    width: 18px;
    height: 18px;
    color: #000;
    display: block;
   }
   .menuinside ul li a p{
      padding: 0;
      font-family: Roboto;
      color: #696969;
      font-size: 11.5px;
      line-height: 30px;
   }
   .menuinside ul li a p span.countnoitfy{
    margin: 7px 0 0 6px;
    line-height: 18px;
    box-shadow: none;
    background-color: #e99d1f;
    font-size: 11px;
    font-weight: 600;
   }

   .menuinside ul li:hover{ background: inherit; }
   .menuinside ul li a:hover,
   .menuinside ul li.active a{
    background-color: #00a94f;
      border-color: #00a94f;
   }

   .menuinside ul li a:hover p{}

   .menuinside ul li:hover a img.acthover{ display: block; }

   .menuinside ul li a:hover img.acthover,
   .menuinside ul li.active a img.acthover{
          filter: brightness(0) invert(1);
   }


</style>


 <div class="crossmob"> <i class="fas fa-times"></i></div>
<ul>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/user/homeafterlogin'){?> active <?php }?>">
      <a href="<?php echo base_url(); ?>user/homeafterlogin">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/search_job.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/search_job.png" class="acthover">
         <p>Search Jobs</p>
      </a>
   </li>
    <li class="<?php if($_SERVER['REQUEST_URI']=='/profile'){?> active <?php }?>">
      <a href="<?php echo base_url(); ?>profile">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/user.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/user.png" class="acthover">
         <p>My Profile</p>
      </a>
   </li>
     <!--  <li>
      <a href="<?php echo base_url('dashboard/filters') ?>">
         <img src="<?php echo base_url().'webfiles/';?>img/search.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>img/searchover.png" class="acthover">
         <p>Search Jobs</p>
      </a>
   </li> -->
 <li>
      <a href="#" data-toggle="modal" data-target="#statusModalCenter36">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/search_job.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/search_job.png" class="acthover">
         <p>Job Search Status</p>
      </a>
   </li>  
   <li class="<?php if($_SERVER['REQUEST_URI']=='/signinbonus'){?> active <?php }?>">
      <a href="<?php echo base_url('signinbonus') ?>">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/bonus.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/bonus.png" class="acthover">
         <p>Sign In Bonus</p>
      </a>
   </li>
   
  
   <li class="<?php if($_SERVER['REQUEST_URI']=='/successstory'){?> active <?php }?>">
      <a href="<?php echo base_url(); ?>successstory">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/success.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/success.png" class="acthover">
         <p>Success Stories</p>
      </a>
   </li>
  
   <li class="<?php if($_SERVER['REQUEST_URI']=='/hotjob'){?> active <?php }?>">
      <a href="<?php echo base_url(); ?>hotjob">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/hot_job.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/hot_job.png" class="acthover">
         <p>Hot Jobs</p>
      </a>
   </li>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/savedjob'){?> active <?php }?>">
      <a href="<?php echo base_url(); ?>savedjob">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/saved_job.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/saved_job.png" class="acthover">
         <p>Saved Jobs</p>
      </a>
   </li>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/appliedjobs'){?> active <?php }?>">
      <a href="<?php echo base_url(); ?>appliedjobs">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/applied_job.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/applied_job.png" class="acthover">
         <p>Applied Jobs</p>
      </a>
   </li>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/notifications'){?> active <?php }?>">
      <a href="<?php echo base_url(); ?>notifications">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/notification.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/notification.png" class="acthover">
         <p>Notification <?php if(!empty($badge)){?><span class="countnoitfy"><?php if(!empty($badge)){ echo $badge; }else{  }?></span><?php }?></p> 
      </a>
   </li>
   <li>
   <?php $userSess = $this->session->userdata('usersess');
                  $type = $usersess['type'];
                  if($type=='normal'){
                  ?>
      <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModalCenter">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/change_password.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/change_password.png" class="acthover">
         <p>change Password</p>
      </a>
      <?php }else{ ?>
      <a href="javascript:void(0);" data-toggle="modal" data-target="#socialModal">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/change_password.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/change_password.png" class="acthover">
         <p>change Password</p>
      </a>
      <?php }?>
   </li>
    <!-- 
	<li>
      <a href="<?php echo base_url(); ?>dashboard/aboutus">
         <img src="<?php echo base_url().'webfiles/';?>img/edit.png" class="nonhover"><img src="<?php echo base_url().'webfiles/';?>img/edithover.png" class="acthover">
         <p>About US</p>
      </a>
   </li>
   
   <li>
      <a href="<?php echo base_url(); ?>dashboard/contactUs">
         <img src="<?php echo base_url().'webfiles/';?>img/mail-1.png" class="nonhover"><img src="<?php echo base_url().'webfiles/';?>img/mail-2.png" class="acthover">
         <p>Contact Us</p>
      </a>
   </li>  -->
   
  
   <li>
      <a href="<?php echo base_url(); ?>dashboard/logout">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/logout.png" class="nonhover">
         <img src="<?php echo base_url().'webfiles/';?>newone/images/logout.png" class="acthover">
         <p>Logout</p>
      </a>
   </li>
</ul>