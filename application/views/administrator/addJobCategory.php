<style type="text/css">
  .validError{
    color: red;
  }
</style>
<?php include('header.php'); 
//print_r($jobtitleLists);
?>
<div class="content custom-scrollbar">
<div class="doc data-table-doc page-layout simple full-width">

 <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-playlist-check"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Add Job Category</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
						</div>
						
						<div class="page-content p-6">
					 <div class="content container">
                           <div class="row">
                              <div class="col-12"> 
					 
					 <div class="example">
					 
					  <div class="source-preview-wrapper">
                                       <div class="preview">
									    <div class="preview-elements">
                    <div id="registers">

                        <div class="form-wrapper">

                         
                    <div class="mainjob">
                            <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/jobcategoryInsert" method="post" novalidate>
                        <div class="job">
                                <div class="form-group mb-4">
                                    <input type="text" name="category" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" required="required" />
                                    <label for="registerFormInputName">Job Category</label>
                                    <?php if(!empty($errors['category'])){?>
                                    <span class="validError"><?php echo $errors['category']; ?></span>
                                    <?php } ?>
                                </div>
						</div>		
                           <div class="jobbtn">     
                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Add Category
                                </button>
							</div>	
							</div>
                               <div class="newcustom">
							      <div class="preview">
                                          <div class="preview-elements">
                                           <!--  <div id="sample-data-table_wrapper" class="dataTables_wrapper no-footer"><div class="dataTables_length" id="sample-data-table_length"><label>Show <select name="sample-data-table_length" aria-controls="sample-data-table" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="sample-data-table_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="sample-data-table"></label></div>-->
											 <table id="sample-data-table" class="table dataTable no-footer" role="grid" aria-describedby="sample-data-table_info" style="width: 855px;">
                                                <thead>
                                                   <tr role="row"><th class="secondary-text sorting_asc" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                                                         
                                                            ID
                                                         
                                                      : activate to sort column descending" style="width: 40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">ID</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">JobCategory</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Action
                                                         
                                                      : activate to sort column ascending" style="width: 68px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th></tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $x1 =1;
                                                    foreach($jobcategoryLists as $jobcategory){
                                                    ?>
                                                    <tr role="row" class="odd">
                                                      <td class="sorting_1"><?php echo $x1;?></td>
                                                      <td><?php echo $jobcategory['category'];?></td>
                                                      
                                                      
                                                      
                                                      <td>
													  
														 <a href="<?php echo base_url(); ?>administrator/recruiter/deletejobcategory?id=<?php echo $jobcategory['id'] ?>" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Job Level"><i class="icon s-4 icon-trash"></i></a>  
														 	 
                                                      </td> 
                                                      
                                                   </tr>
                                                   <?php $x1++;
                                                   }?>
                                                   </tbody>
                                             </table><!--
											 <div class="dataTables_info" id="sample-data-table_info" role="status" aria-live="polite">Showing 1 to 10 of 17 entries</div><div class="dataTables_paginate paging_simple_numbers" id="sample-data-table_paginate"><a class="paginate_button previous disabled" aria-controls="sample-data-table" data-dt-idx="0" tabindex="0" id="sample-data-table_previous">Previous</a><span><a class="paginate_button current" aria-controls="sample-data-table" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="sample-data-table" data-dt-idx="2" tabindex="0">2</a></span><a class="paginate_button next" aria-controls="sample-data-table" data-dt-idx="3" tabindex="0" id="sample-data-table_next">Next</a></div></div>-->
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
							   </div>
                            </form>
                        </div>
                    </div>
					
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					
                  </div>
                </div>
            </div>
            

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
        </nav>
    </main>
</body>

</html>