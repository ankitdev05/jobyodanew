<style type="text/css">
    .validError{
        color: red;
    }
    .imgmsg{
      font-size: 13px;
      display: block;
      text-align: center;
      margin: 15px 0 0 0;
    }
    p#getlengthtext {
    position: absolute;
    bottom: -28px;
    right: 16px;
    font-size: 11px;
}
</style>
<?php include('header.php'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Add Video Advertisement</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/videoadvertiseInsert" method="post" novalidate enctype="multipart/form-data">

                                <div class="form-group mb-4">
                                    <label>Upload Video</label>
                                    <center id="loadervideo" style="display: none;">
                                        <img src="<?php echo base_url();?>recruiterfiles/loadervideo.gif"> 
                                    </center>
                                    <input type="file" name="videoupload" class="form-control" id="imgInp">
                                    
                                </div>
                                <span class="imgmsg">*Video size upto 100 MB.</span>
                                <div class="form-group mb-4">
                                    <input type="text" name="title" value="<?php if(!empty($userData['title'])){ echo $userData['title']; } ?>" class="form-control" aria-describedby="nameHelp"  >
                                    <label>Title</label>
                                    <?php if(!empty($errors['title'])){echo "<span class='validError'>".$errors['title']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea id="lengthtext" rows="5" name="desc" class="form-control" maxlength="800"></textarea>
                                    <p id="getlengthtext" style="color:#27ae62"> </p>
                                    <?php if(!empty($errors['desc'])){echo "<span class='validError'>".$errors['desc']."</span>";}?>
                                </div>

                                
                                

                                <div class="form-group mb-4">
                                    <input type="text" autocomplete="off" name="start_date" id="start_date" value="<?php if(!empty($userData['start_date'])){ echo $userData['start_date']; } ?>" class="form-control" aria-describedby="nameHelp" />
                                    <label>Start Date</label>
                                    <?php if(!empty($errors['start_date'])){echo "<span class='validError'>".$errors['start_date']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="text" autocomplete="off" id="end_date" value="<?php if(!empty($userData['end_date'])){ echo $userData['end_date']; } ?>" name="end_date" class="form-control" >
                                    <label>End Date</label>
                                    <?php if(!empty($errors['end_date'])){echo "<span class='validError'>".$errors['end_date']."</span>";}?>
                                </div>

                                <input type="hidden" name="videourl" value="" id="videourl">
                                <input type="hidden" name="videothumb" value="" id="videothumb">
                                <input type="hidden" name="publicid" value="" id="publicid">

                                <button type="submit" id="buttondisable" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN" disabled="">
                                    Submit
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
</body>

</html>

<script type="text/javascript">
      $(function(){
            $('#lengthtext').on('keyup', function() {
                  var lengthText = $(this).val();
                  console.log(lengthText);
                  var textcount = "Text Count : " + lengthText.length;
                  $('#getlengthtext').html(textcount);
            });
        });
</script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
    $('#cname').change(function(){
        var cname = $('#cname').val();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "administrator/recruiter/getsite",
          data: {cname:cname},
          cache:false,
          success:function(htmldata){
   
            $("#site").html(htmldata);
              
          },
      });
    });
    /*$('#cname').change(function(){
        var cname = $('#cname').val();
        $('#compid').val(cname);
    })*/

   $(function(){  
        $.datepicker.setDefaults({  
            dateFormat: 'yy-mm-dd'   
        }); 
        $("#start_date").datepicker({minDate: "0" });  
        $("#end_date").datepicker({minDate: "0" }); 
        
   });
</script> 

<script type="text/javascript">
    function getExtension(filename) {
        var parts = filename.split('.');
        return parts[parts.length - 1];
    }

    function isVideo(filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
          case 'm4v':
          case 'avi':
          case 'mpg':
          case 'mp4':
            return true;
        }
        return false;
    }
      $("document").ready(function() {
            $("#imgInp").on('change', function(e){
                  e.preventDefault();
                  // Get form
                  var data = new FormData();
                  //data.append( 'media', $( '#imgInp' )[0].files[0] );
                  data.append( 'file', $( '#imgInp' )[0].files[0] );
                  data.append( 'upload_preset', 'vv0sspbd');
                
                  if (!isVideo($('#imgInp').val())) {
                        alert("Please select only video files");
                  } else {
                    $("#loadervideo").css("display","block");
                      $.ajax({
                            type: 'POST',
                            enctype: 'multipart/form-data',
                            url: 'https://api.cloudinary.com/v1_1/doba9c1j8/video/upload',
                            data: data,
                            timeout: 0,
                            mimeType: "multipart/form-data",
                            contentType: false,
                            //cache: false,
                            processData:false,
                            success: function(response) {
                                    var objres = JSON.parse(response);
                                  //if(response.status == true) {
                                        $("#loadervideo").css("display","none");
                                        
                                        $("#videourl").val(objres.url);
                                        $("#publicid").val(objres.public_id);

                                        var videourl = objres.secure_url;
                                        var resimg = videourl.replace(".mp4", ".jpg");

                                        $("#videothumb").val(resimg);
                                        $("#blah").attr("src",resimg);
                                        
                                        $("#buttondisable").attr("disabled",false);
                                  //} else {
                                   //     $("#loadervideo").css("display","none");
                                  //}
                            },
                            error:function(jqXHR, textStatus, errorThrown){
                                 $("#loadervideo").css("display","none");
                            }
                      });
                  }

            });
      });
</script>