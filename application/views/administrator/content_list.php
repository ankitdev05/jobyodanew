

<?php include('header.php');?>

               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon-chart-bar-stacked s-6"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Content Management</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example scrollmobusr">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements dsymgnd">
                                             
                                                            
                                             
                                                                <table id="sample-data-table" class="table dataTable no-footer" role="grid" aria-describedby="sample-data-table_info" style="width: 855px;">
                                                <thead>
                                                   <tr role="row"><th class="secondary-text sorting_asc" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                                                         
                                                            ID
                                                         
                                                      : activate to sort column descending" style="width: 40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Sno</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Title</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Content</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Action
                                                         
                                                      : activate to sort column ascending" style="width: 68px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th></tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $x1 =1;
                                                    foreach($contentLists as $contentList){
                                                    ?>
                                                    <tr role="row" class="odd">
                                                      <td class="sorting_1"><?php echo $x1;?></td>
                                                      <td><?php echo $contentList['title'];?></td>
                                                      <td><?php echo substr($contentList['content'], 0,300);?></td>
                                                      
                                                      
                                                      <td>
                            
                             <a title="Edit" href="<?php echo base_url(); ?>administrator/recruiter/addstaticcontent?id=<?php echo base64_encode($contentList['id']) ?>" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon s-4 icon-pencil"></i></a>  
                             <a title="Delete"  href="<?php echo base_url(); ?>administrator/recruiter/deletecontent?id=<?php echo base64_encode($contentList['id']) ?>" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon s-4 icon-trash"></i></a>
                               
                                                      </td> 
                                                      
                                                   </tr>
                                                   <?php $x1++;
                                                   }?>
                                                   </tbody>
                                             </table>
                                                       
                                                    </div>
                                                </div>
                                                
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
           <!-- <a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>



   </body>
</html>
