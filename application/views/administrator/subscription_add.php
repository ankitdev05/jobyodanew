<style type="text/css">
    .validError{
        color: red;
    }
    .imgmsg{
      font-size: 13px;
      display: block;
      text-align: center;
      margin: 15px 0 0 0;
    }
    #register .form-wrapper {
        width: 60.4rem!important;
        max-width: 60.4rem!important;
        background: #FFFFFF;
        text-align: center;
    }
</style>
<?php include('header.php'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Add Plan</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/subscription_plan_create" method="post" novalidate enctype="multipart/form-data">


                                <div class="form-group mb-4">
                                    <input type="text" name="plan_name" value="<?php if(!empty($userData['plan_name'])){ echo $userData['plan_name']; } ?>" class="form-control" aria-describedby="nameHelp"  >
                                    <label>Plan name</label>
                                    <?php if(!empty($errors['plan_name'])){echo "<span class='validError'>".$errors['plan_name']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea rows="5" name="desc" id="content" class="form-control" maxlength="200"></textarea>
                                    
                                    <?php if(!empty($errors['desc'])){echo "<span class='validError'>".$errors['desc']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="duration" value="<?php if(!empty($userData['duration'])){ echo $userData['duration']; } ?>" class="form-control" aria-describedby="nameHelp"  >
                                    <label>Duration (In Months)</label>
                                    <?php if(!empty($errors['duration'])){echo "<span class='validError'>".$errors['duration']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="cost" value="<?php if(!empty($userData['cost'])){ echo $userData['cost']; } ?>" class="form-control" aria-describedby="nameHelp"  >
                                    <label>Cost</label>
                                    <?php if(!empty($errors['cost'])){echo "<span class='validError'>".$errors['cost']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="total_mail" value="<?php if(!empty($userData['total_mail'])){ echo $userData['total_mail']; } ?>" class="form-control" id="txtplaces" aria-describedby="nameHelp"  >
                                    <label>Total Mails</label>
                                    <?php if(!empty($errors['total_mail'])){echo "<span class='validError'>".$errors['total_mail']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="total_job_post" value="<?php if(!empty($userData['total_job_post'])){ echo $userData['total_job_post']; } ?>" class="form-control" id="txtplaces" aria-describedby="nameHelp"  >
                                    <label>Total Job Posting</label>
                                    <?php if(!empty($errors['total_job_post'])){echo "<span class='validError'>".$errors['total_job_post']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="total_resume_access" value="<?php if(!empty($userData['total_resume_access'])){ echo $userData['total_resume_access']; } ?>" class="form-control" id="txtplaces" aria-describedby="nameHelp"  >
                                    <label>Total Resume Access</label>
                                    <?php if(!empty($errors['total_resume_access'])){echo "<span class='validError'>".$errors['total_resume_access']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="total_video_post" value="<?php if(!empty($userData['total_video_post'])){ echo $userData['total_video_post']; } ?>" class="form-control" id="txtplaces" aria-describedby="nameHelp"  >
                                    <label>Total Video Post</label>
                                    <?php if(!empty($errors['total_video_post'])){echo "<span class='validError'>".$errors['total_video_post']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="total_advertise" value="<?php if(!empty($userData['total_advertise'])){ echo $userData['total_advertise']; } ?>" class="form-control" id="txtplaces" aria-describedby="nameHelp"  >
                                    <label>Total Advertisement</label>
                                    <?php if(!empty($errors['total_advertise'])){echo "<span class='validError'>".$errors['total_advertise']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="total_job_boost" value="<?php if(!empty($userData['total_job_boost'])){ echo $userData['total_job_boost']; } ?>" class="form-control" id="txtplaces" aria-describedby="nameHelp"  >
                                    <label>Total Job Boosting</label>
                                    <?php if(!empty($errors['total_job_boost'])){echo "<span class='validError'>".$errors['total_job_boost']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <select name="status" class="form-control" aria-describedby="nameHelp" />
                                        <option value=""> Select Status</option>
                                        <option value="1"> Active </option>
                                        <option value="2"> InActive </option>
                                    </select>
                                    <?php if(!empty($errors['status'])){echo "<span class='validError'>".$errors['status']."</span>";}?>
                                </div>

                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Submit
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
            

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
</body>

</html>


<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
      CKEDITOR.replace('content');
    </script>