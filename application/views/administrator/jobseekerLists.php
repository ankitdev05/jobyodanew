<?php include('header.php'); ?>
<div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="mainheadicos">
                                 <i class="icon s-4 icon-account-search"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">JobSeekers Listing</div>
                                 <div class="">Total: <?php if($Lists){echo count($Lists); }?></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <div class="col-auto">
                          <!-- <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>-->
                        </div>
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements">
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text" style="width:40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text" >
                                                         <div class="table-header">
                                                            <span class="column-title">Email</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Phone</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Registration Date</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Experience</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Education</span>
                                                         </div>
                                                      </th>
                                                      
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Location</span>
                                                         </div>
                                                      </th>
                                                     
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Nationality</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Superpower</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Platform</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">App Version</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Last Used</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Signup Via</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">App sign in bonus</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Bonus Count</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Withdraw</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Bonus History</span>
                                                         </div>
                                                      </th>
                                                   </tr>
                                                </thead>
                                                <tbody> 
                                                   <?php
                                                      $x1 =1;
                                                      foreach($Lists as $List) {
                                                      ?>
                                                   <tr>
                                                      <td style="width:30px;"><?php echo $x1;?></td>
                                                      <td style="width:40px;"><?php echo $List['name'];?></td>
                                                      <td style="width:40px;"><?php echo $List['email'];?></td>
                                                      <td style="width:40px;"><?php if($List['phone']!=0){ echo $List['phone'];}?></td>
                                                      <td style="width:40px;"><?php  echo date('Y-m-d',strtotime($List['created_at']));?></td>  
                                                      <td style="width:40px;"><?php if($List['exp_year']>1){ echo $List['exp_year'].' Years '; }else{ echo $List['exp_year'].' Year '; }  if($List['exp_month']>1){ echo $List['exp_month'].' Months '; }else{ echo $List['exp_month'].' Month '; } ?></td>
                                                      <td style="width:40px;"><?php echo $List['education'];?></td>
                                                      <td style="width:40px;"><?php echo $List['location'];?></td>
                                                      <td style="width:40px;"><?php echo $List['nationality'];?></td>
                                                      <td style="width:40px;"><?php echo $List['superpower'];?></td> 
                                                      <td style="width:40px;"><?php if(!empty($List['device_type'])){ if($List['device_type']=='ios'){  echo "iOS";} elseif ($List['device_type']=='android') {
                                                         echo "Android"; } } else{echo "Web";} ?></td>
                                                      <td style="width:40px;"><?php if(!empty($List['app_version'])){ echo $List['app_version'];}?></td>
                                                      <td style="width:40px;"><?php if(!empty($List['last_used'])){ echo $List['last_used'];}?></td>   
                                                      <td style="width:40px;"><?php if(!empty($List['type'])){ if($List['type']=='gmail'){ echo "Gmail";} elseif($List['type']=='facebook'){ echo "Facebook";} else{echo "Normal";}} ?></td>
                                                      
                                                      <td>
                                                         <a title="View" href="<?php echo base_url();?>administrator/JobSeeker/fetchJobseeker?id=<?php echo base64_encode($List['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon icon-eye s-4"></i></a> 
                                                         <?php if($List['active'] == 1){?>
                                                          <a title="Active" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getaid(this.id)" data-target="#activeModal" class="btn btn-icon usricos" ><i class="icon icon-checkbox-marked-circle-outline s-4"></i></a>               
                                                          <?php } else{ ?>
                                                           <a title="Block" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getbid(this.id)" data-target="#blockModal" class="btn btn-icon usricos" ><i class="icon icon-block-helper s-4"></i></a><?php } ?>
                                                         <a title="Delete" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" class="btn btn-icon usricos" ><i class="icon s-4 icon-trash"></i></a>
                                                      </td>
                                                      
                                                      
                                                      <td>
                                                         <a title="App Sign in bonus" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getBonus(this.id)" data-target="#modalBonus" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-login s-4"></i></a>
                                                      </td>
                                                      
                                                      <td style="width:40px;"><?php echo $List['countid'];?></td>
                                                      <td>
                                                         <a title="Withdraw" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getuid(this.id)" data-target="#myModal2" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-looks s-4"></i></a>
                                                      </td>
                                                      <td>
                                                         <a title="Bonus History" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getid2(this.id)" data-target="#myModal3" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-history s-4"></i></a>
                                                      </td>
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete JobSeeker</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to delete this JobSeeker?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="blockModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Block JobSeeker</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to block this JobSeeker?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createbLink">Block</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="activeModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Active JobSeeker</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to active this JobSeeker?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createaLink">Active</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal2" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Withdraw Bonus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="bonus_message"></p>
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-secondary" id="createwLink">Yes</a> 
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal3" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Bonus History</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body bonushstry">
                <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text" style="width:40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">S No</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text" >
                                                         <div class="table-header">
                                                            <span class="column-title">Bonus</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Added At</span>
                                                         </div>
                                                      </th>
                                                      
                                                   </tr>
                                                </thead>
                                                <tbody id="bonus_history"> 
                                                   <!-- <?php
                                                      $x1 =1;
                                                      foreach($Lists1 as $List) {
                                                      ?>
                                                   <tr>
                                                      <td style="width:20px;"><?php echo $x1;?></td>
                                                      <td style="width:20px;"><?php echo $List['name'];?></td>
                                                      <td style="width:20px;"><?php echo $List['bonus'];?></td>
                                                      <td style="width:20px;"><?php echo $List['added_at'];?></td>   
                                                      
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?> -->
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>

 <!-- Modal -->
<div id="modalBonus" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h3>Sign In Bonus</h3>
        <form method="post" action="<?php echo base_url();?>administrator/JobSeeker/bonusamountInsert">
            <div class="form-group">
                <input type="number" name="bonus_amount" class="form-control" placeholder="Enter Amount" required="">
                <label>Amount</label>
                <input type="hidden" name="userId" id="userId">
            </div>
            
            <button type="submit" class="btn btn-info">Submit</button>
            
        </form>

      </div>
      <div class="modal-footer">
         
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/deleteJobSeeker?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }
    function getbid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/jobseekerBlock?id="+rid;
        var aa = document.getElementById('createbLink');
        aa.setAttribute("href", link);
    }

    function getaid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/jobseekerActive?id="+rid;
        var aa = document.getElementById('createaLink');
        aa.setAttribute("href", link);
    }

    function getuid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/withdrawBonus?id="+rid;
        var aa = document.getElementById('createwLink');
        aa.setAttribute("href", link);
        $.ajax({
         type:"POST",
         url:"<?php echo base_url(); ?>/administrator/JobSeeker/fetchBonusAmount",
         data:{
            rid:rid
         },
         success:function(data){
            if(data>0){
               $('#bonus_message').html('Do you want to withdraw the Bonus Amount of'+ data +' Peso ?');
               $('#createwLink').show();
            }else{
               $('#bonus_message').html('You donot have sufficient balance to withdraw');
               $('#createwLink').hide();
            }
            
         }
        });
    }

    function getBonus(rid){
      var userId = document.getElementById('userId');
      userId.setAttribute("value",rid);
    }

    function getid2(id) {
        var rid = id;
        //alert(id);
          var url =   '<?php echo base_url(); ?>administrator/JobSeeker/fetchbonusHistory';
       
          $.ajax({
                   type:"POST",
                   url:url,
                   data:{
                       rid : rid
                   },

                   success:function(data)

                    {
                     //alert(data);
                     
                  $("#bonus_history").html(data);
                    }
                });
    }


</script>
   </body>
</html>

