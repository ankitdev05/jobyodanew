<style type="text/css">
    .validError{
        color: red;
    }
    .imgmsg{
      font-size: 13px;
      display: block;
      text-align: center;
      margin: 15px 0 0 0;
    }
</style>
<?php include('header.php'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Add Banner</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/advertiseInsert" method="post" novalidate enctype="multipart/form-data">

                                <div class="form-group mb-4">
                                    <label>Upload Banner</label>
                                    <input type="file" name="banner" class="form-control">
                                    
                                </div>
                                <?php if(!empty($errors['profilePic'])){echo "<span class='validError'>".$errors['profilePic']."</span>";}?>
                                <span class="imgmsg">*Image dimension should be between 400X200 - 450X250.</span>
                                <div class="form-group mb-4">
                                    <input type="text" name="location" value="<?php if(!empty($userData['location'])){ echo $userData['location']; } ?>" class="form-control" id="txtplaces" aria-describedby="nameHelp"  >
                                    <label>Location/City</label>
                                    <?php if(!empty($errors['location'])){echo "<span class='validError'>".$errors['location']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea rows="5" name="desc" class="form-control" maxlength="200"></textarea>
                                    
                                    <?php if(!empty($errors['desc'])){echo "<span class='validError'>".$errors['desc']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <select name="company" id="cname" class="form-control" aria-describedby="nameHelp" />
                                        <option value=""> Select Company</option>
                                    <?php
                                        foreach($company as $companys) {
                                    ?>
                                        <option value="<?php echo $companys['id'];?>"> <?php echo $companys['cname'];?></option>
                                    <?php
                                        }
                                    ?>
                                    </select>
                                    <?php if(!empty($errors['company'])){echo "<span class='validError'>".$errors['company']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <select name="site" id="site" class="form-control" aria-describedby="nameHelp" />
                                        <option value=""> Select Site</option>
                                    <!-- <?php
                                        foreach($company as $companys) {
                                    ?>
                                        <option value="<?php echo $companys['id'];?>"> <?php echo $companys['cname'];?></option>
                                    <?php
                                        }
                                    ?>
                                    </select>
                                    <?php if(!empty($errors['company'])){echo "<span class='validError'>".$errors['company']."</span>";}?> -->
                                    </select>
                                </div>

                                <!-- <div class="form-group mb-4">
                                    <input type="text" name="compid" id="compid" value="<?php if(!empty($userData['compid'])){ echo $userData['compid']; } ?>" class="form-control" aria-describedby="nameHelp" />
                                    <label>Company ID</label>
                                </div> -->

                                <div class="form-group mb-4">
                                    <input type="text" autocomplete="off" name="start_date" id="start_date" value="<?php if(!empty($userData['start_date'])){ echo $userData['start_date']; } ?>" class="form-control" aria-describedby="nameHelp" />
                                    <label>Start Date</label>
                                    <?php if(!empty($errors['start_date'])){echo "<span class='validError'>".$errors['start_date']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="text" autocomplete="off" id="end_date" value="<?php if(!empty($userData['end_date'])){ echo $userData['end_date']; } ?>" name="end_date" class="form-control" >
                                    <label>End Date</label>
                                    <?php if(!empty($errors['end_date'])){echo "<span class='validError'>".$errors['end_date']."</span>";}?>
                                </div>


                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Submit
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
</body>

</html>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&sensor=false&libraries=places&callback=initMap"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtplaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.A;
            var longitude = place.geometry.location.F;
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
        });
    });
    </script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
    $('#cname').change(function(){
        var cname = $('#cname').val();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "administrator/recruiter/getsite",
          data: {cname:cname},
          cache:false,
          success:function(htmldata){
   
            $("#site").html(htmldata);
              
          },
      });
    });
    /*$('#cname').change(function(){
        var cname = $('#cname').val();
        $('#compid').val(cname);
    })*/

   $(function(){  
        $.datepicker.setDefaults({  
            dateFormat: 'yy-mm-dd'   
        }); 
        $("#start_date").datepicker({minDate: "0" });  
        $("#end_date").datepicker({minDate: "0" }); 
        
   });
</script> 

