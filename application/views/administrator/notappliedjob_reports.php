<?php  $this->load->model('Jobpostadmin_Model');?>

<?php include('header.php');?>

               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon-chart-bar-stacked s-6"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Report Management</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example scrollmobusr">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements">
                                             <!-- JAVASCRIPT BEHAVIOR -->
                                   
                                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="day-tab" data-toggle="tab" href="#day" role="tab" aria-controls="day" aria-expanded="true">Daily</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="weekly-tab" data-toggle="tab" href="#weekly" role="tab" aria-controls="weekly">Weekly</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-controls="monthly">Monthly</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="yearly-tab" data-toggle="tab" href="#yearly" role="tab" aria-controls="yearly">Yearly</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="myTabContent">
                                                            <div class="tab-pane fade show active" id="day" role="tabpanel" aria-labelledby="day-tab">
                                                                <h3>Daily Report</h3>
                                                                <table id="sample-data-table" class="table">
                                                                <thead>
                                                                   <tr>
                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">SNo</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Job Title</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Company Name</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Site Name</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Experience</span>
                                                                         </div>
                                                                      </th>
                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Expiry Date</span>
                                                                         </div>
                                                                      </th>
                                                                      
                                                                   </tr>
                                                                </thead>
                                                                <tbody>
                                                                  <?php 
                                                                  $x1 =1;
                                                                  $date =  date('Y-m-d');
                                                                  foreach($jobLists as $jobList) {
                                                                    $compName = $this->Jobpostadmin_Model->site_name($jobList['parent_id']);
                                                                     if( date('Y-m-d', strtotime($jobList['created_at'])) == $date  ){?>
                                                                      <tr>
                                                                        <td style="width:40px;"><?php echo $x1; ?></td>
                                                                        <td style="width:100px;"><?php echo $jobList['jobtitle']; ?></td>
                                                                        <td style="width:100px;"><?php echo $compName[0]['cname']; ?></td>
                                                                        <td style="width:100px;"><?php echo $jobList['cname']; ?></td>
                                                                        <td style="width:70px;"><?php echo $jobList['experience']; ?></td>
                                                                        <td style="width:70px;"><?php echo $jobList['jobexpire']; ?></td>
                                                                      </tr>
                                                                    <?php $x1++;
                                                                     }
                                                                  
                                                                  }?>

                                                                </tbody>
                                                                </table>
                                                            </div>

                                                            <div class="tab-pane fade" id="weekly" role="tabpanel" aria-labelledby="weekly-tab">
                                                                <h3>Weekly Report</h3>
                                                                <table id="sample-data-tableweekly" class="table">
                                                                <thead>
                                                                   <tr>
                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">SNo</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Job Title</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Company Name</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Site Name</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Experience</span>
                                                                         </div>
                                                                      </th>
                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Expiry Date</span>
                                                                         </div>
                                                                      </th>
                                                                      
                                                                   </tr>
                                                                </thead>
                                                                <tbody>
                                                                  <?php 
                                                                  $x1 =1;
                                                                  $date =  date('Y-m-d');
                                                                  foreach($jobLists as $jobList) {
                                                                    $compName = $this->Jobpostadmin_Model->site_name($jobList['parent_id']);
                                                                     if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-7 days'))  ){?>
                                                                      <tr>
                                                                        <td style="width:40px;"><?php echo $x1; ?></td>
                                                                        <td style="width:100px;"><?php echo $jobList['jobtitle']; ?></td>
                                                                        <td style="width:100px;"><?php echo $compName[0]['cname']; ?></td>
                                                                        <td style="width:100px;"><?php echo $jobList['cname']; ?></td>
                                                                        <td style="width:70px;"><?php echo $jobList['experience']; ?></td><td style="width:70px;"><?php echo $jobList['jobexpire']; ?></td>
                                                                      </tr>
                                                                    <?php $x1++;
                                                                     }
                                                                  
                                                                  }?>

                                                                </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="monthly" role="tabpanel" aria-labelledby="monthly-tab">
                                                              <h3>Monthly Report</h3>
                                                                <table id="sample-data-tablemonthly" class="table">
                                                                <thead>
                                                                   <tr>
                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">SNo</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Job Title</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Company Name</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Site Name</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Experience</span>
                                                                         </div>
                                                                      </th>
                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Expiry Date</span>
                                                                         </div>
                                                                      </th>
                                                                      
                                                                   </tr>
                                                                </thead>
                                                                <tbody>
                                                                  <?php 
                                                                  $x1 =1;
                                                                  $date =  date('Y-m-d');
                                                                  foreach($jobLists as $jobList) {
                                                                    $compName = $this->Jobpostadmin_Model->site_name($jobList['parent_id']);
                                                                     if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-30 days'))  ){?>
                                                                      <tr>
                                                                        <td style="width:40px;"><?php echo $x1; ?></td>
                                                                        <td style="width:100px;"><?php echo $jobList['jobtitle']; ?></td>
                                                                        <td style="width:100px;"><?php echo $compName[0]['cname']; ?></td>
                                                                        <td style="width:100px;"><?php echo $jobList['cname']; ?></td>
                                                                        <td style="width:70px;"><?php echo $jobList['experience']; ?></td><td style="width:70px;"><?php echo $jobList['jobexpire']; ?></td>
                                                                      </tr>
                                                                    <?php $x1++;
                                                                     }
                                                                  
                                                                  }?>

                                                                </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="yearly" role="tabpanel" aria-labelledby="yearly-tab">
                                                                <h3>Yearly Report</h3>
                                                                <table id="sample-data-tableyearly" class="table">
                                                                <thead>
                                                                   <tr>
                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">SNo</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Job Title</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Company Name</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Site Name</span>
                                                                         </div>
                                                                      </th>

                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Experience</span>
                                                                         </div>
                                                                      </th>
                                                                      <th class="secondary-text">
                                                                         <div class="table-header">
                                                                            <span class="column-title">Expiry Date</span>
                                                                         </div>
                                                                      </th>
                                                                      
                                                                   </tr>
                                                                </thead>
                                                                <tbody>
                                                                  <?php 
                                                                  $x1 =1;
                                                                  $date =  date('Y-m-d');
                                                                  foreach($jobLists as $jobList) {
                                                                   
                                                                    $compName = $this->Jobpostadmin_Model->site_name($jobList['parent_id']);
                                                                    
                                                                     if(date('Y-m-d', strtotime($jobList['created_at'])) >= date('Y-m-d', strtotime('-365 days'))  ){?>
                                                                      <tr>
                                                                        <td style="width:40px;"><?php echo $x1; ?></td>
                                                                        <td style="width:100px;"><?php echo $jobList['jobtitle']; ?></td>
                                                                        <td style="width:100px;"><?php echo $compName[0]['cname']; ?></td>
                                                                        <td style="width:100px;"><?php echo $jobList['cname']; ?></td>
                                                                        <td style="width:70px;"><?php echo $jobList['experience']; ?></td><td style="width:70px;"><?php echo $jobList['jobexpire']; ?></td>
                                                                      </tr>
                                                                    <?php $x1++;
                                                                     }
                                                                  
                                                                  }?>

                                                                </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
           <!-- <a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#sample-data-table').DataTable({ 
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
         destroy:true,
     });$('#sample-data-tableweekly').DataTable({ 
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
         destroy:true,
     });$('#sample-data-tablemonthly').DataTable({ 
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
         destroy:true,
     });$('#sample-data-tableyearly').DataTable({ 
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
         destroy:true,
     });
  })
</script>

   </body>
</html>

