<?php include('header.php'); ?>


               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-person-box"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Pending Recruiters Listing</div>
                                 <div class="">Total: <?php if($Lists){echo count($Lists); }?></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <div class="col-auto">
                           <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>
                        </div>
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements reculsting pdrct"> 
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Company</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Email</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Registration Date</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Status</span>
                                                         </div>
                                                      </th>
                                                      <!-- <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Salary Status</span>
                                                         </div>
                                                      </th> -->
													  
													              <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th>
                                                      
                                                     <!--  <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Company Site</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Invoice</span>
                                                         </div>
                                                      </th> -->
													  
                                                     
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                      $x1 =1;
                                                      foreach($Lists as $List) {
                                                      ?>
                                                   <tr>
                                                      <td><?php echo $x1;?></td>
                                                      <td><?php echo $List['fname'] .' '.$List['lname'];?></td>
                                                      <td style="width:200px;"><?php echo $List['cname'];?></td>
                                                      <td><?php echo $List['email'];?></td>
                                                      <td><?php echo date('Y-m-d',strtotime($List['created_at'])); ;?></td>
                                                      <td><?php if($List['status'] == 1){ echo "Approved"; } else{?><a href="#" class="btn btn-icon pendbtn" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getpid(this.id)" data-target="#pendingModal" aria-label="Product details">Pending</i></a><?php } ?></td>
                                                      <!-- <td><?php if($List['salary_status'] == 1){ ?> <a href="#" class="btn btn-icon pendbtn" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getpid(this.id)" data-target="#pendingModal" aria-label="Product details">Active</i></a><?php } else{?><a href="#" class="btn btn-icon pendbtn" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getpid(this.id)" data-target="#pendingModal" aria-label="Product details">Inactive</i></a><?php } ?></td> -->
                                                      <!-- <td><?php if($List['active'] == 1){?><a title="Block" href="<?php echo base_url();?>administrator/recruiter/recruiterBlock?id=<?php echo $List['id'];?>" class="btn btn-icon" aria-label="Product details"><i class="icon icon-block-helper s-4"></i></a><?php } else{?><a title="Active" href="<?php echo base_url();?>administrator/recruiter/recruiterActive?id=<?php echo $List['id'];?>" class="btn btn-icon" aria-label="Product details"><i class="icon icon-checkbox-marked-circle-outline s-4"></i></a><?php } ?></td> -->
                                                      <td>
													  
            														 <a title="View Recruiter" href="<?php echo base_url();?>administrator/recruiter/recruiterview?id=<?php echo base64_encode($List['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>   
            														 
                                                       <a title="Edit" href="<?php echo base_url();?>administrator/recruiter/updateRecruiter?id=<?php echo base64_encode($List['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon icon-pencil s-4"></i></a>

            														 <a title="Delete" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-trash"></i></a>
														 
														 
                                                      </td>
                                                      
                                                      <!-- <td style="width:96px;">
													  
														 <a title="View Company" href="<?php echo base_url();?>administrator/recruiter/companysitelisting?id=<?php echo $List['id'];?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>  
														 	 
                                                      </td> -->
                                                      <!-- <td>
                                                         <a href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getrid1(this.id)" data-target="#myModal2" class="btn btn-block btn-secondary fuse-ripple-ready" aria-label="Product details">Invoice</a>
                                                      </td>  -->
                                                      
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Recruiter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="pendingModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Approve Recruiter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to approve this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createApprove">Approve</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal2" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="hired_list">
            <input type="hidden" name="recruiter_id" id="recruiter_id" value="">
            
            
        </div>
    </div>
</div>

<!-- <div class="modal" id="invoiceModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generate Invoice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="invoice_body">
                
               
            </div>
            <div class="modal-footer">
                <a href="#"  class="btn btn-secondary"  id="createLink1">Send Invoice</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div> -->


<script type="text/javascript">
    function getpid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/recruiterApprove?id="+rid;
        var aa = document.getElementById('createApprove');
        aa.setAttribute("href", link);
    }

    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/deletepRecruiter?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }

    function getrid1(id) {
        var rid = id;
          var url =   '<?php echo base_url(); ?>/administrator/recruiter/fetchhired';
       
          $.ajax({
                   type:"POST",
                   url:url,
                   data:{
                       rid : rid
                   },

                   success:function(data)

                    {
                     //alert(data);
                     
                  $("#hired_list").html(data);
                  $('#recruiter_id').val(rid);
                  
                    }
                });
    }

    function generate_invoice(){
      var amount = $('#amount').val();
      var recruiter_id = $('#recruiter_id').val();
      var url =   '<?php echo base_url(); ?>/administrator/recruiter/fetchhired1';
       var link = "<?php echo base_url();?>administrator/recruiter/pdfdetails?rid="+recruiter_id+"&amount="+amount;
        var aa = document.getElementById('createiLink');
        if(amount==""){
         return false;
        }
        //aa.setAttribute("href", link);
        else{
          window.open(link, "_blank");  
        }
       
          /*$.ajax({
                   type:"POST",
                   url:url,
                   data:{
                       rid : recruiter_id,
                       amount : amount
                   },

                   success:function(data)

                    {
                     $('#invoiceModal').modal('show');
                     $('#myModal2').modal('hide');
                     $("#invoice_body").html(data);
                     aa.setAttribute("href", link);
                     
                    }
                });*/
      
    }

    /*function generate_pdf(){
      var amount = $('#amount').val();
      var recruiter_id = $('#recruiter_id').val();
      var link = "<?php echo base_url();?>administrator/recruiter/pdfdetails?rid="+recruiter_id+"&amount="+amount;
        var aa = document.getElementById('createLink1');
        aa.setAttribute("href", link);
      
    }*/

    /*function printDiv() 
      {

        var divToPrint=document.getElementById('invoice_body');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

      }*/
</script>
   </body>
</html>

