<ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">
   <li class="subheader">
      <!--<span>APPS</span>-->
   </li>
   <li class="nav-item" role="tab" id="heading-dashboards">
      <a class="nav-link ripple" href="<?php echo base_url();?>administrator/dashboard">
      <i class="icon s-4 icon-tile-four"></i>
      <span>Dashboard</span>
      </a>
   </li>
   <li class="nav-item" role="tab" id="heading-ecommerce">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-ecommerce" href="#" aria-expanded="false" aria-controls="collapse-ecommerce">
      <i class="icon s-4 icon-person-box"></i>
      <span>Recruiters</span>
      </a>
      <ul id="collapse-ecommerce" class='collapse' role="tabpanel" aria-labelledby="heading-ecommerce" data-children=".nav-item">
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/recruiterList" data-url="index.html">
            <span>Company/Recruiters Listing</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/pendingrecruiters" data-url="index.html">
            <span>Pending Recruiters</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/companyList" data-url="index.html">
            <span>Site Listing</span>
            </a>
         </li>
         
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addrecruiter" data-url="index.html">
            <span>Add Recruiter</span>
            </a>
         </li>

         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/recruitertransfer" data-url="index.html">
            <span>Transfer Request</span>
            </a>
         </li>

         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/recruiter_testimonial" data-url="index.html">
            <span>Recruiter Testimonial</span>
            </a>
         </li>
         
      </ul>
   </li>

   <li class="nav-item" role="tab" id="heading-ecommerce">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-ecommerce1" href="#" aria-expanded="false" aria-controls="collapse-ecommerce">
      <i class="icon s-4 icon-briefcase"></i>
      <span>Job Post</span>
      </a>
      <ul id="collapse-ecommerce1" class='collapse' role="tabpanel" aria-labelledby="heading-ecommerce" data-children=".nav-item">
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/joblist" data-url="index.html">
            <span>Job Post Listing</span>
            </a>
         </li>

         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/jobexpirelist" data-url="index.html">
            <span>Job Post Expire Listing</span>
            </a>
         </li>

         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/boostjobs" data-url="index.html">
            <span>Boost Job Listing</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/jobnotificationlist" data-url="index.html">
            
            <span>JOB ALERTS</span>
            </a>
         </li>
      </ul>
   </li>
   
   <li class="nav-item" role="tab" id="heading-ecommerce">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-ecommerce2" href="#" aria-expanded="false" aria-controls="collapse-ecommerce">
     <i class="icon s-4 icon-account-search"></i>
      <span>Job Seekers</span>
      </a>
      <ul id="collapse-ecommerce2" class='collapse' role="tabpanel" aria-labelledby="heading-ecommerce" data-children=".nav-item">
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/jobseekerlist" data-url="index.html">
            <span>Job Seekers Listing</span>
            </a>
         </li>
      </ul>
   </li>

   <li class="nav-item" role="tab" id="heading-subscribe">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-subscribe" href="#" aria-expanded="false" aria-controls="collapse-subscribe">
     <i class="icon s-4 icon-account-search"></i>
      <span>Subscription</span>
      </a>
      <ul id="collapse-subscribe" class='collapse' role="tabpanel" aria-labelledby="heading-subscribe" data-children=".nav-item">
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/subscription_plans" data-url="index.html">
            <span>Subscription Plan</span>
            </a>
         </li>
      </ul>
   </li>

   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/apiversioning" data-url="index.html">
      <i class="icon s-4 icon-library-books"></i>
      <span>API Versioning</span>
      </a>
   </li>


   <!-- <li class="subheader">
      <span>PAGES</span>
   </li> -->
  <!--  <li class="nav-item" role="tab" id="heading-authentication">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-authentication" href="#" aria-expanded="false" aria-controls="collapse-authentication">
      <i class="icon s-4 icon-lock"></i>
      <span>Authentication</span>
      </a>
      <ul id="collapse-authentication" class='collapse' role="tabpanel" aria-labelledby="heading-authentication" data-children=".nav-item">
         
         <li class="nav-item">
            <a class="nav-link ripple" href="<?php echo base_url();?>administrator/admin/signup" data-url="index.html">
            <span>Register</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/admin/adminLists" data-url="index.html">
            <span>Admin Listing</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/admin/reset" data-url="index.html">
            <span>Change Password</span>
            </a>
         </li>
      </ul>
   </li> -->
    <li class="nav-item" role="tab" id="heading-authentication">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-authentication" href="#" aria-expanded="false" aria-controls="collapse-authentication">
      <i class="icon s-4 icon-chart-bar-stacked"></i>
      <span>Reports Management</span>
      </a>
      <ul id="collapse-authentication" class='collapse' role="tabpanel" aria-labelledby="heading-authentication" data-children=".nav-item">
         
         <li class="nav-item">
            <a class="nav-link ripple" href="<?php echo base_url();?>administrator/hiringreport" data-url="index.html">
            <span>Application Status List</span>
            </a>
         </li>

         <li class="nav-item">
            <a class="nav-link ripple" href="<?php echo base_url();?>administrator/screeningreport" data-url="index.html">
            <span>Screening Reports</span>
            </a>
         </li>
         
         <li class="nav-item">
            <a class="nav-link ripple" href="<?php echo base_url();?>administrator/notappliedreport" data-url="index.html">
            <span>Report of Not applied Jobs</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple" href="<?php echo base_url();?>administrator/userreport" data-url="index.html">
            <span>Report of Users</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple" href="<?php echo base_url();?>administrator/nonuserreport" data-url="index.html">
            <span>Report of Not Registered Users</span>
            </a>
         </li>
         
      </ul>
   </li>

   <!-- <li class="nav-item" role="tab" id="heading-authentication">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-advertise" href="#" aria-expanded="false" aria-controls="collapse-advertise">
      <i class="icon s-4 icon-chart-bar-stacked"></i>
      <span>Advertisement</span>
      </a>
      <ul id="collapse-advertise" class='collapse' role="tabpanel" aria-labelledby="heading-authentication" data-children=".nav-item">
         
         <li class="nav-item">
            <a class="nav-link ripple" href="<?php echo base_url();?>administrator/addprice" data-url="index.html">
            <span>Add Price</span>
            </a>
         </li>

      </ul>
   </li> -->

   
   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/hirecost" data-url="index.html">
      <i class="icon s-4 icon-chart-bar-stacked"></i>
      <span>Statement of Account Generation</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/storylist" data-url="index.html">
      <i class="icon s-4 icon-library-books"></i>
      <span>Success Stories</span>
      </a>
   </li>
   <li class="nav-item" role="tab" id="heading-level">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-level" href="#" aria-expanded="false" aria-controls="collapse-level">
      <i class="icon s-4 icon-briefcase-checked"></i>
      <span>Job Level Management</span>
      </a>
      <ul id="collapse-level" class='collapse' role="tabpanel" aria-labelledby="heading-level" data-children=".nav-item">
         <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addjobtitle" data-url="index.html">
     <i class="icon s-4 icon-briefcase-upload"></i>
      <span>Add Job Title</span>
      </a>
   </li>
         
   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addindustry" data-url="index.html">
      <i class="icon s-4 icon-hospital-building"></i>
      <span>Add Industry</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addjoblevel" data-url="index.html">
      <i class="icon s-4 icon-briefcase-checked"></i>
      <span>Add Job Level</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addjobcategory" data-url="index.html">
      <i class="icon icon-playlist-check"></i>
      <span>Add Job Category</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addjobsubcategory" data-url="index.html">
        <i class="icon icon-playlist-check"></i>
      <span>Add Job SubCategory</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addskills" data-url="index.html">
        <i class="icon icon-playlist-check"></i>
      <span>Add Job Skills</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addsuperpower" data-url="index.html">
        <i class="icon icon-playlist-check"></i>
      <span>Add Superpower</span>
      </a>
   </li>
         
      </ul>
   </li>
   

   <li class="nav-item" role="tab" id="heading-authentication2">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-authentication2" href="#" aria-expanded="false" aria-controls="collapse-authentication">
      <i class="icon s-4 icon-text-shadow"></i>
      <span>Content Management System</span>
      </a>
      <ul id="collapse-authentication2" class='collapse' role="tabpanel" aria-labelledby="heading-authentication2" data-children=".nav-item">
         <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addstaticcontent" data-url="index.html">
      <span>CMS</span>
      </a>
   </li>
        <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/contentlist" data-url="index.html">
      <span>Content List</span>
      </a>
   </li>
      </ul>
</li> 
   
   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/contactlist" data-url="index.html">
      <i class="icon s-4 icon-comment-question-outline"></i>
      <span>Jobseeker Queries</span>
      </a>
   </li>

   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/loadingscreen" data-url="index.html">
     <i class="icon s-4 icon-buffer"></i>
      <span>Loading Management Content</span>
      </a>
   </li>

   <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/reconciliation" data-url="index.html">
      <i class="icon s-4 icon-headphones-box"></i>
      <span>Auto Reconciliation</span>
      </a>
   </li>
<li class="nav-item" role="tab" id="heading-authentication1">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-authentication1" href="#" aria-expanded="false" aria-controls="collapse-authentication">
      <i class="icon s-4 icon-bell"></i>
      <span>Promotional Notification Management</span>
      </a>
      <ul id="collapse-authentication1" class='collapse' role="tabpanel" aria-labelledby="heading-authentication1" data-children=".nav-item">
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/notifications" data-url="index.html">
            
            <span>Send Notification</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/notificationlist" data-url="index.html">
            
            <span>Sent Notifications Listing</span>
            </a>
         </li>
      </ul>
</li> 
 

<li class="nav-item" role="tab" id="heading-authentication20">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-authentication20" href="#" aria-expanded="false" aria-controls="collapse-authentication">
      <i class="icon s-4 icon-bell"></i>
      <span>Referral Code Management</span>
      </a>
      <ul id="collapse-authentication20" class='collapse' role="tabpanel" aria-labelledby="heading-authentication20" data-children=".nav-item">
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addreferral" data-url="index.html">
            
            <span>Referral Code</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/referralreport" data-url="index.html">
            
            <span>Referral Report</span>
            </a>
         </li>
      </ul>
</li>   
<li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/invoice" data-url="index.html">
      <i class="icon s-4 icon-file-pdf-box"></i>
      <span>Invoice Generation Management</span>
      </a>
   </li> 

   <!-- <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/Recruiter/addtestimonials" data-url="index.html">
      <i class="icon s-4 icon-file-pdf-box"></i>
      <span>Testimonials</span>
      </a>
   </li>  -->
<li class="nav-item" role="tab" id="heading-authentication3">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-authentication3" href="#" aria-expanded="false" aria-controls="collapse-authentication">
      <i class="icon s-4 icon-text-shadow"></i>
      <span>Advertisement Banner</span>
      </a>
      <ul id="collapse-authentication3" class='collapse' role="tabpanel" aria-labelledby="heading-authentication2" data-children=".nav-item">
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/adadvertise" data-url="index.html">
            <span>Add Banner</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/advertisement" data-url="index.html">
               <span>Banner List</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple" href="<?php echo base_url();?>administrator/addprice" data-url="index.html">
            <span>Add Price</span>
            </a>
         </li>
      </ul>
</li>

<li class="nav-item" role="tab" id="heading-authentication3">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-authentication333" href="#" aria-expanded="false" aria-controls="collapse-authentication">
      <i class="icon s-4 icon-text-shadow"></i>
      <span>Video Advertisement</span>
      </a>
      <ul id="collapse-authentication333" class='collapse' role="tabpanel" aria-labelledby="heading-authentication2" data-children=".nav-item">
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/videoadvertise" data-url="index.html">
            <span>Add Video Advertisement</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/videoadvertisement" data-url="index.html">
               <span>Video Advertisement List</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple" href="<?php echo base_url();?>administrator/addvideoprice" data-url="index.html">
            <span>Add Price</span>
            </a>
         </li>
      </ul>
</li> 

<li class="nav-item" role="tab" id="heading-authentication3">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-authentication444" href="#" aria-expanded="false" aria-controls="collapse-authentication">
      <i class="icon s-4 icon-text-shadow"></i>
      <span>News / Updates</span>
      </a>
      <ul id="collapse-authentication444" class='collapse' role="tabpanel" aria-labelledby="heading-authentication2" data-children=".nav-item">
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/news" data-url="index.html">
            <span>Add News</span>
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link ripple " href="<?php echo base_url();?>administrator/newslist" data-url="index.html">
               <span>News List</span>
            </a>
         </li>
      </ul>
</li> 

<li class="nav-item" role="tab" id="heading-authentication4">
      <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-authentication4" href="#" aria-expanded="false" aria-controls="collapse-authentication4">
      <i class="icon s-4 icon-text-shadow"></i>
      <span>FAQs</span>
      </a>
      <ul id="collapse-authentication4" class='collapse' role="tabpanel" aria-labelledby="heading-authentication4" data-children=".nav-item">
         <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/addfaq" data-url="index.html">
      <span>Add FAQ</span>
      </a>
   </li>
        <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/faqlist" data-url="index.html">
      <span>FAQ List</span>
      </a>
   </li>
      </ul>
</li> 
 <li class="nav-item">
      <a class="nav-link ripple " href="<?php echo base_url();?>administrator/resetpassword" data-url="index.html">
      <i class="icon s-4 icon-key"></i>
      <span>Change Password</span>
      </a>
   </li>       
</ul>