<?php include('header.php'); ?>
<div class="content custom-scrollbar jobseekdres">
<div class="doc data-table-doc page-layout simple full-width">
				 <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                             <div class="mainheadicos">
                                <i class="icon s-4 icon-account-search"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Jobseeker Details</div>
                                 <!--<div class="">Total: <?php if($Lists){echo count($Lists); }?></div>-->
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <!--<div class="col-auto">
                           <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>
                        </div>-->
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
				
				  <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
							
                              <div class="col-12">
                                 <div class="example ">
								    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements notifds">
									<div class="col-12">
										<div id="register">

                        <div class="fullwhit">

                          
                              
                                <div class="form-group">
								  <div class="row">
								  <div class="col-md-3 col-xs-3">
									<div class="">
									    <span>Name: </span>
							        </div>		
								</div>
                                    <div class="col-md-9 col-xs-9">
									   <div class="">
									<span><?php if($users[0]['name']){echo $users[0]['name'];}?> </span>
									   </div>
									</div>
                                     </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="">
                                                <span>Email: </span>
                                            </div>    
                                        </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <span><?php if($users[0]['email']){echo $users[0]['email'];}?> </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="">
                                                <span>Phone: </span>
                                            </div>    
                                        </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <span><?php if($users[0]['phone']){echo $users[0]['phone'];}?> </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="">
                                                <span>Location: </span>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <span><?php if($users[0]['location']){echo $users[0]['location'];}?> </span>
                                                </div>
                                            </div>
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="">
                                                <span>Designation: </span>
                                                </div>
                                            </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <span><?php if($users[0]['designation']){echo $users[0]['designation'];}?> </span>
                                            </div>
                                        </div>    
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="">
                                                <span>Superpower: </span>
                                                </div>
                                            </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <span><?php if($users[0]['superpower']){echo $users[0]['superpower'];}?> </span>
                                            </div>
                                        </div>    
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="">
                                                <span>Nationality: </span>
                                                </div>
                                            </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <span><?php if($users[0]['nationality']){echo $users[0]['nationality'];}?> </span>
                                            </div>
                                        </div>    
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="">
                                                <span>Education: </span>
                                                </div>
                                            </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <span><?php if($users[0]['education']){echo $users[0]['education'];}?> </span>
                                            </div>
                                        </div>    
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="">
                                                <span>Total Experience: </span>
                                                </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="">
                                                <span><?php if($users[0]['exp_year']>1){echo $users[0]['exp_year'].' Years';}else{ echo $users[0]['exp_year'].' Year'; }?>  </span><span><?php if($users[0]['exp_month']>1){echo $users[0]['exp_month'].' Months';} else{ echo $users[0]['exp_month'].' Month'; }?>  </span>
                                            </div>
                                        </div>    
                                    </div>
                                    
                                </div>

                                <HR>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                           <h4 style="float:left;">Experience</h4>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                              <?php
                                 foreach($usersExps as $usersExp) {
                              ?> 
                                    <div class="expborder">
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-3">
                                                  <div class="">
                                                      <span>Title: </span>
                                                  </div>
                                              </div>      
                                              <div class="col-md-9">
                                                  <div class="">
                                                      <span><?php if($usersExp['title']){echo $usersExp['title'];}?> </span>
                                                  </div>      
                                              </div>
                                          </div>
                                      </div>
                                      
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-3">
                                                  <div class="">
                                                      <span>Company: </span>
                                                  </div>
                                              </div>      
                                              <div class="col-md-9">
                                                  <div class="">
                                                      <span><?php if($usersExp['company']){echo $usersExp['company'];}?> </span>
                                                  </div>
                                              </div>      
                                          </div>      
                                      </div>
                                      
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-3">
                                                  <div class="">
                                                      <span>Job Description: </span>
                                                  </div>
                                              </div>      
                                              <div class="col-md-9">
                                                  <div class="">
                                                      <span><?php if($usersExp['jobDesc']){echo $usersExp['jobDesc'];}?> </span>
                                                  </div>
                                              </div>      
                                          </div>      
                                      </div>
                                      
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-3">
                                                  <div class="">
                                                      <span>From: </span>
                                                  </div>
                                              </div>      
                                              <div class="col-md-9">
                                                  <div class="">
                                                      <span><?php if($usersExp['workFrom']){echo $usersExp['workFrom'];}?> </span>
                                                  </div>
                                              </div>
                                          </div>      
                                      </div>
                                      
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-3">
                                                  <div class=""><span>To: </span>
                                                  </div>
                                              </div>
                                              <div class="col-md-9">
                                                  <div class="">
                                                      <span><?php if($usersExp['workTo']){echo $usersExp['workTo'];}?> </span>
                                                  </div>
                                              </div>
                                          </div>
                                          
                                      </div>
									  
                                 </div>

                              <?php
                                 }
                              ?>
<HR>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                           <h4 style="float:left;">Education</h4>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div style="clear:both;"></div>
                              <?php
                                 foreach($usersEdus as $usersEdu) {
                              ?>
                                    <div class="eduborder">
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-3">
                                                  <div class="">
                                                      <span>Title: </span>
                                                  </div>
                                              </div>
                                              <div class="col-md-9">
                                                  <div class="">
                                                      <span><?php if($usersEdu['attainment']){echo $usersEdu['attainment'];}?> </span>
                                                  </div>
                                              </div>
                                          </div>
                                          
                                      </div>
                                      
                                      <div class="form-group">
                                          <div class="row">
                                            <div class="col-md-3">
                                                <div class="">
                                                    <span>Degree: </span>
                                                </div>
                                            </div>    
                                            <div class="col-md-9">
                                                <div class="">
                                                    <span><?php if($usersEdu['degree']){echo $usersEdu['degree'];}?> </span>
                                                </div>    
                                            </div>
                                          </div>
                                      </div>
                                      
                                      <div class="form-group">
                                          <div class="row">
                                            <div class="col-md-3">
                                                <div class="">
                                                    <span>University: </span>
                                                </div>
                                            </div>     
                                            <div class="col-md-9">
                                                <div class="">
                                                    <span><?php if($usersEdu['university']){echo $usersEdu['university'];}?> </span>
                                                </div>
                                            </div>
                                      </div>
                                      
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-3">
                                                  <div class="">
                                                      <span>Degree From: </span>
                                                  </div>
                                              </div>
                                              <div class="col-md-9">
                                                  <div class="">
                                                      <span><?php if($usersEdu['degreeFrom']){echo $usersEdu['degreeFrom'];}?> </span>
                                                  </div>
                                              </div> 
                                          </div>      
                                      </div>
                                      
                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-3">
                                                  <div class="">
                                                      <span>Degree To: </span>
                                                  </div>
                                              </div>
                                              <div class="col-md-9">
                                                  <div class="">
                                                      <span><?php if($usersEdu['degreeTo']){echo $usersEdu['degreeTo'];}?> </span>
                                                  </div>
                                              </div>      
                                          </div>
                                      </div>
                                    </div>
                              <?php
                                 }
                              ?><HR>
                                 <div class="form-group">
                                     <div class="row">
                                        <div class="col-md-12">
                                           <h4 style="float:left;">Self Assessment</h4>
                                        </div>
                                     </div>    
                                </div>
                                <div style="clear:both;"></div>
                                <?php if(!empty($usersassis[0]['verbal'])){?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="">
                                                <span>Verbal Communication skills: </span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="">     
                                                <div class="progressbar"> 
                                                <div class="progress" style="width: <?php if(isset($usersassis[0]['verbal'])){echo $usersassis[0]['verbal']*10;} else{ echo "0"; } ?>%;"><span> <?php if($usersassis[0]['verbal']){echo $usersassis[0]['verbal']*20;}else{echo "0";} ?>%</span>
                                                </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                <?php }?>

                                <?php if(!empty($usersassis[0]['written'])){?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="">
                                                <span>Written Communication skills: </span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="">
                                                <div class="progressbar"> <div class="progress" style="width: <?php if(isset($usersassis[0]['written'])){echo $usersassis[0]['written']*10;} else{ echo "0"; } ?>%;"><span> <?php if($usersassis[0]['written']){echo $usersassis[0]['written']*20;}else{echo "0";} ?>%</span></div> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } if(!empty($usersassis[0]['listening'])){?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="">
                                                <span>Listening skills: </span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="">
                                                <div class="progressbar"> <div class="progress" style="width: <?php if(isset($usersassis[0]['listening'])){echo $usersassis[0]['listening']*10;} else{ echo "0"; } ?>%;"><span> <?php if($usersassis[0]['listening']){echo $usersassis[0]['listening']*20;}else{echo "0";} ?>%</span></div> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } if(!empty($usersassis[0]['problem'])){?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="">
                                                <span>Problem solving skills: </span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="">
                                                <div class="progressbar"> <div class="progress" style="width: <?php if(isset($usersassis[0]['problem'])){echo $usersassis[0]['problem']*10;} else{ echo "0"; } ?>%;"><span> <?php if($usersassis[0]['problem']){echo $usersassis[0]['problem']*20;}else{echo "0";} ?>%</span></div> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <HR>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                           <h4 style="float:left;">Expert in industry</h4>
                                        </div>
                                    </div>
                                </div>
								
								
								
                                <div style="clear:both;"></div>
                                <div class="form-group">
                                    <div class="asses">
                                    
                                        <div class="col-md-12"> 
                                        <?php if($usersexperts){
                                           $userExpert = $usersexperts[0]['expert'];
                                           $userExpert = explode(",", $userExpert);
                                           $expert = "";
                                           foreach($userExpert as $expert) {
                                              if($expert=='1'){
                                                  $expert .=". Automative, ";
                                              }if($expert=='2'){
                                                  $expert .= ". banking and financial services, ";
                                              }if($expert=='3'){
                                                  $expert .= ". consumer electronics, ";
                                              }if($expert=='4'){
                                                  $expert .= ". healthcare and pharmaceuticals, ";
                                              }if($expert=='7'){
                                                  $expert .= ". technology, ";
                                              }if($expert=='5'){
                                                  $expert .= ". media and communication, ";
                                              }if($expert=='6'){
                                                  $expert .= ". retail and ecommerce, ";
                                              }if($expert=='8'){
                                                  $expert .= ". travel, transportation and tourism, ";
                                              }
                                        ?>
                                              <span><?php echo ucwords($expert); ?></span>
                                        <?php
                                           }}
                                        ?>
                                        </div>
                                    </div>    
                                </div>
								
                              <div style="clear:both;"></div>
							  <HR>
                                 <div class="form-group">
                                     <div class="row">
                                        <div class="col-md-12">
                                           <h4 style="float:left;">Skills</h4>
                                        </div>
                                     </div>    
                                </div>
                                <div style="clear:both;"></div>

                                <div class="form-group">
                                    <div class="cust">
                                    
                                        <div class="col-md-12">
                                            <?php foreach($usersskills as $skils){ ?>
                                            <span><?php echo $skils['skills'];?> </span>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                
                                
                        </div>
                    </div>

                </div>
									</div>
								 </div>
								 
								 </div>
								 </div>
								 </div>
							  </div>
							  
							</div>
						</div>
				  </div>
				  </div>
                  </div>  
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
</body>

</html>