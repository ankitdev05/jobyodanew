<style type="text/css">
    .validError{
        color: red;
    }
    .imgmsg{
      font-size: 13px;
      display: block;
      text-align: center;
      margin: 15px 0 0 0;
    }
</style>
<?php include('header.php'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Edit Video Advertisement</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/videoadvertiseUpdate" method="post" novalidate enctype="multipart/form-data">

                                <div class="form-group mb-4">
                                    <label>Upload Video</label>
                                    <input type="file" name="videoupload" class="form-control" id="imgInp">
                                    
                                </div>
                                <span class="imgmsg">*Video size upto 100 MB.</span>
                                <br>
                                <div class="form-group mb-4">
                                    <input type="hidden" name="id" value="<?php if(!empty($advertiseLists[0]['id'])){ echo $advertiseLists[0]['id']; } ?>">
                                    <input type="text" name="title" value="<?php if(!empty($advertiseLists[0]['title'])){ echo $advertiseLists[0]['title']; } ?>" class="form-control" aria-describedby="nameHelp" />
                                    <label>Title</label>
                                    
                                    <?php if(!empty($errors['title'])){echo "<span class='validError'>".$errors['title']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea rows="5" name="desc" class="form-control" maxlength="200"><?php if(!empty($advertiseLists[0]['description'])){ echo $advertiseLists[0]['description']; } ?></textarea>
                                    
                                    <?php if(!empty($errors['desc'])){echo "<span class='validError'>".$errors['desc']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" autocomplete="off" name="start_date" id="start_date" value="<?php if(!empty($advertiseLists[0]['start_date'])){ echo $advertiseLists[0]['start_date']; } ?>" class="form-control" aria-describedby="nameHelp" />
                                    <label>Start Date</label>
                                    <?php if(!empty($errors['start_date'])){echo "<span class='validError'>".$errors['start_date']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="text" autocomplete="off" id="end_date" value="<?php if(!empty($advertiseLists[0]['end_date'])){ echo $advertiseLists[0]['end_date']; } ?>" name="end_date" class="form-control" >
                                    <label>End Date</label>
                                    <?php if(!empty($errors['end_date'])){echo "<span class='validError'>".$errors['end_date']."</span>";}?>
                                </div>

                                <input type="hidden" name="videourl" value="" id="videourl">
                                <input type="hidden" name="videothumb" value="" id="videothumb">
                                <input type="hidden" name="publicid" value="" id="publicid">

                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Submit
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>


        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
</body>

</html>


<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">

   $(function(){  
        $.datepicker.setDefaults({  
            dateFormat: 'yy-mm-dd'   
        }); 
        $("#start_date").datepicker({minDate: "0" });  
        $("#end_date").datepicker({minDate: "0" }); 
        
   });
</script> 

<script type="text/javascript">
      $("document").ready(function() {
            $("#imgInp").on('change', function(e){
                  e.preventDefault();
                  // Get form
                  var data = new FormData();
                  data.append( 'file', $( '#imgInp' )[0].files[0] );
                  data.append( 'upload_preset', 'vv0sspbd');
                  
                  console.log('in jquery');

                  $.ajax({
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        url: 'https://api.cloudinary.com/v1_1/doba9c1j8/video/upload',
                        data: data,
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function(response) { 
                              var objres = JSON.parse(response);
                                  //if(response.status == true) {
                                        $("#loadervideo").css("display","none");
                                        
                                        $("#videourl").val(objres.url);
                                        $("#publicid").val(objres.public_id);

                                        var videourl = objres.secure_url;
                                        var resimg = videourl.replace(".mp4", ".jpg");

                                        $("#videothumb").val(resimg);
                                        $("#blah").attr("src",resimg);
                                        
                                        $("#buttondisable").attr("disabled",false);
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                              console.log(textStatus, errorThrown);
                        }
                  });

            });
      });
</script>