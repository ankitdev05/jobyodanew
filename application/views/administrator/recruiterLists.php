<?php include('header.php'); ?>


               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-person-box"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Company/Recruiter Listing</div>
                                 <div class="">Total: <?php if($Lists){echo count($Lists); }?></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <div class="col-auto">
                           <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>
                        </div>
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements reculsting"> 
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">SubRecruiter</span>
                                                         </div>
                                                      </th>
                                                      
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Assign Subscription</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Subscription</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Company</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Email</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Registration Date</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Status</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Active</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Salary Status</span>
                                                         </div>
                                                      </th>
                                         
                                                     <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th>
                                                      
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Company Site</span>
                                                         </div>
                                                      </th>

                                                      

                                                      <!-- <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Invoice</span>
                                                         </div>
                                                      </th> -->
                                         
                                                     
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                      $x1 =1;
                                                      foreach($Lists as $List) {

                                                         $getAssignPlan = $this->Subscription_Model->view_existing($List['id']);
                                                         if($getAssignPlan) {

                                                            $getPlan = $this->Subscription_Model->view($getAssignPlan[0]['plan_id']);

                                                            $haveSubscribed = "<span style='color:green'><b>".$getPlan[0]['plan_name']."</b></span>";

                                                         } else {
                                                            $haveSubscribed = "<span style='color:red'><b>"."Inactive"."</b></span>";
                                                         }

                                                      ?>
                                                   <tr>
                                                      <td><?php echo $x1;?></td>
                                                      
                                                      <td style="width:96px;">                                       
                                                       <a style="margin:0 0px;" title="View SubRecruiter" href="<?php echo base_url();?>administrator/recruiter/subrecruiterlist?id=<?php echo base64_encode($List['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>
                                                      </td>

                                                      <td style="width:96px;">                                       
                                                       <a style="margin:0 0px;" title="Assign Subscription Plan" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="setplanrecruiter(this.id)" data-target="#planModal" class="btn btn-icon usricos" aria-label="Product details"> <i class="icon s-4 icon-briefcase-checked"></i></a>
                                                      </td>

                                                      <td> <?php echo $haveSubscribed; ?> </td>

                                                      <td><?php echo $List['fname'] .' '.$List['lname'];?></td>
                                                      <td style="width:200px;"><?php echo $List['cname'];?></td>
                                                      <td><?php echo $List['email'];?></td>
                                                      <td><?php echo date('Y-m-d',strtotime($List['created_at'])); ;?></td>
                                                      <td><?php if($List['status'] == 1){ echo "Approved"; } else{?><a  href="<?php echo base_url();?>administrator/recruiter/recruiterApprove?id=<?php echo $List['id'];?>" class="btn btn-icon pendbtn" aria-label="Product details">Pending</i></a><?php } ?></td>
                                                      <td><?php if($List['active'] == 1){?>
                                                      <a title="Block" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getbid(this.id)" data-target="#blockModal" class="btn btn-icon usricos" aria-label="Product details"><i class="icon icon-block-helper s-4"></i></a>
                                                      <?php } else{?><a title="Active" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getaid(this.id)" data-target="#activeModal" class="btn btn-icon usricos" aria-label="Product details"><i class="icon icon-checkbox-marked-circle-outline s-4"></i></a>
                                                      <?php } ?></td>
                                                      <td><?php if($List['salary_status'] == 1){?><a title="Active" href="#" class="btn btn-icon" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getsbid(this.id)" data-target="#pendingSalaryModal" aria-label="Product details"><i class="btn btn-icon" aria-label="Product details"><i class="icon icon-checkbox-marked-circle-outline s-4"></i></a><?php } 
                                                      else{?><a title="Block" href="#" class="btn btn-icon" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getsid(this.id)" data-target="#approveSalaryModal" aria-label="Product details"><i class="btn btn-icon" aria-label="Product details"><i class="icon icon-block-helper s-4"></i></a><?php } ?></td>
                                                      <td>
                                         
                                                          <a style="margin:0 0px;" title="View Recruiter" href="<?php echo base_url();?>administrator/recruiter/recruiterview?id=<?php echo base64_encode($List['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>   
                                           
                                                         <a style="margin:0 0px;" title="Edit" href="<?php echo base_url();?>administrator/recruiter/updateRecruiter?id=<?php echo base64_encode($List['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon icon-pencil s-4"></i></a>

                                                          <a style="margin:0 0px;" title="Delete" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-trash"></i></a>
                                           
                                           
                                                      </td>
                                                      
                                                      <td style="width:96px;">
                                         
                                                         <a  style="margin:0 0px;" title="View Company" href="<?php echo base_url();?>administrator/recruiter/companysitelisting?id=<?php echo base64_encode($List['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-eye"></i></a>  
                                              
                                                      </td>

                                                      
                                                      <!-- <td>
                                                         <a style="margin:0 0px;" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getrid1(this.id)" data-target="#myModal2" class="btn btn-block btn-secondary fuse-ripple-ready" aria-label="Product details">Invoice</a>
                                                      </td>  -->
                                                      
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Recruiter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="blockModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Block Recruiter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to block this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createbLink">Block</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="planModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>/administrator/subscription/subscribed">
               <div class="modal-header">
                   <h5 class="modal-title">Assign Subscription</h5>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true">×</span>
                   </button>
               </div>
               <div class="modal-body">
                  
                  <div class="form-group mb-4">
                     <select name="plan_action" onchange="planactiondropdown(this.value)" class="form-control" aria-describedby="nameHelp" />
                         <option value=""> Select action </option>
                         <option value="new"> New Plan </option>
                         <option value="update"> Update Plan </option>
                         <option value="renew"> Renew Plan </option>
                     </select>
                  </div>

                  <div class="form-group mb-4" id="planhideshow" style="display:none;">
                     <select name="plan" class="form-control" onchange="plandropdown(this.value)" aria-describedby="nameHelp" />
                         <option value=""> Select Plan </option>
                         <?php
                           if($subscriptions) {

                              foreach($subscriptions as $subscription) {
                         ?>
                                 <option value="<?php echo $subscription['id']; ?>"> <?php echo $subscription['plan_name']; ?> </option>
                         <?php
                              }
                           }
                         ?>
                     </select>
                     <span class="planerror"></span>
                  </div>

                  <div class="form-group mb-4">
                     <label>Duration (In Months)</label>
                     <input type="text" name="duration" value="" id="durationValue" class="form-control md-has-value" aria-describedby="nameHelp" required="">
                     <span class="durationerror"></span>
                  </div>

                  <div class="form-group mb-4">
                     <label>Cost</label>
                     <input type="text" name="cost" value="" id="costValue" class="form-control md-has-value" aria-describedby="nameHelp" required="">
                     <span class="costerror"></span>
                  </div>

                  <div class="form-group mb-4">
                        <label>Total Mails</label>
                        <input type="text" name="total_mail" value="" id="mailValue" class="form-control" aria-describedby="nameHelp" required="">
                        <span class="mailerror"></span>
                  </div>

                  <div class="form-group mb-4">
                        <label>Total Job Posting</label>
                        <input type="text" name="total_job_post" value="" id="jobValue" class="form-control" aria-describedby="nameHelp" required="">
                        <span class="joberror"></span>
                  </div>

                  <div class="form-group mb-4">
                        <label>Total Resume Access</label>
                        <input type="text" name="total_resume_access" value="" id="resumeValue" class="form-control" aria-describedby="nameHelp" required="">
                        <span class="resumeerror"></span>
                  </div>

                  <div class="form-group mb-4">
                        <label>Total Video Post</label>
                        <input type="text" name="total_video_post" value="" id="videoValue" class="form-control" aria-describedby="nameHelp" required="">
                        <span class="videoerror"></span>
                  </div>

                  <div class="form-group mb-4">
                        <label>Total Advertisement</label>
                        <input type="text" name="total_advertise" value="" id="advertiseValue" class="form-control" aria-describedby="nameHelp" required="">
                        <span class="advertiserror"></span>
                  </div>

                  <div class="form-group mb-4">
                     <label>Total Job Boosting</label>
                     <input type="text" name="total_job_boost" value="" id="boostValue" class="form-control" aria-describedby="nameHelp" required="">
                     <span class="boosterror"></span>
                  </div>
                  <input type="hidden" id="getrecruterid" name="recruiter_id" value="">
               </div>
               <div class="modal-footer">
                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                   <button type="submit" class="btn btn-primary" id="createbLink">Save</button>
               </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="activeModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Active Recruiter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to active this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createaLink">Active</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal2" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="hired_list">
            <input type="hidden" name="recruiter_id" id="recruiter_id" value="">
            
            
        </div>
    </div>
</div>

<div class="modal" id="approveSalaryModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Approve Salary</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to active salary for this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="salaryApprove">Approve</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="pendingSalaryModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inactive Salary</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to inactive salary for this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="salaryBlock">Inactive</a>
            </div>
        </div>
    </div>
</div>
<!-- <div class="modal" id="invoiceModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generate Invoice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="invoice_body">
                
               
            </div>
            <div class="modal-footer">
                <a href="#"  class="btn btn-secondary"  id="createLink1">Send Invoice</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div> -->


<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/deleteRecruiter?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }

    function getbid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/recruiterBlock?id="+rid;
        var aa = document.getElementById('createbLink');
        aa.setAttribute("href", link);
    }

    function getaid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/recruiterActive?id="+rid;
        var aa = document.getElementById('createaLink');
        aa.setAttribute("href", link);
    }

    function getsid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/salaryApprove?id="+rid;
        var aa = document.getElementById('salaryApprove');
        aa.setAttribute("href", link);
    }

    function getsbid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/salaryBlock?id="+rid;
        var aa = document.getElementById('salaryBlock');
        aa.setAttribute("href", link);
    }
    function getrid1(id) {
        var rid = id;
          var url =   '<?php echo base_url(); ?>/administrator/recruiter/fetchhired';
       
          $.ajax({
                   type:"POST",
                   url:url,
                   data:{
                       rid : rid
                   },

                   success:function(data)

                    {
                     //alert(data);
                     
                  $("#hired_list").html(data);
                  $('#recruiter_id').val(rid);
                  
                    }
                });
    }

    function generate_invoice(){
      var amount = $('#amount').val();
      var recruiter_id = $('#recruiter_id').val();
      var url =   '<?php echo base_url(); ?>/administrator/recruiter/fetchhired1';
       var link = "<?php echo base_url();?>administrator/recruiter/pdfdetails?rid="+recruiter_id+"&amount="+amount;
        var aa = document.getElementById('createiLink');
        if(amount==""){
         return false;
        }
        //aa.setAttribute("href", link);
        else{
          window.open(link, "_blank");  
          $('#myModal2').modal('hide');
        }
       
          /*$.ajax({
                   type:"POST",
                   url:url,
                   data:{
                       rid : recruiter_id,
                       amount : amount
                   },

                   success:function(data)

                    {
                     $('#invoiceModal').modal('show');
                     $('#myModal2').modal('hide');
                     $("#invoice_body").html(data);
                     aa.setAttribute("href", link);
                     
                    }
                });*/
      
    }

    /*function generate_pdf(){
      var amount = $('#amount').val();
      var recruiter_id = $('#recruiter_id').val();
      var link = "<?php echo base_url();?>administrator/recruiter/pdfdetails?rid="+recruiter_id+"&amount="+amount;
        var aa = document.getElementById('createLink1');
        aa.setAttribute("href", link);
      
    }*/

    /*function printDiv() 
      {

        var divToPrint=document.getElementById('invoice_body');

        var newWin=window.open('','Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

        newWin.document.close();

        setTimeout(function(){newWin.close();},10);

      }*/


   function setplanrecruiter(id) {
      var rid = id;
      $("#getrecruterid").val(rid);
   }

   function planactiondropdown(id) {
      console.log(id);
      var aid = id;
      var rid = $('#getrecruterid').val();
      
      if(aid == "update") {

         $('#planhideshow').css('display', 'none');
         var url =  '<?php echo base_url(); ?>/administrator/subscription/ajaxexistingplan';

         $.ajax({
               type:"POST",
               url:url,
               data:{
                  rid : rid,
               },
               success:function(data) {
                  
                     result = JSON.parse(data);
                  
                  if(result != null) {

                     $('#durationValue').val(result.duration);
                     $('#costValue').val(result.cost);
                     $('#mailValue').val(result.total_mail);
                     $('#jobValue').val(result.total_job_post);
                     $('#resumeValue').val(result.total_resume_access);
                     $('#videoValue').val(result.total_video_post);
                     $('#advertiseValue').val(result.total_advertise);
                     $('#boostValue').val(result.total_job_boost);
                  
                  } else {

                     alert("No Plan Found");
                  }
               }
         });

      } else if(aid == "renew" || aid == "new") {

         $('#planhideshow').css('display', 'block');
      }

   }

   function plandropdown(id) {
      var pid = id;
      var url =  '<?php echo base_url(); ?>/administrator/subscription/ajaxplan';
       
      $.ajax({
            type:"POST",
            url:url,
            data:{
               pid : pid
            },
            success:function(data) {
               console.log(data);
               result = JSON.parse(data);

               $('#durationValue').val(result.duration);
               $('#costValue').val(result.cost);
               $('#mailValue').val(result.total_mail);
               $('#jobValue').val(result.total_job_post);
               $('#resumeValue').val(result.total_resume_access);
               $('#videoValue').val(result.total_video_post);
               $('#advertiseValue').val(result.total_advertise);
               $('#boostValue').val(result.total_job_boost);
            }
      });
   }
</script>
   </body>
</html>

