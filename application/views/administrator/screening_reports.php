

<?php include('header.php');?>
<style type="text/css">
  div.dataTables_wrapper {
max-width: 1300px;
overflow-x: scroll;
}
</style>
               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon-chart-bar-stacked s-6"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Report Management</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example scrollmobusr">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements dsymgnd">
                                             <!-- JAVASCRIPT BEHAVIOR -->
                                   
                                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link <?php if(empty($_GET['status'])){ echo "active"; }else{ echo ""; } ?> " id="day-tab" data-toggle="tab" href="#day" role="tab" aria-controls="day" aria-expanded="true">Daily</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="weekly-tab" data-toggle="tab" href="#weekly" role="tab" aria-controls="weekly">Weekly</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-controls="monthly">Monthly</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link <?php if(!empty($_GET['status'])){ echo "active"; }else{ echo ""; } ?>" id="yearly-tab" data-toggle="tab" href="#yearly" role="tab" aria-controls="yearly">Yearly</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="date-tab" data-toggle="tab" href="#date" role="tab" aria-controls="date">DateWise</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="myTabContent">
                                                            <div class="tab-pane fade  <?php if(empty($_GET['status'])){ echo "show active"; }else{ echo ""; } ?>" id="day" role="tabpanel" aria-labelledby="day-tab">
															                                 <h3>Daily Report </h3>
															 
                                                              
                                                                <table id="sample-data-table" class="table">
                                                                
                                                                </table>
                                                            </div>

                                                            <div class="tab-pane fade" id="weekly" role="tabpanel" aria-labelledby="weekly-tab">
															                               <h3>Weekly Report</h3>
                                                                
                                                                <table id="sample-data-tableweekly" class="table">
                                                                
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="monthly" role="tabpanel" aria-labelledby="monthly-tab">
															                                 <h3>Monthly Report</h3>
                                                            
                                                                <table id="sample-data-tablemonthly" class="table">
                                                                
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade <?php if(!empty($_GET['status'])){ echo "show active"; }else{ echo ""; } ?>" id="yearly" role="tabpanel" aria-labelledby="yearly-tab">
                                                                <h3>Yearly Report</h3>
                                                                
                                                                <table id="sample-data-tableyearly" class="table">
                                                                
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="date" role="tabpanel" aria-labelledby="date-tab">
                                                            <h3>Datewise Report</h3>
															 <div class="row" style="margin-bottom:15px;"> 
                                                              <div class="col-md-3">  
                                                               <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                                                              </div>  
                                                              <div class="col-md-3">  
                                                                 <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                                                              </div> 
															 
															  <div class="col-md-3">  
															  
															   </div>
															   
                                                              <div class="col-md-3">  
                                                               <input type="button" name="filter" id="filter" value="Filter" class="btn btn-info" />  
                                                            </div>
															
															 </div>
                                                                <table id="sample-data-tabledate" class="table">
                                                                
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
           <!-- <a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>



   </body>
</html>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
  <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
      <!-- / JAVASCRIPT -->
<script type="text/javascript">
  $('#status_dropdown').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatus";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-table").html(data);
      $('#sample-data-table').DataTable({ 
        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
        ordering: false,
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
         destroy:true,
     });
        }
      });
  })

  $('#status_dropdown_weekly').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatusweek";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-tableweekly").html(data);
      $('#sample-data-tableweekly').DataTable({ 
        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
        ordering: false,
        destroy: true, //use for reinitialize datatable
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],

     });
        }
      });
  })
  $('#status_dropdown_monthly').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatusmonth";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-tablemonthly").html(data);
      $('#sample-data-tablemonthly').DataTable({ 
        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
        ordering: false,
        destroy: true, //use for reinitialize datatable
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
     });
        }
      });
  })
  $('#status_dropdown_yearly').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatusyear";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-tableyearly").html(data);
      $('#sample-data-tableyearly').DataTable({ 
        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
        ordering: false,
        destroy: true,
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
     });
        }
      });
  })
    
</script>

<script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker({maxDate: "0" });  
                $("#to_date").datepicker({maxDate: "0" }); 
                $('#to_date').datepicker('setDate', 'today');
                $('#from_date').datepicker('setDate', 'today-30');
 
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                var status = $('#status_dropdown_date').val();
                if(from_date == '' )  
                {  
                   alert("Please Select From Date");   
                } else if(to_date == '' )  
                {  
                   alert("Please Select To Date");   
                } else if(status == '' )  
                {  
                   alert("Please Select Status");   
                }   
                else  
                {  
                     $.ajax({  
                          url:"<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatusDate",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date, status:status},  
                          success:function(data)  
                          {  
                              $("#sample-data-tabledate").html(data);
                              $('#sample-data-tabledate').DataTable({
                              "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],  
                              dom: 'lBfrtip',
                              buttons: [
                                  'excel', 'csv'
                                 ],
                                 destroy : true,
                              }); 
                          }  
                     });  
                }  
           });  
      });  

      $(document).ready(function(){
    var status = $('#status_dropdown').val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatus";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-table").html(data);
      $('#sample-data-table').DataTable({ 
        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
        ordering: false,
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
         destroy:true,
     });
        }
      });
  })

      $(document).ready(function(){
    var status = $('#status_dropdown_weekly').val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatusweek";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-tableweekly").html(data);
      $('#sample-data-tableweekly').DataTable({ 
        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
        ordering: false,
        destroy: true, //use for reinitialize datatable
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
     });
        }
      });
  })

      $(document).ready(function(){
    var status = $('#status_dropdown_monthly').val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatusmonth";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-tablemonthly").html(data);
      $('#sample-data-tablemonthly').DataTable({ 
        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
        ordering: false,
        destroy: true, //use for reinitialize datatable
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
     });
        }
      });
  })
  $(document).ready(function(){
    var status = $('#status_dropdown_yearly').val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatusyear";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-tableyearly").html(data);
      $('#sample-data-tableyearly').DataTable({ 
        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
        ordering: false,
        destroy: true,
        dom: 'lBfrtip',
         buttons: [
          'excel', 'csv'
         ],
     });
        }
      });
  })


  $(document).ready(function(){    
      var status = $('#status_dropdown_date').val();
       
     $.ajax({  
          url:"<?php echo base_url(); ?>administrator/jobpost/getScreenDatabyStatusmonth",  
          method:"POST",  
          data:{status:status},  
          success:function(data)  
          {  
              $('#from_date').val();
              $("#sample-data-tabledate").html(data);
              $('#sample-data-tabledate').DataTable({
                "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
                ordering: false,
              dom: 'lBfrtip',
              buttons: [
                  'excel', 'csv'
                 ],
                 destroy : true,
              }); 
          }  
     });  
    
 }); 
 </script>