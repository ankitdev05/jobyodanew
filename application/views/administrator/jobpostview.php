<?php include('header.php'); ?>
<style type="text/css">
  .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.myprokhg.jobviewsd p.showexps {
    float: left;
    margin-right: 30px;
    width: 50%;
}

</style>		 
               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon-cube-outline s-6"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Job Post View</div>
                                 <!--<div class="">Total: <?php if($jobLists){echo count($jobLists); }?></div>-->
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                   <div class="col-sm-12">
									
									<!--<div class="myrecruimga">
										<img class="" src="http://mobuloustech.com/jobyodhaa/adminfiles/assets/images/avatars/profile.jpg">
									</div> -->
									
										<div class="myprokhg jobviewsd">
										
										<div class="row">
											<div class="col-3">
												<label>Job Title:<label>
											</div>
											<div class="col-6">
												<p><?php if($getJobs[0]['jobtitle']){echo $getJobs[0]['jobtitle'];}?></p>
											</div>
										</div>
										
										<div class="row">
											<div class="col-3">
												<label>No. of openings:<label>
											</div>
											<div class="col-6">
												<p>	<?php if($getJobs[0]['opening']){echo $getJobs[0]['opening'];}?></p>
											</div>
										</div>
										
										<div class="row">
											<div class="col-3">
												<label>Experience Required:<label>
											</div>
											<div class="col-6">
												<p class="showexps"><?php if($getJobs[0]['experience']){echo $getJobs[0]['experience'];}?></p>
                        <label class="switch" style="width: 66px;">
                            <input type="checkbox" name="experience_status" <?php if(!empty($getJobs[0]['experience_status'])){ if($getJobs[0]['experience_status']=="1"){ echo "checked"; } else{ echo ""; } } ?>>
                            <span class="slider round" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                       </label>
											</div>
										</div>
										
										<div class="row">
											<div class="col-3">
												<label>Salary Offered:<label>
											</div>
											<div class="col-6">
												<p><?php if(!empty($jobExps[0]['basicsalary'])){echo $jobExps[0]['basicsalary'];}?></p>
											</div>
										</div>
										
										<!-- <div class="row">
											<div class="col-3">
												<label>Industry:<label>
											</div>
											<div class="col-6">
												<p><?php if($getJobs[0]['industry']){ echo( $industryLists[0]['name']); 
												//echo $industryLists['name']; 
												}?></p>
											</div>
										</div> -->
										
										<div class="row">
											<div class="col-3">
												<label>Level:<label>
											</div>
											<div class="col-6">
												<p class="showexps"><?php if($getJobs[0]['level']){ echo $levels[0]['level']; }?></p>
                        <label class="switch" style="width:66px;">
                            <input type="checkbox" name="level_status" <?php if(!empty($getJobs[0]['level_status'])){ if($getJobs[0]['level_status']=="1"){ echo "checked"; } else{ echo ""; } } ?> >
                            <span class="slider round" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                       </label>
											</div>
										</div>
										
										<div class="row">
											<div class="col-3">
												<label>Language:<label>
											</div>
											<div class="col-6">
												<p><?php if($getJobs[0]['language']){ echo $langs[0]['name']; }?></p>
											</div>
										</div>
										
										<div class="row">
											<div class="col-3">
												<label>Job Location:<label>
											</div>
											<div class="col-6">
												<p><?php if($getJobloc[0]['address']){ echo $getJobloc[0]['address']; } ?></p>
											</div>
										</div>
										
										<div class="row">
											<div class="col-3">
												<label>Job Description:<label>
											</div>
											<div class="col-6">
												<p><?php if($getJobs[0]['jobDesc']){ echo $getJobs[0]['jobDesc']; }?></p>
											</div>
										</div>
										
										<div class="row">
											<div class="col-3">
												<label>Skills:<label>
											</div>
											<div class="col-6">
												<p><?php if($jobskills){ foreach ($jobskills as $skills) {
                          echo $skills['skill']; echo ", ";
                        } }?></p>
											</div>
										</div>
										
										<div class="row">
											<div class="col-3">
												<label>Education:<label>
											</div>
											<div class="col-6">
												<p class="showexps"><?php if($getJobs[0]['education']){ echo $getJobs[0]['education'];} ?></p>
                        <label class="switch" style="width:66px;">
                            <input type="checkbox" name="education_status" <?php if(!empty($getJobs[0]['education_status'])){ if($getJobs[0]['education_status']=="1"){ echo "checked"; } else{ echo ""; } } ?>>
                            <span class="slider round" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                       </label>
											</div>
										</div>
										
										<div class="row">
											<div class="col-3">
												<label>Job Expire:<label>
											</div>
											<div class="col-6">
												<p><?php if($getJobs[0]['jobexpire']){ echo date("d-m-Y", strtotime($getJobs[0]['jobexpire']));  }?></p>
											</div>
										</div>
										
										<!-- <div class="row">
											<div class="col-3">
												<label>Company Details:<label>
											</div>
											<div class="col-6">
												<p><?php if($getJobs[0]['companydetail']){ echo $getJobs[0]['companydetail']; }?></p>
											</div>
										</div> -->
										
										</div>
										
									
							  
										
										<div class="formbtncenter">
                                                            <a href="<?php echo base_url();?>/administrator/Jobpost/index" class="btn btn-primary fuse-ripple-ready">Back</a>
										</div>
									</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
           <!-- <a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Job Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/jobpost/jobpostdelete?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }
</script>

<style>

</style>
   </body>
</html>

