<style type="text/css">
  .validError {
     color: red;
     }

#myModal11 .modal-dialog{ width: 500px }
#myModal11 .modal-dialog .modal-body{
    text-align: center;
}
#myModal11 .modal-dialog .modal-body h4{
    text-align: center;
    margin: 0 0 10px;
    font-weight: 500;
    color: #000;
}
#myModal11 .modal-dialog .modal-body button.close{
    position: absolute;
    right: -15px;
    top: -15px;
    width: 30px;
    height: 30px;
    background-color: #fff;
    opacity: 1;
    border-radius: 50%;
    font-size: 16px;
}
#myModal11 .modal-dialog .modal-body p{
    font-size: 16px;
}
#myModal11 .modal-dialog .modal-body form button.Submit{
    padding: 0 30px;
    box-shadow: none;
}


#myModal22 .modal-dialog{ width: 500px }
#myModal22 .modal-dialog .modal-body{
    text-align: center;
}
#myModal22 .modal-dialog .modal-body h4{
    text-align: center;
    margin: 0 0 10px;
    font-weight: 500;
    color: #000;
}
#myModal22 .modal-dialog .modal-body button.close{
    position: absolute;
    right: -15px;
    top: -15px;
    width: 30px;
    height: 30px;
    background-color: #fff;
    opacity: 1;
    border-radius: 50%;
    font-size: 16px;
}
#myModal22 .modal-dialog .modal-body p{
    font-size: 16px;
}
#myModal22 .modal-dialog .modal-body form button.Submit{
    padding: 0 30px;
    box-shadow: none;
}

</style>
<?php include( 'header.php'); ?>
<div class="content custom-scrollbar">
  <div class="doc data-table-doc page-layout simple full-width">
    <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
      <!-- APP TITLE -->
      <div class="col-12 col-sm">
        <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
          <div class="logo-icon mr-3 mt-1"> <i class="icon s-6 icon-playlist-check"></i>
          </div>
          <div class="logo-text">
            <div class="h4">Recruiter Testimonial</div>
          </div>
        </div>
      </div>
      <!-- / APP TITLE -->
    </div>
    <div class="page-content p-6">
      <div class="content container">
        <div class="row">
          <div class="col-12">
            <div class="example">
              <div class="source-preview-wrapper">
                <div class="preview">
                  <div class="preview-elements">
                    <div id="registers">
                      <div class="form-wrapper">
                        <div class="mainjob">
                          <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/jobcategoryInsert" method="post" novalidate>
                            <div class="job">
                            </div>
                        </div>
                        <div class="newcustom">
                          <div class="preview">
                            <div class="preview-elements">
                              <table id="sample-data-table" class="table dataTable no-footer" role="grid" aria-describedby="sample-data-table_info" style="width: 855px;">
                                <thead>
                                  <tr role="row">
                                    <th class="secondary-text sorting_asc" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                                       ID
                                       : activate to sort column descending" style="width: 40px;">
                                      <div class="table-header"> <span class="column-title">ID</span>
                                      </div>
                                    </th>
                                    
                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Posted By</span>
                                      </div>
                                    </th>

                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Company Name</span>
                                      </div>
                                    </th>

                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" 
                                    rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Title</span>
                                      </div>
                                    </th>

                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" 
                                    rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Description</span>
                                      </div>
                                    </th>

                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                       Action
                                       : activate to sort column ascending" style="width: 68px;">
                                      <div class="table-header"> <span class="column-title">Action</span>
                                      </div>
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                    $x1=1; 
                                    
                                    foreach($recruitFetchs as $recruitFetch) { 
                                  ?>
                                      <tr role="row" class="odd">
                                        <td class="sorting_1"><?php echo $x1;?></td>
                                        
                                        <td> <?php echo $recruitFetch['postedby']; ?></td>
                                        <td> <?php echo $recruitFetch['cname']; ?> </td>
                                        <td> <?php echo $recruitFetch['title']; ?> </td>
                                        <td> <?php echo $recruitFetch['description']; ?> </td>
                                        <td>
                                            <?php if($recruitFetch['status']=='0') { ?>
                                             
                                             <a title="Block" href="#" data-toggle="modal" id="<?php echo $recruitFetch['id'];?>" onclick="getid(this.id)" data-target="#myModal11" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-block-helper s-4"></i></a>
                                             
                                             <?php } else { ?>
                                             
                                             <a title="Active" href="#" data-toggle="modal" id="<?php echo $recruitFetch['id'];?>" onclick="getid(this.id)" data-target="#myModal22" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-checkbox-marked-circle-outline s-4"></i></a> 
                                             
                                             <?php } ?> 
                                          </td>
                                      </tr>
                                  <?php 
                                    $x1++; 
                                    }
                                  ?>
                                </tbody>
                              </table>
                              <script type="text/javascript">
                                $('#sample-data-table').DataTable();
                              </script>
                            </div>
                          </div>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6"></nav>

<!-- Modal -->
<div id="myModal11" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Testimonial</h4>
        <p>Are you sure? Want to accept Testimonial.</p>        
        <form method="post" action="<?php echo base_url();?>administrator/Recruiter/testimonial_status">
          <input type="hidden" value="" name="cid" id="cidd">
          <button type="submit" class="btn Submit" aria-label="Job Level"> Accept </button>
        </form>

      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="myModal22" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Testimonial</h4>
        <p>Are you sure? Want to Inactive Testimonial.</p>        
        <form method="post" action="<?php echo base_url();?>administrator/Recruiter/testimonial_status">
          <input type="hidden" value="" name="cid" id="cidd1">
          <button type="submit" class="btn Submit" aria-label="Job Level"> Accept </button>
        </form>

      </div>
    </div>

  </div>
</div>



</main>

<script type="text/javascript">

      function getid(id) {
        $('#cidd').val(id);
        $('#cidd1').val(id);
      }
</script>

</body>

</html>