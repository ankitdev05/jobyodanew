<style type="text/css">
    .validError{
        color: red;
    }
</style>
<?php include('header.php'); ?>
<div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Add Recruiter</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/recruiterInsert" method="post" novalidate>

                                <div class="form-group mb-4">
                                    <input type="text" name="fname" value="<?php if(!empty($userData['fname'])){ echo $userData['fname']; } ?>" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" />
                                    <label for="registerFormInputName">First Name</label>
                                    <?php if(!empty($errors['fname'])){echo "<span class='validError'>".$errors['fname']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="text" name="lname" value="<?php if(!empty($userData['lname'])){ echo $userData['lname']; } ?>" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" />
                                    <label for="registerFormInputName">Last Name</label>
                                    <?php if(!empty($errors['lname'])){echo "<span class='validError'>".$errors['lname']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="cname" value="<?php if(!empty($userData['cname'])){ echo $userData['cname']; } ?>" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" />
                                    <label for="registerFormInputName">Company Name</label>
                                    <?php if(!empty($errors['cname'])){echo "<span class='validError'>".$errors['cname']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="email" name="email" value="<?php if(!empty($userData['email'])){ echo $userData['email']; } ?>" class="form-control" id="registerFormInputEmail" aria-describedby="emailHelp" />
                                    <label for="registerFormInputEmail">Email address</label>
                                    <?php if(!empty($errors['email'])){echo "<span class='validError'>".$errors['email']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <select name="phonecode" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" />
                                        <option> Select Country Code</option>
                                    <?php
                                        foreach($phonecodes as $phonecode) {
                                    ?>
                                        <option value="<?php echo $phonecode['phonecode'];?>" <?php if($phonecode['phonecode']=='63'){ echo "selected"; } ?>> <?php echo $phonecode['name'] .' - '. $phonecode['phonecode'];?></option>
                                    <?php
                                        }
                                    ?>
                                    </select>
                                    <?php if(!empty($errors['phonecode'])){echo "<span class='validError'>".$errors['phonecode']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="text" name="phone" value="<?php if(!empty($userData['phone'])){ echo $userData['phone']; } ?>" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" />
                                    <label for="registerFormInputName">Phone Number</label>
                                    <?php if(!empty($errors['phone'])){echo "<span class='validError'>".$errors['phone']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="text" name="address" value="<?php if(!empty($userData['address'])){ echo $userData['address']; } ?>" class="form-control" id="txtplaces" aria-describedby="nameHelp" />
                                    <label for="registerFormInputName">Address</label>
                                    <?php if(!empty($errors['address'])){echo "<span class='validError'>".$errors['address']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="password" value="<?php if(!empty($userData['password'])){ echo $userData['password']; } ?>" name="password" class="form-control" id="registerFormInputPassword" />
                                    <label for="registerFormInputPassword">Password</label>
                                    <?php if(!empty($errors['password'])){echo "<span class='validError'>".$errors['password']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="password" value="<?php if(!empty($userData['confirmPassword'])){ echo $userData['confirmPassword']; } ?>" name="confirmPassword" class="form-control" id="registerFormInputPasswordConfirm" />
                                    <label for="registerFormInputPasswordConfirm">Password (Confirm)</label>
                                    <?php if(!empty($errors['confirmPassword'])){echo "<span class='validError'>".$errors['confirmPassword']."</span>";}?>
                                </div>

                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    CREATE ACCOUNT
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
</body>

</html>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&sensor=false&libraries=places&callback=initMap"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtplaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.A;
            var longitude = place.geometry.location.F;
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
        });
    });
    </script>