<style type="text/css">
    .validError{
        color: red;
    }
    .imgmsg{
      font-size: 13px;
      display: block;
      text-align: center;
      margin: 15px 0 0 0;
    }
    #register .form-wrapper {
    width: 80.4rem!important;
    max-width: 80.4rem!important;
    background: #FFFFFF;
    text-align: center;
}
</style>
<?php include('header.php'); ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Add News</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/newsadvertiseInsert" method="post" novalidate enctype="multipart/form-data">

                                <div class="form-group mb-4">
                                    <label>Upload Image</label>
                                    <input type="file" name="banner" class="form-control">
                                    <?php if(!empty($errors['banner'])){echo "<span class='validError'>".$errors['banner']."</span>";}?>
                                </div>

                                <span class="imgmsg">*Image dimension should be between 250X150 - 350X250, Image allowed types are jpeg, jpg, png</span>
                                <br>

                                <div class="form-group mb-4">
                                    <input type="text" name="title" value="<?php if(!empty($userData['title'])){ echo $userData['title']; } ?>" class="form-control" aria-describedby="nameHelp"  >
                                    <label>Title</label>
                                    <?php if(!empty($errors['title'])){echo "<span class='validError'>".$errors['title']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea rows="5" name="desc" id="content" class="form-control" maxlength="800"></textarea>
                                    
                                    <?php if(!empty($errors['desc'])){echo "<span class='validError'>".$errors['desc']."</span>";}?>
                                </div>

                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Submit
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
        </nav>
    </main>
</body>

</html>

<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
   $(function(){  
        $.datepicker.setDefaults({  
            dateFormat: 'yy-mm-dd'   
        }); 
        $("#start_date").datepicker({minDate: "0" });  
        $("#end_date").datepicker({minDate: "0" }); 
        
   });
</script> 

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
      CKEDITOR.replace('content');
    </script>