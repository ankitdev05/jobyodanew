<!DOCTYPE html>
<html lang="en-us">

<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="http://mobuloustech.com/jobyoda/recruiterfiles/images/fav.png" type="image/png" sizes="16x16">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">

    <!-- STYLESHEETS -->
    <style type="text/css">
            [fuse-cloak],
            .fuse-cloak {
                display: none !important;
            }
            .validError{color:#f00;}
            
        </style>

    <!-- Icons.css -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/icons/fuse-icon-font/style.css">
    <!-- Animate.css -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/node_modules/animate.css/animate.min.css">
    <!-- PNotify -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/PNotifyBrightTheme.css">
    <!-- Nvd3 - D3 Charts -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/node_modules/nvd3/build/nv.d3.min.css" />
    <!-- Perfect Scrollbar -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css" />
    <!-- Fuse Html -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/fuse-html/fuse-html.min.css" />
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/css/main.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/css/fonts">
<link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/css/fontawesome.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url().'adminfiles/';?>assets/css/all.css">
    <!-- / STYLESHEETS -->

    <!-- JAVASCRIPT -->
    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Mobile Detect -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/mobile-detect/mobile-detect.min.js"></script>
    <!-- Perfect Scrollbar -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <!-- Popper.js -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Nvd3 - D3 Charts -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/d3/d3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/nvd3/build/nv.d3.min.js"></script>
    <!-- Data tables -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables.net/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables-responsive/js/dataTables.responsive.js"></script>
    <!-- PNotify -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotify.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyStyleMaterial.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyButtons.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyCallbacks.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyMobile.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyHistory.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyDesktop.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyConfirm.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/pnotify/dist/iife/PNotifyReference.js"></script>
    <!-- Fuse Html -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/fuse-html/fuse-html.min.js"></script>
    <!-- Main JS -->
    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/js/main.js"></script>
    <!-- / JAVASCRIPT -->
</head>

<body class="layout layout-vertical layout-left-navigation layout-above-toolbar layout-above-footer">
    <main>
        
        <div id="wrapper">
            
            <div class="content-wrapper">
                <div class="content custom-scrollbar">

                    <div id="login" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="logo">
                                <img src="<?php echo base_url(); ?>recruiterfiles/images/jobyoda.png" style="width: 100%;">
                                <!--<span>F</span>-->
                            </div>

                            <div class="title mt-4 mb-8">Log in to your account</div>
                            <?php
                                if($this->session->tempdata('loginerror') !== null) {
                            ?>
                                <p style="color: red;"> <?php echo $this->session->tempdata('loginerror');?> </p>
                            <?php
                                }
                            ?>
                            <form name="loginForm" action="<?php echo base_url();?>administrator/admin/logincheck" method="post" novalidate>

                                <div class="form-group mb-4">
                                    <input type="text" name="username" class="form-control" id="loginFormInputEmail" aria-describedby="emailHelp" placeholder=" " value="<?php if (get_cookie('uemail')) { echo get_cookie('uemail'); } ?>"/>
                                    <label for="loginFormInputEmail">Username</label>
                                    <?php if(isset($errors['username'])){echo "<span class='validError'>".$errors['username']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="password" name="password" class="form-control" id="loginFormInputPassword" placeholder="Password" value="<?php if (get_cookie('upassword')) { echo get_cookie('upassword'); } ?>" />
                                    <label for="loginFormInputPassword">Password</label>
                                    <?php if(isset($errors['password'])){echo "<span class='validError'>".$errors['password']."</span>";}?>
                                </div>

                                <div class="remember-forgot-password row no-gutters align-items-center justify-content-between pt-4">

                                    <div class="form-check remember-me mb-4">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="remember_me" class="form-check-input" aria-label="Remember Me" checked/>
                                            <span class="checkbox-icon"></span>
                                            <span class="form-check-description">Remember Me</span>
                                        </label>
                                    </div>

                                    <a href="<?php echo base_url();?>administrator/forgot" class="forgot-password text-secondary mb-4">Forgot Password?</a>
                                </div>

                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    LOG IN
                                </button>

                            </form>

                            <!--<div class="register d-flex flex-column flex-sm-row align-items-center justify-content-center mt-8 mb-6 mx-auto">
                                <span class="text mr-sm-2">Don't have an account?</span>
                                <a class="link text-secondary" href="<?php echo base_url();?>administrator/admin/signup">Create an account</a>
                            </div>-->

                        </div>
                    </div>

                </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
    </main>
</body>

</html>