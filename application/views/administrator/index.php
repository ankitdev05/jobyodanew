<?php include('header.php');?>
<div class="content custom-scrollbar">

                    <div id="project-dashboard" class="page-layout simple right-sidebar">

                        <div class="page-content-wrapper custom-scrollbar">

                            <!-- HEADER -->
                            <div class="page-header bg-primary text-auto d-flex flex-column justify-content-between px-6 pt-4 pb-0">

                                <div class="row no-gutters align-items-start justify-content-between flex-nowrap">

                                     <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                                        <div class="logo-icon mr-3 mt-1">
                                            <i class="icon s-6 icon-tile-four"></i>
                                        </div>
                                        <div class="logo-text">
                                            <div class="h4">Dashboard</div>
                                           
                                        </div>
                                    </div>
                                </div>

                                <div class="row no-gutters align-items-center project-selection">

                                   <!-- <div class="selected-project h6 px-4 py-2">ACME Corp. Backend App</div>-->

                                    <div class="project-selector">
                                        <!--<button type="button" class="btn btn-icon">
                                            <i class="icon icon-dots-horizontal"></i>
                                        </button>-->
                                    </div>

                                </div>

                            </div>
                            <!-- / HEADER -->

                            <!-- CONTENT -->
                            <div class="page-content">

                                <!--<ul class="nav nav-tabs" id="myTab" role="tablist">

                                    <li class="nav-item">
                                        <a class="nav-link btn active" id="home-tab" data-toggle="tab" href="#home-tab-pane" role="tab" aria-controls="home-tab-pane" aria-expanded="true">Home</a>
                                    </li>

                                    
                                </ul>-->

                                <div class="tab-content dashboardsdfg">
                                    <div class="tab-pane fade show active p-3" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab">

                                        <!-- WIDGET GROUP -->
                                        <div class="widget-group row no-gutters mainadminyoda">

                                            <!-- WIDGET 1 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                                <a href="<?php echo base_url(); ?>/administrator/recruiterList">
                                                <div class="widget widget1 card">

                                                    

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">

                                                        <div class="title text-secondary"><?php echo count($appLists);?></div>

                                                        <span class="h6">Company/Recruiter Listing</span>
                                                    </div>
                                                    
                                                   

                                                    <!--<div class="widget-footer p-4 bg-light row no-gutters align-items-center">

                                                        <span class="text-muted">Completed:</span>

                                                        <span class="ml-2">7</span>

                                                    </div>-->
                                                </div>
                                                </a>
                                            </div>
                                            <!-- / WIDGET 1 -->
                                            <!-- WIDGET 2 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                                <a href="<?php echo base_url(); ?>/administrator/pendingrecruiters">
                                                <div class="widget widget1 card">

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">

                                                        <div class="title text-warning"><?php echo count($pendingLists);?></div>

                                                        <span class="h6">Pending Recruiter Listing</span>
                                                    </div>
                                                    
                                                </div>
                                                </a>
                                            </div>
                                            <!-- / WIDGET 1 -->
                                            <!-- WIDGET 5 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url(); ?>/administrator/companyList">
                                                <div class="widget widget4 card">

                                                    

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-danger"><?php echo count($compListing); ?></div>
                                                        <!--<div class="sub-title h6 text-muted">PROPOSALS</div>-->
                                                        <span class="h6">Site Listing</span>
                                                    </div>
                                                    
                                                    
                                                    
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 5 -->

                                            <!-- WIDGET 4 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url(); ?>/administrator/jobseekerlist">
                                                <div class="widget widget4 card">

                                                    

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-info"><?php echo count($candidates); ?></div>
                                                        <!--<div class="sub-title h6 text-muted">PROPOSALS</div>-->
                                                         <span class="h6">JobSeekers</span>
                                                    </div>
                                                    
                                                   

                                                    <!--<div class="widget-footer p-4 bg-light row no-gutters align-items-center">
                                                        <span class="text-muted">Implemented:</span>
                                                        <span class="ml-2">8</span>
                                                    </div>-->
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 4 -->

                                            <!-- WIDGET 3 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                               <a href="<?php echo base_url(); ?>/administrator/joblist">
                                                <div class="widget widget3 card">

                                                    

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-warning"><?php echo count($jobLists); ?></div>
                                                      <span class="h6">Posted job</span>
                                                    </div>
                                                    
                                                    
                                                    

                                                    <!--<div class="widget-footer p-4 bg-light row no-gutters align-items-center">
                                                        <span class="text-muted">Closed today:</span>
                                                        <span class="ml-2">0</span>
                                                    </div>-->
                                                </div>
                                              </a>    
                                            </div>

                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                               <a href="<?php echo base_url(); ?>/administrator/joblist">
                                                <div class="widget widget3 card">

                                                    

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-info"><?php echo $openings[0]['openings']; ?></div>
                                                      <span class="h6">Employment Opportunities</span>
                                                    </div>
                                                    
                                                </div>
                                              </a>    
                                            </div>

                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                               <a href="<?php echo base_url(); ?>/administrator/hiringreport?status=new">
                                                <div class="widget widget3 card">

                                                    

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-secondary"><?php echo count($appliedJobs); ?></div>
                                                      <span class="h6">New Applications</span>
                                                    </div>
                                                    
                                                    
                                                    

                                                    <!--<div class="widget-footer p-4 bg-light row no-gutters align-items-center">
                                                        <span class="text-muted">Closed today:</span>
                                                        <span class="ml-2">0</span>
                                                    </div>-->
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 3 -->

                                            

                                            <!-- WIDGET 2 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url(); ?>/administrator/storylist">
                                                <div class="widget widget2 card">

                                                   

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-info"><?php echo count($storyListing);?></div>
                                                        <span class="h6">Success Stories</span>
                                                    </div>
                                                    
                                                     

                                                    
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 2 -->

                                            <!-- WIDGET 2 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url(); ?>/administrator/invoice">
                                                <div class="widget widget2 card">

                                                   

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-danger"><?php echo count($invoiceLists);?></div>
                                                        <span class="h6">Invoice</span>
                                                    </div>
                                                    
                                                     

                                                    
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 2 -->

                                            <!-- WIDGET 2 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url(); ?>/administrator/contactlist">
                                                <div class="widget widget2 card">

                                                   

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-warning"><?php echo count($contactLists);?></div>
                                                        <span class="h6">Jobseeker Queries</span>
                                                    </div>
                                                    
                                                     

                                                    
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 2 -->

                                            <!-- WIDGET 2 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url(); ?>/administrator/advertisement?status=pending">
                                                <div class="widget widget12 card">

                                                   

                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-danger"><?php echo count($adLists);?></div>
                                                        <span class="h6">Pending Advertisement</span>
                                                    </div>
                                                    
                                                     

                                                    
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 2 -->

                                            <!-- WIDGET 5 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="#" data-target="#myModal2" data-toggle="modal">
                                                <div class="widget widget4 card">
                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                      <div class="title text-info">  <i class="fas fa-briefcase"></i>
                                                        </div>
														<span class="h6">Boosting job amount</span>
                                                    </div>
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 5 -->

                                            <!-- WIDGET 2 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url(); ?>administrator/recruitertransfer">
                                                <div class="widget widget12 card">
                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-danger"><?php echo count($transferRequest);?></div>
                                                        <span class="h6">Transfer Request</span>
                                                    </div>
                                                     
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 2 -->

                                            <!-- WIDGET 2 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url(); ?>administrator/recruiter_testimonial">
                                                <div class="widget widget12 card">
                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                        <div class="title text-danger"><?php echo count($testimonialRequest);?></div>
                                                        <span class="h6">Testimonial</span>
                                                    </div>
                                                     
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 2 -->

                                            <!-- WIDGET 5 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url();?>administrator/videoadvertise">
                                                <div class="widget widget4 card">
                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                      <div class="title text-info">  <i class="fas fa-file-video"></i>
                                                        </div>
                                                        <span class="h6">Video Advertisement</span>
                                                    </div>
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 5 -->

                                            <!-- WIDGET 5 -->
                                            <div class="col-12 col-sm-6 col-xl-3 p-3">
                                              <a href="<?php echo base_url();?>administrator/news">
                                                <div class="widget widget4 card">
                                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                                      <div class="title text-info">  <i class="fas fa-newspaper"></i>
                                                        </div>
                                                        <span class="h6">News / Updates</span>
                                                    </div>
                                                </div>
                                              </a>    
                                            </div>
                                            <!-- / WIDGET 5 -->
                                           
                                        </div>
                                        <!-- / WIDGET GROUP -->
                                    </div>


                                    <div class="tab-pane fade p-3" id="budget-summary-tab-pane" role="tabpanel" aria-labelledby="budget-summary-tab">

                                        <!-- WIDGET GROUP -->
                                        <div class="widget-group row no-gutters">

                                            <!-- WIDGET 8 -->
                                            <div class="col-12 col-lg-6 p-3">

                                                <div class="widget widget8 card">

                                                    <div class="widget-header px-4 row no-gutters align-items-center justify-content-between">
                                                        <div class="col">
                                                            <span class="h6">Budget Distribution</span>
                                                        </div>
                                                    </div>

                                                    <div class="widget-content row">

                                                        <div class="col-12">

                                                            <div id="widget8-main-chart" class="p-4">
                                                                <svg></svg>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- / WIDGET 8 -->

                                            <!-- WIDGET 9 -->
                                            <div class="col-12 col-lg-6 p-3">

                                                <div class="widget widget9 card">

                                                    <div class="widget-header px-4 row no-gutters align-items-center justify-content-between">

                                                        <div class="col">
                                                            <span class="h6">Spent</span>
                                                        </div>

                                                        <div class="">
                                                            <select id="widget9-option-select" class="h6 custom-select">
                                                                <option selected="" value="TW">This Week</option>
                                                                <option value="LW">Last Week</option>
                                                                <option value="2W">2 Weeks Ago</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="widget-content">

                                                        <div id="widget9-weeklySpent" class="budget-item p-4 row no-gutters align-items-center">

                                                            <div class="col-12 col-sm-auto p-2">

                                                                <div class="item-title text-muted">WEEKLY SPENT</div>

                                                                <div class="pt-2 h2">
                                                                    <span class="text-muted">$</span>
                                                                    <span class="item-value">3,630.15</span>
                                                                </div>

                                                            </div>

                                                            <div class="chart-wrapper col-12 col-sm">
                                                                <svg></svg>
                                                            </div>
                                                        </div>

                                                        <div id="widget9-totalSpent" class="budget-item p-4 row no-gutters align-items-center">

                                                            <div class="col-12 col-sm-auto p-2">

                                                                <div class="item-title text-muted">TOTAL SPENT</div>

                                                                <div class="pt-2 h2">
                                                                    <span class="text-muted">$</span>
                                                                    <span class="item-value">34,758.34</span>
                                                                </div>
                                                            </div>

                                                            <div class="chart-wrapper col-12 col-sm">
                                                                <svg></svg>
                                                            </div>
                                                        </div>

                                                        <div id="widget9-remaining" class="budget-item p-4 row no-gutters align-items-center">

                                                            <div class="col-12 col-sm-auto p-2">

                                                                <div class="item-title text-muted">REMAINING</div>

                                                                <div class="pt-2 h2">
                                                                    <span class="text-muted">$</span>
                                                                    <span class="item-value">89.241,66</span>
                                                                </div>

                                                            </div>

                                                            <div class="chart-wrapper col-12 col-sm">
                                                                <svg></svg>
                                                            </div>

                                                        </div>

                                                        <div class="divider w-100 my-2"></div>

                                                        <div id="widget9-total-budget" class="p-4">

                                                            <div class="item-title text-muted">TOTAL BUDGET</div>

                                                            <div class="pt-2 h2">
                                                                <span class="text-muted">$</span>
                                                                <span class="item-value">124.000,00</span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- / WIDGET 9 -->

                                            <!-- WIDGET 10 -->
                                            <div class="col-12 p-3">

                                                <div class="widget widget10 card">

                                                    <div class="widget-header pl-4 pr-2 row no-gutters align-items-center justify-content-between">

                                                        <div class="col">
                                                            <span class="h5">Budget Details</span>
                                                        </div>

                                                        <button type="button" class="btn btn-icon">
                                                            <i class="icon icon-dots-vertical"></i>
                                                        </button>

                                                    </div>

                                                    <div class="widget-content p-4 table-responsive">

                                                        <table class="table">

                                                            <thead>
                                                                <tr>

                                                                    <th>
                                                                        Budget Type
                                                                    </th>

                                                                    <th>
                                                                        Total Budget
                                                                    </th>

                                                                    <th>
                                                                        Spent ($)
                                                                    </th>

                                                                    <th>
                                                                        Spent (%)
                                                                    </th>

                                                                    <th>
                                                                        Remaining ($)
                                                                    </th>

                                                                    <th>
                                                                        Remaining (%)
                                                                    </th>

                                                                </tr>
                                                            </thead>

                                                            <tbody>

                                                                <tr>

                                                                    <td>
                                                                        <span class="badge badge-primary">
                                                                            Wireframing
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-bold">
                                                                            $14,880.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $14,000.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-success">
                                                                            %94.08
                                                                        </span>

                                                                        <i class="icon icon-trending-up text-success s-4"></i>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $880.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            %5.92
                                                                        </span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <span class="badge badge-success">
                                                                            Design
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-bold">
                                                                            $21,080.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $17,240.34
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-success">
                                                                            %81.78
                                                                        </span>

                                                                        <i class="icon icon-trending-up text-success s-4"></i>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $3,839.66
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            %18.22
                                                                        </span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <span class="badge badge-warning">
                                                                            Coding
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-bold">
                                                                            $34,720.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $3,518.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-danger">
                                                                            %10.13
                                                                        </span>

                                                                        <i class="icon icon-trending-down text-danger s-4"></i>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $31,202.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            %89.87
                                                                        </span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <span class="badge badge-info">
                                                                            Marketing
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-bold">
                                                                            $34,720.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $0.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-info">
                                                                            %0.00
                                                                        </span>

                                                                        <i class="icon icon-minus text-info s-4"></i>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $34,720.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            %100.00
                                                                        </span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <span class="badge badge-danger">
                                                                            Extra
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-bold">
                                                                            $18,600.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $0.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="text-info">
                                                                            %0.00
                                                                        </span>

                                                                        <i class="icon icon-minus text-info s-4"></i>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            $34,720.00
                                                                        </span>

                                                                    </td>

                                                                    <td>
                                                                        <span class="">
                                                                            %100.00
                                                                        </span>

                                                                    </td>

                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- / WIDGET 10 -->
                                        </div>
                                        <!-- / WIDGET GROUP -->
                                    </div>
                                    <div class="tab-pane fade p-6" id="team-members-tab-pane" role="tabpanel" aria-labelledby="team-members-tab">

                                        <!-- WIDGET GROUP -->
                                        <div class="widget-group row no-gutters">

                                            <!-- WIDGET 1 -->
                                            <div class="col-12">

                                                <div class="card">

                                                    <div class="widget-header px-4 py-6 row no-gutters align-items-center justify-content-between">

                                                        <div class="col">
                                                            <span class="h5">Team Members</span>
                                                        </div>

                                                        <div>
                                                            <span class="badge badge-danger">20 members</span>
                                                        </div>

                                                    </div>

                                                    <div class="divider"></div>

                                                    <div class="widget-content table-responsive">

                                                        <table class="table">

                                                            <thead>
                                                                <tr>

                                                                    <th>

                                                                    </th>

                                                                    <th>
                                                                        Name
                                                                    </th>

                                                                    <th>
                                                                        Position
                                                                    </th>

                                                                    <th>
                                                                        Office
                                                                    </th>

                                                                    <th>
                                                                        E-Mail
                                                                    </th>

                                                                    <th>
                                                                        Phone
                                                                    </th>

                                                                </tr>
                                                            </thead>

                                                            <tbody>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/james.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Jack Gilbert</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Design Manager</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Johor Bahru</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>jgilbert48@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;16 298 032 7774</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/katherine.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Kathy Anderson</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Recruiting Manager</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Solţānābād</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>kanderson49@mail.com.br</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;23 572 311 1136</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/andrew.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Mark Turner</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Recruiting Manager</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Neftegorsk</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>mturner4a@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;01 139 803 9263</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/jane.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Kathryn Martinez</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Director of Sales</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Palekastro</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>kmartinez4b@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;25 467 022 5393</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/alice.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Annie Gonzales</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Actuary</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Candon</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>agonzales4c@mail.edu</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;99 891 619 7138</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/vincent.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Howard King</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Human Resources</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Bergen op Zoom</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>hking4d@mail.gov</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;46 984 348 1409</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/joyce.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Elizabeth Dixon</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Electrical Engineer</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Písečná</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>edixon4e@mail.gov</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;33 332 067 9063</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/danielle.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Dorothy Morris</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Office Assistant</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Magsaysay</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>dmorris4f@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;05 490 958 6120</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/carl.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Mark Gonzales</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Quality Control</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Matsue-shi</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>mgonzales4g@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;03 168 394 9935</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/profile.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Catherine Rogers</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Programmer Analyst</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Kangar</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>crogers4h@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;86 235 407 5373</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/garry.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Ruth Grant</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Community Outreach</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Beaune</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>rgrant4i@mail.pl</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;36 288 083 8460</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/james.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Phyllis Gutierrez</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Administrative Assistant</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Shlissel’burg</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>pgutierrez4j@mail.net</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;52 749 861 9304</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/alice.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Lillian Morris</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Media Planner</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Berlin</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>lmorris4k@mail.de</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;59 695 110 3856</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/vincent.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Jeremy Anderson</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Systems Engineer</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Lũng Hồ</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>janderson4l@mail.uk</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;40 384 115 1448</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/carl.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Arthur Lawrence</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Nurse Practicioner</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Sarkanjut</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>alawrence4m@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;36 631 599 7867</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/andrew.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>David Simmons</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Social Worker</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Ushumun</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>dsimmons4n@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;01 189 681 4417</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/danielle.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Daniel Johnston</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Help Desk</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>São Carlos</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>djohnston4o@mail.gov</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;60 028 943 7919</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/joyce.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Ann King</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Internal Auditor</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Liren</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>aking4p@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;91 103 932 6545</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/james.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Phillip Franklin</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>VP Accounting</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Soba</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>pfranklin4q@mail.com</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;25 820 986 7626</span>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td>

                                                                        <img class="avatar" src="<?php echo base_url().'adminfiles/';?>assets/images/avatars/garry.jpg">

                                                                    </td>

                                                                    <td>

                                                                        <span>Gary Gonzalez</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Speech Pathologist</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>Gangkou</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>ggonzalez4r@mail.cc</span>

                                                                    </td>

                                                                    <td>

                                                                        <span>&#43;10 862 046 7916</span>

                                                                    </td>

                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- WIDGET 1 -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- / CONTENT -->
                        </div>
                    </div>

                    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/js/apps/dashboard/project.js"></script>

                </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            
        </nav>
    </main>

    <!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body boostjonf">
        <h3>Boosting job amount</h3>
        <form method="post" action="<?php echo base_url();?>administrator/JobSeeker/boostamountInsert">
            <div class="form-group">
			<label>Add Boost Job Amount</label>
                <input type="text" name="boost_amount" class="form-control" placeholder="Enter Amount" required="" value="<?php if(!empty($boostamount[0]['boost_amount'])){ echo $boostamount[0]['boost_amount']; }?>">
                <input type="hidden" name="userId" id="userId">
            </div>
            
            <button type="submit" class="btn btn-secondary">Submit</button>
			<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0 5px;">Close</button>
            
        </form>

      </div>
      
    </div>

  </div>
</div>
</body>

</html>