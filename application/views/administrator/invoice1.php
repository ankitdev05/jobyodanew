<?php include('header.php'); ?>
<div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="mainheadicos">
                                 <i class="icon s-4 icon-file-pdf-box"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Invoice</div>
                                 <div class="">Total: <?php if($Lists){echo count($Lists); }?></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <div class="col-auto">
                          <!-- <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>-->
                        </div>
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements contactlst">
                                          <div class="row" style="margin-bottom:15px;"> 
                                                              <div class="col-md-3">  
                                                               <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                                                              </div>  
                                                              <div class="col-md-3">  
                                                                 <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                                                              </div> 
                                              
                                               
                                                
                                                              <div class="col-md-3">  
                                                               <input type="button" name="filter" id="filter" value="Filter" class="btn btn-info" />  
                                                            </div>
                                             
                                              </div>
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text" style="width:40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      
                                                      <th class="secondary-text" >
                                                         <div class="table-header">
                                                            <span class="column-title">Company</span>
                                                         </div>
                                                      </th>
                                                      
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Invoice</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Type</span>
                                                         </div>
                                                      </th>
                                                     
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Date</span>
                                                         </div>
                                                      </th>
                                                   </tr>
                                                </thead>
                                                <tbody> 
                                                   <?php
                                                      $x1 =1;
                                                      foreach($Lists as $List) {
                                                      ?>
                                                   <tr>
                                                      <td style="width:30px;"><?php echo $x1;?></td>
                                                      <td style="width:40px;"><?php echo $List['cname'];?></td>
                                                      <td style="width:40px;"><a class="btn btn-block btn-secondary fuse-ripple-ready" target="_blank" href="<?php echo base_url().$List['invoice'];?>">Download</a></td>
                                                      <td style="width:40px;"><?php if($List['type']=='1'){ echo "Hired Candidates";}else if($List['type']=='2'){ echo "Boost Job"; }else if($List['type']=='5'){ echo "Video Advertise"; } ?></td>
                                                      <td style="width:40px;"><?php echo date('Y-m-d',strtotime($List['date']) );?></td>
                                                      
                                                      <!-- <td>
                                                         <a href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" class="btn btn-icon deletehit" aria-label="Product details"><i class="icon-account-remove s-4"></i></a>
                                                      </td> -->
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete JobSeeker</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to delete this JobSeeker?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/deleteJobSeeker?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }


      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker({maxDate: "0" });  
                $("#to_date").datepicker({maxDate: "0" }); 
                $('#to_date').datepicker('setDate', 'today');
                $('#from_date').datepicker('setDate', 'today-30');
 
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                if(from_date == '' )  
                {  
                   alert("Please Select From Date");   
                } else if(to_date == '' )  
                {  
                   alert("Please Select To Date");   
                }   
                else  
                {  
                     $.ajax({  
                          url:"<?php echo base_url(); ?>administrator/jobpost/invoicebyDate",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                              $("#sample-data-table").html(data);
                              $('#sample-data-table').DataTable({
                              dom: 'lBfrtip',
                              buttons: [
                                  'excel', 'csv', 'pdf', 'copy'
                                 ],
                                 destroy : true,
                              }); 
                          }  
                     });  
                }  
           });  
      });  

</script>
   </body>
</html>

