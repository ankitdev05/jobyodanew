<?php include('header.php'); ?>


               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-person-box"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Video Advertisement Listing</div>
                                 <div class="">Total: <?php if($advertiseLists){echo count($advertiseLists); }?></div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements reculsting"> 
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text sorting_asc" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                                                         
                                                            ID
                                                         
                                                      : activate to sort column descending" style="width: 40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Sno</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Video</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Company</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Start Date</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">End Date</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Title</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Description</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Action
                                                         
                                                      : activate to sort column ascending" style="width: 68px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Invoice
                                                         
                                                      : activate to sort column ascending" style="width: 68px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Invoice</span>
                                                         </div>
                                                      </th>

                                                      <!-- <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Invoice</span>
                                                         </div>
                                                      </th> -->
                            
                                                     
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                    $x1 =1;
                                                    foreach($advertiseLists as $advertiseList) {
                                                   ?>
                                                    <tr role="row" class="odd">
                                                      <td class="sorting_1"><?php echo $x1;?></td>
                                                      <td><img src="<?php if(!empty($advertiseList['thumbnail'])){ echo $advertiseList['thumbnail'];}?>" style="width: 37px;"></td>
                                                      <td><?php echo $advertiseList['cname'];?></td>
                                                      <td><?php echo $advertiseList['start_date'];?></td>
                                                      <td><?php echo $advertiseList['end_date'];?></td>
                                                      <td><?php echo $advertiseList['title'];?></td>
                                                      <td><?php echo $advertiseList['desc'];?></td>
                                                      <td>
                                         
                                                         <a title="Edit" href="<?php echo base_url(); ?>administrator/recruiter/editvideoadvertise?id=<?php echo $advertiseList['id'] ?>" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon s-4 icon-pencil"></i></a>
                                                       
                                                       <?php if($advertiseList['status']=='1'){?>
                                                            <a title="Block" href="#" data-toggle="modal" id="<?php echo $advertiseList['id'];?>" onclick="getaid(this.id)" data-target="#activeModal" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-block-helper s-4"></i></a>

                                                       <?php } else { ?>
                                                            <a title="Active" href="#" data-toggle="modal" id="<?php echo $advertiseList['id'];?>" onclick="getbid(this.id)" data-target="#blockModal" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon icon-checkbox-marked-circle-outline s-4"></i></a> 
                                                       <?php }?>

                                                       <a title="Delete"  href="<?php echo base_url(); ?>administrator/recruiter/deletevideo?id=<?php echo $advertiseList['id'] ?>" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon s-4 icon-trash"></i></a>
                                              
                                                      </td> 
                                                      <td>
                                                        <?php 
                                                          if($advertiseList['cname'] == "Admin") {
                                                          } else {
                                                        ?>
                                                           <a href="#" onclick="getid(this.id)" id="<?= $advertiseList['id'] ?>" data-target="#myModal1" data-toggle="modal"  class=" btn btn-block btn-secondary fuse-ripple-ready" aria-label="Product details">Send Invoice</a>
                                                        <?php
                                                          }
                                                        ?>
                                                      </td>
                                                   </tr>
                                                   <?php $x1++;
                                                   }?>
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
                                             </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>

         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="activeModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Active Advertisement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to active this Advertisement?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createaLink">Active</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="blockModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Block Advertisement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to block this Advertisement?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createbLink">Block</a>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Invoice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <input type="number" name="amount" id="amount" class="form-control" value="<?php echo $advertise_price[0]['price']; ?>" disabled>
                  <label for="registerFormInputName">Amount</label>
                  <input type="hidden" name="company_id" id="company_id" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="generatepdf()">Generate Invoice</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>
    </main>
    <script type="text/javascript">

      function getid(id) {
        $('#company_id').val(id);
        //var url = "<?php echo base_url() ?>administrator/jobpost/fetchhiredcandidates?job_id="+id;
      }

      function generatepdf(){
         var jobpost_id =  $('#company_id').val();
         var amount = $('#amount').val();
         var url="<?php echo base_url() ?>administrator/recruiter/generatepdfforvideo?job_id="+jobpost_id+"&amount="+amount;
         
         if(amount==""){
            alert('Enter amount');
            return false;
         } else {
             window.open(url, "_blank");  
             $('#myModal1').modal('hide');
             window.location.href = "<?php echo base_url() ?>administrator/videoadvertisement";
         }
      }

      function getaid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/videoActive?id="+rid;
        var aa = document.getElementById('createaLink');
        aa.setAttribute("href", link);
    }

    function getbid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/videoBlock?id="+rid;
        var aa = document.getElementById('createbLink');
        aa.setAttribute("href", link);
    }
    </script>
   </body>
</html>

