<?php include('header.php');?>
<style type="text/css">
  div.dataTables_wrapper {
max-width: 1300px;
overflow-x: scroll;
}
.custompagination a {
    padding: 10px;
    background-color: chartreuse;
    margin-left: 10px;
    font-size: 18px;
}
.custompagination strong {
    padding: 10px;
    background-color: chartreuse;
    margin-left: 10px;
    font-size: 18px;
}
table#sample-data-tabletest thead th, table#sample-data-tabletest thead td {
    border-bottom: 1px solid rgba(0,0,0,0.12);
    font-size: 13px;
}
table#sample-data-tabletest tbody td {
    padding: 16px 3px;
    font-size: 12px;
}
.exportclasscss{
      float: right;
    background: #000;
    color: #fff;
    padding: 7px 14px;
}
</style>
               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon-chart-bar-stacked s-6"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Report Management</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example scrollmobusr">
                                    <div class="source-preview-wrapper" style="overflow-x: scroll;">
                                       <div class="preview">
                                          <div class="preview-elements dsymgnd">
                                             <!-- JAVASCRIPT BEHAVIOR -->
                                   
                                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link <?php if($checkactive <= 0) { ?> active <?php } ?>" id="day-tab" data-toggle="tab" href="#day" role="tab" aria-controls="day" aria-expanded="true">Daily</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="weekly-tab" data-toggle="tab" href="#weekly" role="tab" aria-controls="weekly">Weekly</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-controls="monthly">Monthly</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="yearly-tab" data-toggle="tab" href="#yearly" role="tab" aria-controls="yearly">Yearly</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="date-tab" data-toggle="tab" href="#date" role="tab" aria-controls="date">Datewise</a>
                                                            </li>

                                                            <li class="nav-item">
                                                                <a class="nav-link <?php if($checkactive > 0) { ?> show active <?php } ?>" id="date-tab" data-toggle="tab" href="#testing" role="tab" aria-controls="date">Testing</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="myTabContent">
                                                            <div class="tab-pane fade  <?php if($checkactive <= 0) { ?> show active <?php } ?>" id="day" role="tabpanel" aria-labelledby="day-tab">
                                                            <h3>Daily Report</h3>
                                                              <select name="status_dropdown" id="status_dropdown">
                                                                <option value="">Select Status</option>
                                                                <option value="all" selected="">All</option>
                                                                <option value="app">Application</option>
                                                                <option value="web">Website</option>
                                                                
                                                              </select>
                                                                <table id="sample-data-table" class="table">
                                                                
                                                                </table>
                                                            </div>

                                                            <div class="tab-pane fade" id="weekly" role="tabpanel" aria-labelledby="weekly-tab">
                                                            <h3>Weekly Report</h3>
                                                                <select name="status_dropdown" id="status_dropdown_weekly">
                                                                <option value="">Select Status</option>
                                                                <option value="all" selected="">All</option>
                                                                <option value="app">Application</option>
                                                                <option value="web">Website</option>
                                                              </select>
                                                                <div class="preloader-set"></div>
                                                                <table id="sample-data-tableweekly" class="table">
                                                                
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="monthly" role="tabpanel" aria-labelledby="monthly-tab">
                                                            <h3>Monthly Report</h3>
                                                            <select name="status_dropdown" id="status_dropdown_monthly">
                                                                <option value="">Select Status</option>
                                                                <option value="all" selected="">All</option>
                                                                <option value="app" >Application</option>
                                                                <option value="web">Website</option>
                                                              </select>
                                                                <div class="preloader-set"></div>
                                                                <table id="sample-data-tablemonthly" class="table">
                                                                
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="yearly" role="tabpanel" aria-labelledby="yearly-tab">
                                                            <h3>Yearly Report</h3>
                                                                <select name="status_dropdown" id="status_dropdown_yearly">
                                                                <option value="">Select Status</option>
                                                                <option value="all" selected="">All</option>
                                                                <option value="app">Application</option>
                                                                <option value="web">Website</option>
                                                              </select>
                                                                <div class="preloader-set"></div>
                                                                <table id="sample-data-tableyearly" class="table">
                                                                
                                                                </table>
                                                            </div>
                                                            <div class="tab-pane fade" id="date" role="tabpanel" aria-labelledby="date-tab">
                                                            <h3>Datewise Report</h3>
															 <div class="row" style="margin-bottom:15px;"> 
                                                              <div class="col-md-3">  
                                                               <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                                                              </div>  
                                                              <div class="col-md-3">  
                                                                 <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                                                              </div>
															   <div class="col-md-3">  
                                                                <select name="status_dropdown" id="status_dropdown_date">
                                                                <option value="">Select Status</option>
                                                                <option value="all" selected="">All</option>
                                                                <option value="app" >Application</option>
                                                                <option value="web">Website</option>
                                                              </select>
															  </div>
                                                              <div class="col-md-3">  
                                                               <input type="button" name="filter" id="filter" value="Filter" class="btn btn-info" />  
                                                            </div>
															
															</div>
                                                                <div class="preloader-set"></div>
                                                                <table id="sample-data-tabledate" class="table">
                                                                
                                                                </table>
                                                            </div>

                                                            <div class="tab-pane fade <?php if($checkactive > 0) { ?> show active <?php } ?>" id="testing" role="tabpanel" aria-labelledby="testing-tab">
                                                            <h3>Report ( <?php echo $resultTotal; ?>)</h3>

                                                            <!-- Export Data --> 
                                                            <a class="exportclasscss" href="<?php echo base_url(); ?>administrator/jobpost/export_csv_report" target="_blank">Export</a>
                                                                
                                                            <br><br>

                                                            <form method="post">
                                                            <div class="row">
                                                            
                                                              <div class="col-md-6">
                                                                <div class="input-group mb-3">
                                                                  <select name="status_dropdown" class="form-control" id="status_dropdown_year" onchange="this.form.submit()" style="height: 36px;">
                                                                  <option value="">Select Status</option>
                                                                  <option value="all" <?php if($searchStatus == "all"){ echo "selected"; }?>>All</option>
                                                                  <option value="app" <?php if($searchStatus == "app"){ echo "selected"; }?>>Application</option>
                                                                  <option value="web" <?php if($searchStatus == "web"){ echo "selected"; }?>>Website</option>
                                                                  </select>
                                                                </div>
                                                              </div>
                                                              <div class="col-md-6">
                                                                <div class="input-group mb-3">
                                                                    <input type="text" name="searchKeyword" class="form-control" placeholder="Search by keyword..." value="<?php echo $searchKeyword; ?>">
                                                                    <div class="input-group-append">
                                                                        <input type="submit" name="submitSearch" class="btn btn-outline-secondary" value="Search">
                                                                        <input type="submit" name="submitSearchReset" class="btn btn-outline-secondary" value="Reset">
                                                                    </div>
                                                                </div>
                                                              </div>
                                                            
                                                            </div>
                                                            </form>
                                                                
                                                                <table id="sample-data-tabletest" class="table">
                                                                    <thead>
                                                                       <tr>
                                                                          <th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">SNo</span>
                                                                             </div>
                                                                          </th>

                                                                          <th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Name</span>
                                                                             </div>
                                                                          </th>

                                                                          <th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Email</span>
                                                                             </div>
                                                                          </th>

                                                                          <th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Phone</span>
                                                                             </div>
                                                                          </th>

                                                                          <th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Registration Date</span>
                                                                             </div>
                                                                          </th>

                                                                          <th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Experience</span>
                                                                             </div>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Location</span>
                                                                             </div>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">State</span>
                                                                             </div>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">City</span>
                                                                             </div>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Intrested In</span>
                                                                             </div>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Education</span>
                                                                             </div>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Nationality</span>
                                                                             </div>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Superpower</span>
                                                                             </div>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Platform</span>
                                                                             </div>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">App Version</span>
                                                                             </div>
                                                                          </th>
                                                                          </th><th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Last Login</span>
                                                                             </div>
                                                                          </th>
                                                                          <th class="secondary-text">
                                                                             <div class="table-header">
                                                                                <span class="column-title">Signup Via</span>
                                                                             </div>
                                                                          </th>
                                                                          
                                                                       </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    <?php 
                                                                      $sno = $row+1;
                                                                      foreach($result as $data) {

                                                                        if($data['exp_month']>1) {
                                                                          $exp_month = $data['exp_month'].' Months'; 
                                                                        }else{
                                                                          $exp_month = $data['exp_month'].' Month';
                                                                        }

                                                                        if(strlen($data['platform']) >1) {
                                                                          $platform = $data['platform'];
                                                                        } else {
                                                                          $platform = "Web";
                                                                        }

                                                                        echo "<tr>";
                                                                        echo "<td>".$sno."</td>";
                                                                        echo "<td>".$data['name']."</td>";
                                                                        echo "<td>".$data['email']."</td>";
                                                                        echo "<td>".$data['phone']."</td>";
                                                                        echo "<td>".date('Y-m-d', strtotime($data['created_at']))."</td>";
                                                                        echo "<td>".$exp_year.' '.$exp_month. "</td>";
                                                                        echo "<td>".$data['location']."</td>";
                                                                        echo "<td>".$data['state']."</td>";
                                                                        echo "<td>".$data['city']."</td>";
                                                                        echo "<td>".$data['jobsInterested']."</td>";
                                                                        echo "<td>".$data['education']."</td>";
                                                                        echo "<td>".$data['nationality']."</td>";
                                                                        echo "<td>".$data['superpower']."</td>";
                                                                        echo "<td>".$data['device_type']."</td>";
                                                                        echo "<td>".$data['app_version']."</td>";
                                                                        echo "<td>".$data['last_used']."</td>";
                                                                        echo "<td>".$platform."</td>";
                                                                        echo "</tr>";
                                                                        $sno++;

                                                                      }
                                                                      if(count($result) == 0){
                                                                        echo "<tr>";
                                                                        echo "<td colspan='3'>No record found.</td>";
                                                                        echo "</tr>";
                                                                      }

                                                                    ?>

                                                                    </tbody>
                                                                </table>

                                                                <!-- Paginate -->
                                                                 <div class="custompagination" style='margin-top: 10px;text-align: center;'>
                                                                 <?php echo $this->pagination->create_links(); ?>
                                                                 </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
           <!-- <a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>



   </body>
</html>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
  <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script type="text/javascript">
  $('#status_dropdown').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getuserDatabyDay";
      $.ajax({
        type:"POST",
        url:url,
        data:{
           status : status
        },
        beforeSend: function() {
          $("#sample-data-table").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
        },
        success:function(data) {
         
          $("#sample-data-table").html(data);
          $('#sample-data-table').DataTable({ 
            "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
            dom: 'lBfrtip',
            buttons: [
              'excel', 'csv'
            ],
            destroy:true,
            deferRender: true
          });
        }
      });
  })

  $(document).ready(function(){
    var status = $('#status_dropdown').val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getuserDatabyDay";
      $.ajax({
        type:"POST",
        url:url,
        data:{
           status : status
        },

        success:function(data) {
         
          $("#sample-data-table").html(data);
          $('#sample-data-table').DataTable({ 
            "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
            dom: 'lBfrtip',
             buttons: [
              'excel', 'csv'
             ],
             destroy:true,
          });
        }
      });
  })

  $('#status_dropdown_weekly').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getuserDatabyWeek";
      $.ajax({
        type:"POST",
        url:url,
        data:{
             status : status
        },
        beforeSend: function() {
          $(".preloader-set").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
        },
        success:function(data) {
         
          $("#sample-data-tableweekly").html(data);
          var table = $('#sample-data-tableweekly').DataTable({ 
             "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
             destroy: true, //use for reinitialize datatable
             dom: 'lBfrtip',
             buttons: [
               'excel', 'csv'
             ],
          });

          if ( table.data().any() ) {
              $(".preloader-set").html("");
          }
        }
      });
  })

  $(document).ready(function() {

    $("#weekly-tab").on('click', function() {
        var status = $('#status_dropdown_weekly').val();
        var url = "<?php echo base_url(); ?>administrator/jobpost/getuserDatabyWeek";
          $.ajax({
            type:"POST",
            url:url,
            data:{
               status : status
            },
            beforeSend: function() {
              $(".preloader-set").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
            },
            success:function(data) {

              $("#sample-data-tableweekly").html(data);
             var table = $('#sample-data-tableweekly').DataTable({ 
                 "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
                 destroy: true, //use for reinitialize datatable
                 dom: 'lBfrtip',
                 buttons: [
                  'excel', 'csv'
                 ],
              });

              if ( table.data().any() ) {
                  $(".preloader-set").html("");
              }
              
            }
          });
      })
  })

  $('#status_dropdown_monthly').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getuserDatabyMonth";
      $.ajax({
        type:"POST",
        url:url,
        data:{
           status : status
        },
        beforeSend: function() {
          $(".preloader-set").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
        },
        success:function(data) {
         
          $("#sample-data-tablemonthly").html(data);
          var table = $('#sample-data-tablemonthly').DataTable({ 
              "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
              destroy: true, //use for reinitialize datatable
              dom: 'lBfrtip',
              buttons: [
                'excel', 'csv'
              ],
          });

          if ( table.data().any() ) {
              $(".preloader-set").html("");
          }

        }
      });
  })

  $(document).ready(function() {

    $("#monthly-tab").on('click', function() {
        var status = $('#status_dropdown_monthly').val();
        var url = "<?php echo base_url(); ?>administrator/jobpost/getuserDatabyMonth";
          $.ajax({
           type:"POST",
           url:url,
           data:{
               status : status
           },
           beforeSend: function() {
            $(".preloader-set").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
           },
           success:function(data) {
             
              $("#sample-data-tablemonthly").html(data);
              var table = $('#sample-data-tablemonthly').DataTable({ 
                 "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
                 destroy: true, //use for reinitialize datatable
                 dom: 'lBfrtip',
                 buttons: [
                  'excel', 'csv'
                 ],
              });

              if ( table.data().any() ) {
                  $(".preloader-set").html("");
              }
            }
          });
      });
  })

  $('#status_dropdown_yearly').change(function() {
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>administrator/jobpost/getuserDatabyYear";
      $.ajax({
        type:"POST",
        url:url,
        data:{
           status : status
        },
        beforeSend: function() {
          $(".preloader-set").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
        },
        success:function(data) {
         
          $("#sample-data-tableyearly").html(data);
          var table = $('#sample-data-tableyearly').DataTable({ 
            "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
            destroy: true,
            dom: 'lBfrtip',
            buttons: [
              'excel', 'csv'
            ],
          });

            if ( table.data().any() ) {
                $(".preloader-set").html("");
            }
        }
      });
  })

  $(document).ready(function() {

    $("#yearly-tab").on('click', function() {

      var status = $('#status_dropdown_yearly').val();
      var url = "<?php echo base_url(); ?>administrator/jobpost/getuserDatabyYear";
        $.ajax({
          type:"POST",
          url:url,
          data:{
             status : status
          },
          beforeSend: function() {
            $(".preloader-set").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
          },
          success:function(data) {
            
            //console.log(data);

            $("#sample-data-tableyearly").html(data);

            var table = $('#sample-data-tableyearly').DataTable({ 
                "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],
                destroy: true,
                dom: 'lBfrtip',
                buttons: [
                  'excel', 'csv'
                ],
                deferRender: true
            });

            if ( table.data().any() ) {
                $(".preloader-set").html("");
            }
          }
        });
    });
  })
    
</script>


<script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker({maxDate: "0" });  
                $("#to_date").datepicker({maxDate: "0" }); 
                $('#to_date').datepicker('setDate', 'today');
                $('#from_date').datepicker('setDate', 'today-30'); 
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                var status = $('#status_dropdown_date').val();
                if(from_date == '' )  
                {  
                   alert("Please Select From Date");   
                } else if(to_date == '' )  
                {  
                   alert("Please Select To Date");   
                } else if(status == '' )  
                {  
                   alert("Please Select Status");   
                }  
                else  
                {  
                  $.ajax({  
                          url:"<?php echo base_url(); ?>administrator/jobpost/getuserDatabyDate",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date, status:status},
                          beforeSend: function() {
                            $(".preloader-set").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
                          },
                          success:function(data)  
                          {  
                              $("#sample-data-tabledate").html(data);
                              var table = $('#sample-data-tabledate').DataTable({
                                  "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],  
                                  dom: 'lBfrtip',
                                  buttons: [
                                    'excel', 'csv'
                                  ],
                                  destroy : true,
                              }); 

                              if ( table.data().any() ) {
                                  $(".preloader-set").html("");
                              }
                          }  
                     });  
                      
                }  
           });  
      }); 

      $(document).ready(function() {

        $("#date-tab").on('click', function() {

            var status = $('#status_dropdown_date').val();
             
            $.ajax({  
                url:"<?php echo base_url(); ?>administrator/jobpost/getuserDatabyMonth",  
                method:"POST",  
                data:{status:status},  
                beforeSend: function() {
                  $(".preloader-set").html('<div class="popularloader"><center><img src="https://jobyoda.com/webfiles/newone/loadericon.gif"></center></div>');
                },
                success:function(data) {
                    $("#sample-data-tabledate").html(data);
                    var table = $('#sample-data-tabledate').DataTable({
                        "lengthMenu": [[10, 25, 50,100,200, -1], [10, 25, 50, 100,200, "All"]],  
                        dom: 'lBfrtip',
                        buttons: [
                          'excel', 'csv'
                        ],
                        destroy : true,
                    }); 

                    if ( table.data().any() ) {
                        $(".preloader-set").html("");
                    }
                }  
            });

          });  
               
       });   
 </script>