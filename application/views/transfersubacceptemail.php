<html>
<head>

   
	<link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i" rel="stylesheet" />
   
	<title>Email Template</title>
	
	

	<style type="text/css" media="screen">
		/* Linked Styles */
		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4; -webkit-text-size-adjust:none }
		a { color:#777777; text-decoration:none }
		p { padding:0 !important; margin:0 !important } 
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }

				
		/* Mobile styles */
		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			.mobile-shell { width: 100% !important; min-width: 100% !important; }
			.bg { background-size: 100% auto !important; -webkit-background-size: 100% auto !important; }
			
			.text-header,
			.m-center { text-align: center !important; }
			
			.center { margin: 0 auto !important; }
			.container { padding: 20px 10px !important }
			
			.td { width: 100% !important; min-width: 100% !important; }

			.m-br-15 { height: 15px !important; }
			.p30-15 { padding: 30px 15px !important; }
			.p0-15-30 { padding: 0px 15px 30px 15px !important; }
			.p0-15 { padding: 0px 15px !important; }
			.pt0 { padding-top: 0px !important; }
			.mpb30 { padding-bottom: 30px !important; }
			.mpb15 { padding-bottom: 15px !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

			.column,
			.column-dir,
			.column-top,
			.column-empty,
			.column-empty2,
			.column-dir-top { float: left !important; width: 100% !important; display: block !important; }

			.column-empty { padding-bottom: 30px !important; }
			.column-empty2 { padding-bottom: 10px !important; }

			.content-spacing { width: 15px !important; }
		}
	</style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" valign="top">
				<!-- Header -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center" class="p30-15" style="padding: 30px 0 0;">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;background: #06a045;">
										<!-- Header -->
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<th class="column" width="145" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="img m-center" style="font-size:0pt; line-height:0pt; text-align:left; padding: 10px 8px;"><img src="https://jobyoda.com/recruiterfiles/images/jobyoDa.png" width="150" height="29" border="0"></td>
														</tr>
													</table>
												</th>
												<th class="column-empty2" width="1" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;"></th>
												<th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															
														</tr>
													</table>
												</th>
											</tr>
										</table>
										<!-- END Header -->
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Header -->

				<!-- Intro -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; border:1px solid #ddd; border-bottom:0px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											
											<tr>
												<td class="bbrr" bgcolor="#ffffff" style="border-radius:0px 0px 12px 12px;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="p30-15" style="padding: 50px 30px 60px 30px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="h3 center pb25" style="color:#000000; font-family:'Lato', Arial ,sans-serif; font-size:24px; line-height:32px; font-weight:bold; text-align:center; padding-bottom:25px;">JobYoDA Transfer of Ownership Approval</td>
																	</tr>
																	<tr>
																		<td class="text-center pb25" style="color:#777777; font-family:'Lato', Arial,sans-serif; font-size:17px; line-height:30px; padding-bottom:25px;">
																			Hi <strong><?php echo $subreqdata['name']; ?>,</strong><br>
																			
																			<p>You are now the Super User for JobYoDA account at <?php echo $reqdata[0]['cname']; ?></p>

																			<p> You can now login to your account using credentials below : </p>

																			<p style="color:orange;"><a href="<?php echo base_url();?>recruiter"  style="color:orange;"> <?php echo base_url();?>recruiter </a></p>

																			<p>Email  : <?php echo $subreqdata['email']; ?></p>
																			<p>Password  : <?php echo $password; ?></p>

																			<p></p>
																			<p>Thank You,</p>
																			<p>JobYoDA Team</p>
																		</td>
																	</tr>
																	
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<!-- END Intro -->
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Intro -->

				<!-- Footer -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
					<tr>
						<td valign="top" align="center" class="p30-15" style="padding: 0px 0px 50px 0px;">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; border:1px solid #ddd; border-top:0px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											
											<tr>
												<td class="text-footer1 pb10" style="color:#999999; font-family:'Lato', Arial,sans-serif; font-size:14px; line-height:20px; text-align:center; padding-bottom:10px;">JobYoDA - All Rights Reserved @2019</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Footer -->
			</td>
		</tr>
	</table>
</body>
</html>
