<?php
$getPlan = $this->session->userdata('userSubscriptionSession');
?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
	     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/individual/css-candidate_single.css" />
    
   </head>
   <style>
      .CandidateProfile .CandidateInfo {
         padding: 22px 250px 0 170px!important;
      }
      .CandidateProfile{min-height: 178px!important;}
      a.Back{float:left!important;}
      .CandidateProfile .DownloadCV a{    padding: 11px 14px!important;}
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>

        <!--
         <section class="overlape">
            <div class="block no-padding">
               <div data-velocity="-.1"></div> 
               <div class="container fluid">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="inner-header">
                           <div class="container">
                              <div class="row">
                                 <div class="col-lg-6">
                                    <div class="skills-btn">
                                    </div>
                                 </div>
                                 <div class="col-lg-6">
                                    <div class="action-inner">
                                       <a href="#" title=""><i class="la la-paper-plane"></i>Save Resume</a>
                                       <a href="#" title=""><i class="la la-envelope-o"></i>Contact David</a>
                                       </div> 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
        -->
  
         <section class="overlape" style="padding: 155px 0 0 0">
            <div class="block remove-top">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <a href="javascript:void(0)" class="Back" onclick="goBack()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Back</a>
                        <div class="CandidateProfile">
                           <?php
                              if($candidatesApplied1) {
                                foreach($candidatesApplied1 as $applied) {
                              ?> 
                           <div class="CandidateInfo">
                              <figure><?php
                                 if(@getimagesize($applied['profilePic'])) {
                                 ?>
                                 <img src="<?php echo $applied['profilePic']; ?>" alt="">
                                 
                                 <?php
                                    } else {
                                    ?>
                                 <img src="<?php echo base_url().'recruiterfiles/';?>images/man.png" alt="">
                                 <?php
                                    }
                                    ?>
                              </figure>
                              <h3><?php echo $applied['name']; ?></h3>
                              <h4><i><?php echo ucwords($applied['title']); ?></i> <?php if(strlen($applied['company'])>1) {?> at <?php echo ucwords($applied['company']); ?> <?php } ?> </h4>

                              <ul>
                                 <li><span><i class="fa fa-envelope" aria-hidden="true"></i></span> <?php echo $applied['email']; ?></li>
                                 <li><span><i class="fa fa-user" aria-hidden="true"></i></span> Member Since, <?php echo date("Y", strtotime($applied['created_at'])) ; ?></li> 
                                 <li><span><i class="la la-map-marker"></i></span> <?php echo $applied['location']; ?></li>
                                 <li><span><i class="fa fa-phone" aria-hidden="true"></i></span> <?php echo $applied['phone']; ?></li>
                                 <li><span><i class="fa fa-globe" aria-hidden="true"></i></span> <?php echo $applied['nationality']; ?></li>
                              </ul>

                              <!-- <p><?php echo $applied['email']; ?></p>
                              <p>Member Since, <?php echo date("Y", strtotime($applied['created_at'])) ; ?></p>
                              <p><i class="la la-map-marker"></i><?php echo $applied['location']; ?></p> -->
                              <div class="DownloadCV">
                            <?php
                              if($applied['resume']) {
                            ?>

                              <?php
                                 if(count($getPlan) > 0 && $getPlan['remaining_resume_access'] > 0) {
                              ?>
                                    <a href="javascript:void(0)" onclick="resumefunction('<?php echo $applied['resume']; ?>')" title="">Download CV <i class="la la-download"></i></a>
                              <?php
                                 } else {
                              ?>
                                    <a href="javascript:void(0)" title="">Download CV <i class="la la-download"></i></a>
                              <?php
                                 }
                              ?>

                            <?php
                              }else{
                            ?>
                            <a href="javascript:void(0)" title=""> No Resume attached </a>
                            <?php }?>
                           </div>
                           </div>

                           
                           <?php }} ?>

                        </div>

                        <div class="CandidateWork">
                           <div class="row">
                              <div class="col-sm-9">
                                 <div class="CandidateLeft">
                                    <ul>
                                       <?php
                                          if($candidateEducation) {
                                       ?>
                                       <li>
                                          <a href="#education">Education</a>
                                       </li>
                                       <?php } ?>

                                       <li>
                                          <a href="#experience">Work Experience</a>
                                       </li>

                                       <?php if($candidateSkills || @$applied['superpower']) { ?>
                                       <li>
                                          <a href="#skills">Professional Skills</a>
                                       </li> 
                                       <?php } ?>

                                       <?php
                                             if($candidateLang) { 
                                       ?>
                                       <li>
                                          <a href="#languages">Languages Spoken</a>
                                       </li>
                                       <?php } ?>

                                       <?php if($candidateAssessment && ($candidateAssessment[0]['verbal']>0 || $candidateAssessment[0]['written']>0 || $candidateAssessment[0]['listening']>0 || $candidateAssessment[0]['problem']>0)){ ?>
                                       <li>
                                          <a href="#assessment" >User Assessment</a>
                                       </li>
                                       <?php }?>
                                       
                                       <?php
                                          if($candidateExpert && $candidateExpert[0]['expert']!="") { ?>
                                       <li>
                                          <a href="#expert">Industry Expert</a>
                                       </li>
                                       <?php } ?> 
                                    </ul>
                                 </div>
                                 <div class="CandidateRight">
                                     

                                    <?php
                                       if($candidateEducation) {
                                    ?>
                                       <div class="edu-history-sec" id="education">
                                             
                                             <h2 class="Title">Education</h2>
                                             <?php
                                                foreach($candidateEducation as $education) {
                                                ?>

                                             <div class="Educationss">
                                                <span>
                                                <i class="la la-graduation-cap"></i> </span>
                                                   <h3><?php echo ucwords($education['university']);  ?></h3>
                                                   <h4><?php echo $education['degreeFrom']; ?> - <?php echo $education['degreeTo']; ?></h4>
                                                   <p><?php echo ucwords($education['attainment']);  ?> <i><?php echo ucwords($education['degree']);  ?></i></p>
                                                 
                                             </div>

                                             <?php }?>
                                          </div>
                                       <?php }?>

                                          <div class="edu-history-sec" id="experience">
                                             <?php if(!empty($applied['exp_month']) || !empty($applied['exp_year'])){ 
                                             if($applied['exp_month']>0){
                                                $workmonth= $applied['exp_month'].' Months';
                                             }else{
                                                $workmonth = $applied['exp_month'].' Month';
                                             }
                                             if($applied['exp_year']>0){
                                                $workyear= $applied['exp_year'].' Years';
                                             }else{
                                                $workyear = $applied['exp_year'].' Year';
                                             }
                                             ?>
                                             <h2 class="Title">Total Experience</h2>
                                             <span style=" font-size: 14px; margin: 0 0 20px 0; display: block; font-weight: 600; color: #424141;"><?php echo $workyear.' '.$workmonth; ?></span>
                                             <?php }?>
                                                <?php
                                                   if($candidateExp) {
                                                       ?>
                                                <h2 class="Title">Work & Experience</h2>
                                                <?php 
                                                   foreach($candidateExp as $experience) {
                                                   ?>
                                                <div class="edu-history style2">
                                                   <i></i>
                                                   <div class="edu-hisinfo">
                                                      <h3><?php echo ucwords($experience['title']);  ?> <span><?php echo ucwords($experience['company']);  ?></span></h3>
                                                      <i><?php echo $experience['workFrom']; ?> - <?php if($experience['workTo']==''){ echo $experience['workTo']; }else{ echo "Present"; }?></i>
                                                      <p><?php echo $experience['jobDesc'];  ?></p>
                                                   </div>
                                                </div>
                                                <?php }}?>
                                          </div>

                                          <?php if($candidateSkills || @$applied['superpower']){ ?>

                                          <div class="progress-sec" id="skills">
                                             <?php if($applied['superpower']){ ?>
                                             <h2 class="Title">Superpower</h2>
                                             <p style=" margin: 0 0 20px 0; line-height: 18px; font-size: 15px; font-weight: 600;"><?php echo $applied['superpower']; ?></p>
                                             <?php }?>

                                             <?php if(strlen(@$applied['jobsInterested']) >1){ ?>
                                             <h2 class="Title">Interested In</h2>
                                             <p style=" margin: 0 0 20px 0; line-height: 18px; font-size: 15px; font-weight: 600;"><?php echo $applied['jobsInterested']; ?></p>
                                             <?php }?>

                                             <?php
                                                if($candidateSkills) { ?>
                                             <h2 class="Title">Professional Skills</h2>
                                             <ul>
                                             <?php foreach($candidateSkills as $skills) {
                                                ?>
                                             <li>
                                                <span> <i class="la la-certificate"></i></span> <?php echo ucwords($skills['skills']); ?>
                                             </li>
                                             <?php }} ?>
                                             </ul>
                                          </div>
                                          <?php } ?>

                                          <?php
                                             if($candidateLang) { 
                                          ?>

                                          <div class="progress-sec" id="languages">
                                             <?php
                                                if($candidateLang) { ?>
                                             <h2 class="Title">Languages Spoken</h2>
                                             <?php
                                                foreach($candidateLang as $lang) {
                                                ?>
                                             <div class="progress-sec1">
                                                <span> <i class="la la-language la-3x"></i> <?php echo ucwords($lang['name']); ?></span>
                                             </div>
                                             <?php }} ?>
                                          </div>
                                          <?php } ?>


                                          <?php if($candidateAssessment && ($candidateAssessment[0]['verbal']>0 || $candidateAssessment[0]['written']>0 || $candidateAssessment[0]['listening']>0 || $candidateAssessment[0]['problem']>0)){ ?>

                                          <div class="progress-sec" id="assessment">
                                             <?php if($candidateAssessment && ($candidateAssessment[0]['verbal']>0 || $candidateAssessment[0]['written']>0 || $candidateAssessment[0]['listening']>0 || $candidateAssessment[0]['problem']>0)){ ?>
                                             <h2 class="Title">User Assessment</h2>
                                             <?php foreach($candidateAssessment as $assessment){ ?>
                                             <div class="progress-sec">
                                                <span>Verbal</span>
                                                <div class="progressbar">
                                                   <div class="progress" style="width: <?php echo $assessment['verbal']*20; ?>%;"><span><?php echo $assessment['verbal']*20; ?>%</span></div>
                                                </div>
                                             </div>
                                             <div class="progress-sec">
                                                <span>Written</span>
                                                <div class="progressbar">
                                                   <div class="progress" style="width: <?php echo $assessment['written']*20; ?>%;"><span><?php echo $assessment['written']*20; ?>%</span></div>
                                                </div>
                                             </div>
                                             <div class="progress-sec">
                                                <span>Listening</span>
                                                <div class="progressbar">
                                                   <div class="progress" style="width: <?php echo $assessment['listening']*20; ?>%;"><span><?php echo $assessment['listening']*20; ?>%</span></div>
                                                </div>
                                             </div>
                                             <div class="progress-sec">
                                                <span>Problem</span>
                                                <div class="progressbar">
                                                   <div class="progress" style="width: <?php echo $assessment['problem']*20; ?>%;"><span><?php echo $assessment['problem']*20; ?>%</span></div>
                                                </div>
                                             </div>
                                             <?php }} ?>
                                          </div>
                                          <?php } ?>


                                          <?php
                                                //print_r($candidateExpert);
                                                if($candidateExpert && $candidateExpert[0]['expert']!="") { ?>

                                          <div class="progress-sec" id="expert">
                                             <?php
                                                //print_r($candidateExpert);
                                                if($candidateExpert && $candidateExpert[0]['expert']!="") { ?>
                                             <h2 class="Title">Industry Expert</h2>
                                             <?php 
                                                $userExpert = $candidateExpert[0]['expert'];
                                                $userExpert = explode(",", $userExpert);
                                                //print_r($userExpert);die;
                                                foreach($userExpert as $expert) {
                                                  if($expert=='1'){
                                                      $expert="Automative";
                                                  }
                                                  if($expert=='2'){
                                                      $expert="banking and financial services";
                                                  }if($expert=='3'){
                                                      $expert="consumer electronics";
                                                  }if($expert=='4'){
                                                      $expert="healthcare and pharmaceuticals";
                                                  }if($expert=='7'){
                                                      $expert="technology";
                                                  }if($expert=='5'){
                                                      $expert="media and communication";
                                                  }if($expert=='6'){
                                                      $expert="retail and ecommerce";
                                                  }if($expert=='8'){
                                                      $expert="travel, transportation and tourism";
                                                  }
                                                ?>
                                             <div class="progress-sec1">
                                                <span> <i class="la la-industry la-3x"></i> <?php echo ucwords($expert); ?></span>
                                             </div>
                                             <?php }}
                                                $userSession = $this->session->userdata('userSession');
                                                ?>
                                          </div>

                                       <?php }?>

                                       <?php if($candidateClients){ ?>
                                          <div class="Companies_Follow">
                                             <?php if($candidateClients){ ?>
                                             <h2 class="Title">Companies Followed By</h2>
                                             <?php }?> 
                                                <ul>
                                                   <?php if($candidateClients){ 
                                                      foreach($candidateClients as $clients){
                                                      ?>
                                                   <li>
                                                      <span> <i class="la la-building"></i> </span> <?php echo ucwords($clients['clients']); ?>
                                                   </li>
                                                   <?php }} ?>
                                                </ul> 
                                          </div>
                                       <?php } ?>
                                     
                                 </div>



                                    

                              </div>
                              <div class="col-lg-3 column"> 
                                 <div class="quick-form-job">
                                    <h3>Contact applicant via email</h3>
                                    <?php if($this->session->tempdata('contactSuccess')) {?>
                                    <p class="insertMsg"><?php echo $this->session->tempdata('contactSuccess'); ?></p>
                                    <?php } ?>
                                    <form action="<?php echo base_url();?>recruiter/candidate/contact" method="post">
                                       <input type="text" placeholder="Enter your Name *" name="name" required/>
                                       <?php
                                          $userSession = $this->session->userdata('userSession');
                                       ?>
                                       <input type="text" id="recruiter_email" placeholder="Email Address*"  name="email"  value="<?php echo $userSession['email'] ?>" />
                                       <input type="text" placeholder="Phone Number" name="phone" required/>
                                       <textarea name="message" placeholder="Message should atleast have 50 characters" required></textarea>
                                       <input type="hidden" name="user_id" value="<?php echo $_GET['id']; ?>">
                                       <?php
                                          if($candidatesApplied1) {
                                            foreach($candidatesApplied1 as $applied) {
                                          ?> 
                                       <input type="hidden" name="user_email" value="<?php echo $applied['email']; ?>">
                                       <input type="hidden" name="user_name" value="<?php echo $applied['name']; ?>">
                                       <?php }}?>
                                       
                                       <?php
                                          if(count($getPlan) > 0 && $getPlan['remaining_mail'] > 0) {
                                       ?>
                                             <button class="submit">Send Email</button>
                                       <?php
                                          } else {
                                       ?>
                                             <a class="submit" href="javascript:void(0)">Send Email</a>
                                       <?php
                                          }
                                       ?>
                                       <!--<span><a href="#" title="">Click here</a> to view and accept our Terms and Conditions</span>-->
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <!-- <ul class="cand-extralink">
                           <li><a href="#education" title="">Education</a></li>
                           <li><a href="#experience" title="">Work Experience</a></li>
                           <li><a href="#skills" title="">Professional Skills</a></li>
                           <li><a href="#languages" title="">Languages Spoken</a></li>
                           <li><a href="#assessment" title="">User Assessment</a></li>
                           <li><a href="#expert" title="">Industry Expert</a></li>
                        </ul>
                        <div class="cand-details-sec">
                           <div class="row">
                              <div class="col-lg-8 column">
                                 <div class="cand-details" id="about">
                                    <div class="edu-history-sec" id="education">
                                    <?php //if($applied['education']){ ?>
                                       <h2>Education</h2>
                                       <span><?php //echo $applied['education']; ?></span>
                                       <?php //}?>
                                       <?php
                                          //if($candidateEducation) {?>
                                       <h2>Education</h2>
                                       <?php
                                          //foreach($candidateEducation as $education) {
                                          ?>
                                       <div class="edu-history">
                                          <i class="la la-graduation-cap"></i>
                                          <div class="edu-hisinfo">
                                             <h3><?php //echo ucwords($education['university']);  ?></h3>
                                             <i><?php //echo $education['degreeFrom']; ?> - <?php //echo $education['degreeTo']; ?></i>
                                             <span><?php //echo ucwords($education['attainment']);  ?> <i><?php //echo ucwords($education['degree']);  ?></i></span>
                                          </div>
                                       </div>
                                       <?php //}}?>
                                    </div>
                                    <div class="edu-history-sec" id="experience">
                                    <?php //if(!empty($applied['exp_month']) || !empty($applied['exp_year'])){ 
                                       //if($applied['exp_month']>0){
                                         // $workmonth= $applied['exp_month'].' Months';
                                       //}else{
                                         // $workmonth = $applied['exp_month'].' Month';
                                       //}
                                       //if($applied['exp_year']>0){
                                         // $workyear= $applied['exp_year'].' Years';
                                       //}else{
                                         // $workyear = $applied['exp_year'].' Year';
                                       //}
                                       ?>
                                    <h2>Work Experience</h2>
                                    <span><?php //echo $workyear.' '.$workmonth; ?></span>
                                    <?php //}?>
                                       <?php
                                         //if($candidateExp){
                                              ?>
                                       <h2>Work & Experience</h2>
                                       
                                       <?php 
                                          //foreach($candidateExp as $experience) {
                                          ?>
                                       <div class="edu-history style2">
                                          <i></i>
                                          <div class="edu-hisinfo">
                                             <h3><?php //echo ucwords($experience['title']);  ?> <span><?php //echo ucwords($experience['company']);  ?></span></h3>
                                             <i><?php //echo $experience['workFrom']; ?> - <?php //echo $experience['workTo']; ?></i>
                                             <p><?php //echo $experience['jobDesc'];  ?></p>
                                          </div>
                                       </div>
                                       <?php //}}?>
                                    </div>
                                    <div class="progress-sec" id="skills">
                                       
                                       <?php //if($applied['superpower']){ ?>
                                       <h2>Superpower</h2>
                                       <span><?php //echo $applied['superpower']; ?></span>
                                       <?php //}?>
                                       <?php
                                          //if($candidateSkills) { ?>
                                       <h2>Professional Skills</h2>
                                       <?php //foreach($candidateSkills as $skills) {
                                          ?>
                                       <div class="progress-sec1">
                                          <span> <i class="la la-certificate la-3x"></i> <?php //echo ucwords($skills['skills']); ?></span>
                                       </div>
                                       <?php //}} ?>
                                    </div>
                                    <div class="companyies-fol-sec">
                                       <?php //if($candidateClients){ ?>
                                       <h2>Companies Followed By</h2>
                                       <?php //}?>
                                       <div class="cmp-follow">
                                          <div class="row">
                                             <?php //if($candidateClients){ 
                                                //foreach($candidateClients as $clients){
                                                ?>
                                             <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                                <span> <i class="la la-building la-3x"></i> <?php //echo ucwords($clients['clients']); ?></span>
                                             </div>
                                             <?php //}} ?>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="progress-sec" id="languages">
                                       <?php
                                          //if($candidateLang) { ?>
                                       <h2>Languages Spoken</h2>
                                       <?php
                                          //foreach($candidateLang as $lang) {
                                          ?>
                                       <div class="progress-sec1">
                                          <span> <i class="la la-language la-3x"></i> <?php ///echo ucwords($lang['name']); ?></span>
                                       </div>
                                       <?php //}} ?>
                                    </div>

                                    <div class="progress-sec" id="assessment">
                                       <?php //if($candidateAssessment && ($candidateAssessment[0]['verbal']>0 || $candidateAssessment[0]['written']>0 || $candidateAssessment[0]['listening']>0 || $candidateAssessment[0]['problem']>0)){ ?>
                                       <h2>User Assessment</h2>
                                       <?php //foreach($candidateAssessment as $assessment){ ?>
                                       <div class="progress-sec">
                                          <span>Verbal</span>
                                          <div class="progressbar">
                                             <div class="progress" style="width: <?php //echo $assessment['verbal']*20; ?>%;"><span><?php //echo $assessment['verbal']*20; ?>%</span></div>
                                          </div>
                                       </div>
                                       <div class="progress-sec">
                                          <span>Written</span>
                                          <div class="progressbar">
                                             <div class="progress" style="width: <?php //echo $assessment['written']*20; ?>%;"><span><?php //echo $assessment['written']*20; ?>%</span></div>
                                          </div>
                                       </div>
                                       <div class="progress-sec">
                                          <span>Listening</span>
                                          <div class="progressbar">
                                             <div class="progress" style="width: <?php //echo $assessment['listening']*20; ?>%;"><span><?php //echo $assessment['listening']*20; ?>%</span></div>
                                          </div>
                                       </div>
                                       <div class="progress-sec">
                                          <span>Problem</span>
                                          <div class="progressbar">
                                             <div class="progress" style="width: <?php //echo $assessment['problem']*20; ?>%;"><span><?php //echo $assessment['problem']*20; ?>%</span></div>
                                          </div>
                                       </div>
                                       <?php //}} ?>
                                    </div>
                                    <div class="progress-sec" id="expert">
                                       <?php 
                                          //if($candidateExpert && $candidateExpert[0]['expert']!="") { ?>
                                       <h2>Industry Expert</h2>
                                       <?php 
                                          //$userExpert = $candidateExpert[0]['expert'];
                                          //$userExpert = explode(",", $userExpert); 
                                          //foreach($userExpert as $expert) {
                                          //  if($expert=='1'){
                                          //      $expert="Automative";
                                          //  }
                                          //  if($expert=='2'){
                                          //      $expert="banking and financial services";
                                          //  }if($expert=='3'){
                                          //      $expert="consumer electronics";
                                          //  }if($expert=='4'){
                                          //      $expert="healthcare and pharmaceuticals";
                                          //  }if($expert=='7'){
                                          //      $expert="technology";
                                          //  }if($expert=='5'){
                                          //      $expert="media and communication";
                                          //  }if($expert=='6'){
                                          //      $expert="retail and ecommerce";
                                          //  }if($expert=='8'){
                                          //      $expert="travel, transportation and tourism";
                                          //  }
                                          ?>
                                       <div class="progress-sec1">
                                          <span> <i class="la la-industry la-3x"></i> <?php //echo ucwords($expert); ?></span>
                                       </div>
                                       <?php //}}
                                          //$userSession = $this->session->userdata('userSession');
                                          ?>
                                    </div>
                                 </div>
                              </div>

                              <div class="col-lg-4 column"> 
                                 <div class="quick-form-job">
                                    <h3>Contact</h3>
                                    <?php //if($this->session->tempdata('contactSuccess')) {?>
                                    <p class="insertMsg"><?php //echo $this->session->tempdata('contactSuccess'); ?></p>
                                    <?php //} ?>
                                    <form action="<?php //echo base_url();?>recruiter/candidate/contact" method="post">
                                       <input type="text" placeholder="Enter your Name *" name="name" required/>
                                       <input type="text" id="recruiter_email" placeholder="Email Address*"  name="email"  value="<?php //echo $userSession['email'] ?>" />
                                       <input type="text" placeholder="Phone Number" name="phone" required/>
                                       <textarea name="message" placeholder="Message should have more than 50 characters" required></textarea>
                                       <input type="hidden" name="user_id" value="<?php //echo $_GET['id']; ?>">
                                       <?php
                                          //if($candidatesApplied1) {
                                           // foreach($candidatesApplied1 as $applied) {
                                          ?> 
                                       <input type="hidden" name="user_email" value="<?php //echo $applied['email']; ?>">
                                       <input type="hidden" name="user_name" value="<?php //echo $applied['name']; ?>">
                                       <?php //}}?>
                                       <button class="submit">Send Email</button> 
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div> -->



                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <?php include_once('footer.php');?>
      <!-- SIGNUP POPUP -->
      <div class="coverletter-popup">
         <div class="cover-letter">
            <i class="la la-close close-letter"></i>
            <h3>Ali TUFAN - UX / UI Designer</h3>
            <p>My name is Ali TUFAN I am thrilled to be applying for the [position] role in your company. After reviewing your job description, it’s clear that you’re looking for an enthusiastic applicant that can be relied upon to fully engage with the role and develop professionally in a self-motivated manner. Given these requirements, I believe I am the perfect candidate for the job.</p>
         </div>
      </div>
      <div class="contactus-popup">
         <div class="contact-popup">
            <i class="la la-close close-contact"></i>
            <h3>Send Message to “Ali TUFAN”</h3>
            <form>
               <div class="popup-field">
                  <input type="text" placeholder="Tera Planer" />
                  <i class="la la-user"></i>
               </div>
               <div class="popup-field">
                  <input type="text" placeholder="demo@jobhunt.com" />
                  <i class="la la-envelope-o"></i>
               </div>
               <div class="popup-field">
                  <input type="text" placeholder="+90 538 845 09 85" />
                  <i class="la la-phone"></i>
               </div>
               <div class="popup-field">
                  <textarea placeholder="Message"></textarea>
               </div>
               <button type="submit">Send Message</button>
            </form>
         </div>
      </div>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>

      <script>
function goBack() {
  window.history.go(-1);
}
</script>

      <script type="text/javascript">
         $(window).scroll(function() {    
          var scroll = $(window).scrollTop();

          if (scroll >= 300) {
              $(".CandidateLeft").addClass("darkHeader");
              $(".CandidateRight").addClass("dark");
          } else {
              $(".CandidateLeft").removeClass("darkHeader");
              $(".CandidateRight").removeClass("dark");
          }
      });
      </script>
      <script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

<script>
   function resumefunction(url) {

      $.ajax({
          type: "GET",
          url: "<?php echo base_url(); ?>" + "recruiter/candidate/updateplanresume",
          success: function(data) {
               
               window.open(url,'_blank');
          }
      });
   }

</script>

   </body>
</html>


