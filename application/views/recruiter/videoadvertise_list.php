<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoda</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <link rel="icon" href="<?php echo base_url().'recruiterfiles/';?>images/fav.png" type="image/png" sizes="16x16">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      
   </head>
   <style>
div#msgModal i.fa.fa-check {
    padding: 29px 27px;
    background: #26ae61;
    color: #fff;
    border-radius: 100%;
    font-size: 30px;
}
.dataTables_length label{text-transform: capitalize!important;}
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .main {
    float: left;
    width: 100%;
    padding:20px 0px 20px 0px;
    
}
.main label {
    margin: 0px 0px;
    padding: 0px 22px;
}
      .main label::before, label::after
      
      {
          display:none;
      }
      .searchs {
    float: right;
}
.search {
    float: left;
}
.manage-jobs-sec.boostjbs.addscroll {
    overflow-x: scroll;
    overflow-y: hidden;
}
      .manage-jobs-sec > table {
    float: left;
    margin-top: 25px;
    margin-bottom: 60px;
    margin-left: 30px;
    width: 1800px;
}
.manage-jobs-sec table thead tr td {
    width: 183px !important;
    padding: 6px 8px;
}
.manage-jobs-sec > table tbody td {
    vertical-align: middle;
    padding: 29px 8px;
    border-bottom: 1px solid #e8ecec;
}
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 10px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      background: #26ae61;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .links a {
        background: #26ae61;
    color: #fff;
    padding: 11px 10px;
    font-size: 11px;
    word-break: keep-all;
    white-space: pre;
    margin-top: 10px;
}
.modal-backdrop.show{opacity: 0;}
.modal-backdrop{z-index:0;}
.success_msg {
    color: #26ae61;
    margin-bottom: 0px;
    font-size: 16px;
}

div#msgModal {
    text-align: center;
}

input[type="checkbox"] {
    opacity: 1;
    float: none;
    position: relative;
    z-index: 9;
    visibility: visible;
}

table.dataTable tbody td button {
    font-size: 13px;
    padding: 8px 12px;
}

.boostjbs td img {
    width: 37px;
}

   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                    <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 column psthbs MainWrapper">
                        <div class="padding-left transactdto">
                           <div class="manage-jobs-sec">
                              <h3>Advertisement Listing</h3>
                           </div>
                           
                           <div class="manage-jobs-sec boostjbs">
                           <div class="col-md-12">
                           <div class="main">
                                
                           </div>
                           </div>
                           
						                <div class="table table-responsive">
                              <table id="sample-data-table" class="table">
                                <thead>
                                   <tr>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">ID</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Banner</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Title</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Description</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Start Date</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">End Date</span>
                                         </div>
                                      </th>
                                      
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Action</span>
                                         </div>
                                      </th>
                                      
                                   </tr>
                                </thead>
                                <tbody>
                                   <?php
                                      $x1 =1;
                                      if(isset($recruitFetch)) {
                                        //var_dump($recruitFetch);
                                        //die;
                                        foreach($recruitFetch as $jobList) {
                                    ?>
                                           <tr>
                                              <td><?php echo $x1;?></td>
                                              <td style=""><img  class="locned" src="<?php if(isset($jobList['thumbnail'])){ echo $jobList['thumbnail'];}?>"></td>
                                              <td><?php echo $jobList['title']; ?></td>
                                              <td><?php if(!empty($jobList['description'])){echo $jobList['description'];} ?></td>
                                              <td><?php if(!empty($jobList['start_date'])){echo $jobList['start_date'];} ?></td>
                                              <td><?php if(!empty($jobList['end_date'])){echo $jobList['end_date'];} ?></td>
                                              
                                              <td style="width:100px;">

                                               <a title="Delete" href="#" data-toggle="modal" id="<?php echo $jobList['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" class="deleicd btn-icon usricos" aria-label="Product details"><i class="fa fa-trash"></i></a>

                                             </td>
                                           </tr>
                                   <?php
                                      $x1++;
                                      }
                                    }
                                   ?>
                                </tbody>
                             </table>
                             </div>
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <div class="account-popup-area signin-popup-box">
         <div class="account-popup">
            <span class="close-popup"><i class="la la-close"></i></span>
            <h3>User Login</h3>
            <span>Click To Login With Demo User</span>
            <div class="select-user">
               <span>Candidate</span>
               <span>Employer</span>
            </div>
            <form>
               <div class="cfield">
                  <input type="text" placeholder="Username" />
                  <i class="la la-user"></i>
               </div>
               <div class="cfield">
                  <input type="password" placeholder="********" />
                  <i class="la la-key"></i>
               </div>
               <p class="remember-label">
                  <input type="checkbox" name="cb" id="cb1"><label for="cb1">Remember me</label>
               </p>
               <a href="#" title="">Forgot Password?</a>
               <button type="submit">Login</button>
            </form>
            <div class="extra-login">
               <span>Or</span>
               <div class="login-social">
                  <a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
                  <a class="tw-login" href="#" title=""><i class="fa fa-twitter"></i></a>
               </div>
            </div>
         </div>
      </div>
      <!-- LOGIN POPUP -->
      <div class="account-popup-area signup-popup-box">
         <div class="account-popup">
            <span class="close-popup"><i class="la la-close"></i></span>
            <h3>Sign Up</h3>
            <div class="select-user">
               <span>Candidate</span>
               <span>Employer</span>
            </div>
            <form>
               <div class="cfield">
                  <input type="text" placeholder="Username" />
                  <i class="la la-user"></i>
               </div>
               <div class="cfield">
                  <input type="password" placeholder="********" />
                  <i class="la la-key"></i>
               </div>
               <div class="cfield">
                  <input type="text" placeholder="Email" />
                  <i class="la la-envelope-o"></i>
               </div>
               <div class="dropdown-field">
                  <select data-placeholder="Please Select Specialism" class="chosen">
                     <option>Web Development</option>
                     <option>Web Designing</option>
                     <option>Art & Culture</option>
                     <option>Reading & Writing</option>
                  </select>
               </div>
               <div class="cfield">
                  <input type="text" placeholder="Phone Number" />
                  <i class="la la-phone"></i>
               </div>
               <button type="submit">Signup</button>
            </form>
            <div class="extra-login">
               <span>Or</span>
               <div class="login-social">
                  <a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
                  <a class="tw-login" href="#" title=""><i class="fa fa-twitter"></i></a>
               </div>
            </div>
         </div>
      </div>
       <!-- Modal -->
  <div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
          <i class="fa fa-check" aria-hidden="true"></i>
          <p class="success_msg"><?php if($this->session->tempdata('inserted')){ echo $this->session->tempdata('inserted'); } ?></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Advertisement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this Advertisement?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink" style="background: red;
    border: red;">Delete</a>
            </div>
        </div>
    </div>
</div>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <!-- SIGNUP POPUP -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/jquery/dist/jquery.min.js"></script>
     <!-- <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>-->
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
     <!-- Data tables -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables.net/js/jquery.dataTables.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables-responsive/js/dataTables.responsive.js"></script>           
           
      <!-- Data tables -->
      <script type="text/javascript">
      $(document).ready(function(){  
        $('#sample-data-table').DataTable({
             "searching": true,
        });
      });

      function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>recruiter/recruiter/deleteVideoadvertise?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }    
     </script>

     <?php
         if($this->session->tempdata('inserted')!='') {
             echo($this->session->tempdata('inserted')) ;
             /*if($this->session->tempdata('validationError')){$this->session->unset_tempdata('validationError');}*/
      ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                    $('#msgModal').modal('show');
                });
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href );
                    //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
                }
            </script>
      <?php
         }
     ?>
   </body>
</html>

