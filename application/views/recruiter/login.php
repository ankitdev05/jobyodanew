<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="icon" href="<?php echo base_url().'recruiterfiles/';?>images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <style>
        p.greentxt {
            margin: 0;
            color: #27aa60;
        }
        .inform {
            text-align: center;
            font-family: 'Open Sans', sans-serif;
        }
      </style>
   </head>
   <style>
      .rap{
      float: left;
      width: 100%;
      padding: 50px 10px;
      border: 1px solid rgba(51, 51, 51, 0.08);
      box-shadow: 0px 8px 16px -4px rgba(128, 128, 128, 0.22);
      /* margin-top: 70px; */
      height: 316px;
      }
      .rap .account-popup .cfield{
      margin-bottom: 35px
      }
      .loginmess p {
         text-align: center;
         color: #00FF00;
      }
      .validError{color:#f00;}
      .TestimonialView{    position: absolute;
    top: 30px;
    right: 30px;
    background-color: #084d87;
    color: #fff;
    font-size: 14px;
    padding: 10px 20px;
    border-radius: 5px;
    font-weight: 500;}

    .TestimonialView:hover{ background-color: #26b060; color: #fff }
   </style>
   <body style="background: #f5f5f5;">
      <div class="page-loading" style="display: none;">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/loader.gif" alt="">
         <span>Skip Loader</span>
      </div>
      <div class="theme-layout" id="scrollup">
         <div class="responsive-header">
            <div class="responsive-menubar">
               <div class="res-logo"><a href="index.html" title=""><img src="https://placehold.it/178x40" alt=""></a></div>
               <div class="menu-resaction">
                  <div class="res-openmenu">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/icon.png" alt=""> Menu
                  </div>
                  <div class="res-closemenu">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/icon2.png" alt=""> Close
                  </div>
               </div>
            </div>
         </div>
         
         <section>
            <div class="block remove-bottomms bgatype LoginArea">
                
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="account-popup-area signin-popup-box static loginfrms">
                           <div class="jobyodaformlogo">
                            <a href="<?php echo base_url();?>recruiter">
                              <img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoda.png">  
                            </a>
                           </div>
                           <div class="account-popup">
                              <div class="rap">
                                 <h4 style="float: left; margin: 0 0 15px 0;">Sign in</h4>
                                 <div style="clear:both;"></div>
                                 <div class="loginmess">
                                    <?php  if($this->session->tempdata('changesuccess') != null) { 
                                    ?>
                                            <p class="text-success"><?php echo $this->session->tempdata('changesuccess'); ?></p>
                                    <?php
                                            }
                                    ?>
                                    <?php if($this->session->tempdata('loginerror')) {?>
                                       <p class="text-danger"><?php echo $this->session->tempdata('loginerror'); ?></p>
                                    <?php } 
                                       if(isset($verifysuccess)){ ?>
                                         <p class="text-success"><?php echo $verifysuccess; ?></p>
                                      <?php } ?>
                                    
                                 </div>
                                 <form method="post" action="<?php echo base_url();?>recruiter/recruiter/logincheck">
                                    <div class="cfield">
                                       <input type="text" placeholder="Email" name="email" value="<?php if (get_cookie('uemail')) { echo get_cookie('uemail'); } ?>">
                                       <i class="la la-user"></i>
                                    </div>
                                    <?php if(isset($errors['email'])){echo "<p class='validError'>".$errors['email']."</p>";}?>
                                    
                                    <div class="cfield">
                                       <input type="password" placeholder="Password" name="password" value="<?php if (get_cookie('upassword')) { echo get_cookie('upassword'); } ?>">
                                       <i class="la la-key"></i>
                                    </div>
                                    <?php if(isset($errors['password'])){echo "<p class='validError'>".$errors['password']."</p>";}?>
                                    
                                    <p class="remember-label">
                                       <input type="checkbox" name="remember" id="cb1" checked><label for="cb1">Remember me</label>
                                       <a href="#" title=""  data-toggle="modal" data-target="#exampleModalCenter18" data-dismiss="modal">Forgot Password?</a>
                                    </p>
                                    <div class="fullwdt">
                                       <button type="submit" class="loginlink">Login</button>
                                       <!--<button type="submit">Login</button> -->
                                    </div>
                                 </form>
                              </div>
                              <div class="sign">
                                 <p>Don't Have An Account Yet?<span><a href="<?php echo base_url();?>recruiter/signup"><strong>Sign Up Now</strong></a></span></P>
                              </div>
                           </div>
                        </div>
                        <!-- LOGIN POPUP -->
                     </div>
                  </div>
               </div>

               
            </div>
         </section>
         <?php include_once('footer.php');?>

      </div>
      <div class="account-popup-area signin-popup-box">
         <div class="account-popup">
            <span class="close-popup"><i class="la la-close"></i></span>
            <h3>User Login</h3>
            <span>Click To Login With Demo User</span>
            <div class="select-user">
               <span>Candidate</span>
               <span>Employer</span>
            </div>
            <form>
               <div class="cfield">
                  <input type="text" placeholder="Username">
                  <i class="la la-user"></i>
               </div>
               <div class="cfield">
                  <input type="password" placeholder="********">
                  <i class="la la-key"></i>
               </div>
               <p class="remember-label">
                  <input type="checkbox" name="cb" id="cbwq"><label for="cbwq">Remember me</label>
               </p>
               <a href="#" title="">Forgot Password?</a>
               <button type="submit">Login</button>
            </form>
            <div class="extra-login">
               <span>Or</span>
               <div class="login-social">
                  <a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
                  <a class="tw-login" href="#" title=""><i class="fa fa-twitter"></i></a>
               </div>
            </div>
         </div>
      </div>
      <!-- LOGIN POPUP -->
      <div class="account-popup-area signup-popup-box">
         <div class="account-popup">
            <span class="close-popup"><i class="la la-close"></i></span>
            <h3>Sign Up</h3>
            <div class="select-user">
               <span>Candidate</span>
               <span>Employer</span>
            </div>
            <form>
               <div class="cfield">
                  <input type="text" placeholder="Username">
                  <i class="la la-user"></i>
               </div>
               <div class="cfield">
                  <input type="password" placeholder="********">
                  <i class="la la-key"></i>
               </div>
               <div class="cfield">
                  <input type="text" placeholder="Email">
                  <i class="la la-envelope-o"></i>
               </div>
               <div class="dropdown-field">
                  <select data-placeholder="Please Select Specialism" class="chosen" style="display: none;">
                     <option>Web Development</option>
                     <option>Web Designing</option>
                     <option>Art &amp; Culture</option>
                     <option>Reading &amp; Writing</option>
                  </select>
                  <div class="chosen-container chosen-container-single chosen-container-single-nosearch" title="" style="width: 0px;">
                     <a class="chosen-single">
                        <span>Web Development</span>
                        <div><b></b></div>
                     </a>
                     <div class="chosen-drop">
                        <div class="chosen-search">
                           <input class="chosen-search-input" type="text" autocomplete="off" readonly="">
                        </div>
                        <ul class="chosen-results"></ul>
                     </div>
                  </div>
               </div>
               <div class="cfield">
                  <input type="text" placeholder="Phone Number">
                  <i class="la la-phone"></i>
               </div>
               <button type="submit">Signup</button>
            </form>
            <div class="extra-login">
               <span>Or</span>
               <div class="login-social">
                  <a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
                  <a class="tw-login" href="#" title=""><i class="fa fa-twitter"></i></a>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="exampleModalCenter18" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                     <div class="filldetails" style="margin:0;">
                        <form method="post" action="<?php echo base_url();?>recruiter/recruiter/forgotPassword">
                           <div class="inform">
                            <p class="greentxt" style="margin: 0 0 10px 0; color: #000;">Forgot Password</p>
                            <?php
                              if($this->session->tempdata('forgotsuccess') != null){
                                    echo "<p>". $this->session->tempdata('forgotsuccess') ."</p>";
                                    
                              } else {
                            ?>
                              <p>Enter your registered Email id. We will send you a link and code to change the password.</p>
                              <div>
                                 <div class="forminputspswd">
                                    <input type="text" name="email" class="form-control" placeholder="Email Address" required="">
                                    <img src="<?php echo base_url().'webfiles/';?>img/msgping.png">
                                 </div>
                                 <p style="color: red;"><?php if(!empty($forgoterror)){ echo $forgoterror; } ?></p>
                                 <div class="forminputspswd loginspace">
                                    <!--<button type="submit" class="srchbtns">Login</button>-->
                                    <button type="submit" class="srchbtns Resetpassword">Reset Password</button>
                                 </div>
                              </div>
                            <?php
                              }
                            ?>
                          </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- SIGNUP POPUP -->
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/maps2.js" type="text/javascript"></script>

     <?php
         if($this->session->tempdata('forgotsuccess') != null) {
             if($this->session->tempdata('forgotsuccess')){$this->session->unset_tempdata('forgotsuccess');}
      ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                    $('#exampleModalCenter18').modal('show');
                });
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href );
                    //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
                }
            </script>
      <?php
         }else if(!empty($forgoterror)){ ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                    $('#exampleModalCenter18').modal('show');
                });
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href );
                    //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
                }
            </script>
         <?php }
     ?>
     <script>
         $('#exampleModalCenter18').on('hidden.bs.modal', function () {
             location.reload();
            })
     </script>
   </body>
</html>

