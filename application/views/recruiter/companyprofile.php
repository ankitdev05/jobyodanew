<?php $recruiterId = $this->session->userdata('userSession');?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <link rel="icon" href="<?php echo base_url().'recruiterfiles/';?>images/fav.png" type="image/png" sizes="16x16">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
   </head>
   <style>
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      .deletes {
    position: absolute;
    top: 7%;
    left: 9%!important;
    background: #47b476;
    padding: 1px 5px;
    border-radius: 10px;
    color: #fff;
}
      }
      .deletes {
          position: absolute;
          top: 5%;
          left: 13%;
          background: #47b476;
          padding: 1px 5px;
          border-radius: 10px;
          color: #fff;
      }

      .editss {
          position: absolute;
          top: 5%;
          left: 78%;
          background: #47b476;
          padding: 1px 5px;
          border-radius: 10px;
          color: #fff;
      }

      
      .Delete{    background-color: #f6ab27;
    width: 25px;
    height: 25px;
    left: auto;
    right: 2px;
    top: 45px;
    border-radius: 50%;
    text-align: center;
    line-height: 23px;}
      .Delete a{display: block;
    font-size: 12px;}
      .Edit{    position: absolute;
    top: 10px;
    left: auto;
    right: 2px;
    background-color: transparent;
    padding: 0;
    border-radius: 0;}
      .Edit a{    width: 25px;
    height: 25px;
    background-color: #47b476;
    display: block;
    text-align: center;
    font-size: 12px;
    line-height: 25px;
    border-radius: 50%;}

    .Delete:hover,
    .Edit a:hover{ background-color: #224b89 }
div#msgModal i.fa.fa-check {
    padding: 29px 27px;
    background: #26ae61;
    color: #fff;
    border-radius: 100%;
    font-size: 30px;
}

div#msgModal {
    text-align: center;
}
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                    <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 psthbs cmpprofd MainWrapper">
                        <div class="row">
                           <div class="col-md-12">
                           <!-- <span  class="succsmdf" style="color: green;"><?php if($this->session->tempdata('inserted')){ echo $this->session->tempdata('inserted'); } ?></span> -->
						   <div class="cdfts">
						   <div class="headsteps-gt">
                                             <h5>Company Information</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
										  
                              <div class="companydtls">
									
										  
                                 <div class="logoareas">
                                    <?php 
                                        if(isset($companyDetails[0]['companyPic'])) {
                                            $comLogo = $companyDetails[0]['companyPic'];
                                        } else { 
                                            $comLogo = base_url().'recruiterfiles/images/camera1.png';
                                        }
                                        //print_r($companie);
                                    ?>
                                    <img src="<?php echo $comLogo;?>" style="width:200px;height:157px;">
                                 </div>
                                 <div class="txtdesc">
                                    <div class="main-hda right">
                                       <h3><?php if(!empty($companie[0]['cname'])){ echo $companie[0]['cname']; }?></h3>
                                       <?php if(!empty($recruiterId['label']) && $recruiterId['label']==3){ 
                                          if(!empty($recruiterId['edit_delete']) && $recruiterId['edit_delete']==1){
                                          ?>
                                       
                                       <?php }}else{?>
                                          <a href="<?php echo base_url();?>recruiter/manageCompany" class="rightedits">Edit Profile  <i class="fas fa-pen"></i></a>
                                       <?php }?>
                                    </div>
                                    <p><?php if(isset($companyDetails[0]['companyDesc'])) {echo $companyDetails[0]['companyDesc'];} ?></p>
                                 </div>
                              </div>
							  
							  </div>
							
							<div class="cdfts">
						   <div class="headsteps-gt">
                                             <h5>Sites Listing</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
										  
                              <div class="companythumbs">
                                 
                                 <div class="row">
                                 <?php
                                    if($companySites) {
                                      foreach($companySites as $companySite) {
                                 ?>
                                    <div class="col-md-6 col-lg-3">
                                       <div class="siglethumbs">
                                          <a href="<?php echo base_url();?>recruiter/recruiter/companySiteView?view=<?php echo base64_encode($companySite['id']); ?>">
                                          <?php 
                                              if($companySite['companyPic']) {
                                                  $comPic = $companySite['companyPic'];
                                              } else { 
                                                  $comPic = base_url().'recruiterfiles/images/camera1.png';
                                              } 
                                          ?>
                                             <img src="<?php echo $comPic;?>">
                                             <h5><?php echo $companySite['cname']; ?></h5>
                                             <?php if(!empty($recruiterId['label']) && $recruiterId['label']==3){ 
                                                if(!empty($recruiterId['edit_delete']) && $recruiterId['edit_delete']==1){
                                                ?>
                                          </a>


                                           
                                                <!-- <div class="deletes Delete">                                             
                                                <a title="Delete" href="#" data-toggle="modal" id="<?php //echo $companySite['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" ><i class="fa fa-trash"></i></a>
                                                </div> -->
                                                <?php }}else{?>
                                                <!-- <div class="deletes Delete">
                                                <a title="Delete" href="#" data-toggle="modal" id="<?php //echo $companySite['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" ><i class="fa fa-trash"></i></a>
                                                </div> -->
                                                <?php }?>
                                                <div class="editss Edit">
                                                <a title="Edit" href="<?php echo base_url(); ?>recruiter/recruiter/editSite?view=<?php echo base64_encode($companySite['id']); ?>"  id="<?php echo $companySite['id'];?>" onclick="getrid(this.id)" ><i class="fas fa-pen"></i></a>
                                                </div>
                                              

                                             <div class="cmpaddr"><p><?php echo $companySite['address']; ?></p></div>
                                             <p style="word-wrap: break-word;"><?php echo $companySite['companyDesc']; ?></p>
                                          </a>
                                       </div>
                                    </div>
                                 <?php
                                      }
                                    }
                                 ?>
                                 <?php if(!empty($recruiterId['label']) && $recruiterId['label']==3){ 
                                    if(!empty($recruiterId['site_details']) && $recruiterId['site_details']==1){?>
                                     <div class="col-md-3 col-lg-3">
                                       <a href="<?php echo base_url();?>recruiter/recruiter/companySite"><img src="<?php echo base_url().'recruiterfiles/';?>images/addsite.png"></a>
                                    </div>
                                     <?php }}else{
                                    ?>
                                    <div class="col-md-3 col-lg-3 smallicoyda">
                                       <a href="<?php echo base_url();?>recruiter/recruiter/companySite"><img src="<?php echo base_url().'recruiterfiles/';?>images/addsite.png"></a>
                                    </div>
                                    <?php }?>
                                 </div>
                              </div>
							  </div>
                           </div>
                           <div class="col-md-12" style="margin-top:30px;">
						   <div class="stepcountfrms"> 
						   <div class="headsteps-gt">
                                             <h5>Company-wide Benefits</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                              
                               <?php if($topPicks2){ ?>
                               <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Top Picks</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <?php if($topPicks2){if(in_array(1, $topPicks2)){  ?>  
                                       <li class="<?php if($topPicks2){if(in_array(1, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="toppicks[]" value="1" >
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/joining-bonus.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/joining-bonus-1.png" class="hvrsicos">
                                          <p> Joining<br>Bonus</p>
                                       </li>
                                       <?php }} if($topPicks2){if(in_array(2, $topPicks2)){  ?>
                                       <li class="<?php if($topPicks2){if(in_array(2, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="topPicks[]" value="2">
                                        <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food.png" class="normicos">  

                           <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food-1.png" class="hvrsicos"> 						   
                                          <p> Free <br>Food</p>
                                       </li>
                                        <?php }} if($topPicks2){if(in_array(3, $topPicks2)){?>
                                       <li class="<?php if($topPicks2){if(in_array(3, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="toppicks[]" value="3">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-white.png" class="hvrsicos"> 
                                          <p>Day 1 HMO</p>
                                       </li>
                                        <?php }} if($topPicks2){if(in_array(4, $topPicks2)){?>
                                       <li class="<?php if($topPicks2){if(in_array(4, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="toppicks[]" value="4">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended-1.png" class="hvrsicos"> 
                                          <p> Day 1 HMO<br> for Dependent</p>
                                       </li>
                                        <?php }} if($topPicks2){if(in_array(5, $topPicks2)){?>
                                       <li class="<?php if($topPicks2){if(in_array(5, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="toppicks[]" value="5" >
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-shift.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-shift-1.png" class="hvrsicos">
                                          <p>Day Shift</p>
                                       </li>
                                        <?php }} if($topPicks2){if(in_array(6, $topPicks2)){?>
                                       <li class="<?php if($topPicks2){if(in_array(6, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="toppicks[]" value="6" >
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay-1.png" class="hvrsicos">
                                          <p> 14th Month Pay</p>
                                       </li>

                                    <?php }} if($topPicks2){if(in_array(7, $topPicks2)){?>
                                       <li class="<?php if($topPicks2){if(in_array(7, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="toppicks[]" value="7" >
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfh.png" class="normicos">  
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfhun.png" class="hvrsicos">
                                          <p> Work From Home </p>
                                       </li>
                                       <?php }}?>
                                    </ul>
                                 </div>
                                 
                              </div>
                              <?php } if($allowance2){?>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Allowances and Incentives</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul id="filterchekers">
                                        <?php if($allowance2){if(in_array(1, $allowance2)){  ?>
                                       <li class="tapsct <?php if($allowance2){if(in_array(1, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct1" data-value="1">
                                          <input type="checkbox" name="allowances[]" value="1">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance-1.png" class="hvrsicos">
                                          <p> Cellphone <br>Allowance</p>
                                       </li>
                                        <?php }} if($allowance2){if(in_array(2, $allowance2)){  ?>
                                       <li class="tapsct <?php if($allowance2){if(in_array(2, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct2" data-value="2">
                                          <input type="checkbox" name="allowances[]" value="2">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking-1.png" class="hvrsicos">
                                          <p>Free <br>Parking</p>
                                       </li>
                                        <?php }} if($allowance2){if(in_array(3, $allowance2)){  ?>
                                       <li class="tapsct <?php if($allowance2){if(in_array(3, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct3" data-value="3">
                                          <input type="checkbox" name="allowances[]" value="3">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle-1.png" class="hvrsicos"> 
                                          <p> Free <br> Shuttle</p>
                                       </li>
                                        <?php }} if($allowance2){if(in_array(4, $allowance2)){  ?>
                                       <li class="tapsct <?php if($allowance2){if(in_array(4, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct4" data-value="4">
                                          <input type="checkbox" name="allowances[]" value="4">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus-1.png" class="hvrsicos">
                                          <p> Annual <br> Performance Bonus</p>
                                       </li>
                                        <?php }} if($allowance2){if(in_array(5, $allowance2)){  ?>
                                       <li class="tapsct <?php if($allowance2){if(in_array(5, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct5" data-value="5">
                                          <input type="checkbox" name="allowances[]" value="5">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirment-benifits.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirments-benefits-1.png" class="hvrsicos">
                                          <p> Retirements <br> Benefits</p>
                                       </li>
                                        <?php }} if($allowance2){if(in_array(6, $allowance2)){  ?>
                                       <li class="tapsct <?php if($allowance2){if(in_array(6, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct6" data-value="6">
                                          <input type="checkbox" name="allowances[]" value="6">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation-1.png" class="hvrsicos">
                                          <p> Transport <br> Allowance</p>
                                       </li>
                                       <?php }}?>

                                      <?php if($allowance2){if(in_array(7, $allowance2)){  ?>
                                       <li class="tapsct <?php if($allowance2){if(in_array(7, $allowance2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="allowances[]" value="7">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive-1.png" class="hvrsicos">
                                          <p> Monthly Performance <br> Incentives</p>
                                       </li>
                                       <?php }}?>
                                    </ul>
                                 </div>
                              </div>
                              <?php } if($medical2){?>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Medical Benefits</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                        <?php if($medical2){if(in_array(1, $medical2)){  ?>
                                       <li class="<?php if($medical2){if(in_array(1, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="medical[]" value="1">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents-1.png" class="hvrsicos">
                                          <p> Free HMO for<br>Dependents</p>
                                       </li>
                                        <?php }} if($medical2){if(in_array(2, $medical2)){?>
                                       <li class="<?php if($medical2){if(in_array(2, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="medical[]" value="2">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits1.png" class="hvrsicos">
                                          <p> Critical Illness <br>Benefits</p>
                                       </li>
                                        <?php }} if($medical2){if(in_array(3, $medical2)){?>
                                       <li class="<?php if($medical2){if(in_array(3, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="medical[]" value="3">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence-1.png" class="hvrsicos">
                                          <p>Life <br>Insurance</p>
                                       </li>
                                        <?php }} if($medical2){if(in_array(4, $medical2)){?>
                                       <li class="<?php if($medical2){if(in_array(4, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="medical[]" value="4">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance-1.png" class="hvrsicos">
                                          <p> Maternity<br> Assistance</p>
                                       </li>
                                        <?php }} if($medical2){if(in_array(5, $medical2)){?>
                                       <li class="<?php if($medical2){if(in_array(5, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="medical[]" value="5">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer-1.png" class="hvrsicos">
                                          <p>Medicine <br>Reimbursement</p>
                                       </li>
                                       <?php }}?>
                                    </ul>
                                 </div>
                              </div>
                              <?php }if($leave2){?>
                              
                              <!--<div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Leaves</h3>
                                 </div>
                                 <div class="filterchekers">
                                   
                                        
                                       <ul>
                                           <?php  if($leave2){if(in_array(1, $leave2)){?>
                                       <li class="<?php if($leave2){if(in_array(1, $leave2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="shifts4" type="checkbox" name="leavs[]" value="1" <?php if($leave2){ if(in_array(1, $leave2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/weekend-off.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/weekend-off-1.png" class="hvrsicos">
                                          <p> Weekend Off</p>
                                       </li>
                                        <?php  }} if($leave2){if(in_array(2, $leave2)){?>
                                       <li class="<?php if($leave2){if(in_array(2, $leave2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="shifts5" type="checkbox" name="leavs[]" value="2" <?php if($leave2){ if(in_array(2, $leave2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/holiday-off.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/holiday-off-1.png" class="hvrsicos">
                                          <p>Holiday Off</p>
                                       </li>
                                        <?php }}?>
                                    
                                        
                                      
                                    </ul>
                                 </div>
                                 
                              </div>-->
                              <?php } if($workshift2){?>
                              
                              <!--<div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Work Shifts</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                        <?php  if($workshift2){if(in_array(1, $workshift2)){?>
                                       <li class="<?php if($workshift2){if(in_array(1, $workshift2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="shifts[]" value="1">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/mid-shift.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/mid-shift-1.png" class="hvrsicos">
                                          <p> Mid Shift </p>
                                       </li>
                                        <?php }} if($workshift2){if(in_array(2, $workshift2)){?>
                                       <li class="<?php if($workshift2){if(in_array(2, $workshift2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="shifts[]" value="2">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/night-shift.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/night-shift-1.png" class="hvrsicos">
                                          <p> Night Shift </p>
                                       </li>
                                        <?php }} if($workshift2){if(in_array(3, $workshift2)){?>
                                       <li class="<?php if($workshift2){if(in_array(3, $workshift2)){ echo "selectedgreen"; }}?>">
                                          <input type="checkbox" name="shifts[]" value="3">
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/24.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/24-1.png" class="hvrsicos">
                                          <p>24/7 </p>
                                       </li>
                                        
                                       <?php }}?>
                                    </ul>
                                 </div>
                                 
                              </div>-->
                              <?php }?>
							  </div>
                           </div>       
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      </section>
      <!-- Modal -->
     <div class="modal fade" id="msgModal" role="dialog">
       <div class="modal-dialog">
       
         <!-- Modal content-->
         <div class="modal-content">
           
           <div class="modal-body">
             <i class="fa fa-check" aria-hidden="true"></i>
             <p class="success_msg"><?php if($this->session->tempdata('inserted')){ echo $this->session->tempdata('inserted'); } ?></p>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
         </div>
         
       </div>
     </div>
      <div class="modal" id="myModal1" role="dialog">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title">Delete Job</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <p>Are you sure to delete this Job?</p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <a href="" class="btn btn-primary" id="createLink">Delete</a>
                  </div>
              </div>
          </div>
      </div>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script type="text/javascript">
            function getrid(id) {
              var rid = id;
              var link = "<?php echo base_url();?>recruiter/recruiter/deleteSite?id="+rid;
              var aa = document.getElementById('createLink');
              aa.setAttribute("href", link);
          }
      </script>
      <?php
         if($this->session->tempdata('inserted')!='') {
             echo($this->session->tempdata('inserted')) ;
             /*if($this->session->tempdata('validationError')){$this->session->unset_tempdata('validationError');}*/
      ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                    $('#msgModal').modal('show');
                });
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href );
                    //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
                }
            </script>
      <?php
         }
     ?>
   </body>
</html>

