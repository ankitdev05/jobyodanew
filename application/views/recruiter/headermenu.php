 
<?php
  $userData = $this->session->userdata('userSession');
  if(!empty($userData['label']) && $userData['label']=='3'){
     $subrecruiter_id = $userData['id'];
     $userSession['id'] = $userData['parent_id'];
  }else{
     $userSession['id'] = $userData['id'];
     $subrecruiter_id = 0;
  }
  $recruiterDatas = $this->Recruit_Model->recruiter_single($userData['id']);

  if($recruiterDatas[0]['email']  !== $userData['email']) {
      $this->session->unset_userdata('userSession');
      $this->session->unset_userdata('userSession11');
  }

  $recruiterDatas11 = $this->Recruit_Model->company_details_single($userData['id']);
?>

<div class="Logo">
  <a href="<?php echo base_url();?>recruiter/dashboard">
    <img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoDa.png">
  </a>
</div>
 

  <div class="ProfileArea">
  <div class="Jobyodadata">
  <?php
    $userData11 = $this->session->userdata('userSession11');
  ?>
    <ul>
      <li data-number="<?php echo $userData11['onedata']; ?>">
        <h4>Employment Opportunities</h4>
        <span id="number1"> <?php echo $userData11['onedata']; ?> </span>
        
      </li>
      <li data-number="<?php echo $userData11['twodata']; ?>">
        <h4> Jobseekers</h4>
        <span id="number2"><?php echo $userData11['twodata']; ?></span>
      </li>

      <li>
      <?php
        if($userSession['id'] == 59300000000000000000000000000000000) {
      ?>
          <h4>Search</h4>
          <form action="<?php echo base_url();?>recruiter/searchCandidates" method="post">
            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            <div class="searchbarnav">
              <input type="text" placeholder="Search candidates by Job Title" name="keyword" required>
            </div>
          </form>
      <?php
        }
      ?>
      </li>

    </ul>

    <ol>
      <li>
          
     </li>

      <li>
        <a href="<?php echo base_url();?>recruiter/recruiterprofile">
          <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofile.png">
          Recruiter Profile
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>recruiter/dashboard/logout">
          <img src="<?php echo base_url().'recruiterfiles/';?>images/logout.png">
          Logout
        </a>
      </li>
    </ol>
  </div>
  
  <div class="FacebookLink">
    <a href="https://www.facebook.com/james.deakin/videos/2645412085501619/">
      <i class="fa fa-facebook"></i>
      <span> James Deakin  </span>
    </a>
  </div>

  <div class="Profilebox">
    <?php
      if($recruiterDatas11) {
        if(strlen($recruiterDatas11[0]['companyPic']) > 0) {
    ?>
          <figure><img src="<?php echo $recruiterDatas11[0]['companyPic']; ?>"></figure>
    
    <?php } else { ?>
    
          <figure><img src="<?php echo base_url().'recruiterfiles/';?>images/placeholder.png"></figure>
    
    <?php } } else {
?>
          <figure><img src="<?php echo base_url().'recruiterfiles/';?>images/placeholder.png"></figure>
<?php
    } ?>
    
    <h1><?php echo $userData['fname'] .' '.$userData['lname']; ?></h1> 
  </div>
</div>

  <!-- <div class="FacebookLink">
  <a target="_blank" href="https://www.facebook.com/james.deakin/videos/2645412085501619/">
    <i class="fa fa-facebook"></i>
    <span>James Deakin</span>
  </a>
</div> -->
                   
				  
<!-- <div class="mebilemenuham">
   <i class="fas fa-bars"></i>
   <i class="fas fa-times"></i>
</div>
<div class="btn-extars">
   <a href="#" title="" class="post-job-btn"><i class="la la-plus"></i>Post Jobs</a>
   <ul class="account-btns">
      <li class="signup-popup"><a title=""><i class="la la-key"></i> Sign Up</a></li>
      <li class="signin-popup"><a title=""><i class="la la-external-link-square"></i> Login</a></li>
   </ul>
</div> -->
<!-- Btn Extras -->
<!-- <nav>
  <ul>
    <li class="menu-item-has-children">
        <a href="<?php echo base_url();?>recruiter/recruiter/companyprofile" title="">Company Profile</a>
    </li>
    <li class="menu-item-has-children">
        <a href="<?php echo base_url();?>recruiter/dashboard" title="">Recruiter Dashboard</a>
    </li>
    <li class="menu-item-has-children">
        <a href="<?php echo base_url();?>recruiter/jobpost" title="">Post a job</a>
    </li>
    <li>
      <form action="<?php echo base_url();?>recruiter/searchCandidates" method="post">
        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
        <div class="searchbarnav">
          <input type="text" placeholder="Search candidates by Job Title" name="keyword" required>
        </div>
      </form>
    </li>
  </ul>
</nav> -->