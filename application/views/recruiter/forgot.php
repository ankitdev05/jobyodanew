<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoda</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <style>
        p.greentxt {
            margin: 0;
            color: #27aa60;
        }
        .inform {
            text-align: center;
            font-family: 'Open Sans', sans-serif;
        }
        .valid_err{
         color: red;
        }
      </style>
   </head>
   <style>
      .rap{
      float: left;
      width: 100%;
      padding: 50px 10px;
      border: 1px solid rgba(51, 51, 51, 0.08);
      box-shadow: 0px 8px 16px -4px rgba(128, 128, 128, 0.22);
      /* margin-top: 70px; */
      height: 316px;
      }
      .rap .account-popup .cfield{
      margin-bottom: 35px
      }
      .loginmess p {
         text-align: center;
         color: #00FF00;
      }
      .validError{color:#f00;}
   </style>
   <body style="background: #f5f5f5;">
      <div class="page-loading" style="display: none;">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/loader.gif" alt="">
         <span>Skip Loader</span>
      </div>
      <div class="theme-layout" id="scrollup">
         <div class="responsive-header">
            <div class="responsive-menubar">
               <div class="res-logo"><a href="index.html" title=""><img src="http://placehold.it/178x40" alt=""></a></div>
               <div class="menu-resaction">
                  <div class="res-openmenu">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/icon.png" alt=""> Menu
                  </div>
                  <div class="res-closemenu">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/icon2.png" alt=""> Close
                  </div>
               </div>
            </div>
         </div>
         
         <section>
            <div class="block remove-bottomms bgatype">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="account-popup-area signin-popup-box static loginfrms">
                           <div class="jobyodaformlogo">
                              <img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoda.png">  
                           </div>
                           <div class="account-popup">
                              <div class="rap">
                                 <h4 style="float: left; margin: 0 0 15px 0;">Create New Password</h4>
                                 <div style="clear:both;"></div>
                                 <div class="loginmess">
                                    <?php
                                       if(isset($forgoterror)){
                                          echo "<p class='text-center text-danger'>".$forgoterror."</p>";
                                       }

                                    ?>
                                 </div>
                                 <form method="post" action="<?php echo base_url();?>recruiter/recruiter/resetPassword">
                                    <!--<div class="cfield">
                                       <input type="text" placeholder="Email" name="email">
                                       <i class="la la-user"></i>
                                       <?php if(isset($errors)){echo "<span class='validError'>".$errors['email']."</span>";}?>
                                    </div>-->
                                    <?php 
                                    if(!empty($id)){
                                       $encrypt_id = $id;
                                    }else{
                                       $encrypt_id = base64_decode($this->uri->segment(3));
                                    }
                                        /*if($this->uri->segment(3)) {
                                            $encrypt_id = $this->uri->segment(3);
                                        } else {
                                            $encrypt_id = $id;
                                        }*/
                                    ?>
                                    <input type="hidden" name="id" value="<?php echo $encrypt_id; ?>">
                                    <div class="cfield">
                                       <input type="password" placeholder="New Password" name="newpassword" value="<?php if(!empty($userData['newpassword'])){ echo $userData['newpassword']; } ?>">
                                       <i class="la la-key"></i>
                                    </div>
                                    <span class="pwd_err" style="font-size: 11px; float: left; margin: -19px 0 0;">Password should be alphanumeric.</span>
                                    <?php if(isset($errors['newpassword'])){?><p class='text-center text-danger'><?php echo $errors['newpassword']; ?></span><?php } ?>
                                    <div class="cfield">
                                       <input type="password" placeholder="Confirm Password" name="cpassword" value="<?php if(!empty($userData['cpassword'])){ echo $userData['cpassword']; } ?>">
                                       
                                       <i class="la la-key"></i>
                                       
                                    </div>
                                    <?php if(isset($errors['cpassword'])){?><p class='text-center text-danger'><?php echo $errors['cpassword']; ?></span><?php } ?>
                                    <div class="cfield">
                                       <input type="text" placeholder="Verify Code" name="verifyCode" value="<?php if(!empty($userData['verifyCode'])){ echo $userData['verifyCode']; } ?>">
                                       <i class="la la-key"></i>
                                       
                                    </div>
                                    <?php if(isset($errors['verifyCode'])){?><p class='text-center text-danger'><?php echo $errors['verifyCode']; ?></span><?php } ?>
                                    <div class="fullwdt">
                                       <button type="submit" class="loginlink">Change Password</button>
                                       <!--<button type="submit">Login</button> -->
                                    </div>
                                 </form>
                              </div>
                              
                           </div>
                        </div>
                        <!-- LOGIN POPUP -->
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      

      <!-- SIGNUP POPUP -->
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/maps2.js" type="text/javascript"></script>

   </body>
</html>

