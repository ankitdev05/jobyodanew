<?php 
//print_r($candidatedetails);die;
$recruiterId = $this->session->userdata('userSession');

$getPlan = $this->session->userdata('userSubscriptionSession');

?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <link rel="icon" href="<?php echo base_url().'recruiterfiles/';?>images/fav.png" type="image/png" sizes="16x16">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      
   </head>
   <style>
   
      table.dataTable thead .sorting, table.dataTable thead .sorting_asc, table.dataTable thead .sorting_desc, table.dataTable thead .sorting_asc_disabled, table.dataTable thead .sorting_desc_disabled{
	   font-size: 12px;
   }
   
   table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{
	       padding: 5px 14px;
   }
div#msgModal i.fa.fa-check {
    padding: 29px 27px;
    background: #26ae61;
    color: #fff;
    border-radius: 100%;
    font-size: 30px;
}
.dataTables_length label{text-transform: capitalize!important;}
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .main {
    float: left;
    width: 100%;
    padding:20px 0px 20px 0px;
    
}
.main label {
    margin: 0px 0px;
    padding: 0px 22px;
}
      .main label::before, label::after
      
      {
          display:none;
      }
      .searchs {
    float: right;
}
.search {
    float: left;
}
.manage-jobs-sec.boostjbs.addscroll {
    overflow-x: scroll;
    overflow-y: hidden;
}
      .manage-jobs-sec > table {
    float: left;
    margin-top: 25px;
    margin-bottom: 60px;
    margin-left: 30px;
    width: 1800px;
}
.manage-jobs-sec table thead tr td {
    width: 183px !important;
    padding: 6px 8px;
}
.manage-jobs-sec > table tbody td {
    vertical-align: middle;
    padding: 29px 8px;
    border-bottom: 1px solid #e8ecec;
}
      .link a {
      color: #fff;
      word-break: keep-all;
      font-size: 10px;
      line-height: 0;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      background: #26ae61;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .links a {
    color: #fff;
    font-size: 11px;
    word-break: keep-all;
    white-space: pre;
}


.modal-backdrop.show{opacity: 0;}
.modal-backdrop{z-index:0;}
.success_msg {
    color: #26ae61;
    margin-bottom: 0px;
    font-size: 16px;
}

div#msgModal {
    text-align: center;
}

.wrps .links , .wrps .link {width: 30px;}

.wrps {
    display: flex;
}

.wrps .link a, .wrps .links a {
    padding: 4px;
}

/*p.locned {
    height: 20px;
    overflow: hidden;
    width: 37px;
}*/

.links a, .link a{
      padding: 4px 3px;
}

.locnts {
    position: absolute;
    width: 249px;
    background: #fff;
    border: 1px solid #ddd;
    padding: 10px;
    opacity: 0;
    transition: all ease 0.5s;
}

.locnts p {
    font-size: 13px;
    line-height: 20px;
}

td{position:relative;}
p.locned:hover ~ .locnts {
    opacity:1;
    visibility:visible;
  z-index:9;
}

/* width */
::-webkit-scrollbar {
  width: 10px;
  height:10px;
  box-shadow:#000 0px 0px 2px 0px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #ffffff; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}

::-webkit-scrollbar-thumb {
  background: #47b476; 
}

@supports (-moz-appearance:none) {
  .scrld {
    scrollbar-color: #47b476 #fff;
    scrollbar-width: thin;
    border-radius: 10px;
    box-shadow: #000 0px 1px 1px 0px;
}
} 

.link.vew {
    text-align: center;
}

.scrld {
  min-height: 900px;
}


   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                    <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 column psthbs MainWrapper">
                        <div class="padding-left transactdto">
                           <div class="manage-jobs-sec">
                              <h3>Manage Jobs</h3>
                           </div>
                           <?php if(!empty($_GET['cid'])){
                                $cid = $_GET['cid'];
                                }else{
                                  $cid="";
                                  }?>
                           <div class="extra-job-info">
                              <!-- <span><i class="la la-clock-o"></i><strong>
                              <?php 
                                 if(isset($jobFetch)) {
                                    $jobCount = count($jobFetch);
                                    echo $jobCount;
                                 } else {
                                    echo "0";
                                 }
                              ?>
                              </strong> Job Posted</span>
                              <span><i class="la la-file-text"></i><strong>
                              <?php 
                                 if($jobApplicationFetch) {
                                    $jobApplicationFetchCount = count($jobApplicationFetch);
                                    echo $jobApplicationFetchCount;
                                 } else {
                                    echo "0";
                                 }
                              ?>
                              </strong> Application</span> -->
                              <span><i class="la la-file-text"></i><strong>
                              <?php 
                                 if($newjobApplicationFetch) {
                                    $newjobApplicationFetchCount = count($newjobApplicationFetch);
                                    echo $newjobApplicationFetchCount;
                                 } else {
                                    echo "0";
                                 }
                              ?>
                              </strong><a href="<?php echo base_url() ?>recruiter/candidate/manageCandidates?app_status=1">New Applications</a></span>

                              <span>
                                <i class="la la-file-text"></i>
                                <strong class="Red-text">
                                <?php 
                                   if($candidateswaiting) {
                                      $candidateswaiting = count($candidateswaiting);
                                      echo $candidateswaiting;
                                   } else {
                                      echo "0";
                                   }
                                ?>
                                </strong>
                                <a class="Red-text" href="<?php echo base_url() ?>recruiter/notification">Waiting for my action</a>
                              </span>
                              <?php if(!empty($_GET['status']) && $_GET['status']=='Expired'){ ?>
                              <span><i class="la la-users"></i><strong>
                              <?php 
                                 if($activejobFetch) {
                                    $activejobFetchCount = count($activejobFetch);
                                    echo $activejobFetchCount;
                                 } else {
                                    echo "0";
                                 }
                              ?>
                              
                              </strong> <a href="<?php echo base_url() ?>recruiter/jobpost/manageJobView?status=Active&cid=<?php echo $cid; ?>">Active Jobs</a></span>
                              <?php }else{?>
                               <span style="color:#26ae61 "><i class="la la-users"></i><strong>
                              <?php 
                                 if($activejobFetch) {
                                    $activejobFetchCount = count($activejobFetch);
                                    echo $activejobFetchCount;
                                 } else {
                                    echo "0";
                                 }
                              ?>
                              </strong> <a href="<?php echo base_url() ?>recruiter/jobpost/manageJobView?status=Active&cid=<?php echo $cid; ?>">Active Jobs</a></span>
                              <?php }?>

                              <?php if(!empty($_GET['status']) && $_GET['status']=='Expired'){ ?>
                              <span style="color: #26ae61;"><i class="la la-users"></i>
                                <strong>
                              <?php 
                                 if($expirejobFetch) {
                                    $expirejobFetchCount = count($expirejobFetch);
                                    echo $expirejobFetchCount;
                                 } else {
                                    echo "0";
                                 }
                              ?>
                              </strong><a href="<?php echo base_url() ?>recruiter/jobpost/manageJobView?status=Expired"> Expired Jobs </a></span>
                              <?php }else{?>
                              <span><i class="la la-users"></i>
                                <strong  class="Red-text">
                              <?php 
                                 if($expirejobFetch) {
                                    $expirejobFetchCount = count($expirejobFetch);
                                    echo $expirejobFetchCount;
                                 } else {
                                    echo "0";
                                 }
                              ?>
                              </strong><a class="Red-text" href="<?php echo base_url() ?>recruiter/jobpost/manageJobView?status=Expired"> Expired Jobs </a></span>
                              <?php }?>
                           </div>
                           <div class="manage-jobs-sec boostjbs">
                           <div class="col-md-12">
                           <div class="main">
                                
                           </div>
                           </div>
                           
               <div class="scrld"> 
                              <table id="sample-data-table" class="table">
                                <thead>
                                   <tr>
                                      <!-- <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">SNo</span>
                                         </div>
                                      </th> -->
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Job ID</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Job Title</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Site Name</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Job Posted Date</span>
                                         </div>
                                      </th>
                                     
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Job Expiry Date</span>
                                         </div>
                                      </th>
                                       <?php if($recruiterId['parent_id']=='0'){ ?>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Job Posted By</span>
                                         </div>
                                      </th>
                                      <?php }?>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Interview Mode</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Target HC</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Hired</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">New Applications</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">On Going Applications</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Accepted Job Offer</span>
                                         </div>
                                      </th>
                                      
                                      
                                     <!--  <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Job Description</span>
                                         </div>
                                      </th> -->
                                      <!-- <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Qualification required</span>
                                         </div>
                                      </th>
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Skills required</span>
                                         </div>
                                      </th> -->
 									   <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Suggested Candidates</span>
                                         </div>
                                      </th>

                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Boost Status</span>
                                         </div>
                                      </th>
                                      <!-- <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Total No. of Applicants</span>
                                         </div>
                                      </th> -->
                                      
                                      <th class="secondary-text">
                                         <div class="table-header">
                                            <span class="column-title">Action</span>
                                         </div>
                                      </th>
                                      
                                     
                                      <!-- <?php if(!empty($recruiterId['label']) && $recruiterId['label']==3){ 
                                              if(!empty($recruiterId['edit_delete']) && $recruiterId['edit_delete']==1){?>
                                     
                                      <?php }}else{?>
                                      
                                      <?php }?> -->

                                       
                                     
                                   </tr>
                                </thead>
                                <tbody>
                                   <?php
                                      $x1 =1;
                                      if(isset($jobFetch)) {
                                        foreach($jobFetch as $jobList) {
                                    ?>
                                           <tr>
                                              <!-- <td><?php echo $x1;?></td> -->
                                              <td><?php echo $jobList['id']; ?></td>
                                              <td><a href="<?php echo base_url();?>recruiter/candidate?type=<?php echo $jobList['id']; ?>" style="text-decoration: underline;color: #084d87;"><?php echo $jobList['jobtitle'];?></a></td>
                                              <td style=""><p class="locned"><?php if(isset($jobList['site_name'])){ echo $jobList['site_name'];}?></p></td>
                                              
                                              <td><?php echo date('F d,Y', strtotime($jobList['created_at']));?></td>
                                              <td><?php echo date('F d,Y',strtotime($jobList['jobExpire']));?></td>
                                               <?php if($recruiterId['parent_id']=='0'){ ?>
                                              <td><?php echo $jobList['subrecruiter_name'];?></td>
                                              <?php }?>

                                              <td><?php echo $jobList['mode'];?></td>

                                              <td><?php echo $jobList['opening'];?></td>
                                              <td><?php echo $jobList['hiredcount'];?></td>
                                              <td><?php echo $jobList['newcount'];?></td>
                                              <td><?php echo $jobList['countgoingjobs'];?></td>
                                              <td><?php echo $jobList['acptcount'];?></td>
                                               <!-- <td><?php if(strlen($jobList['jobDesc']) > 50){echo substr($jobList['jobDesc'], 0,50) .' ... ';} else{echo $jobList['jobDesc'];}?></td> -->
                                               <td>
                                                <div class="links">
                                                   <a data-toggle="tooltip" title="Suggested Candidates" href="<?php echo base_url();?>recruiter/candidate/candidate_suggested?type=<?php echo base64_encode($jobList['id']); ?>"><i class="far fa-eye"><!-- <?php echo $jobList['countsuggested']; ?> --></i></a>
                                                   <!-- <p><?php echo $jobList['countsuggested'];?></p> -->
                                                </div>
                                              </td>
                                              <!-- <td><p class="locned qualfnt"><?php echo $jobList['qualification'];?></p></td>
                                              <td><?php echo $jobList['skills'];?></td> -->
                                              <td>
                                              <?php if($jobList['boost_status']=='1') {
                                                
                                                if(!empty($_GET['status']) && $_GET['status']=='Expired'){
                                              ?>
                                                <img src="<?php echo base_url().'recruiterfiles/';?>images/Job_Visible.png">
                                              
                                              <?php 
                                                } else {
                                              ?>
                                                  
                                                  <?php
                                                     if(count($getPlan) > 0 && $getPlan['total_job_boost'] > 0) {
                                                  ?>

                                                        <img src="<?php echo base_url().'recruiterfiles/';?>images/Job_Visible.png" data-toggle="modal" data-target="#boostModal<?php echo $x1; ?>">
                                                  
                                                  <?php
                                                    } else {
                                                  ?>
                                                        <img src="<?php echo base_url().'recruiterfiles/';?>images/Job_Visible.png">
                                                  <?php
                                                    }
                                                  ?>

                                              <?php }
                                                } else{
                                                  
                                                  if(!empty($_GET['status']) && $_GET['status']=='Expired') {
                                              ?>
                                                  <img src="<?php echo base_url().'recruiterfiles/';?>images/Job-Disable.png">
                                              
                                              <?php 
                                                } else {
                                              ?>
                                              
                                              <img src="<?php echo base_url().'recruiterfiles/';?>images/Job-Disable.png" data-toggle="modal" data-target="#myModal<?php echo $x1; ?>">
                                              <?php }}?>

                                                <!-- Modal -->
                                                <div id="myModal<?php echo $x1; ?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog modal-dialog-centered">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       
                                                      </div>
                                                      <div class="modal-body">
													                              <h4 class="modal-title" style="color: #383838;">BOOSTING JOB POST</h4>
                                                        <p>Do you want to boost this job?</p>
                                                        <input type="hidden" name="jid" value="<?php echo $jobList['id'];?>">
                                                        <p><button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal<?php echo $x1.$x1; ?>" data-dismiss="modal" style="margin-left: 15px;"> Yes </button> <button type="button" class="btn btn-default" data-dismiss="modal"> No </button></p>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div id="boostModal<?php echo $x1; ?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog modal-dialog-centered">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       
                                                      </div>
                                                      <div class="modal-body">
													                               <h4 class="modal-title" style="color: #383838;">BOOSTING JOB POST</h4>
                                                        <p>This job is already boosted</p>
                                                        <!-- <input type="hidden" name="jid" value="<?php echo $jobList['id'];?>">
                                                        <p><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal<?php echo $x1.$x1; ?>" data-dismiss="modal" style="margin-left: 15px;"> Yes </button> <button type="button" class="btn btn-default" data-dismiss="modal"> No </button></p> -->
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                                <!-- Modal -->
                                                <div id="myModal<?php echo $x1.$x1; ?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog modal-dialog-centered">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        
                                                      </div>
                                                      <div class="modal-body">
													                              <h4 class="modal-title" style="color: #383838;">BOOSTING JOB POST</h4>
                                                        <p style="margin:0;">You can boost this job.Payment will be added to the next invoice</p>
                                                        <p><strong style="color:#000;">Boost Amount :</strong> <?php echo $boostAmountFetch[0]['boost_amount']; ?></p>
                                                        <form method="POST" action="<?php echo base_url() ?>/recruiter/jobpost/booststatus">
                                                        <input type="hidden" name="jid" value="<?php echo $jobList['id'];?>">
                                                        <p><button type="submit" class="btn btn-success" style="margin-left: 15px;"> Yes </button> <button type="button" class="btn btn-default" data-dismiss="modal"> No </button></p>
                                                        </form>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                              </td>

                                              <!-- <td ><p class="locned" style="width:85px;  margin: 0; text-align: center;"><?php echo $jobList['totalApply']; ?></p></td> -->
                                              
                                              

                                              <td>
                                                <div class="wrps">
                                                                       
                                                 <div class="link editbxd">
                                                   <a href="<?php if(isset($jobList['id'])){ echo base_url();?>recruiter/jobpost/jobpostView?type=<?php echo base64_encode($jobList['id']); }?>" class="edipro"><i class="fas fa-pen"></i></a>
                                                </div>
                        
                                                <!-- <div class="link delbxd">
                                                   <a title="Delete" href="#" data-toggle="modal" id="<?php echo $jobList['id'];?>" onclick="getrid(this.id)" data-target="#deleteModal" ><i class="fa fa-trash"></i></a>
                                                </div> -->
                                                </div>
                                              </td>
                                              
                                              
                                              <!-- <?php if(!empty($recruiterId['label']) && $recruiterId['label']==3){ 
                                              if(!empty($recruiterId['edit_delete']) && $recruiterId['edit_delete']==1){?>
                                             
                                              <?php }}else{?>
                                              
                                              <?php }?> -->
                                               
                                           </tr>
                                   <?php
                                      $x1++;
                                      }
                                    }
                                   ?>
                                </tbody>
                             </table>
                             </div>
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <div class="account-popup-area signin-popup-box">
         <div class="account-popup">
            <span class="close-popup"><i class="la la-close"></i></span>
            <h3>User Login</h3>
            <span>Click To Login With Demo User</span>
            <div class="select-user">
               <span>Candidate</span>
               <span>Employer</span>
            </div>
            <form> 
               <div class="cfield">
                  <input type="text" placeholder="Username" />
                  <i class="la la-user"></i>
               </div>
               <div class="cfield">
                  <input type="password" placeholder="********" />
                  <i class="la la-key"></i>
               </div>
               <p class="remember-label">
                  <input type="checkbox" name="cb" id="cb1"><label for="cb1">Remember me</label>
               </p>
               <a href="#" title="">Forgot Password?</a>
               <button type="submit">Login</button>
            </form>
            <div class="extra-login">
               <span>Or</span>
               <div class="login-social">
                  <a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
                  <a class="tw-login" href="#" title=""><i class="fa fa-twitter"></i></a>
               </div>
            </div>
         </div>
      </div>
      <!-- LOGIN POPUP -->
      <div class="account-popup-area signup-popup-box">
         <div class="account-popup">
            <span class="close-popup"><i class="la la-close"></i></span>
            <h3>Sign Up</h3>
            <div class="select-user">
               <span>Candidate</span>
               <span>Employer</span>
            </div>
            <form>
               <div class="cfield">
                  <input type="text" placeholder="Username" />
                  <i class="la la-user"></i>
               </div>
               <div class="cfield">
                  <input type="password" placeholder="********" />
                  <i class="la la-key"></i>
               </div>
               <div class="cfield">
                  <input type="text" placeholder="Email" />
                  <i class="la la-envelope-o"></i>
               </div>
               <div class="dropdown-field">
                  <select data-placeholder="Please Select Specialism" class="chosen">
                     <option>Web Development</option>
                     <option>Web Designing</option>
                     <option>Art & Culture</option>
                     <option>Reading & Writing</option>
                  </select>
               </div>
               <div class="cfield">
                  <input type="text" placeholder="Phone Number" />
                  <i class="la la-phone"></i>
               </div>
               <button type="submit">Signup</button>
            </form>
            <div class="extra-login">
               <span>Or</span>
               <div class="login-social">
                  <a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
                  <a class="tw-login" href="#" title=""><i class="fa fa-twitter"></i></a>
               </div>
            </div>
         </div>
      </div>
       <!-- Modal -->
  <div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
          <i class="fa fa-check" aria-hidden="true"></i>
          <p class="success_msg"><?php if($this->session->tempdata('inserted')){ echo $this->session->tempdata('inserted'); } ?></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="modal" id="deleteModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Job</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this job?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink" style="background: red;
    border: red;">Delete</a>
            </div>
        </div>
    </div>
</div>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <!-- SIGNUP POPUP -->
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables.net/js/jquery.dataTables.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/node_modules/datatables-responsive/js/dataTables.responsive.js"></script>
     <!-- <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>-->
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
     <!-- Data tables -->
               
           
      <!-- Data tables -->
      <script type="text/javascript">
      $(document).ready(function(){  
        $('#sample-data-table').DataTable({
             "searching": true,
             "order": [[ 0, "desc" ]]
        });
      });    

      function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>recruiter/jobpost/jobCancel?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
      } 
     </script>

     <!-- <script type="text/javascript">
       $('document').ready(function(){
        var msg = '<?php  $this->session->tempdata('inserted') ?>';
        if(msg!=''){
          $('msgModal').modal('show');
        }
       })
     </script> -->
     <?php
         if($this->session->tempdata('inserted')!='') {
             echo($this->session->tempdata('inserted')) ;
             /*if($this->session->tempdata('validationError')){$this->session->unset_tempdata('validationError');}*/
      ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                    $('#msgModal').modal('show');
                });
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href );
                    //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
                }
            </script>
      <?php
         }
     ?>
   </body>
</html>

