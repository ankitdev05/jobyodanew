

<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>
      <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
      
   </head>
   <style>
      .menu-sec nav > ul > li.menu-item-has-children > a::before, .btn-extars{
      display:none;}
      .modal-backdrop{position: relative!important;z-index: 0!important;}
      .modal .modal-dialog{margin-top:102px;}
   


   <style type="text/css">
             .glsrtf span.rates {
    width: 64%!important;
    float: left!important;
}
.glsrtf span {
    width: 7%!important;
    float: left!important;
}



.caption{display: none!important;}
  .rating-loading{
    width:25px;
    height:25px;
    font-size:0;
    color:#fff;
    background:url(../img/loading.gif) top left no-repeat;border:none
    }
}
span.ratings .rating-stars {
    float: left;
    margin: -12px 0px;
}
.rating-container .rating-stars
    {position:relative;
    cursor:pointer;
    vertical-align:middle;
    display:inline-block;
    overflow:hidden;
    white-space:nowrap;
         }
         .rating-container .rating-input
         {position:absolute;
         cursor:pointer;
         width:100%;
         height:1px;
         bottom:0;left:0;
         font-size:1px;
         border:none;
         background:0 0;
         padding:0;
         margin:0
         }
         .rating-disabled .rating-input,.rating-disabled .rating-stars
         {
             cursor:not-allowed
             
         }
         .rating-container .star{
             display:inline-block;
             margin:0 3px;
             text-align:center;
            font-size: 16px;
         }
         .rating-container .empty-stars{
             color:#aaa;
             font-size: 16px;
         }
         .rating-container .filled-stars
         {
             position:absolute;
             left:0;
             top: 0px;
             margin:auto;
             color:#fde16d;
             white-space:nowrap;
             overflow:hidden;
             -webkit-text-stroke:1px #777;
             text-shadow:1px 1px #999;
         }
             .rating-rtl
             {
                 float:right
                 
             }
             .rating-animate .filled-stars
             {
                 transition:width .25s ease;
                 -o-transition:width .25s ease;
                 -moz-transition:width .25s ease;-webkit-transition:width .25s ease
             }
             .rating-rtl .filled-stars
             {left:auto;right:0;-moz-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);-webkit-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);-o-transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0);
             transform:matrix(-1,0,0,1,0,0) translate3d(0,0,0)}.rating-rtl.is-star 
             .filled-stars{
                 right:.06em
                 
             }
             .rating-rtl.is-heart .empty-stars{
                 margin-right:.07em
                 
             }
             .rating-lg{
                 font-size:3.91em
                 
             }
             .rating-md{
                 font-size:3.13em
                 
             }
             .rating-sm{
                 font-size:2.5em
                 
             }
             .rating-xs{
                 font-size:2em
                 
             }
             .rating-xl{
             font-size:4.89em
                 
             }
             .rating-container .clear-rating{
                 color:#aaa;
                 cursor:not-allowed;
                 display:inline-block;
                 vertical-align:middle;
                 font-size:60%;
                 padding-right:5px
                 
             }
             .clear-rating-active{
                 cursor:pointer!important
                 
             }
             .clear-rating-active:hover{
                 color:#843534
                 
             }
             .rating-container .caption{
                 color:#999;
                 display:inline-block;
                 vertical-align:middle;
                 font-size:60%;
                 margin-top:-.6em;
                 margin-left:5px;
                 margin-right:0
                 
             }
             .rating-rtl .caption{
                 margin-right:5px;
                 margin-left:0
                 
             }
             @media print{.rating-container .clear-rating{
                 display:none
                 
             }
                 
             }
			 
			 .rating-md {
    font-size: 10px;
}

span.ratings.leftrtgd {
    width: 46%;
}
.validerr {
    color: red;
    float: left;
    width: 100%;
    padding: 15px 0px;
}
div#msgModal i.fa.fa-check {
    padding: 29px 27px;
    background: #26ae61;
    color: #fff;
    border-radius: 100%;
    font-size: 30px;
    margin-top: 10px;
}
div#msgModal {
    text-align: center;
}
div#msgModal p.success_msg {
    margin-bottom: 0px;
}
.pf-field > input, .pf-field > textarea {
    margin-bottom: 0px;
    float: left;
    width: 100%;
    border: 2px solid #e8ecec;
    margin-bottom: 0px;
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    -ms-border-radius: 8px;
    -o-border-radius: 8px;
    border-radius: 8px;
    padding: 14px 45px 14px 15px;
    background: #ffffff;
    font-family: Open Sans;
    font-size: 13px;
    color: #101010;
    line-height: 24px;
}
</style>

   <body>
      <div class="theme-layout" id="scrollup">
      <header class="stick-top nohdascr">
         <div class="menu-sec">
            <div class="container-fluid dasboardareas">
               
               <!-- Logo -->
               <?php include_once('headermenu.php');?>
            </div>
         </div>
      </header>
      <section>
         <div class="block no-padding">
            <div class="container-fluid dasboardareas">
               <div class="row">
                  <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                     <div class="widget">
                        <div class="menuinside">
                           <?php include_once('sidebar.php'); ?>
                        </div>
                     </div>
                  </aside>
                  <div class="col-md-9 col-lg-10 recruprogfd psthbs MainWrapper">
                     <div class="row">
                       
                           <div class="companyprofileforms managerecsite addrecrutforms">
						    <div class="col-md-12 psthbs">
                            
                                
                                      <h6>Email Support</h6>
                                   
                                 
                                    <div class="col-md-12">
         
          <div class="col-lg-12 column">
            <div class="">
              <h3></h3>
              <?php //print_r($errors);die;?>
              <!-- <?php if($this->session->tempdata('contactSuccess')){?>
              <span style="color: #47b476;"><?php echo $this->session->tempdata('contactSuccess'); ?></span>
              <?php } ?> -->
              <form action="<?php echo base_url();?>recruiter/recruiter/contact_form" method="post">
                <div class="row">
                  <div class="col-lg-12">
                    <!-- <span class="pf-title">Subject</span> -->
                    <div class="pf-field">
                      <!-- <input type="text" name="subject" placeholder="Enter Subject" > -->
                      <select name="subject" id="subject" class="form-control" style="font-size: 13px; color: #888888;
    padding: 0px 11px;">
                        <option value="">Select</option>
                        <option value="Functional Issue">Functional Issue</option>
                        <option value="Design Issue">Design Issue</option>
                        <option value="Compatibility Issue">Compatibility Issue</option>
                        <option value="Others">Others</option>
                      </select>
                      <span class="validerr"><?php if(!empty($errors['subject'])){ echo $errors['subject']; } ?></span>
                    </div>
                  </div>
                  <div class="col-lg-12" id="other_subject" style="display:none; margin-bottom:22px;">
                    <div class="pf-field">
                      <input type="text" name="other_subject">
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <!-- <span class="pf-title">Message</span> -->
                    <div class="pf-field">
                      <textarea name="message" placeholder="Enter Message" ></textarea>
                       <span class="validerr"><?php if(!empty($errors['message'])){ echo $errors['message']; } ?></span>
                    </div>
                  </div>
                  <div class="col-lg-12" style="text-align:center; margin:0 0 30px 0;"> 
                    <button type="submit" style="    margin: auto; float: none;">Send</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          
         </div>
                              
                              </div>
                           </div>
                        </div>

                        <div class="row">
                        
         </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include_once('footer.php'); ?>
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                     <div class="filldetails">
                        <form>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="Change Password">
                              <img src="images/keypass.png">
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="New Password">
                              <img src="images/keypass.png">
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="Confirm New Password">
                              <img src="images/keypass.png">
                           </div>
                           <button type="submit" class="srchbtns">Change</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="msgModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
          <i class="fa fa-check" aria-hidden="true"></i>
          <p class="success_msg"><?php if($this->session->tempdata('contactSuccess')){ echo $this->session->tempdata('contactSuccess'); } ?></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      
   </body>

   <script>
   $('#subject').on('change', function() {
      if($("#subject").val()=="Others"){
          $('#other_subject').css('display','block');
    
      }else{
          $('#other_subject').css('display','none');
         
      }
    });  

      $(function () { 
        $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
      });  
      
      // $( window ).scroll(function() {   
       // if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
        $(".progress-bar").each(function(){
          each_bar_width = $(this).attr('aria-valuenow');
          $(this).width(each_bar_width + '%');
        }); 
   </script>

   <?php
         if($this->session->tempdata('contactSuccess')!='') {
             echo($this->session->tempdata('contactSuccess')) ;
             /*if($this->session->tempdata('validationError')){$this->session->unset_tempdata('validationError');}*/
      ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                    $('#msgModal').modal('show');
                });
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href );
                    //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
                }
            </script>
      <?php
         }
     ?>
