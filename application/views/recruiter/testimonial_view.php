<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>JobYoDA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="author" content="CreativeLayers">
    <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-R64BFR96SR"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-R64BFR96SR');
      </script>
      <!-- Global site tag (gtag.js) - Google Ads: 851948051 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-851948051"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-851948051');
    </script>
    <meta name="title" content="<?php if(isset($meta_title)){ echo $meta_title; } ?>">
    <meta name="description" content="<?php if(isset($meta_description)){ echo $meta_description; } ?>">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
    <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
    <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/croppie.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/owl.carousel.css">
  </head>  
  
  <body>    
    <section>
      <div class="ClientArea">
        <div class="ClientLogo">
          <figure>
            <a href="<?php echo base_url();?>recruiter">
              <img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoDa.png">
            </a>
          </figure>
          <ul>
            <li><a href="<?php echo base_url();?>recruiter/login">Sign In</a></li>
            <li><a href="<?php echo base_url();?>recruiter/signup">Sign Up</a></li>
          </ul>
          <h1 style="color: #feab28;">Welcome To JobYoDA Recruiter Panel</h1>
          <div class="clear"></div>
        </div>
        <div class="container">
          <h4>Our Testimonial</h4>
          <h2>Our Best Reward is to deliver value to our recruitment partners</h2>
          <div id="demos">
            <div class="owl-carousel owl-theme" id="Client">
            <?php
              if(isset($recruitFetch)) {
                foreach($recruitFetch as $jobList) {
            ?>  
              <div class="item">
                <div class="ClientBox">
                  <figure>
                    <?php
                      if(strlen($jobList['companyPic']) > 0) {
                    ?>
                      <img src="<?php echo $jobList['companyPic']; ?>">
                    <?php
                      } else {
                    ?>
                      <img src="<?php echo base_url().'recruiterfiles/';?>images/testimony.png">
                    <?php
                      }
                    ?>
                  </figure>
                  <!--<h3><?php //echo $jobList['fname'].' '.$jobList['lname']; ?></h3>-->
                  <?php
                    if(strlen($jobList['cname']) > 1) {
                  ?>
                    <h3><b><?php echo $jobList['cname']; ?></b></h3>
                  <?php } else { ?>
                    <h3><b><?php echo $jobList['cnameee']; ?></b></h3>
                  <?php } ?>
                                
                  <h4><?php echo $jobList['name']; ?></h4>
                  <p><?php echo $jobList['description']; ?></p>
                </div>
              </div> 

              <?php
                  }
                } else {
              ?>
              <div class="item">
                <div class="ClientBox">
                  <figure><img src="https://d2nkb28djb813.cloudfront.net/assets/img/testimonial/Anand.png"></figure>
                  <h3>Anand Shukla</h3>
                  <h4>Founder-Tavalong</h4>
                  <p>Top Mobile App Development Company .Mobulous completed an app that users from several countries have downloaded and enjoyed, specifically the app’s flow and look. The team delivered the app as requested and worked long hours to complete it. They are exceedingly skilled and produce high-quality work.</p>
                </div>
              </div> 
              <?php
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>

 
  <?php include_once("footer.php");?>
  <?php include_once("modalpassword.php");?>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
  <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/croppie.js"></script>
  <script src="<?php echo base_url().'recruiterfiles/';?>js/owl.carousel.js"></script>
  <script type="text/javascript">
    $('#Client').owlCarousel({
      loop:true,
      margin:0,
      center:true,
      smartSpeed: 1000,
      autoplay:5000,
      nav:true,
      autoplayHoverPause: true,
      responsive:{
        0:{ items:1 },
        500:{ items:2 },
        800:{ items:2 },
        1000:{ items:3 }
      }
    });
  </script>
</body>
 




