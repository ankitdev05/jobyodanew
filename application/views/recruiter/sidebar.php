<?php $recruiterId = $this->session->userdata('userSession');

$getPlan = $this->session->userdata('userSubscriptionSession');

//print_r($recruiterSubscribed);die;

?>
<ul>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/jobyoda/recruiter/dashboard'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/dashboard">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/dashico.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/dashover.png" class="acthover">
         <p>Dashboard</p>
      </a>
   </li>
   
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/companyprofile'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/companyprofile">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/walk.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/walkhover.png" class="acthover">
         <p>Company Profile </p>
      </a>
   </li>

   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/addsubrecruiter' || $_SERVER['REQUEST_URI']=='/recruiter/subrecuriterlist'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapseRecruiter" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/add-rec-2.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/add-rec-1.png" class="acthover">
         <p>Add Recruiters</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapseRecruiter">
         <ul class="innersubd">
            <li><a href="<?php echo base_url();?>recruiter/addsubrecruiter">Add Sub-Recruiter</a></li>
            <li><a href="<?php echo base_url();?>recruiter/subrecuriterlist">Sub-Recruiter Listing</a></li>
            <li><a href="<?php echo base_url();?>recruiter/transfer_recruiter">Super User Transfer</a></li>
         </li>   
         </ul>
      </div>
   </li>

   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/postjob'){?> active <?php }?>">
      <?php
         if(count($getPlan) > 0 && $getPlan['remaining_job_post'] > 0) {
      ?>
         <a href="<?php echo base_url();?>recruiter/postjob">
      <?php
         } else {
      ?>
         <a href="javascript:void(0)">
      <?php
         }
      ?>
            <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcase.png" class="nonhover">
            <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcasehover.png" class="acthover">
            <p>Post a Job </p>
         </a>
   </li>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/assignjob'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/assignjob">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcase.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcasehover.png" class="acthover">
         <p> Assign a Job </p>
      </a>
   </li>
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/managejobs'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/managejobs">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcase.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/briefcasehover.png" class="acthover">
         <p>Manage Jobs</p>
      </a>
   </li>

   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/managecandidates'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/managecandidates">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/managecandid.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/managecanhover.png" class="acthover">
         <p>Manage Candidates</p>
      </a>
   </li>

<?php
$userSession = $this->session->userdata('userSession');
if(!empty($userSession['label']) && $userSession['label']=='3'){
     $subrecruiter_id = $userSession['id'];
     $userSession['id'] = $userSession['parent_id'];
 }else{
     $userSession['id'] = $userSession['id'];
     $subrecruiter_id = 0;
 }

$pending = $this->Candidate_Model->candidateduenotification_list($userSession['id'],$subrecruiter_id); 
if (!empty($pending)) {
   $totalnotify = count($pending);
} else {
   $totalnotify = 0;
}

?>
   
   
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/notification'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/notification">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/notify.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/notifyhover.png" class="acthover">
         <p>Notification
            <span class="countnoitfy"><?php echo $totalnotify; ?></span>
         </p>
      </a>
   </li>
   <?php if(!empty($recruiterId['label']) && $recruiterId['label']==3){ 
      }else{
      ?>
   
   <?php }?>
   <!-- <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/recruiterprofile'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/recruiterprofile">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofile.png" class="nonhover"><img src="<?php echo base_url().'recruiterfiles/';?>images/recprofilehover.png" class="acthover">
         <p>Recruiter Profile</p>
      </a>
   </li> -->
   
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/rating'){?> active <?php }?>">
      <a href="<?php echo base_url();?>recruiter/rating">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/star.png" class="nonhover"><img src="<?php echo base_url().'recruiterfiles/';?>images/starhover.png" class="acthover">
         <p>Rating & Reviews</p>
      </a>
   </li>

   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/advertisement' || $_SERVER['REQUEST_URI']=='/recruiter/advertisementlist'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapsead" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofile.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofilehover.png" class="acthover">
         <p>Advertisement Banner</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapsead">
         <ul class="innersubd">
            <?php
               if(count($getPlan) > 0 && $getPlan['remaning_advertise'] > 0) {
            ?>
               <li><a href="<?php echo base_url();?>recruiter/advertisement">Add Banner</a></li>
               <li><a href="<?php echo base_url();?>recruiter/advertisementlist">Banner List</a></li>
            <?php
               } else {
            ?>
               <li><a href="javascript:void(0)">Add Banner</a></li>
               <li><a href="javascript:void(0)">Banner List</a></li>
            <?php
               }
            ?>
            
         </ul>
      </div>
   </li>

   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/videoadvertisement' || $_SERVER['REQUEST_URI']=='/recruiter/videoadvertisementlist'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapsead11" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofile.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/recprofilehover.png" class="acthover">
         <p>Video Advertisements</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapsead11">
         <ul class="innersubd">
            <?php
               if(count($getPlan) > 0 && $getPlan['remaining_video_post'] > 0) {
            ?>
               <li><a href="<?php echo base_url();?>recruiter/videoadvertisement">Add Video Advertisement</a></li>
               <li><a href="<?php echo base_url();?>recruiter/videoadvertisementlist">Video Advertisement List</a></li>
            <?php
               } else {
            ?>
               <li><a href="<?php echo base_url();?>recruiter/videoadvertisement">Add Video Advertisement</a></li>
               <li><a href="<?php echo base_url();?>recruiter/videoadvertisementlist">Video Advertisement List</a></li>
            <?php
               }
            ?>
            
         </ul>
      </div>
   </li>
  
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/report' || $_SERVER['REQUEST_URI']=='/recruiter/jobwisereport' || $_SERVER['REQUEST_URI']=='/recruiter/candidatereport'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapseExample1" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
         <p>Reports</p>
   		<i class="fas fa-plus"></i>
   		<i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapseExample1">
         <ul class="innersubd">
            <li><a href="<?php echo base_url();?>recruiter/report">Hired Report</a></li>
            <li><a href="<?php echo base_url();?>recruiter/jobwisereport">Jobwise C&B Report</a></li>
            <li><a href="<?php echo base_url();?>recruiter/candidatereport">Candidate Report</a></li>
            <li><a href="<?php echo base_url();?>recruiter/screeningreport">Instant screening</a></li>
         </ul>
      </div>
   </li>

   <li>
      <a data-toggle="collapse" href="#collapseExample5" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
         <p>Testimonial</p>
         <i class="fas fa-plus"></i>
         <i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapseExample5">
         <ul class="innersubd">
            <li><a href="<?php echo base_url();?>recruiter/testimonialadd">Add Testimonial</a></li> 
            <li><a href="<?php echo base_url();?>recruiter/testimoniallist">Testimonial List</a></li>  
            
         </ul>
      </div>
   </li>
   
   <?php 
      if(!empty($recruiterId['label']) && $recruiterId['label']==3){ 
         if(!empty($recruiterId['invoice_view']) && $recruiterId['invoice_view']==1){
   ?>
            <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/transaction'){?> active <?php }?>">
               <a href="<?php echo base_url();?>recruiter/transaction">
                  <img src="<?php echo base_url().'recruiterfiles/';?>images/transact.png" class="nonhover">
                  <img src="<?php echo base_url().'recruiterfiles/';?>images/transacthover.png" class="acthover">
                  <p>Transaction</p>
               </a>
            </li>
   
   <?php  }
      } else {
   ?>
         <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/transaction'){?> active <?php }?>">
            <a href="<?php echo base_url();?>recruiter/transaction">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/transact.png" class="nonhover">
               <img src="<?php echo base_url().'recruiterfiles/';?>images/transacthover.png" class="acthover">
               <p>Transaction</p>
            </a>
         </li>
   <?php } ?>
   
    
   <li class="<?php if($_SERVER['REQUEST_URI']=='/recruiter/contact' || $_SERVER['REQUEST_URI']=='/recruiter/faq'){?> active <?php }?>">
      <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="collapsed">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/mail-1.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/mail-2.png" class="acthover">
         <p>Contact Us</p>
   		<i class="fas fa-plus"></i>
   		<i class="fas fa-minus"></i>
      </a>

      <div class="collapse" id="collapseExample">
         <ul class="innersubd">
         <li> <a href="<?php echo base_url();?>recruiter/contact">Email Support</a></li>
        <!--  <li><a href="<?php echo base_url();?>recruiter/faq">FAQs</a></li> -->
         </ul>
      </div>
   </li>

<?php
   if($userSession['id'] == 593) {
?>
      <li class="">
         <a href="<?php echo base_url();?>recruiterfiles/downloadresume.xlsx">
            <img src="<?php echo base_url().'recruiterfiles/';?>images/edit.png" class="nonhover">
            <img src="<?php echo base_url().'recruiterfiles/';?>images/edithover.png" class="acthover">
            <p>Download Resume</p>
         </a>
      </li>
<?php
   }   
?>
   <!-- <li>
      <a href="<?php echo base_url();?>recruiter/dashboard/logout">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/logout.png" class="nonhover">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/logouthover.png" class="acthover">
         <p>Logout</p>
      </a>
   </li> -->
</ul>


<style type="text/css">
   ul.innersubd li {
    display: block;
    width: 100%;
    margin: 0 0 6px 0;
}

ul.innersubd {
    margin: 7px 0 0 0;
    float: left;
    width: 100%;
    padding-left: 29px;
}

div#collapseExample, #collapseExample1 {
    float: left;
    width: 100%;
}

</style>