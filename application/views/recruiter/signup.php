<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA - Recruiter</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">

      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-R64BFR96SR"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-R64BFR96SR');
      </script>
      <!-- Global site tag (gtag.js) - Google Ads: 851948051 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-851948051"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-851948051');
    </script>
    
      <meta name="title" content="<?php if(isset($meta_title)){ echo $meta_title; } ?>">
      <meta name="description" content="<?php if(isset($meta_description)){ echo $meta_description; } ?>">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <style type="text/css">
         .stepcounter p {
         margin-bottom: 0px;
         text-align: center;
         color: #f00;
         }
         .account-popup .cfield select {
         border: 0px;
         height: 50px;
         color: #9a9a9a !important;
         float: left;
         width: 100%;
         background: no-repeat;
         margin: 0;
         font-family: Open Sans;
         font-size: 13px;
         color: #474747;
         padding: 16px 25px 16px 15px;
         }
         .cfield span{font-size: 11px;color: #f00;}
         button.nextstepsecond {
         float: none;
         width: 150px;
         }
      </style>
   </head>
   <body style="background: #f5f5f5;">
      <section>
         <div class="signdvs LoginArea" style="padding: 15px 0">
            <div class="account-popup-area signup-popup-box static ">
               <div class="jobyodaformlogo">
                <a href="<?php echo base_url();?>recruiter">
                  <img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoda.png">  
                </a>
               </div>
               <form method="post" action="<?php echo base_url();?>recruiter/recruiter/signupinsert">
                  <div class="account-popup">
                     <div class="rap">
                        <h4 style="float: left; margin: 0 0 15px 18px;">Sign up</h4>
                        <div class="stepcounter">
                           <?php //echo validation_errors(); ?>
                        </div>
                        <div id="Stepone" class="tabcontent signuperronly" style="display: block;">
                           <div class="spnrotw">
                              <div class="cfield">
                                 <input type="text" placeholder="First Name" name="fname" value="<?php if(!empty($userData['fname'])){ echo $userData['fname']; } ?>" autocomplete="off" >
                                 <i class="la la-user"></i>
                              </div>
                              <?php if(!empty($errors['fname'])) {echo "<span>".$errors['fname']."</span>";} ?>
                           </div>
                           <div class="spnrotw">
                              <div class="cfield">
                                 <input type="text" placeholder="Middle Name, Last name" name="lname" value="<?php if(!empty($userData['lname'])){ echo $userData['lname']; } ?>" autocomplete="off">
                                 <i class="la la-user"></i>
                              </div>
                              <?php if(!empty($errors['lname'])) {echo "<span>".$errors['lname']."</span>";} ?>
                           </div>
                           <div class="spnrotw">
                              <div class="cfield">
                                 <input type="text" placeholder="Company Name" name="cname" value="<?php if(!empty($userData['cname'])){ echo $userData['cname']; } ?>" autocomplete="off">
                                 <i class="la la-building"></i>
                              </div>
                              <?php if(!empty($errors['cname'])) {echo "<span>".$errors['cname']."</span>";} ?>
                           </div>
                           <div class="spnrotw">
                              <div class="cfield dmname">
                              <div class="dmtypes">
                                <span>www.</span>
                              </div>
                                 <input type="text" placeholder="Domain Name/Company Name" name="domain" id="domainField" value="<?php if(!empty($userData['domain'])){ echo $userData['domain']; } ?>" autocomplete="off">
                                 <i class="la la-building"></i>
                              </div>
                              <?php if(!empty($errors['domain'])) {echo "<span>".$errors['domain']."</span>";} ?>
                           </div>
                           <div class="spnrotw">
                              <div class="cfield">
                                 <input type="email" placeholder="Email Id" name="email" id="emailField" value="<?php if(!empty($userData['email'])){ echo $userData['email']; } ?>" autocomplete="off">
                                 <i class="la la-envelope-o"></i>
                              </div>
                              <?php if(!empty($errors['email'])) {echo "<span>".$errors['email']."</span>";} ?>
                           </div>
                           <div class="spnrotw">
                              <div class="cfield ">
                                 <input type="text" placeholder="Address" name="address" id="txtPlaces" value="<?php if(!empty($userData['address'])){ echo $userData['address']; } ?>" autocomplete="off">
                                 <div id="location-error"></div>
                                 <i class="la la-map-marker"></i>
                              </div>
                              <?php if(!empty($errors['address'])) {echo "<span>".$errors['address']."</span>";} ?>
                           </div>
                           <div class="spnrotw">
                              <div class="cfield">
                                 <select name="phonecode" id="phonecode">
                                    <option value=""> Select Country Code</option>
                                    <?php
                                       foreach($phonecodes as $phonecode) {
                                       ?>
                                    <option value="<?php echo $phonecode["phonecode"];?>" <?php if(!empty($userData['phonecode'])) { if($userData['phonecode'] == $phonecode["phonecode"]){ echo "selected";}} ?> <?php if($phonecode["phonecode"]=='63'){ echo "selected";} ?> > <?php echo $phonecode["name"];?> - <?php echo $phonecode["phonecode"];?> </option>
                                    <?php
                                       }
                                    ?>
                                 </select>
                              </div>
                              <?php if(!empty($errors['phonecode'])) {echo "<span>".$errors['phonecode']."</span>";} ?>
                           </div>
                           <div class="spnrotw">
                              <div class="cfield">
                                 <input type="number" placeholder="Phone Number" name="phone" value="<?php if(!empty($userData['phone'])){ echo $userData['phone']; } ?>" autocomplete="off">
                                 <i class="la la-phone"></i>
                              </div>
                              <?php if(!empty($errors['phone'])) {echo "<span>".$errors['phone']."</span>";} ?>
                           </div>
                           <!-- <div class="caddress">
                              </div> -->
                           <div class="spnrotw" style="min-height:83px;">
                              <div class="cfield">
                                 <input type="password" placeholder="Password" name="password" value="<?php if(!empty($userData['password'])){ echo $userData['password']; } ?>" autocomplete="off">
                                 <i class="la la-key"></i>
                              </div>
                              <?php if(!empty($errors['password'])) {echo "<span>".$errors['password']."</span>";}else{ ?>
                              <p class="pwd_err" style="font-size: 11px; float: left; margin: -10px 0 0;">Password should be alphanumeric.</p>
                              <?php } ?>
                           </div>
                           <div class="spnrotw" style="min-height:93px;">
                              <div class="cfield">
                                 <input type="password" placeholder="Confirm Password" name="confirm_password" value="<?php if(!empty($userData['confirm_password'])){ echo $userData['confirm_password']; } ?>"  autocomplete="off">
                                 <i class="la la-key"></i>
                              </div>
                              <?php if(!empty($errors['confirm_password'])) {echo "<span>".$errors['confirm_password']."</span>";} ?>
                           </div>
                           <br>
                           <button type="submit" class="nextstepsecond">Submit</button>
                        </div>
                     </div>
               </form>
               <div class="sign">
               <p>Already Have An Account?<span><a href="<?php echo base_url();?>recruiter/login"><strong>Sign In</strong></a></span></p>
               </div>
               </div>
            </div>
         </div>
      </section>
      <?php include_once('footer.php'); ?>
      </div>
      <!-- LOGIN POPUP -->
      <!-- SIGNUP POPUP -->
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&sensor=false&libraries=places&callback=initMap"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/maps2.js" type="text/javascript"></script>
      <script type="text/javascript">
         google.maps.event.addDomListener(window, 'load', function () {
             var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
             google.maps.event.addListener(places, 'place_changed', function () {
                 var place = places.getPlace();
                 var address = place.formatted_address;
                 var latitude = place.geometry.location.A;
                 var longitude = place.geometry.location.F;
                 var mesg = "Address: " + address;
                 mesg += "\nLatitude: " + latitude;
                 mesg += "\nLongitude: " + longitude;
             });
         });
      </script>
      <script type="text/javascript">
         function doFunction() {
              alert("asdas");
              if($("#domainField").val() == '') {
                 $("#domainFieldspan").html('Domain field is required');
              } else{
                 var em = $("#emailField").val();
                 var do = $("#domainField").val();
                 var splitEm = str.split("@");
                 alert(splitEm);
              }
           };
            
      </script>
   </body>
</html>


