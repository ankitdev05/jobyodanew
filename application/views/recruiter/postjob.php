<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery-ui.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery-ui.theme.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery.timepicker.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/individual/css-postjob.css" />
      <style type="text/css">
         .work-radio{height: 62px!important;width: 120px!important;}
         .leave-radio{height: 62px!important;width: 120px!important;}
         .allowcls input{height: 62px!important;width: 120px!important;}
      </style>
   </head>
    
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 MainWrapper">
                        <div class="row">
                           <div class="col-md-12 psthbs">
                              <div class="companyprofileforms fullwidthform">
                                 <?php if($this->session->tempdata('inserted')) {?>
                                 <p class="insertMsg"><?php echo $this->session->tempdata('inserted'); ?></p>
                                 <?php } ?>
                                 <?php if($this->session->tempdata('postError')) {?>
                                 <p class="errorMsg"><?php echo $this->session->tempdata('postError'); ?></p>
                                 <?php } ?>
                                 <form id="jobpostid" method="post" action="<?php echo base_url();?>recruiter/jobpost/jobpostInsert" enctype="multipart/form-data">
                                    <div class="myhdainside">
                                       <h6>Let's Do this!!</h6>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 1</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p>Let's get the general details locked for this post</p>
                                                </div>
                                                <div class="col-md-6"> 
                                                   <label>What is the Job Title?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input autocomplete="off" type="text" class="form-control" name="jobTitle" placeholder="Job Title" value="<?php if(!empty($jobdata['jobTitle'])){ echo $jobdata['jobTitle'];}?>">
                                                   <?php if(!empty($errors['jobTitle'])){echo "<p class='validError'>".$errors['jobTitle']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Which site is the opening for (Target)?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select class="locselect" name="jobLoc" id="jobLoc">
                                                      <option value=""> Select Location </option>
                                                      <?php
                                                         if($addresses) {
                                                            //print_r($addresses);die;
                                                             foreach($addresses as $address) {
                                                         ?>
                                                      <option value="<?php echo $address['recruiter_id']; ?>" > <?php echo $address['cname']; ?> </option>
                                                      <?php
                                                         }
                                                         }
                                                         ?>
                                                   </select>
                                                   <?php if(!empty($errors['jobLoc'])){ echo "<p class='validError'>".$errors['jobLoc']."</p>";} ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>How many people do you need for this job?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="number" autocomplete="off" class="form-control" name="opening" placeholder="No. of Openings" min="1" value="<?php if(!empty($jobdata['opening'])){ echo $jobdata['opening'];}?>">
                                                   <?php if(!empty($errors['opening'])){echo "<p class='validError'>".$errors['opening']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>When do you want us to take down this post?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" autocomplete="off" class="form-control" id="jobExpireid" name="jobExpire" placeholder="Expiry Date" value="<?php if(!empty($jobdata['jobExpire'])){ echo $jobdata['jobExpire'];}?>">
                                                   <?php if(!empty($errors['jobExpire'])){echo "<p class='validError'>".$errors['jobExpire']."</p>";}?>
                                                   <?php $userSession = $this->session->userdata('userSession');
                                                   if(!empty($userSession['label']) && $userSession['label']=='3'){
                                                            $userSession['id'] = $userSession['parent_id'];
                                                        }else{
                                                            $userSession['id'] = $userSession['id'];
                                                        }
                                                    ?>
                                                   <input type="hidden" name="recruId" value="<?php echo $userSession['id']; ?>">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>What is the Job level for this post?</label>
                                                </div>
                                                <div class="col-md-6 lvlty">
                                                   <select name="level" class="halfsideth">
                                                      <option value=""> Select Level </option>
                                                      <?php
                                                         if($levels) {
                                                             foreach($levels as $level) {
                                                         ?>
                                                      <option <?php if(!empty($jobdata['level']) && $jobdata['level']==$level['id']){ echo "selected"; }?>  value="<?php echo $level['id']; ?>" > <?php echo $level['level']; ?> </option>
                                                      <?php
                                                         }
                                                         }
                                                         ?>
                                                   </select>
                                                   <label class="switch" style="width:66px;">
                                                   <input type="checkbox" name="level_status" >
                                                   <span class="slider" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                                                   </label>
                                                   <?php if(!empty($errors['level'])){echo "<p class='validError'>".$errors['level']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Required level of education?</label>
                                                </div>
                                                <div class="col-md-6  lvlty">
                                                   <select name="education" class="halfsideth">
                                                      <option value=""> Select Education Level </option>
                                                      <option value="No Requirement" <?php if(!empty($jobdata['education'])) { if($jobdata['education'] == 'No Requirement'){  echo "selected"; }}?>>No Requirement</option>
                                                      <option value="Vocational" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'Vocational'){ echo "selected"; }}?>>Vocational</option>
                                                      <option value="High School Graduate" <?php if(!empty($jobdata['education'])){ $jobdata['education'] == 'High School Graduate' ? ' selected="selected"' : '';}?>>High School Graduate</option>
                                                      
                                                      <option value="Undergraduate" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'Undergraduate'){ echo  "selected"; }}?>>Undergraduate</option>
                                                      <option value="Associate Degree" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'Associate Degree'){ echo "selected"; }}?>>Associate Degree</option>
                                                      <option value="College Graduate" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'College Graduate'){ echo "selected"; }}?>>College Graduate</option>
                                                      <option value="Post Graduate" <?php if(!empty($jobdata['education'])){ if($jobdata['education'] == 'Post Graduate'){ echo "selected"; }}?>>Post Graduate</option>
                                                   </select>
                                                   <label class="switch" style="width:66px;">
                                                   <input type="checkbox" name="education_status" >
                                                   <span class="slider" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                                                   </label>
                                                   <?php if(!empty($errors['education'])){echo "<p class='validError'>".$errors['education']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Certification Required? </label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" name="certification"  placeholder="Enter Certifications" value="<?php if(!empty($jobdata['certification'])){ echo $jobdata['certification'];}?>">
                                                   <?php if(!empty($errors['certification'])){echo "<p class='validError'>".$errors['certification']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Required Level of Experience?</label>
                                                </div>
                                                <div class="col-md-6 lvlty">
                                                   <select name="experience" id="requiredexp" class="halfsideth">
                                                      <option value=""> Select Experience </option>
                                                      
                                                      <option value="All Tenure" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == 'All Tenure'){ echo  "selected"; }}?>>All Tenure</option>
                                                      <option value="No Experience" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == 'No Experience'){ echo  "selected"; }}?>>No Experience</option>
                                                      <option value="< 6 months" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '< 6 months'){ echo "selected"; }}?>>< 6 months</option>

                                                      <option value="> 6 months" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '> 6 months'){ echo "selected"; }}?>>> 6 months</option>
                                                      
                                                      <option value="> 1 yr" <?php if(!empty($jobdata['experience'])){ if(strcmp($jobdata['experience'], "> 1 yr")){ "selected"; }}?>>> 1 yr</option>
                                                      <option value="> 2 yr" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '> 2 yr'){ echo "selected"; }}?>>> 2 yr</option>
                                                      <option value="> 3 yr" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '> 3 yr'){ echo "selected"; }}?>>> 3 yr</option>
                                                      <option value="> 4 yr" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '> 4 yr'){ echo "selected"; }}?>>> 4 yr</option>
                                                      <option value="> 5 yr" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '> 5 yr'){ echo "selected"; }}?>>> 5 yr</option>
                                                      <option value="> 6 yr" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '> 6 yr'){ echo "selected"; }}?>>> 6 yr</option>
                                                      <option value="> 7 yr" <?php if(!empty($jobdata['experience'])){ if($jobdata['experience'] == '> 7 yr'){ echo "selected"; }}?>>> 7 yr</option>
                                                   </select>
                                                   <label class="switch" style="width:66px;">
                                                   <input type="checkbox" name="experience_status" >
                                                   <span class="slider" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                                                   </label>
                                                   <?php if(!empty($errors['experience'])){echo "<p class='validError'>".$errors['experience']."</p>";}?>
                                                </div>
                                             </div>
                                             <?php if(!empty($recruiterStatus[0]['salary_status']) && $recruiterStatus[0]['salary_status']=='1'){ ?>
                                             <div class="row">
                                                <div class="col-sm-6">
                                                      <label>Offers can vary depending on experience, you might want to put an amount based on experience range</label>
                                                </div>
                                                <div class="col-sm-6">
                                                      <table id="myTable">
                                                            <tbody>
                                                                  <tr>
                                                                        <td width="130px">
                                                                              <select name="expRange[]" required="" id="expgrid" class="expgrid">
                                                                              </select>
                                                                        </td>
                                                                        <td>
                                                                              <input name="expBasicSalary[]" type="number" min="1" class="form-control" placeholder="Enter Basic Salary per Month" required="">
                                                                        </td>
                                                                  </tr>
                                                            </tbody>
                                                      </table>
                                                </div>
                                                <div class="col-sm-12">
                                                      <p onclick="myFunction()" class="addmre">Add More Salary Range</p>
                                                </div>
                                             </div>
                                             
                                             <div style="clear:both;"></div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>How much is the Total Guaranteed Allowance? </label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="number" class="form-control" name="allowance" min='0' placeholder="Enter Total Guaranteed Allowance" value="<?php if(!empty($jobdata['allowance'])){ echo $jobdata['allowance'];}?>">
                                                   <?php if(!empty($errors['allowance'])){echo "<p class='validError'>".$errors['allowance']."</p>";}?>
                                                </div>
                                             </div>
                                             
                                             <?php }else{} ?>

                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>How much is the Joining Bonus? </label>
                                                   <input style="opacity: 1;float: right;position: absolute;top: 16%;z-index: 0;left: 70%;" type="checkbox" name="bonuscheck" id="bonuscheck" onclick="checkstatus()" <?php if(!empty($jobdata['bonuscheck'])){ echo "checked";}?>>
                                                </div>
                                                <div class="col-md-6" id="bonusdiv" <?php if(!empty($jobdata['bonuscheck'])){?> style="visibility:visible;opacity:1;" <?php } else { ?> style="visibility:hidden;opacity:0;" <?php }?> >
                                                   <input type="number" autocomplete="off" class="form-control" id="bonus_amount" name="bonus_amount" placeholder="Enter Joining Bonus" value="<?php if(!empty($jobdata['bonus_amount'])){ echo $jobdata['bonus_amount'];}?>">
                                                   <p class='validError customerror'></p>
                                                </div>
                                             </div>

                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Interview Mode? </label>
                                                </div>
                                                <div class="col-md-6">
                                                   <div style="display: flex;">
                                                      <input style="opacity:1;z-index:0;position:relative;" type="radio" name="modecheck" value="Walk-in" checked="" class="screeningurlRadio"><span> Walk-in</span>
                                                      <input  style="opacity:1;z-index:0;position:relative;" type="radio" name="modecheck" value="Call" class="screeningurlRadio"><span> Call</span>
                                                      <input style="opacity:1;z-index:0;position:relative;" type="radio" name="modecheck" value="Instant screening" class="screeningurlRadio"><span> Instant screening</span>
                                                   </div>
                                                   <p class='validError customerror'></p>
                                                </div>
                                             </div>

                                             <div class="row" id="screeningurl" style="display: none;">
                                                <div class="col-md-6">
                                                   <label>Instant screening URL</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="autocomplete">
                                                      <input id="myInputtt" type="text" name="modeurl" placeholder="Enter URL" class="form-control">
                                                   </div>
                                                </div>
                                             </div>
                                             
                                          </div>
                                       </div>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 2</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p>To help narrow down the talents, let's categorize this post.</p>
                                                </div>
                                                <div class="col-md-6">
                                                   <label>Please choose the best category for the job.</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select name="category" id="category">
                                                      <option value=""> Select Category </option>
                                                      <?php
                                                         if($category) {
                                                             foreach($category as $categorys) {
                                                         ?>
                                                      <option  value="<?php echo $categorys['id']; ?>" <?php if(!empty($jobdata['category'])){ if($jobdata['category'] == $categorys['id']){ echo "selected";}}?>> <?php echo $categorys['category']; ?> </option>
                                                      <?php
                                                         }
                                                         }
                                                         ?>
                                                   </select>
                                                   <?php if(!empty($errors['category'])){ echo "<p class='validError'>".$errors['category']."</p>";} ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Let's put in some more detailed category.</label>
                                                </div>
                                                <div class="col-md-6">
                                                <select name="subcategory" id="subcategory">
                                                <?php 
                                                      if(!empty($jobdata['category'])){
                                                      foreach($subcategory as $sub) {
                                                ?>
                                                      <option value="<?php echo $sub['id'] ?>" <?php if($jobdata['subcategory']==$sub['id']){echo "selected";}?>> <?php echo $sub['subcategory'] ?> </option>
                                                <?php
                                                      }
                                                } else {
                                                ?>
                                                  
                                                      <option value="">Select SubCategory</option>
                                                   
                                                <?php
                                                }
                                                ?>
                                                </select>
                                                <?php if(!empty($errors['subcategory'])){ echo "<p class='validError'>".$errors['subcategory']."</p>";} ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>Can you tell us about the language to be supported?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select name="lang">
                                                      <option value="">Select Language</option>
                                                      <?php
                                                         foreach($langs as $lang) {
                                                         ?>
                                                      <option value="<?php echo $lang['id'];?>" <?php if(!empty($jobdata['lang'])){ if($jobdata['lang'] == $lang['id']) {echo "selected";}} ?>><?php echo $lang['name'];?></option>
                                                      <?php }?>
                                                   </select>
                                                   <?php if(!empty($errors['lang'])){ echo "<p class='validError'>".$errors['lang']."</p>";} ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-6">
                                                   <label>If you need bilingual agents, please specify the other language.</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="autocomplete">
                                                      <input id="myInput" type="text" name="otherlanguage" placeholder="Select Languange" class="form-control">
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- <div class="row">
                                                <div class="col-md-6">
                                                   <label>Type of Job</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <select name="job_type" id="job_type">
                                                         <option value="">Select Type of Job</option>   
                                                         <option value="1">Direct</option>
                                                         <option value="2">Walk In</option>
                                                   </select>
                                                </div>
                                                </div>
                                                <div class="hidden_div">
                                                <div class="row">
                                                <div class="col-md-6">
                                                   <label>Walkin Date</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="date" class="form-control" name="walkin_date" value="<?php if(!empty($jobdata['walkin_date'])){ echo $jobdata['walkin_date'];}?>">
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                                   <label>From Time</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" name="walkin_from" id="walkin_from" value="<?php if(!empty($jobdata['walkin_from'])){ echo $jobdata['walkin_from'];}?>">
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                                   <label>Walkin To</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" name="walkin_to" id="walkin_to" value="<?php if(!empty($jobdata['walkin_to'])){ echo $jobdata['walkin_to'];}?>">
                                                </div>
                                                </div>
                                                </div> -->
                                          </div>
                                       </div>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 3</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p>Let's get to know more about the job</p>
                                                </div>
                                                <div class="col-md-4">
                                                   <label>Create your job Pitch!</label>
                                                </div>
                                                <div class="col-md-8">
                                                   <textarea id="lengthtexttt" maxlength="2000" class="form-control" placeholder="Make it count! This will attract jobseekers to view your job posting!" name="jobPitch"><?php if(!empty($jobdata['jobPitch'])){ echo $jobdata['jobPitch'];}?></textarea>

                                                   <span style="color:#27ae62;font-size: 11px;">Maximum characters limit: 2000 </span>
                                                   <span id="getlengthtexttt" style="color:#27ae62"> </span>

                                                   <?php if(!empty($errors['jobPitch'])){echo "<p class='validError'>".$errors['jobPitch']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row" style="margin: 15px -15px 0px -15px">
                                                <div class="col-md-4">
                                                   <label>Please describe the Job</label>
                                                </div>
                                                <div class="col-md-8">
                                                   <textarea id="lengthtext" maxlength="2000" rows="6" class="form-control" placeholder="Job Description" name="jobDesc"><?php if(!empty($jobdata['jobDesc'])){ echo $jobdata['jobDesc'];}?></textarea>
                                                   <span style="color:#27ae62;font-size: 11px;">Maximum characters limit: 2000 </span>
                                                   <span id="getlengthtext" style="color:#27ae62"> </span>
                                                   
                                                   <?php if(!empty($errors['jobDesc'])){echo "<p class='validError'>".$errors['jobDesc']."</p>";}?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-4">
                                                   <label>What skills do you require for this job?</label>
                                                </div>
                                                <div class="col-md-6 multipleopyodf">
                                                      <select name="skills[]" id="multiselect" multiple="multiple" required="">
                                                      <option value="">Select Skills</option>
                                                      <?php
                                                         if($skills) {
                                                             foreach($skills as $skill) {
                                                         ?>
                                                      <option  value="<?php echo $skill['id']; ?>" <?php if(!empty($jobdata['skills'])){ if($jobdata['skills'] == $skill['id']){ echo "selected";}}?>> <?php echo $skill['skill']; ?> </option>
                                                      <?php
                                                         }
                                                         }

                                                         ?>
                                                      <option id="others" value="12" <?php if(!empty($jobdata['skills'])){ if($jobdata['skills'] == 12){ echo "selected";}}?>>Others</option>   
                                                   </select>
                                                   <?php if(!empty($errors['skills'])){echo "<p class='validError'>".$errors['skills']."</p>";}?>
                                                    <input type="text" class="form-control" name="skill" id="other_skill" placeholder="Other Skills" value="<?php if(!empty($jobdata['skill'])){ echo $jobdata['skill'];}?>" style="visibility: hidden; opacity: 0;"> 
                                                   
                                                </div>
                                             </div>
                                             <!-- <div class="row">
                                                <div class="col-md-6">
                                                   <label>What Qualifications do you require for this job?</label>
                                                </div>
                                                <div class="col-md-6">
                                                   <input type="text" class="form-control" name="qualification" placeholder="Qualifications" value="<?php if(!empty($jobdata['qualification'])){ echo $jobdata['qualification'];}?>">
                                                   <?php if(!empty($errors['qualification'])){echo "<p class='validError'>".$errors['qualification']."</p>";}?>
                                                   <select>
                                                      <option value="Bs Graduate">Bs Graduate</option>
                                                      <option value="Undergraduate">Undergraduate</option>
                                                      <option value="Associate Degree">Associate Degree</option>
                                                      <option value="College Graduate">College Graduate</option>
                                                      <option value="Post Graduate">Post Graduate</option>
                                                      
                                                      </select>
                                                </div>
                                             </div> -->
                                          </div>
                                       </div>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 4</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p><sup>*</sup> You can upload up to 3 pictures which would show on the job posting</p>
                                                </div>
                                                <div class="col-md-4">
                                                   <label>Advertise your job by posting pics (RnR,Family Day,Site or Account Pics etc)</label>
                                                </div>
                                                <div class="col-md-8">
                                                   <table id="myTable1">
                                                      <tbody>
                                                         <tr>
                                                            <td><input type="file" id="jobfile" name="job_image[]" class="form-control" required="" accept="image/x-png,image/jpg,image/jpeg">
                                                               <span style="color:red;font-size:14px;" id="invalidfileize"></span><BR>
                                                                  <span style="font-size:13px;color:#feab28;">Please advertise your job by posting pics (RnR,Family Day,Site or Account Pics etc) here. Image dimension within 400X300 - 450X350. It is very important to follow Image specifications for optimal results on the App</span>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <p onclick="myFunction1()" class="addmre" id="addmr">Add Picture</p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="filldetails">
                                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Step 5</h5>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                          </div>
                                          <div class="dividehalfd">
                                             <div class="row">
                                                <div class="col-md-12 stepsinfosd">
                                                   <p>We have taken the benefits offered by your company and at the site level. Please review the list and update as necessary to include offers exclusive to this job.</p>
                                                </div>
                                                <div class="col-md -12 showformsdf">
                                                   <h6>Top Picks</h6>
                                                   <div class="filterchekers">
                                                      <ul>
                                                         <li id="topicks1" class="allowcls">
                                                            <input type="checkbox" id="toppic1" name="toppicks[]" value="1" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/joining-bonus.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/joining-bonus-1.png" class="hvrsicos">
                                                            <p> Joining<br>Bonus</p>
                                                         </li>
                                                         <li id="topicks2" class="allowcls">
                                                            <input type="checkbox" id="toppic2" name="toppicks[]" value="2" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food-1.png" class="hvrsicos">                                    
                                                            <p> Free <br>Food</p>
                                                         </li>
                                                         <li id="topicks3" class="allowcls">
                                                            <input type="checkbox" id="toppic3" name="toppicks[]" value="3" onclick="clickfuncheck(this.id)">
                                                            <!--  <i class="fas fa-heartbeat"></i> -->
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-white.png" class="hvrsicos">     
                                                            <p>Day 1 HMO</p>
                                                         </li>
                                                         <li id="topicks4" class="allowcls">
                                                            <input type="checkbox" id="toppic4" name="toppicks[]" value="4" onclick="clickfuncheck(this.id)">
                                                            <!--   <i class="fas fa-heartbeat"></i> -->
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended-1.png" class="hvrsicos"> 
                                                            <p> Day 1 HMO<br> for Dependent</p>
                                                         </li>
                                                         <li id="topicks5" class="allowcls">
                                                            <input type="checkbox" id="toppic5" name="toppicks[]" value="5" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-shift.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-shift-1.png" class="hvrsicos">
                                                            <p>Day Shift</p>
                                                         </li>
                                                         <li id="topicks6" class="allowcls">
                                                            <input type="checkbox" id="toppic6" name="toppicks[]" value="6" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay-1.png" class="hvrsicos">
                                                            <p> 14th Month Pay</p>
                                                         </li>

                                                         <li id="topicks7" class="allowcls">
                                                            <input type="checkbox" id="toppic7" name="toppicks[]" value="7" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfh.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfhun.png" class="hvrsicos">
                                                            <p> Work From Home</p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="col-md-12 showformsdf">
                                                   <h6>Medical Benefits</h6>
                                                   <div class="filterchekers">
                                                      <ul>
                                                         <li id="medical1" class="allowcls">
                                                            <input type="checkbox" id="medi1" name="medical[]" value="1" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents-1.png" class="hvrsicos">
                                                            <p> Free HMO for<br>Dependents</p>
                                                         </li>
                                                         <li id="medical2" class="allowcls">
                                                            <input type="checkbox" id="medi2" name="medical[]" value="2" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits1.png" class="hvrsicos">
                                                            <p> Critical Illness <br>Benefits</p>
                                                         </li>
                                                         <li id="medical3" class="allowcls">
                                                            <input type="checkbox" id="medi3" name="medical[]" value="3" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence-1.png" class="hvrsicos">
                                                            <p>Life <br>Insurance</p>
                                                         </li>
                                                         <li id="medical4" class="allowcls">
                                                            <input type="checkbox" id="medi4" name="medical[]" value="4" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance-1.png" class="hvrsicos">
                                                            <p> Maternity<br> Assistance</p>
                                                         </li>
                                                         <li id="medical5" class="allowcls">
                                                            <input type="checkbox" id="medi5" name="medical[]" value="5" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer-1.png" class="hvrsicos">
                                                            <p>Medicine <br>Reimbursement</p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="col-md-12 showformsdf">
                                                   <h6>Allowances and Incentives</h6>
                                                   <div class="filterchekers">
                                                      <ul>
                                                         <li id="allowance1" class="allowcls">
                                                            <input type="checkbox" id="allowanc1" name="allowances[]" value="1" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance-1.png" class="hvrsicos">
                                                            <p> Cellphone <br>Allowance</p>
                                                         </li>
                                                         <li id="allowance2" class="allowcls">
                                                            <input type="checkbox" id="allowanc2" name="allowances[]" value="2" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking-1.png" class="hvrsicos">
                                                            <p>Free <br>Parking</p>
                                                         </li>
                                                         <li id="allowance3" class="allowcls">
                                                            <input type="checkbox" id="allowanc3" name="allowances[]" value="3" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle-1.png" class="hvrsicos"> 
                                                            <p> Free <br> Shuttle</p>
                                                         </li>
                                                         <li id="allowance4" class="allowcls">
                                                            <input type="checkbox" id="allowanc4" name="allowances[]" value="4" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus-1.png" class="hvrsicos">
                                                            <p> Annual <br> Performance Bonus</p>
                                                         </li>
                                                         <li id="allowance5" class="allowcls">
                                                            <input type="checkbox" id="allowanc5" name="allowances[]" value="5" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirment-benifits.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirments-benefits-1.png" class="hvrsicos">
                                                            <p> Retirements <br> Benefits</p>
                                                         </li>
                                                         <li id="allowance6" class="allowcls">
                                                            <input type="checkbox" id="allowanc6" name="allowances[]" value="6" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation-1.png" class="hvrsicos">
                                                            <p> Transport <br> Allowance</p>
                                                         </li>
                                                         <li id="allowance7" class="allowcls">
                                                            <input type="checkbox" id="allowanc7" name="allowances[]" value="7" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive-1.png" class="hvrsicos"> 
                                                            <p> Monthly Performance <br> Incentives</p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="col-md-12 showformsdf">
                                                   <h6>Schedule</h6>
                                                   <div class="filterchekers">
                                                      <ul>
                                                         <li id="leaves1" class="leave-class">
                                                            <input type="checkbox" id="leave1" class="leave-radio" name="leavs[]" value="41" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/weekend-off.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/weekend-off-1.png" class="hvrsicos">
                                                            <p> Weekends Off</p>
                                                         </li>
                                                         <li id="leaves2" class="leave-class">
                                                            <input type="checkbox" id="leave2" class="leave-radio" name="leavs[]" value="42" onclick="clickfuncheck(this.id)">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/holiday-off.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/holiday-off-1.png" class="hvrsicos">
                                                            <p>Holidays Off</p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="col-md-12 showformsdf">
                                                   <h6>Work Shifts</h6>
                                                   <div class="filterchekers1">
                                                      <ul>
                                                         <li id="workshift1" class="work-class">
                                                            <input type="radio" id="works1" class="work-radio" name="shifts[]" value="1">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/mid-shift.png" class="normicos">  
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/mid-shift-1.png" class="hvrsicos">
                                                            <p> Mid Shift </p>
                                                         </li>
                                                         <li id="workshift2" class="work-class">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/night-shift.png" class="normicos">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/night-shift-1.png" class="hvrsicos">
                                                            <input type="radio" id="works2" class="work-radio" name="shifts[]" value="2">
                                                            <p> Night Shift </p>
                                                         </li>
                                                         <li id="workshift3" class="work-class">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/24.png" class="normicos">
                                                            <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/24-1.png" class="hvrsicos">
                                                            <input type="radio" id="works3" class="work-radio" name="shifts[]" value="3">
                                                            <p>24/7 </p>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="PostjobButton"> 
                                                  <button id="post_button" type="submit">Post Job</button> 
                                             </div>
                                          </div>
                                 </form>
                                 </div>     
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      <!-- <input type="hidden" id="numberofrecord" value="0"> -->
      </section>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/jquery.timepicker.min.js"></script>
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

      <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> 
      <script type="text/javascript">
         $(document).ready(function(){  
            $.datepicker.setDefaults({  
                 dateFormat: 'yy-mm-dd'   
            });  
            $(function(){  
                 $("#jobExpireid").datepicker({ minDate: +1}); 
            });
           });
      </script>
      <script>
         function myFunction() {

        
            
           var table = document.getElementById("myTable");
           var rows = document.getElementById("myTable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
           
           var row = table.insertRow(rows);
           var cell1 = row.insertCell(0);
           var cell2 = row.insertCell(1);
           var exp_select = $('#requiredexp').val();

           cell1.innerHTML = '<select name="expRange[]" class="expgrid"><option value="">Select Experience</option>';
            
            if(exp_select=='All Tenure'){
                  cell1.innerHTML = '<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="0">All Tenure</option></select>';

            } else if(exp_select=='No Experience'){
                  cell1.innerHTML = '<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="1">Minimum Experience</option></select>';
            
            } else if(exp_select=='< 6 months'){
                  cell1.innerHTML = '<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="1">Minimum Experience</option><option value="2">less than 6 months</option></select>';
            
            } else if(exp_select=='> 6 months'){
                  cell1.innerHTML ='<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="3">6mo to 1 yr</option><option value="4">1 yr to 2 yr</option><option value="5">2 yr to 3 yr</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            } else if(exp_select=='> 1 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="4">1 yr to 2 yr</option><option value="5">2 yr to 3 yr</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            } 
            else if(exp_select=='> 2 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="5">2 yr to 3 yr</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            } else if(exp_select=='> 3 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            } else if(exp_select=='> 4 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            }  
             else if(exp_select=='> 5 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            } 
             else if(exp_select=='> 6 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            }
            else if(exp_select=='> 7 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="10">7 yr & up</option></select>';
            }  
           /*cell1.innerHTML = '<select name="expRange[]" class="expgrid"><option value="">Select Experience</option><option value="All Tenure">All Tenure</option><option value="less than 6 months">less than 6 months</option><option value="6mo to 1 yr">6mo to 1 yr</option><option value="1 yr to 2 yr">1 yr to 2 yr</option><option value="2yr to 3 yr">2yr to 3 yr</option><option value="3yr and up">3yr and up</option></select>';*/
           cell2.innerHTML = "<input type='number' min='1' name='expBasicSalary[]' class='form-control' placeholder='Enter Basic Salary per Month'/><a href='javascript:void(0);' class='remove'><i class='fa fa-trash'></i></a>";
         }
       $(document).on("click", "a.remove" , function() {
            $(this).parent().parent().remove();
        });
      </script>
      <script type="text/javascript">
         function checkstatus(){
                var check = $('#bonuscheck').is(':checked');
                if(check==true){
                     $('#bonusdiv').css('visibility','visible');
                     $('#bonusdiv').css('opacity','1');
                     $('#topicks1').addClass('selectedgreen');
                     $('#toppic1').attr("checked", "checked");
                }else{
                     $('#bonusdiv').css('visibility','hidden');
                     $('#bonusdiv').css('opacity','0');
                     $('#topicks1').removeClass('selectedgreen');
                }
         }
               
      </script>
      <script type="text/javascript">
         function autocomplete(inp, arr) {
           /*the autocomplete function takes two arguments,
           the text field element and an array of possible autocompleted values:*/
           var currentFocus;
           /*execute a function when someone writes in the text field:*/
           inp.addEventListener("input", function(e) {
               var a, b, i, val = this.value;
               /*close any already open lists of autocompleted values*/
               closeAllLists();
               if (!val) { return false;}
               currentFocus = -1;
               /*create a DIV element that will contain the items (values):*/
               a = document.createElement("DIV");
               a.setAttribute("id", this.id + "autocomplete-list");
               a.setAttribute("class", "autocomplete-items");
               /*append the DIV element as a child of the autocomplete container:*/
               this.parentNode.appendChild(a);
               /*for each item in the array...*/
               for (i = 0; i < arr.length; i++) {
                 /*check if the item starts with the same letters as the text field value:*/
                 if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                   /*create a DIV element for each matching element:*/
                   b = document.createElement("DIV");
                   /*make the matching letters bold:*/
                   b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                   b.innerHTML += arr[i].substr(val.length);
                   /*insert a input field that will hold the current array item's value:*/
                   b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                   /*execute a function when someone clicks on the item value (DIV element):*/
                   b.addEventListener("click", function(e) {
                       /*insert the value for the autocomplete text field:*/
                       inp.value = this.getElementsByTagName("input")[0].value;
                       /*close the list of autocompleted values,
                       (or any other open lists of autocompleted values:*/
                       closeAllLists();
                   });
                   a.appendChild(b);
                 }
               }
           });
           /*execute a function presses a key on the keyboard:*/
           inp.addEventListener("keydown", function(e) {
               var x = document.getElementById(this.id + "autocomplete-list");
               if (x) x = x.getElementsByTagName("div");
               if (e.keyCode == 40) {
                 /*If the arrow DOWN key is pressed,
                 increase the currentFocus variable:*/
                 currentFocus++;
                 /*and and make the current item more visible:*/
                 addActive(x);
               } else if (e.keyCode == 38) { //up
                 /*If the arrow UP key is pressed,
                 decrease the currentFocus variable:*/
                 currentFocus--;
                 /*and and make the current item more visible:*/
                 addActive(x);
               } else if (e.keyCode == 13) {
                 /*If the ENTER key is pressed, prevent the form from being submitted,*/
                 e.preventDefault();
                 if (currentFocus > -1) {
                   /*and simulate a click on the "active" item:*/
                   if (x) x[currentFocus].click();
                 }
               }
           });
           function addActive(x) {
             /*a function to classify an item as "active":*/
             if (!x) return false;
             /*start by removing the "active" class on all items:*/
             removeActive(x);
             if (currentFocus >= x.length) currentFocus = 0;
             if (currentFocus < 0) currentFocus = (x.length - 1);
             /*add class "autocomplete-active":*/
             x[currentFocus].classList.add("autocomplete-active");
           }
           function removeActive(x) {
             /*a function to remove the "active" class from all autocomplete items:*/
             for (var i = 0; i < x.length; i++) {
               x[i].classList.remove("autocomplete-active");
             }
           }
           function closeAllLists(elmnt) {
             /*close all autocomplete lists in the document,
             except the one passed as an argument:*/
             var x = document.getElementsByClassName("autocomplete-items");
             for (var i = 0; i < x.length; i++) {
               if (elmnt != x[i] && elmnt != inp) {
                 x[i].parentNode.removeChild(x[i]);
               }
             }
           }
           /*execute a function when someone clicks in the document:*/
           document.addEventListener("click", function (e) {
               closeAllLists(e.target);
           });
         }
         
         /*An array containing all the country names in the world:*/
         var countries = ["English", "Afar", "Abkhazian", "Afrikaans","Amharic","Arabic","Assamese","Aymara","Azerbaijani","Bashkir","Belarusian","Bulgarian","Bihari","Bislama","Bengali/Bangla","Tibetan","Breton","Catalan","Corsican","Czech","Welsh","Danish","German","Bhutani","Greek","Esperanto",
              "Spanis","Estonian","Basque","Persian","Finnish","Fiji","Faeroese","French","Frisian","Irish","Scots/Gaelic","Galician","Guarani","Gujarati","Hausa","Hindi","Croatian","Hungarian","Armenian","Interlingua","Interlingue","Inupiak","Indonesian","Icelandic","Italian","Hebrew","Japanese","Yiddish","Javanese","Georgian","Kazakh","Greenlandic","Cambodian","Kannada","Korean","Kashmiri","Kurdish","Kirghiz","Latin","Lingala","Laothian","Lithuanian","Latvian/Lettish","Malagasy","Maori","Macedonian","Malayalam","Mongolian","Moldavian","Marathi","Malay","Maltese","Burmese","Nauru","Nepali","Dutch","Norwegian","Occitan","(Afan)/Oromoor/Oriya","Punjabi","Polish","Pashto/Pushto","Portuguese","Quechua","Rhaeto-Romance","Kirundi","Romanian","Russian","Kinyarwanda","Sanskrit","Sindhi","Sangro","Serbo-Croatian","Singhalese","Slovak","Slovenian","Samoan","Shona","Somali","Albanian","Serbian","Siswati","Sesotho","Sundanese","Swedish","Swahili","Tamil","Telugu","Tajik","Thai","Tigrinya","Turkmen","Tagalog","Setswana","Tonga","Turkish","Tsonga","Tatar","Twi","Ukrainian","Urdu","Uzbek","Vietnamese","Volapuk","Wolof","Xhosa","Yoruba","Chinese","Zulu"];
         
         /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
         autocomplete(document.getElementById("myInput"), countries);
      </script>
      <style>
         table#myTable {
         width: 100%;
         }
         table#myTable td {
         padding: 0 9px 0 0;
         }
         .addmre {
         margin: 0;
         float: left;
         color: #fff;
         padding: 3px 8px;
         margin-bottom: 20px;
         cursor: pointer;
         float: right;
         }
         table#myTable td:last-child {
         padding: 0;
         }
      </style>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
      <script type="text/javascript">
         $(document).ready(function(){
            //e.preventDefault();
               $('#jobpostid').validate({
                    rules: {
                      jobTitle: {required:true},
                      jobLoc: {required:true},
                      opening: {required:true, number: true},
                      experience: { required: true},
                      salaryOffered: { required: true, number: true},
                      industry: { required: true},
                      level: { required: true},
                      education: { required: true},
                      category: { required: true},
                      subcategory: { required: true},
                      lang: { required: true},
                      jobDesc: { required: true},
                      jobPitch: { required: true},
                      skills: { required: true},
                      qualification: { required: true},
                      jobExpire: { required: true},
                      bonus_amount:{required: "#bonuscheck:checked"}
                    },
                    messages: {
                      jobTitle: {required:'Job Title field is required'},
                      jobLoc: {required:'Job Location field is required'},
                      opening: {required:'No. of Opening field is required'},
                      experience: { required:'Experience field is required'},
                      salaryOffered: { required:'Salary Offered field is required'},
                      industry: {required:'Industry field is required'},
                      level: {required:'Level field is required'},
                      education: {required:'Education field is required'},
                      category: {required:'Category field is required'},
                      subcategory: {required:'Sub-Category field is required'},
                      lang: {required:'Language field is required'},
                      jobDesc: { required:'Job Description field is required'},
                      jobPitch: { required:'Job Pitch field is required'},
                      skills: { required:'Skills field is required'},
                      qualification: { required:'Qualification field is required'},
                      jobExpire: { required:'Job Expire field is required'},
                      bonus_amount:{required:'Bonus Amount field is required'}
                    },
                    ignore: ':hidden:not("#multiselect")',
                    errorPlacement: function(error, element) {
                          if(element.attr("name") == "skills") {
                            error.appendTo( element.next("div") );
                          } else {
                            error.insertAfter(element);
                          }
                        }
               });
         });
      </script>

      <script>
         $(function() {
             $( "#date" ).datepicker({ minDate: +1 });
         });
         
         $(document).ready(function(){
             $('#jobLoc').change(function() {
                 var recruiter_id = $('#jobLoc').val();
                 //alert(recruiter_id);
                 $.ajax({
                 type:"POST",
                 url : "<?php echo base_url(); ?>/recruiter/recruiter/recruiter_location",
                 data : {recruiter_id:recruiter_id},
                 success : function(response1) {
                     var response2 = JSON.parse(response1);
                        $(".allowcls").removeClass("selectedgreen");       
                     if(response2['topPicks2'].length >= 1) {
                         var topicks = [2,3,4,6,7];
                         for($i=0; $i<response2['topPicks2'].length; $i++) {
                             var abc = topicks.indexOf(response2['topPicks2'][$i]);
                             if(abc) {
                                 var customclass = "#topicks" + response2['topPicks2'][$i];
                             
                                 $(customclass).addClass('selectedgreen');
                                 var cusid = "#toppic" + response2['topPicks2'][$i];
                                 $(cusid).attr("checked", "checked");
                             }
                         }
                     }
                     
                     if(response2['allowance2'].length >= 1) {
                         var allowance = [1,2,3,4,5,6,7];
                         for($i=0; $i<response2['allowance2'].length; $i++) {
                             var abc = allowance.indexOf(response2['allowance2'][$i]);
                             if(abc) {
                                 var customclass = "#allowance" + response2['allowance2'][$i];
                                  
                                  $(customclass).addClass('selectedgreen');
                                  var cusid = "#allowanc" + response2['allowance2'][$i];
                                  $(cusid).attr("checked", "checked");
                             }
                         }
                     }
                     
                     if(response2['medical2'].length >= 1) {
                         var medical = [1,2,3,4,5];
                         for($i=0; $i<response2['medical2'].length; $i++) {
                             var abc = medical.indexOf(response2['medical2'][$i]);
                             if(abc) {
                                 var customclass = "#medical" + response2['medical2'][$i];
                               
                                 $(customclass).addClass('selectedgreen');
                                 var cusid = "#medi" + response2['medical2'][$i];
                                 $(cusid).attr("checked", "checked");
                             }
                         }
                     }
                     
                     
                     /*if(response2['leave2'].length >= 1) {
                         var leave = [1,2];
                         for($i=0; $i<response2['leave2'].length; $i++) {
                             var abc = leave.indexOf(response2['leave2'][$i]);
                             if(abc) {
                                 var customclass = "#leaves" + response2['leave2'][$i];
                                 $(customclass).addClass('selectedgreen');
                                 var cusid = "#leave" + response2['leave2'][$i];
                                 $(cusid).attr("checked", "checked");
                             }
                         }
                     }*/
                     
                     
                     /*if(response2['workshift2'].length >= 1) {
                         var workshift = [1,2,3];
                         for($i=0; $i<response2['workshift2'].length; $i++) {
                             var abc = workshift.indexOf(response2['workshift2'][$i]);
                             if(abc) {
                                 var customclass = "#workshift" + response2['workshift2'][$i];
                                 $(customclass).addClass('selectedgreen');
                                 var cusid = "#works" + response2['workshift2'][$i];
                                 $(cusid).attr("checked", "checked");
                             }
                         }
                     }*/
                     
                 },
                 error: function() {
                     alert('Error occured');
                 }
                 });
             });
         });
      </script>
      <script>
         $(".filterchekers li").click(function(){
             $(this).toggleClass("selectedgreen");
         });

         $(".filterchekers1 li").click(function(){
             $(".work-class").removeClass("selectedgreen");
             $(this).toggleClass("selectedgreen");
         });
         
         
         function clickfuncheck(id) {
            if(id=='toppic1'){
                  $('#toppic1').addClass("selectedgreen");
                  $('#toppic1').prop('checked', true);
            }
             var cid =  "#"+id;
             if (document.getElementById(id).checked) {
                 $(cid).prop('checked', true);
             } else {
                 $(cid).prop('checked', false);
             }
         }
         
         
         $('.work-radio').change(function() {
             $(".work-class").removeClass("selectedgreen");
             if ($(this).is(':checked')){
                 $(this).closest("li").addClass("selectedgreen");
               }
               else
                 $(this).closest("li").removeClass("selectedgreen");
         });
      </script>
      <script>
         function myFunction1() {
          // var limit=5;
           var table = document.getElementById("myTable1");
           var rows = document.getElementById("myTable1").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
            if(rows<=2)
            {
               var row = table.insertRow(rows);
               var cell1 = row.insertCell(0);
                  
               cell1.innerHTML = "<input type='file' onchange='functionimg"+rows+"()' id='jobfile"+rows+"' name='job_image[]' class='form-control addinput' ><a href='javascript:void(0);' class='remove'><i class='fa fa-close'></i></a><BR><span style='color:red;font-size:14px;' id='invalidfileize"+rows+"'></span><BR>";
            }
            else{
                $('#addmr').hide();
            }
           
           
         }
         $(document).on("click", "a.remove" , function() {
            $(this).parent().remove();
         });
         
         $("#category").change(function(){
             //get category value
             var cat_val = $("#category").val();
             //alert(cat_val);
             // put your ajax url here to fetch subcategory
             var url             =   '<?php echo base_url(); ?>/recruiter/Jobpost/fetchSubcategory';
             // call subcategory ajax here 
             $.ajax({
                            type:"POST",
                            url:url,
                            data:{
                                cat_val : cat_val
                            },
         
                            success:function(data)
         
                             {
                              
                           $("#subcategory").html(data);
                        
                             }
                         });
         });

         $('#requiredexp').change(function(){
            var exp_select = $('#requiredexp').val();

            //var selectboxarray = [];

            if(exp_select=='All Tenure'){
              //    $("#numberofrecord").val(1);
                //  selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="0">All Tenure</option><select>');
            }
            else if(exp_select=='No Experience'){
                  //$("#numberofrecord").val(1);
                  //selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="1">Minimum Experience</option></select>');
            }
            else if(exp_select=='< 6 months'){
                  //$("#numberofrecord").val(2);
                  //selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="1">Minimum Experience</option><option value="2">less than 6 months</option></select>');
            }else if(exp_select=='> 6 months'){
                  //$("#numberofrecord").val(8);
                  //selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="3">6mo to 1 yr</option><option value="4">1 yr to 2 yr</option><option value="5">2 yr to 3 yr</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>');
            }else if(exp_select=='> 1 yr'){
                  //$("#numberofrecord").val(7);
                  //selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="4">1 yr to 2 yr</option><option value="5">2 yr to 3 yr</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>');
            }else if(exp_select=='> 2 yr'){
                  //$("#numberofrecord").val(3);
                  //selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="5">2 yr to 3 yr</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select></select>');
            }else if(exp_select=='> 3 yr'){
                  //$("#numberofrecord").val(2);
                  //selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>');
            }else if(exp_select=='> 4 yr'){
                  //$("#numberofrecord").val(2);
                  //selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>');
            }else if(exp_select=='> 5 yr'){
                  //$("#numberofrecord").val(2);
                  //selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>');
            }else if(exp_select=='> 6 yr'){
                  //$("#numberofrecord").val(2);
                  //selectboxarray = ["All Tenure"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>');
            }else if(exp_select=='> 7 yr'){
                  //$("#numberofrecord").val(2);
                  //selectboxarray = ["7 yr & up"];
                  $('.expgrid').html('<select name="expRange[]" required="" id="expgrid" class="expgrid"><option value="">Select Experience</option><option value="10">7 yr & up</option></select>');
            }
         })
         
         $('#walkin_from').timepicker({ 'timeFormat': 'H:i' });
         $('#walkin_to').timepicker({ 'timeFormat': 'H:i' });
         
         /*$(document).ready(function(){
           $('.hidden_div').hide();  
         });
           
         $('#job_type').on('change', function() {
              if( this.value =='2'){
                  $(".hidden_div").show();
              }
              if( this.value =='1'){
                  $(".hidden_div").hide();
              }
            });*/
      </script>
      <style>
         .psthbs .filldetails select.halfsideth {
         width: 63%;
         float: left;
         }
         .psthbs .filldetails select.halfsideth {
         width: 63%;
         float: left;
         }



         .filldetails .multipleopyodf button {
    padding: 0;
    float: left;
    width: 100%;
    background: transparent;
    color: #000;
    font-weight: 100;
    font-size: 12px;
    margin: 0;
    outline: none !important;
}

.multipleopyodf {
    position: relative;
}

.filldetails .multipleopyodf .btn-group {
    width: 100%;
}

.multipleopyodf input[type="checkbox"] {
    opacity: 1;
    position: relative;
    left: 3px;
    top: 2px;
    margin-right: 6px;
}

.multipleopyodf .dropdown-menu {
    padding: 10px;
    width: 100%;
}

      </style>

      <script type="text/javascript">
            $(document).ready(function () {
  $('#multiselect').multiselect({
    includeSelectAllOption: true,
    nonSelectedText: 'Select an Option' });
  $('#multiselect').on('change', function() {

      
  if($("#multiselect").val().includes("12")){
      $('#other_skill').css('visibility','visible');
      $('#other_skill').css('opacity','1');
  }else{
      $('#other_skill').css('visibility','hidden');
      $('#other_skill').css('opacity','0');
  }
});

});

function getSelectedValues() {
  var selectedVal = $("#multiselect").val();
  for (var i = 0; i < selectedVal.length; i++) {
    function innerFunc(i) {
      setTimeout(function () {
        location.href = selectedVal[i];
      }, i * 2000);
    }
    innerFunc(i);
  }
}
    
$(document).ready(function() {       
$('#logo').bind('change', function() {
    var a=(this.files[0].size);
    alert(a);
    if(a > 2000000) {
        alert('large');
    };
});
});
      </script>

      <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>
     <script type="text/javascript">
           $(".mebilemenuham").click(function (e) {
    e.stopPropagation();
    $(".border-right").toggleClass('opensidemenu');
}); $('.mebilemenuham').on('click', function () {        $(this).toggleClass('opensidemenu');});$(document).click(function (e) {    if (!$(e.target).hasClass('mebilemenuham')) {        $('.border-right.opensidemenu').removeClass('opensidemenu')    }})
$(document).click(function (e) {    if (!$(e.target).hasClass('mebilemenuham')) {        $('.mebilemenuham.opensidemenu').removeClass('opensidemenu')    }}) 
     </script>

     <script type="text/javascript">
           $(function() {
    var nav = $(".nohdascr");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 110) {
            nav.removeClass('nohdascr').addClass("hdafxd");
        } else {
            nav.removeClass("hdafxd").addClass('nohdascr');
        }
    });
});
     </script>

     <script type="text/javascript">
      $(document).ready(function() {
            $('#lengthtext').on('keyup', function() {
                  var lengthText = $(this).val();
                  var calcLength = parseInt(2000) - parseInt(lengthText.length);
                  var textcount = "Remaining characters : " + calcLength;
                  $('#getlengthtext').html(textcount);
            });
            $('#lengthtexttt').on('keyup', function() {
                  var lengthText = $(this).val();
                  var calcLength1 = parseInt(2000) - parseInt(lengthText.length);
                  var textcount = "Remaining characters : " + calcLength1;
                  $('#getlengthtexttt').html(textcount);
            });

      });
</script>

<script type="text/javascript"> 
   $(document).ready(function(){
         $('#jobfile').change(function () {
            var fileUpload = document.getElementById("jobfile");
            
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {

               if (typeof (fileUpload.files) != "undefined") {

                  //Initiate the FileReader object.
                  var reader = new FileReader();
                  //Read the contents of Image File.
                  reader.readAsDataURL(fileUpload.files[0]);
                  reader.onload = function (e) {
                      //Initiate the JavaScript Image object.
                      var image = new Image();
       
                      //Set the Base64 string return from FileReader as source.
                      image.src = e.target.result;
                             
                      //Validate the File Height and Width.
                      image.onload = function () {
                          var height = parseInt(this.height);
                          var width = parseInt(this.width);

                          if (width>=400 && height>=300) {

                              if(width<= 450 && height<= 350) {
                                 $("#invalidfileize").html("");

                              } else {
                                 $("#invalidfileize").html("Image size must be within 400X300 - 450X350");
                                 $('#jobfile').val('');
                              }
                              
                          } else {
                              $("#invalidfileize").html("Image size must be within 400X300 - 450X350");
                              $('#jobfile').val('');
                          }
                      };
       
                  }
               } else {
                  $('#jobfile').val('');
               }

            } else {
               $("#invalidfileize").html("Please select a valid Image file.");
               $('#jobfile').val('');
            }
         });
   });

   function functionimg1() {


         //$('#jobfile1').change(function () {
            var fileUpload = document.getElementById("jobfile1");

            console.log(fileUpload);
            
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {

               if (typeof (fileUpload.files) != "undefined") {

                  //Initiate the FileReader object.
                  var reader = new FileReader();
                  //Read the contents of Image File.
                  reader.readAsDataURL(fileUpload.files[0]);
                  reader.onload = function (e) {
                      //Initiate the JavaScript Image object.
                      var image = new Image();
       
                      //Set the Base64 string return from FileReader as source.
                      image.src = e.target.result;
                             
                      //Validate the File Height and Width.
                      image.onload = function () {
                          var height = parseInt(this.height);
                          var width = parseInt(this.width);

                          if (width>=400 && height>=300) {

                              if(width<= 450 && height<= 350) {
                                 $("#invalidfileize1").html("");

                              } else {
                                 $("#invalidfileize1").html("Image size must be within 400X300 - 450X350");
                                 $('#jobfile1').val('');
                              }
                              
                          } else {
                              $("#invalidfileize1").html("Image size must be within 400X300 - 450X350");
                              $('#jobfile1').val('');
                          }
                      };
       
                  }
               } else {
                  $('#jobfile1').val('');
               }

            } else {
               $("#invalidfileize1").html("Please select a valid Image file.");
               $('#jobfile1').val('');
            }
         //});
   }

   function functionimg2() {
         //$('#jobfile2').change(function () {
            var fileUpload = document.getElementById("jobfile2");
            
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {

               if (typeof (fileUpload.files) != "undefined") {

                  //Initiate the FileReader object.
                  var reader = new FileReader();
                  //Read the contents of Image File.
                  reader.readAsDataURL(fileUpload.files[0]);
                  reader.onload = function (e) {
                      //Initiate the JavaScript Image object.
                      var image = new Image();
       
                      //Set the Base64 string return from FileReader as source.
                      image.src = e.target.result;
                             
                      //Validate the File Height and Width.
                      image.onload = function () {
                          var height = parseInt(this.height);
                          var width = parseInt(this.width);

                          if (width>=400 && height>=300) {

                              if(width<= 450 && height<= 350) {
                                 $("#invalidfileize2").html("");

                              } else {
                                 $("#invalidfileize2").html("Image size must be within 400X300 - 450X350");
                                 $('#jobfile2').val('');
                              }
                              
                          } else {
                              $("#invalidfileize2").html("Image size must be within 400X300 - 450X350");
                              $('#jobfile2').val('');
                          }
                      };
       
                  }
               } else {
                  $('#jobfile2').val('');
               }

            } else {
               $("#invalidfileize2").html("Please select a valid Image file.");
               $('#jobfile2').val('');
            }
         //});
   }

   $(document).ready(function() {
         $('.screeningurlRadio').on('click', function() {
               var selectedValue = $(this).val();
               if(selectedValue == "Instant screening") {
                  $('#screeningurl').css('display','flex');
               } else {
                  $('#screeningurl').css('display','none');
               }
         });
   });

</script>
   
   </body>
</html>

