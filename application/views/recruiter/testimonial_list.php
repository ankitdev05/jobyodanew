 
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/croppie.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/individual/css-addrecruiter.css" />

      <style type="text/css">
          .TestimonailArea{padding: 50px;}
          .TestimonailArea table{background-color: #fff;}
          .TestimonailArea table tr th{    background-color: #26ae61;
    color: #fff;
    font-size: 15px;
    font-weight: 600;
    padding: 10px 15px;}
          .TestimonailArea table tr td{    padding: 10px;
    font-size: 14px;
    font-family: Roboto;
    line-height: 22px;}
          .TestimonailArea table tr td figure{    width: 50px;
    height: 50px;
    border-radius: 50%;
    overflow: hidden;
    margin: 0 auto 7px;
    border: 2px solid #000;}
          .TestimonailArea table tr td figure img{    width: 100%;}

          .TestimonailArea table tr td a:first-child{ margin: 0 5px;    color: red;
    font-size: 15px; }
          .TestimonailArea table tr td a:last-child{   margin: 0 5px; color: #26ae61;
    font-size: 15px;}

    .TestimonailArea table tr:nth-child(odd){ background-color: #f3f3f3; }

      </style>

   </head>
   
 
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                    <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 recruprogfd MainWrapper">
                        <div class="TestimonailArea">
                            <table>
                                <tr>
                                    <th>S.No</th>
                                    <th>Recruiter Name</th>
                                    <th width="200px">Title</th>
                                    <th width="300px">Company Name</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>

                                <tr>
                                <?php
                                  $x1 =1;
                                  if(isset($recruitFetch)) {
                                    foreach($recruitFetch as $jobList) {
                                ?>
                                    <td><?php echo $x1;?></td>
                                    <td><?php echo $jobList['fname'].' '.$jobList['lname']; ?> </td>
                                    <td><?php echo $jobList['name']; ?></td>
                                    <td><?php echo $jobList['cname']; ?></td>
                                    <td><?php echo $jobList['description']; ?></td>
                                    <td align="center">
                                        <a href="JavaScript:Void(0);" onclick="deletefunction(<?php echo $jobList['id']; ?>)"><i class="fa fa-trash"></i></a>

                                        <a href="<?php echo base_url();?>recruiter/testimonialedit?testimonial=<?php echo $jobList['id']; ?>"><i class="fa fa-pencil"></i></a>
                                    </td> 
                                </tr>
                                <?php
                                    $x1++;
                                    }
                                  }
                                ?>

                            </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>


<div class="ModalBox">
  <div id="DeleteModal" class="modal fade" role="dialog">
      <div class="modal-dialog"> 
        <div class="modal-content"> 
          <div class="modal-body"> 
            <div class="Decline">
              <a href="JavaScript:Void(0);" class="CloseModal" data-dismiss="modal">&times;</a>                       
              <h3>Delete</h3>
              <p>Are you sure you want to delete this Testimonaial ?</p>
              <h4>
                <form method="post" action="<?php echo base_url();?>recruiter/testimonial/delete">
                  <input type="hidden" name="tid" id="ttid" value="">
                  <a href="JavaScript:Void(0);" data-dismiss="modal">no</a>
                  <button type="submit">Yes</button>
                </form>
              </h4>
            </div>
          </div> 
        </div>
      </div>
  </div>
</div>
    
      </section>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/croppie.js"></script>

      <script type="text/javascript">
          function deletefunction($id) {
              $("#ttid").val($id);
              $("#DeleteModal").modal('show');
          }
      </script>
   </body>
 


