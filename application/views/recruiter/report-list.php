<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDa</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->

      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="https://mobuloustech.com/jobyoda/recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
       <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
       <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.jqueryui.min.css">
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" /> -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <!-- <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script> -->
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->
      <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" /> -->
   </head>
   <style>
   
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
      .tab-content {margin-top:40px;}
      .tab-content .tab-pane{min-height: 500px;
    border: 1px solid #e7e7e7;}
	
	table.dataTable th.secondary-text {
    padding: 0 11px;
    font-size: 12px;
    width: auto !important;
    vertical-align: middle !important;
    line-height: 13px;
}

.tab-content .tab-pane{
	    min-height: 589px;
}
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
      <header class="stick-top nohdascr">
         <div class="menu-sec">
            <div class="container-fluid dasboardareas">
               
               <!-- Logo -->
               <?php include_once('headermenu.php');?>
            </div>
         </div>
      </header>
      <section>
         <div class="block no-padding">
            <div class="container-fluid dasboardareas">
               <div class="row">
                  <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                     <div class="widget">
                        <div class="menuinside">
                           <?php include_once('sidebar.php'); ?>
                        </div>
                     </div>
                  </aside>
                  <div class="col-md-9 col-lg-10 MainWrapper">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="companyprofileforms fullwidthform">
                                 <h6>Reports</h6>
                           </div> 
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12 repocts transactdto">
                            <ul class="nav nav-tabs" style="width: 100%;">
                              <li class="active"><a data-toggle="tab" href="#home" class="active show">Daily</a></li>
                              <li><a data-toggle="tab" href="#menu1">Weekly</a></li>
                              <li><a data-toggle="tab" href="#menu2">Monthly</a></li>
                              <li><a data-toggle="tab" href="#menu3">Yearly</a></li>
                              <li><a data-toggle="tab" href="#menu4">DateWise</a></li>
                            </ul>

                            <div class="tab-content reportsettctn">
                              <div id="home" class="tab-pane fade in active show">
                              
                                    <div class="row fitspfd">
                                                        <div class="col-md-6 spdsd">
                                                            <h3>Daily Report</h3>
                                                        </div>
                                                        <!-- <div class="col-md-6 spdsd">
                                          <form method="" action="" style="width:100%">
                                                
                                                      <select class="form-control" name="status_dropdown" id="status_dropdown">
                                                           
                                                            <option value="">Select Status</option>
                                                                <option value="1" selected="">New Application</option>
                                                                <option value="2">No Show</option>
                                                                <option value="3">Fall Out</option>
                                                                <option value="4">Refer</option>
                                                                <option value="5">On Going Application</option>
                                                                <option value="6">Accepted JO</option>
                                                                <option value="7">Hired</option>
                                                      </select>
                                                      
                                                
                                          </form>
                                                              </div> -->
                                    </div>
                                    <div style="clear:both;"></div>
                                    
                                          <div class="col-md-12">
                                                                   <div class="table-responsive">
                                                <table id="sample-data-table" class="table table-striped table-bordered" style="width:2400px">
                                                 
                                              </table>
                                              </div>
                                                                  </div>
                              </div>
                              <div id="menu1" class="tab-pane fade">
                                
                                     <div class="row fitspfd">
                                                       <div class="col-md-6 spdsd">
                                                       <h3>Weekly Report</h3>
                                                       </div>
                                                       
                                                       <!-- <div class="col-md-6 spdsd">
                                          <form method="" action="" style="width:100%">
                                                <div class="col-md-12">
                                                      <select class="form-control" name="status_dropdown" id="status_dropdown_weekly">
                                                                <option value="">Select Status</option>
                                                                <option value="1" selected="">New Application</option>
                                                                <option value="2">No Show</option>
                                                                <option value="3">Fall Out</option>
                                                                <option value="4">Refer</option>
                                                                <option value="5">On Going Application</option>
                                                                <option value="6">Accepted JO</option>
                                                                <option value="7">Hired</option>
                                                      </select>
                                                     
                                                </div>
                                          </form>
                                                      </div> -->
                                    </div>
                                    <div style="clear:both;"></div>
                                   
                                          <div class="col-md-12">
                                                               <div class="table-responsive">
                                                <table id="sample-data-tableweekly" class="table table-striped table-bordered" style="width:3000px">
                                               
                                              </table>
                                                                    </div>
                                          </div>
                                   
                              </div>
                              <div id="menu2" class="tab-pane fade">
                               
                                     <div class="row fitspfd">
                                                        <div class="col-md-6 spdsd">
                                                        <h3>Monthly Report</h3>
                                                        </div>
                                                        
                                                         <!-- <div class="col-md-6 spdsd">
                                          <form method="" action="" style="width:100%">
                                                <div class="col-md-12">
                                                      <select class="form-control" name="status_dropdown" id="status_dropdown_monthly">
                                                                <option value="">Select Status</option>
                                                                <option value="1" selected="">New Application</option>
                                                                <option value="2">No Show</option>
                                                                <option value="3">Fall Out</option>
                                                                <option value="4">Refer</option>
                                                                <option value="5">On Going Application</option>
                                                                <option value="6">Accepted JO</option>
                                                                <option value="7">Hired</option>
                                                      </select>
                                                      
                                                </div>
                                          </form>
                                                            </div> -->
                                    </div>
                                    <div style="clear:both;"></div>
                                          <div class="col-md-12">
                                                              <div class="table-responsive">
                                                <table id="sample-data-tablemonthly" class="table table-striped table-bordered" style="width:3000px">
                                                
                                              </table>
                                                                    </div>
                                          </div>
                              </div>
                              <div id="menu3" class="tab-pane fade">
                                  
                                    <div class="row fitspfd">
                                                       <div class="col-md-6 spdsd">
                                                      <h3>Yearly Report</h3>
                                                      </div>
                                                      <!-- <div class="col-md-6 spdsd">
                                          <form method="" action="" style="width:100%">
                                                <div class="col-md-12">
                                                      <select class="form-control" name="status_dropdown" id="status_dropdown_yearly">
                                                             <option value="">Select Status</option>
                                                                <option value="1" selected="">New Application</option>
                                                                <option value="2">No Show</option>
                                                                <option value="3">Fall Out</option>
                                                                <option value="4">Refer</option>
                                                                <option value="5">On Going Application</option>
                                                                <option value="6">Accepted JO</option>
                                                                <option value="7">Hired</option>
                                                      </select>
                                                      
                                                </div>
                                          </form>
                                                            </div> -->
                                    </div>
                                    <div style="clear:both;"></div>
                                    
                                          <div class="col-md-12">
                                                              <div class="table-responsive">
                                                <table id="sample-data-tableyearly" class="table table-striped table-bordered" style="width:3000px;">
                                                  
                                              </table>
                                          </div>
                                                              </div>
                                    
                              </div>
                              <div id="menu4" class="tab-pane fade">
                                  
                                    <div class="row fitspfd">
                                     <div class="col-md-3 spdsd">
                                          <h3>Datewise Report</h3>
                                    </div>
                                    <div class="col-md-3">  
                                     <input type="date" name="from_date" id="from_date" class="form-control" placeholder="From Date" />  
                                    </div>  
                                    <div class="col-md-3">  
                                       <input type="date" name="to_date" id="to_date" class="form-control" placeholder="To Date" />  
                                    </div>  
                                    
                                    <div class="col-md-3">  
                                       <input type="button" name="filter" id="filter" value="Filter" class="btn btn-success" />  
                                    </div>  
                                                
                                    </div>
                                    <div style="clear:both;"></div>
                                    
                                          <div class="col-md-12">
                                          <div class="table-responsive">
                                                <table id="sample-data-tabledate" class="table table-striped table-bordered" style="width:3200px">
                                                  
                                                </table>
                                          </div>
                                          </div>
                                    
                              </div>
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include_once('footer.php');?>
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                     <div class="filldetails">
                        <form>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="Change Password">
                              <!-- <img src="<?php echo base_url(); ?>images/keypass.png"> -->
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="New Password">
                              <!-- <img src="images/keypass.png"> -->
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="Confirm New Password">
                              <!-- <img src="images/keypass.png"> -->
                           </div>
                           <button type="submit" class="srchbtns">Change</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      

      <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
      
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>

      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script> -->
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.jqueryui.min.js"></script>
      <!-- <script src="<?php //echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script> -->
      <!-- <script src="<?php //echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script> -->
      
      
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
     
      <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
      
      <!-- <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>     -->  
   </body>
   <script>
    
      
      // $( window ).scroll(function() {   
       // if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
        $(".progress-bar").each(function(){
          each_bar_width = $(this).attr('aria-valuenow');
          $(this).width(each_bar_width + '%');
        });
        $(document).ready(function() {
                /*$('#sample-data-table').DataTable();
                $('#sample-data-tableweekly').DataTable();
                $('#sample-data-tablemonthly').DataTable();
                $('#sample-data-tableyearly').DataTable();*/
            } );
   </script>


<script type="text/javascript">
  /*$('#status_dropdown').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>recruiter/report/getDatabyStatus";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-table").html(data);
      $('#sample-data-table').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'excel', 'csv'
               ],
               destroy : true,
            });
        }
      });
  })*/


  $(document).ready(function(){
            //var status = $('#status_dropdown').val();
            var url = "<?php echo base_url(); ?>recruiter/report/getDatabyStatus";
            $.ajax({
             type:"POST",
             url:url,
             /*data:{
                 status : status
             },*/

             success:function(data)

              {
               //alert(data);
               
            $("#sample-data-table").html(data);
            $('#sample-data-table').DataTable({
                  dom: 'lBfrtip',
                  buttons: [
                      //'excel', 'csv'
                     ],
                     destroy : true,
                  });

              }
            });
       });

  /*$('#status_dropdown_weekly').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>recruiter/report/getDatabyStatusweek";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-tableweekly").html(data);
      $('#sample-data-tableweekly').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'excel', 'csv'
               ],
               destroy : true,
            });
        }
      });
  })*/

/*  
  $('#status_dropdown_monthly').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>recruiter/report/getDatabyStatusmonth";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         
         
      $("#sample-data-tablemonthly").html(data);
      $('#sample-data-tablemonthly').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'excel', 'csv'
               ],
            destroy : true,
            });
        }
      });
  })*/

  
 /* $('#status_dropdown_yearly').change(function(){
    var status = $(this).val();
    var url = "<?php echo base_url(); ?>recruiter/report/getDatabyStatusyear";
      $.ajax({
       type:"POST",
       url:url,
       data:{
           status : status
       },

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-tableyearly").html(data);
      $('#sample-data-tableyearly').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'excel', 'csv'
               ],
               destroy : true,
            });

        }
      });
  })*/
    
</script>

<script>  
      $(document).ready(function() {
           $('#filter').click(function() {
            //alert('dfs');
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                //var status = $('#status_dropdown_day').val();
                if(from_date == '' )  
                {  
                  alert("Please Select From Date"); 
                     
                } else if(to_date == '')  
                {  
                  alert("Please Select To Date"); 
                     
                } /*else if(status =='')  
                {  
                  alert("Please Select Status"); 
                     
                }  */
                else  
                {  
                    $.ajax({  
                          url:"<?php echo base_url(); ?>recruiter/report/getDatabyStatusDate",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                              $("#sample-data-tabledate").html(data);
                              $('#sample-data-tabledate').DataTable({
                              dom: 'lBfrtip',
                              buttons: [
                                  //'excel', 'csv'
                                 ],
                                 destroy : true,
                              }); 
                          }  
                     });    
                }  
           });  
      });  
 </script>

 <script type="text/javascript">
       $(document).ready(function(){
            //var status = $('#status_dropdown_yearly').val();
            var url = "<?php echo base_url(); ?>recruiter/report/getDatabyStatusyear";
            $.ajax({
             type:"POST",
             url:url,
             /*data:{
                 status : status
             },*/

             success:function(data)

              {
               //alert(data);
               
            $("#sample-data-tableyearly").html(data);
            $('#sample-data-tableyearly').DataTable({
                  dom: 'lBfrtip',
                  buttons: [
                      //'excel', 'csv'
                     ],
                     destroy : true,
                  });

              }
            });
       });


$(document).ready(function(){
    //var status = $('#status_dropdown_weekly').val();
    var url = "<?php echo base_url(); ?>recruiter/report/getDatabyStatusweek";
      $.ajax({
       type:"POST",
       url:url,
       /*data:{
           status : status
       },*/

       success:function(data)
      {
            alert(data);
         
            // $("#sample-data-tableweekly").html(data);
            // $('#sample-data-tableweekly').DataTable({
            // dom: 'lBfrtip',
            // buttons: [
            //     //'excel', 'csv'
            //    ],
            //    destroy : true,
            // });
        }
      });
  })

$(document).ready(function(){
    //var status = $('#status_dropdown_monthly').val();
    var url = "<?php echo base_url(); ?>recruiter/report/getDatabyStatusmonth";
      $.ajax({
       type:"POST",
       url:url,
       /*data:{
           status : status
       },*/

       success:function(data)

        {
         //alert(data);
         
      $("#sample-data-tablemonthly").html(data);
      $('#sample-data-tablemonthly').DataTable({
            dom: 'lBfrtip',
            buttons: [
                //'excel', 'csv'
               ],
            destroy : true,
            });
        }
      });
  })

$(document).ready(function(){   
//var status = $('#status_dropdown_day').val();
  $.ajax({  
  url:"<?php echo base_url(); ?>recruiter/report/getDatabyStatusmonth",  
  method:"POST",  
  //data:{status:status},  
  success:function(data)  
  {  
      $("#sample-data-tabledate").html(data);
      $('#sample-data-tabledate').DataTable({
      dom: 'lBfrtip',
      buttons: [
          //'excel', 'csv'
         ],
         destroy : true,
      }); 
  }  
   });    
     
});

$(document).ready(function(){
      $.datepicker.setDefaults({  
            dateFormat: 'yy-mm-dd'   
      });  
     $(function(){  
          $("#from_date").datepicker({ maxDate: "0" });  
          $("#to_date").datepicker({  maxDate: "0" }); 
          $('#to_date').datepicker('setDate', 'today');
          $('#from_date').datepicker('setDate', 'today-30'); 
     });
});
</script>