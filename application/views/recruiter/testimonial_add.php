 
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDa</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/croppie.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/individual/css-addrecruiter.css" />

      <style type="text/css">
           .Testimonial_Add{
    padding: 50px;
}
          .Testimonial_Add form{
    background-color: #fff;
    padding: 25px;
}
          .Testimonial_Add form aside{
    border-bottom: 2px solid #47b476;
    padding: 0 0 15px 0;
    margin: 0 0 30px;
}
          .Testimonial_Add form aside img{
    float: right;
    width: 30px;
    margin: -6px 0 0 0;
}
          .Testimonial_Add form aside h4{
    font-size: 17px;
    margin: 6px 0;
    font-weight: 600;
    color: #000;
}
          .Testimonial_Add form .form-group{
    width: 60%;
    margin: 0 auto 40px;
}
          .Testimonial_Add form .form-group label{
    padding: 0;
    display: block;
    margin: 0 0 10px 0;
    font-weight: 600;
    color: #000;
    font-size: 14px;
}
          .Testimonial_Add form .form-group label:before{
    content: none;
}
          .Testimonial_Add form .form-group .form-control{
    float: none;
    border: 1px solid #ddd;
    padding: 10px;
    font-size: 14px;
}

.Testimonial_Add form .form-group button{    background-color: #FBAF33;
    min-width: 150px;
    letter-spacing: 0;
    max-width: 150px;
    width: 150px !important;
    margin: auto;
    display: block;
    font-size: 18px !important;
    padding: 10px 0 !important;float: none;}
.Testimonial_Add form .form-group button:hover{}


      </style>

   </head>
   
 
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                    <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 recruprogfd MainWrapper">
                        <div class="Testimonial_Add">
                            <form method="post" action="<?php echo base_url();?>recruiter/testimonial/create">
                                <aside>
                                    <img src="http://3.134.95.218/recruiterfiles/images/fav.png">
                                    <h4>Testimonial Add</h4>
                                </aside>
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter Your Title">

                                    <?php if(!empty($errors['name'])){ echo "<span class='err'>".$errors['name']."</span>";}?>
                                </div>
                                <div class="form-group">
                                    <label>Company name</label>
                                    <input type="text" name="cname" class="form-control" placeholder="Enter Your Company name">
                                    <?php if(!empty($errors['cname'])){ echo "<span class='err'>".$errors['cname']."</span>";}?>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea rows="8" class="form-control" name="description" placeholder="Enter Description"></textarea>

                                    <?php if(!empty($errors['description'])){ echo "<span class='err'>".$errors['description']."</span>";}?>
                                    
                                </div> 
                                <div class="form-group">
                                    <button type="submit">Submit</button>
                                </div>     
                            </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>


 
    
      </section>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/croppie.js"></script>
   </body>
 




