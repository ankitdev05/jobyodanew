      

<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoda</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery-ui.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery-ui.theme.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
      <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> 
   </head>
   <style>
   #ui-datepicker-div{
      display: none;
   }
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }

      .mailerstelco p.filtsts {
    width: 41%;
    float: right;
}
.shoropnt1 ul li{float:left;}
      .shoropnt1 ul li label{font-size: 13px;}
      .jobidsclassshide span{float: left;}
      .jobidsclassshide .jobsid {
      border: 1px solid #c0c0c0!important;
      padding: 5px!important;
      width: auto!important;
      float: left!important;
      }
     
      .statuspanel button{margin-right: 25px;}
      .modal-backdrop{    position: relative!important;    z-index: 0!important;}
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 MainWrapper">
                        <div class="row">
                           <div class="col-md-12 nopadds">
                              <div class="backgrounposionar">
                                 <div class="designation-notified">
                                    <div class="positiondispl">
                                       <p class=""><?php if(isset($jobdetail[0]['jobtitle'])){ echo $jobdetail[0]['jobtitle']; }?></p>
                                       <span> </span>
                                       <a href="<?php if(isset($jobdetail[0]['id'])){ echo base_url();?>recruiter/jobpost/jobpostView?type=<?php echo base64_encode($jobdetail[0]['id']); }?>&key=change" class="edipro">Edit <i class="fas fa-pen"></i></a>
                                       <div class="locateuser">
                                          <p><i class="fas fa-map-marker-alt"></i> <?php if(isset($jobdetail[0]['address'])){ echo $jobdetail[0]['address']; }?></p>
                                       </div>
                                    </div>
                                    <div class="positionvacant">
                                       <div class="rounddtls">
                                          <div class="icoentry">
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/usrlog.png">
                                          </div>
                                          <div class="icoentxtbase">
                                             <a href="<?php echo base_url();?>recruiter/candidate/candidate_lists?type=<?php echo $_GET['type'];?>&status=1">
                                                <p class="designdata">Hired</p>
                                                <span> (<?php echo $hiredlist[0]['hiredCount']; ?>)</span>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="rounddtls">
                                          <div class="icoentry">
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/refer.png">
                                          </div>
                                          <div class="icoentxtbase">
                                            <a href="<?php echo base_url();?>recruiter/candidate/candidate_lists?type=<?php echo $_GET['type'];?>&status=2">
                                                <p class="designdata">Refer</p>
                                                <span> (<?php echo $referedlist[0]['rejectCount'];?>)</span>
                                            </a>
                                          </div>
                                       </div>
                                       <div class="rounddtls">
                                          <div class="icoentry">
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/fallout.png">
                                          </div>
                                          <div class="icoentxtbase">
                                            <a href="<?php echo base_url();?>recruiter/candidate/candidate_lists?type=<?php echo $_GET['type'];?>&status=3">
                                              <p class="designdata">Fall Out</p>
                                              <span> (<?php echo $rejectedlist[0]['rejectCount'];?>)</span>
                                            </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12 nopadds">
                              <div class="row nopaddrow">
                                 <div class="col-md-12 col-lg-4">
                                    <div class="appstats">
                                       <div class="applicationcount">
                                          <div class="appicodt">
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/useblack.png">
                                          </div>
                                          <div class="appfeedct">
                                             <h6>New Applications</h6>
                                             <span> (<?php if($newApp){ echo count($newApp); }?>)</span>
                                          </div>
                                       </div>
                                       <?php
                                          if($newApp) {
                                                $xx= 1;
                                              //print_r($newApp);die;
                                              foreach($newApp as $newAppValue) {
                                          ?>   
                                       <div class="userprpanel">
                                          <div class="profilepic">
                                             <?php
                                                if(empty($newAppValue['profilePic'])) {
                                                ?>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/man.png">
                                             <?php
                                                } else {
                                                ?>
                                             <img src="<?php echo $newAppValue['profilePic'];?>">
                                             <?php 
                                                }
                                                ?>
                                             <div class="rateprof">
                                                <i class="fas fa-star"></i>
                                                <p> <?php echo $newAppValue['rating'];?> </p>
                                             </div>
                                          </div>
                                          <div class="profiledtat">
                                             <p class="usrnm"><a href="<?php echo base_url();?>recruiter/candidate/singleCandidates?id=<?php echo base64_encode($newAppValue['user_id']); ?>"> <?php echo $newAppValue['name'];?> </a></p>
                                             <p class="dsn"><?php if(isset($newAppValue['designation'])){ echo $newAppValue['designation']; }?></p>
                                             <p class="locn"><?php if(isset($newAppValue['location'])){ echo substr($newAppValue['location'],0,20).'....'; }?></p>
                                          </div>
                                          <div class="mytext">
                                             <p><?php echo date('F d,Y', strtotime($newAppValue['interviewdate'])); ?></br><?php echo date('h:i a', strtotime($newAppValue['interviewtime'])) ?> </p>
                                          </div>
                                          <div class="mailerstelco">
                                          <p class="filtsts" data-toggle="modal" data-target="#myModal<?php echo $xx.$xx;?>"><i class="fas fa-edit"></i> Change Status</p>
                                             <p>
                                              <img src="<?php echo base_url().'recruiterfiles/';?>images/emaillv.png"><span><?php echo substr($newAppValue['email'],0,20).'....';?></span>
                                              <BR>
                                              <img src="<?php echo base_url().'recruiterfiles/';?>images/telico.png"><span class="pincts"><a href="tel:<?php echo $newAppValue['country_code'].' '. $newAppValue['phone']; ?>"><?php echo $newAppValue['country_code'].' '.$newAppValue['phone']; ?></a>                                             </p>
                                             <p class="rgtflo"></span></p>
                                          </div>
                                          <!-- The Modal -->
                                         <div class="modal" id="myModal<?php echo $xx.$xx;?>">
                                            <div class="modal-dialog modal-dialog-centered bd-example-modal-lg statuspopchang" style="    margin-top: 12%;">
                                               <div class="modal-content">
                                                  <!-- Modal Header -->
                                                  <div class="modal-header">
                                                     <h4 class="modal-title">Change Status</h4>
                                                     
                                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  </div>
                                                  <!-- Modal body -->
                                                  <div class="modal-body">
                                                     <div class="filterstatsd">
                                                        <div class="">
                                                           <form method="post" action="<?php echo base_url();?>recruiter/candidate/jobstatuschange">
                                                              <div class="shoropnt1">
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                       <p class="remember-label"></p>
                                                                       <input type="hidden" name="recruiter_id" value="<?php if(!empty($newAppValue['compid'])){ echo $newAppValue['compid'];} ?>">    
                                                                       <input type="hidden" name="jobtitle" value="<?php if(!empty($newAppValue['jobtitle'])){ echo $newAppValue['jobtitle']; }  ?>">
                                                                    </div>
                                                                 </div>
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                       <ul>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value, '<?php echo $xx.$xx.$xx;?>')" class="<?php echo $xx.$xx;?>" name="statusradio" value="1" id="ab<?php echo $xx;?>" <?php if($newAppValue['status'] == 1){ echo "checked"; } ?>> <label for="ab<?php echo $xx;?>">New Application</label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" class="<?php echo $xx.$xx;?>" name="statusradio" value="2" id="cd<?php echo $xx;?>" <?php if($newAppValue['status'] == 2){ echo "checked"; } ?>><label for="cd<?php echo $xx;?>">No Show</label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" value="3" class="<?php echo $xx.$xx;?>" id="ef<?php echo $xx;?>" <?php if($newAppValue['status'] == 3){ echo "checked"; } ?>><label for="ef<?php echo $xx;?>"> Fall Out </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" value="4" class="<?php echo $xx.$xx;?>" id="gh<?php echo $xx;?>" <?php if($newAppValue['status'] == 4){ echo "checked"; } ?>><label for="gh<?php echo $xx;?>"> Refer </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" value="5" class="<?php echo $xx.$xx;?>" id="ij<?php echo $xx;?>" <?php if($newAppValue['status'] == 5){ echo "checked"; } ?>><label for="ij<?php echo $xx;?>"> On Going Application </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" class="<?php echo $xx.$xx;?>" value="6" id="kl<?php echo $xx;?>" <?php if($newAppValue['status'] == 6){ echo "checked"; } ?>><label for="kl<?php echo $xx;?>"> Accepted JO </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $xx.$xx.$xx;?>')" name="statusradio" class="<?php echo $xx.$xx;?>" value="7" id="mn<?php echo $xx;?>" <?php if($newAppValue['status'] == 7){ echo "checked"; } ?>><label for="mn<?php echo $xx;?>"> Hired </label>
                                                                          </li>
                                                                       </ul>
                                                                    </div>
                                                                 </div>
                                                                 <div style="clear:both;"></div>
                                                                 <div class="row fallouthide1 falloutshow<?php echo $xx.$xx.$xx;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span class="reasonout"> Fall Out Reason : </span>
                                                                       <select name="falloutreason" class="reasonout1" id="fall<?php echo $xx.$xx;?>">
                                                                          <option value=""> Select Reason </option>
                                                                          <!-- <option value="1"> No show </option> -->
                                                                          <option value="2"> Did not meet requirement </option>
                                                                          <option value="3"> Did not Accept Job Offers </option>
                                                                          <!-- <option value="4"> Refer to another job listing </option> -->
                                                                          <option value="5"> Day 1 No Show </option>
                                                                       </select>
                                                                    </div>
                                                                    <span id="fallerr<?php echo $xx.$xx;?>" style="color:red;"></span>
                                                                 </div>
                                                                 <div class="row jobidsclassshide jobidsclasss<?php echo $xx.$xx.$xx;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                    <div class="valdydas">
                                                                       <span> Enter Job ID</span>
                                                                       <input type="text" name="jobid" id="job<?php echo $xx.$xx;?>" class="jobsid" >
                                                                        <span  class="errydg" id="validerr<?php echo $xx.$xx;?>" style="color:red;"></span>
                                                                  </div>
                                                                    </div>
                                                                   
                                                                 </div>
                                                                 
                                                                 <!-- <div class="row ongoinghide ongoinghideshow<?php echo $xx.$xx.$xx;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span class="reasonout"> On Going Status : </span>
                                                                       <select name="goingreason" class="reasonout1" id="ongo<?php echo $xx.$xx;?>">
                                                                          <option value=""> Select Reason </option>
                                                                          <option value="1"> Passes initial interview </option>
                                                                          <option value="2"> Did not meet requirement </option>
                                                                       </select>
                                                                       
                                                                    </div>
                                                                    <span id="goerr<?php echo $xx.$xx;?>" style="color:red;"></span>
                                                                 </div> -->
                                                                 <div class="row acceptedhide acceptedeshow<?php echo $xx.$xx.$xx;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span id="ajoerr<?php echo $xx.$xx;?>" style="color:red;"></span>
                                                                    <input type="text" class="schedule_date" autocomplete="off" name="schedule_date" id="schedule_date<?php echo $xx.$xx;?>" placeholder="Enter Schedule of Day1" style="    border: 1px solid #000; padding: 11px 0px; width: 46%; margin-top: 15px;">
                                                                    </div>
                                                                    
                                                                 </div>
                                                                 <div style="clear:both;"></div>
                                                                 <div class="row">
                                                                    <div class="col-md-12 statuspanel">
                                                                       <p class="select_candidate"><i class="la la-check">
                                                                          </i>
                                                                          <?php if($newAppValue['status'] == 1){ echo "New Application"; } ?>
                                                                          <?php if($newAppValue['status'] == 2){ echo "No Show"; } ?>
                                                                          <?php if($newAppValue['status'] == 3){ echo "Fall Out"; } ?>
                                                                          <?php if($newAppValue['status'] == 4){ echo "Refer"; } ?>
                                                                          <?php if($newAppValue['status'] == 5){ echo "On Going Application"; } ?>
                                                                          <?php if($newAppValue['status'] == 6){ echo "Accepted JO"; } ?>
                                                                          <?php if($newAppValue['status'] == 7){ echo "Hired"; } ?>
                                                                       </p>
                                                                       <input type="hidden" name="type" value="<?php echo $_GET['type']; ?>">
                                                                       <input type="hidden" name="key_type" value="view_key">
                                                                       <input type="hidden" name="interview_date" value="<?php echo $newAppValue['interviewdate']; ?>">
                                                                       <input type="hidden" name="interview_time" value="<?php echo $newAppValue['interviewtime']; ?>">
                                                                       <input type="hidden" name="appid" value="<?php echo $newAppValue['id']; ?>">
                                                                       <input type="hidden" name="jid" value="<?php if(!empty($newAppValue['jobpost_id'])){ echo $newAppValue['jobpost_id']; }?>">
                                                                       <input type="hidden" name="uid" value="<?php echo $newAppValue['user_id']; ?>">
                                                                       <button id="submitchangebtn<?php echo $xx.$xx;?>" type="button" class="statusbutton" data="<?php echo $xx.$xx;?>">Change Status </button>
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                           </form>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>
                                       </div>
                                       <script type="text/javascript">
         $(function () {
            $(".schedule_date").datepicker({ 
              minDate: +1
           })
         });
          
    </script>
                                       <script type="text/javascript">

                                                $('.statusbutton').click(function(){
                                                      var single_var = $(this).attr('data');
                                                      if($("."+single_var+":checked").val() == '3'){
                                                            if($("#fall"+single_var).val() == ''){
                                                               $("#fallerr"+single_var).text("Select at least one reason");   
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      }else if($("."+single_var+":checked").val() == '4'){
                                                            if($("#job"+single_var).val() == ''){
                                                               $("#validerr"+single_var).text("Job Id is required");   
                                                            } else {
                                                                  var job_id = $("#job"+single_var).val();
                                                                  var url = "<?php echo base_url() ?>recruiter/Candidate/checkjobid";
                                                                   $.ajax({
                                                                         type:"POST",
                                                                         url:url,
                                                                         data:{
                                                                             job_id : job_id
                                                                         },

                                                                         success:function(data1)

                                                                          {
                                                                              //alert(data1.length);
                                                                              if(data1.length==6){
                                                                                    //alert(data) ;
                                                                                    $("#submitchangebtn"+single_var).attr('type','submit');
                                                                              }else{
                                                                                   $("#validerr"+single_var).text("Please Enter Correct Job Id"); 
                                                                              }
                                                                          // alert(data);
                                                                           
                                                                        //$("#submitchangebtn"+single_var).attr('type','submit');
                                                                          }
                                                                   });
                                                             
                                                            }
                                                      } /*else if($("."+single_var+":checked").val() == '5'){
                                                            if($("#ongo"+single_var).val() == ''){
                                                               $("#goerr"+single_var).text("Select at least one reason");   
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      }*/else if($("."+single_var+":checked").val() == '6'){
                                                             if($("#schedule_date"+single_var).val() == ''){
                                                                  $("#ajoerr"+single_var).text("Please enter date"); 
                                                            }else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      } else {
                                                            $("#submitchangebtn"+single_var).attr('type','submit');
                                                      }

                                                      // var jobid = $('#job<?php echo $xx.$xx.$xx;?>').val();
                                                      // var radio = $('input[name="statusradio"]:checked').val();
                                                      // //alert(radio);
                                                      // if(radio=='4' && jobid==''){
                                                      //       $('.validerr').css('display','block');
                                                      //       $('.validerr').css('color','red');
                                                      //       return false;
                                                      // }
                                                      //return false;
                                                       
                                                });
                                          </script>
                                       <?php
                                       $xx++;
                                          }
                                          }
                                          ?>
                                    </div>
                                 </div>
                                 <div class="col-md-12 col-lg-4">
                                    <div class="appstats">
                                       <div class="applicationcount">
                                          <div class="appicodt">
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/useblack.png">
                                          </div>
                                          <div class="appfeedct">
                                             <h6>On Going Applications</h6>
                                             <span> (<?php if($goingApp) {echo count($goingApp); }?>)</span>
                                          </div>
                                       </div>
                                       <?php
                                          if($goingApp) {
                                               // print_r($goingApp);die;
                                                $yy=51;
                                              foreach($goingApp as $goingAppValue) {
                                          ?>   
                                       <div class="userprpanel">
                                          <div class="profilepic">
                                             <?php
                                                if(empty($goingAppValue['profilePic'])) {
                                                ?>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/man.png">
                                             <?php
                                                } else {
                                                ?>
                                             <img src="<?php echo $goingAppValue['profilePic'];?>">
                                             <?php 
                                                }
                                                
                                                ?>
                                             <div class="rateprof">
                                                <i class="fas fa-star"></i>
                                                <p><?php echo $goingAppValue['rating'];?></p>
                                             </div>
                                          </div>
                                          <div class="profiledtat">
                                             <p class="usrnm"><a href="<?php echo base_url();?>recruiter/candidate/singleCandidates?id=<?php echo base64_encode($goingAppValue['user_id']); ?>"><?php echo $goingAppValue['name'];?></a></p>
                                             <p class="dsn"><?php if(!empty($goingAppValue['designation'])){ echo $goingAppValue['designation'];} ?></p>
                                             <p class="locn"><?php echo substr($goingAppValue['location'],0,20).'....';?></p>
                                          </div>
                                          <div class="mytext">
                                             <p><a href="<?php if(!empty($goingAppValue['resume'])){ echo $goingAppValue['resume'];}?>" target="_blank"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></p>
                                          </div>
                                          <div class="mailerstelco">
                                         <p class="filtsts" data-toggle="modal" data-target="#myModal<?php echo $yy.$yy;?>"><i class="fas fa-edit"></i> Change Status</p>
                                          
                                             <p><img src="<?php echo base_url().'recruiterfiles/';?>images/emaillv.png"><span><?php echo substr($goingAppValue['email'],0,20).'....';?></span>
                                             <br>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/telico.png"><span class="pincts"><a href="tel:<?php echo $goingAppValue['country_code'].' '. $goingAppValue['phone']; ?>"><?php echo $goingAppValue['country_code'].' '.$goingAppValue['phone']; ?></a></span>
                                             </p>
                                             
                                          </div>
                                          <!-- The Modal -->
                                         <div class="modal" id="myModal<?php echo $yy.$yy;?>">
                                            <div class="modal-dialog modal-dialog-centered bd-example-modal-lg statuspopchang" style="    margin-top: 12%;">
                                               <div class="modal-content">
                                                  <!-- Modal Header -->
                                                  <div class="modal-header">
                                                     <h4 class="modal-title">Change Status</h4>
                                                     
                                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  </div>
                                                  <!-- Modal body -->
                                                  <div class="modal-body">
                                                     <div class="filterstatsd">
                                                        <div class="">
                                                           <form method="post" action="<?php echo base_url();?>recruiter/candidate/jobstatuschange">
                                                              <div class="shoropnt1">
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                       <p class="remember-label"></p>
                                                                       <input type="hidden" name="recruiter_id" value="<?php if(!empty($goingAppValue['compid'])){ echo $goingAppValue['compid'];} ?>">    
                                                                       <input type="hidden" name="jobtitle" value="<?php if(!empty($goingAppValue['jobtitle'])){ echo $goingAppValue['jobtitle']; }  ?>">
                                                                    </div>
                                                                 </div>
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                       <ul>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value, '<?php echo $yy.$yy.$yy;?>')" class="<?php echo $yy.$yy;?>" name="statusradio" value="1" id="ab<?php echo $yy;?>" <?php if($goingAppValue['status'] == 1){ echo "checked"; } ?>> <label for="ab<?php echo $yy;?>">New Application</label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $yy.$yy.$yy;?>')" class="<?php echo $yy.$yy;?>" name="statusradio" value="2" id="cd<?php echo $yy;?>" <?php if($goingAppValue['status'] == 2){ echo "checked"; } ?>><label for="cd<?php echo $yy;?>">No Show</label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $yy.$yy.$yy;?>')" name="statusradio" value="3" class="<?php echo $yy.$yy;?>" id="ef<?php echo $yy;?>" <?php if($goingAppValue['status'] == 3){ echo "checked"; } ?>><label for="ef<?php echo $yy;?>"> Fall Out </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $yy.$yy.$yy;?>')" name="statusradio" value="4" class="<?php echo $yy.$yy;?>" id="gh<?php echo $yy;?>" <?php if($goingAppValue['status'] == 4){ echo "checked"; } ?>><label for="gh<?php echo $yy;?>"> Refer </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $yy.$yy.$yy;?>')" name="statusradio" value="5" class="<?php echo $yy.$yy;?>" id="ij<?php echo $yy;?>" <?php if($goingAppValue['status'] == 5){ echo "checked"; } ?>><label for="ij<?php echo $yy;?>"> On Going Application </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $yy.$yy.$yy;?>')" name="statusradio" class="<?php echo $yy.$yy;?>" value="6" id="kl<?php echo $yy;?>" <?php if($goingAppValue['status'] == 6){ echo "checked"; } ?>><label for="kl<?php echo $yy;?>"> Accepted JO </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $yy.$yy.$yy;?>')" name="statusradio" class="<?php echo $yy.$yy;?>" value="7" id="mn<?php echo $yy;?>" <?php if($goingAppValue['status'] == 7){ echo "checked"; } ?>><label for="mn<?php echo $yy;?>"> Hired </label>
                                                                          </li>
                                                                       </ul>
                                                                    </div>
                                                                 </div>
                                                                 <div style="clear:both;"></div>
                                                                 <div class="row fallouthide1 falloutshow<?php echo $yy.$yy.$yy;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span class="reasonout"> Fall Out Reason : </span>
                                                                       <select name="falloutreason" class="reasonout1" id="fall<?php echo $yy.$yy;?>">
                                                                          <option value=""> Select Reason </option>
                                                                          <!-- <option value="1"> No show </option> -->
                                                                          <option value="2"> Did not meet requirement </option>
                                                                          <option value="3"> Did not Accept Job Offers </option>
                                                                          <!-- <option value="4"> Refer to another job listing </option> -->
                                                                          <option value="5"> Day 1 No Show </option>
                                                                       </select>
                                                                    </div>
                                                                    <span id="fallerr<?php echo $yy.$yy;?>" style="color:red;"></span>
                                                                 </div>
                                                                 <div class="row jobidsclassshide jobidsclasss<?php echo $yy.$yy.$yy;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                    <div class="valdydas">
                                                                       <span> Enter Job ID</span>
                                                                       <input type="text" name="jobid" id="job<?php echo $yy.$yy;?>" class="jobsid" >
                                                                        <span  class="errydg" id="validerr<?php echo $yy.$yy;?>" style="color:red;"></span>
                                                                  </div>
                                                                    </div>
                                                                   
                                                                 </div>
                                                                 
                                                                <!--  <div class="row ongoinghide ongoinghideshow<?php echo $yy.$yy.$yy;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span class="reasonout"> On Going Status : </span>
                                                                       <select name="goingreason" class="reasonout1" id="ongo<?php echo $yy.$yy;?>">
                                                                          <option value=""> Select Reason </option>
                                                                          <option value="1"> Passes initial interview </option>
                                                                          <option value="2"> Did not meet requirement </option>
                                                                       </select>
                                                                       
                                                                    </div>
                                                                    <span id="goerr<?php echo $yy.$yy;?>" style="color:red;"></span>
                                                                 </div> -->
                                                                 <div class="row acceptedhide acceptedeshow<?php echo $yy.$yy.$yy;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span id="ajoerr<?php echo $yy.$yy;?>" style="color:red;"></span>
                                                                       <input type="text" class="schedule_date" autocomplete="off" name="schedule_date" id="schedule_date<?php echo $yy.$yy;?>" placeholder="Enter Schedule of Day1" style="    border: 1px solid #000; padding: 11px 0px; width: 46%; margin-top: 15px;">
                                                                    </div>
                                                                    
                                                                 </div>
                                                                 <div style="clear:both;"></div>
                                                                 <div class="row">
                                                                    <div class="col-md-12 statuspanel">
                                                                       <p class="select_candidate"><i class="la la-check">
                                                                          </i>
                                                                          <?php if($goingAppValue['status'] == 1){ echo "New Application"; } ?>
                                                                          <?php if($goingAppValue['status'] == 2){ echo "No Show"; } ?>
                                                                          <?php if($goingAppValue['status'] == 3){ echo "Fall Out"; } ?>
                                                                          <?php if($goingAppValue['status'] == 4){ echo "Refer"; } ?>
                                                                          <?php if($goingAppValue['status'] == 5){ echo "On Going Application"; } ?>
                                                                          <?php if($goingAppValue['status'] == 6){ echo "Accepted JO"; } ?>
                                                                          <?php if($goingAppValue['status'] == 7){ echo "Hired"; } ?>
                                                                       </p>
                                                                       <input type="hidden" name="key_type" value="view_key">
                                                                       <input type="hidden" name="type" value="<?php echo $_GET['type']; ?>">
                                                                       <input type="hidden" name="appid" value="<?php echo $goingAppValue['id']; ?>">
                                                                       <input type="hidden" name="interview_date" value="<?php echo $goingAppValue['interviewdate']; ?>">
                                                                       <input type="hidden" name="interview_time" value="<?php echo $goingAppValue['interviewtime']; ?>">
                                                                       <input type="hidden" name="jid" value="<?php if(!empty($goingAppValue['jobpost_id'])){ echo $goingAppValue['jobpost_id']; }?>">
                                                                       <input type="hidden" name="uid" value="<?php echo $goingAppValue['user_id']; ?>">
                                                                       <button id="submitchangebtn<?php echo $yy.$yy;?>" type="button" class="statusbutton" data="<?php echo $yy.$yy;?>">Change Status </button>
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                           </form>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>
                                       </div>
                                       <script type="text/javascript">
         $(function () {
            $(".schedule_date").datepicker({ 
              minDate: +1
           })
         });
          
    </script>
                                       <script type="text/javascript">

                                                $('.statusbutton').click(function(){
                                                      var single_var = $(this).attr('data');
                                                      if($("."+single_var+":checked").val() == '3'){
                                                            if($("#fall"+single_var).val() == ''){
                                                               $("#fallerr"+single_var).text("Select at least one reason");   
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      }else if($("."+single_var+":checked").val() == '4'){
                                                            if($("#job"+single_var).val() == ''){
                                                               $("#validerr"+single_var).text("Job Id is required");   
                                                            } else {
                                                                  var job_id = $("#job"+single_var).val();
                                                                  var url = "<?php echo base_url() ?>recruiter/Candidate/checkjobid";
                                                                   $.ajax({
                                                                         type:"POST",
                                                                         url:url,
                                                                         data:{
                                                                             job_id : job_id
                                                                         },

                                                                         success:function(data1)

                                                                          {
                                                                              //alert(data1.length);
                                                                              if(data1.length==6){
                                                                                    //alert(data) ;
                                                                                    $("#submitchangebtn"+single_var).attr('type','submit');
                                                                              }else{
                                                                                   $("#validerr"+single_var).text("Please Enter Correct Job Id"); 
                                                                              }
                                                                          // alert(data);
                                                                           
                                                                        //$("#submitchangebtn"+single_var).attr('type','submit');
                                                                          }
                                                                   });
                                                             
                                                            }
                                                      }/* else if($("."+single_var+":checked").val() == '5'){
                                                            if($("#ongo"+single_var).val() == ''){
                                                               $("#goerr"+single_var).text("Select at least one reason");   
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      }*/else if($("."+single_var+":checked").val() == '6'){
                                                             if($("#schedule_date"+single_var).val() == ''){
                                                                  $("#ajoerr"+single_var).text("Please enter date"); 
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      } else {
                                                            $("#submitchangebtn"+single_var).attr('type','submit');
                                                      }

                                                       
                                                });
                                          </script>
                                       <?php
                                       $yy++;
                                          }
                                          }
                                          ?>
                                    </div>
                                 </div>
                                 <div class="col-md-12 col-lg-4">
                                    <div class="appstats">
                                       <div class="applicationcount">
                                          <div class="appicodt">
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/useblack.png">
                                          </div>
                                          <div class="appfeedct">
                                             <h6>Accepted Job Offer</h6>
                                             <span> (<?php if($acceptedApp) {echo count($acceptedApp);} ?>)</span>
                                          </div>
                                       </div>
                                       <?php
                                          if($acceptedApp) {
                                                $zz=101;
                                              foreach($acceptedApp as $suggestValue) {
                                          ?>   
                                       <div class="userprpanel">
                                          <div class="profilepic">
                                             <?php
                                                if(empty($suggestValue['profilePic'])) {
                                                ?>
                                             <img src="<?php echo base_url().'recruiterfiles/';?>images/man.png">
                                             <?php
                                                } else {
                                                ?>
                                             <img src="<?php echo $suggestValue['profilePic'];?>">
                                             <?php 
                                                }
                                                ?>
                                             <div class="rateprof">
                                                <i class="fas fa-star"></i>
                                                <p><?php echo $suggestValue['rating'];?></p>
                                             </div>
                                          </div>
                                          <div class="profiledtat">
                                             <p class="usrnm"><a href="<?php echo base_url();?>recruiter/candidate/singleCandidates?id=<?php echo base64_encode($suggestValue['user_id']); ?>"><?php echo $suggestValue['name'];?></a></p>
                                             <p class="dsn"><?php if(!empty($suggestValue['designation'])){ echo $suggestValue['designation'];} ?></p>
                                             <p class="locn"><?php echo substr($suggestValue['location'],0,20).'....';?></p>
                                          </div>
                                          <div class="mytext">
                                             <p><?php echo date('F d,Y', strtotime($suggestValue['interviewdate'])); ?> </p>
                                          </div>
                                          <div class="mailerstelco">
                                          <p class="filtsts" data-toggle="modal" data-target="#myModal<?php echo $zz.$zz;?>"><i class="fas fa-edit"></i> Change Status</p>
                                             <p><img src="<?php echo base_url().'recruiterfiles/';?>images/emaillv.png"><span><?php echo substr($suggestValue['email'],0,20).'....';?></span><br>
                                          <img src="<?php echo base_url().'recruiterfiles/';?>images/telico.png"><span class="pincts"><a href="tel:<?php echo $suggestValue['country_code'].' '. $suggestValue['phone']; ?>"><?php echo $suggestValue['country_code'].' '.$suggestValue['phone']; ?></a></span>
                                             </p>
                                             
                                          </div>
                                          <!-- The Modal -->
                                         <div class="modal" id="myModal<?php echo $zz.$zz;?>">
                                            <div class="modal-dialog modal-dialog-centered bd-example-modal-lg statuspopchang" style="    margin-top: 12%;">
                                               <div class="modal-content">
                                                  <!-- Modal Header -->
                                                  <div class="modal-header">
                                                     <h4 class="modal-title">Change Status</h4>
                                                     
                                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  </div>
                                                  <!-- Modal body -->
                                                  <div class="modal-body">
                                                     <div class="filterstatsd">
                                                        <div class="">
                                                           <form method="post" action="<?php echo base_url();?>recruiter/candidate/jobstatuschange">
                                                              <div class="shoropnt1">
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                       <p class="remember-label"></p>
                                                                       <input type="hidden" name="recruiter_id" value="<?php if(!empty($suggestValue['compid'])){ echo $suggestValue['compid'];} ?>">    
                                                                       <input type="hidden" name="jobtitle" value="<?php if(!empty($suggestValue['jobtitle'])){ echo $suggestValue['jobtitle']; }  ?>">
                                                                    </div>
                                                                 </div>
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                       <ul>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value, '<?php echo $zz.$zz.$zz;?>')" class="<?php echo $zz.$zz;?>" name="statusradio" value="1" id="ab<?php echo $zz;?>" <?php if($suggestValue['status'] == 1){ echo "checked"; } ?>> <label for="ab<?php echo $zz;?>">New Application</label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $zz.$zz.$zz;?>')" class="<?php echo $zz.$zz;?>" name="statusradio" value="2" id="cd<?php echo $zz;?>" <?php if($suggestValue['status'] == 2){ echo "checked"; } ?>><label for="cd<?php echo $zz;?>">No Show</label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $zz.$zz.$zz;?>')" name="statusradio" value="3" class="<?php echo $zz.$zz;?>" id="ef<?php echo $zz;?>" <?php if($suggestValue['status'] == 3){ echo "checked"; } ?>><label for="ef<?php echo $zz;?>"> Fall Out </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $zz.$zz.$zz;?>')" name="statusradio" value="4" class="<?php echo $zz.$zz;?>" id="gh<?php echo $zz;?>" <?php if($suggestValue['status'] == 4){ echo "checked"; } ?>><label for="gh<?php echo $zz;?>"> Refer </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $zz.$zz.$zz;?>')" name="statusradio" value="5" class="<?php echo $zz.$zz;?>" id="ij<?php echo $zz;?>" <?php if($suggestValue['status'] == 5){ echo "checked"; } ?>><label for="ij<?php echo $zz;?>"> On Going Application </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $zz.$zz.$zz;?>')" name="statusradio" class="<?php echo $zz.$zz;?>" value="6" id="kl<?php echo $zz;?>" <?php if($suggestValue['status'] == 6){ echo "checked"; } ?>><label for="kl<?php echo $zz;?>"> Accepted JO </label>
                                                                          </li>
                                                                          <li>
                                                                             <input type="Radio" onclick="myfun(this.value,'<?php echo $zz.$zz.$zz;?>')" name="statusradio" class="<?php echo $zz.$zz;?>" value="7" id="mn<?php echo $zz;?>" <?php if($suggestValue['status'] == 7){ echo "checked"; } ?>><label for="mn<?php echo $zz;?>"> Hired </label>
                                                                          </li>
                                                                       </ul>
                                                                    </div>
                                                                 </div>
                                                                 <div style="clear:both;"></div>
                                                                 <div class="row fallouthide1 falloutshow<?php echo $zz.$zz.$zz;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span class="reasonout"> Fall Out Reason : </span>
                                                                       <select name="falloutreason" class="reasonout1" id="fall<?php echo $zz.$zz;?>">
                                                                          <option value=""> Select Reason </option>
                                                                          <!-- <option value="1"> No show </option> -->
                                                                          <option value="2"> Did not meet requirement </option>
                                                                          <option value="3"> Did not Accept Job Offers </option>
                                                                          <!-- <option value="4"> Refer to another job listing </option> -->
                                                                          <option value="5"> Day 1 No Show </option>
                                                                       </select>
                                                                    </div>
                                                                    <span id="fallerr<?php echo $zz.$zz;?>" style="color:red;"></span>
                                                                 </div>
                                                                 <div class="row jobidsclassshide jobidsclasss<?php echo $zz.$zz.$zz;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                    <div class="valdydas">
                                                                       <span> Enter Job ID</span>
                                                                       <input type="text" name="jobid" id="job<?php echo $zz.$zz;?>" class="jobsid" >
                                                                        <span  class="errydg" id="validerr<?php echo $zz.$zz;?>" style="color:red;"></span>
                                                                  </div>
                                                                    </div>
                                                                   
                                                                 </div>
                                                                 
                                                                 <!-- <div class="row ongoinghide ongoinghideshow<?php echo $zz.$zz.$zz;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span class="reasonout"> On Going Status : </span>
                                                                       <select name="goingreason" class="reasonout1" id="ongo<?php echo $zz.$zz;?>">
                                                                          <option value=""> Select Reason </option>
                                                                          <option value="1"> Passes initial interview </option>
                                                                          <option value="2"> Did not meet requirement </option>
                                                                       </select>
                                                                       
                                                                    </div>
                                                                    <span id="goerr<?php echo $zz.$zz;?>" style="color:red;"></span>
                                                                 </div> -->
                                                                 <div class="row acceptedhide acceptedeshow<?php echo $zz.$zz.$zz;?>" style="display:none;">
                                                                    <div class="col-md-12">
                                                                       <span id="ajoerr<?php echo $zz.$zz;?>" style="color:red;"></span>
                                                                        <input type="text" class="schedule_date" autocomplete="off" name="schedule_date" id="schedule_date<?php echo $zz.$zz;?>" placeholder="Enter Schedule of Day1" style="    border: 1px solid #000; padding: 11px 0px; width: 46%; margin-top: 15px;">
                                                                    </div>
                                                                    
                                                                 </div>
                                                                 <div style="clear:both;"></div>
                                                                 <div class="row">
                                                                    <div class="col-md-12 statuspanel">
                                                                       <p class="select_candidate"><i class="la la-check">
                                                                          </i>
                                                                          <?php if($suggestValue['status'] == 1){ echo "New Application"; } ?>
                                                                          <?php if($suggestValue['status'] == 2){ echo "No Show"; } ?>
                                                                          <?php if($suggestValue['status'] == 3){ echo "Fall Out"; } ?>
                                                                          <?php if($suggestValue['status'] == 4){ echo "Refer"; } ?>
                                                                          <?php if($suggestValue['status'] == 5){ echo "On Going Application"; } ?>
                                                                          <?php if($suggestValue['status'] == 6){ echo "Accepted JO"; } ?>
                                                                          <?php if($suggestValue['status'] == 7){ echo "Hired"; } ?>
                                                                       </p>
                                                                       <input type="hidden" name="type" value="<?php echo $_GET['type']; ?>">
                                                                       <input type="hidden" name="key_type" value="view_key">
                                                                       <input type="hidden" name="appid" value="<?php echo $suggestValue['id']; ?>">
                                                                       <input type="hidden" name="jid" value="<?php if(!empty($suggestValue['jobpost_id'])){ echo $suggestValue['jobpost_id']; }?>">
                                                                       <input type="hidden" name="uid" value="<?php echo $suggestValue['user_id']; ?>">
                                                                       <button id="submitchangebtn<?php echo $zz.$zz;?>" type="button" class="statusbutton" data="<?php echo $zz.$zz;?>">Change Status </button>
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                           </form>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                         </div>
                                       </div>
                                       <script type="text/javascript">

                                                $('.statusbutton').click(function(){
                                                      var single_var = $(this).attr('data');
                                                      if($("."+single_var+":checked").val() == '3'){
                                                            if($("#fall"+single_var).val() == ''){
                                                               $("#fallerr"+single_var).text("Select at least one reason");   
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      }else if($("."+single_var+":checked").val() == '4'){
                                                            if($("#job"+single_var).val() == ''){
                                                               $("#validerr"+single_var).text("Job Id is required");   
                                                            } else {
                                                                  var job_id = $("#job"+single_var).val();
                                                                  var url = "<?php echo base_url() ?>recruiter/Candidate/checkjobid";
                                                                   $.ajax({
                                                                         type:"POST",
                                                                         url:url,
                                                                         data:{
                                                                             job_id : job_id
                                                                         },

                                                                         success:function(data1)

                                                                          {
                                                                              //alert(data1.length);
                                                                              if(data1.length==6){
                                                                                    //alert(data) ;
                                                                                    $("#submitchangebtn"+single_var).attr('type','submit');
                                                                              }else{
                                                                                   $("#validerr"+single_var).text("Please Enter Correct Job Id"); 
                                                                              }
                                                                          // alert(data);
                                                                           
                                                                        //$("#submitchangebtn"+single_var).attr('type','submit');
                                                                          }
                                                                   });
                                                             
                                                            }
                                                      } /*else if($("."+single_var+":checked").val() == '5'){
                                                            if($("#ongo"+single_var).val() == ''){
                                                               $("#goerr"+single_var).text("Select at least one reason");   
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      }*/else if($("."+single_var+":checked").val() == '6'){
                                                             if($("#schedule_date"+single_var).val() == ''){
                                                                  $("#ajoerr"+single_var).text("Please enter date"); 
                                                            } else {
                                                             $("#submitchangebtn"+single_var).attr('type','submit');
                                                            }
                                                      } else {
                                                            $("#submitchangebtn"+single_var).attr('type','submit');
                                                      }

                                                       
                                                });
                                          </script>
                                       <?php
                                       $zz++;
                                          }
                                          }
                                          ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      </section>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <div class="modal fade" id="exampleModalCenter30" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Update Salary Grid</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="fullwidthform">
                     <div class="filldetails">
                        <form>
                           <div class="row">
                              <div class="col-md-6">
                                 <label>Job Title</label>
                              </div>
                              <div class="col-md-6">
                                 <p>UI/UX</p>
                              </div>
                              <div class="col-md-6">
                                 <label>Job Staus</label>
                              </div>
                              <div class="col-md-6">
                                 <p>On Going Applications</p>
                              </div>
                              <div class="col-md-6">
                                 <label>Experience</label>
                              </div>
                              <div class="col-md-6">
                                 <select>
                                    <option>0-11 month</option>
                                    <option>0-2 Year</option>
                                    <option>0-5 Year</option>
                                 </select>
                              </div>
                              <div class="col-md-6">
                                 <label>Salary</label>
                              </div>
                              <div class="col-md-6">
                                 <input type="text" class="form-control" placeholder="Edit Salary" value="18000 Rs" style="width:100%;">
                              </div>
                              <button type="button" style="float: none;width: auto;font-size: 13px;margin: 10px auto;">Update</button>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include_once("modalpassword.php");?>
      
   </body>
   <script>
      $(function () { 
        $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
      });  
      
      // $( window ).scroll(function() {   
       // if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
        $(".progress-bar").each(function(){
          each_bar_width = $(this).attr('aria-valuenow');
          $(this).width(each_bar_width + '%');
        });
             
   </script>


<script type="text/javascript">
         function myfun(value, class1) {
               var checkval = value;
               var checkval1 = class1;
               $(".fallouthide1").css("display", "none");
               $(".jobidsclassshide").css("display", "none");
               $(".ongoinghide").css("display", "none");
               $(".acceptedhide").css("display", "none");
         
               if(checkval == 3) {    
                   var createclass = ".falloutshow" + checkval1;
                   $(createclass).css("display", "block");
               }
               if(checkval == 4) {
                   var createclass = ".jobidsclasss" + checkval1;
                   $(createclass).css("display", "block");
               }
               if(checkval == 5) {
                   var createclass = ".ongoinghideshow" + checkval1;
                   $(createclass).css("display", "block");
               }
               if(checkval == 6) {
                   var createclass = ".acceptedeshow" + checkval1;
                   $(createclass).css("display", "block");
               }
         }
      </script>