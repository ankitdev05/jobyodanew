<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url(); ?>recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery-ui.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery-ui.theme.min.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery.timepicker.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
   </head>
   <style>
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .filterchekers ul {
      width: 100%;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
      .validError{
      color:#f00;font-size: 12px;
      margin: 0;
      float: left;
      }
      .insertMsg {
      text-align: center;
      color: #27aa60;
      font-size: 18px;
      }
      .filldetails label{width:100%;}
      .form-control.locselect{padding:0px!important;}
      .filterchekers input[type="checkbox"]{
      position: absolute;
      opacity: 0;
      z-index: auto;
      margin: 0;
      width: 90px;
      height: 55px;
      }
      table#myTable1 {
      width: 100%;
      }
      table#myTable td {
      padding: 0 9px 0 0;
      }
      a.remove i.fa.fa-trash {
      color: #fff;
      }
      input.form-control {
      margin: 20px 0px;
      }
      a.remove {
      padding: 3px 4px;
      text-align: center;
      background: #26ae61;
      border-radius: 100px;
      margin-top: 10px;
      float: left;
      color: #fff;
      border-radius: 0pc;
      font-size: 10px;
      }
      input.form-control.addinput {
      width: 80% !important;
      float: left;
      margin-right: 33px !important;
      }
      }
      @media only screen and (max-width: 767px){
      .psthbs .filterchekers ul {
      display: inline-block;
      width: 100%;
      }
      }  
      .psthbs .filterchekers ul{
      display: inline-block;
      }
      button.multiselect.dropdown-toggle.btn.btn-default {
      overflow: hidden;
      text-overflow: ellipsis;
      text-align: left;
      }
      div#myInputautocomplete-list {
      font-size: 13px;
      }
      div#myInputautocomplete-list div strong{
      font-weight: 400 !important
      }
      label#multiselect-error {
      position: absolute;
      bottom: 15px;
      }




      .Jobassign{ 
    align-items: center;
}      


      .Jobassign aside{width: 90%;
    position: relative;
    display: flex;
    align-items: center;
    padding: 0 0 10px 0;}
      .Jobassign aside button{    float: none;
    margin: 0;
    position: absolute;
    top: 0;
    right: 0;
    background-color: transparent;
    color: #000;
    padding: 0;
    width: 40px;
    height: 30px;}
      .Jobassign aside button:hover{ color: #fbaf31 }

      .Jobassign span{
    font-size: 14px;
    color: #000;
    font-weight: 500;
    width: 90px;
} 
      .Jobassign input{
    padding: 0;
    margin: 0;
    float: none;
    line-height: 27px;
    font-size: 16px;
}

.fullwidthform {
    float: left;
    min-height: 1000px;
}

ul.Jobassigndetails{}
ul.Jobassigndetails li{
    font-size: 14px;
    margin: 0 0 20px;
}
ul.Jobassigndetails li:last-child{
    margin: 0 0 20px;      
}


ul.Jobassigndetails li span.Title{
    font-weight: 600;
    color: #000;
    display: inline-block;
    width: 220px;
    position: relative;
}
ul.Jobassigndetails li span.Title:after{
      content: ':';
      position: absolute;
      right: 10px;
}




   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  <!-- Logo -->
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 MainWrapper">
                        <div class="row">
                           <div class="col-md-12 psthbs">
                              <div class="companyprofileforms fullwidthform">
                                 
                                 <div class="myhdainside">
                                    <h6>Assign a Job that you will be working on</h6>
                                 </div>

                                 <?php if($this->session->tempdata('inserted')) {?>
                                 <p class="insertMsg"><?php echo $this->session->tempdata('inserted'); ?></p>
                                 <?php } ?>
                                 <?php if($this->session->tempdata('postError')) {?>
                                 <p class="errorMsg"><?php echo $this->session->tempdata('postError'); ?></p>
                                 <?php } ?>
                                 
                                 <div class="filldetails">
                                    <div class="stepcountfrms">
                                       <div class="headsteps-gt">
                                          <div class="Jobassign">
                                                <img src="<?php echo base_url().'recruiterfiles/';?>images/fav.png">
                                                <aside>      
                                                      <span>Search : </span>
                                                      <input type="text" name="" placeholder="Assign Job" id="searchjobid">
                                                      <button type="button" onclick="seachjob()" id="searchjobbutton"><i class="fa fa-search" aria-hidden="true"></i></button>
                                                </aside>
                                          </div>
                                       </div>
                                       <p class="data0 text-center">  </p>
                                       <ul class="Jobassigndetails">
                                             <li>
                                                   <span class="Title">Job Title</span>
                                                   <span class="data2"> </span>
                                             </li>
                                             <li>
                                                   <span class="Title">Site Name</span>
                                                   <span class="data3"> </span>
                                             </li>
                                             <li>
                                                   <span class="Title">Target HC</span>
                                                   <span class="data4"> </span>
                                             </li>
                                             <li>
                                                   <span class="Title">Job Posted Date</span>
                                                   <span class="data5"> </span>
                                             </li>
                                             <li>
                                                   <span class="Title">Job Expiry Date</span>
                                                   <span class="data6"> </span>
                                             </li>
                                             <li>
                                                   <span class="Title">Job Level</span>
                                                   <span class="data7"> </span>
                                             </li>
                                             <li>
                                                   <span class="Title">Required Level of Education</span>
                                                   <span class="data8"> </span>
                                             </li>
                                             <li>
                                                   <span class="Title">Required Level of Experience</span>
                                                   <span class="data9"> </span>
                                             </li>
                                       </ul>

                                       <div class="dividehalfd">
                                          <button type="button" id="search_button" class="updts" data-toggle="modal" data-target="#myModal11" disabled="disabled">Assign Job</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>


      <!-- Modal -->
      <div id="myModal11" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 457px;">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"> Are you sure you would like to assign this job?</h4>
            </div>
            <div class="modal-body">
              
                  <form action="https://jobyoda.com/recruiter/jobpost/assignrecruit" method="post">
                        <div class="row">
                          <div class="col-md-4">
                              Sub-Recruiter : 
                          </div>
                          <div class="col-md-4">
                              <select name="assignid">
                              <?php
                                if($recruitFetch) {
                                  foreach($recruitFetch as $recruitF) {
                              ?>
                                    <option value="<?php echo $recruitF['id'];?>"> <?php echo $recruitF['fname']." ".$recruitF['lname'];?> </option>
                              <?php
                                  }
                                }
                              ?>
                              </select>
                          </div>
                        </div>
                        <div class="row">
                              <div class="col-md-12"><center>
                                    <input type="hidden" name="jid" value="" class="data10">
                                    <button class="updts" id="search_button1" type="submit" style="float: none;    margin-top: 30px;">Assign Job</button>
                              </center></div>
                        </div>
                  </form>

            </div>
          </div>

        </div>
      </div>


      <!-- <input type="hidden" id="numberofrecord" value="0"> -->
      </section>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/jquery.timepicker.min.js"></script>
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> 
      <script>
         $(document).ready(function(){
             $('#jobLoc').change(function() {
                 var recruiter_id = $('#jobLoc').val();
                 //alert(recruiter_id);
                 $.ajax({
                       type:"POST",
                       url : "<?php echo base_url(); ?>/recruiter/recruiter/recruiter_location",
                       data : {recruiter_id:recruiter_id},
                       success : function(response1) {
                           var response2 = JSON.parse(response1);
                              $(".allowcls").removeClass("selectedgreen");
                       },
                       error: function() {
                           alert('Error occured');
                       }
                 });
             });
         });

         function seachjob() {
            var jid = $('#searchjobid').val();
            console.log(jid);
            $.ajax({
                 type:"POST",
                 url : "<?php echo base_url(); ?>/recruiter/jobpost/serchfetchjob",
                 data : {jid:jid},
                 success : function(response) {
                        var response1 = JSON.parse(response);
                        console.log(response1);

                        if(response1.error) {
                              $(".data0").html(response1.error);
                              $(".data1").html('');
                              $(".data2").html('');
                              $(".data3").html('');
                              $(".data4").html('');
                              $(".data5").html('');
                              $(".data6").html('');
                              $(".data7").html('');
                              $(".data8").html('');
                              $(".data9").html('');
                              $(".data10").val('');
                        } else {
                              $(".data0").html('');
                              $(".data1").html(response1.postedBy);
                              $(".data2").html(response1.getJobs[0]['jobtitle']);
                              $(".data3").html(response1.getJobs[0]['site_name']);
                              $(".data4").html(response1.getJobs[0]['opening']);
                              $(".data5").html(response1.created_at);
                              $(".data6").html(response1.getJobs[0]['jobexpire']);
                              $(".data7").html(response1.levels[0]['level']);
                              $(".data8").html(response1.getJobs[0]['education']);
                              $(".data9").html(response1.getJobs[0]['experience']);
                              $(".data10").val(response1.getJobs[0]['id']);
                              $("#search_button").attr("disabled", false);
                        }
                 },
                 error: function() {
                     alert('Error occured');
                 }
            });
         }
      </script>
      <style>
         .psthbs .filldetails select.halfsideth {
         width: 63%;
         float: left;
         }
         .psthbs .filldetails select.halfsideth {
         width: 63%;
         float: left;
         }
         .filldetails .multipleopyodf button {
         padding: 0;
         float: left;
         width: 100%;
         background: transparent;
         color: #000;
         font-weight: 100;
         font-size: 12px;
         margin: 0;
         outline: none !important;
         }
         .multipleopyodf {
         position: relative;
         }
         .filldetails .multipleopyodf .btn-group {
         width: 100%;
         }
         .multipleopyodf input[type="checkbox"] {
         opacity: 1;
         position: relative;
         left: 3px;
         top: 2px;
         margin-right: 6px;
         }
         .multipleopyodf .dropdown-menu {
         padding: 10px;
         width: 100%;
         }
      </style>
      <script type="text/javascript">
         $(function() {
         var nav = $(".nohdascr");
         $(window).scroll(function() {    
         var scroll = $(window).scrollTop();
         
         if (scroll >= 110) {
          nav.removeClass('nohdascr').addClass("hdafxd");
         } else {
          nav.removeClass("hdafxd").addClass('nohdascr');
         }
         });
         });
      </script>
   </body>
</html>
