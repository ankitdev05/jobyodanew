<?php include_once('header.php');
// echo "<pre>";
// print_r($jobcount);die;
   if($this->session->userdata('userfsess')) {
       $userfsess = $this->session->userdata('userfsess');
       $type = $userfsess['type'];
     }
     $ip = $_SERVER['REMOTE_ADDR'];
     $location = file_get_contents('http://ip-api.com/json/'.$ip);
     $data_loc = json_decode($location);
     
     $Address = $data_loc->city;
     $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$Address.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk');
     $output = json_decode($geocodeFromAddr);
               
     //Get latitude and longitute from json data
    $dataLatitude  = $output->results[0]->geometry->location->lat; 
    $dataLongitude = $output->results[0]->geometry->location->lng;
   
?>
<style>
   .filterchekers input[type="checkbox"] {
   opacity: 0;
   z-index: auto;
   margin: 0;
   width: 90px;
   height: 55px;
   margin-top: -59px;
   }
   input[type="checkbox"], input[type="radio"] {
   z-index: 0!important;
   }
   .filterchekers ul li i {
   font-size: 25px;
   color: #27aa60;
   padding: 13px 12px;
   box-shadow: #adadad 1px 1px 6px 0px;
   border-radius: 25px;
   margin-bottom: 6px;
   text-align: center;
   width: 100px;
   height: 50px;
   }
   .managerpart input[type="reset"] {
   float: none;
   width: 93%;
   border: none;
   border-bottom: 1px solid #ddd;
   padding: 8px 13px;
   margin: 10px 0px;
   }
   #snackbar {
   visibility: hidden;
   min-width: 250px;
   margin-left: -125px;
   background-color: #dff0d8
   color: #3c763d;
   text-align: center;
   border-radius: 2px;
   padding: 1px;
   position: fixed;
   z-index: 10000;
   left: 74%;
   top: 39%;
   font-size: 17px;
   }
   .alert, .thumbnail {
   margin-bottom: 0px;
   }
   #snackbar.show {
   visibility: visible;
   -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
   animation: fadein 0.5s, fadeout 0.5s 2.5s;
   }
   @media screen and (max-width: 767px) and (min-width: 320px){
   .managerpart input[type="reset"] {
   float: none;
   width: 93%;
   border: none;
   border-bottom: 1px solid #ddd;
   padding: 8px 13px;
   margin: 10px 0px;
   }
   }
</style>
<div class="managerpart">
   <div class="container-fluid">
      <div class="row">

       
           <div class="col-md-3 sideshift normnoscr">
		    <form method="post" action="<?php echo base_url('dashboard/filters') ?>">
              <div class="dashboardleftmenu">
			  <div class="menuinside">
                    <?php include_once('sidebar.php'); ?>
                 </div>
				 
                 <div class="mainheadings-yoda">
                    <h5>Search Jobs</h5>
                    <div class="prosave filink">
                       <div class="combinefilters filrgt">
                          <a href="<?php echo base_url('dashboard');?>"><i class="fas fa-arrow-left"></i>back</a>
                       </div>
                    </div>
                 </div>
                 <div class="searchareas">
                    <button type="submit" class="searchfiels" style="float:left;">Search Jobs</button>
                 </div>
                 <div class="mainheadings-yoda">
                    <h5>Filters</h5>
                 </div>
                 <div class="basicfiltersleft">
                    <p class="greenhdn">Sort By :</p>
                    <div class="checkflowrt">
                       <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control" <?php if(!empty($salarysort)){if($salarysort=='1'){echo "checked";}}?> id="customRadio" name="salarySort" value="1">
                          <label class="custom-control-label" for="customRadio">Salary</label>
                       </div>
                    </div>
                    <div class="checkflowrt">
                       <div class="custom-control custom-radio">
                          <input type="radio" class="custom-control" <?php if(!empty($locationsort)){if($locationsort=='1'){echo "checked";}}?> id="customRadio3" name="locationSort" value="1">
                          <!-- <input type="hidden" name="lat" value="<?= $dataLatitude;?>">
                          <input type="hidden" name="long" value="<?= $dataLongitude;?>"> -->
                          <label class="custom-control-label" for="customRadio3">Nearby Location</label>
                       </div>
                    </div>
                 </div>
                 <div class="basicfiltersleft">
                    <p class="greenhdn">Basic Filters :</p>
                    <div class="formsrches">
                       <div class="formmidaress">
                          <div class="filldetails">
                             <input type="text" class="form-control" name="location" placeholder="City/Location" id="txtPlaces">
                             <input type="hidden" name="lat" id="lat" >
                             <input type="hidden" name="long" id="long" >
                             <select name="industry" class="form-control" style="margin-left:0px" style="margin-left:0px;">
                                <option value=""> Industry </option>
                                <?php
                                   foreach ($industrylists as $industrie) {
                                   ?>
                                <option value="<?php echo $industrie['id'];?>"> <?php echo $industrie['name'];?> </option>
                                <?php
                                   }
                                   ?>
                             </select>
                             <div class="form-group"></div>
                             <select name="joblevel" class="form-control" style="margin-left:0px" placeholder="Select Job Level">
                                <option value="">Job Level</option>
                                <?php
                                   foreach ($level_list as $level_lists) {
                                   ?>
                                <option value="<?php echo $level_lists['id'];?>"> <?php echo $level_lists['level'];?> </option>
                                <?php
                                   }
                                   ?>
                             </select>
                             <div class="form-group"></div>
                             <select name="jobcategory" id="jobcategoryy" class="form-control" onchange='getcatsubcat()' style="margin-left:0px" placeholder="Select Job Category">
                                <option value="">Job Category</option>
                                <?php
                                   foreach ($category_list as $category_lists) {
                                   ?>
                                <option value="<?php echo $category_lists['id'];?>"> <?php echo $category_lists['category'];?> </option>
                                <?php
                                   }
                                   ?>
                             </select>
                             <div class="form-group"></div>
                             <select name="jobsubcategory" id="subcatresss" class="form-control" style="margin-left:0px" placeholder="Select Job Subcategory">
                                <option value=""> Job Subcategory</option>
                             </select>
                             <div class="form-group"></div>
                             <select name="language" class="form-control" style="margin-left:0px" style="margin-left:0px;">
                                <option value="">  Language </option>
                                <?php
                                   foreach ($languagelist as $languagelists) {
                                   ?>
                                <option value="<?php echo $languagelists['id'];?>"> <?php echo $languagelists['name'];?> </option>
                                <?php
                                   }
                                   ?>
                             </select>
                             <div class="form-group"></div>
                             <select name="exp_year" placeholder="Years of Exp." class="form-control" style="margin-left:0px" style="margin-left:0px;">
                                <option value="">Years of Exp.</option>
                                <?php for($i=0;$i<=20;$i++) {?>
                                <option value="<?php echo $i;?>"><?php echo $i.' '.'years';?>
                                </option>
                                <?php }?>
                             </select>
                             <div class="form-group"></div>
                             <select name="exp_month" placeholder="Months of Exp." class="texts form-control" style="margin-left:0px" style="margin-left:0px;">
                                <option value="">Months of Exp.</option>
                                <?php for($i=0;$i<=12;$i++) {?>
                                <option value="<?php echo $i;?>"><?php echo $i.' '.'months';?></option>
                                <?php }?>
                             </select>
                          </div>
                       </div>
                       <input type="reset" name="" value="Reset All">
                    </div>
                 </div>
                 <div class="basicfiltersleft">
                    <p class="greenhdn">Advance Filter : </p>
                    <div class="formsrches">
                       <div class="dropwidgetsar">
                          <p>Compensation & Benefits</p>
                          <a class="plusshow" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-plus"></i></a>
                       </div>
                       <div class="collapse" id="collapseExample">
                          <input type="text" class="form-control" name="basicSalary" placeholder="Enter Minimum Expectation">
                       </div>
                    </div>
                    <div class="formsrches">
                       <div class="dropwidgetsar">
                          <p>Top Picks</p>
                          <a class="plusshow" data-toggle="collapse" href="#collapseExample8" role="button" aria-expanded="false" aria-controls="collapseExample2"><i class="fas fa-plus"></i></a>
                       </div>
                       <div class="collapse" id="collapseExample8">
                          <div class="filterchekers">
                             <ul>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/joining-bonus.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/joining-bonus-1.png" class="hvrsicos">
                                   <input type="checkbox"  name="toppicks[]" value="1">
                                   <p>Joining <br>bonus</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-food.png" class="normicos">
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-food-1.png" class="hvrsicos">
                                   <input type="checkbox" name="toppicks[]" value="2">
                                   <p> Free<br> Food</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo-for-depended.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo-for-depended-1.png" class="hvrsicos">
                                   <input type="checkbox"  name="toppicks[]" value="3">
                                   <p>Day 1 HMO for <br> Department</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-1-hmo-white.png" class="hvrsicos">
                                   <input type="checkbox"  name="toppicks[]" value="4">
                                   <p>Day 1<br>HMO</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-shift.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/day-shift-1.png" class="hvrsicos">
                                   <input type="checkbox"  name="toppicks[]" value="5">
                                   <p> Day <br> Shift</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/14-month-pay.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/14-month-pay-1.png" class="hvrsicos">
                                   <input type="checkbox" id="topic9" name="toppicks[]" value="6">
                                   <p> 14th Month<br>Pay</p>
                                </li>
                             </ul>
                          </div>
                       </div>
                    </div>
                    <div class="formsrches">
                       <div class="dropwidgetsar">
                          <p>Allowances & Incentives</p>
                          <a class="plusshow collapsed" data-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="true" aria-controls="collapseExample2"><i class="fas fa-minus"></i></a>
                       </div>
                       <div class="collapse show" id="collapseExample3">
                          <div class="filterchekers">
                             <ul>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/cell-phone-allowance.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/cell-phone-allowance-1.png" class="hvrsicos">
                                   <input type="checkbox" name="allowances[]" value="1">
                                   <p> Cell Phone<br>Allowances</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/annual--performance-bonus.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/annual--performance-bonus-1.png" class="hvrsicos">
                                   <input type="checkbox" name="allowances[]" value="2">
                                   <p> Annual <br> Performance Bonus</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-parking.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-parking-1.png" class="hvrsicos">
                                   <input type="checkbox" name="allowances[]" value="3">
                                   <p>Free <br>Parking</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-shuttle.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-shuttle-1.png" class="hvrsicos">
                                   <input type="checkbox" name="allowances[]" value="4">
                                   <p> Free <br> Shuttle</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/reteirment-benifits.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/reteirments-benefits-1.png" class="hvrsicos">
                                   <input type="checkbox" name="allowances[]" value="5">
                                   <p> Retirement <br> Benefits</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/transportation.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/transportation-1.png" class="hvrsicos">
                                   <input type="checkbox" name="allowances[]" value="6">
                                   <p> Transportation <br> Allowance</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/monthly-performance-incentive.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/monthly-performance-incentive-1.png" class="hvrsicos">
                                   <input type="checkbox" name="allowances[]" value="7">
                                   <p> Monthly Performance<br>Incentive</p>
                                </li>
                             </ul>
                          </div>
                       </div>
                    </div>
                    <div class="formsrches">
                       <div class="dropwidgetsar">
                          <p>Medical Benefits</p>
                          <a class="plusshow" data-toggle="collapse" href="#collapseExample4" role="button" aria-expanded="true" aria-controls="collapseExample2"><i class="fas fa-plus"></i></a>
                       </div>
                       <div class="collapse" id="collapseExample4">
                          <div class="filterchekers">
                             <ul>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/medicine-reimbursemer.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/medicine-reimbursemer-1.png" class="hvrsicos">
                                   <input type="checkbox" name="medical[]" value="1">
                                   <p> Medicine <br>Reimbursemer</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-hmo-for-dependents.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/free-hmo-for-dependents-1.png" class="hvrsicos">
                                   <input type="checkbox" name="medical[]" value="2">
                                   <p> Free HMO for Dependents <br>Allowances</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/critical-illness-benefits.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/critical-illness-benefits1.png" class="hvrsicos">
                                   <input type="checkbox" name="medical[]" value="3">
                                   <p>Cretical illness <br>Benefits</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/life-insurence.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/life-insurence-1.png" class="hvrsicos">
                                   <input type="checkbox" name="medical[]" value="4">
                                   <p> Life<br> Insurance</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/maternity-assistance.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/maternity-assistance-1.png" class="hvrsicos">
                                   <input type="checkbox" name="medical[]" value="5">
                                   <p>Maternity <br>Assistance</p>
                                </li>
                             </ul>
                          </div>
                       </div>
                    </div>
                    <div class="formsrches">
                       <div class="dropwidgetsar">
                          <p>Work Shifts/Leaves</p>
                          <a class="plusshow" data-toggle="collapse" href="#collapseExample5" role="button" aria-expanded="true" aria-controls="collapseExample2"><i class="fas fa-plus"></i></a>
                       </div>
                       <div class="collapse" id="collapseExample5">
                          <div class="filterchekers">
                             <ul>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/mid-shift.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/mid-shift-1.png" class="hvrsicos">
                                   <input type="checkbox" name="workshift[]" value="1">
                                   <p> Mid <br>Shift</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/night-shift.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/night-shift-1.png" class="hvrsicos">
                                   <input type="checkbox" name="workshift[]" value="2">
                                   <p> Night <br>Shift</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/24.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/24-1.png" class="hvrsicos">
                                   <input type="checkbox" name="workshift[]" value="3">
                                   <p>24/7</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/weekend-off.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/weekend-off-1.png" class="hvrsicos">
                                   <input type="checkbox" name="leaves[]" value="41">
                                   <p> Weekend<br> Off</p>
                                </li>
                                <li>
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/holiday-off.png" class="normicos">  
                                   <img src="<?php echo base_url(); ?>webfiles/img/toppics/holiday-off-1.png" class="hvrsicos">
                                   <input type="checkbox" name="leaves[]" value="42">
                                   <p>Holiday <br>Off</p>
                                </li>
                             </ul>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
			  </form>
           </div>
        

         <div class="col-md-9 expanddiv">
		 <div class="innerbglay">
            <div class="mainheadings-yoda">
               <h5>Search Jobs</h5>
               <div class="prosave filink">
                  <div class="combinefilters filrgt">
                     <a href="javascript:void(0)" class="listtoggle"><i class="fas fa-bars"></i> List</a>
                     <a href="javascript:void(0)" class="listtoggle1" style="display: none;"><i class="fas fa-bars"></i> Map</a>
                  </div>
               </div>
            </div>

            <div class="adminopnts maplistedsie">
			 <div class="searchareas">
                    <button type="submit" class="searchfiels onmapsrchs" style="float:left;">Search Jobs</button>
                 </div>
				 
               <?php if(empty($jobList)){ ?>
               <center>
                  <h3 id="mapdata">No Jobs Found</h3>
               </center>
               <?php } else{?>
               <div class="mapsectionapp">
                  <div id="map" style="width: 100%; height: 470px;"></div>
               </div>
               <?php }?>
            </div>
            <div class="adminopnts">
               <div class="jobllisting" id="result" style="display:none">
                  <div class="tabledivsdsn">
                     
                     <div class="row tableshead">
                        <div class="col-md-3 col-lg-3 ">
                           <h6>Applied Jobs</h6>
                        </div>
                        <div class="col-md-6 col-lg-5">
                           <h6>Position</h6>
                        </div>
                        <div class="col-md-3 col-lg-2">
                           <h6>Salary</h6>
                        </div>
                     </div>
                     <?php
                        if(array_filter($jobList)) {
                           $x=30;
                           foreach($jobList as $jobListing) {
                                 $jobDetails = $this->Jobpost_Model->job_detail_fetch($jobListing['jobpost_id']);
                                 $recruiterdetail = $this->Jobpost_Model->job_detailLocation_fetch($jobDetails[0]['company_id']);
                                 $timeFrom = $recruiterdetail[0]['from_time'];
                                 $timeFromm = date('H:i', strtotime($timeFrom));
                                 $timeTo   = $recruiterdetail[0]['to_time'];
                                 $timeToo = date('H:i', strtotime($timeTo));
                      ?>
                      <div class="row tablesrow">
                         <div class="col-md-3 col-lg-3 ">
                            <div class="imagethumbs">
                               <?php
                                  if(!empty($jobListing['job_image']))
                                  {
                                  ?>
                               <img src="<?php echo $jobListing['job_image'];?>" style="width:180px;height:160px;">  
                               <?php
                                  }
                                  else {
                                  ?>
                               <img src="<?php echo base_url().'webfiles/';?>img/thumpost.jpg">
                               <?php
                                  }
                                  ?>
                            </div>
                         </div>
                         <div class="col-md-6 col-lg-5">
                            <div class="positionarea">
                               <!--    <h6>Position</h6>-->
                               <a href="<?php echo base_url();?>dashboard/jobDescription?listing=<?php echo $jobListing['jobpost_id'];?>">
                                  <p class="posttypes"><?php echo $jobListing['job_title']; ?></p>
                               </a>
                               <h3><?php echo $jobListing['cname']; ?></h3>
                               <a href="<?php echo base_url('dashboard/companyProfile/'.$jobListing['company_id'])?>">
                                  <h3><?php echo $jobListing['companyName']; ?></h3>
                               </a>
                               <p><?= $jobListing['jobPitch'];?></p>
                               <p class="locationtype"><img src="<?php echo base_url().'webfiles/';?>img/locmap.png"><?php echo $jobListing['company_address']; ?></p>
                               <p><?php echo $jobListing['distance'];?>KM</p>
                               <?php 
                                  if($jobListing['save_status']==1)
                                  {
                                   $title="Saved";
                                  ?>
                               <span id="test3<?php echo $jobListing['jobpost_id'];?>"  onclick="savedjob('<?php echo $jobListing['jobpost_id']?>')">
                               <i class="fa fa-heart" title="<?php echo $title;?>" style="color:red;font-size: 24px;cursor:pointer;"></i>
                               </span>
                               <?php 
                                  }
                                  if($jobListing['save_status']==0)
                                  {
                                    $title="Unsaved";
                                  
                                  ?>
                               <span id="test4<?php echo $jobListing['jobpost_id'];?>" title="<?php echo $title;?>" onclick="savedjob('<?php echo $jobListing['jobpost_id']?>')">
                               <i class="fa fa-heart" title="<?php echo $title;?>" style="color:#b5b5b5;font-size: 24px;cursor:pointer;"></i>
                               </span>
                               <?php }?>
                               <span id="checkedstattus<?php echo $jobListing['jobpost_id'];?>" onclick="savedjob('<?php echo $jobListing['jobpost_id']?>')"></span>
                               <button class="greenbtn" data-toggle="modal" data-target="#exampleModalCenterb<?php echo $x;?>">Apply</button>
                            </div>
                         </div>
                         <div class="col-md-9 col-lg-3 lefthalf">
                            <div class="salryedits">
                               <h4>
                                  <?php 
                                     if(isset($jobListing['salary']) && $jobListing['salary'] !='')
                                       {
                                       echo $jobListing['salary']."/Month";
                                       }  
                                     
                                     ?>
                               </h4>
                               <?php
                                  if(!empty($jobListing['toppicks1']))
                                    {
                                     if($jobListing['toppicks1']=='1')
                                     {
                                     ?>
                               <div class="salry">
                                  <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                  <span>Bonus</span>
                               </div>
                               <?php 
                                  }
                                  
                                  if($jobListing['toppicks1']=='2')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                  <span>Free Food</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks1']=='3')
                                   {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                  <span>Day 1 HMO</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks1']=='4')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                  <span>Day 1 HMO for Dependent</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks1']=='5')
                                  {
                                    ?>
                               <div class="salry">
                                  <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                  <span>Day Shift</span>
                               </div>
                               <?php
                                  }
                                  if($jobListing['toppicks1']=='6')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                  <span>14th Month Pay</span>
                               </div>
                               <?php
                                  }}
                                  
                                  
                                  if(!empty($jobListing['toppicks2']))
                                  {
                                  if($jobListing['toppicks2']=='1')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                  <span>Bonus</span>
                               </div>
                               <?php 
                                  }
                                  
                                  if($jobListing['toppicks2']=='2')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                  <span>Free Food</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks2']=='3')
                                   {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                  <span>Day 1 HMO</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks2']=='4')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                  <span>Day 1 HMO for Dependent</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks2']=='5')
                                  {
                                    ?>
                               <div class="salry">
                                  <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                  <span>Day Shift</span>
                               </div>
                               <?php
                                  }
                                  if($jobListing['toppicks2']=='6')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                  <span>14th Month Pay</span>
                               </div>
                               <?php
                                  }}
                                  
                                  
                                  if(!empty($jobListing['toppicks3']))
                                  {
                                  if($jobListing['toppicks3']=='1')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Bonus" src="<?php echo base_url(); ?>recruiterfiles/images/m_bonus.png">
                                  <span>Bonus</span>
                               </div>
                               <?php 
                                  }
                                  
                                  if($jobListing['toppicks3']=='2')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Free Food" src="<?php echo base_url(); ?>recruiterfiles/images/m_freefood.png">
                                  <span>Free Food</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks3']=='3')
                                   {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO" src="<?php echo base_url(); ?>recruiterfiles/images/m_day_1_hmo.png">
                                  <span>Day 1 HMO</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks3']=='4')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="Day 1 HMO for Dependent" src="<?php echo base_url(); ?>recruiterfiles/images/m_dependent_hmo.png">
                                  <span>Day 1 HMO for Dependent</span>
                               </div>
                               <?php }
                                  if($jobListing['toppicks3']=='5')
                                  {
                                    ?>
                               <div class="salry">
                                  <img title="Day Shift" src="<?php echo base_url(); ?>recruiterfiles/images/m_dayshift.png">
                                  <span>Day 1 HMO for Dependent</span>
                               </div>
                               <?php
                                  }
                                  if($jobListing['toppicks3']=='6')
                                  {
                                  ?>
                               <div class="salry">
                                  <img title="14th Month Pay" src="<?php echo base_url(); ?>recruiterfiles/images/m_14th_pay.png">
                                  <span>14th Month Pay</span>
                               </div>
                               <?php
                                  }}
                                  ?>
                            </div>
                         </div>
                      </div>

                     <div class="modal fade" id="exampleModalCenterb<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form method="post" action="<?php echo base_url();?>dashboard/resumeUploadListing" enctype="multipart/form-data">
                                          <div class="addupdatecent">
                                             <div class="profileupload">
                                                <img src="<?php echo base_url().'webfiles/';?>img/savedbighover.png" style="width:50px; height:auto;">
                                                <input type="file" class="resumeupdtf" id="file<?php echo $x;?>" title="" name="resumeFile" onchange='getFilename("<?php echo $x ?>")'>
                                             </div>
                                             <p id="setimgres<?php echo $x ?>"></p>
                                             <input type="hidden" id="imagedata<?php echo $x ?>">
                                             <p>Would you like to update Your Resume?</p>
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-toggle="modal" data-target="#exampleModalCenterc<?php echo $x;?>" data-dismiss="modal">No</p>
                                                &nbsp;
                                                <input  type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="button" onclick="PdfImagesend('<?php echo $x ?>')" data-toggle="modal" data-target="#exampleModalCenterdd<?php echo $x;?>" data-dismiss="modal"  class="updty updt">Yes</button>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="exampleModalCenterc<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                          <div class="schedulejobgs">
                                             <h6>Schedule interview</h6>
                                             <p>Select Date & Time</p>
                                             <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker1<?php echo $jobListing['jobpost_id']?>" autocomplete="off" placeholder="mm/dd/yy" onchange="getdayname('<?php echo $jobListing['jobpost_id']?>')"  required="required">
                                                <span id="setval<?php echo $jobListing['jobpost_id']?>" style="color:red"></span>
                                                <input type="hidden" id="dayfromm<?php echo $jobListing['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytoo<?php echo $jobListing['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                                                <input type="hidden" id="timefromm<?php echo $jobListing['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetoo<?php echo $jobListing['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                             </div>
                                             <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTime('<?php echo $jobListing['jobpost_id']?>')" id="datepicker1<?php echo $jobListing['jobpost_id']?>"  data-format="hh:mm:ss" required="required"> 
                                                <span id="setvall<?php echo $jobListing['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                             </div>
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $jobListing['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="submit" class="updt" id="schedule<?php echo $jobListing['jobpost_id']?>" disabled>Schedule & Apply</button>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="exampleModalCenterdd<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form method="post" action="<?php echo base_url();?>dashboard/scheduledListing">
                                          <div class="schedulejobgs">
                                             <h6>Schedule interview</h6>
                                             <p>Select Date & Time</p>
                                             <div class="forminputspswd">
                                                <p>Date</p>
                                                <input type="text" name="scheduledate" class="form-control datetimepicker1" id="datetimepicker12<?php echo $jobListing['jobpost_id']?>" placeholder="mm/dd/yy" onchange="getdaynamee('<?php echo $jobListing['jobpost_id']?>')"  required="required" autocomplete="off">
                                                <input type="hidden" id="dayfrommm<?php echo $jobListing['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayfrom'];?>">
                                                <input type="hidden" id="daytooo<?php echo $jobListing['jobpost_id']?>" value="<?php echo $recruiterdetail[0]['dayto'];?>">
                                                <input type="hidden" id="timefrommm<?php echo $jobListing['jobpost_id']?>" value="<?php echo $timeFromm;?>">
                                                <input type="hidden" id="timetooo<?php echo $jobListing['jobpost_id']?>" value="<?php echo $timeToo;?>">
                                                <span id="setval12<?php echo $jobListing['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-calendar-alt fieldicons"></i>
                                             </div>
                                             <div class="forminputspswd">
                                                <p>Time</p>
                                                <input type="text" name="scheduletime" autocomplete="off" class="form-control bootstrap-timepicker timepicker" onchange="getTimee('<?php echo $jobListing['jobpost_id']?>')" id="datepicker13<?php echo $jobListing['jobpost_id']?>"  data-format="hh:mm:ss" required="required"> 
                                                <span id="setvall12<?php echo $jobListing['jobpost_id']?>" style="color:red"></span>
                                                <i class="far fa-clock"></i>
                                             </div>
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <input type="hidden" name="listing" value="<?php echo $jobListing['jobpost_id'];?>">
                                                <input type="hidden" name="type" value="<?php echo $x;?>">
                                                <button type="submit" class="updt" id="schedulees<?php echo $jobListing['jobpost_id']?>" disabled>Schedule & Apply</button>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="exampleModalCenter100<?php echo $x;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header"> 
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <form>
                                          <div class="addupdatecent">
                                             <img src="<?php echo base_url('webfiles/img/savedbighover.png')?>">
                                             <p>Job Applied Successfully</p>
                                             <div class="statsusdd">
                                                <p class="updt" data-dismiss="modal" aria-label="Close">Ok</p>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                  <?php
                        $x++;
                      }
                    }else{
                  ?>
                     <center>
                        <h3>No Jobs Found</h3>
                     </center>
                  <?php }?>
                  </div>
               </div>
            </div>
			</div>
         </div>
      </div>
   </div>
</div>

<?php include_once('footer.php'); ?>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <span id="errorfield"></span>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Enter New Password" name="newpass" id="newpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Confirm New Password" name="confpass" id="confpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <button type="button" id="changepassbtn" class="srchbtns">Change</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModalCenterjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">
               <span id="savedjobmessage"></span>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="snackbar"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>recruiterfiles/css/jquery.timepicker.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>recruiterfiles/js/jquery.timepicker.min.js"></script>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script>
<script src="<?php echo base_url(); ?>recruiterfiles/js/maps2.js" type="text/javascript"></script>

<script type="text/javascript">
   google.maps.event.addDomListener(window, 'load', function () {
      var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
    
       google.maps.event.addListener(places, 'place_changed', function () {
         
           var place = places.getPlace();
           var address = place.formatted_address;
           var latitude = place.geometry.location.A;
           var longitude = place.geometry.location.F;
           $('#lat').val(place.geometry.location.lat());
           $('#long').val(place.geometry.location.lng());
           var mesg = "Address: " + address;
           mesg += "\nLatitude: " + latitude;
           mesg += "\nLongitude: " + longitude;
       });
   });
</script>

<script type="text/javascript">
   $(function () {
      $(".datetimepicker1").datepicker({ 
       dateFormat: "dd/mm/yy", 
        yearRange: '1900:2020', 
        defaultDate: '',
        autoclose: true,
     })
   });
   $('.timepicker').timepicker({ 'timeFormat': 'H:i' });
    
</script>

<?php
   if(isset($_GET["msg"]) && $_GET["msg"] == "uploadcomplete" ) {
?>
      <script type="text/javascript">
         $(window).on('load',function(){
         $('#exampleModalCenter9').modal('show');
         });
      </script>
<?php
   }
?>

<?php
   if(isset($_GET["msg"]) && $_GET["msg"] == "scheduled") {
?>
    <script type="text/javascript">
       $(document).ready(function() {
         $(".mapsectionapp").css("display","none");
         $("#mapdata").text('');
         $(".jobllisting").css("display","block");
         var type='<?php echo $_GET["type"]?>';
         var customid = '#exampleModalCenter100'+type;
       // alert(customid);
         $(customid).modal('show');
        });
    </script>
<?php
   }
?>


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places"></script>
<script type="text/javascript">
   function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          disableDefaultUI: true,
          center: {lat: <?php echo $dataLatitude;?>, lng: <?php echo $dataLongitude;?>}
        });

         var icon = { 
                url: 'http://mobuloustech.com/jobyoda/markericon/maps.png'
            };

         var infoWin = new google.maps.InfoWindow();   

        var markers = locations.map(function(location, i) {
                var marker = new google.maps.Marker({
                    position: {lat: location.lat, lng: location.lng},
                    icon:{url:location.url}
                });
                google.maps.event.addListener(marker, 'click', function(evt) {
                    infoWin.setContent(location.info);
                    infoWin.open(map, marker);
                })
                return marker;
            });

        var mcOptions = {
              //imagePath: 'https://googlemaps.github.io/js-marker-clusterer/images/m',
              styles:[{
                    url: "https://googlemaps.github.io/js-marker-clusterer/images/m1.png",
                    width: 53,
                    height:53,
                    fontFamily:"comic sans ms",
                    textSize:15,
                    textColor:"white"
              }]
              
            };

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers, mcOptions);
      }

      var locations = [
            <?php
              
              foreach ($jobcount as $js) {
                    //var_dump($js);
                    //print_r($jobcount);
                     echo "{lat: ".$js['lat'].", lng: ".$js['lng'].", info: '".$js['info']."', url:'".$js['url']."'},";
               }
            ?>     
      ];
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&callback=initMap">
    </script>
<script>
   $('input.custom-control').on('change', function() {
      $('input.custom-control').not(this).prop('checked', false);  
   });
</script>

<script>
   $(document).ready(function(){    
     $(".listtoggle").on("click", function(){
         $(".jobllisting").css("display", "block");
         $(".mapsectionapp").css("display", "none");
         $("#mapdata").html('');
         $(".listtoggle1").css("display", "block");
         $(".listtoggle").css("display", "none");
     });
     $(".listtoggle1").on("click", function(){
         $(".jobllisting").css("display", "none");
         $(".mapsectionapp").css("display", "block");
         ("#mapdata").text("No Data Found");
         $(".listtoggle1").css("display", "none");
         $(".listtoggle").css("display", "block");
     });
   
   });
   
   $(".filterchekers li").click(function(){
   $(this).toggleClass("selectedgreen");  
   });
   
</script>

<script>
   function getdayname(id)
      {
       var getdate = $("#datetimepicker1"+id).val();
       var dayfrom = $("#dayfromm"+id).val();
       var dayto   = $("#daytoo"+id).val();
       if(dayfrom ==1){
         var day ='Monday';
       }
       if(dayfrom ==2){
         var day ='Tuesday';
       }
       if(dayfrom ==3){
         var day ='Wednesday';
       }
       if(dayfrom ==4){
         var day ='Thursday';
       }
       if(dayfrom ==5){
         var day ='Friday';
       }
       if(dayfrom ==6){
         var day ='Saturday';
       }
       if(dayfrom ==7){
         var day ='Sunday';
       }
       
      if(dayto ==1){
         var dayt ='Monday';
       }
       if(dayto ==2){
         var dayt ='Tuesday';
       }
       if(dayto ==3){
         var dayt ='Wednesday';
       }
       if(dayto ==4){
         var dayt ='Thursday';
       }
       if(dayto ==5){
         var dayt ='Friday';
       }
       if(dayto ==6){
         var dayt ='Saturday';
       }
       if(dayto ==7){
         var dayt ='Sunday';
       }
       $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
         'data' :'dateValue='+getdate+'&jobId='+id,
         'success':function(htmlres)
          {
           if(htmlres !=''){
              if(htmlres == 1){
                     $('#schedule'+id).attr('disabled',false);
                     $("#setval"+id).html("");
                     $("#datetimepicker1"+id).val(getdate);
              } else {
                   $("#setval"+id).html("Please Select Schedule date between ("+day+" to "+dayt+")");
                   $("#datetimepicker1"+id).val(''); 
                   $('#schedule'+id).attr('disabled',true);
              }
            }
          }            
        });
    }
</script>
<script>
   function getTime(id) {
         var gettime= $("#datepicker1"+id).val();
         var timefrom= $("#timefromm"+id).val();
         var timetoo= $("#timetoo"+id).val();
         //return false;
         $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
            'data' :'timeValue='+gettime+'&jobId='+id,
            'success':function(htmlres)
            {
              if(htmlres !='') {
                if(htmlres == 1) {
                  $('#schedule'+id).attr('disabled',false);
                  $("#setvall"+id).html("");
                  $("#datepicker1"+id).val(gettime);
                
                } else {
                   $("#setvall"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                   $("#datepicker1"+id).val(''); 
                   $('#schedule'+id).attr('disabled',true);
                }
            }
          }
              
        });
    }
</script>
<script>
   function getdaynamee(id) {
       var getdate= $("#datetimepicker12"+id).val();
       var dayfrom = $("#dayfrommm"+id).val();
       var dayto   = $("#daytooo"+id).val();
       if(dayfrom ==1){
         var day ='Monday';
       }
       if(dayfrom ==2){
         var day ='Tuesday';
       }
       if(dayfrom ==3){
         var day ='Wednesday';
       }
       if(dayfrom ==4){
         var day ='Thursday';
       }
       if(dayfrom ==5){
         var day ='Friday';
       }
       if(dayfrom ==6){
         var day ='Saturday';
       }
       if(dayfrom ==7){
         var day ='Sunday';
       }
       
      if(dayto ==1){
         var dayt ='Monday';
      }
       if(dayto ==2){
         var dayt ='Tuesday';
       }
       if(dayto ==3){
         var dayt ='Wednesday';
       }
       if(dayto ==4){
         var dayt ='Thursday';
       }
       if(dayto ==5){
         var dayt ='Friday';
       }
       if(dayto ==6){
         var dayt ='Saturday';
       }
       if(dayto ==7){
         var dayt ='Sunday';
       }
       $.ajax({
         'type' :'POST',
         'url' :"<?php echo base_url('dashboard/checkdateschedule') ?>",
         'data' :'dateValue='+getdate+'&jobId='+id,
         'success':function(htmlres)
          {
              if(htmlres !=''){
                if(htmlres == 1){
                     $('#schedulees'+id).attr('disabled',false);
                     $("#setval12"+id).html("");
                     $("#datetimepicker12"+id).val(getdate);
                }else{
                   $("#setval12"+id).html("Please Select Schedule date between ("+day+" to "+dayt+")");
                   $("#datetimepicker12"+id).val(''); 
                   $('#schedulees'+id).attr('disabled',true);
                }
              }
          }    
        });
    }
</script>

<script>
   function getTimee(id)
      {
         var gettime= $("#datepicker13"+id).val();
         var timefrom= $("#timefrommm"+id).val();
         var timetoo= $("#timetooo"+id).val();
         $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/checktimeschedule') ?>",
            'data' :'timeValue='+gettime+'&jobId='+id,
            'success':function(htmlres)
            {
                if(htmlres !='') {
                  if(htmlres == 1) {
                    $('#schedulees'+id).attr('disabled',false);
                    $("#setvall12"+id).html("");
                    $("#datepicker13"+id).val(gettime);
                  
                  } else {
                      $("#setvall12"+id).html("Please Select Schedule time between ("+timefrom+" to "+timetoo+")");
                      $("#datepicker13"+id).val(''); 
                      $('#schedulees'+id).attr('disabled',true);
                  }
                }
            }
              
          });
      }
</script>
<script>
   function savedjob(id)
         {
           $.ajax({
            'type' :'POST',
            'url' :"<?php echo base_url('dashboard/savedjovData') ?>",
            'data' :'jobId='+id,
            'success':function(htmlres)
            {
               //alert(htmlres);return false;  
               if(htmlres !='')
               {
               if(htmlres == 1)
               {
                  $("#test4"+id).hide();
                  $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Saved" style="color:red;font-size: 24px;cursor:pointer;"></i>');
                 var msg="Job saved successfully";
                 myFunction(msg);
                }
               if(htmlres == 2)
               {
                 $("#test3"+id).hide();
                 $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Unsaved" style="color:#b5b5b5;font-size: 24px;cursor:pointer;"></i>');
                 /*var msg="jobs are unsaved successfully";
                 myFunction(msg);*/
                }
                if(htmlres == 3)
               {
                 $("#test4"+id).hide();
                 $("#checkedstattus"+id).html('<i class="fa fa-heart" title="Saved" style="color:red;font-size: 24px;cursor:pointer;"></i>');
                 var msg="jobs are saved successfully";
                 myFunction(msg);
               }
            }
            }
              
          });
      }
      
   function myFunction(msg) {
    var x = document.getElementById("snackbar");
    x.className = "show";
    $("#snackbar").html('<div class="alert alert-success">'+msg+ '</div>');
    setTimeout(function(){ x.className = x.className.replace("show", "top"); }, 3000);
     }
</script>
<script>
   $(document).ready(function(){   
   $("#changepassbtn").click(function() {
     //var oldpass = $("#oldpass").val();
     var newpass = $("#newpass").val();
     var confpass = $("#confpass").val();
     $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>" + "user/changepass",
         data: {newpass:newpass,confpass:confpass},
         cache:false,
         success:function(htmldata) {
            $('#errorfield').html(htmldata);
         },
         error:function(){
           console.log('error');
         }
     });
   });
   $("input[name='locationSort']").click(function(){
     var location = $('#txtPlaces').val();
     if(location!=''){
       alert('You have already entered location');
       return false;
     }
   });
   
   });
</script>
<script>
   function getFilename(id)
   {
      var name = document.getElementById("file"+id).files[0].name;
      var filetype= $("#file"+id).val();
      var ext = filetype.split('.').pop();
      if(ext =="jpg" || ext =="jpeg" || ext =="png"){
        $("#setimgres"+id).html("<span style='color:red'>plese select file format(pdf/doc/docx)</span>");
        return false;
     } 
     else
     {
      $("#setimgres"+id).html("");
      var form_data = new FormData();
      form_data.append("file", document.getElementById('file'+id).files[0]);
      $.ajax({
        url:'<?php echo base_url('dashboard/imageUpload')?>',
        method:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success:function(imagedata)
        { 
          $("#imagedata"+id).val(imagedata);
          $("#file"+id).val('');
          $("#setimgres"+id).html(imagedata);
        }
      });
     }
   }
</script>
<script type="text/javascript">
   function PdfImagesend(id)
   {
     //alert('hi');
     var image= $("#imagedata"+id).val();
       $.ajax({
              'type' :'POST',
              'url' :"<?php echo base_url('dashboard/fileInsert') ?>",
              'data' :'imagedata='+image,
              'success':function(htmlres)
              {
   
                if(htmlres=='1')
                {
                 $("#exampleModalCenterdd"+id).modal('show');
                }
                if(htmlres=='2')
                {
                  $("#exampleModalCenterdd"+id).modal('show');
                }
              }
        });
    }
</script>
<script>
   function getcatsubcat(id)
   {
     var catid = $("#jobcategoryy").val();
     $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/getsubcategoryInner",
          data: {catid:catid},
          cache:false,
          success:function(htmldata){
   
            $("#subcatresss").html(htmldata);
              
          },
      });
   }
</script>

</body>
</html>

