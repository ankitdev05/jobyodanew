<?php include_once('header.php'); ?>
<style type="text/css">
   @import url('https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap');
   .filldetails input.form-control, 
   .profileformstop .filldetails select{
   box-shadow: none;
   height: 40px !important;
   padding: 7px 15px !important;
   font-family: Roboto;
   font-weight: 500;
   color: #000;
   margin: 0;
   width: 100%;
   float: none;
   }
   .clear{
   clear: both
   }
   .basetst p {
   font-size: 17px;
   margin: 3px 0 0 0;
   font-family: Roboto;
   color: #000;
   font-weight: 500;
   }
   .paneleityodas {
   border: 1px solid #ddd;
   box-shadow: none;
   border-radius: 5px;
   margin: 0 20px 20px 0;
   padding: 15px;
   }
   .showinfoarea.mypaneleditfsa {
   position: relative;
   float: left;
   width: 95%;
   box-shadow: none;
   padding: 20px;
   height: 200px;
   margin-bottom: 10px;
   border: 1px solid #ddd;
   border-radius: 8px;
   }
   .showinfoarea.mypaneleditfsa .paneleityodas {
   box-shadow: none;
   position: absolute;
   top: 20px;
   right: 0px;
   width: auto;
   }
   .showinfoarea p.designation {
   color: #1f1f1f;
   font-weight: 500;
   font-family: roboto;
   font-size: 15px;
   padding: 0 50px 0px 0;
   }
   .showinfoarea p {
   color: #949191;
   font-size: 14px;
   margin: 0;
   font-family: roboto;
   }
   .fullratings .checkflowrt label {
   font-size: 14px;
   padding: 5px 0;
   font-family: Roboto;
   }
   .addcall {
   cursor: pointer;
   font-family: Roboto;
   font-weight: 600;
   }
   .checkflowrt label {
   font-size: 14px;
   padding: 4px 0 0 2px;
   font-family: Roboto;
   font-weight: 500;
   }
   .custom-control-label::before {
   border: 1px solid #464646;
   }
   .ratingschecks.formratesweaks .centerratingdf {
   margin: 35px auto;
   width: 50%;
   padding: 10px;
   box-shadow: none;
   border: 1px solid #ddd;
   padding: 25px;
   border-radius: 10px;
   }
   .ratingschecks.formratesweaks .centerratingdf p {
   font-family: Roboto;
   font-weight: 500;
   color: #000;
   font-size: 14px;
   margin: 0 0 10px;
   }
   .ratingschecks.formratesweaks {
   float: left;
   margin: 10px auto 50px;
   background: #fff;
   border: 1px solid #ddd;
   padding: 10px;
   width: 100%;
   box-shadow: 0px 1px 6px #cacaca;
   border-radius: 15px;
   }
   .innerbglay .adminopnts {
   padding: 0;
   box-shadow: none;
   border-radius: 0;
   background-color: transparent;
   }
   .InfomationBox{
   background-color: #fff;
   padding: 25px;
   border-radius: 10px;
   margin: 0 0 30px 0;
   border: 1px solid #ddd;
   }
   .InfomationBox .editssections{
   padding: 0 0 20px 0;
   margin: 0 0 20px 0;
   }
   .InfomationBox .editssections.EditNone{
   padding: 0;
   border: none;
   margin: 0
   }
   .InfomationBox .showinfoarea {
   margin: 20px 10px 0 0;
   float: left;
   width: 24%;
   }
   .InfomationBox .showinfoarea .paneleityodas{
   margin: 0;
   padding: 10px;
   width: 100%;
   display: block;
   }
   .InfomationBox .showinfoarea .paneleityodas p.designation{ width: 70%; float: left; }
   .InfomationBox .showinfoarea .paneleityodas p{ float: right; }
   .ProfileUploadPic{
   position: relative;
   width: 150px;
   height: 150px;
   border: 5px solid #27aa60;
   border-radius: 50%;
   overflow: hidden;
   background-color: #000;
   margin: auto;
   }
   .ProfileUploadPic span{
   position: absolute;
   top: 0;
   left: 0;
   width: 40px;
   height: 40px;
   bottom: 0;
   right: 0;
   margin: auto;
   color: #fff;
   font-size: 35px;
   text-align: center;
   z-index: 8;
   cursor: pointer;
   }
   .ProfileUploadPic img{
   width: 100%;
   opacity: 0.7;
   }
   .ProfileUploadPic input{
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   border-radius: 50%;
   opacity: 0;
   cursor: pointer;
   }
   .showinfoarea.mypaneleditfsa .paneleityodas{
   width: auto;
   top: 5px;
   right: 5px;
   padding: 5px 10px;
   background-color: #fff;
   }
   .workexperience .basetst {
   width: 25%;
   }
   .basetst img{
   width: 30px;
   height: 30px;
   }
   .modal .filldetails input.form-control, 
   .modal .profileformstop .filldetails select{
   width: 100%;
   height: 45px !important;
   margin: 0 0 15px 0;
   }
   .modal .srchbtns{
   width: 150px;
   margin: 20px auto 0;
   }
   ul.multiselect-container.dropdown-menu{
   min-width: 300px;
   right: 0;
   left: auto !important;
   height: 350px;
   overflow: auto;
   z-index: 9;
   }
   ul.multiselect-container.dropdown-menu li .input-group{
   margin: 0;
   padding: 5px;
   }
   ul.multiselect-container.dropdown-menu li .input-group span{
   display: none;
   }
   ul.multiselect-container.dropdown-menu li .input-group input{
   width: 100%;
   flex: inherit;
   }
   ul.multiselect-container.dropdown-menu li{}
   ul.multiselect-container.dropdown-menu li a{
   display: block;
   color: #000;
   }
   ul.multiselect-container.dropdown-menu li a:focus{
   box-shadow: none;
   border:none;
   outline: none;
   }
   ul.multiselect-container.dropdown-menu li a:hover{
   text-decoration: none;
   }
   ul.multiselect-container.dropdown-menu li a label{
   height: auto;
   padding: 10px 15px;
   display: block;
   outline: 0;
   font-family: Roboto;
   font-size: 14px;
   font-weight: 400;
   }
   ul.multiselect-container.dropdown-menu li a label input{
   margin: 0 10px 0 0
   }
   .InfomationBox .editssections .addcall .multiselect.dropdown-toggle {
   background-color: #f9f9f9!important;
   height: 40px!important;
   width: 100%!important;
   border: 1px solid #ddd;
   box-shadow: none;
   font-size: 14px;
   font-family: Roboto;
   font-weight: 500;
   border-radius: 5px;
   padding: 0 17px;
   letter-spacing: 0.3px;
   }
   .resumeurl{
      float: right;
    width: 62%;
    text-align: right;
    font-size: 12px;
    padding: 5px 0;
   }
.viewiconsd{width: 110px!important;}
</style>
<div class="managerpart">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-9 expanddiv profilemyyods">
            <div class="innerbglay">
               <form method="post" action="<?php echo base_url();?>user/userDetailUpdate" enctype="multipart/form-data">
                  <div class="mainheadings-yoda">
                     <?php 
                        if(!empty($this->session->flashdata('msg')))
                        {
                         echo $this->session->flashdata('msg');
                        } 
                        ?> 
                     <!-- <button type="submit" class="prosave" style="cursor: pointer;color: #f6ab27;">Save</button> -->
                  </div>
                  <div class="adminopnts">
                     <div class="InfomationBox">
                        <div class="addresumes editssections">
                           <div class="addresumeare">
                              <div class="basetst">
                                 <img src="<?php echo base_url();?>webfiles/img/personal.png">
                                 <!-- <?php echo $this->session->tempdata('updatemsg'); ?> -->
                                 <p>Personal Information</p>
                              </div>
                           </div>
                        </div>
                        <div class="basicdetailsect editssections EditNone">
                           <div class="row">
                              <div class="col-sm-9">
                                 <div class="profileformstop">
                                    <div class="filldetails" style=" margin: 0 0 0;">
                                       <div class="row">
                                          <div class="form-group col-sm-6">
                                             <input type="text" class="form-control" name="name" value="<?php echo $profileDetail['name'];?>" placeholder="Name">
                                          </div>
                                          <div class="form-group col-sm-6">
                                             <input type="text" id="proid" class="form-control" name="dob" value="<?php if($profileDetail['dob']){echo $profileDetail['dob'];}?>" placeholder="Date Of Birth" autocomplete="off">
                                          </div>
                                          <div class="form-group col-sm-6">
                                             <select name="country_code" placeholder=" Select Phone Code" class="">
                                                <option value="">Select Phone Code</option>
                                                <?php
                                                   foreach($phonecodes as $phonecode) {
                                                   ?>
                                                <option <?php if($phonecode['phonecode']==$profileDetail['country_code']){ echo "selected"; }?> value="+<?php echo $phonecode['phonecode'];?>"> <?php echo $phonecode['name'];?> - <?php echo $phonecode['phonecode'];?> </option>
                                                <?php
                                                   }
                                                   ?>
                                             </select>
                                          </div>
                                          <div class="form-group col-sm-6">
                                             <input type="text" class="form-control" value="<?php echo $profileDetail['phone'];?>" name="phone" maxlength="10" id="phone12" placeholder="Contact Number" >
                                          </div>
                                          <div class="form-group col-sm-6">
                                             <input type="text" class="form-control" value="<?php echo $usersess['email'];?>" name="email" placeholder="Email" readonly="">
                                          </div>
                                          <div class="form-group col-sm-6">
                                             <input type="text" class="form-control" value="<?php if($profileDetail['location']){echo $profileDetail['location'];}?>" name="location" placeholder="Location" id="txtPlaces">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <div class="ProfileUploadPic">
                                    <span><i class="fas fa-camera"></i></span>
                                    <input type="file" name="profilePic" id="imgInp">
                                    <?php
                                       if(!empty($profileDetail['profilePic'])){
                                       ?>
                                    <img id="blah" src="<?php echo $profileDetail['profilePic'];?>">
                                    <?php
                                       } else {
                                       ?>
                                    <img src="<?php echo base_url().'webfiles/';?>img/profilepic.png">
                                    <?php
                                       }
                                       ?>
                                    <p>Upload Photo</p>
                                 </div>
                              </div>
                           </div>
                           <div class="profileformstop">
                              <div class="filldetails" style=" margin: 0 0 0;">
                                 <div class="row">
                                    <div class="form-group col-sm-4">
                                       <select name="exp_year" placeholder="Years of Experience" class="">
                                          <option value="">Year</option>
                                          <?php for($i=0;$i<=20;$i++) {?>
                                          <option value="<?php echo $i;?>"
                                             <?php if($profileDetail['exp_year'])
                                                {
                                                if($profileDetail['exp_year']==$i)
                                                {
                                                   echo "selected";
                                                }
                                                }
                                                ?>
                                             ><?php echo $i.' '.'years';?>
                                          </option>
                                          <?php }?>
                                       </select>
                                    </div>
                                    <div class="form-group col-sm-4">
                                       <select name="exp_month" placeholder="Months of Experience" class="texts">
                                          <option value="">Month</option>
                                          <?php for($i=0;$i<=12;$i++) {?>
                                          <option value="<?php echo $i;?>"
                                             <?php if($profileDetail['exp_month'])
                                                {
                                                if($profileDetail['exp_month']==$i)
                                                {
                                                   echo "selected";
                                                }
                                                }
                                                ?>
                                             ><?php echo $i.' '.'months';?></option>
                                          <?php }?>
                                       </select>
                                    </div>
                                    <div class="form-group col-sm-4">
                                       <select name="education" id="education" placeholder="Attainment" >
                                          <option value="">Select Education</option>
                                          <option value="High School Graduate" <?php if($profileDetail['education']){ if($profileDetail['education']=="High School Graduate"){ echo "selected"; } } ?> >High School Graduate</option>
                                          <option value="Vocational" <?php if($profileDetail['education']){ if($profileDetail['education']=="Vocational"){ echo "selected"; } } ?>>Vocational</option>
                                          <option value="Undergraduate" <?php if($profileDetail['education']){ if($profileDetail['education']=="Undergraduate"){ echo "selected"; } } ?>>Undergraduate</option>
                                          <option value="Associate Degree" <?php if($profileDetail['education']){ if($profileDetail['education']=="Associate Degree"){ echo "selected"; } } ?>>Associate Degree</option>
                                          <option value="College Graduate" <?php if($profileDetail['education']){ if($profileDetail['education']=="College Graduate"){ echo "selected"; } } ?>>College Graduate</option>
                                          <option value="Post Graduate" <?php if($profileDetail['education']){ if($profileDetail['education']=="Post Graduate"){ echo "selected"; } } ?>>Post Graduate</option>
                                       </select>
                                    </div>
                                    <div class="form-group col-sm-4">
                                       <select name="nationality" placeholder="Nationality" class="texts" >
                                          <option value="">Select Nationality</option>
                                          <?php
                                             foreach($nations as $nation) {
                                             ?>
                                          <option <?php if(!empty($profileDetail['nationality'])){ if($profileDetail['nationality']==$nation['nationality']) { echo "selected"; } } ?>  value="<?php echo $nation['nationality'];?>" > <?php echo $nation['nationality'];?> </option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </div>
                                    <div class="form-group col-sm-4">
                                       <select name="superpower" placeholder="Nationality" class="">
                                          <option value="">Select Superpower</option>
                                          <?php
                                             foreach($powers as $power) {
                                             ?>
                                          <option <?php if(!empty($profileDetail['superpower'])){ if($profileDetail['superpower']==$power['power']) { echo "selected"; } } ?>  value="<?php echo $power['power'];?>" > <?php echo $power['power'];?> </option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </div>
                                    <div class="form-group col-sm-4">
                                       <input type="text" class="form-control" value="<?php if($profileDetail['designation']){echo $profileDetail['designation'];}?>" name="designation" placeholder="Designation">
                                    </div>
                                    <div class="form-group col-sm-4">
                                       <input type="text" class="form-control" value="<?php if($profileDetail['current_salary']){echo $profileDetail['current_salary'];}?>" name="current_salary" placeholder="Current Salary" id="salary">
                                    </div>
                                    <div class="form-group col-sm-4">
                                       <select name="state" placeholder="State" class="changestate" class="form-control">
                                          <option value="">Select State</option>
                                          <?php
                                             foreach($states as $getstates) {
                                             ?>
                                          <option <?php if(!empty($profileDetail['state'])){ if($profileDetail['state']==$getstates['state']) { echo "selected"; } } ?>  value="<?php echo $getstates['state'];?>" > <?php echo $getstates['state'];?> </option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </div>
                                    <div class="form-group col-sm-4">
                                       <select name="city" placeholder="City/Town" class="changecity" class="form-control">
                                          <option value="<?php if(!empty($profileDetail['city'])) {  echo $profileDetail['city']; } ?>" selected> <?php if(!empty($profileDetail['city'])) {  echo $profileDetail['city']; } ?> </option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                     </div>
                     <input type="hidden" value="<?php echo $profileDetail['token'];?>" name="token">
                     <!-- </form> -->
                     <div class="InfomationBox">
                        <div class="addresumes editssections EditNone">
                           <div class="addresumeare">
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/resume.png">
                                 <p>Add Resume</p>
                              </div>
                              <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter8">
                                 <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png" title="Add Resume">
                              </div>
                              <?php if(!empty($checkResume[0]['resume'])){ ?>
                              <span class="resumeurl"><?php echo  $checkResume[0]['resume']?></span> <a target="blank"  class="viewiconsd" href="<?php echo  $checkResume[0]['resume']?>">View Resume</a>
                              <?php } ?>
                           </div>
                        </div>
                        <div class="clear"></div>
                     </div>
                     <span id="error_msg"></span>
                     <div class="InfomationBox">
                        <div class="addresumes editssections EditNone">
                           <div class="addresumeare">
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/support.png">
                                 <p>Top Client Supported</p>
                              </div>
                              <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter818" title="Top Client Supported">
                                 <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png">
                              </div>
                           </div>
                           <?php
                              if(!empty(array_filter($clientData))) {
                               $xx=0;
                              foreach($clientData as $client) {
                              ?>
                           <div class="showinfoarea">
                              <div class="paneleityodas">
                                 <p class="designation" id="<?php echo $xx;?>"><?php echo $client['clients']; ?></p>
                                 <p>
                                    <a href="javascript:void(0)" class="fa fa-edit" data-toggle="modal" data-target="#clientmodel<?php echo $xx;?>"></a> | 
                                    <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $client['id'] ?>','user_topclients')"></a>
                                 </p>
                              </div>
                              <div class="clear"></div>
                           </div>
                           <div class="modal fade" id="clientmodel<?php echo $xx;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">×</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="formmidaress modpassfull">
                                          <div class="filldetails">
                                             <p>Top Client Supported</p>
                                             <span id="respclient<?php echo $xx;?>"></span>
                                             <input type="text" name="clients" id="cli<?php echo $xx;?>" value="<?php echo $client['clients']; ?>" id="clients" class="form-control" placeholder="Top Client">
                                             <input type="hidden" id="clients<?php echo $xx;?>" value="<?php echo $client['id'] ?>"> 
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <button type="button" class="updt" id="clientbtnn" onclick="getclientval('<?php echo $xx;?>')">Update</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              $xx++;
                              }
                              }
                              ?> 
                        </div>
                        <div class="clear"></div>
                     </div>
                     <div class="InfomationBox">
                        <div class="addresumes editssections EditNone">
                           <div class="addresumeare">
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/Languages.png">
                                 <p>Languages Spoken</p>
                              </div>
                              <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter819">
                                 <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png" title="Languages Spoken"> 
                              </div>
                           </div>
                           <?php  
                              if(!empty(array_filter($languageData))) {
                                $yy=0;
                              foreach($languageData as $languageDataS) {
                              ?>
                           <div class="showinfoarea">
                              <div class="paneleityodas">
                                 <p class="designation" id="<?php echo $yy;?>"><?php echo $languageDataS['name']; ?></p>
                                 <p>
                                    <a href="javascript:void(0)" class="fa fa-edit" data-toggle="modal" data-target="#langmodel<?php echo $yy;?>"></a> | 
                                    <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $languageDataS['id'] ?>','user_language')"></a>
                                 </p>
                              </div>
                              <div class="clear"></div>
                           </div>
                           <div class="modal fade" id="langmodel<?php echo $yy;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">×</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="formmidaress modpassfull">
                                          <div class="filldetails">
                                             <!-- <form method="post" action="<?php //echo base_url();?>user/resumeUpload" enctype="multipart/form-data"> -->
                                             <p>Languages Spoken</p>
                                             <span id="resplang<?php echo $yy;?>"></span>
                                             <select name="languages" id="languagees<?php echo $yy;?>" class="form-control" placeholder="Select Language" style="width:100%">
                                                <option value="">Select Language</option>
                                                <?php
                                                   if(!empty($languagelists))
                                                   {
                                                    foreach($languagelists as $languagelist)
                                                    {
                                                   ?>
                                                <option value="<?php echo $languagelist['id']?>" 
                                                   <?php if($languageDataS['lang_id']==$languagelist['id']){echo "selected";}?>
                                                   ><?php echo $languagelist['name']?></option>
                                                <?php     
                                                   }}
                                                   ?>
                                             </select>
                                             <input type="hidden" id="languageDataS<?php echo $yy;?>" value="<?php echo $languageDataS['id'] ?>">
                                             <div class="form-group"></div>
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <button type="button" class="updt" id="clientbtnnnn" onclick="getlangval('<?php echo $yy;?>')">Update</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              $yy++;
                                  }
                               }
                              ?>
                        </div>
                        <div class="clear"></div>
                     </div>
                     <div class="InfomationBox">
                        <div class="addresumes editssections EditNone">
                           <div class="addresumeare">
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/skills.png">
                                 <p>Add Skills</p>
                              </div>
                              <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter820">
                                 <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png" title="Add Skills">
                              </div>
                           </div>
                           <?php
                              if(!empty(array_filter($skillData))) {
                                 $zz=0;
                                foreach($skillData as $skillDataS) {
                              ?>
                           <div class="showinfoarea">
                              <div class="paneleityodas">
                                 <p class="designation" id="<?php if(!empty($yy)){ echo $yy; } ?>"><?php echo $skillDataS['skills']; ?></p>
                                 <p>
                                    <a href="javascript:void(0)" class="fa fa-edit" data-toggle="modal" data-target="#skillmodel<?php echo $zz;?>"></a> | 
                                    <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $skillDataS['id'] ?>','user_skills')"></a>
                                 </p>
                              </div>
                              <div class="clear"></div>
                           </div>
                           <div class="modal fade" id="skillmodel<?php echo $zz;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">×</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="formmidaress modpassfull">
                                          <div class="filldetails">
                                             <!-- <form method="post" action="<?php //echo base_url();?>user/resumeUpload" enctype="multipart/form-data"> -->
                                             <p>Skills</p>
                                             <span id="respskill<?php echo $zz;?>"></span>
                                             <input type="text" id="skill<?php  echo $zz;?>" value="<?php echo $skillDataS['skills']; ?>" class="form-control">
                                             <input type="hidden" id="skillDataS<?php echo $zz;?>" value="<?php echo $skillDataS['id'] ?>">
                                             <div class="form-group"></div>
                                             <div class="statsusdd">
                                                <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                                <button type="button" class="updt" id="clientbtnnnn" onclick="getskilval('<?php echo $zz;?>')">Update</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              $zz++;
                                  }
                               }
                              ?> 
                        </div>
                        <div class="clear"></div>
                     </div>
                     
                     <div class="modal fade" id="exampleModalCenter818" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <!-- <form method="post" action="<?php //echo base_url();?>user/resumeUpload" enctype="multipart/form-data"> -->
                                       <p>Top Clients Supported</p>
                                       <span id="respclient"></span>
                                       <input type="text" name="clients" id="clients" class="form-control" placeholder="Please enter Top Client">
                                       <div class="statsusdd">
                                          <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                          <button type="button" class="updt" id="clientbtn">Add</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="exampleModalCenter819" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <!-- <form method="post" action="<?php //echo base_url();?>user/resumeUpload" enctype="multipart/form-data"> -->
                                       <p>Languages Spoken</p>
                                       <span id="resplanguage"></span>
                                       <div class="form-group">
                                          <select name="languages" id="languagees" class="form-control" placeholder="Select Language" style="width:100%">
                                             <option value="">Select Language</option>
                                             <?php
                                                if(!empty($languagelists))
                                                {
                                                 foreach($languagelists as $languagelist)
                                                 {
                                                ?>
                                             <option value="<?php echo $languagelist['id']?>"><?php echo $languagelist['name']?></option>
                                             <?php     
                                                }}
                                                ?>
                                          </select>
                                       </div>
                                       <div class="statsusdd">
                                          <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                          <button type="button" class="updt" id="languagebtn">Add</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="modal fade" id="exampleModalCenter820" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="formmidaress modpassfull">
                                    <div class="filldetails">
                                       <!-- <form method="post" action="<?php //echo base_url();?>user/resumeUpload" enctype="multipart/form-data"> -->
                                       <p>Skills</p>
                                       <span id="respskill"></span>
                                       <div class="form-group">
                                          <input type="text" name="skills" id="skills" placeholder="Please enter skills" class="form-control">
                                       </div>
                                       <div class="statsusdd">
                                          <p class="norm" class="close" data-dismiss="modal" aria-label="Close">Cancel</p>
                                          <button type="button" class="updt" id="skillsbtn">Add</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="InfomationBox">
                        <div class="workexperience addresumes editssections EditNone">
                           <div class="addresumeare">
                              <div class="basetst">
                                 <p>Work Experience</p>
                              </div>
                              <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter3">
                                 <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png" title="Work Experience">
                              </div>
                           </div>
                           <?php
                              if(!empty(array_filter($workData))) { 
                              foreach($workData as $work) {
                                    //print_r($work);
                              ?> 
                           <div class="showinfoarea mypaneleditfsa">
                              <p class="designation"><?php echo $work['title']; ?></p>
                              <p class="organsname"><?php echo $work['company']; ?></p>
                              <p class="datedares"><?php echo $work['from']; ?> - <?php if($work['to']=='') { echo "Present"; }else{ echo $work['to'];} ?></p>
                              <p class="headlineentry">
                                 <?php echo $work['desc']; ?>
                              </p>
                              <div class="paneleityodas">
                                 <p>
                                    <a href="javascript:void(0)" class="fa fa-edit" onclick="return gatval('<?php echo $work['id'] ?>')" data-toggle="modal" data-target="#mymodell"></a> | 
                                    <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $work['id'] ?>','user_work_experience')"></a>
                                 </p>
                              </div>
                           </div>
                           <?php
                              }
                              }
                              ?>
                        </div>
                        <div class="clear"></div>
                     </div>

                     <div class="InfomationBox">
                        <div class="workexperience addresumes editssections EditNone">
                           <div class="addresumeare">
                              <div class="basetst">
                                 <img src="<?php echo base_url().'webfiles/';?>img/education.png">
                                 <p>Education</p>
                              </div>
                              <div class="addcall" data-toggle="modal" data-target="#exampleModalCenter2">
                                 <img src="<?php echo base_url().'webfiles/';?>img/plusadd.png" title="Education">
                              </div>
                           </div>
                           <div class="row">
                              <?php
                                 if(!empty(array_filter($eduData))) {
                                 foreach($eduData as $edu) {
                                 ?>
                              <div class="col-sm-4">
                                 <div class="showinfoarea mypaneleditfsa" style="width: 100%">
                                    <p class="designation"><?php echo $edu['attainment']; ?></p>
                                    <p class="organsname"><?php echo $edu['degree']; ?></p>
                                    <p class="datedares"><?php echo $edu['from']; ?> - <?php echo $edu['to']; ?></p>
                                    <p class="headlineentry">
                                       <?php echo $edu['university']; ?>
                                    </p>
                                    <div class="paneleityodas">
                                       <p>
                                          <a href="javascript:void(0)" class="fa fa-edit" onclick="return gatvaledu('<?php echo $edu['id'] ?>')" data-toggle="modal" data-target="#exampleModalCenter25"></a> | 
                                          <a href="javascript:void(0)" class="fa fa-trash" onclick="return deleteRecord('<?php echo $edu['id'] ?>','user_education')"></a>
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <?php
                                 }
                                 }
                                 ?>
                           </div>
                        </div>
                        <div class="clear"></div>
                     </div>

                     <div class="InfomationBox">
                        <div class="addresumes editssections EditNone">
                           <div class="addresumeare" style="display: block;">
                              <div class="basetst" style="float: left;">
                                 <img src="<?php echo base_url().'webfiles/';?>img/Jobs.png">
                                 <p>Add Jobs I'm Interested in</p>
                              </div>
                              <div class="addcall" style="float: right;">
                                 <select class="multiselect-ui" id="framework" name="intrested[]" multiple style="margin-left:0px;width: 100%; margin-bottom: 10px;">
                                    <option value="">Select Interested In</option>
                                    <?php
                                       foreach($intrestedin as $getintrested) {
                                        if($getintrested['subcategory'] == "ALL" || $getintrested['subcategory'] == 'all') {} else {
                                       ?>
                                    <option <?php if(!empty($profileDetail['jobsInterested'])){ if(strpos($profileDetail['jobsInterested'], $getintrested['subcategory']) !== false ) { echo "selected"; } } ?>  value="<?php echo $getintrested['subcategory'];?>" > <?php echo $getintrested['subcategory'];?> </option>
                                    <?php
                                       }
                                       }
                                       ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                     </div>

                     <div class="mainheadings-yoda"> 
                        <input type="hidden" value="<?php echo $profileDetail['token'];?>" name="token">
                     </div>
                     
                     <div class="InfomationBox">
                        <div class="ratingsexperience workexperience addresumes editssections EditNone">
                           <div class="basetst">
                              <p style="font-size: 18px; margin-bottom: 20px;">Self Assesment</p>
                           </div>
                           <div class="showinfoarea" style="width: 100%">
                              <p class="designation">Please rate your expertise on each of the competencies below :</p>
                              <div class="ratingsrange">
                                 <p>Rating 1 - Low</p>
                                 <p>Rating 5 - High</p>
                              </div>
                              <div class="ratingschecks">
                                 <p class="designation">Verbal Communication Skills :</p>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="1" id="customRadio" name="verbal" <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 1){echo "checked";;}}?>>
                                       <label class="custom-control-label" for="customRadio">1</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="2" id="customRadio1" name="verbal" <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 2){echo "checked";;}}?>>
                                       <label class="custom-control-label" for="customRadio1">2</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="3" id="customRadio2" name="verbal"  <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 3){echo "checked";;}}?>>
                                       <label class="custom-control-label" for="customRadio2">3</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="4" id="customRadio3" name="verbal" <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 4){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio3">4</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="5" id="customRadio4" name="verbal" <?php if(!empty($assessData[0]['verbal'])){if($assessData[0]['verbal'] == 5){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio4">5</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="ratingschecks">
                                 <p class="designation">Written Communication Skills :</p>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="1" id="customRadio5" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 1){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio5">1</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="2" id="customRadio6" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 2){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio6">2</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="3" id="customRadio7" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 3){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio7">3</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="4" id="customRadio8" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 4){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio8">4</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="5" id="customRadio9" name="Written" <?php if(!empty($assessData[0]['written'])){if($assessData[0]['written'] == 5){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio9">5</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="ratingschecks">
                                 <p class="designation">Listening Skills :</p>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="1" id="customRadio10" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 1){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio10">1</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="2" id="customRadio11" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 2){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio11">2</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="3" id="customRadio12" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 3){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio12">3</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="4" id="customRadio13" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 4){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio13">4</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="5" id="customRadio14" name="Listening" <?php if(!empty($assessData[0]['listening'])){if($assessData[0]['listening'] == 5){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio14">5</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="ratingschecks">
                                 <p class="designation">Problem Solving Skills :</p>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="1" id="customRadio15" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 1){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio15">1</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="2" id="customRadio16" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 2){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio16">2</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="3" id="customRadio17" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 3){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio17">3</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="4" id="customRadio18" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 4){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio18">4</label>
                                    </div>
                                 </div>
                                 <div class="checkflowrt">
                                    <div class="custom-control custom-radio">
                                       <input type="radio" class="custom-control-input" value="5" id="customRadio19" name="Problem" <?php if(!empty($assessData[0]['problem'])){if($assessData[0]['problem'] == 5){echo "checked";}}?>>
                                       <label class="custom-control-label" for="customRadio19">5</label>
                                    </div>
                                 </div>
                              </div>
                              <?php 
                                 if(!empty($expertData[0]['expert']))
                                 {
                                 $expert_id= explode(",",$expertData[0]['expert']); 
                                 }
                                   ?>
                              <div class="ratingschecks fullratings">
                                 <p class="designation">In which industry would you consider yourself an expert?</p>
                                 <div class="setsboxsone">
                                    <div class="checkflowrt">
                                       <div class="custom-control custom-radio">
                                          <input type="checkbox" class="custom-control-input" value="1" id="customRadio20" name="expert[]" <?php if(!empty($expert_id)){if(in_array('1',$expert_id)){echo "checked";}}?>>
                                          <label class="custom-control-label" for="customRadio20"><img src="<?php echo base_url().'webfiles/';?>img/car.png">Automotive</label>
                                       </div>
                                    </div>
                                    <div class="checkflowrt">
                                       <div class="custom-control custom-radio">
                                          <input type="checkbox" class="custom-control-input" value="2" id="customRadio21" name="expert[]" <?php if(!empty($expert_id)){if(in_array('2',$expert_id)){echo "checked";}}?>>
                                          <label class="custom-control-label" for="customRadio21"><img src="<?php echo base_url().'webfiles/';?>img/sun.png">Technology</label>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="setsboxstwo">
                                    <div class="checkflowrt">
                                       <div class="custom-control custom-radio">
                                          <input type="checkbox" class="custom-control-input" value="3" id="customRadio22" name="expert[]" <?php if(!empty($expert_id)){if(in_array('3',$expert_id)){echo "checked";}}?>>
                                          <label class="custom-control-label" for="customRadio22"><img src="<?php echo base_url().'webfiles/';?>img/home.png">Banking and Financial Service</label>
                                       </div>
                                    </div>
                                    <div class="checkflowrt">
                                       <div class="custom-control custom-radio">
                                          <input type="checkbox" class="custom-control-input" value="4" id="customRadio23" name="expert[]" <?php if(!empty($expert_id)){if(in_array('4',$expert_id)){echo "checked";}}?>>
                                          <label class="custom-control-label" for="customRadio23"><img src="<?php echo base_url().'webfiles/';?>img/people.png">Media & Communications</label>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="setsboxsthree">
                                    <div class="checkflowrt">
                                       <div class="custom-control custom-radio">
                                          <input type="checkbox" class="custom-control-input" value="5" id="customRadio24" name="expert[]" <?php if(!empty($expert_id)){if(in_array('5',$expert_id)){echo "checked";}}?>>
                                          <label class="custom-control-label" for="customRadio24"><img src="<?php echo base_url().'webfiles/';?>img/chip.png">Consumer Electronics</label>
                                       </div>
                                    </div>
                                    <div class="checkflowrt">
                                       <div class="custom-control custom-radio">
                                          <input type="checkbox" class="custom-control-input" value="6" id="customRadio25" name="expert[]"  <?php if(!empty($expert_id)){if(in_array('6',$expert_id)){echo "checked";}}?>>
                                          <label class="custom-control-label" for="customRadio25"><img src="<?php echo base_url().'webfiles/';?>img/cart.png">Retail & ECommerce</label>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="setsboxsfour">
                                    <div class="checkflowrt">
                                       <div class="custom-control custom-radio">
                                          <input type="checkbox" class="custom-control-input" value="7" id="customRadio26" name="expert[]" <?php if(!empty($expert_id)){if(in_array('7',$expert_id)){echo "checked";}}?>>
                                          <label class="custom-control-label" for="customRadio26"><img src="<?php echo base_url().'webfiles/';?>img/heartplus.png">Healthcare & Pharmaceutical</label>
                                       </div>
                                    </div>
                                    <div class="checkflowrt">
                                       <div class="custom-control custom-radio">
                                          <input type="checkbox" class="custom-control-input" value="8" id="customRadio27" name="expert[]" <?php if(!empty($expert_id)){if(in_array('8',$expert_id)){echo "checked";}}?>>
                                          <label class="custom-control-label" for="customRadio27"><img src="<?php echo base_url().'webfiles/';?>img/flight.png">Travel, Transportation & Tourism</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                     </div>
                  </div>
                  <!-- </form> -->
                  <div class="ratingschecks formratesweaks">
                     <div class="centerratingdf">
                        <!-- <form method="post" action="<?php echo base_url();?>user/addprofilemore"> -->
                        <p>What are your strengths?</p>
                        <textarea name="strength" placeholder="Write here..." class="form-control" width="100px;"><?php if(!empty($strengthdata[0]['strength'])){echo $strengthdata[0]['strength'];}?></textarea>
                        <p>What are your weaknesses?</p>
                        <textarea name="weakness" placeholder="Write here...." class="form-control"><?php if(!empty($strengthdata[0]['weakness'])){echo $strengthdata[0]['weakness'];}?></textarea>
                        <p>What are your 3 biggest achievements?</p>
                        <textarea name="achievements" placeholder="Write here...." class="form-control"><?php if(!empty($strengthdata[0]['achievements'])){echo $strengthdata[0]['achievements'];}?></textarea>
                        <button type="submit" class="srchbtns">Save</button>
               </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <span id="errorfield"></span>
         <div class="modal-body">
            <div class="formmidaress modpassfull">
               <div class="filldetails">
                  <!-- <form method="post" action="<?php echo base_url('user/changepass') ?>">-->
                  <!--<div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Enter Old Password" name="oldpass" id="oldpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                     </div>-->
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Enter New Password" name="newpass" id="newpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <div class="forminputspswd">
                     <input type="password" class="form-control" placeholder="Confirm New Password" name="confpass" id="confpass">
                     <img src="<?php echo base_url().'webfiles/';?>img/keypass.png">
                  </div>
                  <button type="button" id="changepassbtn" class="srchbtns">Change</button>
                  <!-- </form>-->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="workexperience addresumes editssections">
               <p>Add Education</p>
               <div class="formmidaress modpassfull">
                  <div class="filldetails">
                     <form method="post" action="<?php echo base_url();?>user/education">
                        <div class="forminputspswd">
                           <input type="text" name="university" class="form-control" placeholder="University/Institute">
                        </div>
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <select name="attainment" id="attainment" class="form-control" placeholder="Attainment" style="width:100%">
                                 <option value="">Choices for Educational Attainment</option>
                                 <option value="High School Graduate" >High School Graduate</option>
                                 <option value="Vocational" >Vocational</option>
                                 <option value="Undergraduate" >Undergraduate</option>
                                 <option value="Associate Degree" >Associate Degree</option>
                                 <option value="College Graduate" >College Graduate</option>
                                 <option value="Post Graduate" >Post Graduate</option>
                              </select>
                              <!-- <input type="text" name="attainment" class="form-control" placeholder="Highest Attainment"> -->
                           </div>
                           <div class="forminputspswd">
                              <input type="text" name="degree" class="form-control" placeholder="Degree/Course">
                           </div>
                        </div>
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <p>From</p>
                              <input type='text' name="from" class="form-control" id='datepicker11' autocomplete="off"/> 
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                           <div class="forminputspswd">
                              <p>To</p>
                              <input type='text' name="to" class="form-control" id='datepicker12' autocomplete="off"/>
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="hidden" name="id" value="">
                           <button type="submit" class="srchbtns">Save</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="workexperience addresumes editssections">
               <p>Add Experience</p>
               <div class="formmidaress modpassfull">
                  <div class="filldetails">
                     <form method="post" action="<?php echo base_url();?>user/workExperience">
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <input type="text" name="company" class="form-control" placeholder="Company Name">
                           </div>
                           <div class="forminputspswd">
                              <input type="text" name="title" class="form-control" placeholder="Job Title">
                           </div>
                        </div>
                        <div class="onewayfields">
                           <select name="joblevel" id="testts" class="form-control forminputspswd" placeholder="Select Level" style="margin-left: 0px;">
                              <option value="">Select Level</option>
                              <?php
                                 if($levellist) {
                                    foreach($levellist as $levellists) {
                                 ?>
                              <option value="<?php echo $levellists['id'];?>"> <?php echo $levellists['level'];?> </option>
                              <?php
                                 }
                                 }
                                 ?>
                           </select>
                           <!--  <textarea class="form-control" name="desc" placeholder="Add Description"></textarea> -->
                           <select name="category" id="slctsubcat" class="form-control forminputspswd" placeholder="Select Category" style="margin-left: 0px;" onchange="getSubcat()">
                              <option value="">Select Category</option>
                              <?php
                                 if($categorylist) {
                                    foreach($categorylist as $categorylists) {
                                 ?>
                              <option value="<?php echo $categorylists['id'];?>"> <?php echo $categorylists['category'];?> </option>
                              <?php
                                 }
                                 }
                                 ?>
                           </select>
                        </div>
                        <div class="onewayfields">
                           <select name="subcategory" id="subcatres" class="form-control forminputspswd" placeholder="Select Subcategory" style="margin-left: 0px;">
                              <option value="">Select Subcategory</option>
                           </select>
                        </div>
                        <div class="onewayfields">
                           <p class="blachd">Currently Working on this role  <input type ="checkbox" value="1" id="checkwid" name="regularcheck" class="check_status" onchange="valueChanged()"></p>
                           <div class="forminputspswd">
                              <p>From</p>
                              <!--   <input type="text" class="form-control" id="datepicker"> -->
                              <input type='text' name="from" autocomplete="off" class="form-control" id="datepicker" /> 
                              <i class="far fa-calendar-alt fieldicons" placeholder="Working From.."></i>
                           </div>
                           <span id="rgst" style="float:left;"></span>
                           <div class="forminputspswd" id="statusTOdate">
                              <p>To</p>
                              <input type='text' name="to" class="form-control" autocomplete="off" id="datepicker1" />
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <textarea class="form-control" name="desc" placeholder="Add Description"></textarea> 
                        </div>
                        <div class="forminputspswd">
                           <input type="hidden" name="id" value="">
                           <button type="submit" class="srchbtns">Save</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="workexperience addresumes editssections">
               <p>Add Education</p>
               <div class="formmidaress modpassfull">
                  <div class="filldetails">
                     <form method="post" action="<?php echo base_url();?>user/education">
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <input type="text" name="attainment" class="form-control" placeholder="Highest Attainment">
                           </div>
                           <div class="forminputspswd">
                              <input type="text" name="degree" class="form-control" placeholder="Degree/Course">
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="text" name="university" class="form-control" placeholder="University/Institute">
                        </div>
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <p>From</p>
                              <input type='text' name="from" class="form-control" id='datepicker11' autocomplete="off"/> 
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                           <div class="forminputspswd">
                              <p>To</p>
                              <input type='text' name="to" class="form-control" id='datepicker12' autocomplete="off"/>
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="hidden" name="id" value="">
                           <button type="submit" class="srchbtns">Save</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="exampleModalCenter25" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="workexperience addresumes editssections">
               <p>Add Education</p>
               <div class="formmidaress modpassfull">
                  <div class="filldetails">
                     <form method="post" action="<?php echo base_url();?>user/education">
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <input type="text" name="attainment" class="form-control" id="attainment" placeholder="Highest Attainment">
                           </div>
                           <div class="forminputspswd">
                              <input type="text" name="degree" class="form-control" placeholder="Degree/Course" id="degree">
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="text" name="university" class="form-control" placeholder="University/Institute" id="university">
                        </div>
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <p>From</p>
                              <input type='text' name="from" class="form-control" id='datepicker111' autocomplete="off" /> 
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                           <div class="forminputspswd">
                              <p>To</p>
                              <input type='text' name="to" class="form-control" id='datepicker122' autocomplete="off" />
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="hidden" name="id" id="setid">
                           <button type="submit" class="srchbtns">Update</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="mymodell" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered addeducation" role="document">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   <div class="modal-body">
      <div class="workexperience addresumes editssections">
         <p>Add Experience</p>
         <div class="formmidaress modpassfull">
            <div class="filldetails">
               <form method="post" action="<?php echo base_url('user/workExperience')?>">
                  <div class="onewayfields">
                     <div class="forminputspswd">
                        <input type="text" name="company" id="camp_name" class="form-control" placeholder="Company Name">
                     </div>
                     <div class="forminputspswd">
                        <input type="text" name="title" id="title" class="form-control" placeholder="Job Title">
                     </div>
                  </div>
                  <div class="onewayfields">
                     <select name="joblevel" id="slct" class="form-control forminputspswd" placeholder="Select Level" style="margin-left: 0px;">
                        <option value="">Select Level</option>
                        <?php
                           if($levellist) {
                              foreach($levellist as $levellists) {
                           ?>
                        <option value="<?php echo $levellists['id'];?>"> <?php echo $levellists['level'];?> </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                     <select name="category" id="slctsubcatt" class="form-control forminputspswd" placeholder="Select Category" style="margin-left: 0px;" onchange="getSubcatt()">
                        <option value="">Select Category</option>
                        <?php
                           if($categorylist) {
                              foreach($categorylist as $categorylists) {
                           ?>
                        <option value="<?php echo $categorylists['id'];?>"> <?php echo $categorylists['category'];?> </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
                  <div class="onewayfields">
                     <select name="subcategory" id="subcatress" class="form-control forminputspswd" placeholder="Select Subcategory" style="margin-left: 0px;">
                        <option value="">Select Subcategory</option>
                        <?php
                           if($subcategorylist) {
                              foreach($subcategorylist as $subcategorylists) {
                           ?>
                        <option value="<?php echo $subcategorylists['id'];?>"> <?php echo $subcategorylists['subcategory'];?> </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
                  <div class="onewayfields">
                     <p class="blachd">Currently Working on this role  <input type ="checkbox" value="1" id="checkwid" name="regularcheck" class="check_status" onchange="valueChangedd()"></p>
                     <div class="forminputspswd">
                        <p>From</p>
                        <input type='text' name="from" autocomplete="off" class="form-control" id="datepicker5" /> 
                        <!--   <input type="text" class="form-control" id="datepicker"> -->
                     </div>
                     <span id="rgstt" style="float: left;">
                        <p class="text-success"></p>
                     </span>
                     <div class="forminputspswd" id="statusTOdatee">
                        <p>To</p>
                        <input type='text' name="to" class="form-control" autocomplete="off" id="datepicker6" />
                        <i class="far fa-calendar-alt fieldicons"></i>
                     </div>
                  </div>
                  <div class="forminputspswd">
                     <textarea class="form-control" name="desc" id="desc" placeholder="Add Description"></textarea> 
                  </div>
                  <div class="forminputspswd">
                     <input type="hidden" name="id" id="workid">
                     <button type="submit" class="srchbtns">Update</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>

<div class="modal fade" id="mymodelll" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered addeducation" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="workexperience addresumes editssections">
               <p>Add Education</p>
               <div class="formmidaress modpassfull">
                  <div class="filldetails">
                     <form method="post" action="<?php echo base_url();?>user/education">
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <select name="attainment" id="attainment" class="form-control" placeholder="Attainment" style="width:100%">
                                 <option value="">Choices for Educational Attainment</option>
                                 <option value="High School Graduate" >High School Graduate</option>
                                 <option value="Vocational" >Vocational</option>
                                 <option value="Undergraduate" >Undergraduate</option>
                                 <option value="Associate Degree" >Associate Degree</option>
                                 <option value="College Graduate" >College Graduate</option>
                                 <option value="Post Graduate" >Post Graduate</option>
                              </select>
                              <!-- <input type="text" name="attainment" class="form-control" id="attainment" placeholder="Highest Attainment"> -->
                           </div>
                           <div class="forminputspswd">
                              <input type="text" name="degree" class="form-control" placeholder="Degree/Course" id="degree">
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="text" name="university" class="form-control" placeholder="University/Institute" id="university">
                        </div>
                        <div class="onewayfields">
                           <div class="forminputspswd">
                              <p>From</p>
                              <input type='text' name="from" class="form-control" id='datepicker111' autocomplete="off" /> 
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                           <div class="forminputspswd">
                              <p>To</p>
                              <input type='text' name="to" class="form-control" id='datepicker122' autocomplete="off" />
                              <i class="far fa-calendar-alt fieldicons"></i>
                           </div>
                        </div>
                        <div class="forminputspswd">
                           <input type="hidden" name="id" id="setid">
                           <button type="submit" class="srchbtns">Update</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<script src="<?php echo base_url().'webfiles/';?>js/moment.min.js"></script>
<script src="<?php echo base_url().'webfiles/';?>js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
   $.validate({
     modules : 'location, date, security, file',
     onModulesLoaded : function() {
       $('#country').suggestCountry();
     }
   });
   
   // Restrict presentation length
   $('#presentation').restrictLength( $('#pres-max-length') );
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script>
<script type="text/javascript">
   google.maps.event.addDomListener(window, 'load', function () {
      var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
    
       google.maps.event.addListener(places, 'place_changed', function () {
         
           var place = places.getPlace();
           var address = place.formatted_address;
           var latitude = place.geometry.location.A;
           var longitude = place.geometry.location.F;
           var mesg = "Address: " + address;
           mesg += "\nLatitude: " + latitude;
           mesg += "\nLongitude: " + longitude;
       });
   });
</script>
<script>
   $( function() {
     $( "#datepicker" ).datepicker();
   } );
</script>
<script>
   $( function() {
     $( "#datepicker1" ).datepicker();
   } );
</script>
<script>
   $( function() {
     $( "#datepicker11" ).datepicker();
   } );
</script>
<script>
   $( function() {
     $( "#datepicker12" ).datepicker();
   } );
</script>
<script>
   $( function() {
     $( "#datepicker5" ).datepicker();
   } );
</script>
<script>
   $( function() {
     $( "#datepicker6" ).datepicker();
   } );
</script>
<script>
   $( function() {
     $( "#datepicker111" ).datepicker();
   } );
</script>
<script>
   $( function() {
     $( "#datepicker122" ).datepicker();
   } );
</script>
<script>
   $( function() {
     $( "#proid" ).datepicker({
       changeYear: true,
       changeMonth: true,
       maxDate: '-1'
     });
   } );
</script>
<script>
   function gatval(id){
     
   $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>" + "user/updateworkview",
         data: {id:id},
         cache:false,
       
         success:function(htmldata){
           var result = JSON.parse(htmldata);
          // alert(result[0].level);
           $("#camp_name").val(result[0].company);
           $("#title").val(result[0].title);
           $("#slcttt select").val(result[0].level);
           $("#desc").val(result[0].jobDesc);
          $("#datepicker5").val(result[0].workFrom);
           $("#datepicker6").val(result[0].workTo);
          $("#slctcat select").val(result[0].category);
          $("#slctsubcatttt select").val(result[0].subcategory);
          $("#workid").val(result[0].id);
           $("#mymodell").modal(result);
         },
         error:function(){
           console.log('error');
         }
     });
     
   }
   
</script>
<script>
   function deleteRecord(id,tblname){
   
      var condel = confirm("Are you sure you want to delete this client?");
      if(condel)
      $.ajax({
       type: 'POST',
       url: "<?php echo base_url('user/delete_single_work')?>",
       data:'&user_id='+id+'&tbname='+tblname,
       success: function (html) {  
      // alert(html);die(); 
         if(html==1){
            $('#error_msg').html('<p class="alert alert-success" >Client  deleted Successfully.</p>');
             window.setTimeout(function(){location.reload()},1000)
         }else{
            $('#error_msg').html('<p class="alert alert-error flash" >Client  not Deleted.</p>');
            hide_html('error_brand');
         }
       }
     });
   }
</script>
<script>
   function gatvaledu(id){
       
      $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "user/updateeduview",
            data: {id:id},
            cache:false,
          
            success:function(htmldata){
             //alert(htmldata);die();
              var result = JSON.parse(htmldata);
               
              $("#attainment").val(result[0].attainment);
              $("#degree").val(result[0].degree);
              $("#university").val(result[0].university);
              $("#datepicker111").val(result[0].degreeFrom);
              $("#datepicker122").val(result[0].degreeTo);
              $("#setid").val(result[0].id);
              $("#mymodelll").modal(result);
              
           
            },
            error:function(){
              console.log('error');
            }
        });  
    }
   
</script>
<script>
   $(document).ready(function(){   
   $("#changepassbtn").click(function() {
     //var oldpass = $("#oldpass").val();
     var newpass = $("#newpass").val();
     var confpass = $("#confpass").val();
     $.ajax({
         type: "POST",
         url: "<?php echo base_url(); ?>" + "user/changepass",
         data: {newpass:newpass,confpass:confpass},
         cache:false,
         success:function(htmldata){
         
              $('#errorfield').html(htmldata);
         },
         error:function(){
           console.log('error');
         }
     });
   });
   });
</script>
<script>
   function getSubcat()
   {
    var categoryid= $("#slctsubcat").val();
    $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/SubcategoryListing",
          data: {categoryid:categoryid},
          cache:false,
          success:function(htmldata){
            
             $("#subcatres").html(htmldata);
          
          },
          error:function(){
            console.log('error');
          }
      });
    
   }
</script>
<script>
   function getSubcatt()
   {
    var categoryid= $("#slctsubcatt").val();
    //alert(categoryid);return false;
    $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/SubcategoryListing",
          data: {categoryid:categoryid},
          cache:false,
          success:function(htmldata){
            
             $("#subcatress").html(htmldata);
          
          },
          error:function(){
            console.log('error');
          }
      });
    
   }
</script>
<script type="text/javascript">
   function valueChanged()
   {
       if($('.check_status').is(":checked"))
       {  
           $("#statusTOdate").hide();
           $("#rgst").html('<p class="text-success">Present</p>');
        }
       else
       {
           $("#statusTOdate").show();
            $("#rgst").html('');
       }
   }
</script>
<script type="text/javascript">
   function valueChangedd()
   {
       if($('.check_status').is(":checked"))
       {  
           $("#statusTOdatee").hide();
           $("#rgstt").html('<p class="text-primary">Present</p>');
        }
       else
       {
           $("#statusTOdatee").show();
            $("#rgstt").html('');
       }
   }
</script>
<script>
  function getFilename() {
      var name = document.getElementById("file").files[0].name;
      var filetype= $("#file").val();
      var ext = filetype.split('.').pop();
      if(ext =="jpg" || ext =="jpeg" || ext =="png"){
        $("#setimgres").html("<span style='color:red'>plese select file format(pdf/doc/docx)</span>");
        $('#resumebtn').attr('disabled','true');
        return false;

      } else {
          $('#resumebtn').attr('disabled',false);
          $("#setimgres").html("");
      } 
  }
</script>

<?php
  $geturlerror = $this->uri->segment(3);
  if(!empty($geturlerror)) {
    $error = urldecode($this->uri->segment(3));
?>
<script>
      alert("<?php echo $error; ?> ");
</script>
<?php
    }
?>

<script>
   $("#clientbtn").click(function(){
      var clientname = $("#clients").val();
      if(clientname=="")
      {
        $("#respclient").html("<p class='text-danger'>please add client</p>");
        return false; 
      }
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/topclients",
          data: {clients:clientname},
          cache:false,
          success:function(htmldata){
              $("#respclient").html(htmldata);
              $("#clients").val('');
               setTimeout(function(){
                location.reload();
                }, 2000); 
            // $("#subcatress").html(htmldata);
          
          },
          error:function(){
            console.log('error');
          }
      });
   });
</script>
<script>
   function getclientval(id)
   {
    var clientId= $("#clients"+id).val();
     var clientname = $("#cli"+id).val();
       if(clientname=="")
       {
         $("#respclient"+id).html("<p class='text-danger'>please add clients</p>");
         return false; 
       }
         $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/topclientsedit",
           data: {clients:clientname,clientId:clientId},
           cache:false,
           success:function(htmldata){
               $("#respclient"+id).html(htmldata);
               //$("#cli"+id).val('');
                setTimeout(function(){
                 location.reload();
                 }, 1000); 
             // $("#subcatress").html(htmldata);
           
           },
           error:function(){
             console.log('error');
           }
       });
   
   }
</script>
<script>
   $("#languagebtn").click(function(){
      var languagees = $("#languagees").val();
   
      if(languagees=="")
      {
        $("#resplanguage").html("<p class='text-danger'>please add Language</p>");
        return false; 
      }
      else
      {
          $("#resplanguage").html(""); 
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/languageadd",
          data: {language:languagees},
          cache:false,
          success:function(htmldata){
              $("#resplanguage").html(htmldata);
              $("#languagees").val('');
               setTimeout(function(){
                location.reload();
                }, 2000); 
            // $("#subcatress").html(htmldata);
          
          },
          error:function(){
            console.log('error');
          }
      });
     }
   });
</script>
<script>
   function getlangval(id)
   {
    var langId= $("#languageDataS"+id).val();
     var langname = $("#languagees"+id).val();
       if(langname=="")
       {
         $("#resplang"+id).html("<p class='text-danger'>please add language</p>");
         return false; 
       }
         $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/languageedit",
           data: {langname:langname,langId:langId},
           cache:false,
           success:function(htmldata){
               $("#resplang"+id).html(htmldata);
               //$("#languagees"+id).val('');
               setTimeout(function(){
                 location.reload();
                 }, 2000); 
             // $("#subcatress").html(htmldata);
           
           },
           error:function(){
             console.log('error');
           }
       });
   
   }
</script>
<script>
   $("#skillsbtn").click(function(){
      var skills = $("#skills").val();
   
      if(skills=="")
      {
        $("#respskill").html("<p class='text-danger'>please add Skill</p>");
        return false; 
      }
      else
      {
          $("#respskill").html(""); 
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/skilladd",
          data: {skill:skills},
          cache:false,
          success:function(htmldata){
              $("#respskill").html(htmldata);
              $("#skills").val('');
               setTimeout(function(){
                location.reload();
                }, 2000); 
            // $("#subcatress").html(htmldata);
          
          },
          error:function(){
            console.log('error');
          }
      });
     }
   });
</script>
<script>
   function getskilval(id)
   {
    var skillId= $("#skillDataS"+id).val();
     var skill = $("#skill"+id).val();
       if(skill=="")
       {
         $("#respskill"+id).html("<p class='text-danger'>please add skill</p>");
         return false; 
       }
         $.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "user/skilledit",
           data: {skill:skill,skillId:skillId},
           cache:false,
           success:function(htmldata){
               $("#respskill"+id).html(htmldata);
               //$("#skill"+id).val('');
               setTimeout(function(){
                 location.reload();
                 }, 2000); 
             // $("#subcatress").html(htmldata);
           
           },
           error:function(){
             console.log('error');
           }
       });
   
   }
</script>
<script>
   $(document).ready(function () {
      $("input[name='expert[]']").change(function () {
          var maxAllowed = 3;
          var cnt = $("input[name='expert[]']:checked").length;
          if (cnt > maxAllowed) {
              $(this).prop("checked", "");
              alert('You can select maximum ' + maxAllowed + ' expert!!');
          }
      });
   });
</script>
<script>
   $(document).ready(function () {
   $("#salary").keypress(function (e){
      if(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
     }
    });
   });
   
   
   
</script>
<script>
   $(document).ready(function () {
   $("#phone12").keypress(function (e){
      if(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
     }
    });
   });
   
   function readURL(input) {
   
      if (input.files && input.files[0]) {
        var reader = new FileReader();
    
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
    
        reader.readAsDataURL(input.files[0]);
      }
    }
    
    $("#imgInp").change(function() {
      readURL(this);
    });
   
</script>
<?php include_once('footer.php'); ?>
</body>
</html>

