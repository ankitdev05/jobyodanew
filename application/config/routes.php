<?php
defined('BASEPATH') OR exit('No direct script access allowed');
session_start();
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['user/(:any)'] = 'user/otpverification/$1';
//$CI = &get_instance();
//$CI->load->library("session");

$route['default_controller'] = 'user';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['home'] = 'user/home';

$route['jobs'] = 'user/find_jobs';
$route['jobs/(:any)'] = 'user/find_jobs';
$route['jobs/(:any)/(:any)?'] = 'user/find_jobs';

$route['jobs1'] = 'homepage/find_jobs';
$route['jobs1/(:any)'] = 'homepage/find_jobs';
$route['jobs1/(:any)/(:any)?'] = 'homepage/find_jobs';

$route['job_listings/(:any)/(:any)?'] = 'dashboard/find_jobs';

$route['site_details/(:any)'] = 'user/companyProfileDetails';

$route['company_details/(:any)'] = 'user/recruiterProfileDetails';

$route['search'] = 'user/home_search';

//if(isset($CI->session->userdata('usersess'))) {
//if(isset($_SESSION['usersess'])) {
//	$route['job/description/(:any)'] = 'dashboard/jobDescription';
//} else {
$route['job/description/(:any)'] = 'user/jobDescription';
//}

$route['thank_you'] = 'user/thankyou';

$route['our_partners'] = 'user/ourpartners';
$route['city_search'] = 'user/citysearch';
$route['your_expertise'] = 'user/yourexpertise';
$route['contact'] = 'user/contact';
$route['sitemap'] = 'user/sitemap';
$route['forgotpassword/(:any)'] = 'user/forgotpasswordviewapp/$1';
$route['forgot/(:any)'] = 'user/forgotpasswordview/$1';
$route['privacy_policy'] = 'frontpage/privacy';
$route['terms'] = 'frontpage/terms';
$route['about'] = 'frontpage/aboutUs';
$route['faq'] = 'frontpage/faq';
$route['how_it_works'] = 'frontpage/howitworks';

$route['profile'] = 'dashboard/myprofile';

$route['signinbonus'] = 'dashboard/signinBonus';
$route['savedjob'] = 'dashboard/savedjob';
$route['hotjob'] = 'dashboard/hotjob';
$route['appliedjobs'] = 'dashboard/appliedJobs';
$route['notifications'] = 'dashboard/notifications';
$route['successstory'] = 'dashboard/successstory';
//$route['search'] = 'user/search1';

//  new urls
	$route['new_login'] = 'auth/login';
	$route['new_register_one'] = 'auth/registerstepone';
	$route['new_register_two'] = 'auth/registersteptwo';
	$route['new_otp'] = 'auth/verifyotp';
	$route['new_forogt_password'] = 'auth/forgotpassword';
	$route['new_set_password'] = 'auth/setpassword';
	$route['new_success_reset'] = 'auth/successreset';
	$route['new_success_signup'] = 'auth/successsignup';
//

$route['recruiter'] = 'recruiter/testimonial/testimonial_view';
$route['recruiter/login'] = 'recruiter/recruiter/index';


$route['recruiter/signup'] = 'recruiter/recruiter/signup';
$route['recruiter/subrecuriterlist'] = 'recruiterN/recruiter/recruiterlist';
$route['recruiter/addsubrecruiter'] = 'recruiterN/recruiter/addrecruiter';
$route['recruiter/createpassword/(:any)'] = 'recruiterN/recruiter/createpassword/$1';
$route['recruiter/passwordchange/(:any)'] = 'recruiter/recruiter/passwordchange/$1';
$route['recruiter/companyprofile'] = 'recruiter/recruiter/companyprofile';
$route['recruiter/postjob'] = 'recruiter/jobpost/index';

$route['recruiter/assignjob'] = 'recruiter/jobpost/assignjob';

$route['recruiter/managejobs'] = 'recruiter/jobpost/manageJobView';
$route['recruiter/managecandidates'] = 'recruiter/candidate/manageCandidates';
$route['recruiter/recruiterprofile'] = 'recruiter/recruiter/recruiterprofile';
$route['recruiter/transaction'] = 'recruiter/recruiter/transaction';
$route['recruiter/rating'] = 'recruiter/recruiter/rating';
$route['recruiter/jobwisereport'] = 'recruiter/report/jobwise_report';
$route['recruiter/candidatereport'] = 'recruiter/report/candidate_report';
$route['recruiter/screeningreport'] = 'recruiter/report/screening_report';
$route['recruiter/contact'] = 'recruiter/recruiter/contact';
$route['recruiter/faq'] = 'recruiter/recruiter/faq';
$route['recruiter/manageCompany'] = 'recruiter/recruiter/manageCompany';
$route['recruiter/filtercandidate'] = 'recruiter/candidate/filtercandidate';
$route['recruiter/filtermanagecandidate'] = 'recruiter/candidate/filtermanagecandidate';
$route['recruiter/searchCandidates'] = 'recruiter/candidate/searchCandidates';

$route['recruiter/advertisement'] = 'recruiter/recruiter/add_advertisement';
$route['recruiter/advertisementlist'] = 'recruiter/recruiter/advertise_list';

$route['recruiter/videoadvertisement'] = 'recruiter/recruiter/add_videoadvertisement';
$route['recruiter/videoadvertisementlist'] = 'recruiter/recruiter/videoadvertise_list';

$route['recruiter/testimonialview'] = 'recruiter/testimonial/testimonial_view';
$route['recruiter/testimoniallist'] = 'recruiter/testimonial/testimonial_list';
$route['recruiter/testimonialadd'] = 'recruiter/testimonial/testimonial_add';
$route['recruiter/testimonialedit'] = 'recruiter/testimonial/testimonial_edit';

$route['recruiter/transfer_recruiter'] = 'recruiter/recruiter/transfer';

$route['administrator'] = 'administrator/admin/index';
$route['administrator/dashboard'] = 'administrator/dashboard/index';
$route['administrator/forgot'] = 'administrator/admin/forgot';
$route['administrator/recruiterList'] = 'administrator/recruiter/recruiterListing';
$route['administrator/pendingrecruiters'] = 'administrator/recruiter/pending_recruiters';
$route['administrator/companyList'] = 'administrator/recruiter/companyListing';
$route['administrator/joblist'] = 'administrator/Jobpost/index';
$route['administrator/jobexpirelist'] = 'administrator/Jobpost/jobexpirelist';
$route['administrator/jobseekerlist'] = 'administrator/JobSeeker/jobseekerlisting';
$route['administrator/storylist'] = 'administrator/recruiter/storyListing';
$route['administrator/contentlist'] = 'administrator/recruiter/staticcontentlist';
$route['administrator/loadingscreen'] = 'administrator/JobSeeker/loadingscreen';
$route['administrator/addrecruiter'] = 'administrator/recruiter/addRecruiter';
$route['administrator/boostjobs'] = 'administrator/Jobpost/boostjobs';
$route['administrator/addprice'] = 'administrator/Recruiter/addprice';
$route['administrator/hiringreport'] = 'administrator/Jobpost/hiring_reports';
$route['administrator/screeningreport'] = 'administrator/Jobpost/screening_reports';
$route['administrator/notappliedreport'] = 'administrator/Jobpost/notappliedjob_reports';

$route['administrator/userreport'] = 'administrator/Jobpost/user_reports';
$route['administrator/nonuserreport'] = 'administrator/Jobpost/non_user_reports';


$route['administrator/invoice'] = 'administrator/Recruiter/invoice';
$route['administrator/contactlist'] = 'administrator/JobSeeker/contactlisting';
$route['administrator/resetpassword'] = 'administrator/admin/reset';
$route['administrator/addfaq'] = 'administrator/recruiter/addfaq';
$route['administrator/faqlist'] = 'administrator/recruiter/faqlist';
$route['administrator/addjobtitle'] = 'administrator/recruiter/addJobTitle';
$route['administrator/addindustry'] = 'administrator/recruiter/addIndustry';
$route['administrator/addjoblevel'] = 'administrator/recruiter/addJobLevel';
$route['administrator/addjobcategory'] = 'administrator/recruiter/addJobCategory';
$route['administrator/addjobsubcategory'] = 'administrator/recruiter/addJobSubCategory';
$route['administrator/addskills'] = 'administrator/recruiter/addSkills';
$route['administrator/addstaticcontent'] = 'administrator/recruiter/addstaticcontent';
$route['administrator/addsuperpower'] = 'administrator/recruiter/addPower';
$route['administrator/reconciliation'] = 'administrator/JobSeeker/reconciliation';
$route['administrator/notifications'] = 'administrator/JobSeeker/notifications';

$route['administrator/notificationlist'] = 'administrator/JobSeeker/notificationslist';

$route['administrator/jobnotificationlist'] = 'administrator/JobSeeker/jobnotificationslist';

$route['administrator/addreferral'] = 'administrator/recruiter/addreferral';
$route['administrator/referralreport'] = 'administrator/recruiter/referralreport';
$route['administrator/hirecost'] = 'administrator/Jobpost/hirecost';

$route['administrator/adadvertise'] = 'administrator/Recruiter/adadvertise';
$route['administrator/advertisement'] = 'administrator/Recruiter/advertiselist';

$route['administrator/videoadvertise'] = 'administrator/Recruiter/videoadvertise';
$route['administrator/videoadvertisement'] = 'administrator/Recruiter/videoadvertiselist';
$route['administrator/addvideoprice'] = 'administrator/Recruiter/addvideoprice';

$route['administrator/news'] = 'administrator/Recruiter/newsadvertise';
$route['administrator/newslist'] = 'administrator/Recruiter/newsadvertiselist';

$route['administrator/apiversioning'] = 'administrator/JobSeeker/apiversioning';

$route['administrator/recruitertransfer'] = 'administrator/Recruiter/recruitertransfer';
$route['administrator/recruiter_testimonial'] = 'administrator/Recruiter/testimonial_view';

$route['administrator/subscription_plans'] = 'administrator/subscription/subscriptionlist';
$route['administrator/subscription_plan_add'] = 'administrator/subscription/subscriptionadd';
$route['administrator/subscription_plan_create'] = 'administrator/subscription/subscriptioncreate';
$route['administrator/subscription_plan_edit'] = 'administrator/subscription/subscriptionedit';
$route['administrator/subscription_plan_update'] = 'administrator/subscription/subscriptionupdate';