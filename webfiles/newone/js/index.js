// Ofer Slider

$(document).ready(function () {
    $("#Advertisement").owlCarousel({
        margin: 10,
        smartSpeed: 1000,
        autoplay: 5000,
        center:true,
        nav: false,
        dots:true,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 3,
            },
        },
    });
});

$(document).ready(function () {
    $("#Company").owlCarousel({
        margin: 10,
        smartSpeed: 1000,
        autoplay: 5000, 
        nav: true,
        dots:false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            },
        },
    });
});

$(document).ready(function () {
    $("#Expertise").owlCarousel({
        margin: 0,
        smartSpeed: 1000,
        autoplay: 5000, 
        nav: true,
        dots:false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            },
        },
    });
});

$(document).ready(function () {
    $("#City").owlCarousel({
        margin: 10,
        smartSpeed: 1000,
        autoplay: 5000, 
        nav: true,
        dots:false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            },
        },
    });
});

$(document).ready(function () {
    $("#Check").owlCarousel({
        margin: 0, 
        center:true,
        smartSpeed: 1000,
        autoplay: 5000, 
        nav: true,
        dots:false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 3,
            },
        },
    });
});

$(document).ready(function () {
    $(".JobSlider").owlCarousel({
        margin: 0,
        smartSpeed: 1000,
        autoplay: 5000, 
        nav: true,
        dots:false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 3,
            },
        },
    });
});

$(document).ready(function () {
    $(".recomendSlider").owlCarousel({
        margin: 0,
        smartSpeed: 1000,
        autoplay: 5000, 
        nav: true,
        dots:false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 3,
            },
        },
    });
});

$(document).ready(function () {
    $("#JobsAvailable").owlCarousel({
        margin: 0, 
        nav: true,
        dots:false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 5,
            },
        },
    });
});

$(document).ready(function () {
    $("#Locations").owlCarousel({
        margin: 0, 
        nav: true,
        dots:false,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        loop: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            },
        },
    });
});

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 80) {
        $(".Header").addClass("Fixed");
    } else {
        $(".Header").removeClass("Fixed");
    }
});

$.fn.jQuerySimpleCounter = function( options ) {
    var settings = $.extend({
        start:  0,
        end:    100,
        easing: 'swing',
        duration: 400,
        complete: ''
    }, options );

    var thisElement = $(this);

    $({count: settings.start}).animate({count: settings.end}, {
        duration: settings.duration,
        easing: settings.easing,
        step: function() {
            var mathCount = Math.ceil(this.count);
            thisElement.text(mathCount);
        },
        complete: settings.complete
    });
};
var numberhtml = $('#Number').html();
var number1html = $('#Number1').html();
$('#Number').jQuerySimpleCounter({end: numberhtml,duration: 3000});
$('#Number1').jQuerySimpleCounter({end: number1html,duration: 3000});


// let spinnerWrapper = document.querySelector('.Loader');

// window.addEventListener('load', function () { 
//     spinnerWrapper.parentElement.removeChild(spinnerWrapper);
// });
  